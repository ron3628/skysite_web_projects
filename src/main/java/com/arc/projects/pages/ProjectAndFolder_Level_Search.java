package com.arc.projects.pages;

import java.awt.AWTException;
import java.awt.List;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.RobotUtils;
import com.arc.projects.utils.SkySiteUtils;
import com.arc.projects.utils.randomFileName;
import com.arcautoframe.utils.Log;

public class ProjectAndFolder_Level_Search extends LoadableComponent<ProjectAndFolder_Level_Search> {

	WebDriver driver;

	private boolean isPageLoaded;

	@FindBy(xpath = "//*[@id='txtComment']")
	WebElement Addcomment;

	@FindBy(xpath = "//i[@class='icon icon-sort-by-attributes-alt']")
	WebElement Ascending;

	@FindBy(xpath = "//*[text()='All RFI']")
	WebElement AllRFITab;

	@FindBy(css = "#liFirstTab")
	WebElement FirstRFITab;

	@FindBy(xpath = "//*[@id='iSortOrder']")
	WebElement AssendAndDecend;

	@FindBy(xpath = "//*[@id='btnSubmit']")
	WebElement Submitbutton;

	@FindBy(css = "#FeedbackClose>span")
	WebElement FeedBackmeesage;

	@FindBy(css = "#button-1")
	WebElement YesButton;

	@FindBy(xpath = "//span [@class='img-circle profile-no-image md']")
	WebElement Profile;

	@FindBy(xpath = "(//a[@onclick='javascript:return confirmlogout(this);'])[2]")
	WebElement Logout;

	@FindBy(xpath = "//i[@class='icon icon-app-project icon-2x pulse animated']")
	WebElement ProjectManagement;

	@FindBy(css = "#a_showRFIList")
	WebElement ProjectManagement_RFI;

	@FindBy(xpath = "//a[@class ='aPunch']")
	WebElement ProjectManagement_Punch;

	@FindBy(css = "#a_showSUBMITTALList")
	WebElement ProjectManagement_Submittal;

	@FindBy(xpath = "//button[@class='btn btn-primary btn-lg new-rfi']")
	WebElement CreateRFI_Button;

	@FindBy(xpath = "(//i [@class='icon icon-app-contact icon-lg'])[2]")
	WebElement TOMail;

	@FindBy(xpath = "(//i [@class='icon icon-app-contact icon-lg'])[3]")
	WebElement ccMail;

	@FindBy(xpath = "//div/div[2]/div[2]/div[1]/div[8]/div/input")
	WebElement NewcustomAttributeField;

	@FindBy(xpath = "//*[@id='txtDisciplineData']")
	WebElement Discipline;

	@FindBy(xpath = "//*[@id='txtQuestion']")
	WebElement Question;

	@FindBy(xpath = "//input[@data-bind='value: CP1']")
	WebElement Attribute1;

	@FindBy(xpath = "//input[@data-bind='value: CP2']")
	WebElement Attribute2;

	@FindBy(xpath = "//input[@data-bind='value: CP3']")
	WebElement Attribute3;

	@FindBy(xpath = "//input[@data-bind='value: CP4']")
	WebElement Attribute4;

	@FindBy(xpath = "//input[@data-bind='value: CP5']")
	WebElement Attribute5;

	@FindBy(xpath = "(//span[@ class='yellow-txt'])[1]")
	WebElement OpenStatusD;

	@FindBy(xpath = "(//i [@class='icon icon-rfi icon-3x rfi-stump pull-left float-icon pw-icon-lg-orange'])[1]")
	WebElement RFIImage;

	@FindBy(css = "#reassign_block_read-only>small")
	WebElement RFI_Email_TO;

	@FindBy(css = "#CCRecipient")
	WebElement RFI_Email_CC;

	@FindBy(xpath = "//*[@id='txtAnswerEditable']")
	WebElement AnswerEditBox;
	
	
	//@FindBy(xpath = "//button[@data-bind='click: ReplyRFI, clickBubble: false, visible: CanReplyRFI()']")
	@FindBy(xpath = "//*[contains(text(),'Reply') and @type='button']")
	WebElement ReplyButton;
	
	//button[@data-bind='click: ReplyRFI, clickBubble: false, visible: CanReplyRFI()']

	@FindBy(xpath = "(//i[@class='icon icon-paper-clip icon-lg'])[2]")
	WebElement AddAttachment;

	@FindBy(xpath = "//*[@id='btnCreateRFI']")
	WebElement ButtonRFI;
	
	@FindBy(xpath = "//button [@class='close tabindex' and  @tabindex='1']")
	WebElement CloseButton;
	
	@FindBy(xpath = "//a [@class='btn btn-primary project-tab']")
	WebElement ProjectDashboard;
	
	

	@FindBy(xpath = "//*[@id='punchAttachedFiles']/span[1]")
	WebElement AttachementCount;

	@FindBy(xpath = "//*[@id='divEditRFIUI']/div/div[1]/div[1]/div[1]/div[3]/h4/span[2]")
	WebElement Subject1;

	@FindBy(xpath = "//*[@id='txtSubject']")
	WebElement Subject;

	@FindBy(xpath = "//span[@data-bind='html: Question']")
	WebElement RFI_Questions;

	@FindBy(xpath = "//span[@class='label label-warning pull-right pw-status']")
	WebElement RFI_Status;

	@FindBy(xpath = "//h4 [@class='info-modal has-tooltip']")
	WebElement QuestionDropdown;

	@FindBy(xpath = "(//div[@class='col-sm-6']/h5)[3]")
	WebElement Automation_Status1;

	@FindBy(xpath = "(//div[@class='col-sm-6']/h5)[4]")
	WebElement Automation_Status2;

	@FindBy(xpath = "(//div[@class='col-sm-6']/h5)[5]")
	WebElement Automation_Status3;

	@FindBy(xpath = "(//div[@class='col-sm-6']/h5)[6]")
	WebElement Automation_Status4;

	@FindBy(xpath = "(//div[@class='col-sm-6']/h5)[7]")
	WebElement Automation_Status5;

	@FindBy(xpath = "(//span [@class='label label-warning'])[1]")
	WebElement duedate;

	@FindBy(xpath = "//*[@id='btn-cancel-file']")
	WebElement Cancel;

	@FindBy(xpath = "//*[@id='btnCreateRFIDraft']")
	WebElement SaveAsdraft;

	@FindBy(xpath = "//*[@id='btnCreateRFIDraft']")
	WebElement CreateRFI_BUTTON;

	@FindBy(xpath = "//*[@id='btnSearchOptions']")
	WebElement Search_GlobalButton;

	@FindBy(xpath = "//*[@id='txtSearchKeyword']")
	WebElement TxtSearch_Keyword;

	@FindBy(xpath = "//i[@class='icon icon-filter']")
	WebElement AdvanceSearch_Link;

	@FindBy(xpath = "//input [@id='txtStatus_filter']")
	WebElement Status_Inbox;

	@FindBy(css = "#btnSearchFilter")
	WebElement SearchButton_Advance;

	@FindBy(css = "#btnSearch")
	WebElement Search_Button;

	@FindBy(xpath = "//button[@class='btn btn-primary' and @data-bind='click: SelectContactEvent']")
	WebElement SelectUser_Button;

	@FindBy(xpath = "//li[@ id='liWhereIAmAll']")
	WebElement Everywhere_AllContents;

	@FindBy(xpath = "//*[@id='liWhereIAmRfi']/a")
	WebElement RFI_search;
	

	@FindBy(xpath = "//h4 [@class='info-modal has-tooltip']")
	WebElement RFI_ModelInfo;
	
	
	@FindBy(xpath = "//small[@ data-bind='text: CP1']")
	WebElement CustomAttribute_text ;
	

	@FindBy(css = "#liWhereIAmSearchIn>a")
	WebElement search;

	@FindBy(xpath = "(//div [@class='row has-tooltip'])[1]")
	WebElement FirstRow_Click;

	@FindBy(css = ".btn.btn-default.btn-forward")
	WebElement Forward_Button;

	@FindBy(xpath = "(//i [@class='icon icon-app-contact icon-lg'])[3]")
	WebElement Forward_Contactbutton;

	@FindBy(xpath = "(//i[@class='icon icon-rfi icon-lg'])[2]")
	WebElement CreateRFILink;
	
	@FindBy(xpath = "(//i [@class='icon icon-rfi icon-3x rfi-stump pull-left float-icon pw-icon-grey'])[1]")
	WebElement DraftRFILink;
	


	@FindBy(css = "#txtUserSearchKeyword")
	WebElement SelectUserInbox;

	@FindBy(xpath = "//i [@class='icon icon-search icon-lg']")
	WebElement ForwadedSearchButton;

	@FindBy(xpath = "(//a [@class='input-group-addon pw-contact-list-modal'])[1]")
	WebElement SearchButton;

	@FindBy(xpath = "//a[@class='pull-left contact-img']")
	WebElement AssignTo_Checkbox;

	@FindBy(xpath = "//input[@class='pull-left']")
	WebElement To_Checkbox;

	@FindBy(xpath = "(//button[@value='save'])[4]")
	WebElement SelectuserButton;

	@FindBy(xpath = "//*[@id='btn-Load-attachment']")
	WebElement Addattachment;

	@FindBy(xpath = "//*[@id='txtForwardComment']")
	WebElement ForwardComment;

	@FindBy(xpath = "(//button [@class='btn btn-default'])[8]")
	WebElement Forwaded_SendButton;

	@FindBy(xpath = "(//li [@class='update-rfi'])[1]")
	WebElement RFIList;

	@FindBy(xpath = "(//li [@class='update-rfi'])")
	WebElement RFIList1;

	@FindBy(css = ".btn.btn-primary.set-files")
	WebElement selectfiles;

	@FindBy(xpath = "//*[@id='btnCreateRFI']")
	WebElement btnCreateRFI;
	
	
	@FindBy(xpath = "//*[@id='btnCreateRFIDraft']")
	WebElement saveAsDraft;

	@FindBy(xpath = "//a [@id='liFirstTab']")
	WebElement Myrfi;
	
	@FindBy(xpath = "//*[@id='liSecondTab']")
	WebElement RFI_Assignto_me;
	
	@FindBy(xpath = "//*[@id='liThirdTab']")
	WebElement AllRFI;

	@FindBy(xpath = "(//i [@class='icon icon-app-project icon-3x pw-icon-grey'])[1]")
	WebElement RFIFirstShortcut;
	
	@FindBy(xpath = "//span [@data-placement='top' ]")
	WebElement RFI_subject;
	
	//span [@data-placement='top' ]

	@FindBy(xpath = "(//span [@data-bind='text: RFINumber()'])[1]")
	WebElement RFIDisplay;

	@FindBy(xpath = "//*[@id='add-project-file']")
	WebElement Projectfiles;

	@FindBy(xpath = "//*[@id='txtDocSearchKeyword']")
	WebElement SearchProjectfilesInbox;

	@FindBy(xpath = "//i [@class='icon icon-search icon-lg']")
	WebElement ProjectfilesTab_searchButton;

	@FindBy(xpath = "//*[@id='ulDocumentList']/li[1]/div[1]")
	WebElement Folder;

	@FindBy(xpath = "//i[@class='icon icon-file-alt icon-3x']")
	WebElement File_CheckBox;

	@FindBy(xpath = "(//i[@class='icon icon-lg icon-download-alt'])[1]")
	WebElement DownloadButton;
	
	@FindBy(xpath = "//button[@class='btn btn-primary set-files']")
	WebElement Selectfiles;
	

	// ========Comparision RFI Number====================================

	@FindBy(xpath = "(//span [@data-bind='text: RFINumber()'])")
	WebElement DisplayRFINO;

	@FindBy(xpath = "//div[@class='prj-dtl']/h4")
	WebElement Displaysubject;

	@FindBy(xpath = "(//span [@class='label label-warning pull-right pw-status'])[1]")
	WebElement DisplayStatus;

	@FindBy(xpath = "//span [@class='noty_text']")
	WebElement RFISTATUS;

	@FindBy(xpath = "//span[@ class='yellow-txt']")
	WebElement OpenStatus;

	@FindBy(xpath = "//span[@ class='yellow-txt']")
	WebElement OpenStatus_popUp;

	@FindBy(xpath = "//span [@class='closed-txt']")
	WebElement CloseStatus;

	@FindBy(xpath = "//span [@class='closed-txt']")
	WebElement Close_POPUP;

	@FindBy(xpath = "(//span [@class='label label-info pull-right pw-status'])[1]")
	WebElement EmployeeStatus;
	
	@FindBy(xpath = "//span [@class='submited']")
	WebElement Responded;
	
	

	@FindBy(xpath = "//textarea[ @id='txtAnswerEditable']")
	WebElement textArea;

	@FindBy(xpath = "(//button [@class='btn btn-default'])[6]")
	WebElement REplay;

	@FindBy(xpath = "(//button[@data-original-title='Close'])[1]")
	WebElement closebutton;

	@FindBy(xpath = "//button [@class='btn btn-default' and @data-bind='click: CloseRFI, clickBubble: false, visible: CanClose()']")
	WebElement CloseRFIButton;

	@FindBy(xpath = "//button [@ data-bind='click: ReopenRFI, clickBubble: false, visible: CanReopen()']")
	WebElement ReopenRFI;

	@FindBy(xpath = "//button [@data-bind='click: ForwardRFI, clickBubble: false, visible: CanForwardRFI()']")
	WebElement Forwardbutton;

	@FindBy(xpath = "(//i [@class='icon icon-app-contact icon-lg'])[3]")
	WebElement Forward_Contact;

	@FindBy(xpath = " //textarea [@id='txtForwardComment']")
	WebElement TextArea;

	@FindBy(xpath = "//button[@data-bind='click: ForwardSendRFI, clickBubble: false, visible: CanForwardSendRFI()']")
	WebElement SendButton;

	@FindBy(xpath = "//*[@id='txtUserSearchKeyword']")
	WebElement SelectUse_Inbox;

	@FindBy(css = "input.pull-left")
	WebElement SelectUser_CheckBox;

	@FindBy(css = ".leaflet-marker-icon.icon.icon-rfi.icon-3x.pw-icon-lg-orange.leaflet-zoom-animated.leaflet-clickable")
	WebElement afterImagearea_RFI;
	
	@FindBy(xpath = "(//div [@class='pull-left']/i)[1]")
	WebElement RfiList_1;
	
	@FindBy(xpath = "(//div [@class='rfi-cc pull-left text-center float-icon'])[1]")
	WebElement RfiList_cc;
	
	
	@FindBy(xpath = "//*[@id='liSecondTab']")
	WebElement RFI_Assigntome;	
	
	
	@FindBy(xpath = "//textarea [@id='txtComment']")
	WebElement comment;
	
	@FindBy(xpath = "//*[@id='btnAttachment']")
	WebElement Addattachment_rfi;
	
	
	
	// =======================================================

	@Override
	protected void load() {
		isPageLoaded = true;

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}

	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory
	 * class.
	 * 
	 * @param driver
	 */
	public ProjectAndFolder_Level_Search(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	public boolean ObjectFeedback() {
		SkySiteUtils.waitForElement(driver, FeedBackmeesage, 20);
		Log.message("Waiting for FreeTrail Link to be appeared");
		if (FeedBackmeesage.isDisplayed())
			return true;
		else
			return false;
	}

	public boolean Logout() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		Log.message("Entered into Logout Method");
		SkySiteUtils.waitForElement(driver, Profile, 60);
		Profile.click();
		Log.message("Profile Option Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, Logout, 60);
		SkySiteUtils.waitTill(2000);
		Logout.click();
		Log.message("Logout option Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, YesButton, 60);
		YesButton.click();

		return result;

	}

	public ProjectDashboardPage YesButton_Alert() throws Throwable

	{
		SkySiteUtils.waitForElement(driver, YesButton, 60);
		YesButton.click();
		SkySiteUtils.waitTill(2000);
		String actualTitle = "Sign in - SKYSITE";
		String expectedTitle = driver.getTitle();
		Log.message("expected title is:" + expectedTitle);
		return new ProjectDashboardPage(driver).get();

	}
	
	
	public boolean RFI_photoUpload() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, RfiList_1, 60);
		RfiList_1.click();
		SkySiteUtils.waitTill(5000);
		Log.message("First RFI Selected Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, Addcomment, 60);
		String addComment = PropertyReader.getProperty("AddComment");
		Addcomment.sendKeys(addComment);
		SkySiteUtils.waitTill(5000);
		Log.message("Entered Value In Add Comment inbox: " + addComment);
		SkySiteUtils.waitForElement(driver, Addattachment_rfi, 60);
		Addattachment_rfi.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Add Attachment Clicked Sucessfully");
		// ===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_PhotoFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);
		Log.message("FileCount: " + FileCount);
		SkySiteUtils.waitTill(5000);
		//Outer Upload
		UploadFiles_old(FolderPath,FileCount);
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, ButtonRFI, 60);
		ButtonRFI.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Upload And Submit Button Selected Clicked Sucessfully");
		SkySiteUtils.waitTill(8000);
		SkySiteUtils.waitForElement(driver, CloseButton, 60);
		SkySiteUtils.waitTill(5000);
		CloseButton.click();
		Log.message("Close Button Clicked Sucessfully");	
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, ProjectDashboard, 60);
		SkySiteUtils.waitTill(5000);
		ProjectDashboard.click();
		Log.message("Clicked Project Dashboard Page");
		SkySiteUtils.waitTill(5000);
		return result;

	}
	
	
	
	public boolean RFI_replyAndChangeStatus() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		
		// RFI tab Clicked 
		SkySiteUtils.waitForElement(driver, RFI_Assigntome, 60);
		RFI_Assigntome.click();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, RFIImage, 60);
		RFIImage.click();
		SkySiteUtils.waitTill(5000);
		Log.message("First RFI Selected Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AnswerEditBox, 60);
		SkySiteUtils.waitTill(2000);
		AnswerEditBox.sendKeys("Entered the Value for Validation");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, ReplyButton, 60);
		ReplyButton.click();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, YesButton, 60);
		YesButton.click();
		SkySiteUtils.waitForElement(driver, Responded, 60);
		String Responded1 = Responded.getText();

		Log.message("RESPONDED Button Display: " + Responded);

		if (Responded1.equalsIgnoreCase("RESPONDED")) {
			Log.message("Status Responded Button verified Sucessfully ");
			return true;

		} else {
			Log.message("Status RESPONDED not verified Sucessfully");
			return false;
		}
		
		
		
	}

	public boolean RFI_EditValidation()throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		
		// RFI tab Clicked 
		SkySiteUtils.waitForElement(driver, RFI_Assigntome, 60);
		RFI_Assigntome.click();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, RfiList_cc, 60);
		RfiList_cc.click();
		SkySiteUtils.waitTill(5000);
		Log.message("First RFI Selected Clicked Sucessfully");
		
		
		
		if(driver.findElement(By.xpath("//*[@id='txtAnswerEditable']"))!= null){
			System.out.println("Element Edit answer Option is Present");
			Log.message("Element Edit answer Option is Present");
			return result = true;
			}
		    else
		    {
			System.out.println("Element edit answer Option Not  Present");
			Log.message("Element edit answer Option Not  Present");
			return result= false;
			}
		
		

	}
	
	public boolean RFI_EditValidation_notPresent()throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		
		// RFI tab Clicked 
		SkySiteUtils.waitForElement(driver, RFI_Assigntome, 60);
		RFI_Assigntome.click();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, RfiList_cc, 60);
		RfiList_cc.click();
		SkySiteUtils.waitTill(5000);
		Log.message("First RFI Selected Clicked Sucessfully");		
		
		if(driver.findElement(By.xpath("//*[@id='txtAnswerEditable']")).isSelected())
		{
			System.out.println("Element Edit answer Option is Present");
			Log.message("Element Edit answer Option is Present");
			return false;
		}
		else
		{
			System.out.println("Element edit answer Option NOT Present");
			Log.message("Element edit answer Option  NOT Present");
			return true;
		}
		
		

	}



	public boolean DownloadPDF() throws Throwable

	{
		boolean result = false;

		SkySiteUtils.waitForElement(driver, DownloadButton, 60);
		DownloadButton.click();

		Log.message("Download PDF Button Clicked Sucessfully");
		RobotUtils.initRobot();
		RobotUtils.pressDownKey(1);
		RobotUtils.pressEnterKey();
		//// String Downloadpath =
		//// PropertyReader.getProperty("FiledownloadPath");

		// Assert.assertTrue(isFileDownloaded(Downloadpath, "RFI 0001.pdf"),
		// "Failed to download Expected documen");
		/*
		 * String RFINumber = RFIDisplay.getText(); String RFIadd =
		 * "RFI"+""+RFINumber+".pdf";
		 * Log.message("Download PDF Button Clicked Sucessfully"+ RFIadd);
		 * Assert.assertTrue(isFileDownloaded(Downloadpath, "RFIadd"));
		 */

		return result;

	}

	private boolean isFileDownloaded_Ext(String dirPath, String ext) {
		boolean flag = false;
		File dir = new File(dirPath);
		File[] files = dir.listFiles();
		if (files == null || files.length == 0) {
			flag = false;
		}

		for (int i = 1; i < files.length; i++) {
			if (files[i].getName().contains(ext)) {
				flag = true;
			}
		}
		return flag;
	}

	public boolean isFileDownloaded(String downloadPath, String fileName) {
		boolean flag = false;
		File dir = new File(downloadPath);
		File[] dir_contents = dir.listFiles();

		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().equals(fileName))
				return flag = true;
		}

		return flag;
	}

	public boolean ProjectManagement() throws Throwable

	{
		boolean result = false;
		Thread.sleep(5000);
		SkySiteUtils.waitForElement(driver, ProjectManagement, 120);
		ProjectManagement.click();
		Log.message("project Management Link Click Sucessfully");
		SkySiteUtils.waitForElement(driver, ProjectManagement_RFI, 120);
		ProjectManagement_RFI.click();
		Thread.sleep(5000);
		Log.message("RFI Link Click Sucessfully");

		return result;

	}

	public boolean ProjectManagement_Submittal() throws Throwable

	{
		boolean result = false;
		Thread.sleep(2000);
		SkySiteUtils.waitForElement(driver, ProjectManagement, 60);
		ProjectManagement.click();
		Thread.sleep(2000);
		Log.message("project Management Link Click Sucessfully");
		SkySiteUtils.waitForElement(driver, ProjectManagement_Submittal, 60);
		ProjectManagement_Submittal.click();
		Log.message("Submittal Link Click Sucessfully");

		return result;

	}

	public boolean ProjectManagement_Punch() throws Throwable {
		boolean result = false;
		Thread.sleep(2000);
		SkySiteUtils.waitForElement(driver, ProjectManagement, 60);
		ProjectManagement.click();
		Thread.sleep(2000);
		Log.message("project Management Link Click Sucessfully");
		SkySiteUtils.waitForElement(driver, ProjectManagement_Punch, 60);
		ProjectManagement_Punch.click();
		Log.message("Punch Click Sucessfully");

		return result;

	}

	public boolean RFIProjectlevelMessageValidation() throws Throwable

	{
		boolean result = false;

		SkySiteUtils.waitForElement(driver, RFISTATUS, 120);
		String RFI = RFISTATUS.getText();
		Log.message("RFI Message  Display: " + RFI);

		if (RFI.contains("RFI number is not valid")) {
			Log.message("RFI number is not valid Status verified Sucessfully ");
			SkySiteUtils.waitTill(2000);
			return true;
		} else {
			Log.message("RFI number is not valid Status not verified Sucessfully");
			return false;
		}
	}

	public boolean RFIProjectlevelMessageValidation_blank() throws Throwable

	{
		boolean result = false;

		SkySiteUtils.waitForElement(driver, RFISTATUS, 120);
		String RFI = RFISTATUS.getText();
		Log.message("RFI Message  Display: " + RFI);

		if (RFI.contains("Starting RFI number required")) {
			Log.message("Starting RFI number required Status verified Sucessfully ");
			SkySiteUtils.waitTill(2000);
			return true;
		} else {
			Log.message("Starting RFI number required not verified Sucessfully");
			return false;
		}

	}

	public boolean MainStatus() throws Throwable

	{
		boolean result = false;
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;
		boolean result4 = false;
		boolean result5 = false;
		boolean result6 = false;

		Log.message("Enter into main status ");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, OpenStatus, 60);
		String Open = OpenStatus.getText();
		Log.message("Open Button Display: " + Open);

		if (Open.equalsIgnoreCase("OPEN")) {
			result1 = true;
			Log.message("Status Open Button verified Sucessfully ");

		} else {
			result1 = false;
			Log.message("Status Open not verified Sucessfully");
		}

		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, OpenStatus, 60);
		OpenStatus.click();

		SkySiteUtils.waitForElement(driver, OpenStatus, 60);
		String Open1 = OpenStatus.getText();
		Log.message("Open Button Display: " + Open1);

		if (Open1.equalsIgnoreCase("OPEN")) {
			result2 = true;
			Log.message("Status Open POP UP verified Sucessfully ");

		} else {
			result2 = false;
			Log.message("Status Open POP UP not verified Sucessfully");
		}

		SkySiteUtils.waitForElement(driver, CloseRFIButton, 120);
		SkySiteUtils.waitTill(5000);
		CloseRFIButton.click();
		SkySiteUtils.waitForElement(driver, YesButton, 60);
		YesButton.click();

		SkySiteUtils.waitForElement(driver, CloseStatus, 60);
		String close = CloseStatus.getText();
		Log.message("Close Button Display: " + close);

		if (close.equalsIgnoreCase("CLOSED")) {
			result3 = true;
			Log.message("Status Close Button verified Sucessfully ");

		} else {
			result3 = false;
			Log.message("Status Close not verified Sucessfully");
		}

		SkySiteUtils.waitForElement(driver, CloseStatus, 60);
		SkySiteUtils.waitTill(5000);
		CloseStatus.click();

		SkySiteUtils.waitForElement(driver, Close_POPUP, 60);
		String close2 = Close_POPUP.getText();
		Log.message("Close Button Display: " + close2);

		if (close2.equalsIgnoreCase("CLOSED")) {
			result4 = true;
			Log.message("Status Close POP UP verified Sucessfully ");

		} else {
			result4 = false;
			Log.message("Status Close POP UP not verified Sucessfully");
		}
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, ReopenRFI, 60);
		ReopenRFI.click();
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, YesButton, 60);
		YesButton.click();

		SkySiteUtils.waitTill(5000);
		String Reopen = OpenStatus.getText();
		Log.message("ReOpen Button Display: " + Reopen);

		if (Reopen.equalsIgnoreCase("RE-OPENED")) {
			result5 = true;
			Log.message("Status Reopen  verified Sucessfully ");

		} else {
			result5 = false;
			Log.message("Status Reopen not verified Sucessfully");
		}
		SkySiteUtils.waitForElement(driver, OpenStatus, 60);
		OpenStatus.click();
		SkySiteUtils.waitTill(8000);
		SkySiteUtils.waitForElement(driver, OpenStatus_popUp, 60);
		String Reopen1 = OpenStatus_popUp.getText();
		Log.message("Open Button Display: " + Reopen1);

		if (Reopen1.equalsIgnoreCase("RE-OPENED")) {
			result6 = true;
			Log.message("Status RE-OPENED POP UP verified Sucessfully ");

		} else {
			result6 = false;
			Log.message("Status RE-OPENED  POP UP not verified Sucessfully");
		}

		SkySiteUtils.waitForElement(driver, CloseRFIButton, 60);
		SkySiteUtils.waitTill(5000);
		CloseRFIButton.click();
		Log.message("Close Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, YesButton, 60);
		YesButton.click();
		Log.message("Yes Button Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		if ((result1 == true) && (result2 == true) && (result3 == true) && (result4 == true) && (result5 == true)
				&& (result6 == true))
			return true;
		else
			return false;

	}

	public boolean MainStatus1() throws Throwable

	{
		boolean result = true;
		Log.message("Enter into main status ");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, OpenStatus, 60);
		String Open = OpenStatus.getText();

		Log.message("Open Button Display: " + Open);

		if (Open.equalsIgnoreCase("OPEN")) {
			Log.message("Status Open Button verified Sucessfully ");

		} else {
			Log.message("Status Open not verified Sucessfully");
		}

		SkySiteUtils.waitForElement(driver, CloseRFIButton, 60);
		SkySiteUtils.waitTill(5000);
		CloseRFIButton.click();
		SkySiteUtils.waitForElement(driver, YesButton, 60);
		YesButton.click();

		SkySiteUtils.waitForElement(driver, CloseStatus, 60);
		String close = CloseStatus.getText();
		Log.message("Close Button Display: " + close);

		if (close.equalsIgnoreCase("CLOSED")) {
			Log.message("Status Close Button verified Sucessfully ");

		} else {
			Log.message("Status Close not verified Sucessfully");
		}

		SkySiteUtils.waitForElement(driver, CloseStatus, 60);
		SkySiteUtils.waitTill(5000);
		CloseStatus.click();

		SkySiteUtils.waitForElement(driver, Close_POPUP, 60);
		String close2 = Close_POPUP.getText();
		Log.message("Close Button Display: " + close2);

		if (close2.equalsIgnoreCase("CLOSED")) {
			Log.message("Status Close POP UP verified Sucessfully ");

		} else {
			Log.message("Status Close POP UP not verified Sucessfully");
		}
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, ReopenRFI, 60);
		ReopenRFI.click();
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, YesButton, 60);
		YesButton.click();

		SkySiteUtils.waitTill(5000);
		String Reopen = OpenStatus.getText();
		Log.message("ReOpen Button Display: " + Reopen);

		if (Reopen.equalsIgnoreCase("RE-OPENED")) {
			Log.message("Status Reopen  verified Sucessfully ");

		} else {
			Log.message("Status Reopen not verified Sucessfully");
		}
		SkySiteUtils.waitForElement(driver, OpenStatus, 60);
		OpenStatus.click();
		SkySiteUtils.waitTill(8000);
		SkySiteUtils.waitForElement(driver, OpenStatus_popUp, 60);
		String Reopen1 = OpenStatus_popUp.getText();
		Log.message("Open Button Display: " + Reopen1);

		if (Reopen1.equalsIgnoreCase("RE-OPENED")) {
			Log.message("Status RE-OPENED POP UP verified Sucessfully ");

		} else {
			Log.message("Status RE-OPENED  POP UP not verified Sucessfully");
		}

		SkySiteUtils.waitForElement(driver, closebutton, 60);
		closebutton.click();
		SkySiteUtils.waitTill(5000);
		return result;

	}

	public boolean EmployeeStatus() throws Throwable

	{
		boolean result = true;
		Log.message("Enter into main status ");
		SkySiteUtils.waitTill(5000);
		OpenStatus.click();
		Log.message("Status  verified Sucessfully ");
		SkySiteUtils.waitForElement(driver, textArea, 60);
		textArea.sendKeys("OK");
		Log.message("Entered OK Value In text Area ");
		SkySiteUtils.waitForElement(driver, REplay, 60);
		REplay.click();

		SkySiteUtils.waitForElement(driver, EmployeeStatus, 60);
		String Responded = EmployeeStatus.getText();

		Log.message("RESPONDED Button Display: " + Responded);

		if (Responded.equalsIgnoreCase("RESPONDED")) {
			Log.message("Status Responded Button verified Sucessfully ");

		} else {
			Log.message("Status RESPONDED not verified Sucessfully");
		}

		SkySiteUtils.waitForElement(driver, OpenStatus, 60);
		SkySiteUtils.waitTill(5000);
		OpenStatus.click();

		SkySiteUtils.waitForElement(driver, EmployeeStatus, 60);
		String Responded2 = EmployeeStatus.getText();

		Log.message("RESPONDED Button Display: " + Responded);

		if (Responded2.equalsIgnoreCase("RESPONDED")) {
			Log.message("Status Responded POP UP verified Sucessfully ");

		} else {
			Log.message("Status RESPONDED POP UP not verified Sucessfully");
		}

		return result;

	}

	public boolean RFINumberWithLocalSearch(String rFINumber) throws Throwable

	{
		boolean result = true;
		SkySiteUtils.waitTill(5000);
		Log.message("Enter Into Local search");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		String FirstNumber = DisplayRFINO.getText();
		Search_GlobalButton.click();
		Log.message("Global Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Everywhere_AllContents, 120);
		search.click();
		Log.message("Everywhere AllContents Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		SkySiteUtils.waitTill(5000);
		TxtSearch_Keyword.sendKeys(rFINumber);
		Log.message("Enter The Value In search Inbox Field: - " + FirstNumber);
		SkySiteUtils.waitTill(50000);
		// SkySiteUtils.waitForElement(driver, Search_Button, 60);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		return result;

	}

	public boolean AdvanceStatus(String StatusDispaly) throws Throwable {
		boolean result = true;
		Log.message("Enter Into  filter By search status");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 60);
		AdvanceSearch_Link.click();
		Log.message("Advance Search Option Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, Status_Inbox, 60);
		SkySiteUtils.waitTill(3000);
		Status_Inbox.sendKeys(StatusDispaly);
		Log.message("Entered The Value In search Inbox Text Field: - " + StatusDispaly);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 60);
		SkySiteUtils.waitTill(50000);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(3000);
		return result;

	}

	public boolean LocalSearchwithAnything(String Anything) throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		Search_GlobalButton.click();
		Log.message("Local Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Everywhere_AllContents, 120);
		search.click();
		Log.message("Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		SkySiteUtils.waitTill(5000);
		TxtSearch_Keyword.sendKeys(Anything);
		// SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(60000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		if (result == true)
			return true;
		else
			return false;

	}

	public boolean SearchRFI_RFINumber_olddata() throws Throwable

	{

		boolean result1 = false;
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		Search_GlobalButton.click();
		Log.message("Global Search Option Click Sucessfully");
		//SkySiteUtils.waitForElement(driver, Everywhere_AllContents, 120);
		//Everywhere_AllContents.click();
		Log.message("Everywhere AllContents Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		String RFI_Number1 = driver.findElement(By.xpath("(//span[@ data-bind='text: RFINumber()'])[1]")).getText();
		SkySiteUtils.waitTill(1000);
		TxtSearch_Keyword.sendKeys(RFI_Number1);
		Log.message("Enter The Value In search Inbox Field: - " + RFI_Number1);
		// SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(30000);
		Search_Button.click();
		Log.message("Search button has been clicked");
		SkySiteUtils.waitTill(6000);
		String RFI_Number2 = driver.findElement(By.xpath("//span [@data-bind='text: RFINumber()']")).getText();

		if (RFI_Number1.trim().equalsIgnoreCase((RFI_Number2.trim()))) {
			Log.message("RFI Number Verified Sucessfully");
			return result1 = true;
		} else {
			Log.message("RFI Number Not Verified Sucessfully");
			return result1 = false;

		}

	}

	public boolean SearchRFI_RFINumber(String rFINumber) throws Throwable

	{

		boolean result1 = false;
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		Search_GlobalButton.click();
		Log.message("Global Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Everywhere_AllContents, 120);
		Everywhere_AllContents.click();
		Log.message("Everywhere AllContents Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		SkySiteUtils.waitTill(1000);
		TxtSearch_Keyword.sendKeys(rFINumber);
		Log.message("Enter The Value In search Inbox Field: - " + rFINumber);
		// SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(50000);
		Search_Button.click();
		Log.message("Search button has been clicked");
		SkySiteUtils.waitTill(5000);
		int RFI_Count = driver.findElements(By.xpath(".//*[@id='spnRFIStatusO']")).size();
		Log.message("RFI count is :" + RFI_Count);
		int i = 0;
		for (i = 1; i <= RFI_Count; i++) {
			String RFI_Number = driver.findElement(By.xpath("(//span[@class='rfiData-load']/span)[" + i + "]"))
					.getText().toString();
			Log.message("RFI Number is:" + RFI_Number);
			SkySiteUtils.waitTill(5000);
			if (RFI_Number.contains(rFINumber)) {
				result1 = true;
				Log.message("Rfi number validation sucessfully");
				break;
			}
		}
		if (result1 == true) {
			Log.message("RFI Number Verified Sucessfully");
			return true;
		} else {
			Log.message("RFI Number Not Verified Sucessfully");
			return false;
		}

		// return result;

	}

	public boolean SearchRFISubject_Module(String Sub_Name) throws Throwable

	{
		boolean result1 = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		String FirstNumber = DisplayRFINO.getText();
		Search_GlobalButton.click();
		Log.message("Global Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, RFI_search, 120);
		RFI_search.click();
		Log.message("Everywhere (all projects & content Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		SkySiteUtils.waitTill(2000);
		TxtSearch_Keyword.sendKeys(Sub_Name);
		Log.message("Enter The Value In search Inbox Field: - " + Sub_Name);
		// SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(50000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		return result1;
	}

	public boolean SearchRFISubject_Global(String Sub_Name) throws Throwable

	{
		boolean result1 = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		String FirstNumber = DisplayRFINO.getText();
		Search_GlobalButton.click();
		Log.message("Global Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Everywhere_AllContents, 120);
		Everywhere_AllContents.click();
		Log.message("Everywhere (all projects & content Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		SkySiteUtils.waitTill(2000);
		TxtSearch_Keyword.sendKeys(Sub_Name);
		Log.message("Enter The Value In search Inbox Field: - " + Sub_Name);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(30000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");

		SkySiteUtils.waitTill(20000);
		int RFI_Count = driver.findElements(By.xpath(".//*[@id='spnRFIStatusO']")).size();
		Log.message("RFI count is :" + RFI_Count);
		int i = 0;
		for (i = 1; i <= RFI_Count; i++) {
			String RFI_subject = driver.findElement(By.xpath("(//div[@class='prj-dtl']/h4)[" + i + "]")).getText()
					.toString();
			Log.message("RFI Subject Name is:" + Sub_Name);
			SkySiteUtils.waitTill(5000);
			if (RFI_subject.contains(Sub_Name)) {
				result1 = true;
				Log.message("RFI Subject validated sucessfully");
				break;
			}
		}
		if (result1 == true) {
			Log.message("RFI Subject Verified Sucessfully");
			return true;
		} else {
			Log.message("RFI Subject Verified Sucessfully");
			return false;
		}

	}

	public boolean SearchRFISubject_Global_Olddata() throws Throwable

	{
		boolean result1 = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		String FirstNumber = DisplayRFINO.getText();
		Search_GlobalButton.click();
		Log.message("Global Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Everywhere_AllContents, 120);
		Everywhere_AllContents.click();
		Log.message("Everywhere (all projects & content Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		String RFI_subject1 = driver.findElement(By.xpath("//*[@id='ulRFIList']/li[1]/div[2]/div[1]/div/h4/span")).getText();
		SkySiteUtils.waitTill(2000);
		TxtSearch_Keyword.sendKeys(RFI_subject1);
		Log.message("Enter The Value In search Inbox Field: - " + RFI_subject1);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(30000);
		Search_Button.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Search Button Clicked Sucessfully");
		String RFI_subject2 = driver.findElement(By.xpath("(//div[@ class='prj-dtl']/h4)[1]")).getText();
		SkySiteUtils.waitTill(20000);

		if (RFI_subject1.equalsIgnoreCase(RFI_subject2)) {
			Log.message("RFI Subject validated sucessfully");
			return result1 = true;

		}

		else {
			Log.message("RFI Subject Verified Sucessfully");
			return result1 = false;

		}

	}

	public boolean SearchRFI_Global(String customattribute) throws Throwable

	{
		boolean result1 = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		Search_GlobalButton.click();
		Log.message("Global Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Everywhere_AllContents, 120);
		Everywhere_AllContents.click();
		Log.message("Everywhere (all projects & content Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		SkySiteUtils.waitTill(2000);
		TxtSearch_Keyword.sendKeys(customattribute);
		Log.message("Enter The Value In search Inbox Field: - " + customattribute);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(30000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");

		SkySiteUtils.waitTill(5000);
		int RFI_Count = driver.findElements(By.xpath(".//*[@id='spnRFIStatusO']")).size();
		Log.message("RFI count is :---" + RFI_Count);
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath(".//*[@id='spnRFIStatusO']")).click();
		Log.message("List RFI Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		RFI_ModelInfo.click();
		Log.message("RFI Info Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		String newobj =CustomAttribute_text.getText();
		/*int i = 0;
		for (i = 1; i <= RFI_Count; i++) {
			String RFI_subject = driver.findElement(By.xpath("(//div[@class='prj-dtl']/h4)[" + i + "]")).getText()
					.toString();
			Log.message("RFI Attribute Name is:" + customattribute);
			SkySiteUtils.waitTill(5000);*/
		
		
			/*if (newobj.contains(customattribute)) {
				result1 = true;
				Log.message("RFI Custom Attribute validated sucessfully");
				break;
			}*/
		
		if (newobj.contains(customattribute)) {
			Log.message("RFI Custom Attribute Verified Sucessfully");
			return true;
		} else {
			Log.message("RFI Custom Attribute not Verified Sucessfully");
			return false;
		}

	}

	public boolean RFICreation_withSubject(String subject) throws Throwable

	{
		boolean result = true;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, CreateRFILink, 60);
		CreateRFILink.click();
		Log.message("Create RFI Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, TOMail, 60);
		SkySiteUtils.waitTill(5000);
		TOMail.click();
		Log.message("To Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUserInbox, 60);
		String ToEmail = PropertyReader.getProperty("SelectUserAssignTo_Email");
		SkySiteUtils.waitTill(5000);
		SelectUserInbox.sendKeys(ToEmail);
		Log.message("Enter value In Select User InBox:" + ToEmail);
		SkySiteUtils.waitForElement(driver, SearchButton, 60);
		SkySiteUtils.waitTill(50000);
		SearchButton.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, AssignTo_Checkbox, 120);
		AssignTo_Checkbox.click();
		Log.message("Checkbox Selected for particular Mail Id");
		SkySiteUtils.waitForElement(driver, SelectuserButton, 100);
		SkySiteUtils.waitTill(5000);
		SelectuserButton.click();
		Log.message("Select User Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, ccMail, 120);
		ccMail.click();
		Log.message("Cc Link Clicked Sucessfully");
		String CCMail = PropertyReader.getProperty("SelectUserCCOption_Email");
		Log.message("CC Display:" + CCMail);
		SkySiteUtils.waitTill(4000);
		SelectUserInbox.sendKeys(CCMail);
		Log.message("Enter value In Select User InBox:" + CCMail);
		SkySiteUtils.waitForElement(driver, SearchButton, 60);
		SearchButton.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, AssignTo_Checkbox, 120);
		AssignTo_Checkbox.click();
		Log.message("Checkbox Selected for particular Mail Id");
		SkySiteUtils.waitForElement(driver, SelectuserButton, 60);
		SkySiteUtils.waitTill(2000);
		SelectuserButton.click();
		Log.message("Select User Button Clicked Sucessfully");
		// ============Subject Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Subject, 60);
		// String subject1 = PropertyReader.getProperty("RFI_Subject");
		Subject.sendKeys(subject);
		Log.message("Subject Entered Into Inbox");
		// ============Question Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Question, 60);
		String question = PropertyReader.getProperty("RFI_Questions");
		Question.sendKeys(question);
		Log.message("Question Entered Into Inbox");

		/*
		 * if(Addattachment.isDisplayed()){ Addattachment.click();
		 * Log.message("Attachment button Click Sucessfully"); } //===External
		 * And Internal File Upload String FolderPath =
		 * PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		 * Log.message("FolderPath: "+FolderPath); String CountOfFilesInFolder =
		 * PropertyReader.getProperty("FileCount_viewer"); int FileCount =
		 * Integer.parseInt(CountOfFilesInFolder);//String to Integer
		 * Log.message("FileCount: "+FileCount); SkySiteUtils.waitTill(1000);
		 * UploadFiles(FolderPath, FileCount);
		 */
		SkySiteUtils.waitForElement(driver, btnCreateRFI, 120);
		btnCreateRFI.click();
		Log.message("Button RFI Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, RFIFirstShortcut, 360);
		if (RFIFirstShortcut.isDisplayed())
			return true;
		else
			return false;

	}

	public boolean RFI_CreationwithcustomAttribute(String CustomAttribute) throws Throwable

	{
		boolean result = true;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, CreateRFILink, 60);
		CreateRFILink.click();
		Log.message("Create RFI Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, TOMail, 30);
		TOMail.click();
		Log.message("To Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUserInbox, 60);
		String ToEmail = PropertyReader.getProperty("SelectUserAssignTo_Email");
		SkySiteUtils.waitTill(2000);
		SelectUserInbox.sendKeys(ToEmail);
		Log.message("Enter value In Select User InBox:--" + ToEmail);
		// SkySiteUtils.waitForElement(driver, SearchButton,60);
		SkySiteUtils.waitTill(50000);
		SearchButton.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AssignTo_Checkbox, 60);
		AssignTo_Checkbox.click();
		Log.message("Checkbox Selected for particular Mail Id");
		SkySiteUtils.waitForElement(driver, SelectuserButton, 60);
		SelectuserButton.click();
		Log.message("Select User Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, ccMail, 60);
		ccMail.click();
		Log.message("Cc Link Clicked Sucessfully");
		String CCMail = PropertyReader.getProperty("SelectUserCCOption_Email");
		Log.message("CC Display:" + CCMail);
		SkySiteUtils.waitForElement(driver, SelectUserInbox, 60);
		SkySiteUtils.waitTill(2000);
		SelectUserInbox.sendKeys(CCMail);
		Log.message("Enter value In Select User InBox:" + CCMail);
		SkySiteUtils.waitForElement(driver, SearchButton, 60);
		SearchButton.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AssignTo_Checkbox, 60);
		AssignTo_Checkbox.click();
		SkySiteUtils.waitTill(1000);
		Log.message("Checkbox Selected for particular Mail Id");
		SkySiteUtils.waitForElement(driver, SelectuserButton, 60);
		SelectuserButton.click();
		Log.message("Select User Button Clicked Sucessfully");
		// ============Subject Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Subject, 60);
		String subject1 = PropertyReader.getProperty("RFI_Subject");
		Subject.sendKeys(subject1);
		SkySiteUtils.waitTill(1000);
		Log.message("Subject Entered Into Inbox");

		// ========================================================
		SkySiteUtils.waitForElement(driver, Subject, 60);
		if (NewcustomAttributeField.isDisplayed()) {
			NewcustomAttributeField.sendKeys(CustomAttribute);
			Log.message("New Custom Attribute Field");
		}
		// ============Question Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Question, 60);
		String question = PropertyReader.getProperty("RFI_Questions");
		Question.sendKeys(question);
		SkySiteUtils.waitTill(2000);
		Log.message("Question Entered Into Inbox");

		if (Addattachment.isDisplayed()) {
			Addattachment.click();
			Log.message("Attachment button Click Sucessfully");
		}
		// ===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);// String to
																// Integer
		Log.message("FileCount: " + FileCount);
		SkySiteUtils.waitTill(2000);
		UploadFiles(FolderPath, FileCount);
		// SkySiteUtils.waitTill(5000);
		// project folder Tab
		/*
		 * SkySiteUtils.waitForElement(driver, Projectfiles, 60);
		 * Log.message("Waiting for Project Tab button to be appeared");
		 * Projectfiles.click();
		 * Log.message("Project files Tab Clicked Sucessfully");
		 * SkySiteUtils.waitTill(5000); //entered the value in searchbox String
		 * Folder1 = PropertyReader.getProperty("FolderName");
		 * SearchProjectfilesInbox.sendKeys(Folder1);
		 * SkySiteUtils.waitTill(500);
		 * Log.message("Entered the value in project files Inbox field");
		 * SkySiteUtils.waitForElement(driver, ProjectfilesTab_searchButton,
		 * 30); ProjectfilesTab_searchButton.click();
		 * SkySiteUtils.waitTill(500);
		 * Log.message("Search Button Click Sucessfully");
		 * SkySiteUtils.waitForElement(driver, Folder, 60); Folder.click();
		 * Log.message("Folder Selected And clicked");
		 * SkySiteUtils.waitForElement(driver, File_CheckBox, 60);
		 * File_CheckBox.click();
		 * Log.message("File CheckBox Clicked Sucessfully");
		 */
		SkySiteUtils.waitForElement(driver, btnCreateRFI, 60);
		btnCreateRFI.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Button RFI Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, RFIFirstShortcut, 120);
		if (RFIFirstShortcut.isDisplayed())
			return true;
		else
			return false;

	}
	
	
	public boolean RFI_CreationwithEmail(String ToEmail, String CCMail) throws Throwable

	{
		boolean result = false ;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, CreateRFILink, 60);
		CreateRFILink.click();
		Log.message("Create RFI Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, TOMail, 30);
		TOMail.click();
		Log.message("To Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUserInbox, 60);
		SkySiteUtils.waitTill(2000);
		SelectUserInbox.sendKeys(ToEmail);
		Log.message("Enter value In Select User InBox:--" + ToEmail);
		// SkySiteUtils.waitForElement(driver, SearchButton,60);
		SkySiteUtils.waitTill(50000);
		SearchButton.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AssignTo_Checkbox, 60);
		AssignTo_Checkbox.click();
		Log.message("Checkbox Selected for particular Mail Id");
		SkySiteUtils.waitForElement(driver, SelectuserButton, 60);
		SelectuserButton.click();
		Log.message("Select User Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, ccMail, 60);
		ccMail.click();
		Log.message("Cc Link Clicked Sucessfully");
		// String CCMail =PropertyReader.getProperty("SelectUserCCOption_Email");
		Log.message("CC Display:" + CCMail);
		SkySiteUtils.waitForElement(driver, SelectUserInbox, 60);
		SkySiteUtils.waitTill(2000);
		SelectUserInbox.sendKeys(CCMail);
		Log.message("Enter value In Select User InBox:" + CCMail);
		SkySiteUtils.waitForElement(driver, SearchButton, 60);
		SearchButton.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AssignTo_Checkbox, 60);
		AssignTo_Checkbox.click();
		SkySiteUtils.waitTill(1000);
		Log.message("Checkbox Selected for particular Mail Id");
		SkySiteUtils.waitForElement(driver, SelectuserButton, 60);
		SelectuserButton.click();
		Log.message("Select User Button Clicked Sucessfully");
		// ============Subject Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Subject, 60);
		String subject1 = PropertyReader.getProperty("RFI_Subject");
		Subject.sendKeys(subject1);
		SkySiteUtils.waitTill(1000);
		Log.message("Subject Entered Into Inbox");

		// ============Question Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Question, 60);
		String question = PropertyReader.getProperty("RFI_Questions");
		Question.sendKeys(question);
		SkySiteUtils.waitTill(2000);
		Log.message("Question Entered Into Inbox");

		if (Addattachment.isDisplayed()) {
			Addattachment.click();
			Log.message("Attachment button Click Sucessfully");
		}
		// ===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);// String to
																// Integer
		Log.message("FileCount: " + FileCount);
		SkySiteUtils.waitTill(2000);
		UploadFiles(FolderPath, FileCount);
		 SkySiteUtils.waitTill(5000);
		// project folder Tab

		/*SkySiteUtils.waitForElement(driver, Projectfiles, 60);
		Log.message("Waiting for Project Tab button to be appeared");
		Log.message("Project files Tab Clicked Sucessfully");
		SkySiteUtils.waitTill(5000); // entered the value in searchbox String
		String Folder1 = PropertyReader.getProperty("FolderName");
		SearchProjectfilesInbox.sendKeys(Folder1);
		SkySiteUtils.waitTill(500);
		Log.message("Entered the value in project files Inbox field");
		SkySiteUtils.waitForElement(driver, ProjectfilesTab_searchButton, 30);
		ProjectfilesTab_searchButton.click();
		SkySiteUtils.waitTill(500);
		Log.message("Search Button Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Folder, 60);
		Folder.click();
		Log.message("Folder Selected And clicked");
		SkySiteUtils.waitForElement(driver, File_CheckBox, 60);
		File_CheckBox.click();
		Log.message("File CheckBox Clicked Sucessfully");*/

		SkySiteUtils.waitForElement(driver, btnCreateRFI, 60);
		btnCreateRFI.click();
		SkySiteUtils.waitTill(20000);
		Log.message("Button RFI Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, RFIFirstShortcut, 120);
		if (RFIFirstShortcut.isDisplayed())
			return true;
		else
			return false;

	}
	
	public boolean RFI_CreationwithEmail(String ToEmail) throws Throwable

	{
		boolean result = false ;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, CreateRFILink, 60);
		CreateRFILink.click();
		Log.message("Create RFI Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, TOMail, 30);
		TOMail.click();
		Log.message("To Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUserInbox, 60);
		SkySiteUtils.waitTill(2000);
		SelectUserInbox.sendKeys(ToEmail);
		Log.message("Enter value In Select User InBox:--" + ToEmail);
		// SkySiteUtils.waitForElement(driver, SearchButton,60);
		SkySiteUtils.waitTill(50000);
		SearchButton.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AssignTo_Checkbox, 60);
		AssignTo_Checkbox.click();
		Log.message("Checkbox Selected for particular Mail Id");
		SkySiteUtils.waitForElement(driver, SelectuserButton, 60);
		SelectuserButton.click();
		Log.message("Select User Button Clicked Sucessfully");
		
		// ============Subject Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Subject, 60);
		String subject1 = PropertyReader.getProperty("RFI_Subject");
		Subject.sendKeys(subject1);
		SkySiteUtils.waitTill(1000);
		Log.message("Subject Entered Into Inbox");

		// ============Question Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Question, 60);
		String question = PropertyReader.getProperty("RFI_Questions");
		Question.sendKeys(question);
		SkySiteUtils.waitTill(2000);
		Log.message("Question Entered Into Inbox");

		if (Addattachment.isDisplayed()) {
			Addattachment.click();
			Log.message("Attachment button Click Sucessfully");
		}
		// ===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);// String to
																// Integer
		Log.message("FileCount: " + FileCount);
		SkySiteUtils.waitTill(2000);
		UploadFiles(FolderPath, FileCount);
		 SkySiteUtils.waitTill(5000);
		

		SkySiteUtils.waitForElement(driver, btnCreateRFI, 60);
		btnCreateRFI.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Button RFI Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, RFIFirstShortcut, 120);
		if (RFIFirstShortcut.isDisplayed())
			return true;
		else
			return false;

	}
	
	
	
	
	public boolean RFI_AllTabValidation() throws Throwable

	{
		boolean result1 = false ;
		boolean result2 = false;
		boolean result3 = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Myrfi, 60);
		Myrfi.click();
		SkySiteUtils.waitTill(5000);
		Log.message("My RFI Tab Clicked Sucessfully");
		String RFI_Subject = RFI_subject.getText();
		String subject1 = PropertyReader.getProperty("RFI_Subject");
		if (RFI_Subject.contains(subject1))
		{
			Log.message("RFI Verified Sucessfully");
			 result1= true;
		}
		else
		{			
			Log.message("RFI Not Verified Sucessfully");
			result1 = false;
		}	
		SkySiteUtils.waitForElement(driver, RFI_Assignto_me,60);
		RFI_Assignto_me.click();
		SkySiteUtils.waitTill(5000);
		Log.message("My RFI Tab Clicked Sucessfully");
		String RFI_Subject1 = RFI_subject.getText();
		String subject2 = PropertyReader.getProperty("RFI_Subject");
		if (RFI_Subject1.contains(subject2))
		{
			Log.message("RFI Verified Sucessfully");
			 result2= true;
		}
		else
		{			
			Log.message("RFI Not Verified Sucessfully");
			result2 = false;
		}	
		
		SkySiteUtils.waitForElement(driver, AllRFI,60);
		AllRFI.click();
		SkySiteUtils.waitTill(5000);
		Log.message("My RFI Tab Clicked Sucessfully");
		String RFI_Subject2 = RFI_subject.getText();
		String subject3 = PropertyReader.getProperty("RFI_Subject");
		if (RFI_Subject2.contains(subject3))
		{
			Log.message("RFI Verified Sucessfully");
			 result3= true;
		}
		else
		{			
			Log.message("RFI Not Verified Sucessfully");
			result3 = false;
		}	
		if((result1==true)&&(result2==true)&&(result3==true))
			return true;
		else
			return false;
	}
	
	
	
	public boolean RFI_CreationwithEmail_Draft(String ToEmail) throws Throwable

	{
		boolean result = false ;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, CreateRFILink, 60);
		CreateRFILink.click();
		Log.message("Create RFI Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, TOMail, 30);
		TOMail.click();
		Log.message("To Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUserInbox, 60);
		SkySiteUtils.waitTill(2000);
		SelectUserInbox.sendKeys(ToEmail);
		Log.message("Enter value In Select User InBox:--" + ToEmail);
		// SkySiteUtils.waitForElement(driver, SearchButton,60);
		SkySiteUtils.waitTill(50000);
		SearchButton.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AssignTo_Checkbox, 60);
		AssignTo_Checkbox.click();
		Log.message("Checkbox Selected for particular Mail Id");
		SkySiteUtils.waitForElement(driver, SelectuserButton, 60);
		SelectuserButton.click();
		Log.message("Select User Button Clicked Sucessfully");		
		// ============Subject Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Subject, 60);
		String subject1 = PropertyReader.getProperty("RFI_Subject");
		Subject.sendKeys(subject1);
		SkySiteUtils.waitTill(1000);
		Log.message("Subject Entered Into Inbox");

		// ============Question Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Question, 60);
		String question = PropertyReader.getProperty("RFI_Questions");
		Question.sendKeys(question);
		SkySiteUtils.waitTill(2000);
		Log.message("Question Entered Into Inbox");

		if (Addattachment.isDisplayed()) {
			Addattachment.click();
			Log.message("Attachment button Click Sucessfully");
		}
		// ===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);// String to
																// Integer
		Log.message("FileCount: " + FileCount);
		SkySiteUtils.waitTill(2000);
		UploadFiles(FolderPath, FileCount);
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, saveAsDraft, 60);
		saveAsDraft.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Button RFI Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, RFIFirstShortcut, 120);
		if (RFIFirstShortcut.isDisplayed())
			return true;
		else
			return false;

	}



	public boolean Statusvalidation() throws Throwable

	{
		boolean result = false;

		String ToEmail = PropertyReader.getProperty("SelectUserAssignTo_Email");
		/*
		 * if (subject.equalsIgnoreCase(RFI_Subject)) {
		 * Log.message("Subject verified Sucessfully ");
		 * 
		 * } else { Log.message("Subject not verified Sucessfully"); }
		 */
		return result;

	}

	public boolean RFICREATION_New() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, CreateRFILink, 120);
		CreateRFILink.click();
		Log.message("Create RFI Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, TOMail, 120);
		TOMail.click();
		SkySiteUtils.waitTill(2000);
		Log.message("To Link Clicked Sucessfully");
		String ToEmail = PropertyReader.getProperty("SelectUserAssignTo_Email");
		SkySiteUtils.waitForElement(driver, SelectUserInbox, 120);
		SkySiteUtils.waitTill(5000);
		SelectUserInbox.sendKeys(ToEmail);
		Log.message("Enter value In Select User InBox:" + ToEmail);
		SkySiteUtils.waitForElement(driver, SearchButton, 120);
		SkySiteUtils.waitTill(50000);
		SearchButton.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AssignTo_Checkbox, 60);
		SkySiteUtils.waitTill(5000);
		AssignTo_Checkbox.click();
		Log.message("Checkbox Selected for particular Mail Id");
		SkySiteUtils.waitForElement(driver, SelectuserButton, 120);
		SelectuserButton.click();
		Log.message("Select User Button Clicked Sucessfully");
		/*
		 * SkySiteUtils.waitForElement(driver, ccMail, 120); ccMail.click();
		 * Log.message("Cc Link Clicked Sucessfully"); String CCMail =
		 * PropertyReader.getProperty("SelectUserCCOption_Email");
		 * SkySiteUtils.waitForElement(driver, SelectUserInbox,120);
		 * SkySiteUtils.waitTill(5000); SelectUserInbox.sendKeys(CCMail);
		 * Log.message("CC Display:"+CCMail);
		 * Log.message("Enter value In Select User InBox:"+ CCMail);
		 * SkySiteUtils.waitForElement(driver, SearchButton,120);
		 * SearchButton.click();
		 * Log.message("Search Button Clicked Sucessfully");
		 * SkySiteUtils.waitForElementLoadTime(driver, AssignTo_Checkbox, 60);
		 * SkySiteUtils.waitTill(5000); AssignTo_Checkbox.click();
		 * Log.message("Checkbox Selected for particular Mail Id");
		 * SkySiteUtils.waitForElement(driver, SelectuserButton,120);
		 * SelectuserButton.click();
		 * Log.message("Select User Button Clicked Sucessfully");
		 */
		// ============Subject Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Subject, 60);
		String subject1 = PropertyReader.getProperty("RFI_Subject");
		Subject.sendKeys(subject1);
		Log.message("Subject Entered Into Inbox");
		// ============Question Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Question, 60);
		String question = PropertyReader.getProperty("RFI_Questions");
		Question.sendKeys(question);
		Log.message("Question Entered Into Inbox");

		if (Addattachment.isDisplayed()) {
			Addattachment.click();
			Log.message("Attachment button Click Sucessfully");
		}

		/*
		 * //===External And Internal File Upload String FolderPath =
		 * PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		 * //Log.message("FolderPath: "+FolderPath); String CountOfFilesInFolder
		 * = PropertyReader.getProperty("FileCount_viewer"); int FileCount =
		 * Integer.parseInt(CountOfFilesInFolder);//String to Integer
		 * Log.message("FileCount: "+FileCount); //UploadFiles(FolderPath,
		 * FileCount); SkySiteUtils.waitTill(1000);
		 */
		SkySiteUtils.waitForElement(driver, btnCreateRFI, 60);
		btnCreateRFI.click();
		Log.message("Button RFI Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, RFIFirstShortcut, 120);
		if (RFIFirstShortcut.isDisplayed())
			return true;
		else
			return false;

	}

	/**
	 * Method written for creating new project. Scripted By:Ranjan
	 * 
	 * @return
	 */
	public boolean RFIForwadedUser_Validation(String Email1, String Email2, String Email3)

	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, FirstRow_Click, 120);
		FirstRow_Click.click();
		Log.message(" RFI First row Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, Forward_Button, 120);
		SkySiteUtils.waitTill(5000);
		Forward_Button.click();
		SkySiteUtils.waitForElement(driver, Forward_Contactbutton, 120);
		Log.message("Forward Button Clicked Sucessfully");
		Forward_Contactbutton.click();
		Log.message("Forward Contact Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);

		int all_checkbox_count = 0;
		java.util.List<WebElement> allElements = driver
				.findElements(By.xpath("//*[@id='divUserListView']/ul/li/section[1]/input"));
		for (WebElement Element : allElements) {
			all_checkbox_count = all_checkbox_count + 1;
		}
		Log.message("Available checkbox  count is: " + all_checkbox_count);
		// SkySiteUtils.waitTill(5000);

		for (int i = 1; i <= all_checkbox_count; i++) {
			String Exp_checkbox = driver
					.findElement(By.xpath("(//*[@id='divUserListView']/ul/li/section[2]/a)[" + i + "]")).getText();
			Log.message(Exp_checkbox);
			// Validating - Expected project is selected or not
			if (Exp_checkbox.trim().contentEquals(Email1)) {
				Log.message("Maching Checkbox Found!!:---" + Exp_checkbox);
				driver.findElement(By.xpath("(//*[@id='divUserListView']/ul/li/section[1]/input)[" + i + "]")).click();
				Log.message("Clicked on expected checkbox!!");
				SkySiteUtils.waitTill(5000);
				break;
			}
		}

		for (int j = 1; j <= all_checkbox_count; j++) {
			String Exp_checkbox2 = driver
					.findElement(By.xpath("(//*[@id='divUserListView']/ul/li/section[2]/a)[" + j + "]")).getText();
			Log.message(Exp_checkbox2);
			// Validating - Expected project is selected or not
			if (Exp_checkbox2.trim().contentEquals(Email2)) {
				Log.message("Maching checkbox Found!!:---" + Exp_checkbox2);
				driver.findElement(By.xpath("(//*[@id='divUserListView']/ul/li/section[1]/input)[" + j + "]")).click();
				Log.message("Clicked on expected checkbox!!");
				SkySiteUtils.waitTill(5000);
				break;
			}
		}

		for (int k = 1; k <= all_checkbox_count; k++) {
			String Exp_checkbox1 = driver
					.findElement(By.xpath("(//*[@id='divUserListView']/ul/li/section[2]/a)[" + k + "]")).getText();
			Log.message(Exp_checkbox1);
			// Validating - Expected project is selected or not
			if (Exp_checkbox1.trim().contentEquals(Email3)) {
				Log.message("Maching Project Found!!" + Exp_checkbox1);
				driver.findElement(By.xpath("(//*[@id='divUserListView']/ul/li/section[1]/input)[" + k + "]")).click();
				Log.message("Clicked on expected checkbox!!");
				SkySiteUtils.waitTill(5000);
				break;
			}
		}

		SkySiteUtils.waitForElement(driver, SelectuserButton, 120);
		SelectuserButton.click();
		Log.message("Select User Button Clicked Sucessfully");
		String comment = PropertyReader.getProperty("AddComment");
		SkySiteUtils.waitForElement(driver, ForwardComment, 120);
		ForwardComment.sendKeys(comment);
		Log.message("Comment Entered Sucessfully");
		Forwaded_SendButton.click();
		SkySiteUtils.waitForElement(driver, YesButton, 120);
		YesButton.click();
		Log.message("Yes Button Clicked  Sucessfully");
		SkySiteUtils.waitForElement(driver, RFIList, 120);
		if (RFIList.isDisplayed())

			return true;
		else

			return false;

	}

	public boolean Photo_projectlevel_Attachment() throws Throwable

	{
		boolean result = false;

		return result;

	}

	public boolean RFI_Creation_projectlevel() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(8000);
		SkySiteUtils.waitForElement(driver, CreateRFILink, 60);
		CreateRFILink.click();
		Log.message("Create RFI Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, TOMail, 60);
		TOMail.click();
		SkySiteUtils.waitTill(5000);
		Log.message("To Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUserInbox, 120);
		String ToEmail = PropertyReader.getProperty("SelectUserAssignTo_Email");
		SkySiteUtils.waitTill(5000);
		SelectUserInbox.sendKeys(ToEmail);
		SkySiteUtils.waitTill(2000);
		Log.message("Enter value In Select User InBox:" + ToEmail);
		SkySiteUtils.waitForElement(driver, SearchButton, 120);
		SearchButton.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AssignTo_Checkbox, 120);
		SkySiteUtils.waitTill(2000);
		To_Checkbox.click();
		SkySiteUtils.waitTill(2000);
		Log.message("Checkbox Selected for particular Mail Id");
		SkySiteUtils.waitForElement(driver, SelectuserButton, 120);
		SkySiteUtils.waitTill(2000);
		SelectuserButton.click();
		SkySiteUtils.waitTill(2000);
		Log.message("Select User Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, ccMail, 120);
		SkySiteUtils.waitTill(5000);
		ccMail.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Cc Link Clicked Sucessfully");
		String CCMail = PropertyReader.getProperty("SelectUserCCOption_Email");
		Log.message("CC Display:" + CCMail);
		SkySiteUtils.waitTill(2000);
		SelectUserInbox.sendKeys(CCMail);
		SkySiteUtils.waitTill(5000);
		Log.message("Enter value In Select User InBox:" + CCMail);
		SkySiteUtils.waitForElement(driver, SearchButton, 120);
		SearchButton.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AssignTo_Checkbox, 120);
		SkySiteUtils.waitTill(2000);
		To_Checkbox.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Checkbox Selected for particular Mail Id");
		SkySiteUtils.waitForElement(driver, SelectuserButton, 120);
		SelectuserButton.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Select User Button Clicked Sucessfully");
		// ============Subject Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Subject, 60);
		String subject1 = PropertyReader.getProperty("RFI_Subject");
		SkySiteUtils.waitTill(5000);
		Subject.sendKeys(subject1);
		Log.message("Subject Entered Into Inbox");
		// ============Question Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Question, 60);
		String question = PropertyReader.getProperty("RFI_Questions");
		Question.sendKeys(question);
		SkySiteUtils.waitTill(5000);
		Log.message("Question Entered Into Inbox");

		if (Addattachment.isDisplayed()) {
			Addattachment.click();
			Log.message("Attachment button Click Sucessfully");
		}
		// ===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);// String to
																// Integer
		Log.message("FileCount: " + FileCount);
		UploadFiles(FolderPath, FileCount);
		SkySiteUtils.waitTill(3000);
		// project folder Tab
		SkySiteUtils.waitForElement(driver, Projectfiles, 30);
		Log.message("Waiting for Project Tab button to be appeared");
		Projectfiles.click();
		Log.message("Project files Tab Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		// entered the value in searchbox
		String Folder1 = PropertyReader.getProperty("FolderName");
		SearchProjectfilesInbox.sendKeys(Folder1);
		SkySiteUtils.waitTill(2000);
		Log.message("Entered the value in project files Inbox field");

		SkySiteUtils.waitForElement(driver, ProjectfilesTab_searchButton, 30);
		ProjectfilesTab_searchButton.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Search Button Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Folder, 60);
		Folder.click();
		Log.message("Folder Selected And clicked");
		SkySiteUtils.waitForElement(driver, File_CheckBox, 60);
		File_CheckBox.click();
		Log.message("File CheckBox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, btnCreateRFI, 60);
		btnCreateRFI.click();
		Log.message("Button RFI Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, RFIFirstShortcut, 60);
		if (RFIFirstShortcut.isDisplayed())
			return true;
		else
			return false;

	}

	public boolean RFI_Creation_projectlevel_WITHOUT_attachment() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, CreateRFILink, 60);
		CreateRFILink.click();
		Log.message("Create RFI Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, TOMail, 60);
		TOMail.click();
		SkySiteUtils.waitTill(5000);
		Log.message("To Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUserInbox, 120);
		String ToEmail = PropertyReader.getProperty("SelectUserAssignTo_Email");
		SkySiteUtils.waitTill(5000);
		SelectUserInbox.sendKeys(ToEmail);
		SkySiteUtils.waitTill(2000);
		Log.message("Enter value In Select User InBox:" + ToEmail);
		SkySiteUtils.waitForElement(driver, SearchButton, 120);
		SearchButton.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AssignTo_Checkbox, 120);
		SkySiteUtils.waitTill(2000);
		To_Checkbox.click();
		SkySiteUtils.waitTill(2000);
		Log.message("Checkbox Selected for particular Mail Id");
		SkySiteUtils.waitForElement(driver, SelectuserButton, 120);
		SkySiteUtils.waitTill(2000);
		SelectuserButton.click();
		SkySiteUtils.waitTill(2000);
		Log.message("Select User Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, ccMail, 120);
		SkySiteUtils.waitTill(5000);
		ccMail.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Cc Link Clicked Sucessfully");
		String CCMail = PropertyReader.getProperty("SelectUserCCOption_Email");
		Log.message("CC Display:" + CCMail);
		SkySiteUtils.waitTill(2000);
		SelectUserInbox.sendKeys(CCMail);
		SkySiteUtils.waitTill(5000);
		Log.message("Enter value In Select User InBox:" + CCMail);
		SkySiteUtils.waitForElement(driver, SearchButton, 120);
		SearchButton.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AssignTo_Checkbox, 120);
		SkySiteUtils.waitTill(2000);
		To_Checkbox.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Checkbox Selected for particular Mail Id");
		SkySiteUtils.waitForElement(driver, SelectuserButton, 120);
		SelectuserButton.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Select User Button Clicked Sucessfully");
		// ============Subject Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Subject, 60);
		String subject1 = PropertyReader.getProperty("RFI_Subject");
		SkySiteUtils.waitTill(5000);
		Subject.sendKeys(subject1);
		Log.message("Subject Entered Into Inbox");
		// ============Question Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Question, 60);
		String question = PropertyReader.getProperty("RFI_Questions");
		Question.sendKeys(question);
		SkySiteUtils.waitTill(5000);
		Log.message("Question Entered Into Inbox");

		if (Addattachment.isDisplayed()) {
			Addattachment.click();
			Log.message("Attachment button Click Sucessfully");
		}

		SkySiteUtils.waitForElement(driver, btnCreateRFI, 60);
		btnCreateRFI.click();
		Log.message("Button RFI Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, RFIFirstShortcut, 60);
		if (RFIFirstShortcut.isDisplayed())
			return true;
		else
			return false;

	}
	
	
	public boolean RFI_Creation_projectlevel_Draft() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, CreateRFILink, 60);
		CreateRFILink.click();
		Log.message("Create RFI Link Clicked Sucessfully");		
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, TOMail, 60);
		TOMail.click();
		SkySiteUtils.waitTill(5000);
		Log.message("To Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUserInbox, 120);
		String ToEmail = PropertyReader.getProperty("Self_user");
		SkySiteUtils.waitTill(5000);
		SelectUserInbox.sendKeys(ToEmail);
		SkySiteUtils.waitTill(2000);
		Log.message("Enter value In Select User InBox:" + ToEmail);
		SkySiteUtils.waitForElement(driver, SearchButton, 120);
		SearchButton.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AssignTo_Checkbox, 120);
		SkySiteUtils.waitTill(2000);
		To_Checkbox.click();
		SkySiteUtils.waitTill(2000);
		Log.message("Checkbox Selected for particular Mail Id");
		SkySiteUtils.waitForElement(driver, SelectuserButton, 120);
		SkySiteUtils.waitTill(2000);
		SelectuserButton.click();
		SkySiteUtils.waitTill(2000);
		Log.message("Select User Button Clicked Sucessfully");
		// ============Subject Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Subject, 60);
		String subject1 = PropertyReader.getProperty("RFI_Subject");
		SkySiteUtils.waitTill(5000);
		Subject.sendKeys(subject1);
		Log.message("Subject Entered Into Inbox");
		// ============Question Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Question, 60);
		String question = PropertyReader.getProperty("RFI_Questions");
		Question.sendKeys(question);
		SkySiteUtils.waitTill(5000);
		Log.message("Question Entered Into Inbox");

		if (Addattachment.isDisplayed()) {
			Addattachment.click();
			Log.message("Attachment button Click Sucessfully");
		}

		SkySiteUtils.waitForElement(driver, saveAsDraft, 60);
		saveAsDraft.click();
		Log.message("Save As draft Button Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, RFIFirstShortcut, 60);
		if (RFIFirstShortcut.isDisplayed())
			return true;
		else
			return false;

	}
	
	
	
	public boolean RFI__projectlevel_send() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, DraftRFILink, 60);
		DraftRFILink.click();
		Log.message("Create RFI Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, TOMail, 60);
		TOMail.click();
		SkySiteUtils.waitTill(5000);
		Log.message("To Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUserInbox, 120);
		String ToEmail = PropertyReader.getProperty("SelectUserAssignTo_Email");
		SkySiteUtils.waitTill(5000);
		SelectUserInbox.sendKeys(ToEmail);
		SkySiteUtils.waitTill(2000);
		Log.message("Enter value In Select User InBox:" + ToEmail);
		SkySiteUtils.waitForElement(driver, SearchButton, 120);
		SearchButton.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AssignTo_Checkbox, 120);
		SkySiteUtils.waitTill(2000);
		To_Checkbox.click();
		SkySiteUtils.waitTill(2000);
		Log.message("Checkbox Selected for particular Mail Id");
		SkySiteUtils.waitForElement(driver, SelectuserButton, 120);
		SkySiteUtils.waitTill(2000);
		SelectuserButton.click();
		SkySiteUtils.waitTill(2000);
		Log.message("Select User Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, ccMail, 120);
		SkySiteUtils.waitTill(5000);
		ccMail.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Cc Link Clicked Sucessfully");
		String CCMail = PropertyReader.getProperty("SelectUserCCOption_Email");
		Log.message("CC Display:" + CCMail);
		SkySiteUtils.waitTill(2000);
		SelectUserInbox.sendKeys(CCMail);
		SkySiteUtils.waitTill(5000);
		Log.message("Enter value In Select User InBox:" + CCMail);
		SkySiteUtils.waitForElement(driver, SearchButton, 120);
		SearchButton.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AssignTo_Checkbox, 120);
		SkySiteUtils.waitTill(2000);
		To_Checkbox.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Checkbox Selected for particular Mail Id");
		SkySiteUtils.waitForElement(driver, SelectuserButton, 120);
		SelectuserButton.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Select User Button Clicked Sucessfully");		
		SkySiteUtils.waitForElement(driver, btnCreateRFI, 60);
		btnCreateRFI.click();
		Log.message("Button RFI Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, RFIFirstShortcut, 60);
		if (RFIFirstShortcut.isDisplayed())
			return true;
		else
			return false;

	}



	public boolean RFI_Creation() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, CreateRFILink, 60);
		CreateRFILink.click();
		Log.message("Create RFI Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, TOMail, 60);
		TOMail.click();
		SkySiteUtils.waitTill(5000);
		Log.message("To Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUserInbox, 120);
		String ToEmail = PropertyReader.getProperty("SelectUserAssignTo_Email");
		SkySiteUtils.waitTill(5000);
		SelectUserInbox.sendKeys(ToEmail);
		SkySiteUtils.waitTill(2000);
		Log.message("Enter value In Select User InBox:" + ToEmail);
		SkySiteUtils.waitForElement(driver, SearchButton, 120);
		SearchButton.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AssignTo_Checkbox, 120);
		SkySiteUtils.waitTill(2000);
		AssignTo_Checkbox.click();
		SkySiteUtils.waitTill(2000);
		Log.message("Checkbox Selected for particular Mail Id");
		SkySiteUtils.waitForElement(driver, SelectuserButton, 120);
		SkySiteUtils.waitTill(2000);
		SelectuserButton.click();
		SkySiteUtils.waitTill(2000);
		Log.message("Select User Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, ccMail, 120);
		SkySiteUtils.waitTill(5000);
		ccMail.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Cc Link Clicked Sucessfully");
		String CCMail = PropertyReader.getProperty("SelectUserCCOption_Email");
		Log.message("CC Display:" + CCMail);
		SkySiteUtils.waitTill(2000);
		SelectUserInbox.sendKeys(CCMail);
		SkySiteUtils.waitTill(5000);
		Log.message("Enter value In Select User InBox:" + CCMail);
		SkySiteUtils.waitForElement(driver, SearchButton, 120);
		SearchButton.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AssignTo_Checkbox, 120);
		SkySiteUtils.waitTill(2000);
		AssignTo_Checkbox.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Checkbox Selected for particular Mail Id");
		SkySiteUtils.waitForElement(driver, SelectuserButton, 120);
		SelectuserButton.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Select User Button Clicked Sucessfully");
		// ============Subject Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Subject, 60);
		String subject1 = PropertyReader.getProperty("RFI_Subject");
		SkySiteUtils.waitTill(5000);
		Subject.sendKeys(subject1);
		Log.message("Subject Entered Into Inbox");
		// ============Question Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Question, 60);
		String question = PropertyReader.getProperty("RFI_Questions");
		Question.sendKeys(question);
		SkySiteUtils.waitTill(5000);
		Log.message("Question Entered Into Inbox");
		// ===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);// String to
																// Integer
		Log.message("FileCount: " + FileCount);
		UploadFiles(FolderPath, FileCount);
		SkySiteUtils.waitTill(3000);
		// project folder Tab
		SkySiteUtils.waitForElement(driver, Projectfiles, 30);
		Log.message("Waiting for Project Tab button to be appeared");
		Projectfiles.click();
		Log.message("Project files Tab Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		// entered the value in searchbox
		String Folder1 = PropertyReader.getProperty("FolderName");
		SearchProjectfilesInbox.sendKeys(Folder1);
		SkySiteUtils.waitTill(2000);
		Log.message("Entered the value in project files Inbox field");

		SkySiteUtils.waitForElement(driver, ProjectfilesTab_searchButton, 30);
		ProjectfilesTab_searchButton.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Search Button Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Folder, 60);
		Folder.click();
		Log.message("Folder Selected And clicked");
		SkySiteUtils.waitForElement(driver, File_CheckBox, 60);
		File_CheckBox.click();
		Log.message("File CheckBox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, btnCreateRFI, 60);
		btnCreateRFI.click();
		Log.message("Button RFI Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, RFIFirstShortcut, 60);
		if (RFIFirstShortcut.isDisplayed())
			return true;
		else
			return false;

	}

	public boolean RFI_Attribute_Single_Validation(String Attribute1) throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, FirstRFITab, 120);
		FirstRFITab.click();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, RFIImage, 120);
		RFIImage.click();
		SkySiteUtils.waitForElement(driver, OpenStatusD, 60);
		String Status = OpenStatusD.getText();
		Log.message("Status Message is: " + Status);
		String status = PropertyReader.getProperty("Status");
		if (Status.equalsIgnoreCase(status)) {
			Log.message("Status verified Sucessfully ");
		} else {
			Log.message("Status not verified Sucessfully");
		}

		SkySiteUtils.waitTill(5000);
		String Duedate = duedate.getText();
		String[] splitStr = Duedate.split("\\s+");
		SkySiteUtils.waitTill(500);

		String output = splitStr[2];
		Log.message("No of days :   " + output);
		String days = PropertyReader.getProperty("NOofDays");
		if (days.equalsIgnoreCase(Duedate)) {
			Log.message("Day verified Sucessfully ");

		} else {
			Log.message("Day not verified Sucessfully");
		}

		// =================Email To=================
		SkySiteUtils.waitForElement(driver, RFI_Email_TO, 60);
		String Email_To = RFI_Email_TO.getText();
		Log.message("Email_To Message is: " + Email_To);
		String Email = PropertyReader.getProperty("Email_AssignTo_emp");
		if (Email_To.equalsIgnoreCase(Email)) {
			Log.message("Email To verified Sucessfully ");

		} else {
			Log.message("Email To not verified Sucessfully");
		}

		// =================CC Option=================
		SkySiteUtils.waitForElement(driver, RFI_Email_CC, 60);
		String Email_CC = RFI_Email_CC.getText();
		Log.message("Email_To Message is: " + Email_CC);
		String CCoption = PropertyReader.getProperty("Email_CCOption");
		if (Email_CC.equalsIgnoreCase(CCoption)) {
			Log.message("CC Email  verified Sucessfully ");

		} else {
			Log.message("CC Email  not verified Sucessfully");
		}
		SkySiteUtils.waitForElement(driver, RFI_Questions, 60);
		String question = RFI_Questions.getText();

		String Question = PropertyReader.getProperty("RFI_Questions");
		if (question.equalsIgnoreCase(Question)) {
			Log.message("Question  verified Sucessfully ");

		} else {
			Log.message("Question   not verified Sucessfully");
		}

		SkySiteUtils.waitForElement(driver, QuestionDropdown, 60);
		QuestionDropdown.click();
		// ========================================================
		SkySiteUtils.waitForElement(driver, Automation_Status1, 60);
		String customAttribute1 = Automation_Status1.getText();

		if (Attribute1.contentEquals(customAttribute1)) {
			Log.message("Custom Attribute1  verified Sucessfully ");

		} else {
			Log.message("Custom Attribute1 not verified Sucessfully");
		}

		SkySiteUtils.waitForElement(driver, QuestionDropdown, 60);
		QuestionDropdown.click();
		SkySiteUtils.waitForElement(driver, Addcomment, 60);
		String addComment = PropertyReader.getProperty("AddComment");
		Addcomment.sendKeys(addComment);
		Log.message("Entered Value In Add Comment inbox: " + addComment);
		SkySiteUtils.waitTill(5000);
		Submitbutton.click();
		Log.message("submit button clicked sucessfully");
		SkySiteUtils.waitTill(3000);
		String Commm = driver.findElement(By.xpath("//*[@id='divEditRFIUI']/div/div[2]/div/div[2]/ul/li/p[1]/span"))
				.getText();
		Log.message("Comment name is:" + Commm);
		SkySiteUtils.waitTill(5000);
		if (Commm.trim().contains("'" + addComment + "'"))
			return true;
		else
			return false;

	}

	public boolean RFI_AttributeValidation() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, RFIImage, 120);
		RFIImage.click();
		SkySiteUtils.waitForElement(driver, OpenStatusD, 60);
		String Status = OpenStatusD.getText();
		Log.message("Status Message is: " + Status);
		String status = PropertyReader.getProperty("Status");
		if (Status.equalsIgnoreCase(status)) {
			Log.message("Status verified Sucessfully ");
		} else {
			Log.message("Status not verified Sucessfully");
		}

		SkySiteUtils.waitTill(3000);
		String Duedate = duedate.getText();
		String[] splitStr = Duedate.split("\\s+");
		String output = splitStr[2];
		Log.message("No of days1 :   " + output);
		String days = PropertyReader.getProperty("NOofDays");
		Log.message("No of days2 :   " + days);
		if (output.equalsIgnoreCase(days)) {
			Log.message("Day verified Sucessfully ");

		} else {
			Log.message("Day not verified Sucessfully");
		}

		// =================Email To=================
		SkySiteUtils.waitForElement(driver, RFI_Email_TO, 60);
		String Email_To = RFI_Email_TO.getText();
		Log.message("Email_To Message is: " + Email_To);
		String Email = PropertyReader.getProperty("Email_AssignTo_emp");
		if (Email_To.equalsIgnoreCase(Email)) {
			Log.message("Email To verified Sucessfully ");

		} else {
			Log.message("Email To not verified Sucessfully");
		}

		// =================CC Option=================
		SkySiteUtils.waitForElement(driver, RFI_Email_CC, 60);
		String Email_CC = RFI_Email_CC.getText();
		Log.message("Email_To Message is: " + Email_CC);
		String CCoption = PropertyReader.getProperty("Email_CCOption");
		if (Email_CC.equalsIgnoreCase(CCoption)) {
			Log.message("CC Email  verified Sucessfully ");

		} else {
			Log.message("CC Email  not verified Sucessfully");
		}
		SkySiteUtils.waitForElement(driver, RFI_Questions, 60);
		String question = RFI_Questions.getText();

		String Question = PropertyReader.getProperty("RFI_Questions");
		if (question.equalsIgnoreCase(Question)) {
			Log.message("Question  verified Sucessfully ");

		} else {
			Log.message("Question   not verified Sucessfully");
		}

		SkySiteUtils.waitForElement(driver, QuestionDropdown, 60);
		SkySiteUtils.waitTill(5000);
		QuestionDropdown.click();
		SkySiteUtils.waitTill(5000);
		// ========================================================
		SkySiteUtils.waitForElement(driver, Automation_Status1, 60);
		String customAttribute1 = Automation_Status1.getText();

		String Attribute1 = PropertyReader.getProperty("customAttribute1");
		if (Attribute1.equalsIgnoreCase(customAttribute1)) {
			Log.message("Custom Attribute1  verified Sucessfully ");

		} else {
			Log.message("Custom Attribute1 not verified Sucessfully");
		}

		// ========================================================
		SkySiteUtils.waitForElement(driver, Automation_Status2, 60);
		String customAttribute2 = Automation_Status2.getText();
		String Attribute2 = PropertyReader.getProperty("customAttribute2");
		if (Attribute2.equalsIgnoreCase(customAttribute2)) {
			Log.message("Custom Attribute2  verified Sucessfully ");

		} else {
			Log.message("Custom Attribute2 not verified Sucessfully");
		}

		// ===============================================

		SkySiteUtils.waitForElement(driver, Automation_Status3, 60);
		String customAttribute3 = Automation_Status3.getText();

		String Attribute3 = PropertyReader.getProperty("customAttribute3");
		if (Attribute3.equalsIgnoreCase(customAttribute3)) {
			Log.message("Custom Attribute3  verified Sucessfully ");

		} else {
			Log.message("Custom Attribute3 not verified Sucessfully");
		}
		// =====================================================

		SkySiteUtils.waitForElement(driver, Automation_Status4, 60);
		String customAttribute4 = Automation_Status4.getText();
		String Attribute4 = PropertyReader.getProperty("customAttribute4");
		if (Attribute4.equalsIgnoreCase(customAttribute4)) {
			Log.message("Custom Attribute4  verified Sucessfully ");

		} else {
			Log.message("Custom Attribute4 not verified Sucessfully");
		}
		// =====================================================

		String customAttribute5 = Automation_Status5.getText();

		String Attribute5 = PropertyReader.getProperty("customAttribute5");
		if (Attribute5.equalsIgnoreCase(customAttribute5)) {
			Log.message("Custom Attribute 5 verified Sucessfully ");

		} else {
			Log.message("Custom Attribute 5 not verified Sucessfully");
		}
		SkySiteUtils.waitForElement(driver, QuestionDropdown, 60);
		QuestionDropdown.click();
		SkySiteUtils.waitForElement(driver, Addcomment, 60);
		String addComment = PropertyReader.getProperty("AddComment");
		Addcomment.sendKeys(addComment);
		Log.message("Entered Value In Add Comment inbox: " + addComment);
		SkySiteUtils.waitTill(3000);
		Submitbutton.click();
		Log.message("submit button clicked sucessfully");
		SkySiteUtils.waitTill(3000);
		String Commm = driver.findElement(By.xpath(".//*[@id='divEditRFIUI']/div/div[2]/div/div[2]/ul/li/p[1]"))
				.getText();
		Log.message("Comment name is:" + Commm);
		SkySiteUtils.waitTill(8000);
		if (Commm.trim().contains("'" + addComment + "'"))
			return true;

		else
			return false;

	}

	public boolean RFI_CreationWithSingleAttribute() throws Throwable

	{
		boolean result = true;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, CreateRFILink, 60);
		CreateRFILink.click();
		Log.message("Create RFI Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, TOMail, 60);
		SkySiteUtils.waitTill(2000);
		TOMail.click();
		Log.message("To Link Clicked Sucessfully");
		String ToEmail = PropertyReader.getProperty("SelectUserAssignTo_Email");
		SkySiteUtils.waitForElement(driver, SelectUserInbox, 120);
		SkySiteUtils.waitTill(2000);
		SelectUserInbox.sendKeys(ToEmail);
		Log.message("Enter value In Select User InBox:" + ToEmail);
		SkySiteUtils.waitForElement(driver, SearchButton, 120);
		SearchButton.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AssignTo_Checkbox, 60);
		SkySiteUtils.waitTill(4000);
		AssignTo_Checkbox.click();
		Log.message("Checkbox Selected for particular Mail Id");
		SkySiteUtils.waitForElement(driver, SelectuserButton, 120);
		SelectuserButton.click();
		Log.message("Select User Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, ccMail, 120);
		SkySiteUtils.waitTill(2000);
		ccMail.click();
		SkySiteUtils.waitTill(2000);
		Log.message("Cc Link Clicked Sucessfully");
		String CCMail = PropertyReader.getProperty("SelectUserCCOption_Email");
		SkySiteUtils.waitForElement(driver, SelectUserInbox, 120);
		SkySiteUtils.waitTill(5000);
		SelectUserInbox.sendKeys(CCMail);
		SkySiteUtils.waitTill(2000);
		Log.message("CC Display:" + CCMail);
		Log.message("Enter value In Select User InBox:" + CCMail);
		SkySiteUtils.waitForElement(driver, SearchButton, 120);
		SkySiteUtils.waitTill(2000);
		SearchButton.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AssignTo_Checkbox, 60);
		SkySiteUtils.waitTill(4000);
		AssignTo_Checkbox.click();
		Log.message("Checkbox Selected for particular Mail Id");
		SkySiteUtils.waitForElement(driver, SelectuserButton, 120);
		SelectuserButton.click();
		Log.message("Select User Button Clicked Sucessfully");
		// ============Subject Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Subject, 60);
		String subject1 = PropertyReader.getProperty("RFI_Subject");
		Subject.sendKeys(subject1);
		Log.message("Subject Entered Into Inbox");
		// ============Question Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Question, 60);
		String question = PropertyReader.getProperty("RFI_Questions");
		Question.sendKeys(question);
		Log.message("Question Entered Into Inbox");
		// Multiple Attribute object========================

		String CustomAttribute1 = PropertyReader.getProperty("customAttribute1");
		SkySiteUtils.waitForElement(driver, Attribute1, 60);
		if (Attribute1.isDisplayed()) {
			Attribute1.sendKeys(CustomAttribute1);
		}

		// ===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);// String to
																// Integer
		Log.message("FileCount: " + FileCount);
		SkySiteUtils.waitTill(1000);
		// UploadFiles(FolderPath, FileCount);
		// SkySiteUtils.waitTill(1000);
		// project folder Tab
		/*
		 * SkySiteUtils.waitForElement(driver, Projectfiles, 30);
		 * Log.message("Waiting for Project Tab button to be appeared");
		 * Projectfiles.click();
		 * Log.message("Project files Tab Clicked Sucessfully");
		 * SkySiteUtils.waitTill(5000); //entered the value in searchbox String
		 * Folder1 = PropertyReader.getProperty("FolderName");
		 * SearchProjectfilesInbox.sendKeys(Folder1);
		 * SkySiteUtils.waitTill(2000);
		 * Log.message("Entered the value in project files Inbox field");
		 * 
		 * SkySiteUtils.waitForElement(driver, ProjectfilesTab_searchButton,
		 * 30); ProjectfilesTab_searchButton.click();
		 * SkySiteUtils.waitTill(5000);
		 * Log.message("Search Button Click Sucessfully");
		 * SkySiteUtils.waitForElement(driver, Folder, 60); Folder.click();
		 * Log.message("Folder Selected And clicked");
		 * SkySiteUtils.waitForElement(driver, File_CheckBox, 60);
		 * File_CheckBox.click();
		 * Log.message("File CheckBox Clicked Sucessfully");
		 */
		SkySiteUtils.waitForElement(driver, btnCreateRFI, 60);
		btnCreateRFI.click();
		Log.message("Button RFI Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, Myrfi, 60);
		SkySiteUtils.waitTill(5000);
		Myrfi.click();
		Log.message("MyRFI Tab Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, RFIList, 60);
		if (RFIList.isDisplayed())
			return true;
		else
			return false;

	}

	public boolean RFI_AscendingorderValidation() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, AllRFITab, 120);
		AllRFITab.click();

		SkySiteUtils.waitTill(5000);
		Boolean isPresent = driver.findElements(By.xpath("//*[@id='iSortOrder']")).size() > 0;
		if (isPresent) {

			SkySiteUtils.waitForElement(driver, AssendAndDecend, 60);
			JavascriptExecutor executor1 = (JavascriptExecutor) driver;
			executor1.executeScript("arguments[0].click();", AssendAndDecend);
			Log.message("Acscending Button Clicked Sucessfully");

			ArrayList<String> obtainedList = new ArrayList<>();
			java.util.List<WebElement> elementList = driver
					.findElements(By.xpath("//span[@ data-bind='text: RFINumber()']"));
			for (WebElement we : elementList) {
				obtainedList.add(we.getText());
			}
			ArrayList<String> sortedList = new ArrayList<>();
			for (String s : obtainedList) {
				sortedList.add(s);
			}
			SkySiteUtils.waitTill(5000);
			Collections.sort(sortedList);

			Assert.assertTrue(sortedList.equals(obtainedList));
			Log.message("RFI Ascending ObtainList List Display:-----" + obtainedList);
			Log.message("RFI Ascending sorted List Display:-----" + sortedList);
			Log.message("RFI Ascending order Display:-----" + sortedList.equals(obtainedList));

		}
		// return result;
		if (RFIList1.isDisplayed())

			return true;
		else

			return false;

	}

	public boolean RFI_DecendingorderValidation() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, AllRFITab, 120);
		AllRFITab.click();

		SkySiteUtils.waitTill(8000);

		Log.message("Enter Into decending order method");
		Boolean isPresent2 = driver.findElements(By.id("iSortOrder")).size() > 0;
		if (isPresent2) {
			SkySiteUtils.waitForElement(driver, AssendAndDecend, 60);
			JavascriptExecutor executor1 = (JavascriptExecutor) driver;
			executor1.executeScript("arguments[0].click();", AssendAndDecend);

			// driver.navigate().refresh();
			Log.message("Desending Button Clicked Sucessfully");

			SkySiteUtils.waitTill(15000);

			ArrayList<String> obtainedList = new ArrayList<>();
			java.util.List<WebElement> elementList = driver
					.findElements(By.xpath("//span[@ data-bind='text: RFINumber()']"));
			for (WebElement we : elementList) {
				obtainedList.add(we.getText());
			}
			Log.message("RFI desending ObtainList List Display:-----" + obtainedList);
			ArrayList<String> sortedList1 = new ArrayList<>();
			Collections.copy(obtainedList, sortedList1);
			Thread.sleep(5000);
			Collections.sort(sortedList1);
			SkySiteUtils.waitTill(1000);
			Collections.reverse(sortedList1);
			// Assert.assertTrue(sortedList.equals(obtainedList));
			Log.message("RFI desending ObtainList List Display:-----" + obtainedList);
			Log.message("RFI desending Sorted List Display:-----" + sortedList1);
			Log.message("RFI desending order Display:-----" + obtainedList.equals(sortedList1));

		}
		// return result;

		if (RFIList1.isDisplayed())

			return true;
		else

			return false;

	}

	public boolean PhotoValidation() throws Throwable

	{
		boolean result = false;

		return result;

	}

	public boolean RFI_CreationWithMultipleAttribute() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, CreateRFILink, 120);
		CreateRFILink.click();
		Log.message("Create RFI Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, TOMail, 100);
		if (TOMail.isDisplayed()) {
			TOMail.click();
		}
		Log.message("To Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUserInbox, 120);
		String ToEmail = PropertyReader.getProperty("SelectUserAssignTo_Email");
		SkySiteUtils.waitTill(5000);
		SelectUserInbox.sendKeys(ToEmail);
		Log.message("Enter value In Select User InBox:" + ToEmail);
		SkySiteUtils.waitForElement(driver, SearchButton, 120);
		SearchButton.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AssignTo_Checkbox, 120);
		SkySiteUtils.waitTill(5000);
		AssignTo_Checkbox.click();
		Log.message("Checkbox Selected for particular Mail Id");
		SkySiteUtils.waitForElement(driver, SelectuserButton, 100);
		SelectuserButton.click();
		Log.message("Select User Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, ccMail, 100);
		ccMail.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Cc Link Clicked Sucessfully");
		String CCMail = PropertyReader.getProperty("SelectUserCCOption_Email");
		SkySiteUtils.waitForElement(driver, SelectUserInbox, 100);
		SkySiteUtils.waitTill(5000);
		SelectUserInbox.sendKeys(CCMail);
		Log.message("CC Display:" + CCMail);
		Log.message("Enter value In Select User InBox:" + CCMail);
		SkySiteUtils.waitForElement(driver, SearchButton, 120);
		SkySiteUtils.waitTill(2000);
		SearchButton.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AssignTo_Checkbox, 120);
		SkySiteUtils.waitTill(2000);
		AssignTo_Checkbox.click();
		Log.message("Checkbox Selected for particular Mail Id");
		SkySiteUtils.waitForElement(driver, SelectuserButton, 60);
		SkySiteUtils.waitTill(3000);
		SelectuserButton.click();
		Log.message("Select User Button Clicked Sucessfully");
		// ============Subject Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Subject, 60);
		String subject1 = PropertyReader.getProperty("RFI_Subject");
		Subject.sendKeys(subject1);
		Log.message("Subject Entered Into Inbox");
		// ============Question Entered Into Particular Field=======
		SkySiteUtils.waitForElement(driver, Question, 100);
		String question = PropertyReader.getProperty("RFI_Questions");
		Question.sendKeys(question);
		Log.message("Question Entered Into Inbox");
		// Multiple Attribute object========================

		String CustomAttribute1 = PropertyReader.getProperty("customAttribute1");
		String CustomAttribute2 = PropertyReader.getProperty("customAttribute2");
		String CustomAttribute3 = PropertyReader.getProperty("customAttribute3");
		String CustomAttribute4 = PropertyReader.getProperty("customAttribute4");
		String CustomAttribute5 = PropertyReader.getProperty("customAttribute5");
		SkySiteUtils.waitForElement(driver, Attribute1, 60);
		if (Attribute1.isDisplayed()) {
			Attribute1.sendKeys(CustomAttribute1);
			Log.message("Value Entered into Customer Attribute Field 1: " + CustomAttribute1);
		}
		SkySiteUtils.waitForElement(driver, Attribute2, 60);
		if (Attribute2.isDisplayed()) {
			Attribute2.sendKeys(CustomAttribute2);
			Log.message("Value Entered into Customer Attribute Field 2: " + CustomAttribute2);
		}

		boolean exists = driver.findElements(By.xpath("//input[@data-bind='value: CP3']")).size() != 0;
		Log.message("boolean Value " + exists);
		if (exists == true) {

			SkySiteUtils.waitForElement(driver, Attribute3, 60);
			if (Attribute3.isDisplayed()) {
				Attribute3.sendKeys(CustomAttribute3);
				Log.message("Value Entered into Customer Attribute Field 3: " + CustomAttribute3);
			}
			SkySiteUtils.waitForElement(driver, Attribute4, 60);
			if (Attribute4.isDisplayed()) {
				Attribute4.sendKeys(CustomAttribute4);
				Log.message("Value Entered into Customer Attribute Field 4: " + CustomAttribute4);
			}
			SkySiteUtils.waitForElement(driver, Attribute5, 60);
			if (Attribute5.isDisplayed()) {
				Attribute5.sendKeys(CustomAttribute5);
				Log.message("Value Entered into Customer Attribute Field 5: " + CustomAttribute5);
			}
		}

		// ===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);// String to
																// Integer
		Log.message("FileCount: " + FileCount);
		SkySiteUtils.waitTill(1000);
		// UploadFiles(FolderPath, FileCount);
		// project folder Tab
		/*
		 * SkySiteUtils.waitForElement(driver, Projectfiles, 30);
		 * Log.message("Waiting for Project Tab button to be appeared");
		 * Projectfiles.click();
		 * Log.message("Project files Tab Clicked Sucessfully");
		 * SkySiteUtils.waitTill(5000); //entered the value in searchbox String
		 * Folder1 = PropertyReader.getProperty("FolderName");
		 * SkySiteUtils.waitTill(5000);
		 * SearchProjectfilesInbox.sendKeys(Folder1);
		 * Log.message("Entered the value in project files Inbox field");
		 * SkySiteUtils.waitForElement(driver, ProjectfilesTab_searchButton,
		 * 30); SkySiteUtils.waitTill(2000);
		 * ProjectfilesTab_searchButton.click(); SkySiteUtils.waitTill(5000);
		 * Log.message("Search Button Click Sucessfully");
		 * SkySiteUtils.waitForElement(driver, Folder, 60); Folder.click();
		 * Log.message("Folder Selected And clicked");
		 * SkySiteUtils.waitForElement(driver, File_CheckBox, 60);
		 * File_CheckBox.click();
		 * Log.message("File CheckBox Clicked Sucessfully");
		 */
		SkySiteUtils.waitForElement(driver, btnCreateRFI, 60);
		btnCreateRFI.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Button RFI Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AllRFITab, 60);
		SkySiteUtils.waitTill(5000);
		AllRFITab.click();
		SkySiteUtils.waitForElement(driver, RFIList, 60);
		SkySiteUtils.waitTill(2000);
		if (RFIList.isDisplayed())
			return true;
		else
			return false;

	}

	/*
	 * public boolean PDFFileComparision( ) throws Throwable
	 * 
	 * { boolean result = false;
	 * 
	 * //from Application String Subject =
	 * PropertyReader.getProperty("RFI_Subject"); String RFIStatus
	 * =DisplayStatus.getText(); String RFINumber =DisplayRFINO.getText();
	 * String SubmissionDate =DisplayRFINO.getText(); String From =
	 * DisplayRFINO.getText(); String ContractDueDate =
	 * ContractDueDate.getText(); String Discipline =
	 * ContractDueDate1.getText(); String To = ContractDueDate2.getText();
	 * String cc = ContractDueDate3.getText(); String Specification =
	 * ContractDueDate3.getText(); String SheetAssociated =
	 * ContractDueDate3.getText(); String PotantialcostImpact =
	 * ContractDueDate3.getText(); String PotantialScheduleImpact =
	 * ContractDueDate3.getText(); String Question =
	 * PropertyReader.getProperty("RFI_Questions"); String RFIHistory =
	 * PropertyReader.getProperty("RFI_Questions"); String Attachement =
	 * PropertyReader.getProperty("RFI_Questions");
	 * 
	 * //From PDF reader value
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * return result;
	 * 
	 * 
	 * }
	 * 
	 */

	public boolean UploadFiles(String FolderPath, int FileCount)
			throws InterruptedException, AWTException, IOException {
		boolean result = false;

		SkySiteUtils.waitTill(2000);
		Log.message("Waiting for upload file button to be appeared");
		SkySiteUtils.waitForElement(driver, selectfiles, 60);
		selectfiles.click();
		SkySiteUtils.waitTill(500);
		// =================================
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String Sys_Download_Path = "./File_Download_Location/" + rn.nextFileName() + ".txt";
		Log.message("system download location2" + Sys_Download_Path);
		String tmpFileName = Sys_Download_Path;
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;

		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(1000);
			}
		}

		output.flush();
		output.close();
		SkySiteUtils.waitTill(3000);
		// Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		File dest = new File(FolderPath);
		String path = dest.getAbsolutePath();
		SkySiteUtils.waitTill(5000);
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + path + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(15000);
		SkySiteUtils.waitForElement(driver, btnCreateRFI, 60);
		SkySiteUtils.waitTill(5000);
		// Delete the temp file
		try {
			File file = new File(tmpFileName);
			if (file.delete()) {
				Log.message(file.getName() + " is deleted!");
			} else {
				Log.message("Delete operation is failed.");
			}
		} catch (Exception e) {
			Log.message("Exception occured!!!" + e);
		}

		Log.message("File Uploded Sucessfully ");

		return result;

	}
	
	public void UploadFiles_old(String FolderPath, int FileCount)throws InterruptedException, AWTException, IOException {
		//boolean result = false;
		SkySiteUtils.waitTill(5000);
		Log.message("Waiting for upload file button to be appeared");
		SkySiteUtils.waitForElement(driver, Selectfiles, 120);
		SkySiteUtils.waitTill(5000);
		Selectfiles.click();
		SkySiteUtils.waitTill(10000);
		// =================================
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();

		String Sys_Download_Path = "./File_Download_Location/" + rn.nextFileName() + ".txt";
		Log.message("system download location2" + Sys_Download_Path);
		String tmpFileName = Sys_Download_Path;

		// String tmpFileName="c:/"+rn.nextFileName()+".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;

		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a
												// variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(1000);
			}
		}

		output.flush();
		output.close();

		Log.message("waiting AutoIT Script!!");
		SkySiteUtils.waitTill(25000);

		// ====================Executing .exe autoIt file=======================================
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		File dest = new File(PropertyReader.getProperty("Upload_PhotoFile").toString());
		String path = dest.getAbsolutePath();
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + path + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(15000);

		//SkySiteUtils.waitForElement(driver, CreatePunch, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(5000);
		// ====================Delete the temp file=====================
		try {
			File file = new File(tmpFileName);
			if (file.delete()) {
				Log.message(file.getName() + " is deleted!");
			} else {
				Log.message("Delete operation is failed.");
			}
		} catch (Exception e) {
			Log.message("Exception occured!!!" + e);
		}

		//return result;

	}
	
	
	
	public boolean RFI_Photo_Validation() throws Throwable

	{
		boolean result = false;
		
		
		return result;

	}

}