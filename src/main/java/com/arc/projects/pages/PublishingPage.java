package com.arc.projects.pages;

import java.awt.AWTException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.ExcelReader;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;
import jxl.read.biff.BiffException;

public class PublishingPage extends LoadableComponent<PublishingPage> {
	ProjectsLoginPage projectsLoginPage;
    ProjectDashboardPage projectDashboardPage;
   


static WebDriver driver;
private boolean isPageLoaded;

//===============================================================================
//*******************************************************************************	
@FindBy(css="#UserID")
WebElement txtBoxUserName;	
@FindBy(css="#Password")
WebElement txtBoxPassword;	
@FindBy(css="#btnLogin")
WebElement btnLogin;	
@FindBy(css=".btn.btn-success.btn-icon")
WebElement GoToProject_Button;	
@FindBy(css="#txtFolderName")
WebElement CreateFolder_INBOX;	
@FindBy(css=".chkExcludedrawing")
WebElement ExcludeFromLatesetDoc_CheckBox;	
@FindBy(css=".btn.btn-default.btnCancel.pull-left")
WebElement Cancel_Button;	
@FindBy(css="#btnFolderCreate")
WebElement CreateFolder_Button;	
@FindBy(css="#btnFolderCreateThisFolder")
WebElement UploadFileToThisFolder_Button;
@FindBy(css=".icon.icon-ellipsis-horizontal.media-object.icon-lg")
WebElement Folder_DropdownIcon;	
@FindBy(css=".folderDownload")
WebElement Folder_Icon_DownloadDocument;	
@FindBy(xpath=".//*[@id='liPendingItemsContainer']/a/i")
WebElement PendingContainer;
//==========================Create Project===========================================
//*******************************************************************************
@FindBy(css="#txtProjectName")
WebElement ProjectName_Inbox;	
@FindBy(css="#txtProjectNumber")
WebElement ProjectNumber_Inbox;	
@FindBy(css="#txtProjectStartDate")
WebElement ProjectStartDate_Inbox;	
@FindBy(css="#txtProjectDescription")	
WebElement projectDescription_Inbox;	
@FindBy(css="#txtAddress")
WebElement SiteAddress_Inbox;	
@FindBy(css="#txtCity")
WebElement City_INBOX;	
@FindBy(css="#txtState")
WebElement Zip_Inbox;	
@FindBy(xpath=".//*[@id='txtState']") 
WebElement state;	
@FindBy(css="#txtCountry") 
WebElement Country_Inbox;	
@FindBy(css="#txtProjectPassword")
WebElement Project_Password;	
@FindBy(css="#btnCancel")
WebElement CreateProject_CancelButton;	
@FindBy(css="#btnCreate")
WebElement CreateProject_Button;	
@FindBy(css="#slide-1")
WebElement SeeHowitSWorks_Button;	
@FindBy(css=".btn.btn-default.Slide-Prev")
WebElement Previous_Button;	
@FindBy(css=".btn.btn-primary.Slide-Next")
WebElement Next_Button;	
@FindBy(css="#isAutoHyperlinkEnabled")
WebElement Enable_AutoHyperLink_CheckBox;	
@FindBy(css="#hyperlinkOptiondirectNavigation")
WebElement Enable_HyperLink_Preview_Checkbox;	
@FindBy(css=".btn.btn-default.pull-right")
WebElement Setting_SaveButton;	
@FindBy(css="#button-0")
WebElement Alert_NoButton;	
@FindBy(css="#button-1")
WebElement Alert_YESButton;	
@FindBy(css=".btn.btn-primary.btn-lg.prjAddFolder")
WebElement FolderTree_CreateFolderButton;	
@FindBy(css="#aPrjUpldFile2")
WebElement FolderTree_UploadFileButton;

//===============================================================================
@FindBy(css=".aProjHeaderProjectLink")
WebElement PrivateProjectsTab;


//================================================================================
//*******************************************************************************
@FindBy(css=".icon.icon-ellipsis-horizontal.media-object.icon-lg") 
WebElement FolderLinkMenu;	
@FindBy(css=".folderDownload")
WebElement FolderLinkMenu_DownloadDocument;	
@FindBy(css=".folderMarkUpDownload")
WebElement FolderLinkMenu_DownloadWithMarkUp;	
@FindBy(css=".//*[@id='li_Fld_TSA56l2CK%40EmEPNxcyJ%401Q%3d%3d']/section[4]/ul/li[4]/ul/li[4]/a")
WebElement FolderLinkMenu_OrderPrint;
@FindBy(css=".send-folder.link-event")
WebElement FolderLinkMenu_Send;
@FindBy(css=".new-share")
WebElement FolderLinkMenu_ShareFolder;
@FindBy(css=".edit-projectfolder")
WebElement FolderLinkMenu_Edit;	
@FindBy(css=".delete-projectfolder.btnDelete")
WebElement FolderLinkMenu_Delete;
//============Upload File Sub-menu================================================
//*******************************************************************************
@FindBy(css=".dropdown-toggle")
WebElement UploadFileLink;	
@FindBy(css="#aPrjUpldFile")
WebElement UploadFile_MycomputerLink;	
@FindBy(css="#aPrjUpldFileCloud")
WebElement UploadFile_CloudAccountlink;	
//===========Upload files Page=====================	=================================
//*******************************************************************************
@FindBy(css=".qq-upload-button-selector.pop-btn-cont")
WebElement Choosefiles_Button;	
@FindBy(css="#btnKloudless")
WebElement Cloud_Account;	
@FindBy(css="#aPrjUpldFileCloud")
WebElement UploadWithoutindexing_button;	
@FindBy(css="#aPrjUpldFileCloud")
WebElement Uploadwithindexing_button;	
@FindBy(css="#lblFileCount")
WebElement FileCalculationDisplay;	
//===============Publishing Log Display===============================================
//*******************************************************************************

@FindBy(css=".//*[@id='ulac']/li/div[1]/div[3]/div[1]/div/h5/span[1]")
WebElement UserName;	
@FindBy(css="strong.info-trim")
WebElement FileLogNo;	
@FindBy(xpath="//ul[@id='ulac']/li/div/div[3]/div/div/h5/span[2]")
WebElement Date;	
@FindBy(xpath="//ul[@id='ulac']/li/div/div[3]/div/div/h5/span[3]")
WebElement Time;	
@FindBy(xpath="//ul[@id='ulac']/li/div/div[3]/div/div/h5/span[4]")
WebElement UploadedfileNo;	
@FindBy(linkText="Process completed")
WebElement ProcessCompleted;

@FindBy(css="#SheetName")
WebElement Sheetname;

@FindBy(css="#Discipline")
WebElement Discipline;
@FindBy(css="#btnPublishNew")
WebElement publishButton;

@FindBy(css=".form-control.input-box.txtDescription")
WebElement FileDescription;


@FindBy(xpath=".//*[@id='mainPageContainer']/div[3]/div[3]/div[1]/div/div/div[1]/span[5]")
WebElement MaxValue;




@Override
protected void load() {
	isPageLoaded = true;
	SkySiteUtils.waitForElement(driver, publishButton, 30);
	
}

@Override
protected void isLoaded() throws Error {
	if (!isPageLoaded) {
		Assert.fail();
	}
}




/**
 * Declaring constructor for initializing web elements using PageFactory class.
 * @param driver
 * @return 
 */
public PublishingPage(WebDriver driver) 
{
	   this.driver = driver;
	   PageFactory.initElements(this.driver, this);
	 }

 
	



public String ValidationwithExcelSheet() throws InterruptedException, AWTException, IOException, BiffException
{
	boolean result = false;
	
	SkySiteUtils.waitForElement(driver,publishButton , 30);
	SkySiteUtils.waitTill(5000);
	Log.message("Waiting for publish button to load page");
	
	 ExcelReader xlsrdr = new ExcelReader(PropertyReader.getProperty("TestData"),"wrong_OCR_090119");
	 String maxValue = Sheetname.getText();
	 
	 
	// for (i=0; i<sheetnameValue. );
			 
	//String maxValue = Sheetname.getText();
	 String sheetnameValue = Sheetname.getAttribute("value");		 
	
	 ExcelReader.writeColoumandrow(xlsrdr, sheetnameValue, 1,1, 0);
	 
	 String SheetTitle = xlsrdr.ReadExcel(1,1);
	 
	 Log.message("Excel sheet value :"   +SheetTitle);
	 
	 Log.message("App sheet value :"   +sheetnameValue);
	 
	 if(SheetTitle.trim().equalsIgnoreCase((sheetnameValue).trim()))
	 {
		 Log.message("Read excel file : + Pass");
		 ExcelReader.writeColoumandrow(xlsrdr, "PASSED", 1,2, 1);
	 }
	 else
	 {
		 Log.message("Read excel file : + Fail");
		 ExcelReader.writeColoumandrow(xlsrdr, "FAILED", 1,2,1);
	 }
		 		 
	 Log.message("Finish : + Comparision with SheetValue with Application Display value:"+sheetnameValue);
	 
	 
	 
	return SheetTitle;
	 
	

}


public  boolean MultipleRowValidationwithExcelSheet() throws InterruptedException, AWTException, IOException, BiffException
{
	boolean result = false;		
	SkySiteUtils.waitForElement(driver,publishButton , 30);
	SkySiteUtils.waitTill(5000);
	Log.message("Waiting for Multiple File Validation");		
	ExcelReader xlsrdr = new ExcelReader(PropertyReader.getProperty("TestData"),"wrong_OCR_090119");
	 String value1 = MaxValue.getText();
	 System.out.println(value1);
	 //Truncating value from Application.
	    int NoOfSheetsInAPage =Integer.parseInt( String.valueOf(value1).substring(13,15).trim());
		System.out.println(NoOfSheetsInAPage);
		Log.message("Waiting for Multiple File Validation:    "+NoOfSheetsInAPage);
		int i=1;
		String Exp_SheetNo = null;
		String Sheettitle = null;
		
		String SheetDiscipline= null;
		String Exp_DIscipline = null;
		String Exp_Description = null;
		String SheetDescription = null;
		
		            for(i=1;i<=NoOfSheetsInAPage;i++)
		            {
		            	System.out.println("Loop Number is:+++++++++++++++++++++++++++++++++++++++++++++++++ "+i);
		            	
		            	FileInputStream file = new FileInputStream("./TestExcel_Sheet/wrong_OCR_090119.xls"); 
		                HSSFWorkbook workbook = new HSSFWorkbook(file);
		                HSSFSheet sheet = workbook.getSheetAt(0);
		                
		            	Log.message("Loop Number is: "+i);
                        int noOfColumns = sheet.getRow(0).getLastCellNum();
		            	
		                String[] Headers = new String[noOfColumns];    
		            	
		            	//======Reading File Value from Application =============================
		            	
		            	if(Exp_SheetNo != null &&Exp_SheetNo!="" ) {
		            		
		            	
		            		Exp_SheetNo = driver.findElement(By.xpath("(//input[@id='SheetName'])["+i+"]")).getAttribute("value");
		            	    } 
		            	else
		            	    			            	    
		            	    {
		            		
		            		
		            		for (int a=0;a<noOfColumns;a++)
			                {
			               	 	Headers[a] = sheet.getRow(0).getCell(a).getStringCellValue();
			               	 	
			               	 	if(Headers[a].equals("Field Appeared in the Application"))
			               	 	{                  	
			                   	
			               	 	ExcelReader.writeColoumandrow(xlsrdr,"QA", i,a,0); 
			                   	Log.message("SheetName value Not Extracted Properly: ");
			                   	break;
			                   }
			               	 	
			                }
		               	 	
		            	    }
		            	      
		            	if(Exp_DIscipline != null &&Exp_DIscipline!="" ) {
		            		
		            	
		            		Exp_DIscipline = driver.findElement(By.xpath("(//input[@id='Discipline'])["+i+"]")).getAttribute("value");
		            	
		            	 } 
		            	else
		            	{            	    
		            		for (int a=0;a<noOfColumns;a++)
			                {
			               	 	Headers[a] = sheet.getRow(0).getCell(a).getStringCellValue();
			               	 	//Log.message("Header Display value :" + Headers[a]);
			               	 	if(Headers[a].equals("Discipline appeared in the Application"))
			               	 	{                  	
			                   	
			               	 	ExcelReader.writeColoumandrow(xlsrdr,"QA", i,a,0); 
			                   	Log.message("Discipline value Not Extracted Properly: ");
			                   	break;
			                   }
			               	 	
			                }
		               	 	
		            	    }
		              
		            	if(Exp_DIscipline != null &&Exp_DIscipline!="" ) {
		                Exp_Description = driver.findElement(By.xpath("(//textarea[@class='form-control input-box txtDescription'])["+i+"]")).getAttribute("value");
		            	 } 
		            	else
			            	            	    
		            	 {
		            		
		            		for (int a=0;a<noOfColumns;a++)
			                {
			               	 	Headers[a] = sheet.getRow(0).getCell(a).getStringCellValue();
			               	 	//Log.message("Header Display value :" + Headers[a]);
			               	 	if(Headers[a].equals("Sheet Title"))
			               	 	{                  	
			                   	
			               	 	ExcelReader.writeColoumandrow(xlsrdr,"QA", i,a,0); 
			                   	Log.message("Excelsheet value Not Extracted Properly: ");
			                   	break;
			                   }
			               	 	
			                }
		               	 	
		            	    }
		               
		                
		                   
		         
		                for (int a=0;a<noOfColumns;a++)
		                {
		               	 	Headers[a] = sheet.getRow(0).getCell(a).getStringCellValue();
		               	 	//Log.message("Header Display value :" + Headers[a]);
		               	 	if(Headers[a].equals("Sheet Title"))
		               	 	{                  	
		                   	
		                   	 Sheettitle =sheet.getRow(i).getCell(a).getStringCellValue(); 
		                   	Log.message("Excelsheet value Exp_SheetNo is: "+Sheettitle);
		                   	break;
		                   }
		               	 	
		                }
		               	 	
		                for (int a=1;a<noOfColumns;a++)
		                {
		               	 	Headers[a] = sheet.getRow(0).getCell(a).getStringCellValue();
		               	 	//Log.message("Header message Display:" + Headers[a]);
		               	 	if(Headers[a].equals("Field Appeared in the Application"))
		               	 	{                  	
		                   	
		               	 	ExcelReader.writeColoumandrow(xlsrdr, Exp_SheetNo, i,a,0);
		                   	 
		                   	//Log.message("Excelsheet value Field Appeared in the Application is: "+Sheettitle);
		                   	break;
		                   }
		               	 	
		                }	
		                
		                
		                //====================================================================
		                for (int a=0;a<noOfColumns;a++)
		                {
		               	 	Headers[a] = sheet.getRow(0).getCell(a).getStringCellValue();
		               	 	//Log.message("Header Display value :" + Headers[a]);
		               	 	if(Headers[a].equals("Sheet Discipline"))
		               	 	{                  	
		                   	
		               	 	SheetDiscipline =sheet.getRow(i).getCell(a).getStringCellValue(); 
		                   	Log.message("Excelsheet value Exp_SheetNo is: "+SheetDiscipline);
		                   	break;
		                   }
		               	 	
		                }
		               	 	
		                for (int a=1;a<noOfColumns;a++)
		                {
		               	 	Headers[a] = sheet.getRow(0).getCell(a).getStringCellValue();
		               	 	if(Headers[a].equals("Discipline appeared in the Application"))
		               	 	{                  	
		                   	
		               	 	ExcelReader.writeColoumandrow(xlsrdr, Exp_DIscipline, i,a,0);
		                   	 
		                   //	Log.message("Excelsheet  Field Value  Appeared in the Application is: "+Sheettitle);
		                   	break;
		                   }
		               	 	
		                }	
		                //===============================================================================
		                
		                //====================================================================
		                for (int a=0;a<noOfColumns;a++)
		                {
		               	 	Headers[a] = sheet.getRow(0).getCell(a).getStringCellValue();
		               	 	if(Headers[a].equals("Sheet Description"))
		               	 	{                  	
		                   	
		               	 	SheetDescription =sheet.getRow(i).getCell(a).getStringCellValue(); 
		                   	Log.message("Excelsheet value Exp_SheetNo is: "+SheetDescription);
		                   	break;
		                   }
		               	 	
		                }
		               	 	
		                for (int a=1;a<noOfColumns;a++)
		                {
		               	 	Headers[a] = sheet.getRow(0).getCell(a).getStringCellValue();
		               	 	//Log.message("Header message Display:" + Headers[a]);
		               	 	if(Headers[a].equals("Appeared in the application sheet description"))
		               	 	{                  	
		                   	
		               	 	ExcelReader.writeColoumandrow(xlsrdr, Exp_Description, i,a,0);
		                   	 
		                   	//Log.message("Excelsheet  Field Value  Appeared in the Application is: "+Sheettitle);
		                   	break;
		                   }
		               	 	
		                }	
		                //===============================================================================
		                
		                
		                //=============================================================================
		               	 //Comparing app value and excel value
		               	 if(Sheettitle.trim().equalsIgnoreCase((Exp_SheetNo).trim()))
		          		 {
		               		 Log.message("Comparision Between Excel And Application file : + PASSED");
		          			
		          			 for (int a=2;a<noOfColumns;a++)
				                {
				               	 	Headers[a] = sheet.getRow(0).getCell(a).getStringCellValue();
				               	 	
				               	 	if(Headers[a].equals("Status if B=C then Pass else fail"))
				               	 	{                  	
				                   	
				               	 	ExcelReader.writeColoumandrow(xlsrdr, "PASSED", i,a,0);
				                   	 
				               	 	
				                   	break;
				                   }
				               	 	
				                }	
		          			
		          		 }
		          		 else
		          		 {
		          			
		          			Log.message("Comparision Between  Excel And Application file : + FAILED");
		                   
		          			for (int a=2;a<noOfColumns;a++)			          				
		          				
			                {
			               	 	Headers[a] = sheet.getRow(0).getCell(a).getStringCellValue();
			               	 	//Log.message("Header Display value 3 message :" + Headers[a]);
			               	 	if(Headers[a].equals("Status if B=C then Pass else fail"))
			               	 	{                  	
			                   	
			               	 	ExcelReader.writeColoumandrow(xlsrdr, "FAILED", i,a,0);
			               	 	
			                   	break;
			                   }
			               	 					               	 	
			               	 	
			               	 	
			                }
		          			
		          		 }
		            
		          //=================== Comparision for Disclipne=========================  
		               	 
		          //======================================================================
		            if(SheetDiscipline.trim().equalsIgnoreCase((Exp_DIscipline).trim()))
	          		 {
	               		 Log.message("Comparision Between Excel And Application file : + PASSED");
	          			
	          			 for (int a=2;a<noOfColumns;a++)
			                {
			               	 	Headers[a] = sheet.getRow(0).getCell(a).getStringCellValue();
			               	 	
			               	 	if(Headers[a].equals("Status If D=E then Pass else Fail"))
			               	 	{                  	
			                   	
			               	 	ExcelReader.writeColoumandrow(xlsrdr, "PASSED", i,a,0);
			                   	 
			               	 	
			                   	break;
			                   }
			               	 	
			                }	
	          			
	          		 }
	          		 else
	          		 {
	          			
	          			Log.message("Comparision Between  Excel And Application file : + FAILED");
	                   
	          			for (int a=2;a<noOfColumns;a++)			          				
	          				
		                {
		               	 	Headers[a] = sheet.getRow(0).getCell(a).getStringCellValue();
		               	 	
		               	 	if(Headers[a].equals("Status If D=E then Pass else Fail"))
		               	 	{                  	
		                   	
		               	 	ExcelReader.writeColoumandrow(xlsrdr, "FAILED", i,a,0);
		               	 	
		                   	break;
		                   }
		               	 					               	 	
		               	 	
		               	 	
		                }
	          			
	          		 }
		            
		            
		            //=================== Comparision for Description=========================  
		            //=========================================================================
		            if(SheetDescription.trim().equalsIgnoreCase((Exp_Description).trim()))
	          		 {
	               		 Log.message("Comparision Between Excel And Application file : + PASSED");
	          			
	          			 for (int a=2;a<noOfColumns;a++)
			                {
			               	 	Headers[a] = sheet.getRow(0).getCell(a).getStringCellValue();
			               	 	
			               	 	if(Headers[a].equals(" Status If G=H then Pass else Fail"))
			               	 	{                  	
			                   	
			               	 	ExcelReader.writeColoumandrow(xlsrdr, "PASSED", i,a,0);
			                   	 
			               	 	
			                   	break;
			                   }
			               	 	
			                }	
	          			
	          		 }
	          		 else
	          		 {
	          			
	          			Log.message("Comparision Between  Excel And Application file : + FAILED");
	                   
	          			for (int a=2;a<noOfColumns;a++)			          				
	          				
		                {
		               	 	Headers[a] = sheet.getRow(0).getCell(a).getStringCellValue();
		               	 
		               	 	if(Headers[a].equals(" Status If G=H then Pass else Fail"))
		               	 	{                  	
		                   	
		               	 	ExcelReader.writeColoumandrow(xlsrdr, "FAILED", i,a,0);
		               	 	
		                   	break;
		                   }
		               	 					               	 	
		               	 	
		               	 	
		                }
	          			
	          		 }
		            
		            
		            
		            
		            
		            }
		            
		            
		            
					return result;}
		               	 	
		               	 	
    		 
		                
		                
		                
        
		                  	
		                
		                
		                
						
		                


		                



	
		



public static  String MultipleReadExcel(String Applicationvalue) throws BiffException, IOException 
{	
	
	boolean result = false;

	FileInputStream file = new FileInputStream("./TestExcel_Sheet/wrong_OCR_090119.xls"); 
    HSSFWorkbook workbook = new HSSFWorkbook(file);
    HSSFSheet sheet = workbook.getSheetAt(0);
    
    int noOfColumns = sheet.getRow(0).getLastCellNum();
	Log.message("No of colm display :" + noOfColumns);
    String[] Headers = new String[noOfColumns];       
    
   
   
   // System.out.println(Headers);       
    for (int a=0;a<noOfColumns;a++){
   	 Headers[a] = sheet.getRow(0).getCell(a).getStringCellValue();
   	Log.message("Header Display value :" + Headers[a]);
       if(Headers[a].equals("Sheet Title")){                  	
       	 for(int i=1;i<=20;i++)
       	{
       	
       	String Sheettitle =sheet.getRow(i).getCell(a).getStringCellValue(); 
     
       //	System.out.println(Sheettitle);
       
       	
        if(Sheettitle.trim().contains((Applicationvalue).trim()))
		 {
			 Log.message("Read excel file : + Pass");
			System.out.println(Sheettitle);
			Log.message("sheet Title Display value :" + Sheettitle);
			System.out.println(Applicationvalue);
			
			
			 
		//ExcelReader.writeColoumandrow(xlsrdr, "PASSED", 1,2,0);
		 }
		 else
		 {
			
			 Log.message("Read excelsheet file value :"+ Sheettitle);
        
         Log.message("Read Application file value :"+ Applicationvalue);
			 System.out.println(Applicationvalue);
			 
		//ExcelReader.writeColoumandrow(xlsrdr, "FAILED", 1,2,0);
		 }
       	
       	
       	}
    }}
	return Applicationvalue;
	

}

















	

}
