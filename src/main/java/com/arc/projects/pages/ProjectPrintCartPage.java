package com.arc.projects.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.TimeZone;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arc.projects.utils.randomFileName;
import com.arcautoframe.utils.Log;
import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.steadystate.css.dom.Property;

public class ProjectPrintCartPage extends LoadableComponent<ProjectPrintCartPage>
{
	
	WebDriver driver;
	private boolean isPageLoaded;
	
	@FindBy(xpath="//*[@id='breadCrumb']/div/ul/li[2]")
	WebElement breadcrumbPrintCart;
	
	@FindBy(xpath="//*[text()='Back to files']")
	WebElement btnBackToFiles;
	
	@FindBy(xpath="//*[text()='Remove all']")
	WebElement btnRemoveAll;
	
	@FindBy(xpath="//*[@id='confirmmodal']/div/div/div/div/div/button[2]")
	WebElement btnYes;
	
	@FindBy(xpath=".//*[@id='confirmmodal']/div/div/div/div/div/button[1]")
	WebElement btnNo;
	
	@FindBy(xpath=".//*[@id='hrefCartLink']/a")
	WebElement btnPrintCart;
	
	@FindBy(xpath=".//*[@id='Cartcount']")
	WebElement cartItemsCount;
	
	@FindBy(xpath="//*[@id='orddetail']/div[1]/div/div[3]/a")
	WebElement btnChangeDeliveryAddress;
	
	@FindBy(xpath="//*[text()='Add new address']")
	WebElement btnAddNewAddress;
	
	@FindBy(xpath="//*[text()='Add address']")
	WebElement popoverAddAddress;
	
	@FindBy(xpath="//*[@id='adressform']/form/div[1]/input")
	WebElement txtboxFullName;
	
	@FindBy(xpath="//*[@id='adressform']/form/div[2]/input")
	WebElement txtboxCompanyName;

	@FindBy(xpath="//*[@id='adressform']/form/div[3]/input")
	WebElement txtboxPhone;
	
	@FindBy(xpath="//*[@id='delvmail']/input")
	WebElement txtboxEmail;
	
	@FindBy(xpath="//*[@id='addressinput']")
	WebElement txtboxAddress;
	
	@FindBy(xpath="//*[@id='addressfooter']/button[2]")
	WebElement btnSaveAddress;
	
	@FindBy(xpath="//*[@class='phoneNum pointer']")
	WebElement phone;
	
	@FindBy(xpath="//*[@class='text-primary mailto pointer']")
	WebElement email;
	
	@FindBy(xpath="//*[@id='newaddress1']")
	WebElement address;
	
	@FindBy(xpath="//*[@id='orddetail']/div[1]/div/div[2]/div/div/p[2]")
	WebElement company;
	
	@FindBy(xpath="//*[@id='orddetail']/div[1]/div/div[2]/div/div/p[1]")
	WebElement name;
	
	@FindBy(xpath="//*[@class='btnUpload']")
	WebElement btnUploadFile;
	
	@FindBy(xpath="//*[text()='Profile']")
	WebElement btnProfile;
	
	@FindBy(xpath="//*[@class='icon icon-off']")
	WebElement btnLogout;
	
	@FindBy(xpath="//*[@id='button-1']")
	WebElement btnLogoutYes;
	
	@FindBy(xpath=".//*[@id='btnLogin']")
	WebElement btnLogin;
	
	 @FindBy(xpath="//*[@id='delvtype']/select")
		WebElement dropdownSelectDeliveryType;
		
		@FindBy(xpath="//*[@id='delvtype']/select/option[3]")
		WebElement valueWillCall;
		
		@FindBy(xpath="//*[@id='duedate']/a/span")
		WebElement btnDueDate;
		
		@FindBy(xpath="//*[@id='dailoge']/div[2]/span[2]/select")
		WebElement dropdownSelectTime;
		
		@FindBy(xpath=".//*[@id='dailoge']/div[3]/button")
		WebElement btnDone;
		
		@FindBy(xpath="//*[@id='setinstruction']/textarea")
		WebElement txtAreaDeliveryInstructions;
		
		@FindBy(xpath="//*[text()='Confirm order']")
		WebElement btnConfirmOrder;
		
	@Override
	protected void load() 
	{
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, breadcrumbPrintCart, 20);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		if (!isPageLoaded) 
		{
			Assert.fail();
		}
		

	}
	
	/**
	 * Declaring constructor for initializing web elements using PageFactory
	 * class.
	 * 
	 * @param driver
	 * @return
	 */
	public ProjectPrintCartPage(WebDriver driver) 
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	
	}

	
	/** 
	 * Method for validating addition of folder in the Print Cart page
	 * Scripted By: Ranadeep
	 *  
	 */
	public boolean folderAdditionInPrintCart(String filesCount) 
	{
		SkySiteUtils.waitForElement(driver, btnBackToFiles, 20);
		
		String folderData=driver.findElement(By.xpath("//*[@id='collapse1']/div/div/div[1]/div/div[2]/h4")).getText();
		
		Log.message("Name and files count of the added folder in cart is:- " +folderData);
		
		String expectedFolderName= PropertyReader.getProperty("FolderSelectPrintCart");
		Log.message("Expected folder name is:- " +expectedFolderName);
		
		if(folderData.contains(expectedFolderName) && folderData.contains(filesCount))
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}

	
	/** 
	 * Method to remove all items from Print Cart
	 * Scripted By: Ranadeep
	 *  
	 */
	public boolean removeAllCartItems() 
	{
		SkySiteUtils.waitForElement(driver, btnPrintCart, 20);
		
		SkySiteUtils.waitForElement(driver, btnRemoveAll, 20);
		btnRemoveAll.click();
		Log.message("Clicked on Remove All button");
		
		SkySiteUtils.waitForElement(driver, btnYes, 30);
		btnYes.click();
		Log.message("Clicked on yes option to remove all items from the cart");
		SkySiteUtils.waitTill(8000);
		
		if(!btnPrintCart.isDisplayed())
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}

	
	/** 
	 * Method to validate addition of files in the Print Cart
	 * Scripted By: Ranadeep
	 *  
	 */
	public boolean filesAdditionInPrintCart(int count, ArrayList<String> arr) 
	{
		
		SkySiteUtils.waitForElement(driver, btnPrintCart, 20);
		
		SkySiteUtils.waitTill(5000);
		ArrayList<String> obtainedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='col-sm-8']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFileList.add(we.getText().split("\\s")[0]); 
		     
			}
		}
		
		Log.message("Files count in cart is:- " +obtainedFileList.size());		 
		Log.message("Files in cart are:- " +obtainedFileList);
		
		if(obtainedFileList.equals(arr) && count == obtainedFileList.size())
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}

	
	/** 
	 * Method to validate individual deletion of files in the Print Cart
	 * Scripted By: Ranadeep
	 *  
	 */
	public boolean removeItemsIndividuallyFromCart() 
	{
		
		SkySiteUtils.waitForElement(driver, btnPrintCart, 20);
		
		SkySiteUtils.waitTill(5000);
		ArrayList<String> obtainedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='col-sm-8']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFileList.add(we.getText().split("\\s")[0]); 
		     
			}
		}
		
		Log.message("Files count in cart is:- " +obtainedFileList.size());		 
		Log.message("Files in cart are:- " +obtainedFileList);
		
		int count=obtainedFileList.size();
		
		for(int i=0; i<count; i++)
		{
			driver.findElement(By.xpath("//*[@class='icon icon-trash icon-lg']")).click();		
			Log.message("Clicked to delete the file "+obtainedFileList.get(i));
			SkySiteUtils.waitTill(5000);
			Log.message("Deleted file "+obtainedFileList.get(i));
			
		}
					
		if(!btnPrintCart.isDisplayed())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
	/** 
	 * Method to validate Back to File button click in Print Cart
	 * Scripted By: Ranadeep
	 *  
	 */
	public void backToFile() 
	{
		SkySiteUtils.waitForElement(driver, btnBackToFiles, 20);
		btnBackToFiles.click();
		Log.message("Clicked on back to files button");
		SkySiteUtils.waitTill(8000);
		
	}

	
	/** 
	 * Method to validate No Of Copies for individual items in the Cart
	 * Scripted By: Ranadeep
	 *  
	 */
	public boolean verifyNoOfCopiesForIndividualItemsInCart(int count, ArrayList<String> arr, int count1) 
	{
		
		SkySiteUtils.waitForElement(driver, btnRemoveAll, 20);
		
		SkySiteUtils.waitTill(5000);
		ArrayList<String> obtainedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='col-sm-8']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFileList.add(we.getText().split("\\s")[0]); 
		     
			}
		}
		
		Log.message("Files count in cart is:- " +obtainedFileList.size());		 
		Log.message("Files in cart are:- " +obtainedFileList);
		
		SkySiteUtils.waitForElement(driver, cartItemsCount, 30);
		String actualCartItemsCount = cartItemsCount.getText();
		int count3= Integer.parseInt(actualCartItemsCount);
		Log.message("Actual items in the cart is:- "+count3);
		
		ArrayList<String> noOfCopiesList = new ArrayList<>();
		
		for(int i=0; i<count; i++)
		{
						
			noOfCopiesList.add((driver.findElement(By.xpath("html/body/div[1]/div[3]/div[1]/app-root/orderdetails/div/div[2]/div/div[1]/ul/li/div[2]/div["+i+"+"+1+"]/div/div[2]/div/div/input")).getAttribute("value")));
			
			SkySiteUtils.waitTill(3000);
			
		}
		Log.message("List of the no of copies for" +obtainedFileList+ " added in print cart is " +noOfCopiesList+ " respectively");
		
		
		if(obtainedFileList.equals(arr) && count == count3)
		{
			for(int j=0; j<count; j++)
			{
				if(noOfCopiesList.get(j).equals(count1))
				break;
			}
			
			return true;
		}
		else
		{
			return false;
		}
		
		
		
	}

	
	
	/** 
	 * Method to add new delivery address in Print cart and validate
	 * Scripted By: Ranadeep
	 *  
	 */
	public boolean addNewDeliveryAddress() 
	{
		
		SkySiteUtils.waitForElement(driver, btnChangeDeliveryAddress, 30);
		btnChangeDeliveryAddress.click();
		Log.message("Clicked on the change delivery address button");
		
		SkySiteUtils.waitForElement(driver, btnAddNewAddress, 30);
		btnAddNewAddress.click();
		Log.message("Clicked on Add new Address button");
		SkySiteUtils.waitTill(5000);
		
		String FullName=null;
		String CompanyName=null;
		String PhoneNo=null;
		String EmailId=null;
		String Address=null;
		
		if(popoverAddAddress.isDisplayed())
		{
			SkySiteUtils.waitForElement(driver, txtboxFullName, 30);
			FullName = PropertyReader.getProperty("PCFULLNAME");
			txtboxFullName.clear();
			txtboxFullName.sendKeys(FullName);
			Log.message("Full Name entered is:- " +FullName);
			
			SkySiteUtils.waitForElement(driver, txtboxCompanyName, 30);
			CompanyName = PropertyReader.getProperty("PCCOMPANYNAME");
			txtboxCompanyName.clear();
			txtboxCompanyName.sendKeys(CompanyName);
			Log.message("Company name entered is:- " +CompanyName);
			
			SkySiteUtils.waitForElement(driver, txtboxPhone, 30);
			PhoneNo = PropertyReader.getProperty("PCPHONENO");
			txtboxPhone.clear();
			txtboxPhone.sendKeys(PhoneNo);
			Log.message("Phone no entered is:- " +PhoneNo);
			
			SkySiteUtils.waitForElement(driver, txtboxEmail, 30);
			EmailId = PropertyReader.getProperty("PCEMAIL");
			txtboxEmail.clear();
			txtboxEmail.sendKeys(EmailId);
			Log.message("Email id entered is:- " +EmailId);
			
			SkySiteUtils.waitForElement(driver, txtboxAddress, 30);
			Address = PropertyReader.getProperty("PCADDRESS");
			txtboxAddress.clear();
			txtboxAddress.sendKeys(Address);
			Log.message("Address entered is:- " +Address);
			
			SkySiteUtils.waitForElement(driver, btnSaveAddress, 30);
			btnSaveAddress.click();
			Log.message("Clicked on Save Address button");
			SkySiteUtils.waitTill(5000);
			
		}
		else
		{
			Log.message("Enter address popover opening failed");
		}
				

		SkySiteUtils.waitForElement(driver, name, 30);
		String newName= name.getText();
		Log.message("Saved name is:- " +newName);
		
		SkySiteUtils.waitForElement(driver, company, 30);
		String newCompany= company.getText();
		Log.message("Saved name is:- " +newCompany);
		
		SkySiteUtils.waitForElement(driver, address, 30);
		String newAddress= address.getText();
		Log.message("Saved address is:- " +newAddress);
		
		SkySiteUtils.waitForElement(driver, email, 30);
		String newEmail= email.getAttribute("title");
		Log.message("Saved email is:- " +newEmail);
		
		SkySiteUtils.waitForElement(driver, phone, 30);
		String newPhoneNo= phone.getAttribute("title");
		Log.message("Saved phone no is:- " +newPhoneNo);
		
		if( newName.equals(FullName) && newCompany.equals(CompanyName) && newAddress.equals(Address) && newEmail.equals(EmailId) && newPhoneNo.equals(PhoneNo) )
		{
			return true;
		}
		else
		{
			return false;
		}
			
	}

	
	
	/** 
	 * Method to upload Transmittal or Distribution Address Document in print Cart
	 * Scripted By: Ranadeep
	 * @return 
	 *  
	 */
	public boolean uploadTransmittalDocumentDistributionAddress() throws AWTException, IOException
	{
		      
        SkySiteUtils.waitForElement(driver, btnUploadFile, 30);
		WebElement element = driver.findElement(By.xpath("//*[@class='btnUpload']"));
	    JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].scrollIntoView();", element);
	    SkySiteUtils.waitTill(3000);
	    btnUploadFile.click();
		Log.message("Clicked on Upload File button to upload Transmittal or Distribution Address Document");
		SkySiteUtils.waitTill(6000);
		
		File fis=new  File(PropertyReader.getProperty("FolderPathFile2"));            
        String filepath=fis.getAbsolutePath().toString();
        File fis1=new  File(PropertyReader.getProperty("tempfilepath"));            
        String tempfilepath=fis1.getAbsolutePath().toString();   
        
		BufferedWriter output;                  
        randomFileName rn=new randomFileName();
        String tmpFileName=tempfilepath+rn.nextFileName()+".txt";                
        output = new BufferedWriter(new FileWriter(tmpFileName,true));
        
        String expFilename=null;
        File[] files = new File(filepath).listFiles();
        
        for(File file : files)
        {
              if(file.isFile()) 
              {
                  expFilename=file.getName();//Getting File Names into a variable
                  Log.message("Expected File name is:"+expFilename);  
                  output.append('"' + expFilename + '"');
                  output.append(" ");
                  SkySiteUtils.waitTill(5000);     
  
              }      
        }      
        
        output.flush();
        output.close();
        
        String AutoItexe_Path = PropertyReader.getProperty("AutoITPath");
        //Executing .exe autoIt file            
        Runtime.getRuntime().exec(AutoItexe_Path+" "+ filepath+" "+tmpFileName);                  

        Log.message("AutoIT Script Executed!!");                           
        SkySiteUtils.waitTill(20000);
                     
        try
        {
              File file = new File(tmpFileName);
              if(file.delete())
              {
                     Log.message(file.getName() + " is deleted!");
              }      
              else
              {
                     Log.message("Delete operation is failed.");
              }
        }
        catch(Exception e)
        {                    
              Log.message("Exception occured!!!"+e);
        } 
        
        SkySiteUtils.waitTill(8000);
        
        String actualFileName = PropertyReader.getProperty("Filename");
        Log.message("Actual file name is:- "+actualFileName);
        
        if( driver.findElement(By.xpath("//*[@id='orddetail']/div[3]/div/div/div/div[3]/div[1]/span")).getText().equals(actualFileName))
        {
        	return true;
        }
        else
        {
        	return false;
        }
       		
	}
	
	
	/** 
	 * Method written for Logout from Projects Print Cart
	 * Scripted By: Ranadeep
	 * @return
	 */
	public boolean logout() 
	{
		SkySiteUtils.waitForElement(driver, btnProfile, 20);
		btnProfile.click();
		Log.message("Clicked on Profile.");
		SkySiteUtils.waitForElement(driver, btnLogout, 20);
		SkySiteUtils.waitTill(3000);
		btnLogout.click();
		SkySiteUtils.waitForElement(driver, btnLogoutYes, 20);
		btnLogoutYes.click();
		SkySiteUtils.waitForElement(driver, btnLogin, 30);
		
		Log.message("Checking whether Login button is present?");
		if(btnLogin.isDisplayed())
			return true;
		else
			return true;
	}
	
	 

	/** 
	 * Method to Get The Current Day
	 * Scripted By: Ranadeep
	 * @return
	 */
    private String getCurrentDay()
    {
        //Create a Calendar Object
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
 
        //Get Current Day as a number
        int todayInt = calendar.get(Calendar.DAY_OF_MONTH);
        Log.message("Today Int: " + todayInt +"\n");
 
        //Integer to String Conversion
        String todayStr = Integer.toString(todayInt);
        Log.message("Today Str: " + todayStr + "\n");
 
        return todayStr;
    }
   
    @FindBy(xpath=".//*[@id='ordid']/strong")
    WebElement getOrderId;
    

  
    @FindBy(xpath="//*[@class='icon icon-money']")
    WebElement btnMyOrders;
	
	/** 
	 * Method written for Placing order and tracking in My Orders
	 * Scripted By: Ranadeep
	 * @return
	 */
	public boolean placePrintOrder() 
	{
		SkySiteUtils.waitForElement(driver, dropdownSelectDeliveryType, 30);
		dropdownSelectDeliveryType.click();
		Log.message("Clicked on select delivery type dropdown" );
		SkySiteUtils.waitForElement(driver, valueWillCall, 30);
		valueWillCall.click();
		String deliveryType = driver.findElement(By.xpath("//*[@id='delvtype']/select/option[3]")).getText();
		Log.message("Delivery type selected is:- " +deliveryType);
		
		SkySiteUtils.waitForElement(driver, btnDueDate, 30);
		btnDueDate.click();
		Log.message("Clicked on select due date button");
		String today = getCurrentDay();
        Log.message("Today's number: " + today + "\n");
        
        /** Body of calendar is handled */
        WebElement dateWidgetFrom = driver.findElement(By.xpath(".//*[@id='dailoge']/div[1]/div[2]"));
        List<WebElement> collumn = dateWidgetFrom.findElements(By.xpath("//*[@class='dayhight']"));
        
        /** DatePicker is a table. Thus we can navigate to each cell and if a cell matches with the current date then we will click it */
        for (WebElement cell: collumn) 
        {
     
            if (cell.getText().equals(today)) 
            {
                cell.click();
                Log.message("Today day is selected");
                break;
            }
        }
        Log.message("Date has been selected from the calendar");
        
        SkySiteUtils.waitForElement(driver, dropdownSelectTime, 30);
        dropdownSelectTime.click();
        Log.message("Clicked on the select time dropdown");
        SkySiteUtils.waitTill(4000);
        
        driver.findElement(By.xpath(".//*[@id='dailoge']/div[2]/span[2]/select/option[9]")).click();
        Log.message("Option 9 is selected in time dropdown");
        
        SkySiteUtils.waitForElement(driver, dropdownSelectTime, 30);
        btnDone.click();
        Log.message("Clicked on Done button to set date and time");
        
        SkySiteUtils.waitForElement(driver, txtAreaDeliveryInstructions, 30);
		String DeliveryInstruction = PropertyReader.getProperty("DELIVERYINSTRUCTION");
		txtAreaDeliveryInstructions.clear();
		txtAreaDeliveryInstructions.sendKeys(DeliveryInstruction);
		Log.message("Entered Delivery Instruction is:- " +DeliveryInstruction);
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnConfirmOrder, 30);
		btnConfirmOrder.click();
		Log.message("Clicked on Confirm Order button");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, getOrderId, 30);
		String orderId =  getOrderId.getText();
		Log.message("Placed order id is:- "+orderId);
		
		SkySiteUtils.waitForElement(driver, btnProfile, 20);
		btnProfile.click();
		Log.message("Clicked on Profile");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnMyOrders, 30);
		WebElement element = driver.findElement(By.xpath("//*[@class='icon icon-money']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		//btnMyOrders.click();
		Log.message("Clicked on My Orders Button");
		SkySiteUtils.waitTill(6000);
		
		if(driver.findElement(By.xpath("//*[@id='mainPageContainer']/div[3]/div[1]/app-root/ng-component/div/div[2]/div/div/div/div/div[1]/div[1]/div/div[2]/h4/strong")).getText().contains(orderId))
		{
			return true;
		}
		else 
		{
			return false;
		}
		
	}
}
