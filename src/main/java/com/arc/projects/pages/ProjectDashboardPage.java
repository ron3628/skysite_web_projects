package com.arc.projects.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arcautoframe.utils.Log;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arc.projects.utils.randomFileName;

import com.arc.projects.pages.FolderPage;


public class ProjectDashboardPage  extends LoadableComponent<ProjectDashboardPage> 
{

	WebDriver driver;
	private boolean isPageLoaded;
	
	ProjectDashboardPage projectDashboardPage;
	
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	
	@FindBy(css=".btn.btn-primary.project-tab.active")
	WebElement btnProjectDashboard;
	
	@FindBy(css=".dropdown.sec-pref.open>a")
	WebElement btnLogOff;
	
	 //@FindBy(css="div.modal-footer .btn.btn-default")
	@FindBy(xpath=".//*[@id='WhatsNewFeature']/div/div/div[3]/button[1]")
	WebElement btnSkip;

	
	//@FindBy(css=".close")
	@FindBy(xpath=".//*[@id='WhatsNewFeature']/div/div/div[1]/button")
	WebElement btnClose;
	
	@FindBy(css=".ltmenu-user.badge-stack.dropdown>a")
	WebElement btnProfile;
	
	@FindBy(css="#txtSearchKeyword")
	WebElement txtGlobalSearch;
	
	@FindBy(css="#btnSearch")
	WebElement bunSearch;
	
	@FindBy(xpath="//span[contains(text(),'Create project')]")
	WebElement btnCreateProject;
	
	@FindBy(css="#btnCreate")	
	private WebElement btnCreate;
	
	@FindBy(id="txtProjectName")
	//@CacheLookup - Removed due to fail
	private WebElement txtProjectName;
	
	@FindBy(css="#txtProjectNumber")
	WebElement txtProjectNumber;
	
	@FindBy(xpath=".//*[@id='prj-start-date']/span")
	WebElement butProjectStartDate;
	
	@FindBy(css="#txtProjectDescription")
	WebElement txtProjectDescription;
	
	@FindBy(css="#txtAddress")
	WebElement txtAddress;
	
	@FindBy(css="#txtProjectStartDate")
	WebElement txtProjectStartDate;
	
	@FindBy(css="#txtCity")
	WebElement txtCity;
	
	@FindBy(css="#txtState")
	WebElement txtState;
	
	@FindBy(css="#txtZip")
	WebElement txtZip;
	
	@FindBy(css="#txtCountry")
	WebElement txtCountry;
	
	@FindBy(css="#txtProjectPassword")
	WebElement txtProjectPassword;
	
	@FindBy(css=".noty_text")
	WebElement projectCreationSuccessMsg;
	
	@FindBy(css=".Country-Item[title='USA']")
	WebElement countryTitleItem;
	
	@FindBy(css=".State-Item[data-name='alabama']")
	WebElement stateTitleItem;
	
	@FindBy(xpath="//input[@class='ahRev']")
	WebElement checkboxEnableAutoHypLink;
	
	@FindBy(xpath="(//button[@class='btn btn-default pull-right'])[4]")
	WebElement btnSavePrjSettingsWin;
	
	@FindBy(css="#button-1")
	WebElement btnYes;
	
	@FindBy(css=".popover-content")
	WebElement contentPasswordField;
	
	@FindBy(xpath=".//*[@id='breadCrumb']/div[2]/ul/li[3]")
	WebElement prjNameBreadcrum;
	
	//Export Related Elements
	@FindBy(css="#aPrivateProjects")
	WebElement PrivateProjectsTab;
	
	@FindBy(css="#aFavProjects")
	WebElement FavProjectsTab;
	
	@FindBy(xpath="//small[@id='favoriteProjCounter']")
	WebElement tabFavoriteCount;
	
	@FindBy(css="#aGuestProjects")
	WebElement GuestProjectsTab;
	
	@FindBy(xpath="//*[contains(@class, 'project-icon')]")//naresh
	WebElement All_Proj_Name;
	
	@FindBy(xpath="//a[@data-bind='click: ClickExportProjectEvent, clickBubble: false']")
	WebElement ProjLev_ExptoCSV_Button;
	
	@FindBy(xpath="//i[@class='icon icon-more-option']")//Naresh
	WebElement Prj_MoreOptions;
	
	@FindBy(xpath="//a[contains(text(),'Edit')])[1]")
	WebElement Edit_Tab_FirstPrj;
	
	@FindBy(css=".last")
	WebElement ProjName_Breadcrumb;
	
	//Contacts button
	@FindBy(css=".icon.icon-contacts.icon-lg.pw-icon-white.pulse.animated")//Naresh
	WebElement btnContacts;
	
	@FindBy(css="#add-contact")
	WebElement btnAddNewContact;
	
	@FindBy(xpath=".//*[@id='PName_1KPavDU1%40lpwCCYHX5dXQg%3d%3d']/span")
	WebElement projectclick;
	
	@FindBy(css=".icon.icon-publishing-log")
	WebElement remainingproject;
	
	@FindBy(xpath=".//*[@id='ulac']/li[1]/div[1]/div[3]/div[2]/a[1]")
	WebElement PublishButton;
	
	@FindBy(xpath="//a[contains(text(), 'Sign out')]")
	WebElement btnLogout;

	@FindBy(xpath="//*[@id='txtSearchKeyword']")
	WebElement GlobalSearch;
	
	@FindBy(css="#btnLogin")
	WebElement btnLogin;
	
	@FindBy(xpath="//*[@id='btnSearch']")
	WebElement ButtonSearch;
	
	
	
	@FindBy(css="#liRFI > a")
	WebElement RFITab;
	
	@FindBy(css="#liSortSetting>a")
	WebElement PreferenceTab;
	
	@FindBy(css="#txtStartRFINo")
	WebElement RFI_Inbox;	
	
	@FindBy(xpath="//*[@id='txtRFIDays']")
	WebElement Noofdays;
	
	
	
	@FindBy(xpath="(//input[@class='form-control' and@placeholder='Add custom attribute'])[1]")
	WebElement CustomAttribute_inbox1;
	
	@FindBy(xpath="(//input[@class='form-control' and@placeholder='Add custom attribute'])[2]")
	WebElement CustomAttribute_inbox2;
	
	@FindBy(xpath="(//input[@class='form-control' and@placeholder='Add custom attribute'])[3]")
	WebElement CustomAttribute_inbox3;	
	
	@FindBy(xpath="(//input[@class='form-control' and@placeholder='Add custom attribute'])[4]")
	WebElement CustomAttribute_inbox4;
	
	@FindBy(xpath="(//input[@class='form-control' and@placeholder='Add custom attribute'])[5]")
	WebElement CustomAttribute_inbox5;
	
	@FindBy(xpath="(//button [@id='btnAddPropRFI'])[1]")
	WebElement CustomAttribute_PlusButton1;
	
	@FindBy(xpath="(//button [@id='btnAddPropRFI'])[2]")
	WebElement CustomAttribute_PlusButton2;
	
	@FindBy(xpath="(//button [@id='btnAddPropRFI'])[3]")
	WebElement CustomAttribute_PlusButton3;
	
	@FindBy(xpath="(//button [@id='btnAddPropRFI'])[4]")
	WebElement CustomAttribute_PlusButton4;
	
	@FindBy(xpath="(//button [@id='btnAddPropRFI'])[5]")
	WebElement CustomAttribute_PlusButton5;
	
	
	@FindBy(xpath="//*[@id='RFI']/div[1]/div[1]/ul/li")
	WebElement CustomAttribute_Common;
	
	

	@FindBy(xpath="//*[@id='btnRFISave']")
	WebElement btnSaveRFI;
	
	@FindBy(xpath=".//*[@id='SortSettings']/div[2]/div/button")
	WebElement btnSavePreference;
	
	
	//Footer Links
	@FindBy(css=".download-app>small")
	WebElement linkDownloadSync;
	
	@FindBy(css=".win-app")
	WebElement btnDownloadSync32bit;
	
	@FindBy(css=".win-app-64")
	WebElement btnDownloadSync64bit;
	
	@FindBy(css=".app-help>small")
	WebElement linkFAQ;
	
	@FindBy(xpath="(//a[@class='help-faqs active'])[1]")
	WebElement objFAQsWindowHeadLine;
	
	@FindBy(xpath="(//button[@class='close' and @aria-label='Close'])[1]")
	WebElement btnCloseFAQs;
	
	@FindBy(css=".support>small")
	WebElement linkSupport;
	
	@FindBy(css=".btn.btn-primary.temp-access-btn.btn-sm")
	WebElement btnProvideTempAccessToSupportTeam;
	
	@FindBy(xpath="(//button[@class='close' and @aria-label='Close'])[3]")
	WebElement btnCloseSupportWindow;
	
	@FindBy(css=".feedback>small")
	WebElement linkFeedback;
	
	@FindBy(xpath="//button[@id='btnCancel' and @class='btn btn-default tabindex']")
	WebElement btnCancelFeedback;
	
	@FindBy(css="#txtAreaDesc")
	WebElement txtFeedback;
	
	@FindBy(css="#btnFeedback")
	WebElement btnSubmitFeedback;
	
	@FindBy(xpath="(//i[@class='icon icon-off'])[1]")
	WebElement linkLogoutFooter;
	
	//Send Files Module
	//@FindBy(xpath="//span[contains(text(),'Send files')]")
	@FindBy(xpath="//a[@class='btn btn-primary' and @data-original-title='Send files']")
	WebElement iconSendFiles;
	
	@FindBy(css="#send-file")
	WebElement btnSend;
	
	@FindBy(xpath="//div[@id='fine-uploader']/div/div[2]/input")
	WebElement btnChooseFiles;
	
	@FindBy(css="#txtTo")
	WebElement textToEmail;
	
	@FindBy(xpath="//textarea[@class='emailinputdiv']")
	WebElement textToEmailEnter;
	
	@FindBy(css="#txtSubject")
	WebElement textSubject;
	
	@FindBy(css="#txtMessage")
	WebElement txtMessage;
	
	@FindBy(css="#Tracking")
	WebElement tabTracking;
	
	@FindBy(css=".tracking-details")
	WebElement txtTrackingDetails;
	
	@FindBy(xpath=".//*[@id='tracking-details-modal']/div/div/div[1]")
	WebElement windDetailsofTrackId;
	
	@FindBy(css="#Inbox")	
	WebElement tabSendfilesInbox;
	
	@FindBy(xpath="(//a[@id='sender-details-inbox'])[1]")	
	WebElement txtOrderInbox;
	
	@FindBy(id="sender-details-inbox")	
	WebElement headerOrderDetailsWin;
	
	@FindBy(css="#order-print")	
	WebElement btnOrderPrintsSubmit;
	
	@FindBy(css=".panel-body.has-tooltip>a")	
	WebElement dataServiceProviderMail;
	
	@FindBy(css="#PrintQueue")
	WebElement dataPrintQueue;
	
	@FindBy(css="#txtJobName")	
	WebElement txtJobName;
	
	@FindBy(css="#txtDelInstruction")
	WebElement txtDelInstruction;
	
	@FindBy(css=".btn.btn-warning.print")
	WebElement btnPrintOrderConfPage;
	
	@FindBy(css=".col-md-10>h3")
	WebElement txtOrderConfMsg;
	
	@FindBy(xpath="//span[@class='img-circle profile-no-image md']")	
	WebElement imgProfile;
	
	@FindBy(xpath="//a[contains(text(),'My orders')]")
	WebElement tabMyOrders;
	
	@FindBy(css="#SortByColumnName")
	WebElement tabMyOrdersSort;
	
	@FindBy(xpath="//*[@id='createNewProjectWelcome']")
	WebElement btnCreateNewProjectWelcome;
	
	@FindBy(xpath="//*[@id='closeWelcomeVideo']")
	WebElement btnCloseWelcomeVideo;
	
	@FindBy(xpath="//*[@id='SortByColumnName']")
	WebElement btnProjectSorting;
	
	@FindBy(xpath="//*[@id='aPrivateProjects']")
	WebElement btnMyProject;
	
	@FindBy(xpath="//*[@id='liSorting']/a[1]")
	WebElement btnSortAscending;

	@FindBy(xpath="//*[@id='liSorting']/a[2]")
	WebElement btnSortDescending;
	
	@FindBy(id="ulProjectList")
	List<WebElement> projectlist;
	
	@FindBy(id="ProjectlistContainer")
	List<WebElement> ProjectList;
	
	@FindBy (xpath="//*[@id='sort-ProjectNumber']")
	WebElement sortByProjectNumber;
	
	@FindBy (xpath="//*[@id='sort-ProjectName']")
	WebElement sortByProjectName;

	@FindBy (xpath="//*[@id='sort-ProjectOwner']")
	WebElement sortByProjectOwner;

		
	
	@Override
	protected void load() {
		isPageLoaded = true;
		//SkySiteUtils.waitForElement(driver, btnLogOff, 20);
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		
	}
	
	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * @param driver
	 */
	public ProjectDashboardPage(WebDriver driver) {
		   this.driver = driver;
		   PageFactory.initElements(this.driver, this);
		 }
	
	/** 
	 * Method written for checking the availability of Profile button.
	 * @return
	 */
	public boolean presenceOfProfileButton()
	{
		SkySiteUtils.waitTill(8000);
		String Parent_Window = driver.getWindowHandle(); 
		for (String Child_Window : driver.getWindowHandles())  
	     {  
	     driver.switchTo().window(Child_Window); 
	     JavascriptExecutor js = (JavascriptExecutor)driver;
	     Boolean btnCloseIsPresent = driver.findElements(By.xpath(".//*[@id='WhatsNewFeature']/div/div/div[1]/button")).size()>0;
	     if(btnCloseIsPresent.equals(true))
	     {
	     js.executeScript("arguments[0].click();", btnClose);
	     Log.message("Modal window is closed.");
	     }
	    /* else
	     {
	     SkySiteUtils.waitTill(10000);
	 	 driver.switchTo().window(Parent_Window); 
	     } */
	     }
		SkySiteUtils.waitForElement(driver, btnProfile, 20);
		Log.message("Checking whether Profile button is present?");
		if(btnProfile.isDisplayed())
			return true;
		else
			return true;
		
	}
	
	/** 
	 * Method written for Logout from Projects.
	 * Scripted By: Naresh
	 * @return
	 */
	public boolean Logout_Projects()
	{
		SkySiteUtils.waitForElement(driver, btnProfile, 20);
		btnProfile.click();
		Log.message("Clicked on Profile.");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, btnLogout, 20);
		SkySiteUtils.waitTill(2000);
		btnLogout.click();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnYes, 20);
		btnYes.click();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnLogin, 60);
		Log.message("Checking whether Login button is present?");
		if(btnLogin.isDisplayed())
			return true;
		else
			return true;
		
	}

	

	
	/** 
	 * Method written for creating new project.
	 * Scripted By: Naresh
	 * @return
	 */
	public FolderPage createProject(String Project_Name)
	{
		SkySiteUtils.waitForElement(driver, btnCreateProject,60);
		SkySiteUtils.waitTill(3000);
		btnCreateProject.click();
		Log.message("Create Project button is clicked.");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, txtProjectName,60);
		txtProjectName.clear();
		txtProjectName.sendKeys(Project_Name);
		Log.message("Project Name has been entered as ."+Project_Name);
		txtProjectNumber.clear();
		txtProjectNumber.sendKeys(PropertyReader.getProperty("Projectnumber"));
		Log.message("Project Number has been entered.");
		txtProjectDescription.clear();
		txtProjectDescription.sendKeys(PropertyReader.getProperty("Projectdescription"));
		Log.message("Project description has been entered.");
		//txtProjectStartDate.clear();
		txtProjectStartDate.sendKeys(PropertyReader.getProperty("Projectstartdate"));
		Log.message("Project start date has been entered.");
		txtAddress.clear();
		txtAddress.sendKeys(PropertyReader.getProperty("Siteaddress"));
		Log.message("Site Address has been entered.");
		/*txtCountry.clear();
		txtCountry.sendKeys(PropertyReader.getProperty("Country"));
		SkySiteUtils.waitForElement(driver, txtCountry,60);
		countryTitleItem.click();
		Log.message("Country has been selected.");
		SkySiteUtils.waitTill(3000);
		txtState.clear();
		SkySiteUtils.waitTill(3000);
		txtState.sendKeys(PropertyReader.getProperty("State"));
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, stateTitleItem, 60);
		stateTitleItem.click();
		Log.message("State has been selected.");*/
		txtCity.clear();
		txtCity.sendKeys(PropertyReader.getProperty("City"));
		Log.message("City has been entered.");
		txtZip.clear();
		txtZip.sendKeys(PropertyReader.getProperty("Zip"));
		Log.message("Zip has been entered.");
		SkySiteUtils.waitTill(2000);
		btnCreate.click();
		Log.message("Create Project button has been clicked.");
		/*SkySiteUtils.waitForElement(driver, projectCreationSuccessMsg, 60);
		String expectedProjSuccessMsg = projectCreationSuccessMsg.getText();
		Log.message(expectedProjSuccessMsg);
		String actualProjSuccessMsg = "Project created successfully";*/
		SkySiteUtils.waitTill(7000);
		SkySiteUtils.waitForElement(driver, checkboxEnableAutoHypLink,120);
		SkySiteUtils.waitTill(2000);
		checkboxEnableAutoHypLink.click();
		Log.message("Clicked on enable auto hyperlink checkbox.");
		SkySiteUtils.waitTill(2000);
		btnSavePrjSettingsWin.click();
		Log.message("Clicked on save button of project settings window.");
		SkySiteUtils.waitForElement(driver, btnYes, 60);
		SkySiteUtils.waitTill(2000);
		btnYes.click();
		SkySiteUtils.waitForElement(driver, prjNameBreadcrum, 60);
		SkySiteUtils.waitTill(3000);
		if(prjNameBreadcrum.getText().equalsIgnoreCase(Project_Name))
		Log.message("Project Created Successfully with name : "+Project_Name);
			return new FolderPage(driver).get();
	}
	
	/** 
	 * Method written for creating new project.
	 * Scripted By:Ranjan
	 * @return
	 */
	public FolderPage CreateProject(String Project_Name)
	{   SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnCreateProject,60);		
		btnCreateProject.click();
		Log.message("Create Project button is clicked.");
		SkySiteUtils.waitForElement(driver, txtProjectName,60);
		txtProjectName.clear();
		SkySiteUtils.waitForElement(driver, txtProjectName,60);
		txtProjectName.sendKeys(Project_Name);
		Log.message("Project Name has been entered as ."+Project_Name);
		SkySiteUtils.waitForElement(driver, txtProjectNumber,100);
		txtProjectNumber.clear();
		SkySiteUtils.waitForElement(driver, txtProjectNumber,100);
		txtProjectNumber.sendKeys(PropertyReader.getProperty("Projectnumber"));
		Log.message("Project Number has been entered.");
		txtProjectDescription.clear();
		SkySiteUtils.waitForElement(driver, txtProjectDescription,100);
		txtProjectDescription.sendKeys(PropertyReader.getProperty("Projectdescription"));
		Log.message("Project description has been entered.");
		SkySiteUtils.waitForElement(driver, txtProjectStartDate,100);
		txtProjectStartDate.sendKeys(PropertyReader.getProperty("Projectstartdate"));
		Log.message("Project start date has been entered.");
		txtAddress.clear();
		SkySiteUtils.waitForElement(driver, txtAddress,100);
		txtAddress.sendKeys(PropertyReader.getProperty("Siteaddress"));
		Log.message("Site Address has been entered.");		
		txtCity.clear();
		SkySiteUtils.waitForElement(driver, txtCity,100);
		txtCity.sendKeys(PropertyReader.getProperty("City"));
		Log.message("City has been entered.");
		
				
		
	    /*txtState.clear();
		SkySiteUtils.waitForElement(driver, txtState,100);
		txtState.sendKeys(PropertyReader.getProperty("State"));
		SkySiteUtils.waitForElement(driver, stateTitleItem, 60);
		stateTitleItem.click();
		SkySiteUtils.waitTill(2000);
		Log.message("State has been selected.");		
		txtCountry.clear();
		SkySiteUtils.waitForElement(driver, txtCountry,100);
		txtCountry.sendKeys(PropertyReader.getProperty("Country"));
		SkySiteUtils.waitForElement(driver, txtCountry,100);		
		countryTitleItem.click();
		SkySiteUtils.waitTill(2000);
		Log.message("Country has been selected.");*/
		SkySiteUtils.waitForElement(driver, txtZip,100);
		txtZip.clear();
		SkySiteUtils.waitForElement(driver, txtZip,100);
		txtZip.sendKeys(PropertyReader.getProperty("Zip"));
		Log.message("Zip has been entered.");
		SkySiteUtils.waitTill(5000);
		btnCreate.click();
		Log.message("Create Project button has been clicked.");
		/*SkySiteUtils.waitForElement(driver, projectCreationSuccessMsg, 60);
		String expectedProjSuccessMsg = projectCreationSuccessMsg.getText();
		Log.message(expectedProjSuccessMsg);
		String actualProjSuccessMsg = "Project created successfully";*/
	
		SkySiteUtils.waitForElement(driver, checkboxEnableAutoHypLink,120);
		SkySiteUtils.waitTill(5000);
		checkboxEnableAutoHypLink.click();
		Log.message("Clicked on enable auto hyperlink checkbox.");
		SkySiteUtils.waitForElement(driver, btnSavePrjSettingsWin,60);
		btnSavePrjSettingsWin.click();
		Log.message("Clicked on save button of project settings window.");
		SkySiteUtils.waitForElement(driver, btnYes, 60);
		SkySiteUtils.waitTill(2000);
		btnYes.click();
		SkySiteUtils.waitForElement(driver, prjNameBreadcrum, 60);
		SkySiteUtils.waitTill(3000);
		/*if(prjNameBreadcrum.getText().equalsIgnoreCase(Project_Name))
		Log.message("Project Created Successfully with name : "+Project_Name);*/
			return new FolderPage(driver).get();
	}
	
	
	
	/** 
	 * Method written for creating new Password protected project.
	 * Scripted By: Naresh Babu 
	 * @return
	 */
	public FolderPage ProjectCreation_WithPassword(String Project_Name,String Prj_Password)
	{
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnCreateProject, 60);
		btnCreateProject.click();
		Log.message("Create Project button is clicked.");
		SkySiteUtils.waitTill(5000);
		txtProjectName.clear();
		txtProjectName.sendKeys(Project_Name);
		Log.message("Project Name has been entered as ."+Project_Name);
		txtProjectNumber.clear();
		txtProjectNumber.sendKeys(PropertyReader.getProperty("Projectnumber"));
		Log.message("Project Number has been entered.");
		txtProjectDescription.clear();
		txtProjectDescription.sendKeys(PropertyReader.getProperty("Projectdescription"));
		Log.message("Project description has been entered.");
		//txtProjectStartDate.clear();
		txtProjectStartDate.sendKeys(PropertyReader.getProperty("Projectstartdate"));
		Log.message("Project start date has been entered.");
		txtAddress.clear();
		txtAddress.sendKeys(PropertyReader.getProperty("Siteaddress"));
		Log.message("Site Address has been entered.");
		txtCountry.clear();
		txtCountry.sendKeys(PropertyReader.getProperty("Country"));
		SkySiteUtils.waitForElement(driver, txtCountry,60);
		countryTitleItem.click();
		Log.message("Country has been selected.");
		SkySiteUtils.waitTill(3000);
		txtState.clear();
		SkySiteUtils.waitTill(3000);
		txtState.sendKeys(PropertyReader.getProperty("State"));
		SkySiteUtils.waitForElement(driver, stateTitleItem, 60);
		stateTitleItem.click();
		Log.message("State has been selected.");
		txtCity.clear();
		txtCity.sendKeys(PropertyReader.getProperty("City"));
		Log.message("City has been entered.");
		txtZip.clear();
		txtZip.sendKeys(PropertyReader.getProperty("Zip"));
		Log.message("Zip has been entered.");
		txtProjectPassword.clear();
		txtProjectPassword.sendKeys(Prj_Password);
		SkySiteUtils.waitTill(5000);
		btnCreate.click();
		Log.message("Create Project button has been clicked.");
		/*SkySiteUtils.waitForElement(driver, projectCreationSuccessMsg, 60);
		String expectedProjSuccessMsg = projectCreationSuccessMsg.getText();
		Log.message(expectedProjSuccessMsg);
		String actualProjSuccessMsg = "Project created successfully";*/
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, checkboxEnableAutoHypLink, 60);
		SkySiteUtils.waitTill(5000);
		checkboxEnableAutoHypLink.click();
		Log.message("Clicked on enable auto hyperlink checkbox.");
		SkySiteUtils.waitTill(5000);
		btnSavePrjSettingsWin.click();
		Log.message("Clicked on save button of project settings window.");
		SkySiteUtils.waitTill(10000);
		btnYes.click();
		SkySiteUtils.waitForElement(driver, prjNameBreadcrum, 60);
		SkySiteUtils.waitTill(3000);
		if(prjNameBreadcrum.getText().equalsIgnoreCase(Project_Name))
		Log.message("Project Created Successfully with name : "+Project_Name);
			return new FolderPage(driver).get();
	}
	
	/** 
	 * Method written for Access Password protected project by entering invalid password.
	 * Scripted By: Naresh Babu 
	 * @return
	 */
	public boolean OpenPasswordProtectedProject_WithInvalidPassword(String Project_Name,String Invalid_Password)
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;
		boolean result = false;
		try
		{
			SkySiteUtils.waitTill(5000);
			//Getting count of available projects
			int Avl_Projects_Count=0;
			List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'project-icon')]"));
			for (WebElement Element : allElements)
			{ 
				Avl_Projects_Count =Avl_Projects_Count+1; 	
			}
			Log.message("Available private projects count is: "+Avl_Projects_Count);
			SkySiteUtils.waitTill(5000);
			int i=0;
			 for(i=1;i<=Avl_Projects_Count;i++)
			 {
				 String NameAftProjCreate = driver.findElement(By.xpath("//li["+i+"]/div/section[1]/h4")).getText();	
			//Validating the new project is created or not
				 if(Project_Name.trim().contentEquals(NameAftProjCreate.trim()))
				 {  	
					 result1=true;
					 break; 
				 }
			 }
			 
			 if(result1==true)
			 {
				 Log.message("Maching Project: "+Project_Name+" is Found in projects list!!!");
				 driver.findElement(By.xpath("//li["+i+"]/div/section[1]/h4")).click(); 
				 SkySiteUtils.waitForElement(driver, contentPasswordField, 20);
				 SkySiteUtils.waitTill(5000);
				 String PopOverMsg = contentPasswordField.getText();
				 Log.message("Pop over message for password edit field is displayed as: "+PopOverMsg);
				 
				 if((PopOverMsg.equals("Enter password")) && (driver.findElement(By.xpath("(//input[@class='form-control password-event'])["+i+"]"))).isDisplayed())
				 {
					 result2=true;
					 Log.message("Project: "+Project_Name+" is having password field!!!");
					 driver.findElement(By.xpath("(//input[@class='form-control password-event'])["+i+"]")).sendKeys(Invalid_Password);
					 SkySiteUtils.waitTill(3000);
					 //Press Enter from Keyboard
					 Actions action = new Actions(driver);
					 action.sendKeys(Keys.ENTER).build().perform();
					 SkySiteUtils.waitForElement(driver, contentPasswordField, 20);
					 SkySiteUtils.waitTill(3000);
					 String PopOverMsg_InvalidPassword = contentPasswordField.getText();
					 Log.message("Pop over message after given password is displayed as: "+PopOverMsg_InvalidPassword);
			
					//Condition for checking project name is matched or not
					if(PopOverMsg_InvalidPassword.contentEquals("Wrong password") && driver.findElement(By.xpath("(//input[@class='form-control password-event'])["+i+"]")).isEnabled())
					{
						result3=true;
						Log.message("Project: "+Project_Name+" selection failed by giving Invalid password!!!");
					}
					else
					{
						result3=false;
						Log.message("Project: "+Project_Name+" is selected successfully by giving Invalid password!!!");
					}	 
				 }
				 else
				 {
					 result2=false;
					 Log.message("Project: "+Project_Name+" is not having any password field!!!");
				 }
			 }
			 else
			 {
				 result1=false;
				 Log.message("Maching Project: "+Project_Name+" is NOT Found in projects list!!!");
			 }
			 
			 if((result1==true)&&(result2==true)&&(result3==true))
			{
				 Log.message("Project: "+Project_Name+" is not accepting invalid passwords!!!");
				 result=true;
			}
			else
			{
			 	Log.message("Project: "+Project_Name+" is accepting invalid passwords!!!");
			 	result=false;
			}
		}
		catch(Exception e)
		{
			Log.message("Password protection Failed Due to Exception!!!"+e);
		}
		finally
		 {
			 return result;
		 }
	}
	
	/** 
	 * Method written for Access Password protected project by entering password.
	 * Scripted By: Naresh Babu 
	 * @return
	 */
	@SuppressWarnings("finally")
	public boolean selectPasswordProtectedProject(String Project_Name,String Valid_Password)
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result = false;
		try
		{
			SkySiteUtils.waitTill(5000);
			btnProjectDashboard.click();
			Log.message("Clicked on Project Dashboard.");
			SkySiteUtils.waitForElement(driver, PrivateProjectsTab, 30);
			SkySiteUtils.waitTill(10000);
			//Getting count of available projects
			int Avl_Projects_Count=0;
			List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'project-icon')]"));
			for (WebElement Element : allElements)
			{ 
				Avl_Projects_Count =Avl_Projects_Count+1; 	
			}
			Log.message("Available private projects count is: "+Avl_Projects_Count);
			SkySiteUtils.waitTill(5000);
			int i=0;
			for(i=1;i<=Avl_Projects_Count;i++)
			{
				String NameAftProjCreate = driver.findElement(By.xpath("(//span[@data-bind='text: ProjectName()'])["+i+"]")).getText();	
			//Validating the new project is created or not
				if(Project_Name.trim().contentEquals(NameAftProjCreate.trim()))
				{  	
					result1=true;
					break; 
				 }
			 }
			 
			 if(result1==true)
			 {
				 Log.message("Maching Project: "+Project_Name+" is Found in projects list!!!");
				 driver.findElement(By.xpath("(//span[@data-bind='text: ProjectName()'])["+i+"]")).click(); 
				 SkySiteUtils.waitTill(3000);
				 driver.findElement(By.xpath("(//input[@type='password' and @placeholder='password'])["+i+"]")).click();
				 SkySiteUtils.waitTill(2000);
				 driver.findElement(By.xpath("(//input[@type='password' and @placeholder='password'])["+i+"]")).sendKeys(Valid_Password);
				 SkySiteUtils.waitTill(2000);
				 driver.findElement(By.xpath("(//i[@class='icon icon-arrow-right'])["+i+"]")).click();//Click on Arrow
				 SkySiteUtils.waitTill(1000);
				 SkySiteUtils.waitForElement(driver, prjNameBreadcrum, 30);
				 SkySiteUtils.waitTill(4000);
				 String actual_prjectname=prjNameBreadcrum.getText();//Taking Project Name from Breadcrum
				 //Condition for checking project name is matched or not
				 if(actual_prjectname.contentEquals(Project_Name))
				 {
					 result2=true;
					 Log.message("Project: "+Project_Name+" is selected successfully by giving Valid Password!!!");
				 }
				 else
				 {
					 result2=false;
					 Log.message("Project: "+Project_Name+" selection failed by giving Valid Password!!!");
				 }	 
				
			 }
			 else
			 {
				 result1=false;
				 Log.message("Maching Project: "+Project_Name+" is NOT Found in projects list!!!");
			 }
			 
			 if((result1==true)&&(result2==true))
			{
				 Log.message("Password protected Project: "+Project_Name+" is selected successfully!!!");
				 result=true;
			}
			else
			{
			 	Log.message("Password protected Project: "+Project_Name+" is Failed to select!!!");
			 	result=false;
			}
		}
		catch(Exception e)
		{
			Log.message("Password protection Failed Due to Exception!!!"+e);
		}
		finally
		 {
			 return result;
		 }
	}
	
	/** 
	 * Method written for validate Favorite Projects
	 * Scripted By: Naresh Babu 
	 * @return
	 */
	public boolean AddFavorite_RemoveFavorite()
	{
		boolean res=false;
		boolean res1=false;
		boolean res2=false;
		try
		{
			SkySiteUtils.waitTill(3000);
			String FavCountBefAdd = tabFavoriteCount.getText();//Getting Favourite count before Add Favourite
			PrivateProjectsTab.click();
			Log.message("Selected My Projects Tab.");
			SkySiteUtils.waitTill(10000);
		//Getting all available non Favourite icons count
			int NonFavProjects=0;
			int FavProjectsCount=0;
			String FavCountAfterMadeUnfav=null;
			List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'project-icon')]"));
			for (WebElement Element : allElements)
			{ 
				NonFavProjects = NonFavProjects+1; 	
			}
			
		if((NonFavProjects>=1) && (FavCountBefAdd.contentEquals("(0)")))
		{
			for(int j=1;j<=NonFavProjects;j++)
			{
				driver.findElement(By.xpath("(//i[@class='icon icon-more-option'])["+j+"]")).click();//Click on project drop list
				SkySiteUtils.waitTill(3000);
				WebElement MakeFav = driver.findElement(By.xpath("(//i[@class='icon icon-star-empty'])["+j+"]"));
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",MakeFav);//JSExecutor
				//driver.findElement(By.xpath("(//i[@class='icon icon-heart pw-icon-white icon-stack-1x'])["+j+"]")).click();//Make Favourite
				SkySiteUtils.waitTill(3000);
			}
			
			FavProjectsTab.click();
			Log.message("Clicked on Favorite Projects Tab.");
			SkySiteUtils.waitTill(10000);
		//Getting number of Favourite icons count
			List<WebElement> allElements1 = driver.findElements(By.xpath("//i[@class='icon icon-star pw-icon-yellow']"));
			for (WebElement Element : allElements1)
			{ 
				FavProjectsCount = FavProjectsCount+1; 	
			}
			String FavCountAfterAdd=driver.findElement(By.xpath("//small[@id='favoriteProjCounter']")).getText();//Getting Favourite count from button
		//Checking whether non Favourite projects count and Favourite projects count is same or not
			if((NonFavProjects==FavProjectsCount) && (FavCountAfterAdd.contentEquals("("+FavProjectsCount+")")))
			{
				res1=true;
			}
			
			if(res1==true)
			{
				Log.message("All the "+NonFavProjects+" Non Favourite projects are successfully made as Favourite projects!!!");
		//Making Favourite Projects as un Favourite projects	
				for(int k=1;k<=FavProjectsCount;k++)
				{
					driver.findElement(By.xpath("(//i[@class='icon icon-more-option'])[1]")).click();//Click on project drop list
					SkySiteUtils.waitTill(3000);//Modified on 20th Aug
					WebElement MakeUnFav = driver.findElement(By.xpath("(//i[@class='icon icon-star-empty'])[1]"));
					((JavascriptExecutor) driver).executeScript("arguments[0].click();",MakeUnFav);//JSExecutor
					//driver.findElement(By.xpath("(//i[@class='icon icon-heart pw-icon-white icon-stack-1x'])["+k+"]")).click();//Make un Favourite
					SkySiteUtils.waitTill(5000);
				}
				PrivateProjectsTab.click();
				Log.message("Selected My Projects Tab.");
				SkySiteUtils.waitTill(5000);
				FavCountAfterMadeUnfav=tabFavoriteCount.getText();//Getting Favourite count after made un favourite
				if(FavCountAfterMadeUnfav.contentEquals(FavCountBefAdd))
				{
					res2=true;
					Log.message("All the "+FavProjectsCount+" Favourite projects are successfully made as Un Favourite projects!!!");
				}
				else
				{
					Log.message("All the "+FavProjectsCount+" Favourite projects are FAILED to made as Un Favourite projects!!!");
					res2=false;
				}			
			}
			else
			{
				Log.message("All the "+NonFavProjects+" Non Favourite projects are FAILED to made as Favourite projects!!!");
				res1=false;
			}
		}
		else
		{
			Log.message("No Projects are available for make as Favourite projects!!!");
			res=false;
		}
			
		//Final Condition
			if((res1==true)&&(res2==true))
			{
				res=true;
				Log.message("User Successfully done with Add Favourite and Remove Favourite!!!");
			}
			else
			{
				res=false;
				Log.message("User FAILED with Add Favourite and Remove Favourite!!!");
			}
		
		}//End of try block
		catch(Exception e)
		{
			Log.message("Exception Occurred While dealing with Favourites!!!");
			res=false;
		}
		finally
		{
			return res;
		}
	}
	
	
	/** 
	 * Method written for validate Edit Project
	 * Scripted By: Naresh Babu 
	 * @return
	 */
	public boolean Edit_Project(String Project_Name, String Edit_projectName)
	{
		boolean result1 = false;
		boolean result2 = false;
	
		SkySiteUtils.waitTill(5000);
		btnProjectDashboard.click();
		Log.message("Clicked on Project Dashboard.");
		SkySiteUtils.waitForElement(driver, PrivateProjectsTab, 30);
		SkySiteUtils.waitTill(5000);
		//Getting count of available projects
		int Avl_Projects_Count=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'project-icon')]"));
		for (WebElement Element : allElements)
		{ 
			Avl_Projects_Count =Avl_Projects_Count+1; 	
		}
		Log.message("Available private projects count is: "+Avl_Projects_Count);
		SkySiteUtils.waitTill(5000);
		int i=0;
		for(i=1;i<=Avl_Projects_Count;i++)
		{
			String NameAftProjCreate = driver.findElement(By.xpath("(//span[@data-bind='text: ProjectName()'])["+i+"]")).getText();	
			Log.message("The name of project is: "+NameAftProjCreate);
			//Validating the new project is created or not
			if(Project_Name.trim().contentEquals(NameAftProjCreate.trim()))
			{  	
				result1=true;
				break; 
			}
		 }
		
		if(result1==true)
		{
			Log.message("Expected project is available. You can edit the same.");
			List <WebElement> ele = driver.findElements(By.xpath("//i[@class='icon icon-more-option']"));
			WebElement btnMore = ele.get(i-1);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",btnMore);//JSExecutor
			SkySiteUtils.waitTill(3000);
			WebElement EditLink = driver.findElement(By.xpath("(//div/span/div/ul/li[2]/a)["+i+"]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",EditLink);//JSExecutor
			Log.message("Selected Edit field from project dropdown.");
			SkySiteUtils.waitForElement(driver, btnCreate, 30);//Wait for update project button
			SkySiteUtils.waitTill(3000);
			
			//Generating all input values randomly
			String Random_Number = Generate_Random_Number.generateRandomValue();
			String Edit_projectNumber = Random_Number;
			String Edit_Description = "Description_"+Random_Number;
			String Edit_SiteAddress = "SiteAddress_"+Random_Number;
			String Edit_City = "City_"+Random_Number;
			String Edit_state = null;
			String Edit_Country = null;
			String Edit_Zip = Random_Number;
			String Edit_Password = Random_Number;
			String Edit_Date = null;
			
			txtProjectName.clear();//Clear the existed project name
			txtProjectName.sendKeys(Edit_projectName);//Enter new project name
		
			txtProjectNumber.clear();//Clear the existed Project Number
			txtProjectNumber.sendKeys(Edit_projectNumber);//Enter Project Number
		
			SkySiteUtils.waitTill(2000);
			butProjectStartDate.click();//Clicking on Project Start Date symbol
			SkySiteUtils.waitTill(3000);
			driver.findElement(By.xpath("//td[@class='day active today']")).click();//Select today's date
			Log.message("Date is selected!!!");
			SkySiteUtils.waitTill(3000);
			Edit_Date = txtProjectStartDate.getAttribute("value");
			Log.message("Selected Date is: "+Edit_Date);
			
			txtProjectDescription.clear();//Clear the existed Description
			txtProjectDescription.sendKeys(Edit_Description);//Enter Project Description
		
			txtAddress.clear();//Clear the existed Site Address
			txtAddress.sendKeys(Edit_SiteAddress);//Enter Site Address
		
			txtCity.clear();//Clear Existed City
			txtCity.sendKeys(Edit_City);//Enter City
		
			//Generating a random value in between 1 to 200
			int Max = 201;
			int Min = 1;
			//Create Instance of Random Class
			Random ranomNum = new Random();
			int Gen_Random_Val = Min + ranomNum.nextInt(Max);
			Log.message("Genarated Random Value is: "+Gen_Random_Val);
			
			driver.findElement(By.xpath("//div[@id='ddlCountrydrop']")).click();
			SkySiteUtils.waitTill(2000);
			driver.findElement(By.xpath("(//a[@class='Country-Item'])["+Gen_Random_Val+"]")).click();//Select Any Country
			SkySiteUtils.waitTill(2000);
			Edit_Country = txtCountry.getAttribute("value");
			Log.message("Edit Country is: "+Edit_Country);
		
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath("//div[@id='ddlstatedrop']")).click();
			SkySiteUtils.waitTill(2000);
			driver.findElement(By.xpath("(//a[@class='State-Item'])[1]")).click();//Select Any State
			SkySiteUtils.waitTill(2000);
			Edit_state = txtState.getAttribute("value");
			Log.message("Edit State is: "+Edit_state);
		
			txtZip.clear();//Clear Existed Zip
			txtZip.sendKeys(Edit_Zip);//Enter Zip
			
			btnCreate.click();
			Log.message("Clicked on Update Project Button!!!");
			SkySiteUtils.waitTill(1000);
			
			SkySiteUtils.waitForElement(driver, projectCreationSuccessMsg, 30);//Wait until message get displayed
			String MsgAfterProjEdit = projectCreationSuccessMsg.getText();//Getting Notification message
			Log.message("Notification message After project edit is:"+MsgAfterProjEdit);
			SkySiteUtils.waitTill(10000);
			//Validate Edit project is available in the list
			for(i=1;i<=Avl_Projects_Count;i++)
			{
				String NameAftProjEdit = driver.findElement(By.xpath("(//span[@data-bind='text: ProjectName()'])["+i+"]")).getText();	
			//Validating the new project is created or not
				if(NameAftProjEdit.trim().contentEquals(Edit_projectName.trim()))
				{  	
					result2=true;
					break; 
				}
			 }
		
		}
		if((result1==true) && (result2==true))
		{
			Log.message("Project edited from "+Project_Name+" to "+Edit_projectName+" successfully");
			return true;
		}
		else
		{
			Log.message("Project edit failed.");
			return false;
		}
		
	}
	
	/** 
	 * Method written for validate Delete Project
	 * Scripted By: Naresh Babu 
	 * @return
	 */
	public boolean Delete_Project(String Project_Name)
	{
		boolean result1 = false;
		boolean result2 = false;
		
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, PrivateProjectsTab, 30);
		//Getting count of available projects
		int Avl_Projects_Count=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'project-icon')]"));
		for (WebElement Element : allElements)
		{ 
			Avl_Projects_Count =Avl_Projects_Count+1; 	
		}
		Log.message("Available private projects count is: "+Avl_Projects_Count);
		SkySiteUtils.waitTill(5000);
		int i=0;
		for(i=1;i<=Avl_Projects_Count;i++)
		{
			String NameAftProjCreate = driver.findElement(By.xpath("(//span[@data-bind='text: ProjectName()'])["+i+"]")).getText();	
		//Validating the new project is created or not
			if(Project_Name.trim().contentEquals(NameAftProjCreate.trim()))
			{  	
				result1=true;
				break; 
			}
		 }
		
		if(result1==true)
		{
			Log.message("Expected project is available. You can Delete the same.");
			//List <WebElement> ele = driver.findElements(By.xpath("//i[@class='icon icon-more-option']"));
			//WebElement btnMore = ele.get(i-2);
			//((JavascriptExecutor) driver).executeScript("arguments[0].click();",btnMore);//JSExecutor
//			ele.get(i-2).click();
			driver.findElement(By.xpath("(//i[@class='icon icon-more-option'])["+i+"]")).click();//Click on project drop list
			SkySiteUtils.waitTill(3000);
			WebElement DeleteLink = driver.findElement(By.xpath("(//i[@class='icon icon-trash space-icon'])["+i+"]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",DeleteLink);//JSExecutor
			//driver.findElement(By.xpath("(//i[@class='icon icon-trash space-icon'])["+i+"]")).click();
			Log.message("Selected Delete field from project dropdown.");
			SkySiteUtils.waitTill(2000);
			SkySiteUtils.waitForElement(driver, btnYes, 20);//Wait for update project button
			SkySiteUtils.waitTill(3000);
			btnYes.click();
			Log.message("Clicked on YES to delete project button.");
			SkySiteUtils.waitTill(2000);
			SkySiteUtils.waitForElement(driver, projectCreationSuccessMsg, 30);//Wait until message get displayed
			String MsgAfterProjDelete = projectCreationSuccessMsg.getText();//Getting Notification message
			Log.message("Notification message After project Delete is:"+MsgAfterProjDelete);
			SkySiteUtils.waitTill(10000);
			if(MsgAfterProjDelete.contains(Project_Name))
			{
				result2=true;
				Log.message("Project deleted successfully.");
			}
		}
		if((result1==true) && (result2==true))
		{
			Log.message("Identified expected project and deleted successfully.");
			return true;
		}
		else
		{
			Log.message("Identified expected project and deleted Failed.");
			return false;
		}
		
	}
	
	/** 
	 * Method written for Validate "Download Desktop Sync" footer link
	 * Scripted By: Naresh Babu 
	 * @return
	 */
	public boolean validateFooterLink_DownloadDesktopSync(String Sys_Download_Path) throws InterruptedException, AWTException
	{
		boolean result1 = false;
		boolean result2 = false;
		
		SkySiteUtils.waitForElement(driver, linkDownloadSync, 20);
		SkySiteUtils.waitTill(2000);
		linkDownloadSync.click();
		Log.message("Clicked on Download Desktop Sync Link");
		SkySiteUtils.waitTill(10000);
		//Switch to new download window
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle);
		} 
		//Calling delete files from download folder script
		this.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(5000);
		//Download 32bit Sync .exe file and validate
		SkySiteUtils.waitForElement(driver, btnDownloadSync32bit, 20);
		btnDownloadSync32bit.click();
		Log.message("Clicked on 32bit download");
		SkySiteUtils.waitTill(60000);	
		//Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: "+browserName);
		if(browserName.contains("firefox"))
		{
		//Handling Download PopUp of firefox browser using robot
			Robot robot=null;
			robot=new Robot();
			
			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);
		}
		//After download, checking whether .exe File is existed under download folder or not 
		String ActualFoldername=null;
		File[] files = new File(Sys_Download_Path).listFiles();				
		for(File file : files)
		{
			if(file.isFile()) 
			{
				ActualFoldername=file.getName();//Getting File Names into a variable
				Log.message("Actual Folder name is:"+ActualFoldername);
				SkySiteUtils.waitTill(500);	
				if(ActualFoldername.contains("SKYSITEProjectSync-x86"))						
				{
					result1=true;							
					break;
				}				
			}					
		}
						
		if(result1==true)
		{
			Log.message("32bit sync .exe file is available in system downloads Folder!!!");
		}
		else
		{
			result1=false;
			Log.message("32bit sync .exe file is NOT available in system downloads Folder!!!");
		}
		
		//Download 64bit Sync .exe file and validate
		SkySiteUtils.waitForElement(driver, btnDownloadSync64bit, 20);
		btnDownloadSync64bit.click();
		Log.message("Clicked on 64bit download");
		SkySiteUtils.waitTill(60000);	
		//Get Browser name on run-time.
		browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: "+browserName);
		if(browserName.contains("firefox"))
		{
		//Handling Download PopUp of firefox browser using robot
			Robot robot=null;
			robot=new Robot();
				
			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);
		}
		//After download, checking whether .exe File is existed under download folder or not 
		String ActualFilename=null;
		File[] files1 = new File(Sys_Download_Path).listFiles();				
		for(File file : files1)
		{
			if(file.isFile()) 
			{
				ActualFilename=file.getName();//Getting File Names into a variable
				Log.message("Actual .exe File name is:"+ActualFilename);
				SkySiteUtils.waitTill(500);	
				if(ActualFilename.contains("SKYSITEProjectSync-x64"))						
				{
					result2=true;							
					break;
				}				
			}				
		}				
		if(result1==true)
		{
			Log.message("64bit sync .exe file is available in system downloads Folder!!!");
		}
		else
		{
			result2=false;
			Log.message("64bit sync .exe file is NOT available in system downloads Folder!!!");
		}
		//Close the new window and navigate back to main window
		driver.close();
		SkySiteUtils.waitTill(2000);
		driver.switchTo().window(winHandleBefore);
		SkySiteUtils.waitTill(5000);
		
		if((result1==true) && (result2==true))
		{
			Log.message("32bit and 64bit sync download working successfully.");
			return true;
		}
		else
		{
			Log.message("32bit and 64bit sync download is NOT working.");
			return false;	
		}
			
	}
	
	/** 
	 * Method written for Validate "FAQ" footer link
	 * Scripted By: Naresh Babu 
	 * @return
	 */
	public boolean validateFooterLink_FAQ() throws InterruptedException, AWTException
	{
		boolean result1 = false;
		
		SkySiteUtils.waitForElement(driver, linkFAQ, 20);
		SkySiteUtils.waitTill(2000);
		linkFAQ.click();
		Log.message("Clicked on FAQ link from footer.");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, objFAQsWindowHeadLine, 20);
		if(objFAQsWindowHeadLine.isDisplayed())
		{
			result1=true;
			Log.message("FAQ window was opened successfully.");
		}
		
		btnCloseFAQs.click();
		Log.message("Clicked on close button of FAQs window.");
		if(result1==true)
		{
			Log.message("FAQ link from the Footer is working successfully.");
			return true;
		}
		else
		{
			Log.message("FAQ link from the Footer is NOT working.");
			return false;
		}
	}
	
	/** 
	 * Method written for Validate "Support" footer link
	 * Scripted By: Naresh Babu 
	 * @return
	 */
	public boolean validateFooterLink_Support() throws InterruptedException, AWTException
	{
		boolean result1 = false;
		
		SkySiteUtils.waitForElement(driver, linkSupport, 20);
		SkySiteUtils.waitTill(2000);
		linkSupport.click();
		Log.message("Clicked on Support link from footer.");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnProvideTempAccessToSupportTeam, 20);
		String Support_MailId=driver.findElement(By.xpath(".//*[@id='support-modal']/div/div/div[2]/div[1]/div/div[1]/p[1]/a")).getText();
		Log.message("Support MailId is: "+Support_MailId);
		String GeneralInfo_MailId=driver.findElement(By.xpath(".//*[@id='support-modal']/div/div/div[2]/div[1]/div/div[1]/p[2]/a")).getText();
		Log.message("GeneralInfo MailId is: "+GeneralInfo_MailId);
		
		if((Support_MailId.contentEquals("support@skysite.com")) && (GeneralInfo_MailId.contentEquals("info@skysite.com")))
		{
			result1=true;
			Log.message("Support Link window opend successfully!!!");
		}
		btnCloseSupportWindow.click();
		Log.message("Clicked on close button of Support window.");
		if(result1==true)
		{
			Log.message("Support link from the Footer is working successfully.");
			return true;
		}
		else
		{
			Log.message("Support link from the Footer is NOT working.");
			return false;
		}
	}
	
	/** 
	 * Method written for Validate "Feedback" footer link
	 * Scripted By: Naresh Babu 
	 * @return
	 */
	public boolean validateFooterLink_Feedback() throws InterruptedException, AWTException
	{
		boolean result1 = false;
		
		SkySiteUtils.waitForElement(driver, linkFeedback, 20);
		SkySiteUtils.waitTill(2000);
		linkFeedback.click();
		Log.message("Clicked on Feedback link from footer.");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnCancelFeedback, 20);
		txtFeedback.sendKeys("Testing Feedback functionality from ARC QA Team!!!");
		Log.message("Feedback message was entered.");
		btnSubmitFeedback.click();
		Log.message("Clicked on Submit Feedback button.");
		SkySiteUtils.waitForElement(driver, projectCreationSuccessMsg, 20);
		SkySiteUtils.waitTill(1000);
		String Msg_AfterSendFeedback=projectCreationSuccessMsg.getText();
		Log.message("Message After Send Feedback is: "+Msg_AfterSendFeedback);
		
		if(Msg_AfterSendFeedback.contentEquals("Feedback sent successfully."))
		{
			result1=true;
			Log.message("User able to send feedback from the link successfully!!!");
		}		
		if(result1==true)
		{
			Log.message("Feedback link from the Footer is working successfully.");
			return true;
		}
		else
		{
			Log.message("Feedback link from the Footer is NOT working.");
			return false;
		}
	}
	
	/** 
	 * Method written for Validate "Logout" footer link
	 * Scripted By: Naresh Babu 
	 * @return
	 */
	public boolean validateFooterLink_Logout() throws InterruptedException, AWTException
	{
		boolean result1 = false;
		
		SkySiteUtils.waitForElement(driver, linkLogoutFooter, 20);
		SkySiteUtils.waitTill(2000);
		linkLogoutFooter.click();
		Log.message("Clicked on Logout Link.");
		SkySiteUtils.waitForElement(driver, btnYes, 20);
		SkySiteUtils.waitTill(2000);
		btnYes.click();
		Log.message("Clicked on YES want to Logout.");
		String ExpTitle = "Sign in - SKYSITE";//Expected Page title
		SkySiteUtils.waitTill(5000);
		String actTitle = driver.getTitle();//Getting Actual title of the page
		if(actTitle.contentEquals(ExpTitle))
		{
			result1=true;
			Log.message("User Logout success from logout link!!!");
		}		
		if(result1==true)
		{
			Log.message("Logout link from the Footer is working successfully.");
			return true;
		}
		else
		{
			Log.message("Logout link from the Footer is NOT working.");
			return false;
		}
	}
	
	
	
	public FolderPage createProjectwithSubject(String Project_Name,String subject)
	{
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnCreateProject, 60);
		btnCreateProject.click();
		Log.message("Create Project button is clicked.");
		SkySiteUtils.waitTill(5000);
		txtProjectName.clear();
		txtProjectName.sendKeys(Project_Name);
		Log.message("Project Name has been entered as ."+Project_Name);
		txtProjectNumber.clear();
		txtProjectNumber.sendKeys(PropertyReader.getProperty("Projectnumber"));
		Log.message("Project Number has been entered.");
		txtProjectDescription.clear();
		txtProjectDescription.sendKeys(PropertyReader.getProperty("Projectdescription"));
		Log.message("Project description has been entered.");
		//txtProjectStartDate.clear();
		txtProjectStartDate.sendKeys(PropertyReader.getProperty("Projectstartdate"));
		Log.message("Project start date has been entered.");
		txtAddress.clear();
		txtAddress.sendKeys(PropertyReader.getProperty("Siteaddress"));
		Log.message("Site Address has been entered.");
		txtCountry.clear();
		txtCountry.sendKeys(PropertyReader.getProperty("Country"));
		SkySiteUtils.waitForElement(driver, txtCountry,60);
		countryTitleItem.click();
		Log.message("Country has been selected.");
		SkySiteUtils.waitTill(3000);
		txtState.clear();
		SkySiteUtils.waitTill(3000);
		txtState.sendKeys(PropertyReader.getProperty("State"));
		SkySiteUtils.waitForElement(driver, stateTitleItem, 60);
		stateTitleItem.click();
		Log.message("State has been selected.");
		txtCity.clear();
		txtCity.sendKeys(PropertyReader.getProperty("City"));
		Log.message("City has been entered.");
		txtZip.clear();
		txtZip.sendKeys(PropertyReader.getProperty("Zip"));
		Log.message("Zip has been entered.");
		SkySiteUtils.waitForElement(driver, btnCreate, 60);
		SkySiteUtils.waitTill(5000);
		btnCreate.click();
		
		SkySiteUtils.waitTill(8000);
		//driver.findElement(By.cssSelector("#liSortSetting>a")).click();//click on preference button
		
		/*JavascriptExecutor executor1 = (JavascriptExecutor) driver;
		executor1.executeScript("arguments[0].click();", PreferenceTab);
		Log.message("preference Tab Clicked Sucessfully");		
		SkySiteUtils.waitTill(8000);
		btnYes.click();*/
		//SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, checkboxEnableAutoHypLink, 60);
		checkboxEnableAutoHypLink.click();
		Log.message("Clicked on enable auto hyperlink checkbox.");
		SkySiteUtils.waitTill(5000);		
		//btnSavePreference.click();
		/*Log.message("Create Project button has been clicked.");
		SkySiteUtils.waitForElement(driver, projectCreationSuccessMsg, 60);
		String expectedProjSuccessMsg = projectCreationSuccessMsg.getText();
		Log.message(expectedProjSuccessMsg);
		String actualProjSuccessMsg = "Project created successfully";*/
		//SkySiteUtils.waitForElement(driver, btnSavePrjSettingsWin, 60);
		//SkySiteUtils.waitTill(8000);
		btnSavePrjSettingsWin.click();
		Log.message("Clicked on save button of project settings window.");
		SkySiteUtils.waitTill(10000);
		btnYes.click();
		/*SkySiteUtils.waitForElement(driver, projectCreationSuccessMsg, 20);
		String expectedProjSettingsMsg = projectCreationSuccessMsg.getText();
		Log.message(expectedProjSettingsMsg);
		String actualProjSettingsMsg = "Settings saved successfully";*/
		SkySiteUtils.waitTill(3000);
		/*if((expectedProjSuccessMsg.equalsIgnoreCase(actualProjSuccessMsg)))*/
		Log.message("Project Created Successfully with name : "+Project_Name);
			return new FolderPage(driver).get();
	}

	public FolderPage createProjectwithSpecificRFINumber(String Project_Name,String RFINumber)
	{
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnCreateProject, 60);
		btnCreateProject.click();
		Log.message("Create Project button is clicked.");
		SkySiteUtils.waitTill(5000);
		txtProjectName.clear();
		txtProjectName.sendKeys(Project_Name);
		Log.message("Project Name has been entered as ."+Project_Name);
		txtProjectNumber.clear();
		txtProjectNumber.sendKeys(PropertyReader.getProperty("Projectnumber"));
		Log.message("Project Number has been entered.");
		txtProjectDescription.clear();
		txtProjectDescription.sendKeys(PropertyReader.getProperty("Projectdescription"));
		Log.message("Project description has been entered.");
		//txtProjectStartDate.clear();
		txtProjectStartDate.sendKeys(PropertyReader.getProperty("Projectstartdate"));
		Log.message("Project start date has been entered.");
		txtAddress.clear();
		txtAddress.sendKeys(PropertyReader.getProperty("Siteaddress"));
		Log.message("Site Address has been entered.");
		/*txtCountry.clear();
		txtCountry.sendKeys(PropertyReader.getProperty("Country"));
		SkySiteUtils.waitForElement(driver, txtCountry,60);
		countryTitleItem.click();
		Log.message("Country has been selected.");*/
		SkySiteUtils.waitTill(3000);
		/*txtState.clear();
		SkySiteUtils.waitTill(3000);
		txtState.sendKeys(PropertyReader.getProperty("State"));
		SkySiteUtils.waitForElement(driver, stateTitleItem, 60);
		stateTitleItem.click();
		Log.message("State has been selected.");*/
		txtCity.clear();
		txtCity.sendKeys(PropertyReader.getProperty("City"));
		Log.message("City has been entered.");
		txtZip.clear();
		txtZip.sendKeys(PropertyReader.getProperty("Zip"));
		Log.message("Zip has been entered.");
		SkySiteUtils.waitTill(5000);
		btnCreate.click();
		Log.message("Create Project button has been clicked.");
		
		/*if(projectCreationSuccessMsg.isDisplayed()){
		SkySiteUtils.waitForElement(driver, projectCreationSuccessMsg, 60);
		String expectedProjSuccessMsg = projectCreationSuccessMsg.getText();
		Log.message(expectedProjSuccessMsg);
		String actualProjSuccessMsg = "Project created successfully";
		}*/
		SkySiteUtils.waitTill(5000);		
		SkySiteUtils.waitForElement(driver, RFITab, 60);		
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", RFITab);
		Log.message("RFI Tab Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, RFI_Inbox, 60);
		SkySiteUtils.waitTill(5000);
		RFI_Inbox.click();
		SkySiteUtils.waitTill(5000);
		RFI_Inbox.clear();
		SkySiteUtils.waitTill(5000);
		RFI_Inbox.sendKeys(RFINumber);
		Log.message("Enter RFI Number In RFI Range List");
		//SkySiteUtils.waitForElement(driver, checkboxEnableAutoHypLink, 30);
		//SkySiteUtils.waitTill(5000);
		//checkboxEnableAutoHypLink.click();
		//Log.message("Clicked on enable auto hyperlink checkbox.");
		SkySiteUtils.waitTill(8000);
		//driver.findElement(By.cssSelector("#liSortSetting>a")).click();//click on preference button
		
		JavascriptExecutor executor1 = (JavascriptExecutor) driver;
		executor1.executeScript("arguments[0].click();", PreferenceTab);
		Log.message("preference Tab Clicked Sucessfully");		
		SkySiteUtils.waitTill(8000);
		btnYes.click();
		SkySiteUtils.waitTill(5000);
		checkboxEnableAutoHypLink.click();
		Log.message("Clicked on enable auto hyperlink checkbox.");
		SkySiteUtils.waitTill(5000);		
		btnSavePreference.click();
		Log.message("Clicked on save button of project settings window.");
		SkySiteUtils.waitTill(10000);
		btnYes.click();
		
		/*SkySiteUtils.waitForElement(driver, projectCreationSuccessMsg, 20);
		String expectedProjSettingsMsg = projectCreationSuccessMsg.getText();
		Log.message(expectedProjSettingsMsg);
		String actualProjSettingsMsg = "Settings saved successfully";*/
		//SkySiteUtils.waitTill(3000);
		//if((expectedProjSuccessMsg.equalsIgnoreCase(actualProjSuccessMsg)))
		//Log.message("Project Created Successfully with name : "+Project_Name);
			return new FolderPage(driver).get();
	}
	
	/*
	 * Created Method By Ranjan
	 */
	
	// ############### Script Created By ranjan======================
	
	public FolderPage createProjectwithCustomAtribute(String Project_Name,String CustomAttribute1)
	{
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnCreateProject,120);
		btnCreateProject.click();
		Log.message("Create Project button is clicked.");
		SkySiteUtils.waitTill(5000);
		txtProjectName.clear();
		txtProjectName.sendKeys(Project_Name);
		Log.message("Project Name has been entered as ."+Project_Name);
		txtProjectNumber.clear();
		txtProjectNumber.sendKeys(PropertyReader.getProperty("Projectnumber"));
		Log.message("Project Number has been entered.");
		txtProjectDescription.clear();
		txtProjectDescription.sendKeys(PropertyReader.getProperty("Projectdescription"));
		Log.message("Project description has been entered.");
		//txtProjectStartDate.clear();
		txtProjectStartDate.sendKeys(PropertyReader.getProperty("Projectstartdate"));
		Log.message("Project start date has been entered.");
		txtAddress.clear();
		txtAddress.sendKeys(PropertyReader.getProperty("Siteaddress"));
		Log.message("Site Address has been entered.");
		/*txtCountry.clear();
		txtCountry.sendKeys(PropertyReader.getProperty("Country"));
		SkySiteUtils.waitForElement(driver, txtCountry,60);
		countryTitleItem.click();
		Log.message("Country has been selected.");
		SkySiteUtils.waitTill(3000);
		txtState.clear();
		SkySiteUtils.waitTill(3000);
		txtState.sendKeys(PropertyReader.getProperty("State"));
		SkySiteUtils.waitForElement(driver, stateTitleItem, 60);
		stateTitleItem.click();
		Log.message("State has been selected.");*/
		txtCity.clear();
		txtCity.sendKeys(PropertyReader.getProperty("City"));
		Log.message("City has been entered.");
		txtZip.clear();
		txtZip.sendKeys(PropertyReader.getProperty("Zip"));
		Log.message("Zip has been entered.");
		SkySiteUtils.waitTill(3000);
		btnCreate.click();
		Log.message("Create Project button has been clicked.");
		/*SkySiteUtils.waitForElement(driver, projectCreationSuccessMsg, 60);
		String expectedProjSuccessMsg = projectCreationSuccessMsg.getText();
		Log.message(expectedProjSuccessMsg);
		String actualProjSuccessMsg = "Project created successfully";*/
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, RFITab, 60);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", RFITab);
		Log.message("RFI Tab Clicked Sucessfully");
		
		SkySiteUtils.waitForElement(driver, CustomAttribute_inbox1, 60);	
		SkySiteUtils.waitTill(500);
		CustomAttribute_inbox1.sendKeys(CustomAttribute1);	
		Log.message("Entered Value In customer Arttribute filed:---"+CustomAttribute1);
		
		SkySiteUtils.waitTill(8000);
		//driver.findElement(By.cssSelector("#liSortSetting>a")).click();//click on preference button
		
		JavascriptExecutor executor1 = (JavascriptExecutor) driver;
		executor1.executeScript("arguments[0].click();", PreferenceTab);
		Log.message("preference Tab Clicked Sucessfully");		
		SkySiteUtils.waitTill(8000);
		btnYes.click();
		SkySiteUtils.waitTill(5000);
		checkboxEnableAutoHypLink.click();
		Log.message("Clicked on enable auto hyperlink checkbox.");
		SkySiteUtils.waitTill(5000);		
		btnSavePreference.click();
		
		SkySiteUtils.waitTill(5000);
		//SkySiteUtils.waitForElement(driver, btnSaveRFI, 60);	
		//btnSaveRFI.click();
		Log.message("Clicked on save button of project settings window.");
		//SkySiteUtils.waitTill(10000);
		btnYes.click();
		SkySiteUtils.waitTill(3000);
		/*if((expectedProjSuccessMsg.equalsIgnoreCase(actualProjSuccessMsg)))*/
		Log.message("Project Created Successfully with name : "+Project_Name);
			return new FolderPage(driver).get();
	}
	
	
	public FolderPage createProjectwith_two_CustomAtribute(String Project_Name,String CustomAttribute1,String CustomAttribute2,String RFINumber)
	{
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, btnCreateProject, 60);
		btnCreateProject.click();
		Log.message("Create Project button is clicked.");
		SkySiteUtils.waitForElement(driver, txtProjectName, 60);
		txtProjectName.clear();
		SkySiteUtils.waitForElement(driver, txtProjectName, 60);
		txtProjectName.sendKeys(Project_Name);
		Log.message("Project Name has been entered as ."+Project_Name);
		SkySiteUtils.waitForElement(driver, txtProjectNumber, 60);
		txtProjectNumber.clear();
		SkySiteUtils.waitForElement(driver, txtProjectNumber, 60);
		txtProjectNumber.sendKeys(PropertyReader.getProperty("Projectnumber"));
		Log.message("Project Number has been entered.");
		SkySiteUtils.waitForElement(driver, txtProjectDescription, 60);
		txtProjectDescription.clear();
		SkySiteUtils.waitForElement(driver, txtProjectDescription, 60);
		txtProjectDescription.sendKeys(PropertyReader.getProperty("Projectdescription"));
		Log.message("Project description has been entered.");
		//txtProjectStartDate.clear();
		txtProjectStartDate.sendKeys(PropertyReader.getProperty("Projectstartdate"));
		Log.message("Project start date has been entered.");
		txtAddress.clear();
		txtAddress.sendKeys(PropertyReader.getProperty("Siteaddress"));
		Log.message("Site Address has been entered.");
		txtCountry.clear();
		txtCountry.sendKeys(PropertyReader.getProperty("Country"));
		SkySiteUtils.waitForElement(driver, txtCountry,60);
		countryTitleItem.click();
		Log.message("Country has been selected.");
		SkySiteUtils.waitTill(500);
		txtState.clear();
		SkySiteUtils.waitTill(3000);
		txtState.sendKeys(PropertyReader.getProperty("State"));
		SkySiteUtils.waitForElement(driver, stateTitleItem, 60);
		stateTitleItem.click();
		Log.message("State has been selected.");
		txtCity.clear();
		txtCity.sendKeys(PropertyReader.getProperty("City"));
		Log.message("City has been entered.");
		txtZip.clear();
		txtZip.sendKeys(PropertyReader.getProperty("Zip"));
		Log.message("Zip has been entered.");
		SkySiteUtils.waitForElement(driver, btnCreate, 60);
		btnCreate.click();
		Log.message("Create Project button has been clicked.");
		/*SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, projectCreationSuccessMsg, 60);
		String expectedProjSuccessMsg = projectCreationSuccessMsg.getText();
		Log.message(expectedProjSuccessMsg);
		String actualProjSuccessMsg = "Project created successfully";*/		
		SkySiteUtils.waitTill(500);
		SkySiteUtils.waitForElement(driver, RFITab, 60);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", RFITab);
		Log.message("RFI Tab Clicked Sucessfully");
		//==========Custom Attribute One ==============================================
		SkySiteUtils.waitForElement(driver, CustomAttribute_inbox1, 60);	
		SkySiteUtils.waitTill(1000);
		CustomAttribute_inbox1.sendKeys(CustomAttribute1);	
		Log.message("Entered Value In customer Arttribute filed:---"+CustomAttribute1);		
		CustomAttribute_PlusButton1.click();
		//==============Custom Attribute two==========================================
		SkySiteUtils.waitForElement(driver, CustomAttribute_inbox2, 60);	
		SkySiteUtils.waitTill(1000);
		CustomAttribute_inbox2.sendKeys(CustomAttribute2);	
		Log.message("Entered Value In customer Arttribute filed:---"+CustomAttribute2);		
		CustomAttribute_PlusButton2.click();
		
		SkySiteUtils.waitForElement(driver, btnSaveRFI, 60);	
		btnSaveRFI.click();
		SkySiteUtils.waitForElement(driver, projectCreationSuccessMsg, 60);
		String expectedMsg = projectCreationSuccessMsg.getText();
		Log.message(expectedMsg);
		String actualMsg = "Duplicate item";
		
		if (expectedMsg.equalsIgnoreCase(actualMsg))
		{
		Log.message("Duplication Verified Sucessfully");				
		}
		else
		{
		Log.message("Duplication Not Verified Sucessfully");
		}			
		
		SkySiteUtils.waitForElement(driver, CustomAttribute_inbox2, 60);	
		String CustomAttribute= PropertyReader.getProperty("customAttribute2");
		CustomAttribute_inbox2.sendKeys(CustomAttribute);	
		Log.message("Entered Value In customer Arttribute filed:---"+CustomAttribute);		
		CustomAttribute_PlusButton2.click();		
		SkySiteUtils.waitForElement(driver, btnSaveRFI, 60);	
		btnSaveRFI.click();		
		Log.message("Clicked on save button of project settings window.");
		SkySiteUtils.waitForElement(driver, btnYes, 120);
		btnYes.click();
		SkySiteUtils.waitTill(3000);
		/*if((expectedProjSuccessMsg.equalsIgnoreCase(actualProjSuccessMsg)))
		Log.message("Project Created Successfully with name : "+Project_Name);*/
			return new FolderPage(driver).get();
	}
	
	// ############### Script Created By ranjan======================
	
	public FolderPage createProjectwith_Five_CustomAtribute(String Project_Name,String CustomAttribute1,String CustomAttribute2,String CustomAttribute3,String CustomAttribute4,String CustomAttribute5,String RFINumber)
	{
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitForElement(driver, btnCreateProject,120);
		btnCreateProject.click();
		Log.message("Create Project button is clicked.");
		SkySiteUtils.waitForElement(driver, txtProjectName,60);
		SkySiteUtils.waitTill(1000);
		txtProjectName.clear();
		SkySiteUtils.waitTill(1000);
		txtProjectName.sendKeys(Project_Name);
		Log.message("Project Name has been entered as ."+Project_Name);
		SkySiteUtils.waitForElement(driver, txtProjectNumber,60);
		txtProjectNumber.clear();
		txtProjectNumber.sendKeys(PropertyReader.getProperty("Projectnumber"));
		Log.message("Project Number has been entered.");
		txtProjectDescription.clear();
		txtProjectDescription.sendKeys(PropertyReader.getProperty("Projectdescription"));
		Log.message("Project description has been entered.");
		//txtProjectStartDate.clear();
		txtProjectStartDate.sendKeys(PropertyReader.getProperty("Projectstartdate"));
		Log.message("Project start date has been entered.");
		txtAddress.clear();
		txtAddress.sendKeys(PropertyReader.getProperty("Siteaddress"));
		Log.message("Site Address has been entered.");
		/*txtCountry.clear();
		txtCountry.sendKeys(PropertyReader.getProperty("Country"));
		SkySiteUtils.waitForElement(driver, txtCountry,60);
		countryTitleItem.click();
		Log.message("Country has been selected.");*/
		/*SkySiteUtils.waitForElement(driver, txtState, 60);
		txtState.clear();
		SkySiteUtils.waitForElement(driver, stateTitleItem, 60);
		txtState.sendKeys(PropertyReader.getProperty("State"));
		SkySiteUtils.waitForElement(driver, stateTitleItem, 60);
		stateTitleItem.click();
		Log.message("State has been selected.");*/
		txtCity.clear();
		txtCity.sendKeys(PropertyReader.getProperty("City"));
		Log.message("City has been entered.");
		txtZip.clear();
		txtZip.sendKeys(PropertyReader.getProperty("Zip"));
		Log.message("Zip has been entered.");
		SkySiteUtils.waitTill(2000);
		btnCreate.click();
		Log.message("Create Project button has been clicked.");
		/*SkySiteUtils.waitForElement(driver, projectCreationSuccessMsg, 60);
		String expectedProjSuccessMsg = projectCreationSuccessMsg.getText();
		Log.message(expectedProjSuccessMsg);
		String actualProjSuccessMsg = "Project created successfully";*/
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFITab, 60);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", RFITab);
		Log.message("RFI Tab Clicked Sucessfully");
		//==========Custom Attribute One ==============================================
		SkySiteUtils.waitForElement(driver, CustomAttribute_inbox1, 60);	
		SkySiteUtils.waitTill(1000);
		CustomAttribute_inbox1.sendKeys(CustomAttribute1);	
		Log.message("Entered Value In customer Arttribute filed:---"+CustomAttribute1);		
		CustomAttribute_PlusButton1.click();
		//==============Custom Attribute two==========================================
		SkySiteUtils.waitForElement(driver, CustomAttribute_inbox2, 60);	
		SkySiteUtils.waitTill(1000);
		CustomAttribute_inbox2.sendKeys(CustomAttribute2);	
		Log.message("Entered Value In customer Arttribute filed:---"+CustomAttribute2);		
		CustomAttribute_PlusButton2.click();
		//==============Custom Attribute three=======================================
		
		SkySiteUtils.waitForElement(driver, CustomAttribute_inbox3, 60);	
		SkySiteUtils.waitTill(500);
		CustomAttribute_inbox3.sendKeys(CustomAttribute3);	
		Log.message("Entered Value In customer Arttribute filed:---"+CustomAttribute3);		
		CustomAttribute_PlusButton3.click();
		
		//==============Custom Attribute Four=======================================
		
		SkySiteUtils.waitForElement(driver, CustomAttribute_inbox4, 60);	
		SkySiteUtils.waitTill(500);
		CustomAttribute_inbox4.sendKeys(CustomAttribute4);	
		Log.message("Entered Value In customer Arttribute filed:---"+CustomAttribute4);		
		CustomAttribute_PlusButton4.click();
		//==============Custom Attribute Five======================================
		
		SkySiteUtils.waitForElement(driver, CustomAttribute_inbox5, 60);	
		SkySiteUtils.waitTill(500);
		CustomAttribute_inbox5.sendKeys(CustomAttribute5);	
		Log.message("Entered Value In customer Arttribute filed:---"+CustomAttribute5);	
		
		SkySiteUtils.waitForElement(driver, RFI_Inbox, 60);		
		RFI_Inbox.click();
		Log.message("RFI Inbox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, RFI_Inbox, 60);
		RFI_Inbox.clear();
		Log.message("RFI Inbox Clear Sucessfully");
		SkySiteUtils.waitTill(1000);
		RFI_Inbox.sendKeys(RFINumber);
		Log.message("Entered RFI value In RFI Range List");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, Noofdays, 60);
		Noofdays.clear();
		SkySiteUtils.waitTill(1000);
		Noofdays.sendKeys("2");
		Log.message("No of Days Value Entered Sucessfully");
		SkySiteUtils.waitForElement(driver, btnSaveRFI, 60);	
		btnSaveRFI.click();
		Log.message("Clicked on save button of project settings window.");
		SkySiteUtils.waitTill(2000);
		btnYes.click();
		SkySiteUtils.waitTill(3000);
		/*if((expectedProjSuccessMsg.equalsIgnoreCase(actualProjSuccessMsg)))
		Log.message("Project Created Successfully with name : "+Project_Name);*/
			return new FolderPage(driver).get();
	}
	
	
	public FolderPage CreateProjectwithMaxCustomAtributeAndValidation(String Project_Name,String CustomAttribute1,String CustomAttribute2,String CustomAttribute3,String CustomAttribute4,String CustomAttribute5)
	{	
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnCreateProject,120);
		btnCreateProject.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Create Project button is clicked.");
		SkySiteUtils.waitTill(500);
		txtProjectName.clear();
		txtProjectName.sendKeys(Project_Name);
		Log.message("Project Name has been entered as ."+Project_Name);
		txtProjectNumber.clear();
		txtProjectNumber.sendKeys(PropertyReader.getProperty("Projectnumber"));
		Log.message("Project Number has been entered.");
		txtProjectDescription.clear();
		txtProjectDescription.sendKeys(PropertyReader.getProperty("Projectdescription"));
		Log.message("Project description has been entered.");
		//txtProjectStartDate.clear();
		txtProjectStartDate.sendKeys(PropertyReader.getProperty("Projectstartdate"));
		Log.message("Project start date has been entered.");
		txtAddress.clear();
		txtAddress.sendKeys(PropertyReader.getProperty("Siteaddress"));
		Log.message("Site Address has been entered.");
		txtCountry.clear();
		txtCountry.sendKeys(PropertyReader.getProperty("Country"));
		SkySiteUtils.waitForElement(driver, txtCountry,60);
		countryTitleItem.click();
		Log.message("Country has been selected.");
		SkySiteUtils.waitTill(500);
		txtState.clear();
		SkySiteUtils.waitTill(3000);
		txtState.sendKeys(PropertyReader.getProperty("State"));
		SkySiteUtils.waitForElement(driver, stateTitleItem, 60);
		stateTitleItem.click();
		Log.message("State has been selected.");
		txtCity.clear();
		txtCity.sendKeys(PropertyReader.getProperty("City"));
		Log.message("City has been entered.");
		txtZip.clear();
		txtZip.sendKeys(PropertyReader.getProperty("Zip"));
		Log.message("Zip has been entered.");
		SkySiteUtils.waitTill(2000);
		btnCreate.click();
		Log.message("Create Project button has been clicked.");
		/*SkySiteUtils.waitForElement(driver, projectCreationSuccessMsg, 60);
		String expectedProjSuccessMsg = projectCreationSuccessMsg.getText();
		Log.message(expectedProjSuccessMsg);
		String actualProjSuccessMsg = "Project created successfully";*/
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFITab, 60);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", RFITab);
		Log.message("RFI Tab Clicked Sucessfully");
		//==========Custom Attribute One =========================================
		SkySiteUtils.waitForElement(driver, CustomAttribute_inbox1, 60);	
		SkySiteUtils.waitTill(1000);
		CustomAttribute_inbox1.sendKeys(CustomAttribute1);	
		Log.message("Entered Value In customer Arttribute filed:---"+CustomAttribute1);		
		CustomAttribute_PlusButton1.click();
		//==============Custom Attribute two=======================================
		SkySiteUtils.waitForElement(driver, CustomAttribute_inbox2, 60);	
		SkySiteUtils.waitTill(1000);
		CustomAttribute_inbox2.sendKeys(CustomAttribute2);	
		Log.message("Entered Value In customer Arttribute filed:---"+CustomAttribute2);		
		CustomAttribute_PlusButton2.click();
		//==============Custom Attribute three======================================		
		SkySiteUtils.waitForElement(driver, CustomAttribute_inbox3, 60);	
		SkySiteUtils.waitTill(500);
		CustomAttribute_inbox3.sendKeys(CustomAttribute3);	
		Log.message("Entered Value In customer Arttribute filed:---"+CustomAttribute3);		
		CustomAttribute_PlusButton3.click();		
		//==============Custom Attribute Four=======================================		
		SkySiteUtils.waitForElement(driver, CustomAttribute_inbox4, 60);	
		SkySiteUtils.waitTill(500);
		CustomAttribute_inbox4.sendKeys(CustomAttribute4);	
		Log.message("Entered Value In customer Arttribute filed:---"+CustomAttribute4);		
		CustomAttribute_PlusButton4.click();
		//==============Custom Attribute Five======================================		
		SkySiteUtils.waitForElement(driver, CustomAttribute_inbox5, 60);	
		SkySiteUtils.waitTill(500);
		CustomAttribute_inbox5.sendKeys(CustomAttribute5);	
		Log.message("Entered Value In customer Arttribute filed:---"+CustomAttribute5);	
		//=========================================================================
		
		ArrayList<String> obtainedList = new ArrayList<>(); 
	    java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@id='RFI']/div[1]/div[1]/ul/li"));
	    int size = elementList.size();
	    Log.message("Before Delete the Obtain list:---"+size);   
	    
	    SkySiteUtils.waitTill(500);
	    CustomAttribute_PlusButton2.click();
	    SkySiteUtils.waitTill(500);
	    Log.message("CustomAttribute Box Deleted Clicked");
	    java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@id='RFI']/div[1]/div[1]/ul/li"));
	        
	      int size1 = elementList1.size();
		   
	    Log.message("After Delete the Obtain list---"+size1);
		
	    
	   /* ArrayList<String> sortedList = new ArrayList<>();   
	    for(String s:obtainedList){
	    sortedList.add(s);
	    }*/
		
		
		SkySiteUtils.waitForElement(driver, btnSaveRFI, 60);	
		btnSaveRFI.click();
		Log.message("Clicked on save button of project settings window.");
		SkySiteUtils.waitTill(2000);
		btnYes.click();
		SkySiteUtils.waitTill(3000);
		/*if((expectedProjSuccessMsg.equalsIgnoreCase(actualProjSuccessMsg)))
		Log.message("Project Created Successfully with name : "+Project_Name);*/
			return new FolderPage(driver).get();
	}
	
	
	/** 
	 * Method written for Deleting Existed files from the download folder.
	 * Scripted By : NARESH BABU
	 * @return
	 */
	//Deleting files from a folder
	public void Delete_ExistedFiles_From_DownloadFolder(String Sys_Download_Path)throws InterruptedException 
	{	
		try 
		{ 
			Log.message("Going to Clean existed files from download folder!!!");
			File file = new File(Sys_Download_Path);      
		    String[] myFiles;    
		    if(file.isDirectory())
		    {
		    	myFiles = file.list();
		        for (int i=0; i<myFiles.length; i++) 
		        {
		        	File myFile = new File(file, myFiles[i]); 
		            myFile.delete();
		            //SkySiteUtils.waitTill(2000);
		        }
		        Log.message("Available Folders/Files are deleted from download folder successfully!!!");
		    }
		           
		}//End of try block

		catch(Exception e)
		{
			Log.message("Unable to delete Available Folders/File from download folder!!!"+e);
		}
	}
	
	/** 
	 * Method written for validating Projects Export.
	 * Scripted By : NARESH BABU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	public boolean Validate_ProjectExport(String Sys_Download_Path,String csvFileToRead) throws InterruptedException, AWTException, IOException
	{
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, ProjLev_ExptoCSV_Button, 30);
		//PrivateProjectsTab.click();
		//Log.message("Private Projects button has been clicked!!!");
		SkySiteUtils.waitTill(5000);
		
		//Getting count of available projects
		int Avl_Projects_Count=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'project-icon')]"));
		for (WebElement Element : allElements)
		{ 
			Avl_Projects_Count =Avl_Projects_Count+1; 	
		}
		Log.message("Available private projects count is: "+Avl_Projects_Count);
		SkySiteUtils.waitTill(5000);
		
		//Getting Required values from app to compare with the values from downloaded csv file.
		String ExpProjName = driver.findElement(By.xpath("//div[@class='prj-info']/h4/span")).getText();
		Log.message("Expected Project Name From APP is: "+ExpProjName);
		String ExpProjOwnerName = driver.findElement(By.xpath("//div[@class='project-box-footer']/div[1]/span")).getText();
		Log.message("Expected Project Owner Name From APP is: "+ExpProjOwnerName);	
		//String ExpProjSize = driver.findElement(By.xpath("//li["+Avl_Projects_Count+"]/div[1]/section[1]/div/span[2]")).getText();
		//Log.message("Expected Project Size From APP is: "+ExpProjSize);
		
		//Calling "Deleting download folder files" method
		this.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(5000);
		
		ProjLev_ExptoCSV_Button.click();//Click on Export to csv
		Log.message("Export to Csv has been clicked!!!");
		SkySiteUtils.waitTill(20000);
		
		//Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: "+browserName);
		
		if(browserName.contains("firefox"))
		{
		//Handling Download PopUp of firefox browser using robot
			Robot robot=null;
			robot=new Robot();
			
			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);
		}
		
		
	//After export projects, checking whether exported file is existed under download folder or not 
		String ActualFilename=null;
		File[] files = new File(Sys_Download_Path).listFiles();
					
		for(File file : files)
		{
			if(file.isFile()) 
			{
				ActualFilename=file.getName();//Getting File Names into a variable
				Log.message("Actual File name is:"+ActualFilename);
				SkySiteUtils.waitTill(1000);	
				if(ActualFilename.contains("Documents"))						
				{
					Log.message("Downloaded csv file is available in downloads!!!");							
					break;
				}
							
			}
						
		}
		
		//Validating the CSV file from download folder	 
		BufferedReader br = null;
		String line = null; 
		String splitBy = ",";
		int count = 0;
		String ActProjName = null;
		String ActProjOwnerName = null;
		String ActProjSize = null;
		int Match_Count = 0;
		
		br = new BufferedReader(new FileReader(csvFileToRead)); 
		while ((line = br.readLine()) != null) 
		{  
			count=count+1;
			Log.message("Downloaded csv file have data in : "+count+ " rows");
			String[] ActValue = line.split(splitBy);
			ActProjName = ActValue[1];
			ActProjName = ActProjName.replace("\"", "");
			Log.message("Project Name from csv file is: "+ ActProjName);
			ActProjOwnerName = ActValue[8];
			ActProjOwnerName = ActProjOwnerName.replace("\"", "");
			Log.message("Project Owner Name from csv file is: "+ ActProjOwnerName);
			ActProjSize = ActValue[11];
			ActProjSize = ActProjSize.replace("\"", "");
			Log.message("Project size from csv file is: "+ ActProjSize);
				
			if((ActProjName.contentEquals(ExpProjName))	&& (ActProjOwnerName.contentEquals(ExpProjOwnerName)))
			{
				Log.message("Projects Exported File is having data properly!!!");
				Match_Count = Match_Count+1;
			}
		}
		Log.message("Downloaded csv file have data in : "+count+ " rows");
		Avl_Projects_Count=Avl_Projects_Count+1;
		
		br.close();//Newly Added
		
		if((count==Avl_Projects_Count)&&(Match_Count == 1))
		{
			return true;
		}
		else
		{
			Log.message("Projects Exported File is NOT having data properly!!!");
			return false;
		}
	
	}
	
	
	/** 
	 * Method written for validating Export to csv by editing all the fields of a project.
	 * Scripted By : NARESH BABU KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	public boolean Validate_ModifiedProject_Export(String Sys_Download_Path, String csvFileToRead) throws InterruptedException, AWTException, IOException
	{
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, PrivateProjectsTab, 30);
		//PrivateProjectsTab.click();
		WebElement EditLink = PrivateProjectsTab;
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",EditLink);//JSExecutor to click projects
		Log.message("Private Projects button has been clicked!!!");
		SkySiteUtils.waitTill(5000);
		
		//Getting count of available projects
		int Avl_Projects_Count=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'project-icon')]"));//Naresh
		for (WebElement Element : allElements)
		{ 
			Avl_Projects_Count =Avl_Projects_Count+1; 	
		}
		Log.message("Available private projects count is: "+Avl_Projects_Count);
		SkySiteUtils.waitTill(5000);
		
		//Generating all input values randomly
		String Random_Number = Generate_Random_Number.generateRandomValue();
		String Edit_projectName = "Prj_"+Random_Number;
		String Edit_projectNumber = Random_Number;
		String Edit_Description = "Description_"+Random_Number;
		String Edit_SiteAddress = "SiteAddress_"+Random_Number;
		String Edit_City = "City_"+Random_Number;
		String Edit_state = null;
		String Edit_Country = null;
		String Edit_Zip = Random_Number;
		String Edit_Password = Random_Number;
		
		if(Avl_Projects_Count>=1)
		{
			//Edit 1st located project
//			Modified by subhagat
			
			WebElement morebtn = Prj_MoreOptions;
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",Prj_MoreOptions);//JSExecutor to click more btn
			Log.message("More btn clicked!!!");
			SkySiteUtils.waitTill(5000);
			
//			Prj_MoreOptions.click();//Click on 1st project More options
//			SkySiteUtils.waitTill(5000);
			//SkySiteUtils.waitForElement(driver, Edit_Tab_FirstPrj, 30000);
			//WebElement EditLink1 = driver.findElement(By.xpath("(//a[contains(text(),'Edit')])[1]"));
			//((JavascriptExecutor) driver).executeScript("arguments[0].click();",EditLink1);//JSExecutor to click edit
			driver.findElement(By.xpath("(//a[@class='edit-project'])[1]")).click();
			Log.message("Clicked on Edit tab of a project.");
			SkySiteUtils.waitForElement(driver, btnCreate, 30);//Wait for update project button
			
			txtProjectName.clear();//Clear the existed project name
			txtProjectName.sendKeys(Edit_projectName);//Enter new project name
		
			txtProjectNumber.clear();//Clear the existed Project Number
			txtProjectNumber.sendKeys(Edit_projectNumber);//Enter Project Number
			
			txtProjectDescription.clear();//Clear the existed Description
			txtProjectDescription.sendKeys(Edit_Description);//Enter Project Description
		
			txtAddress.clear();//Clear the existed Site Address
			txtAddress.sendKeys(Edit_SiteAddress);//Enter Site Address
		
			txtCity.clear();//Clear Existed City
			txtCity.sendKeys(Edit_City);//Enter City
		
			//Generating a random value in between 1 to 200
			int Max = 201;
			int Min = 1;
			//Create Instance of Random Class
			Random ranomNum = new Random();
			int Gen_Random_Val = Min + ranomNum.nextInt(Max);
			Log.message("Genarated Random Value is: "+Gen_Random_Val);
			
			driver.findElement(By.xpath("//div[@id='ddlCountrydrop']")).click();
			SkySiteUtils.waitTill(2000);
			driver.findElement(By.xpath("(//a[@class='Country-Item'])["+Gen_Random_Val+"]")).click();//Select Any Country
			SkySiteUtils.waitTill(2000);
			Edit_Country = txtCountry.getAttribute("value");
			Log.message("Edit Country is: "+Edit_Country);
		
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath("//div[@id='ddlstatedrop']")).click();
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath("(//a[@class='State-Item'])[1]")).click();//Select Any State
			SkySiteUtils.waitTill(2000);
			Edit_state = txtState.getAttribute("value");
			Log.message("Edit State is: "+Edit_state);
		
			txtZip.clear();//Clear Existed Zip
			txtZip.sendKeys(Edit_Zip);//Enter Zip
			
			txtProjectPassword.clear();//Clear Existed Password
			txtProjectPassword.sendKeys(Edit_Password);//Enter Password
			
			btnCreate.click();//Click on Create Project button
			Log.message("Clicked on Update Project Button!!!");
			SkySiteUtils.waitTill(1000);
			
			SkySiteUtils.waitForElement(driver, projectCreationSuccessMsg, 30);//Wait until message get displayed
			String MsgAfterProjEdit = projectCreationSuccessMsg.getText();//Getting Notification message
			
			Log.message("Notification message After project edit is:"+MsgAfterProjEdit);
			SkySiteUtils.waitTill(5000);
	}
		
		//Calling "Deleting download folder files" method
		this.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(5000);
		
		//ProjLev_ExptoCSV_Button.click();//Click on Export to csv
		WebElement EditLink2 = ProjLev_ExptoCSV_Button;
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",EditLink2);//JSExecutor to click Export
		Log.message("Export to Csv has been clicked!!!");
		SkySiteUtils.waitTill(20000);
		
		//Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: "+browserName);
		
		if(browserName.contains("firefox"))
		{
		//Handling Download PopUp of firefox browser using robot class
			Robot robot=null;
			robot=new Robot();
			
			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);
		}
		
	//After export projects, checking whether exported file is existed under download folder or not 
		String ActualFilename=null;
		File[] files = new File(Sys_Download_Path).listFiles();
					
		for(File file : files)
		{
			if(file.isFile()) 
			{
				ActualFilename=file.getName();//Getting File Names into a variable
				Log.message("Actual File name is:"+ActualFilename);
				SkySiteUtils.waitTill(1000);	
				if(ActualFilename.contains("Documents"))						
				{
					Log.message("Downloaded csv file is available in downloads!!!");							
					break;
				}
							
			}
						
		}
		
		//Validating the CSV file from download folder	
		BufferedReader br = null;
		String line = null; 
		String splitBy = ",";
		int count = 0;
		String ActProjName = null;
		String ActProjNum = null;
		String ActDescription = null;
		String ActSiteAddress = null;
		String ActCity = null;
		String ActZip = null;
		String ActCountry = null;
		String ActState = null;
		String ActPasswordStatus = null;
		int Match_Count = 0;
		
		br = new BufferedReader(new FileReader(csvFileToRead)); 
		while ((line = br.readLine()) != null) 
		{  
			count=count+1;
			String[] ActValue = line.split(splitBy);
			ActProjNum = ActValue[0].replace("\"", "");
			//Log.message("Project Number from csv file is: "+ ActProjNum);
			
			ActProjName = ActValue[1].replace("\"", "");
			//Log.message("Project Name from csv file is: "+ ActProjName);
			
			ActSiteAddress = ActValue[2].replace("\"", "");
			//Log.message("Project Site Address from csv file is: "+ ActSiteAddress);
						
			ActCity = ActValue[3].replace("\"", "");
			//Log.message("Project City from csv file is: "+ ActCity);
						
			ActZip = ActValue[4].replace("\"", "");
			//Log.message("Project Zip from csv file is: "+ ActZip);
						
			ActDescription = ActValue[5].replace("\"", "");
			//Log.message("Project Description from csv file is: "+ ActDescription);
						
			ActState = ActValue[6].replace("\"", "");
			//Log.message("Project State from csv file is: "+ ActState);
			
			ActCountry = ActValue[7].replace("\"", "");
			//Log.message("Project Country from csv file is: "+ ActCountry);
			
			ActPasswordStatus = ActValue[10].replace("\"", "");
			//Log.message("Project Password Status from csv file is: "+ ActPasswordStatus);
			
			//Validate the download csv file
			if((ActProjNum.contentEquals(Edit_projectNumber))&&(ActProjName.contentEquals(Edit_projectName))&&(ActSiteAddress.contentEquals(Edit_SiteAddress))
					&&(ActCity.contentEquals(Edit_City))&&(ActZip.contentEquals(Edit_Zip))&&(ActDescription.contentEquals(Edit_Description))
					&&(ActState.contentEquals(Edit_state))&&(ActCountry.contentEquals(Edit_Country))&&(ActPasswordStatus.equalsIgnoreCase("True")))
			{
				Log.message("All Modified Project details are available in Exported csv File!!!");
				Match_Count=Match_Count+1;
			}
			
		}
		br.close();//Newly Added		
		
		if(Match_Count==1)
		{
			return true;
		}
		else
		{
			Log.message("All Modified Project details are NOT available in Exported csv File!!!");
			return false;
		}
	
	}
	
	
	/** 
	 * Method written for validating Search a project and Export to csv.
	 * Scripted By : NARESH BABU KAVURU
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */

	public boolean Validate_SearchAProject_Export(String Sys_Download_Path,String csvFileToRead) throws InterruptedException, AWTException, IOException
	{
		SkySiteUtils.waitTill(30000);
		SkySiteUtils.waitForElement(driver, PrivateProjectsTab, 30);
		//PrivateProjectsTab.click();
		WebElement EditLink = PrivateProjectsTab;
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",EditLink);//JSExecutor to click edit
		Log.message("Private Projects button has been clicked!!!");
		SkySiteUtils.waitTill(5000);
	//Getting count of available projects
		int Avl_Projects_Count=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'project-icon')]"));
		for (WebElement Element : allElements)
		{ 
			Avl_Projects_Count =Avl_Projects_Count+1; 	
		}
		Log.message("Available private projects count is: "+Avl_Projects_Count);
		SkySiteUtils.waitTill(5000);
		
		boolean Results = false;
		if(Avl_Projects_Count>=1)
		{
			//Changes done on 5th Jun
			String First_Prj_Name = driver.findElement(By.xpath("(//span[@data-bind='text: ProjectName()'])[1]")).getText();
			txtGlobalSearch.sendKeys(First_Prj_Name);//Search with 1st Project
			bunSearch.click();//Click on search button
			SkySiteUtils.waitTill(10000);
			Results = this.Validate_ProjectExport(Sys_Download_Path,csvFileToRead);//Calling Export Validation Method
		}
		
		if(Results==true)
		{
			Log.message("Export a Project after search and validate is success!!!");
			return true;
		}
		else
		{
			Log.message("Export a Project after search and validate is FAILED!!!");
			return false;
		}
	}

	/** 
	 * Method written for selecting a project.
	 * Scripted By : Ranjan
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */

	public FolderPage ValidateSelectProject(String Prj_Name) throws InterruptedException, AWTException, IOException
	{
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, GlobalSearch,120);
		GlobalSearch.sendKeys(Prj_Name);
		Log.message("Entered Value In Global Search field");
		SkySiteUtils.waitTill(8000);
		SkySiteUtils.waitForElement(driver, ButtonSearch, 120);
		ButtonSearch.click();
		Log.message("Button Search Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		int Avl_Projects_Count=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'project-icon')]"));
		for (WebElement Element : allElements)
		{ 
			Avl_Projects_Count =Avl_Projects_Count+1; 	
		}
		Log.message("Available private projects count is: "+Avl_Projects_Count);
		SkySiteUtils.waitTill(5000);
		
		for(int i=1;i<=Avl_Projects_Count;i++)
		{
			String Exp_ProjName = driver.findElement(By.xpath("(//span[@data-bind='text: ProjectName()'])["+i+"]")).getText();
			
	      //Validating - Expected project is selected or not
			if(Exp_ProjName.trim().contentEquals(Prj_Name.trim()))
			{  	
				Log.message("Maching Project Found!!");
				driver.findElement(By.xpath("(//span[@data-bind='text: ProjectName()'])["+i+"]")).click();
				Log.message("Clicked on expected project!!");
				SkySiteUtils.waitTill(20000);
				break; 				
			}

		}
		return new FolderPage(driver).get();
	}

	/** 
	 * Method written for selecting a project.
	 * Scripted By : NARESH
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	public FolderPage Validate_SelectAProject(String Prj_Name) throws InterruptedException, AWTException, IOException
	{
		SkySiteUtils.waitForElement(driver, PrivateProjectsTab,60);
		PrivateProjectsTab.click();
		Log.message("Private Projects button has been clicked!!!");
		SkySiteUtils.waitTill(5000);
	//Getting count of available projects
		int Avl_Projects_Count=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'project-icon')]"));
		for (WebElement Element : allElements)
		{ 
			Avl_Projects_Count =Avl_Projects_Count+1; 	
		}
		Log.message("Available private projects count is: "+Avl_Projects_Count);
		//SkySiteUtils.waitTill(5000);
		
		for(int i=1;i<=Avl_Projects_Count;i++)
		{
			String Exp_ProjName = driver.findElement(By.xpath("(//span[@data-bind='text: ProjectName()'])["+i+"]")).getText();
			
	//Validating - Expected project is selected or not
			if(Exp_ProjName.trim().contentEquals(Prj_Name.trim()))
			{  	
				Log.message("Maching Project Found!!");
				driver.findElement(By.xpath("(//span[@data-bind='text: ProjectName()'])["+i+"]")).click();
				Log.message("Clicked on expected project!!");
				SkySiteUtils.waitTill(5000);
				break; 				
			}

		}
		return new FolderPage(driver).get();
	}

	/** 
	 * Method written for selecting a project from Shared projects list.
	 * Scripted By : NARESH
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	public FolderPage Validate_Select_SharedProject(String Prj_Name) throws InterruptedException, AWTException, IOException
	{
		SkySiteUtils.waitForElement(driver, GuestProjectsTab,120);
		GuestProjectsTab.click();
		Log.message("Guest Projects button has been clicked!!!");
		SkySiteUtils.waitTill(5000);
	//Getting count of available projects
		int Avl_Projects_Count=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'project-icon')]"));
		//List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='ProjectlistContainer']//div[@id='view']//div//div[@class='project-content']//h4"));
		for (WebElement Element : allElements)
		{ 
			Avl_Projects_Count =Avl_Projects_Count+1; 	
		}
		Log.message("Available private projects count is: "+Avl_Projects_Count);
		
		for(int i=1;i<=Avl_Projects_Count;i++)
		{
			String Exp_ProjName = driver.findElement(By.xpath("(//span[@data-bind='text: ProjectName()'])["+i+"]")).getText();
			//modified
			//String   Exp_ProjName=driver.findElement(By.xpath("//h4[@title='Acc_Submit_Temp1']")).getText();
			//String   Exp_ProjName=driver.findElement(By.xpath("//div[@id='ProjectlistContainer']//div[@id='view']//div["+i+"]//div[@class='project-content']//h4")).getText();
	//Validating - Expected project is selected or not
			if(Exp_ProjName.trim().contentEquals(Prj_Name.trim()))
			{  	
				Log.message("Maching Project Found!!");
				driver.findElement(By.xpath("(//span[@data-bind='text: ProjectName()'])["+i+"]")).click();
				//modfied
				//driver.findElement(By.xpath("//div[@id='ProjectlistContainer']//div[@id='view']//div[\"+i+\"]//div[@class='project-content']//h4")).click();
				Log.message("Clicked on expected project!!");
				SkySiteUtils.waitTill(5000);
				break; 				
			}

		}
		return new FolderPage(driver).get();
	}
	
	/** 
	 * Method written for selecting a Contacts button from top of the Dash board page.
	 * Scripted By : NARESH
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */

	public ContactsPage Validate_SelectContactsBut() throws InterruptedException, AWTException, IOException
	{
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, btnContacts, 30);
		Log.message("Waiting for Contacts button to be appeared on top of Dashboard");
		btnContacts.click();
		Log.message("Clicked on Contacts button");
		SkySiteUtils.waitForElement(driver, btnAddNewContact, 20);
		SkySiteUtils.waitTill(3000);
		
		return new ContactsPage(driver).get();
	}
	
	public PublishingPage SimpleExecution()
	{
		boolean result = false;		
		//projectclick.click();
		SkySiteUtils.waitTill(10000);
		remainingproject.click();
		Log.message("Remaining Log Display button clicked.");
		
		SkySiteUtils.waitTill(2000);
		PublishButton.click();
		Log.message("Display Button clicked.");
		return new PublishingPage(driver).get();		
	}
	
	/** 
	 * Method written for Validate Send Files
	 * Scripted By: Naresh Babu 
	 * @return
	 */
	public boolean validate_SendFilesModule(String FolderPath, String Rec_MailID, int FileCount) throws InterruptedException, AWTException, IOException
	{
		boolean result1 = false;
		boolean result2 = false;
		
		SkySiteUtils.waitForElement(driver, iconSendFiles, 20);
		iconSendFiles.click();
		Log.message("Clicked on Send Files icon from the dashboard.");
		SkySiteUtils.waitForElement(driver, btnSend, 30);
		SkySiteUtils.waitTill(5000);
		btnChooseFiles.click();
		Log.message("Clicked on Choose Files button.");
		SkySiteUtils.waitTill(10000);
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));

		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();

		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into
				// a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(500);
			}
		}

		output.flush();
		output.close();

		// Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path +" "+ FolderPath+" "+tmpFileName );
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(20000);
		SkySiteUtils.waitForElement(driver, btnSend, 30);
		try {
			File file = new File(tmpFileName);
			if (file.delete()) {
				Log.message(file.getName() + " is deleted!");
			} else {
				Log.message("Delete operation is failed.");
			}
		} catch (Exception e) {
			Log.message("Exception occured!!!" + e);

		}
		
		textToEmail.click();
		textToEmailEnter.sendKeys(Rec_MailID);
		Log.message("To email was entered.");
		textSubject.sendKeys("Testing from QA Team!!!");
		Log.message("Subject was entered.");
		txtMessage.sendKeys("Testing from QA Team!!!");
		Log.message("Message was entered.");
		btnSend.click();
		Log.message("Clicked on Send button.");
		SkySiteUtils.waitForElement(driver, projectCreationSuccessMsg, 60);
		SkySiteUtils.waitTill(1000);
		String Message_AfterSendFiles = projectCreationSuccessMsg.getText();
		Log.message("Notification message after sending files is: "+Message_AfterSendFiles);
		//Split the notification message for getting ID number
		String[] splitStr = Message_AfterSendFiles.split("\\s+");
		String ExactID_FromMessage = splitStr[6];
		Log.message("Order Id from notification Message is ----------------->"+ExactID_FromMessage);
		SkySiteUtils.waitTill(3000);
		System.setProperty("RuntimeOrderNumber", ExactID_FromMessage);//Keeping a runtime value in system properties for future use
		tabTracking.click();
		Log.message("Clicked on Tracking Tab.");
		SkySiteUtils.waitForElement(driver, txtTrackingDetails, 30);
		SkySiteUtils.waitTill(5000);
		String Tracking_ID = txtTrackingDetails.getText();
		Log.message("Tracking id is: "+Tracking_ID);
		int Upload_PassCount = 0;
		if(Tracking_ID.contains(ExactID_FromMessage))
		{
			result1=true;
			Log.message("Found Matching Tracking id.");
			txtTrackingDetails.click();
			Log.message("Clicked on Tracking id to validate the details.");
			SkySiteUtils.waitForElement(driver, windDetailsofTrackId, 30);
			SkySiteUtils.waitTill(3000);
			for(int i=1;i<=FileCount;i++)
			{
				String File_Status = driver.findElement(By.xpath(".//*[@id='tracking-details-modal']/div/div/div[2]/div/div[1]/div[3]/div/ul/li[1]/section[2]/h5/span[2]")).getText();
				//Log.message("File status from details page is: "+File_Status);
				if(File_Status.equalsIgnoreCase("Upload Success"))
				{
					Upload_PassCount=Upload_PassCount+1;
				}
			}
			if(Upload_PassCount==FileCount)
			{
				result2=true;
				Log.message("All the files are uploaded success in the details page.");
				driver.findElement(By.xpath("(//button[@class='close'])[2]")).click();//Closing Track details window
			}
			else
			{
				result2=false;
				Log.message("All the files are NOT uploaded success in the details page.");
			}
		}
		else
		{
			result1=false;
			Log.message("NOT Found Matching Tracking id.");
		}
		
		if((result1==true) && (result2==true))
		{
			Log.message("Send files functionality is working successfully.");
			return true;
		}
		else
		{
			Log.message("Send files functionality is working successfully.");
			return false;
		}
	}
	
	/** 
	 * Method written for Validate Send Files details from the Inbox
	 * Scripted By: Naresh Babu 
	 * @return
	 */
	public boolean validate_SendFiles_Details_Inbox() throws InterruptedException, AWTException, IOException
	{	
		SkySiteUtils.waitForElement(driver, iconSendFiles, 20);
		iconSendFiles.click();
		Log.message("Clicked on Send Files icon from the dashboard.");
		SkySiteUtils.waitForElement(driver, btnSend, 30);
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, txtOrderInbox, 30);
		String ID_Inbox=txtOrderInbox.getText();
		Log.message("Order ID from the Inbox is: "+ID_Inbox);
		String Exp_OrderId = System.getProperty("RuntimeOrderNumber");
		Log.message("Expected Id getting from system properties is: "+Exp_OrderId);
		if(ID_Inbox.contains(Exp_OrderId))
		{
			Log.message("Expected order is available under Inbox List.");
			return true;
		}
		else
		{
			Log.message("Expected order is NOT available under Inbox List.");
			return false;
		}
		
	}
	
	/** 
	 * Method written for Validate Zip Download from the Inbox
	 * Scripted By: Naresh Babu 
	 * @return
	 */
	public boolean validate_ZipDownload_SendFilesInbox(String Sys_Download_Path) throws InterruptedException, AWTException, IOException
	{	
		boolean result=false;
		SkySiteUtils.waitForElement(driver, iconSendFiles, 20);
		iconSendFiles.click();
		Log.message("Clicked on Send Files icon from the dashboard.");
		SkySiteUtils.waitForElement(driver, btnSend, 30);
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, txtOrderInbox, 30);
		String ID_Inbox=txtOrderInbox.getText();
		Log.message("Order ID from the Inbox is: "+ID_Inbox);
		//Split the Inbox text for getting ID number
		String[] splitStr = ID_Inbox.split("\\s+");
		String ExactID_FromInbox = splitStr[1];
		Log.message("Order Id from Inbox is ----------------->"+ExactID_FromInbox);
		System.setProperty("OrderNumberInbox", ExactID_FromInbox);//Keeping a runtime value in system properties for future use
		//Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//a[@id='Inboxdownload'])[1]")).click();
		Log.message("Clicked on ZIP download of an order");
		SkySiteUtils.waitTill(50000);
		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);

		if (browserName.contains("firefox")) 
		{
			// Handling Download PopUp of firefox browser using robot
			Robot robot = null;
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);					
		}
		// After Download, checking whether Downloaded file is existed under download folder or not
		String ActualFilename = null;
		File[] files = new File(Sys_Download_Path).listFiles();

		for (File file : files) {
		if (file.isFile()) {
			ActualFilename = file.getName();// Getting File Names into a variable
			Log.message("Actual File name is:" + ActualFilename);
			SkySiteUtils.waitTill(1000);
			Long ActualFileSize = file.length();
			Log.message("Actual File size is:" + ActualFileSize);
			if ((ActualFilename.contains(ExactID_FromInbox)) && (ActualFileSize != 0)) 
			{
				Log.message("Downloaded file is available in downloads!!!");
				result = true;
				break;
			}
		}
		}		
		if(result==true)
		{
			Log.message("Zip download is success from Inbox List.");
			return true;
		}
		else
		{
			Log.message("Zip download is Failed from Inbox List.");
			return false;
		}
		
	}
	
	/** 
	 * Method written for Validate Single File Download from Inbox details window
	 * Scripted By: Naresh Babu 
	 * @return
	 */
	public boolean validate_FileDownload_SendFilesDetailsWindow(String Sys_Download_Path) throws InterruptedException, AWTException, IOException
	{	
		boolean result=false;
		SkySiteUtils.waitForElement(driver, iconSendFiles, 20);
		iconSendFiles.click();
		Log.message("Clicked on Send Files icon from the dashboard.");
		SkySiteUtils.waitForElement(driver, btnSend, 30);
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, txtOrderInbox, 30);
		txtOrderInbox.click();
		Log.message("Clicked on order number to open details window.");
		SkySiteUtils.waitForElement(driver, headerOrderDetailsWin, 30);
		//Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(5000);
		String FirstFileName=driver.findElement(By.xpath(".//*[@id='sender-details-modal']/div/div/div[2]/div/div[3]/div/ul/li[1]/section[2]/h4")).getText();
		driver.findElement(By.xpath(".//*[@id='sender-details-modal']/div/div/div[2]/div/div[3]/div/ul/li[1]/section[4]/ul/li/a/i")).click();
		Log.message("Clicked on File download icon from order details page");
		SkySiteUtils.waitTill(20000);
		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);

		if (browserName.contains("firefox")) 
		{
			// Handling Download PopUp of firefox browser using robot
			Robot robot = null;
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);					
		}
		// After Download, checking whether Downloaded file is existed under download folder or not
		String ActualFilename = null;
		File[] files = new File(Sys_Download_Path).listFiles();

		for (File file : files) {
		if (file.isFile()) {
			ActualFilename = file.getName();// Getting File Names into a variable
			Log.message("Actual File name is:" + ActualFilename);
			SkySiteUtils.waitTill(1000);
			Long ActualFileSize = file.length();
			Log.message("Actual File size is:" + ActualFileSize);
			if ((ActualFilename.contains(FirstFileName)) && (ActualFileSize != 0)) 
			{
				Log.message("Downloaded file is available in downloads!!!");
				result = true;
				break;
			}
		}
		}		
		if(result==true)
		{
			driver.findElement(By.xpath("(//button[@class='close'])[2]")).click();
			Log.message("File download is success from Details Window.");
			return true;
		}
		else
		{
			Log.message("File download is Failed from Details Window.");
			return false;
		}
		
	}
	
	/** 
	 * Method written for Validate tracking details from Sender side
	 * Scripted By: Naresh Babu 
	 * @return
	 */
	public boolean validate_DownloadTrackingDetail() throws InterruptedException, AWTException, IOException
	{	
		boolean result1=false;
		boolean result2=false;
		SkySiteUtils.waitForElement(driver, iconSendFiles, 20);
		iconSendFiles.click();
		Log.message("Clicked on Send Files icon from the dashboard.");
		SkySiteUtils.waitForElement(driver, btnSend, 30);
		SkySiteUtils.waitTill(2000);
		tabTracking.click();
		Log.message("Clicked on Tracking Tab.");
		SkySiteUtils.waitForElement(driver, txtTrackingDetails, 30);
		SkySiteUtils.waitTill(5000);
		String ID_Tracking = txtTrackingDetails.getText();
		Log.message("Order ID from the Tracking is: "+ID_Tracking);
		String Exp_OrderID = System.getProperty("OrderNumberInbox");//Getting System Property value from the before method
		if(ID_Tracking.contains(Exp_OrderID))
		{
			result1=true;
			Log.message("Expected order is available under Tracking.");
			txtTrackingDetails.click();
			Log.message("Clicked on order number to open details window.");
			SkySiteUtils.waitForElement(driver, headerOrderDetailsWin, 30);
			SkySiteUtils.waitTill(3000);
			String DownloadItemCount=driver.findElement(By.xpath("html/body/div[1]/div[6]/div/div/div[2]/div/div[2]/h4")).getText();
			Log.message("Download history from the details page is: "+DownloadItemCount);
			if(DownloadItemCount.contentEquals("Downloaded 2 times"))
			{
				result2=true;
				Log.message("Download tracking is showing properly.");
				driver.findElement(By.xpath("(//button[@class='close'])[2]")).click();//Click on Cancel Button of details window
			}
			else
			{
				result2=false;
				Log.message("Download tracking is NOT showing properly.");
			}
		}
		else
		{
			result1=false;
			Log.message("Expected order is NOT available under Tracking.");
		}
		if((result1==true)&&(result2==true))
			return true;
		else
			return false;
		
	}
	
	/** 
	 * Method written for Validate Create OrderPrints and validate the Details
	 * Scripted By: Naresh Babu 
	 * @return
	 */
	public boolean validate_Create_OrderPrints_AndDetails(int fileCount) throws InterruptedException, AWTException, IOException
	{	
		boolean result1=false;
		boolean result2=false;
		
		String ContactName_OrderPrint=null;
		String CompanyName_OrderPrint=null;
		String Email_OrderPrint=null;
		String address_OrderPrint=null;
		String Phone_OrderPrint=null;
		String ExpectedDeliveryTime=null;
			
		String ServProviderMailId=dataServiceProviderMail.getText();
		Log.message("Service Provider MailId is: "+ServProviderMailId);
		
	//Selected files validation in Order Print Page		
		String NoOfFilesinQueue = Integer.toString(fileCount);//Converting Integer to string
		String ExpQueueTitlewithCount = NoOfFilesinQueue+" file(s)";
		Log.message("Expected Queue title with file count is: "+ExpQueueTitlewithCount);
		String ActQueueTitlewithCount = dataPrintQueue.getText();
		Log.message("Actual Queue title with file count is: "+ActQueueTitlewithCount);
			
	//Checking each file contains the DELETE icon or not
		int DeleteIconsCount=0;
		List<WebElement> allElements1 = driver.findElements(By.xpath("//*[contains(@id, 'sectionDelete')]"));
		for (WebElement Element : allElements1)
		{ 
			DeleteIconsCount = DeleteIconsCount+1; 	
		}
		Log.message("Available Delete icons count is: "+DeleteIconsCount);
			
		if((DeleteIconsCount==fileCount) && (ActQueueTitlewithCount.contentEquals(ExpQueueTitlewithCount)))
		{
			Log.message("All Files are proper in Order Print page!!!");
		//Time validation in Order Print Page
			String PrintProvCurrTimeDetails=driver.findElement(By.xpath(".//*[@id='inforPrintOrder']/div[1]/div[1]/ul/li[1]/div[2]/h5[5]")).getText();
		//Split the current Print Provider date for getting only time
			String[] splitStr = PrintProvCurrTimeDetails.split("\\s+");
			String OnlyTime = splitStr[5];
			Log.message("The required value is: "+OnlyTime);				
			String[] x= OnlyTime.split(":");
			String OnlyHours=x[0];
			String OnlyMins=x[1];
			Log.message("Only Hours value is: "+OnlyHours);
					
			if(OnlyHours.equals("11"))
			{
				ExpectedDeliveryTime="01:"+OnlyMins;
				Log.message("Expected Delivery Time when =11 is: "+ExpectedDeliveryTime);
			}
			else if(OnlyHours.equals("12"))
			{
				ExpectedDeliveryTime="02:"+OnlyMins;
				Log.message("Expected Delivery Time when =12 is: "+ExpectedDeliveryTime);					
			}
			else
			{
				int Hours = Integer.parseInt(OnlyHours);
				Hours=Hours+2;
				//Log.message("After added 2 hours is:"+Hours);
				String Hrs_AfterAdded=null;
				if(Hours>=10)
				{
					Hrs_AfterAdded=Integer.toString(Hours);
					ExpectedDeliveryTime=Hrs_AfterAdded+":"+OnlyMins;
					Log.message("Expected Delivery Time when >=10 is: "+ExpectedDeliveryTime);
				}
				else
				{
					Hrs_AfterAdded=Integer.toString(Hours);
					ExpectedDeliveryTime="0"+Hrs_AfterAdded+":"+OnlyMins;
					Log.message("Expected Delivery Time when <10 is: "+ExpectedDeliveryTime);
				}
					
			}
						
			txtJobName.sendKeys("Ignore - Testing team ARC");
			Log.message("Entered Job Name.");
			txtDelInstruction.sendKeys("Ignore - Testing team ARC");
			Log.message("Entered Delivery Instructions.");			
			//Edit the Delivery Address Details
			driver.findElement(By.xpath(".//*[@id='edit-address']/i")).click();//Click on Edit Address
			SkySiteUtils.waitTill(3000);
			driver.findElement(By.xpath("//input[@id='txtContactName']")).clear();
			driver.findElement(By.xpath("//input[@id='txtContactName']")).sendKeys("PWPAutomation User");
			driver.findElement(By.xpath("//input[@id='txtCompanyName']")).clear();
			driver.findElement(By.xpath("//input[@id='txtCompanyName']")).sendKeys("ARC DOC SOLUTIONS");
			driver.findElement(By.xpath("//input[@id='txtEmail']")).clear();
			driver.findElement(By.xpath("//input[@id='txtEmail']")).sendKeys("pwpuser@rediffmail.com");
			driver.findElement(By.xpath("//input[@id='txtAddress']")).clear();
			driver.findElement(By.xpath("//input[@id='txtAddress']")).sendKeys("California - 50022");
			driver.findElement(By.xpath("//input[@id='txtPhone']")).clear();
			driver.findElement(By.xpath("//input[@id='txtPhone']")).sendKeys("9999999999");
			driver.findElement(By.xpath("//button[@id='_save']")).click();
			SkySiteUtils.waitTill(3000);
				
	//Delivery Address details after edit
			ContactName_OrderPrint=driver.findElement(By.xpath("//h4[@id='lblContactName']")).getText();
			Log.message("Contact Name_Order Print is: "+ContactName_OrderPrint);
			CompanyName_OrderPrint=driver.findElement(By.xpath("//h5[@id='lblCompanyName']")).getText();
			Log.message("Company Name_Order Print is: "+CompanyName_OrderPrint);
			Email_OrderPrint=driver.findElement(By.xpath("//a[@id='lblEmail']")).getText();
			Log.message("Email_Order Print is: "+Email_OrderPrint);
			address_OrderPrint=driver.findElement(By.xpath("//h5[@id='lblAddress1']")).getText();
			Log.message("address_Order Print is: "+address_OrderPrint);
			Phone_OrderPrint=driver.findElement(By.xpath("//h5[@id='lblPhone']")).getText();
			Log.message("Phone_Order Print is: "+Phone_OrderPrint);	
			String DeliveryTime=driver.findElement(By.xpath("//input[@id='txtDeliveryDate']")).getAttribute("value");
			Log.message("Delivery Time is: "+DeliveryTime);
				
	//Split the Before Delivery date for getting only time 
			String[] splitStr1 = DeliveryTime.split("\\s+");
			String OnlyDeliveryTime = splitStr1[1];
			Log.message("The required value is: "+OnlyDeliveryTime);
				
			if((OnlyDeliveryTime.equals(ExpectedDeliveryTime)) && (ContactName_OrderPrint.contentEquals("PWPAutomation User"))
					&& (CompanyName_OrderPrint.contentEquals("ARC DOC SOLUTIONS")) && (Email_OrderPrint.contentEquals("pwpuser@rediffmail.com"))
					&& (address_OrderPrint.contentEquals("California - 50022")) && (Phone_OrderPrint.contentEquals("9999999999")))
			{
				Log.message("Delivery time and Edited Delivery address is proper!!!");
				btnOrderPrintsSubmit.click();
				Log.message("Clicked on Order Prints button");
				SkySiteUtils.waitForElement(driver, btnPrintOrderConfPage, 30);
				//Validations on Order Print confirmation Page
				String ConfWithOrderId=txtOrderConfMsg.getText();
				Log.message("Confirmation With Order Id is: "+ConfWithOrderId);
				//Split to get order id
				String[] splitStr2 = ConfWithOrderId.split("\\s+");
				String OrderNumber = splitStr2[2];
				Log.message("The required Order Number is: "+OrderNumber);
				String ConfPageProviderMail=driver.findElement(By.xpath(".//*[@id='print-order-modal']/div/div/div[2]/div[2]/ul/li[2]/div/div[2]/a")).getText();
				String ConfPageDelName=driver.findElement(By.xpath(".//*[@id='print-order-modal']/div/div/div[2]/div[2]/ul/li[3]/div/div[2]/h4")).getText();
				String ConfPageDelCompany=driver.findElement(By.xpath(".//*[@id='print-order-modal']/div/div/div[2]/div[2]/ul/li[3]/div/div[2]/h5[1]")).getText();
				String ConfPageDelEmail=driver.findElement(By.xpath(".//*[@id='print-order-modal']/div/div/div[2]/div[2]/ul/li[3]/div/div[2]/h5[2]/a")).getText();
				String ConfPageDelAddress=driver.findElement(By.xpath(".//*[@id='print-order-modal']/div/div/div[2]/div[2]/ul/li[3]/div/div[2]/h5[3]")).getText();
				String ConfPageDelPhone=driver.findElement(By.xpath(".//*[@id='print-order-modal']/div/div/div[2]/div[2]/ul/li[3]/div/div[2]/h5[4]")).getText();
					
				if((ConfPageProviderMail.contentEquals(ServProviderMailId)) && (ConfPageDelName.contentEquals(ContactName_OrderPrint))
						&& (ConfPageDelCompany.contentEquals(CompanyName_OrderPrint)) && (ConfPageDelEmail.contentEquals(Email_OrderPrint))
						&& (ConfPageDelAddress.contentEquals(address_OrderPrint)) && (ConfPageDelPhone.contentEquals(Phone_OrderPrint))
						&& (ConfWithOrderId.contains("has successfully sent for printing.")))
				{
					result1=true;
					Log.message("Order Print Conformation page contains proper provider and Delivery details!!!");
					driver.findElement(By.xpath(".//*[@id='print-order-modal']/div/div/div[1]/button")).click();//Click on Close button
					Log.message("Clicked on close button of order details window.");
					SkySiteUtils.waitTill(2000);
					imgProfile.click();
					Log.message("Clicked on user profile Image.");
					SkySiteUtils.waitForElement(driver, tabMyOrders, 20);
					tabMyOrders.click();
					Log.message("Clicked on My Orders Tab");
					SkySiteUtils.waitForElement(driver, tabMyOrdersSort, 30);
					SkySiteUtils.waitTill(3000);
					//Validation in My Orders Page	
					String actualOrderNumber = driver.findElement(By.xpath("(//div[@class='orderPlcRgt']/h4)[1]")).getText();	
					/*//Split the order number
					String[] splitStr3 = actualOrderNumber.split("\\s+");
					String OrderSplit_MyOrders = splitStr3[1];*/
					Log.message("Exp Order Number is:"+OrderNumber);
					Log.message("Act Order Number is:"+actualOrderNumber);	
					String OrderStatus=driver.findElement(By.xpath("(//h4[@class='orderPlaced'])[1]")).getText();
					Log.message("The status of the current order is: "+OrderStatus);
					if(OrderNumber.trim().contains(actualOrderNumber.trim()) && (OrderStatus.equalsIgnoreCase("Order placed")))
					{  	
						result2=true;
						Log.message("Order details are available under My Orders List.");
					}
					else
					{
						result2=false;
						Log.message("Order details are NOT available under My Orders List.");
					}
				}
				else
				{
					result1=false;
					Log.message("Order Print Conformation page NOT contains proper provider and Delivery details!!!");
				}
			}
			else
			{
				Log.message("Delivery time and Edited Delivery address is NOT proper!!!");
			}	
		}
		else
		{
			Log.message("All Files are NOT proper in Order Print page!!!");
		}
		if((result1==true) && (result2==true))
			return true;
		else
			return false;
		
	}
	
	
	/** 
	 * Method written for Validate OrderPrints from the Inbox
	 * Scripted By: Naresh Babu 
	 * @return
	 */
	public boolean validate_OrderPrints_SendFilesInbox(int fileCount) throws InterruptedException, AWTException, IOException
	{	
		boolean result=false;
		SkySiteUtils.waitForElement(driver, iconSendFiles, 20);
		iconSendFiles.click();
		Log.message("Clicked on Send Files icon from the dashboard.");
		SkySiteUtils.waitForElement(driver, btnSend, 30);
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, txtOrderInbox, 30);
		String ID_Inbox=txtOrderInbox.getText();
		Log.message("Order ID from the Inbox is: "+ID_Inbox);
		//Split the Inbox text for getting ID number
		String[] splitStr = ID_Inbox.split("\\s+");
		String ExactID_FromInbox = splitStr[1];
		Log.message("Order Id from Inbox is ----------------->"+ExactID_FromInbox);
		driver.findElement(By.xpath("(//a[@id='InboxPrintOrder']/i)[1]")).click();
		Log.message("Clicked on Order Prints");
		SkySiteUtils.waitForElement(driver, btnOrderPrintsSubmit, 30);
		
		result = this.validate_Create_OrderPrints_AndDetails(fileCount);
			
		if(result==true)
		{
			Log.message("Order Prints from the Inbox list is working successfully.");
			return true;
		}
		else
		{
			Log.message("Order Prints from the Inbox list is NOT working.");
			return false;
		}	
	}

	/** 
	 * Method for entering a Project Test_Project_01
	 * Scripted By: Ranadeep
	 */
	
	public FolderPage enterProject() 
	{
		String actualProjectName=PropertyReader.getProperty("ProjectName");
		Log.message("Actual Project name is:- " +actualProjectName);
		
        for(WebElement Project : projectlist)
        	if(Project.getText().contains(actualProjectName))
        		SkySiteUtils.waitForElement(driver, txtProjectName, 30);
        		driver.findElement(By.xpath("//span[text()='Test_Project_01']/..")).click();
                Log.message("Clicked to enter required Project");
                
        return new FolderPage(driver).get();
	}

	/** 
	 * Method for entering a Project A_Test
	 * Scripted By: Ranadeep
	 */
	
	public FolderPage enterProject1() 
	{
		String actualProjectName=PropertyReader.getProperty("ProjectName2");
		Log.message("Actual Project name is:- " +actualProjectName);
		
        for(WebElement Project : projectlist)
        	if(Project.getText().contains(actualProjectName))
        		SkySiteUtils.waitForElement(driver, txtProjectName, 30);
        		driver.findElement(By.xpath("//span[text()='A_Test']/..")).click();
                Log.message("Clicked to enter required Project");
                
        return new FolderPage(driver).get();
	}
	
	
	/** 
	 * Method for entering a Project B_Test
	 * Scripted By: Ranadeep
	 */
	
	public FolderPage enterProject2() 
	{
		String actualProjectName=PropertyReader.getProperty("ProjectName3");
		Log.message("Actual Project name is:- " +actualProjectName);
		
        for(WebElement Project : projectlist)
        	if(Project.getText().contains(actualProjectName))
        		SkySiteUtils.waitForElement(driver, txtProjectName, 30);
        		driver.findElement(By.xpath("//span[text()='B_Test']/..")).click();
                Log.message("Clicked to enter required Project");
                
        return new FolderPage(driver).get();
	}
	
	/** 
	 * Method for entering a Project C_Test
	 * Scripted By: Ranadeep
	 */
	
	public FolderPage enterProject3() 
	{
		String actualProjectName=PropertyReader.getProperty("ProjectName4");
		Log.message("Actual Project name is:- " +actualProjectName);
		
        for(WebElement Project : projectlist)
        	if(Project.getText().contains(actualProjectName))
        		SkySiteUtils.waitForElement(driver, txtProjectName, 30);
        		driver.findElement(By.xpath("//span[text()='C_Test']/..")).click();
                Log.message("Clicked to enter required Project");
                
        return new FolderPage(driver).get();
	}
	
	
	/** 
	 * Method to enter any Project
	 * Scripted By: Trinanjwan
	 * @return 
	 */
	public FolderPage enteringanyProject(String projectname)
	
	{		
		SkySiteUtils.waitTill(20000);
		String projectpath = "//span[text()='%s']";
		SkySiteUtils.waitTill(10000);
		driver.findElement(By.xpath(String.format(projectpath, projectname))).click();
		Log.message(" Clicked on Project " + projectname + " to navigate inside ");
		
		return new FolderPage(driver).get();	
		
	}
	
	
	/** 
	 * Method for registration validation
	 * Scripted By: Ranadeep
	 * @return 
	 * Modified by Trinanjwan | 05-December-2018
	 */
	public boolean registrationValidation() 
	{
		
		SkySiteUtils.waitForElement(driver, btnCreateNewProjectWelcome, 30);
		if(btnCreateNewProjectWelcome.isDisplayed())
		{
			//btnCloseWelcomeVideo.click();
			SkySiteUtils.waitTill(15000);
			WebElement element = driver.findElement(By.xpath("//*[@id='closeWelcomeVideo']"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
			Log.message("Clicked to close Welcome video popover");
		}
		
		SkySiteUtils.waitForElement(driver, btnCreateProject, 30);
		if(btnCreateProject.isDisplayed())
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	/** 
	 * Method for entering into the viewer project from Project Dashboard
	 * Scripted By: Trinanjwan
	 */
	

	/** 
	 * Method for default sorting validation
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean defaultProjectSorting() 	
	{
		SkySiteUtils.waitForElement(driver, btnProjectSorting, 30);
		String actualDefaultSortingOrder= btnProjectSorting.getText();
		Log.message("Actual default Project sorting order is:- " +actualDefaultSortingOrder);
		
		String expectedDefaultSortingOrder= PropertyReader.getProperty("DEFAULTPROJECTSORTING");
		Log.message("Expected default Project sorting order is:- " +expectedDefaultSortingOrder);
		
		SkySiteUtils.waitForElement(driver, btnSortAscending, 30);
		Log.message("Default sorting is in ascending order");
		
		if(actualDefaultSortingOrder.contains(expectedDefaultSortingOrder) && btnSortAscending.isDisplayed())
		{
			return true;
		}
		else
		{
			return false;
		}
	
	}
	
	
	
	
	/** 
	 * Method for descending order sorting by Project Name
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean descendingOrderSortingByProjectName()  	
	{
		
		SkySiteUtils.waitForElement(driver, btnProjectSorting, 30);
		Log.message("Sort by button is displayed");
		btnProjectSorting.click();
		Log.message("Clicked on Project Sort by button");
		SkySiteUtils.waitForElement(driver, sortByProjectName, 20);
		sortByProjectName.click();
		Log.message("Clicked on sort by Project Name option");
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, btnSortAscending, 30);
		Log.message("Sort by ascending order button is displayed");
		
		ArrayList<String> obtainedList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//span[@data-bind='text: ProjectName()']"));
			    
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
		       obtainedList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Project count before sorting:- " +obtainedList.size());		 
		Log.message("Obtained Project list order is:- " +obtainedList);
		
		SkySiteUtils.waitForElement(driver, btnSortAscending, 30);
		btnSortAscending.click();
		Log.message("Clicked on ascending sorting button");
		SkySiteUtils.waitTill(8000);
		
		ArrayList<String> sortedList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//span[@data-bind='text: ProjectName()']"));
		    
	    for(WebElement we:elementList1)
	    {
	    	if(!we.getText().equals("")) 
			{
	    		sortedList.add(we.getText()); 
		     
			}
	    	  	
	    }
	    
	    Log.message("Sorted Project list order is:- " +sortedList);
	    Log.message("Total number of Projects after sorting is:- " +sortedList.size());
	    
	    if(obtainedList.get(0).contains(sortedList.get(sortedList.size()-1)) && obtainedList.get(obtainedList.size()-1).contains(sortedList.get(0)))
	    {	
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	    
	}
	
	
	
	/** 
	 * Method for ascending order sorting by Project Name
	 * Scripted By: Ranadeep
	 * @return 
	 */
	
	public boolean ascendingOrderSortingByProjectName() 
	{
		
		SkySiteUtils.waitForElement(driver, btnProjectSorting, 30);
		Log.message("Sort by button is displayed");
		btnProjectSorting.click();
		Log.message("Clicked on Project Sort by button");
		SkySiteUtils.waitForElement(driver, sortByProjectName, 20);
		sortByProjectName.click();
		Log.message("Clicked on sort by Project Name option");
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, btnSortAscending, 30);
		Log.message("Sort by ascending order button is displayed");
		btnSortAscending.click();
		Log.message("Clicked on ascending sorting button");
		SkySiteUtils.waitTill(8000);
		
		ArrayList<String> obtainedList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//span[@data-bind='text: ProjectName()']"));
			    
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
		       obtainedList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Project count before sorting:- " +obtainedList.size());		 
		Log.message("Obtained Project list order is:- " +obtainedList);
		
		SkySiteUtils.waitForElement(driver, btnSortDescending, 30);
		btnSortDescending.click();
		Log.message("Clicked on descending sorting button");
		SkySiteUtils.waitTill(8000);
		
		ArrayList<String> sortedList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//span[@data-bind='text: ProjectName()']"));
		    
	    for(WebElement we:elementList1)
	    {
	    	if(!we.getText().equals("")) 
			{
	    		sortedList.add(we.getText()); 
		     
			}
	    	  	
	    }
	    
	    Log.message("Sorted Project list order is:- " +sortedList);
	    Log.message("Total number of Projects after sorting is:- " +sortedList.size());
	    
	    if(obtainedList.get(0).contains(sortedList.get(sortedList.size()-1)) && obtainedList.get(obtainedList.size()-1).contains(sortedList.get(0)))
	    {	
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	    	
	}


	
	
	
	@FindBy(xpath="//span[contains(text(),'Viewer Project')]")
	WebElement Projecticon;
	
	public FolderPage enterviewerlevelProject()
	
	{	
		SkySiteUtils.waitForElement(driver, Projecticon , 15);
		Log.message("The Project is displayed now");
		Projecticon.click();
		Log.message("Clicked on Project icon to navigate inside the project");
		return new FolderPage(driver).get();			
	}

	
	/** 
	 * Method for descending order sorting by Project Number
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean descendingOrderSortingByProjectNumber()  	
	{
		
		SkySiteUtils.waitForElement(driver, btnProjectSorting, 30);
		Log.message("Sort by button is displayed");
		btnProjectSorting.click();
		Log.message("Clicked on Project Sort by button");
		SkySiteUtils.waitForElement(driver, sortByProjectNumber, 20);
		sortByProjectNumber.click();
		Log.message("Clicked on sort by Project Number option");
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, btnSortAscending, 30);
		Log.message("Sort by ascending order button is displayed");
		
		ArrayList<String> obtainedList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//div[@class='prj-id truncate-txt']/span"));
			    
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
		       obtainedList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Project count before sorting:- " +obtainedList.size());		 
		Log.message("Obtained Project numbers are:- " +obtainedList);
		
		SkySiteUtils.waitForElement(driver, btnSortAscending, 30);
		btnSortAscending.click();
		Log.message("Clicked on ascending sorting button");
		SkySiteUtils.waitTill(8000);
		
		ArrayList<String> sortedList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//div[@class='prj-id truncate-txt']/span"));
		    
	    for(WebElement we:elementList1)
	    {
	    	if(!we.getText().equals("")) 
			{
	    		sortedList.add(we.getText()); 
		     
			}
	    	  	
	    }
	    
	    Log.message("Sorted Project list order is:- " +sortedList);
	    Log.message("Total number of Projects after sorting is:- " +sortedList.size());
	    		
	    if(obtainedList.get(0).contains(sortedList.get(sortedList.size()-1)) && obtainedList.get(obtainedList.size()-1).contains(sortedList.get(0)))
	    {	
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
		
	}
	
	
	/** 
	 * Method for restoring default sort settings
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public void resetToDefaultSortSettings()  	
	{
		
		SkySiteUtils.waitForElement(driver, btnProjectSorting, 30);
		
		if(btnProjectSorting.isDisplayed())
		{
			
			btnProjectSorting.click();
			sortByProjectName.click();
			Log.message("Clicked on Sort by Project name");
			SkySiteUtils.waitTill(8000);
			
		}
		
		if(btnSortDescending.isDisplayed())
		{
			btnSortDescending.click();
			Log.message("Clicked on Sort by descending");
			SkySiteUtils.waitTill(8000);
			
		}
		
		Log.message("Default sorting settings restored");
	}

	
	
	
	/** 
	 * Method for ascending order sorting by Project Number
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean ascendingOrderSortingByProjectNumber() 
	{
		
		SkySiteUtils.waitForElement(driver, btnProjectSorting, 30);
		Log.message("Sort by button is displayed");
		btnProjectSorting.click();
		Log.message("Clicked on Project Sort by button");
		SkySiteUtils.waitForElement(driver, sortByProjectNumber, 20);
		sortByProjectNumber.click();
		Log.message("Clicked on sort by Project Number option");
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, btnSortAscending, 30);
		Log.message("Sort by ascending order button is displayed");
		btnSortAscending.click();
		Log.message("Clicked on ascending sorting button");
		SkySiteUtils.waitTill(8000);
		
		ArrayList<String> obtainedList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//div[@class='prj-id truncate-txt']/span"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
		       obtainedList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Project count before sorting:- " +obtainedList.size());		 
		Log.message("Obtained Project list order is:- " +obtainedList);
		
		SkySiteUtils.waitForElement(driver, btnSortDescending, 30);
		btnSortDescending.click();
		Log.message("Clicked on descending sorting button");
		SkySiteUtils.waitTill(8000);
		
		ArrayList<String> sortedList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//div[@class='prj-id truncate-txt']/span"));
		    
	    for(WebElement we:elementList1)
	    {
	    	if(!we.getText().equals("")) 
			{
	    		sortedList.add(we.getText()); 
		     
			}
	    	  	
	    }
	    
	    Log.message("Sorted Project list order is:- " +sortedList);
	    Log.message("Total number of Projects after sorting is:- " +sortedList.size());
	    
	    if(obtainedList.get(0).contains(sortedList.get(sortedList.size()-1)) && obtainedList.get(obtainedList.size()-1).contains(sortedList.get(0)))
	    {	
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}

	
	/** 
	 * Method for descending order sorting by Project Owner
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean descendingOrderSortingByProjectOwner() 
	{
		
		SkySiteUtils.waitForElement(driver, btnProjectSorting, 30);
		Log.message("Sort by button is displayed");
		btnProjectSorting.click();
		Log.message("Clicked on Project Sort by button");
		SkySiteUtils.waitForElement(driver, sortByProjectOwner, 20);
		sortByProjectOwner.click();
		Log.message("Clicked on sort by Project Owner option");
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, btnSortAscending, 30);
		Log.message("Sort by ascending order button is displayed");
		
		ArrayList<String> obtainedList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//span[@data-bind='text: ProjectOwner().FullName()']"));
			    
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
		       obtainedList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Project count before sorting:- " +obtainedList.size());		 
		Log.message("Obtained Project numbers are:- " +obtainedList);
		
		SkySiteUtils.waitForElement(driver, btnSortAscending, 30);
		btnSortAscending.click();
		Log.message("Clicked on ascending sorting button");
		SkySiteUtils.waitTill(8000);
		
		ArrayList<String> sortedList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//span[@data-bind='text: ProjectOwner().FullName()']"));
		    
	    for(WebElement we:elementList1)
	    {
	    	if(!we.getText().equals("")) 
			{
	    		sortedList.add(we.getText()); 
		     
			}
	    	  	
	    }
	    
	    Log.message("Sorted Project list order is:- " +sortedList);
	    Log.message("Total number of Projects after sorting is:- " +sortedList.size());
	    		
	    if(obtainedList.get(0).contains(sortedList.get(sortedList.size()-1)) && obtainedList.get(obtainedList.size()-1).contains(sortedList.get(0)))
	    {	
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }	
		
	}

	
	
	/** 
	 * Method for ascending order sorting by Project Owner
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean ascendingOrderSortingByProjectOwner() 
	{
		
		SkySiteUtils.waitForElement(driver, btnProjectSorting, 30);
		Log.message("Sort by button is displayed");		
		btnProjectSorting.click();
		Log.message("Clicked on Project Sort by button");
		SkySiteUtils.waitForElement(driver, sortByProjectOwner, 20);
		sortByProjectOwner.click();
		Log.message("Clicked on sort by Project Owner option");
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, btnSortAscending, 30);
		Log.message("Sort by ascending order button is displayed");
		btnSortAscending.click();
		Log.message("Clicked on ascending sorting button");
		SkySiteUtils.waitTill(8000);
		
		ArrayList<String> obtainedList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//span[@data-bind='text: ProjectOwner().FullName()']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
		       obtainedList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Project count before sorting:- " +obtainedList.size());		 
		Log.message("Obtained Project list order is:- " +obtainedList);
		
		SkySiteUtils.waitForElement(driver, btnSortDescending, 30);
		btnSortDescending.click();
		Log.message("Clicked on descending sorting button");
		SkySiteUtils.waitTill(8000);
		
		ArrayList<String> sortedList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//span[@data-bind='text: ProjectOwner().FullName()']"));
		    
	    for(WebElement we:elementList1)
	    {
	    	if(!we.getText().equals("")) 
			{
	    		sortedList.add(we.getText()); 
		     
			}
	    	  	
	    }
	    
	    Log.message("Sorted Project list order is:- " +sortedList);
	    Log.message("Total number of Projects after sorting is:- " +sortedList.size());
	    
	    if(obtainedList.get(0).contains(sortedList.get(sortedList.size()-1)) && obtainedList.get(obtainedList.size()-1).contains(sortedList.get(0)))
	    {	
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	
	}

	
	/** 
	 * Method to change sorting order and save data to be validated later
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public ArrayList<String> sortOrderSave() 
	{
		
		SkySiteUtils.waitForElement(driver, btnProjectSorting, 30);
		Log.message("Sort by button is displayed");		
		btnProjectSorting.click();
		Log.message("Clicked on Project Sort by button");
		SkySiteUtils.waitForElement(driver, sortByProjectOwner, 20);
		sortByProjectOwner.click();
		Log.message("Clicked on sort by Project Owner option");
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, btnSortAscending, 30);
		Log.message("Sort by ascending order button is displayed");
		btnSortAscending.click();
		Log.message("Clicked on ascending sorting button");
		SkySiteUtils.waitTill(8000);
		
		ArrayList<String> obtainedList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//span[@data-bind='text: ProjectOwner().FullName()']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
		       obtainedList.add(we.getText()); 
		     
			}
		}
		
		Log.message("Obtained Project count before sorting:- " +obtainedList.size());		 
		Log.message("Obtained Project list order is:- " +obtainedList);
		
		return obtainedList;
			
	}

	
	/** 
	 * Method to verify sorting order saved earlier after Log In
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean sortOrderSaveAfterLogIn(ArrayList<String> arr1) 
	{
		
		SkySiteUtils.waitForElement(driver, btnProjectSorting, 20);
		String actualSortByValue= btnProjectSorting.getText();
		Log.message("Current sort by option displayed is" +actualSortByValue);
		
		String expectedSortByValue = PropertyReader.getProperty("EXPECTEDSORTBYVALUE");
		
		SkySiteUtils.waitForElement(driver, btnSortDescending, 20);
		Log.message("Sort by descending order selected earlier is displayed");
		
		ArrayList<String> obtainedList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//span[@data-bind='text: ProjectOwner().FullName()']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
		       obtainedList.add(we.getText()); 
		     
			}
		}
		
		Log.message("Obtained Project count before sorting:- " +obtainedList.size());		 
		Log.message("Obtained Project list order is:- " +obtainedList);
		
		if(actualSortByValue.contains(expectedSortByValue) && btnSortDescending.isDisplayed() && arr1.equals(obtainedList))
		{
			return true;
			
		}
		else 
		{
			return false;
			
		}
	}

	
	/** 
	 * Method to validate Back to File button click in Print Cart landing to Project Dashboard
	 * Scripted By: Ranadeep
	 *  
	 */
	public boolean verifyProjectDashboardLanding() 
	{

		String expectedPageTitle = PropertyReader.getProperty("ProjectDashboardTitle");
		Log.message("Expected page title is :-" +expectedPageTitle);
		
		String actualPageTitle = driver.getTitle();
		Log.message("Actual page title is :-" +actualPageTitle);
		
		if(actualPageTitle.contains(expectedPageTitle))
		{
			return true;
		}
		else
		{
			return false;
		}
	
	
	}
	
	/** 
	 * Method to land into the Projects Settings Page from the Projects Dashboard
	 * Scripted By: Trinanjwan 
	 */	
	

	@FindBy(xpath="//img[@class='img-circle  img-responsive hoverZoomLink']")
	WebElement profileIcon;
	
	@FindBy(xpath="//i[@class='icon icon-cog']/..")
	WebElement settingsoption;
	
	
	public Projects_SettingsPage navigatingtoSettingsPage()
	
	{
		SkySiteUtils.waitTill(5000);
		Actions ac=new Actions(driver);
		ac.moveToElement(profileIcon).click(profileIcon).build().perform();
		//SkySiteUtils.waitForElement(driver, profileIcon, 40);
		//Log.message("Profile icon is visible now");
		//profileIcon.click();
		Log.message("Profile icon is clicked now");
		SkySiteUtils.waitForElement(driver, settingsoption, 40);
		Log.message("Settings option is visible now");
		settingsoption.click();
		Log.message("Clicked on Settings option");
		
		return new Projects_SettingsPage(driver).get();

		
	}
	

	/** 
	 * Method to verify Project Management options, autohyperlink option, outlook option in Project Settings
	 * Scripted By: Trinanjwan 
	 */	

	@FindBy(xpath="(//div[@class='project-box has-tooltip'])[2]//i[@class='icon icon-more-option']")
	WebElement moreOption;
	
	@FindBy(xpath="(//div[@class='project-box has-tooltip'])[2]//a[@class='settings']")
	WebElement settingsOption;
	
	
	
	public boolean verifypromngSettings(String xpathexp)
	{
	try
	{
		SkySiteUtils.waitTill(15000);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", moreOption);
		Log.message("Clicked on the more option");
		SkySiteUtils.waitTill(10000);
		executor.executeScript("arguments[0].click();", settingsOption);
		Log.message("Clicked on settings option from the list");	
		SkySiteUtils.waitTill(10000);
		driver.findElement(By.xpath(xpathexp));
		return false;
	}
	
	catch (org.openqa.selenium.NoSuchElementException e) {
		Log.message("Element is not present");
        return true;

	}

		
	}
	
	/** 
	 * Method to close the project settings pop over
	 * Scripted By: Trinanjwan 
	 */	
	
	
	@FindBy(xpath="//h4[text()='Settings']/preceding-sibling::button")
	WebElement closebtnsettings;
	
	public void closeSettingspopover()
	
	{
		SkySiteUtils.waitTill(5000);
		closebtnsettings.click();
		Log.message("Clicked on close button to close the modal");

	}
	
	
	
	
	/** 
	 * Method to verify Project Management options, autohyperlink option, outlook option in Project Settings if the package is enterprise
	 * Scripted By: Trinanjwan 
	 */	

	
	public boolean verifypromngSettingspresent(String xpathexp)
	{
		SkySiteUtils.waitTill(15000);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", moreOption);
		Log.message("Clicked on the more option");
		SkySiteUtils.waitTill(10000);
		executor.executeScript("arguments[0].click();", settingsOption);
		Log.message("Clicked on settings option from the list");	
		SkySiteUtils.waitTill(10000);
		WebElement element=driver.findElement(By.xpath(xpathexp));
		if(element.isDisplayed())
		{
		Log.message("Element is displayed");
		return true;
			
		}
		else
		{
			Log.message("Element is not displayed");
			return false;	
			
		}
	}

		
	}