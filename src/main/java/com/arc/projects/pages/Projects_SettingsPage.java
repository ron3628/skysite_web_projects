package com.arc.projects.pages;


import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.TimeZone;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arc.projects.utils.StaleExpectionHandling;
import com.arc.projects.utils.randomFileName;
import com.arcautoframe.utils.Log;
import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.steadystate.css.dom.Property;

public class Projects_SettingsPage extends LoadableComponent<Projects_SettingsPage>

{
	WebDriver driver;
	private boolean isPageLoaded;
	
	@FindBy(xpath="//button[text()='Customize']")
	@CacheLookup
	WebElement customizebtn;
	
	
	
	@Override
	protected void load() 
	{
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, customizebtn, 40);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		if (!isPageLoaded) 
		{
			Assert.fail();
		}
		

	}
	
	/**
	 * Declaring constructor for initializing web elements using PageFactory
	 * class.
	 * 
	 * @param driver
	 * @return
	 */
	
	public Projects_SettingsPage(WebDriver driver) 
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	
	}
	
	
	/** 
	 * Method to verify what is the default package after registration
	 * Scripted By: Trinanjwan
	 */
	
	
	@FindBy(xpath="//i[@class='icon icon-dropdown']")
	WebElement packageselectiondrpdwn;
	
	@FindBy(xpath="//span[@class='pkg-name dropdown-toggle']")
	WebElement packageselectionfield;
	
	@FindBy(xpath="//*[text()='Packages']")
	WebElement tabPackage;

	public boolean verifyDefaultPackage() throws Exception	
	{
		SkySiteUtils.waitTill(5000);
		Actions packageTab = new Actions(driver);
		packageTab.moveToElement(tabPackage).click(tabPackage).build().perform();
		Log.message("Clicked on Package tab");
		SkySiteUtils.waitTill(5000);
		
		String expectedDefaultPackage =PropertyReader.getProperty("DEFAULTPACKAGENAME");
		String expectedDefaultPackageMessage =PropertyReader.getProperty("DEFAULTPACKAGEMESSAGE");
		String actualDefaultPackageMessage = null;
		
		int count = driver.findElements(By.xpath("//h2[@data-bind='text:PackageName']")).size();
		for(int i=1; i<=count; i++)
		{
			SkySiteUtils.waitTill(2000);
			if(driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div/div/div/div[2]/div/div[9]/div/div/div/div/div/div["+i+"]/div/h2")).getText().contains(expectedDefaultPackage))
			{
				actualDefaultPackageMessage = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div/div/div/div[2]/div/div[9]/div/div/div/div/div/div["+i+"]/div/div[1]")).getText();
				Log.message("Actual default package message is: " +actualDefaultPackageMessage);
				SkySiteUtils.waitTill(2000);
				
			}
				
		}
		
		if(expectedDefaultPackageMessage.contains(actualDefaultPackageMessage))
		{
			return true;
		}
		else
		{
			return false;
		}		
	}
	
	
	/** 
	 * Method to change the package to enterprise if the package selected as basic
	 * Scripted By: Trinanjwan
	 */
	
	@FindBy(xpath="//label[text()='Enterprise']")
	WebElement enterprisepackage;
	
	public void initialcheckingpackageenterprise()	
	{
		
		SkySiteUtils.waitTill(3000);
		
		String expectedDefaultPackage =PropertyReader.getProperty("DEFAULTPACKAGENAME");
		int count = driver.findElements(By.xpath("//h2[@data-bind='text:PackageName']")).size();
		for(int i=1; i<=count; i++)
		{
			SkySiteUtils.waitTill(2000);
			
			if(driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div/div/div/div[2]/div/div[9]/div/div/div/div/div/div["+i+"]/div/div[6]/div[1]/a")).getAttribute("style").contains("display: none;")  
					&& !driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div/div/div/div[2]/div/div[9]/div/div/div/div/div/div["+i+"]/div/h2")).getText().contains(expectedDefaultPackage))
			{
				driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div/div/div/div[2]/div/div[9]/div/div/div/div/div/div[3]/div/div[6]/div[1]/a")).click();
				Log.message("Clicked to change to Enterprie Package");
				SkySiteUtils.waitTill(5000);
				
				driver.findElement(By.xpath("//*[@id='button-1']")).click();
				Log.message("Clicked on Yes button");
				SkySiteUtils.waitTill(20000);
				
				break;
				
			}
			else
			{
				Log.message("Enterprise package is selected");
			}		
				
		}
		
		SkySiteUtils.waitTill(5000);
		Actions ac=new Actions(driver);
		ac.moveToElement(profileIcon).click(profileIcon).build().perform();
		Log.message("Profile icon is clicked now");
		SkySiteUtils.waitForElement(driver, settingsoption, 40);
		Log.message("Settings option is visible now");
		settingsoption.click();
		Log.message("Clicked on Settings option");
		
	}
	

	/** 
	 * Method to change the package from enterprise to basic
	 * Scripted By: Trinanjwan
	 */
	
	@FindBy(xpath="//label[text()='Basic']")
	WebElement basicpackage;
	
	@FindBy(xpath="//button[@id='savepricingproducts']")
	WebElement buyNowbtn;
	
	@FindBy(xpath="//img[@class='img-circle  img-responsive hoverZoomLink']")
	WebElement profileIcon;
	
	@FindBy(xpath="//i[@class='icon icon-cog']/..")
	WebElement settingsoption;
	
	@FindBy(xpath="//*[@id='button-1']")
	WebElement btnYes;
	
	
	
	public ProjectDashboardPage changingentertobasic()
	{	
	
		SkySiteUtils.waitTill(5000);
		Actions packageTab = new Actions(driver);
		packageTab.moveToElement(tabPackage).click(tabPackage).build().perform();
		Log.message("Clicked on Package tab");
		SkySiteUtils.waitTill(5000);
		
		this.initialcheckingpackageenterprise(); /**Using the "initialcheckingpackageenterprise" method for checking initially**/
		
		SkySiteUtils.waitTill(5000);
		packageTab.moveToElement(tabPackage).click(tabPackage).build().perform();
		Log.message("Clicked on Package tab");
		SkySiteUtils.waitTill(5000);
		
		int count = driver.findElements(By.xpath("//h2[@data-bind='text:PackageName']")).size();
		String expectedPackage = PropertyReader.getProperty("EXPECTEDBASICPACKAGE");
		
		for(int i=1; i<=count; i++)
		{
			SkySiteUtils.waitTill(2000);
			if(driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div/div/div/div[2]/div/div[9]/div/div/div/div/div/div["+i+"]/div/h2")).getText().contains(expectedPackage))
			{
				driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div/div/div/div[2]/div/div[9]/div/div/div/div/div/div["+i+"]/div/div[6]/div[1]/a")).click();
				Log.message("Clicked to change to Basic Package");
				
				SkySiteUtils.waitForElement(driver, btnYes, 30);
				btnYes.click();
				Log.message("Clicked on Yes button to change package");
				
				break;
				
			}
				
		}
		
		SkySiteUtils.waitTill(20000);
		return new ProjectDashboardPage(driver).get();

	}
	
	/** 
	 * Method to navigate to Project Dashboard from account Settings Page
	 * Scripted By: Trinanjwan
	 */
	
	
	@FindBy(xpath="//span[text()='Project dashboard']")
	WebElement projectDashboardbtn;
	

	public ProjectDashboardPage navigatingDashboard()
	
	{
		
		SkySiteUtils.waitTill(25000);
		Actions ac=new Actions(driver);
		ac.moveToElement(projectDashboardbtn).click(projectDashboardbtn).build().perform();
		Log.message("Clicked on Project Dashboard button");
		return new ProjectDashboardPage(driver).get();

	}
	
	
	
	/** 
	 * Method to change the package to basic if the package selected as enterprise
	 * Scripted By: Trinanjwan
	 */
	public void initialcheckingpackagebasic()
	{
		SkySiteUtils.waitTill(3000);
		
		String expectedPackage =PropertyReader.getProperty("EXPECTEDBASICPACKAGE");
		int count = driver.findElements(By.xpath("//h2[@data-bind='text:PackageName']")).size();
		for(int i=1; i<=count; i++)
		{
			SkySiteUtils.waitTill(2000);
			
			if(driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div/div/div/div[2]/div/div[9]/div/div/div/div/div/div["+i+"]/div/div[6]/div[1]/a")).getAttribute("style").contains("display: none;")  
					&& !driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div/div/div/div[2]/div/div[9]/div/div/div/div/div/div["+i+"]/div/h2")).getText().contains(expectedPackage))
			{
				driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div/div/div/div[2]/div/div[9]/div/div/div/div/div/div[1]/div/div[6]/div[1]/a")).click();
				Log.message("Clicked to change to Basic Package");
				SkySiteUtils.waitTill(5000);
				
				driver.findElement(By.xpath("//*[@id='button-1']")).click();
				Log.message("Clicked on Yes button");
				SkySiteUtils.waitTill(20000);
				
				break;
				
			}
			else
			{
				Log.message("Basic Package is selected");
			}		
				
		}
		
		SkySiteUtils.waitTill(5000);
		Actions ac=new Actions(driver);
		ac.moveToElement(profileIcon).click(profileIcon).build().perform();
		Log.message("Profile icon is clicked now");
		SkySiteUtils.waitForElement(driver, settingsoption, 40);
		Log.message("Settings option is visible now");
		settingsoption.click();
		Log.message("Clicked on Settings option");
		
	}


	
	/** 
	 * Method to change the package from basic to enterprise
	 * Scripted By: Trinanjwan
	 */	
	public ProjectDashboardPage changingbasictoenter()
	{	
		SkySiteUtils.waitTill(5000);
		Actions packageTab = new Actions(driver);
		packageTab.moveToElement(tabPackage).click(tabPackage).build().perform();
		Log.message("Clicked on Package tab");
		SkySiteUtils.waitTill(5000);
		
		this.initialcheckingpackagebasic();//Using the "initialcheckingpackagebasic" method for checking initially
		
		SkySiteUtils.waitTill(5000);
		packageTab.moveToElement(tabPackage).click(tabPackage).build().perform();
		Log.message("Clicked on Package tab");
		SkySiteUtils.waitTill(5000);
		
		int count = driver.findElements(By.xpath("//h2[@data-bind='text:PackageName']")).size();
		String expectedPackage = PropertyReader.getProperty("DEFAULTPACKAGENAME");
		
		for(int i=1; i<=count; i++)
		{
			SkySiteUtils.waitTill(2000);
			if(driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div/div/div/div[2]/div/div[9]/div/div/div/div/div/div["+i+"]/div/h2")).getText().contains(expectedPackage))
			{
				driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div/div/div/div[2]/div/div[9]/div/div/div/div/div/div["+i+"]/div/div[6]/div[1]/a")).click();
				Log.message("Clicked to change to Enterprise Package");
				
				SkySiteUtils.waitForElement(driver, btnYes, 30);
				btnYes.click();
				Log.message("Clicked on Yes button to change package");
				
				break;
				
			}
				
		}

		SkySiteUtils.waitTill(20000);		
		return new ProjectDashboardPage(driver).get();

	}
	
	
	/** 
	 * Method of Addition Credit Card Information
	 * Scripted By: Trinanjwan
	 */
	
	@FindBy(xpath="//button[@class='btn btn-default register-card']")
	WebElement addCreditCardinfobtn;
	
	@FindBy(xpath="//input[@id='bill_to_forename']")
	WebElement firstnamefield;
	
	@FindBy(xpath="//input[@id='bill_to_surname']")
	WebElement lastnamefield;
	
	@FindBy(xpath="//input[@id='bill_to_address_line1']")
	WebElement addressfield;
	
	@FindBy(xpath="//input[@id='bill_to_address_city']")
	WebElement cityfield;
	
	@FindBy(xpath="//select[@id='bill_to_address_country']")
	WebElement countryfield;
	
	@FindBy(xpath="//input[@id='bill_to_address_state']")
	WebElement statefield;
	
	@FindBy(xpath="//input[@id='bill_to_address_postal_code']")
	WebElement zipfield;
	
	@FindBy(xpath="//input[@class='right complete']")
	WebElement finishbtn;
	
	@FindBy(xpath="//input[@id='card_number']")
	WebElement cardnumberfield;
	
	@FindBy(xpath="//select[@id='card_expiry_month']")
	WebElement cardexpirymonth;
	
	@FindBy(xpath="//select[@id='card_expiry_year']")
	WebElement cardexpiryyear;
	
	@FindBy(xpath="//input[@id='card_cvn']")
	WebElement cardcvn;
	
	@FindBy(xpath="//h3[text()='Receipt']")
	WebElement Receiptlabel;
	
	@FindBy(xpath="//h3[text()='Billing information']")
	WebElement Billininfolabel;
	
	@FindBy(xpath="//button[text()=' Close']")
	WebElement closebtnbillinginfo;
	

	public boolean addtionofcreditcardinfo()
	
	{
		SkySiteUtils.waitForElement(driver, addCreditCardinfobtn, 60);
		Log.message("Add Credit Card info button is displayed");
		addCreditCardinfobtn.click();
		Log.message("Clicked on Add Credit Card info button");
		SkySiteUtils.waitTill(15000);
		String MainWindow = driver.getWindowHandle();
		Log.message("The main window is: "+MainWindow);
		System.setProperty("WindowID", MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);
			SkySiteUtils.waitTill(5000);
		}
		String firstname = PropertyReader.getProperty("BillFirstName");
		String lastname = PropertyReader.getProperty("BillLastName");
		String address = PropertyReader.getProperty("BillAddress");
		String city = PropertyReader.getProperty("BillCity");
		String state = PropertyReader.getProperty("BillState/Province");
		String zip = PropertyReader.getProperty("BillZip/PostalCode");
		String cardnumber = PropertyReader.getProperty("BillCardNumberStg");
		String cvn = PropertyReader.getProperty("BillCVNStg");
		
		SkySiteUtils.waitForElement(driver, firstnamefield, 10);	
		Log.message("First Name field is displayed now");
		firstnamefield.clear();
		firstnamefield.sendKeys(firstname);
		Log.message("First Name is:"+firstname);
		
		SkySiteUtils.waitForElement(driver, lastnamefield, 10);
		Log.message("Last Name field is displayed now");
		lastnamefield.clear();
		lastnamefield.sendKeys(lastname);
		Log.message("Last Name is:"+lastname);
		
		SkySiteUtils.waitForElement(driver, addressfield, 10);
		Log.message("Address field is displayed now");
		addressfield.clear();
		addressfield.sendKeys(address);
		Log.message("Address is:"+address);
		
		SkySiteUtils.waitForElement(driver, cityfield, 10);
		Log.message("City field is displayed now");
		cityfield.clear();
		cityfield.sendKeys(city);
		Log.message("City is:"+city);
		

		Select sc=new Select(countryfield);
		sc.selectByValue("IN");
		SkySiteUtils.waitTill(10000);
		
		
		SkySiteUtils.waitForElement(driver, statefield, 10);
		Log.message("State field is displayed now");
		statefield.clear();
		statefield.sendKeys(state);
		Log.message("State is:"+state);
		
		
		SkySiteUtils.waitForElement(driver, statefield, 10);
		Log.message("State field is displayed now");
		statefield.clear();
		statefield.sendKeys(state);
		Log.message("State is:"+state);
		
		SkySiteUtils.waitForElement(driver, zipfield, 10);
		Log.message("Zip field is displayed now");
		zipfield.clear();
		zipfield.sendKeys(zip);
		Log.message("ZIP is:"+zip);
		
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", finishbtn);
		SkySiteUtils.waitTill(10000);
		
		
		SkySiteUtils.waitForElement(driver, cardnumberfield, 10);
		Log.message("Card number field is displayed now");
		cardnumberfield.clear();
		cardnumberfield.sendKeys(cardnumber);
		Log.message("Card number is:"+cardnumber);
		
		
		Select sc1=new Select(cardexpirymonth);
		sc1.selectByValue("12");
		SkySiteUtils.waitTill(5000);
		
		Select sc2=new Select(cardexpiryyear);
		sc2.selectByValue("2030");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, cardcvn, 10);
		Log.message("CVN field is displayed now");
		cardcvn.clear();
		cardcvn.sendKeys(cvn);
		Log.message("CVN is:"+cvn);
		
		SkySiteUtils.waitTill(5000);
		finishbtn.click();
		SkySiteUtils.waitTill(15000);
		
		if(Receiptlabel.isDisplayed() && Billininfolabel.isDisplayed())
		{
			Log.message("Landed successfully in billing information page");	
			((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", closebtnbillinginfo);
			SkySiteUtils.waitTill(5000);
			closebtnbillinginfo.click();
			return true;
		}
		else
		{
			Log.message("Landed successfully in billing information page");	
			((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", closebtnbillinginfo);
			SkySiteUtils.waitTill(5000);
			closebtnbillinginfo.click();
			return false;
		}
		
	}
}
	

