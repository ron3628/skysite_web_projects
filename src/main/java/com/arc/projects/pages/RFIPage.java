package com.arc.projects.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arc.projects.utils.randomFileName;
import com.arcautoframe.utils.Log;


public class RFIPage extends LoadableComponent<RFIPage>
{

	WebDriver driver;
	private boolean isPageLoaded;
	
	ProjectDashboardPage projectDashboardPage;
	FolderPage folderPage;
	
	
	/** Identifying web elements using FindBy annotation*/
	@FindBy (xpath="//*[@id='rfilistContainer']/div[1]/div[1]/ul/li/a")
	WebElement btnCreateRFI;
	
	@FindBy (xpath="//*[@id='btnCreateRFI']")
	WebElement btnCreateRFIPopover;
	
	@FindBy (xpath="//*[@id='divAddRFIUI']/div/div[2]/div[2]/div[1]/div[2]/div/div/a/i")
	WebElement btnAddRFIToUser;
	
	@FindBy (xpath="//*[@id='divProjectUsers']/div/div[3]/button[2]")
	WebElement btnSelectUser;
	
	@FindBy (xpath="//*[@id='divUserListView']/ul/li[2]")
	WebElement RFIToUser;
	
	@FindBy (xpath="//*[@id='txtSubject']")
	WebElement textboxRFISubject;
	
	@FindBy (xpath="//*[@id='txtQuestion']")
	WebElement textboxRFIQuestion;
	
	@FindBy (xpath="//*[@id='ulRFIList']/li[1]/div[2]/div[1]/div/h4/span")
	WebElement createdRFI;
	
	@FindBy (xpath="//*[@id='liActivityMain']/a/span[1]")
	WebElement btnProjectActivity;
	
	@FindBy (xpath="//*[@id='liActivity']/a/span")
	WebElement btnDocumentActivity;
	
	@FindBy (xpath="//*[@id='ulRFIList']/li[1]/div[2]/div[1]/div/h5/span[1]")
	WebElement createdRFINo;
	
	//____________________ Elements written by Naresh _____________________________
	
	@FindBy(css = ".dropdown-toggle.aPunchrfimenu")
	WebElement btnPrjManagement;
	
	@FindBy(css = "#a_showRFIList")
	WebElement tabRFI;
	
	@FindBy(css = ".new-rfi")
	WebElement btncreateNewRFI;
	
	
	@Override
	protected void load() 
	{
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnCreateRFI, 20);

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		
	}
	
	/**
	 * Declaring constructor for initializing web elements using PageFactory
	 * class.
	 * @param driver
	 * @return
	 */
	public RFIPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	
	/** 
	 * Method for creating RFI for Activity tracking
	 * Scripted By: Ranadeep
	 * @return 
	 */
	
	public ProjectActivitiesPage createRFI()	 
	{
		
		SkySiteUtils.waitForElement(driver, btnCreateRFI, 20);
		
		WebElement element = driver.findElement(By.xpath("//*[@id='rfilistContainer']/div[1]/div[1]/ul/li/a"));
	    JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);
	    Log.message("Clicked on the create RFI button");
	    
	    SkySiteUtils.waitForElement(driver, btnAddRFIToUser, 20);
	    btnAddRFIToUser.click();
	    Log.message("Clicked to open the add RFI to user popup");	    
	    
	    SkySiteUtils.waitForElement(driver, RFIToUser, 20);
	    RFIToUser.click();
	    Log.message("Selected the to RFI user");
	    
	    WebElement element1 = driver.findElement(By.xpath("//*[@id='divProjectUsers']/div/div[3]/button[2]"));
	    JavascriptExecutor executor1 = (JavascriptExecutor)driver;
	    executor1.executeScript("arguments[0].click();", element1);
	    Log.message("Clicked on the Select user button");
	    
	    SkySiteUtils.waitForElement(driver, textboxRFISubject, 20);
	    String expectedSubjectRFI = PropertyReader.getProperty("SUBJECTRFI");
	    textboxRFISubject.clear();
	    textboxRFISubject.sendKeys(expectedSubjectRFI);
		Log.message("RFI subject entered is:- " +expectedSubjectRFI);
		
		SkySiteUtils.waitForElement(driver, textboxRFIQuestion, 20);
		String questionRFI = PropertyReader.getProperty("QUESTIONRFI");
		textboxRFIQuestion.clear();
		textboxRFIQuestion.sendKeys(questionRFI);
		Log.message("RFI Question entered is:- " +questionRFI);
		
		SkySiteUtils.waitForElement(driver, btnCreateRFIPopover, 20);
		btnCreateRFIPopover.click();
		Log.message("Clicked on Create RFI button");
		
		SkySiteUtils.waitTill(8000);
		String actualSubjectRFI=createdRFI.getText();
		Log.message("Created RFI subject is:- " +actualSubjectRFI);
		
		if(actualSubjectRFI.equals(expectedSubjectRFI))
		{
			Log.message("RFI created successfully");
			
			SkySiteUtils.waitForElement(driver, btnProjectActivity, 20);
			WebElement element2 = driver.findElement(By.xpath("//*[@id='liActivityMain']/a/span[1]"));
		    JavascriptExecutor executor2 = (JavascriptExecutor)driver;
		    executor2.executeScript("arguments[0].click();", element2);
			Log.message("Clicked on Project Activity");
			
			SkySiteUtils.waitTill(5000);
			
			SkySiteUtils.waitForElement(driver, btnDocumentActivity, 20);
			WebElement element3 = driver.findElement(By.xpath("//*[@id='liActivity']/a/span"));
		    JavascriptExecutor executor3 = (JavascriptExecutor)driver;
		    executor3.executeScript("arguments[0].click();", element3);
			Log.message("Clicked on Document Activity");			
			
			return new ProjectActivitiesPage(driver).get();
			
			
		}		
		
		else
		{
				Log.message("RFI creation failed");
				return null;
		}	
		
	}
	
	/**
	 * Method written for Select RFI List from Project Management Tab
	 * Scipted By: Naresh Babu
	 * @return
	 */
	
	public FolderPage SelectRFIList() 
	{
		SkySiteUtils.waitForElement(driver, btnPrjManagement, 20);
		btnPrjManagement.click();
		Log.message("Clicked on Project Management tab.");
		SkySiteUtils.waitForElement(driver, tabRFI, 20);
		tabRFI.click();
		Log.message("Clicked on RFI List tab.");
		SkySiteUtils.waitForElement(driver, btncreateNewRFI, 20);

		if (btncreateNewRFI.isDisplayed())
			Log.message("Create new RFI button is available.");
		return new FolderPage(driver).get();
	}
	
	
	/**
	 * Method written for Create RFI with attachments(Internal and External)
	 * Scipted By: Naresh Babu
	 * @return
	 */
	
	@FindBy(css="#txtUserSearchKeyword")
	WebElement contactSearchField;
	
	@FindBy(xpath="//i[@class='icon icon-search icon-lg']")
	WebElement iconSearchSelectUser;
	
	@FindBy(xpath="//h4[@data-bind='text: UserName()']")
	WebElement nameFromSearchResult;
	
	@FindBy(xpath="//a[@data-bind='text: Email()']")
	WebElement emailFromSearchResult;
	
	@FindBy(xpath=".//*[@id='divCC']/div[2]/a/i")
	WebElement btnAddRFICCUser;
	
	//@FindBy(xpath="//button[@class='btn btn-primary' and @data-bind='click: SelectContactEvent']")
	//WebElement btnSelectUser;
	
	@FindBy(xpath="//input[@class='pull-left' and @type='checkbox']")
	WebElement checkboxSearchResult;
	
	@FindBy(css="#btn-Load-attachment")
	WebElement btnAddAttachment;
	
	@FindBy(css=".btn.btn-primary.set-files")
	WebElement btnSelectFilesRFIWindow;
	
	@FindBy(css="#lblFileCount")
	WebElement txtNoOfFilesSelectedToRFIWindow;
	
	@FindBy(xpath="//span[@data-bind='text: TotalAttachmentCount()']")
	WebElement TotalAttachmentCount;
	
	@FindBy(xpath="//a[@id='punchAttachedFiles']")
	WebElement RFIAttachedFilesDropDown;
	
	@FindBy(xpath="//button[@id='add-project-file']")
	WebElement btnProjectFiles;
	
	@FindBy(xpath="//i[@class='icon icon-3x icon-folder-close']")
	WebElement folderIconProjectFiles;
	
	@FindBy(xpath="//input[@id='txtDocSearchKeyword']")
	WebElement txtDocSearchProjectFiles;
	
	@FindBy(xpath="//i[@class='icon icon-search icon-lg']")
	WebElement iconDocSearchProjectFiles;
	
	@FindBy(css="#btnCreateRFI")
	WebElement btnCreateRFIDetailsWindow;
	
	@FindBy(xpath="//span[@class='noty_text']")
	WebElement notificationMessage;
	
	@FindBy(xpath="(//span[@data-bind='text: Subject()'])[1]")
	WebElement infoSubjectFirstRFI;
	
	@FindBy(xpath="(//span[@class='yellow-txt'])[1]")
	WebElement OpenStatus;
	
	@FindBy(xpath="(//strong[@data-bind='text:AttachmentCount()'])[1]")
	WebElement attachmentCount;
	
	@FindBy(css="#liThirdTab")
	WebElement tabALLRFI;
	
	
	public boolean Create_RFI_WithAttachments_AndValidate(String TO_USERNAME,String TO_MailId,String CC_USERNAME, String FolderPath, String File_Name) throws IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		
		SkySiteUtils.waitForElement(driver, btncreateNewRFI, 30);
		btncreateNewRFI.click();
		Log.message("Clicked on create RFI button.");
		SkySiteUtils.waitForElement(driver, btnAddRFIToUser, 30);
		SkySiteUtils.waitTill(2000);
	    btnAddRFIToUser.click();
	    Log.message("Clicked on TO person contact list icon.");
	    SkySiteUtils.waitForElement(driver, contactSearchField, 30);
		SkySiteUtils.waitTill(2000);
		contactSearchField.sendKeys(TO_USERNAME);
		iconSearchSelectUser.click();
		Log.message("Clicked on Search icon by entering input");
		SkySiteUtils.waitTill(3000);
		String Name_AfterSearch = nameFromSearchResult.getText();
		Log.message("Name After Search: "+Name_AfterSearch);
		String Mail_AfterSearch = emailFromSearchResult.getText();
		Log.message("Mail After Search: "+Mail_AfterSearch);
	
		if((Name_AfterSearch.contains(TO_USERNAME)) && (Mail_AfterSearch.contentEquals(TO_MailId)))
		{
			Log.message("Search Results are coming properly for adding TO user!!!");
			checkboxSearchResult.click();
			btnSelectUser.click();
			Log.message("Clicked on Select user.");
			SkySiteUtils.waitTill(3000);
			SkySiteUtils.waitForElement(driver, btnAddRFICCUser, 30);
			SkySiteUtils.waitTill(2000);
			btnAddRFICCUser.click();
		    Log.message("Clicked on CC person contact list icon.");
		    SkySiteUtils.waitForElement(driver, contactSearchField, 30);
			SkySiteUtils.waitTill(2000);
			contactSearchField.sendKeys(CC_USERNAME);
			iconSearchSelectUser.click();
			Log.message("Clicked on Search icon by entering input");
			SkySiteUtils.waitTill(3000);
			Name_AfterSearch = nameFromSearchResult.getText();
			Log.message("Name After Search: "+Name_AfterSearch);
			if((Name_AfterSearch.contains(CC_USERNAME)))
			{
				Log.message("Search Results are coming properly for adding CC user!!!");
				checkboxSearchResult.click();
				btnSelectUser.click();
				Log.message("Clicked on Select user.");
				SkySiteUtils.waitForElement(driver, textboxRFISubject, 30);
				SkySiteUtils.waitTill(3000);
				String RFI_Subject = PropertyReader.getProperty("SUBJECTRFI");
				textboxRFISubject.click();
				textboxRFISubject.sendKeys(RFI_Subject);
				Log.message("Entering subject in RFI Subject edit field.");
				SkySiteUtils.waitForElement(driver, textboxRFIQuestion, 20);
				String RFI_Question = PropertyReader.getProperty("QUESTIONRFI");
				textboxRFIQuestion.click();
				textboxRFIQuestion.sendKeys(RFI_Question);
				Log.message("Entering Question in RFI Question edit field.");
				SkySiteUtils.waitTill(2000);
				btnAddAttachment.click();
				Log.message("Clicked on Add Attachment button.");
				SkySiteUtils.waitForElement(driver, btnSelectFilesRFIWindow, 20);
				btnSelectFilesRFIWindow.click();
				Log.message("Clicked on Select files button.");
				SkySiteUtils.waitTill(10000);
				
				// Writing File names into a text file for using in AutoIT Script
				BufferedWriter output;
				randomFileName rn = new randomFileName();
				String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
				output = new BufferedWriter(new FileWriter(tmpFileName, true));
				String expFilename = null;
				File[] files = new File(FolderPath).listFiles();
				for (File file : files) {
					if (file.isFile()) {
						expFilename = file.getName();// Getting File Names into a variable
						Log.message("Expected File name is:" + expFilename);
						output.append('"' + expFilename + '"');
						output.append(" ");
						SkySiteUtils.waitTill(1000);
					}
				}
				output.flush();
				output.close();

				// Executing .exe autoIt file
				String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
				Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
				Log.message("AutoIT Script Executed!!");
				SkySiteUtils.waitTill(10000);			
				SkySiteUtils.waitForElement(driver, txtNoOfFilesSelectedToRFIWindow, 30);
				try
				{
					 File file = new File(tmpFileName);
					 if(file.delete())
					 {
						 Log.message(file.getName() + " is deleted!");
					 }
					 else
					 {
						 Log.message("Delete operation is failed.");
					 }
				}
				catch(Exception e)
				{
					Log.message("Exception occured!!!"+e);	
				}
				SkySiteUtils.waitTill(5000);		 
				String Attachment_Count=TotalAttachmentCount.getText();
				Log.message("Attachment Count: "+Attachment_Count);
				if(Attachment_Count.equals("3"))
				{
					Log.message("Files are selected to grid!!!");
					RFIAttachedFilesDropDown.click();
					Log.message("clicked on dropdown attachment files.");
					SkySiteUtils.waitTill(2000);
					driver.findElement(By.xpath("(//i[@class='icon icon-remove'])[3]")).click();
					Log.message("Removing third file from the list.");
					SkySiteUtils.waitTill(2000);
					Attachment_Count=TotalAttachmentCount.getText();
					Log.message("Attachment Count after remove: "+Attachment_Count);
					if(Attachment_Count.equals("2"))
					{
						Log.message("Attachment got removes successfully.");
						btnProjectFiles.click();
						Log.message("Clciked on project files button");
						SkySiteUtils.waitTill(2000);
						SkySiteUtils.waitForElement(driver, folderIconProjectFiles, 20);
						folderIconProjectFiles.click();
						Log.message("Clicked on Folder available in project.");
						SkySiteUtils.waitForElement(driver, txtDocSearchProjectFiles, 20);
						SkySiteUtils.waitTill(2000);
						txtDocSearchProjectFiles.sendKeys(File_Name);
						iconDocSearchProjectFiles.click();
						SkySiteUtils.waitTill(5000);
						driver.findElement(By.xpath("(//div/input[@type='checkbox'])[1]")).click();//Select Search result checkbox
						btnCreateRFIDetailsWindow.click();
						Log.message("clicked on create RFI button after enter required details.");
						SkySiteUtils.waitForElement(driver, notificationMessage, 60);
						SkySiteUtils.waitTill(2000);
						String NotifyMsg_AfterCreateRFI = notificationMessage.getText();
						Log.message("Notify Msg After Create RFI is: "+NotifyMsg_AfterCreateRFI);
						SkySiteUtils.waitTill(10000);
						String Subject_AfterCreateRFI=infoSubjectFirstRFI.getText();
						Log.message("Subject After Create RFI is : "+Subject_AfterCreateRFI);			
						String AssignedTo=driver.findElement(By.xpath(".//*[@id='ulRFIList']/li[1]/div[2]/div[2]/h4/span")).getText();
						Log.message("Assigned To: "+AssignedTo);
						String Status_AfterCreate = OpenStatus.getText();
						Log.message("Status After Create: "+Status_AfterCreate);
						Attachment_Count = attachmentCount.getText();
						Log.message("Attchment count is: "+Attachment_Count);
						
						if((Attachment_Count.contentEquals("3")) && (Subject_AfterCreateRFI.contentEquals(RFI_Subject)) 
								&& (AssignedTo.contains(TO_USERNAME)) && (Status_AfterCreate.equalsIgnoreCase("Open"))
								&& (NotifyMsg_AfterCreateRFI.equals("RFI is added successfully"))
								&& (driver.findElement(By.xpath("(//i[@class='icon icon-lg icon-download-alt'])[1]")).isDisplayed()))
						{
							result1=true;
							Log.message("RFI Created successfully and same is available under My RFI");
							//Validations from All RFI List Tab
							tabALLRFI.click();
							Log.message("Clicked on All RFI List tab.");
							SkySiteUtils.waitTill(10000);
							Subject_AfterCreateRFI=infoSubjectFirstRFI.getText();
							Log.message("Subject Under All RFI is : "+Subject_AfterCreateRFI);			
							AssignedTo=driver.findElement(By.xpath(".//*[@id='ulRFIList']/li[1]/div[2]/div[2]/h4/span")).getText();
							Log.message("Assigned To: "+AssignedTo);
							Status_AfterCreate = OpenStatus.getText();
							Log.message("Status After Create: "+Status_AfterCreate);
							Attachment_Count = attachmentCount.getText();
							Log.message("Attchment count is: "+Attachment_Count);
							
							if((Attachment_Count.contentEquals("3")) && (Subject_AfterCreateRFI.contentEquals(RFI_Subject)) 
									&& (AssignedTo.contains(TO_USERNAME)) && (Status_AfterCreate.equalsIgnoreCase("Open"))
									&& (driver.findElement(By.xpath("(//i[@class='icon icon-lg icon-download-alt'])[1]")).isDisplayed()))
							{
								result2=true;
								Log.message("RFI details are available under All RFI List.");
							}
							else
							{
								result2=false;
								Log.message("RFI details are NOT available under All RFI List.");
							}
						}
						else
						{
							result1=false;
							Log.message("RFI Creation is Failed.");
						}
				
				
					}
			
				}
			
			}
			
		}

		if ((result1==true) && (result2==true)) 
		{
			Log.message("Creating a RFI with attachments and validate is working successfully.");
			return true;
		} else {
			Log.message("Creating a RFI with attachments and validate is failed.");
			return false;
		}
	}
	
	
	/**
	 * Method written for download RFI Report
	 * Scipted By: Naresh Babu
	 * @return
	 */
	
	@FindBy(xpath="(//span[@data-bind='text: RFINumber()'])[1]")
	WebElement txtFirstRFINumber;

	@FindBy(xpath="(//i[@class='icon icon-lg icon-download-alt'])[1]")
	WebElement btnRFIDownload;
	
	public boolean Verify_download_RFI_Report(String Sys_Download_Path) throws AWTException, InterruptedException 
	{
		boolean result1 = false;
		
		SkySiteUtils.waitForElement(driver, btncreateNewRFI, 30);
		//Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(8000);
		
		String RFI_Number = txtFirstRFINumber.getText();
		Log.message("1st RFI number from the list is: "+RFI_Number);
		btnRFIDownload.click();
		Log.message("Clicked on Download buton of RFI.");
		SkySiteUtils.waitTill(30000);

		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);
		if (browserName.contains("firefox")) 
		{
			// Handling Download PopUp of firefox browser using robot
			Robot robot = null;
			robot = new Robot();

			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(30000);
		}	
		// After Download, checking file Downloaded or not
		String ActualFilename = null;
		File[] files = new File(Sys_Download_Path).listFiles();

		for (File file : files) 
		{
			if (file.isFile()) 
			{
				ActualFilename = file.getName();// Getting File Names into a variable
				Log.message("Actual File name is:" + ActualFilename);
				Long ActualFileSize = file.length();
				Log.message("Actual File size is:" + ActualFileSize);
				if ((ActualFilename.contains("RFI "+RFI_Number+".pdf")) && (ActualFileSize > 0)) 
				{
					Log.message("File is available in downloads!!!");
					result1 = true;
					break;
				}
			}
		}
		if((result1==true))
		{
			Log.message("RFI report download is working successfully!!!");
			return true;
		}
		else 
		{
			Log.message("RFI report download is not working!!!");
			return false;
		}	
	}
	
	
	/**
	 * Method to validate RFI page is landed
	 * Scripted By: Trinanjwan Sarkar
	 * @return
	 */
	
	@FindBy(xpath="//a[@class='new-rfi']")
	WebElement createrfibtn;
	
	
	public boolean rfiPagelanded()
	
	{
		SkySiteUtils.waitForElement(driver, createrfibtn, 50);
		if(createrfibtn.isDisplayed())
		{
			Log.message("Create RFI button is displayed");
			return true;
		}
		else
		{
			Log.message("Create RFI button is not displayed");
			return false;
		}

	}
	
}
