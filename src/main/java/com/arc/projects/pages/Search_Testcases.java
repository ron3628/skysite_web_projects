package com.arc.projects.pages;

import java.awt.AWTException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.RobotUtils;
import com.arc.projects.utils.SkySiteUtils;
import com.arc.projects.utils.randomFileName;
import com.arcautoframe.utils.Log;
import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

public class Search_Testcases extends LoadableComponent<Search_Testcases> {

	WebDriver driver;

	private boolean isPageLoaded;

	@FindBy(css = ".nav.navbar-nav.navbar-left>li>input")
	WebElement chkboxAllPhoto;

	@FindBy(css = ".dropdown.has-tooltip>a")
	WebElement btnDropdownMultPhotoSel;

	@FindBy(xpath = "(//button[@class='btn btn-primary btn-lg'])[2]")
	WebElement btnUploadPhotos1;

	@FindBy(xpath = "//*[@id='btnSelectFiles']")
	WebElement btnChooseFile;

	@FindBy(css = "#btnFileUpload")
	WebElement btnFileUpload;

	@FindBy(xpath = "//button[@class='btn btn-primary' and @data-bind='click: SaveAttributes']")
	WebElement btnSaveAttributes;

	@FindBy(xpath = "//em[@class='pull-left pw-icon-orange']")
	WebElement headerMsgUploadPhotoWindow;

	@FindBy(css = "#aPrjAddFolder")
	WebElement btnAddNewFolder;

	@FindBy(css = "#txtSearchKeyword")
	WebElement txtSearchField;

	@FindBy(css = "#btnSearch")
	WebElement btnGlobalSearch;

	@FindBy(css = "#txtFolderName")
	WebElement txtBoxFolderName;

	@FindBy(css = ".chkExcludedrawing")
	WebElement chkBoxExcludedrawing;

	@FindBy(css = ".chkExcludedrawing")
	WebElement chkBoxExcludeLD;

	@FindBy(css = "#btnFolderCreate")
	WebElement btnCreateFolder;

	@FindBy(css = "#btnFolderCreateThisFolder")
	WebElement btnUploadFilesToFolder;

	@FindBy(css = ".noty_text")
	WebElement notificationMsg;

	@FindBy(css = "#FeedbackClose>span")
	WebElement FeedBackmeesage;

	@FindBy(css = "#button-1")
	WebElement YesButton;

	@FindBy(xpath = "//span [@class='img-circle profile-no-image md']")
	WebElement Profile;
	
	@FindBy(xpath = "//i [@class='icon icon-user']")
	WebElement MYProfile;
	
	@FindBy(xpath="//i [@class='icon icon-create icon-lg']")
	WebElement btnCreateProject;
	
	@FindBy(css="#btnCreate")
	//@CacheLookup - Removed due to fail
	private WebElement btnCreate;
	
	@FindBy(css="#txtProjectName")
	//@CacheLookup - Removed due to fail
	private WebElement txtProjectName;
	
	@FindBy(css="#txtProjectNumber")
	WebElement txtProjectNumber;
	
	@FindBy(xpath=".//*[@id='prj-start-date']/span")
	WebElement butProjectStartDate;
	
	@FindBy(css="#txtProjectDescription")
	WebElement txtProjectDescription;
	
	@FindBy(css="#txtAddress")
	WebElement txtAddress;
	
	@FindBy(css="#txtProjectStartDate")
	WebElement txtProjectStartDate;
	
	@FindBy(css="#txtCity")
	WebElement txtCity;
	
	@FindBy(css="#txtState")
	WebElement txtState;
	
	@FindBy(css="#txtZip")
	WebElement txtZip;
	
	@FindBy(css="#txtCountry")
	WebElement txtCountry;
	
	@FindBy(css="#txtProjectPassword")
	WebElement txtProjectPassword;
	
	@FindBy(css=".noty_text")
	WebElement projectCreationSuccessMsg;
	
	@FindBy(css=".Country-Item[title='USA']")
	WebElement countryTitleItem;
	
	@FindBy(css=".State-Item[data-name='alabama']")
	WebElement stateTitleItem;
	
	@FindBy(css="#isAutoHyperlinkEnabled")
	WebElement checkboxEnableAutoHypLink;
	
	@FindBy(xpath="(//button[@class='btn btn-default pull-right'])[4]")
	WebElement btnSavePrjSettingsWin;
	
	@FindBy(xpath=".//*[@id='breadCrumb']/div[2]/ul/li[3]")
	WebElement prjNameBreadcrum;
	
	@FindBy(xpath="//a [@class='btn btn-primary project-tab active']")
	WebElement projectDasboadLink;
	
	
	@FindBy(css="#button-1")
	WebElement btnYes;
	
	@FindBy(css=".popover-content")
	WebElement contentPasswordField;

	@FindBy(xpath = "(//a[@onclick='javascript:return confirmlogout(this);'])[2]")
	WebElement Logout;

	@FindBy(xpath = "//i[@class='icon icon-app-project icon-2x pulse animated']")
	WebElement ProjectManagement;

	@FindBy(css = "#a_showRFIList")
	WebElement ProjectManagement_RFI;
	
	@FindBy(css = "#a_showSUBMITTALList")
	WebElement ProjectManagement_Submital;
	
	
	//========Photo Annotation Search
	@FindBy(xpath = "(//input[@placeholder='Building'])[1]")
	WebElement Filter_Building;
	
	@FindBy(xpath = "(//input[@placeholder='Level'])[1]")
	WebElement Filter_level;
	
	@FindBy(xpath = "(//input[@placeholder='Room'])[1]")
	WebElement Filter_Room;
	
	@FindBy(xpath = "(//input[@placeholder='Area'])[1]")
	WebElement Filter_Area;
	
	@FindBy(xpath = "(//input[@placeholder='Description'])[1]")
	WebElement Filter_Description_n;
	
	@FindBy(xpath = "//span [@class='icon-calender icon-calendar icon']")
	WebElement Filter_photocreationdate;
	
	@FindBy(xpath = "(//input [@id='txtphotoAttr'])[1]")
	WebElement BuildingText;
	
	@FindBy(xpath = "(//input [@id='txtphotoAttr'])[2]")
	WebElement levelText;
	
	@FindBy(xpath = "(//input [@id='txtphotoAttr'])[3]")
	WebElement RoomText;
	
	@FindBy(xpath = "(//input [@id='txtphotoAttr'])[4]")
	WebElement AreaText;
	
	@FindBy(xpath = "(//input [@id='txtphotoAttr'])[5]")
	WebElement Description_text;
	
	@FindBy(xpath = "(//span [@data-bind='text: ActPhotoCreateDateStr()'])[1]")
	WebElement Photo_text_Date;
	
	@FindBy(xpath = "//i [@class='icon icon-lg icon-th-list']")
	WebElement gridview;
	
	@FindBy(xpath = "//a[@class='photo-viewer preview']")
	WebElement photo;
	
	
	@FindBy(xpath = "(//button[@data-bind='click:CloseViewerPhotoSliderUI'])[2]")
	WebElement Done_Button;
	
	
	
	
	
	//=====old data xpath
	
	@FindBy(xpath = "(//span [@data-bind='text: ProjectName()'])[4]")
	WebElement Dashboard_first_project;
	
	@FindBy(xpath = "(//h4[@data-toggle='tooltip'])[3]")
	WebElement folderName_Olddata;
	
	@FindBy(xpath = "(//span[@class='h4_docname document-preview-event'])[1]")
	WebElement fileName_Olddata;
	
	@FindBy(xpath = "(//span[@ data-placement='top'])[1]")
	WebElement Photo;
	
	@FindBy(xpath = "(//span[@data-original-title='Chrysanthemum'])[2]")
	WebElement Photo_Olddata;
	
	//=================== Static data search=================================	
	
			
	@FindBy(xpath = "(//i [@class='icon icon-more-option'])[4]")
	WebElement three_rd_Project;
	
	
	
	
	// ======project search filter==============
	
	@FindBy(xpath = "//*[@id='txtProjectName_filter']")
	WebElement Filter_ProjectName;
	
	@FindBy(xpath = "//*[@id='txtProjectNumber_filter']")
	WebElement Filter_ProjectNumber;
	
	@FindBy(xpath = "//*[@id='txtProjectDesc_filter']")
	WebElement Filter_Description;
	
	@FindBy(xpath = "//*[@id='txtProjectCity']")
	WebElement Filter_CITY;
	
	@FindBy(xpath = "//*[@id='txtProjectState']")
	WebElement Filter_State;
	
	@FindBy(xpath = "//*[@id='txtProjectCountry']")
	WebElement Filter_Country;
	
	
	//===========sendfile filter search inbox ==========
	
	@FindBy(xpath = ".//*[@id='txtOrderId_filter']")
	WebElement Filter_Exactorder;
	
	@FindBy(xpath = "//span [@class='icon-calender icon-calendar icon']")
	WebElement Filter_InboxDate;
	
	@FindBy(xpath = "//*[@id='txtSendFileSubject_filter']")
	WebElement Filter_Inboxsubject;
	
	@FindBy(css = "#txtRecvEmailId_filter")
	WebElement Filter_Inboxreceivermail;
	
	@FindBy(xpath = ".//*[@id='txtSenderEmailId_filter']")
	WebElement Filter_InboxSendermail;
	
	
	@FindBy(xpath = "(//span [@class='text-muted'])[1]")
	WebElement Filter_exactorderText;
	
	@FindBy(xpath = "(//*[@id='sender-details-inbox'])[1]")
	WebElement Filter_subjectText;
	
	@FindBy(xpath = "(//span[@data-toggle='tooltip'])[1]")
	WebElement inbox_DateText;
	
	@FindBy(xpath = "//input[@placeholder='RFI due date']")
	WebElement Filter_DateText;
	
	@FindBy(xpath = "(//div/div[2]/div/div[2]/div/div[1]/div[2]/ul/li/section[2]/h4[2])[1]")
	WebElement Filter_SenderEmailText;
	
	//@FindBy(xpath = "//div/div[2]/div/div[2]/div[2]/ul/li[1]/section[1]/h4[2]")
	@FindBy(xpath = "//div/div[2]/div/div[2]/div[2]/ul/li[1]/section[1]/a")	
	WebElement Filter_receEmailText;
	
	@FindBy(xpath = "html/body/div[1]/div[3]/div/div[2]/div/div[2]/div/div[2]/div/div[2]/div[2]/ul/li[1]/section[1]/a")	
	WebElement Filter_receEmailText1;
	
	
	//@FindBy(xpath = "(//a[@ id='sender-details-inbox'])[1]")
	@FindBy(xpath = "//a[@class='tracking-details']")
	WebElement Filter_track_ExactorderText;
	
	@FindBy(xpath = "//a[@id='Tracking']")
	WebElement TrackingTab;
	
	
	//@FindBy(xpath = "(//span[@data-toggle='tooltip'])[1]")
	@FindBy(xpath = "(//div/div[2]/div/div[2]/div[2]/ul/li[1]/section[1]/h4[2]/a)[1]")
	WebElement Filter_Track_DateText;
	
	
	

	//================== submittal==============
	
	@FindBy(css = ".new-submittal.dev-new-submittal")
	WebElement createLink_Submital;
	
	@FindBy(css = ".new-submittal.dev-new-submittal")
	WebElement createSubmital_Link;
	//=============================================
	
	@FindBy(css = "#txtSubmittalNumber_filter")
	WebElement Submital_NumberInbox_filter;
	
	@FindBy(css = "#txtSubmittalName_filter")
	WebElement Submital_NameInbox_filter;
	
	@FindBy(css = "#ddlSubmittalType_filter")
	WebElement Submital_Type_filter;
	
	@FindBy(css = ".btn.btn-default.dropdown-toggle")
	WebElement Submital_Status_filter_dropdown;
	
	@FindBy(xpath = "//input[@value='Open!0']")
	WebElement Submital_Status_filter_OpenStatus;
	
	@FindBy(xpath = "//span [@class='icon-calender icon-calendar icon']")
	WebElement Submital_Duedate_filter_Calender;	

	@FindBy(css = "#txtSubmittalNumber")
	WebElement Submital_Duedate_filter;
	
	@FindBy(xpath = "//a [@class='lbl-Email']")
	WebElement EmailText;
	
	@FindBy(xpath = "//h5 [@class='lbl-Phone']")
	WebElement Contacttext;
	
	
	@FindBy(xpath = "(//h5 [@class='lbl-Phone'])[1]")
	WebElement phoneText;
	
	
	
	
	
	//======create submittal page=====
	
	@FindBy(css = "#txtSubmittalNumber")
	WebElement Submital_NumberInbox;
	
	@FindBy(css = "	#txtSubmittalName")
	WebElement Submital_NameInbox;
	
	@FindBy(css = "#txtSubmittalDueDate")
	WebElement Submital_duedateInbox;	
	
	@FindBy(css = ".btn.btn-default.dropdown-toggle")
	WebElement Submital_type;
	
	@FindBy(xpath = "(//a [@class='SubmittaltypeList'])[1]")
	WebElement Submital_List1;
	
	
	@FindBy(xpath = "(//button [@id='btnCreateSubmittal'])[1]")
	WebElement CreateAndSubmit_Button;
	
	
	@FindBy(xpath = "(//button [@id='btnCreateSubmittal'])[1]")
	WebElement Create_Button;
	
	@FindBy(css = "#txtSubmittalNumber")
	WebElement txtSubmittalNumber;

	@FindBy(css = "#txtSubmittalName")
	WebElement txtSubmittalName;

	@FindBy(css = "#txtSubmittalDueDate")
	WebElement txtSubmittalDueDate;

	@FindBy(css = ".icon-calender.icon-calendar.icon")
	WebElement Filter_DueDate1;
	
	@FindBy(xpath = "(//button[@class='btn btn-default dropdown-toggle'])[1]")
	WebElement dropDownSubmittalType;
	
	@FindBy(xpath = "//*[@id='ddlSubmittalType_filter']")
	WebElement dropDownSubmittalType_filter;
	
	
	@FindBy(xpath = "//a[@class='SubmittaltypeList' and @data-name='Bonds']")
	WebElement optionSubmittalTypeSelect;

	@FindBy(xpath = "(//button[@class='btn btn-default' and @data-target='.company-list'])[1]")
	WebElement btnCompanyList;
	
	@FindBy(xpath = "(//button[@class='btn btn-default' and @data-target='.company-list'])[2]")
	WebElement btnCompanyList2;

	@FindBy(xpath = "(//input[@type='radio'])[1]")
	WebElement radiobtnSelectCompany;
			
	@FindBy(xpath = "(//button[@id='btnCreateSubmittal'])[2]")
	WebElement btnCreateSubmittal;

	@FindBy(css = "#button-0")
	WebElement btnNo_WanttoCreateSubmittal;

	@FindBy(css = "#button-0")
	WebElement btnOK_appliedToAllImpSubmittals;
	
	@FindBy(css = "#button-1")
	WebElement btnYes_WanttoCreateSubmittal;

	@FindBy(css = "#button-2")
	WebElement btnYes_WithCoverPage;
	

	@FindBy(xpath = "(//span[@class='sbmtl-id'])[1]")
	WebElement submIdUnderList;

	@FindBy(xpath = "(//span[@class='sbmtl-nm'])[1]")
	WebElement submNameUnderList;

	@FindBy(xpath = "(//span[@class='sbmtl-type'])[1]")
	WebElement submTypeUnderList;

	@FindBy(xpath = "(//span[@class='text-info'])[1]")
	WebElement submDueDateUnderList;

	@FindBy(xpath = "(//span[@class='yellow-txt'])[1]")
	WebElement submOpenStatusUnderList;
	
	@FindBy(xpath = "(//i [@class='icon icon-app-contact icon-lg'])[2]")
	WebElement ASSIGNEDTo;
	
	
	@FindBy(xpath = "(//i [@class='icon icon-app-contact icon-lg'])[3]")
	WebElement CC;
	

	@FindBy(xpath = "(//span[@ data-toggle='tooltip'])[3]")
	WebElement Assignee ;

	
	
	@FindBy(xpath = "//*[@id='txtSubmittalComment']")
	WebElement Comments_submittal;
	
	@FindBy(xpath = "//*[@id='btnCreatesubmittallist']")
	WebElement SubmitButton;
	
	

	//==============================
	//@FindBy(xpath = " (//*[text()[contains(.,'Send files')]])[1]")
	@FindBy(css = ".icon.icon-send-files.icon-lg.pw-icon-white.pulse.animated")	
	WebElement sendfilelink;
	
	@FindBy(xpath = "// i[@class='icon icon-app-contact']")
	WebElement Addcontact;
	
	
	@FindBy(xpath = "//input [@name='qqfile']")
	WebElement selectfiles;
	
	@FindBy(xpath = ".//*[@id='txtContactSearch']")
	WebElement SelectUse_Inbox;
	
	@FindBy(xpath = "//input[@ class='pull-left chklist']")
	WebElement SelectUser_CheckBox;

	@FindBy(xpath = "//*[@id='divAddressBook']/div/div/div[1]/div/div/a[1]")
	WebElement SearchButton;

	@FindBy(xpath = ".//*[@id='btnAddContact']")
	WebElement SelectUserButton;
	
	@FindBy(xpath = ".//*[@id='txtSubject']")
	WebElement Subject;
	
	@FindBy(xpath = "//*[@id='txtMessage']")
	WebElement message;
	
	@FindBy(css = "#btnAddContact")
	WebElement selectbutton;
	
	@FindBy(css = "#send-file")
	WebElement send;
	
	
	
	//===============================

	@FindBy(xpath = "//*[@id='btnSearchOptions']")
	WebElement Search_GlobalButton;

	@FindBy(xpath = "(//i [@class='icon icon-app-punch icon-lg'])[1]")
	WebElement Search_Punchdropdown;

	@FindBy(xpath = "//*[@id='txtSearchKeyword']")
	WebElement TxtSearch_Keyword;

	@FindBy(xpath = "//span [@data-bind='text: PunchDescription()']")
	WebElement Punch_Text;

	@FindBy(xpath = "(//span[@ data-placement='top'])[1]")
	WebElement RFI_Text;

	@FindBy(xpath = "(//span [@data-bind='text: RFINumber()'])[1]")
	WebElement RFI_Number;
	
	@FindBy(xpath = "(//span [@class='label label-warning'])[1]")
	WebElement RFI_Duedate_Text;
	
	
	@FindBy(xpath = "//div [@class='pw-droparea']")
	WebElement DragAnddropLocation;
	
	@FindBy(xpath = "//i [@class='icon icon-app-contact'] ")
	WebElement AddContact;
	
	@FindBy(css = ".icon.icon-search.icon-lg")
	WebElement searchbutton;
	
	@FindBy(xpath = "//a[@title='All RFI']")
	WebElement AllRFITAB;

	
	
	@FindBy(css = ".icon.icon-contacts.icon-lg.pw-icon-white.pulse.animated")
	WebElement Contacts;
	
	@FindBy(xpath = "//span[@data-original-title='Tstemp1 Tstemp1']")
	WebElement Name;	
	
	@FindBy(xpath = "//input [@id='txtSearchKeyword']")
	WebElement ModuleInbox;
	
	@FindBy(xpath = "//span [@class='icon-calender icon-calendar icon']")
	WebElement RevisionDate;
	
	
	
	
	

	@FindBy(css = "#aAdvanceFilter")	
	//@FindBy(xpath = "//i[@class='icon icon-filter']")	
	WebElement AdvanceSearch_Link;
	
	@FindBy(xpath = "//span [@class='icon-calender icon-calendar icon']")
	WebElement Date_Link;	
	
	@FindBy(xpath = "//*[@id='txtContactFirstName_filter']")
	WebElement AdvanceSearch_FirstName;
	
	@FindBy(xpath = "//*[@id='txtContactLastName_filter']")
	WebElement AdvanceSearch_LastName;
	
	@FindBy(xpath = "//*[@id='txtContactEmail_filter']")
	WebElement AdvanceSearch_Email;
	
	@FindBy(xpath = "//*[@id='txtContactPhoneWork_filter']")
	WebElement AdvanceSearch_Phone;
	
	@FindBy(xpath = "//*[@id='txtContactCity_filter']")
	WebElement AdvanceSearch_City;
	
	@FindBy(xpath = "//*[@id='txtContactAddress_filter']")
	WebElement AdvanceSearch_Address;
	
	//==== Filter Search========================
	
	@FindBy(xpath = "//input [@id='txtDocumentName_filter']")
	WebElement AdvanceSearch_SheetNumber;
	
	
	@FindBy(xpath = "//input [@id='txtSheetDesc_filter']")
	WebElement AdvanceSearch_SheetName;
	
	
	@FindBy(xpath = "//input [@id='txtDocumentRevisionName_filter']")
	WebElement AdvanceSearch_RevisionName;
	
	
	@FindBy(xpath = "//*[@id='txtDocumentDiscipline_filter']")
	WebElement AdvanceSearch_Disicipline;
	
	
	@FindBy(xpath = "//span [@class='icon-calender icon-calendar icon']")
	WebElement AdvanceSearch_RevisionDate;
	
	@FindBy(xpath = "//input [@id='txtDocName']")
	WebElement IndexDocumentName;
	
	@FindBy(xpath = "//input [@id='txtRevName']")
	WebElement IndexRevision;
	
	@FindBy(xpath = "//*[@id='txtRevDate']")
	WebElement Indexdate;
	
	@FindBy(xpath = "//*[@id='txtDiscipline']")
	WebElement IndexDecilipline;
	
	@FindBy(xpath = "//textarea [@id='txtDesc']")
	WebElement Indexdesc;
	
	
	@FindBy(xpath = "//i[@class='icon icon-ellipsis-horizontal media-object icon-lg']")
	WebElement Indexthreedoticon;
	
	
	
	@FindBy(xpath = "//a [@class='edit-attributes btnEditAttr']")
	WebElement IndexEditField;
	
	
	
	
	
	
	
	
	
	
	//====== Filter search punch=====================
	
	@FindBy(xpath = "//*[@id='txtPunchDescription_filter']")
	WebElement AdvanceSearch_PunchDescription;
	
	@FindBy(xpath = "//*[@id='txtPunchCreateBy_filter']")
	WebElement AdvanceSearch_PunchCreator;
	
	@FindBy(xpath = "//*[@id='txtPunchAssignee_filter']")
	WebElement AdvanceSearch_PunchAssignee;
	
	@FindBy(xpath = "//input[@placeholder='Status']")
	WebElement AdvanceSearch_PunchStatus;	
	
	
	@FindBy(xpath = "(//span [@data-bind='text: PunchDescription()'])[1]")
	WebElement PunchDescription_text;
	
	@FindBy(xpath = "//*[@id='txtASCompany']")
	WebElement Company_text;
	
	@FindBy(xpath = "//*[@id='txtProjectState']")
	WebElement AdvanceSearch_ProjectState;
	
	@FindBy(xpath = ".//*[@id='txtProjectCountry']")
	WebElement AdvanceSearch_ProjectCountry;
	
	
	@FindBy(xpath = "(//i [@class='icon icon-more-option'])[3]")
	WebElement projectMultipleObject;
	
	@FindBy(xpath = "(//a [@class='edit-project'])[3]")
	WebElement projectMultipleObject_edit;
	
	@FindBy(xpath = "//*[@id='btnCancel']")
	WebElement project_CancelButton;
	
	@FindBy(xpath = "//*[@id='txtState']")
	WebElement project_Statetext;
	
	@FindBy(xpath = ".//*[@id='txtCountry']")
	WebElement project_Countrytext;
	
	
	
	

	
	@FindBy(xpath = "(//span [@data-bind='text: PunchDescription()'])[1]")
	WebElement Punchcreator_text;
	
	@FindBy(xpath = "(//span [@data-bind='text: PunchDescription()'])[1]")
	WebElement PunchAssignee_text;
	
	
	@FindBy(xpath = "(//span [@class='yellow-txt'])[1]")
	WebElement Punchstatus_text;
	
	@FindBy(xpath = "(//span [@class='yellow-txt'])[1]")
	WebElement submittalstatus_text;
	

	@FindBy(xpath = "	(//span[@data-toggle='tooltip'])[1]")
	WebElement Project_NameText;
	
	@FindBy(xpath = "//div [@class='prj-id truncate-txt']")
	WebElement Project_NumberText;
	
	@FindBy(xpath = "(//div [@class='prj-id truncate-txt']/span)[2]")
	WebElement Project_NumberText1;
	
	
	
	@FindBy(xpath = ".//*[@id='txtProjectDescription']")
	WebElement Descriptiontext;
	
	@FindBy(xpath = "//*[@id='txtCity']")
	WebElement CITYtext;

	
	
	//==========Filter search Punch end================================
	

	@FindBy(xpath = "(//span[@ class='icon-calender icon-calendar icon'])")
	WebElement calender_Link;

	@FindBy(xpath = "//*[@id='txtRFINumber_filter']")
	WebElement Filter_RFINumber;

	@FindBy(xpath = "//*[@id='txtRFINumber_filter']")
	WebElement Filter_RFIDueDate;

	@FindBy(xpath = "//*[@id='txtAssignBy_filter']")
	WebElement Filter_Assignee;

	@FindBy(xpath = "//*[@id='txtCreateBy_filter']")
	WebElement Filter_Creator;

	@FindBy(xpath = "//*[@id='txtStatus_filter']")
	WebElement Filter_Status1;

	@FindBy(xpath = "//*[@id='txtCreateBy_filter']")
	WebElement Filter_Status;

	@FindBy(xpath = "//*[@placeholder='Building']")
	WebElement Building;

	@FindBy(xpath = "//*[@placeholder='Level']")
	WebElement Level;

	@FindBy(xpath = "//*[@placeholder='Room']")
	WebElement room;

	@FindBy(xpath = "//*[@placeholder='Area']")
	WebElement Area;

	@FindBy(xpath = "//*[@placeholder='Description']")
	WebElement Description;

	@FindBy(xpath = "//input [@id='txtStatus_filter']")
	WebElement Status_Inbox;
	
	@FindBy(xpath = "//*[@id='txtPunchDescription_filter']")
	WebElement filterSearch_DescriptionName;

	@FindBy(xpath = "//*[@id='txtProjectName_filter']")
	WebElement FilterProject_Name;

	@FindBy(xpath=".//*[@id='btnSearchFilter']/..")
	WebElement SearchButton_Advance;

	@FindBy(css = "#btnSearch")
	WebElement Search_Button;
	
	@FindBy(xpath = "(//i[@class='icon icon-ellipsis-horizontal media-object icon-lg'])[3]")
	WebElement PhotoMore_Option;
	
	@FindBy(xpath = "(//a [@class='edit-photo-data'])[1]")
	WebElement PhotoMore_edit;
	
	@FindBy(xpath = "//*[@id='ulSearchResult']/li/div/div[2]/h4")
	WebElement PhotoMore_file;
	
	@FindBy(xpath = "//a [@class='morepages-event']")
	WebElement ShowEdit;
	
	
	@FindBy(xpath = ".//*[@id='divResultExtraData']/div/div/div/h5[4]")
	WebElement WholeTEXT;
	
	
	
	
	
	
	
	
	@FindBy(xpath = "(//*[@id='ulRFIList'])[1]")
	WebElement RFILIst;
	
	

	@FindBy(xpath = "//button[@class='btn btn-primary' and @data-bind='click: SelectContactEvent']")
	WebElement SelectUser_Button;

	@FindBy(css = "#liWhereIAmAll>a")
	WebElement Everywhere_AllContents;
	

	@FindBy(css = "//*[@id='liWhereIAmSubmittal']/a")
	WebElement SubmittalLink;
	
	


	@FindBy(css = "#liWhereIAmSearchIn>a")
	WebElement search;

	@FindBy(xpath = "//span [@data-bind='text: ProjectName()']")
	WebElement Staticproject;
	
	
	@FindBy(xpath = "(//span [@class='sbmtl-id'])[1]")
	WebElement SubmittalN0;	
	
	@FindBy(xpath = "//*[@id='divPhotoItemContainer']/ul/li[2]/section[2]/h4")
	WebElement PhotoText;	
	
	
	
	
	@FindBy(xpath = "(//span[@ style='white-space:pre;'])[1]")
	WebElement Global_Album;	
	
	@FindBy(xpath = "(//span[@class='sbmtl-nm'])[1]")
	WebElement SubmittalName_t;	
	
	@FindBy(xpath = "(//span [@class='yellow-txt'])[1]")
	WebElement SubmittalStatus;	
	
	
	
	@FindBy(xpath = "(//div [@class='prj-dtl'])[1]")
	WebElement FirstSubmittallist;
	
	@FindBy(xpath = "//span [@class='truncate-sm']")
	WebElement submittalTypeText;
	
	@FindBy(xpath = "//*[@id='divEditSubmittalUI']/div/div[3]/button[1]")
	WebElement CloseSubmittalwindow;
	
	
	
	
	
	
	@FindBy(xpath = "(//span[@ class='sbmtl-nm'])[1]")
	WebElement SubmittalName;

	@FindBy(xpath = "//*[@id='txtSubmittalNumber_filter']")
	WebElement Filter_SubmittalNumber;	
	
	
	@FindBy(xpath = "//*[@id='txtSubmittalName_filter']")
	WebElement Filter_SubmittalName;
	
	@FindBy(xpath = ".//*[@id='ddlSubmittalType_filter']")
	WebElement Filter_SubmittalType;
	
	
	@FindBy(xpath = ".//*[@id='submittalStatusSelectToggle']/button")
	WebElement Filter_SubmittalStatus;
	
	@FindBy(xpath = "//*[@id='submittalStatusSelectToggle']/div/ul/li[4]/label/span")
	WebElement Submittal_Status_filter;
	
	
	@FindBy(xpath = "//span [@class='icon-calender icon-calendar icon']")
	WebElement Filter_Submittalduedate;
	
	

	@FindBy(xpath = "//h4[@data-placement='top']")
	WebElement folder_Number;

	@FindBy(xpath = "//h4 [@class='pw-file-name']")
	WebElement FileName;
	
	@FindBy(xpath = "(//input[@ id='txtphotoAttr'])[1]")
	WebElement Building_Value;
	
	@FindBy(xpath = "(//i [@class='icon icon-ellipsis-horizontal media-object icon-lg'])[3]")
	WebElement PhotoThree_dot;
	
	@FindBy(xpath = "(//a[@class='edit-photo-data'])[1]")
	WebElement EditAttribute;


	@FindBy(css = ".media.navbar-collapse>section>h4>span")
	WebElement FolderName_photo;

	@FindBy(xpath = "//*[@class='day old' and text()='31']")
	WebElement old31;

	@FindBy(xpath = "//*[@class='day' and text()='31']")
	WebElement new31;

	@FindBy(xpath = "//*[@class='day' and text()='29']")
	WebElement feb29;
	
	@FindBy(xpath = ".//*[@class='day' and text()='%s']")
	WebElement expDate;

	// =======================================================

	@Override
	protected void load() {
		isPageLoaded = true;

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}

	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory
	 * class.
	 * 
	 * @param driver
	 */
	public Search_Testcases(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	public boolean ObjectFeedback() {
		SkySiteUtils.waitForElement(driver, FeedBackmeesage, 20);
		Log.message("Waiting for FreeTrail Link to be appeared");
		if (FeedBackmeesage.isDisplayed())
			return true;
		else
			return false;
	}
	
	
	public boolean UploadFiles(String FolderPath, int FileCount)
			throws InterruptedException, AWTException, IOException {
		boolean result = false;
		SkySiteUtils.waitTill(2000);
		Log.message("Waiting for upload file button to be appeared");
		selectfiles.click();
		SkySiteUtils.waitTill(2000);
		// =================================
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();

		String Sys_Download_Path = "./File_Download_Location/" + rn.nextFileName() + ".txt";
		Log.message("system download location2" + Sys_Download_Path);
		String tmpFileName = Sys_Download_Path;
		// String tmpFileName="c:/"+rn.nextFileName()+".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;

		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a
												// variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(1000);
			}
		}

		output.flush();
		output.close();

		Log.message("waiting AutoIT Script!!");
		SkySiteUtils.waitTill(15000);

		// ====================Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		File dest = new File(PropertyReader.getProperty("Upload_TestData_viewer_SingleFile").toString());
		String path = dest.getAbsolutePath();
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + path + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(12000);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(5000);
		// ====================Delete the temp file=====================
		try {
			File file = new File(tmpFileName);
			if (file.delete()) {
				Log.message(file.getName() + " is deleted!");
			} else {
				Log.message("Delete operation is failed.");
			}
		} catch (Exception e) {
			Log.message("Exception occured!!!" + e);
		}

		return result;

	}
	
	public boolean Create_Submital_AndValidate(String Submittal_Number,String Submittal_Name) 
	
		{
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, createSubmital_Link, 60);
		if (createSubmital_Link.isDisplayed()){
			createSubmital_Link.click();
		Log.message(" create Submittal Button Clicked Sucessfully");
		}
		SkySiteUtils.waitForElement(driver, txtSubmittalNumber, 60);
		txtSubmittalNumber.sendKeys(Submittal_Number);
		SkySiteUtils.waitTill(3000);
		Log.message(" Entered The value in submittal Number Field");
		SkySiteUtils.waitForElement(driver, txtSubmittalName, 60);
		txtSubmittalName.sendKeys(Submittal_Name);
		SkySiteUtils.waitTill(3000);
		Log.message(" Entered The value in submittal Name Field");
		SkySiteUtils.waitForElement(driver, txtSubmittalName, 60);
		txtSubmittalDueDate.click();// Click on due date
		Log.message(" Entered The value in Date Field");
		SkySiteUtils.waitTill(3000);
		WebElement element1 = driver.findElement(By.xpath("//td [@class='day active']"));
	    JavascriptExecutor executor1 = (JavascriptExecutor)driver;
	    executor1.executeScript("arguments[0].click();", element1);
	    //driver.findElement(By.xpath("//td [@class='day active today']")).click();// Select due date from cal
		SkySiteUtils.waitTill(5000);
		
		dropDownSubmittalType.click();
		Log.message(" Click SubmittalType Drop Down");
		SkySiteUtils.waitTill(2000);
		optionSubmittalTypeSelect.click();
		Log.message("Selected submittal Type from dropdown.");		
		//Getting number of approvers
		int Avl_Approvers_Count=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//button[@class='btn btn-default' and @data-target='.company-list']")); 
		for (WebElement Element : allElements) 
		{ 
			Avl_Approvers_Count = Avl_Approvers_Count+1; 	
		} 
		Log.message("Available Approver Count is: "+Avl_Approvers_Count);
		
		if(Avl_Approvers_Count==1)
		{
			btnCompanyList.click();
			SkySiteUtils.waitForElement(driver, radiobtnSelectCompany, 20);
			SkySiteUtils.waitTill(2000);
			radiobtnSelectCompany.click();
			Log.message("Selected first company radio button.");
		}
		else if(Avl_Approvers_Count==2)
		{
			btnCompanyList.click();
			SkySiteUtils.waitForElement(driver, radiobtnSelectCompany, 20);
			SkySiteUtils.waitTill(2000);
			radiobtnSelectCompany.click();
			Log.message("Selected first company radio button.");
			SkySiteUtils.waitTill(2000);
			btnCompanyList2.click();
			SkySiteUtils.waitForElement(driver, radiobtnSelectCompany, 20);
			SkySiteUtils.waitTill(2000);
			radiobtnSelectCompany.click();
			Log.message("Selected first company radio button.");
		}
		SkySiteUtils.waitTill(2000);
		btnCreateSubmittal.click();
		Log.message("Clicked on create submittal button.");
		//SkySiteUtils.waitForElement(driver, btnNo_WanttoCreateSubmittal, 20);
		//btnYes_WanttoCreateSubmittal.click();
		//Log.message("Clicked on button 'YES' for conform to create submittal.");
		//SkySiteUtils.waitTill(1000);
		//SkySiteUtils.waitForElement(driver, notificationMsg, 20);
		//String NotifyMsg_SubmitalCreate=notificationMsg.getText();
		//Log.message("Message displayed after submittal create is: "+NotifyMsg_SubmitalCreate);
		SkySiteUtils.waitForElement(driver, submIdUnderList, 20);
		SkySiteUtils.waitTill(5000);
		String SubmId_AllSubTab = submIdUnderList.getText();
		Log.message("Sub ID is: " + SubmId_AllSubTab);
		String SubmName_AllSubTab = submNameUnderList.getText();
		Log.message("Sub Name is: " + SubmName_AllSubTab);
		//String SubmType_AllSubTab = submTypeUnderList.getText();
		//Log.message("Sub Type is: " + SubmType_AllSubTab);
		String SubmStatus_AllSubTab = submOpenStatusUnderList.getText();
		Log.message("Sub status is: " + SubmStatus_AllSubTab);

		if ((SubmId_AllSubTab.equalsIgnoreCase(Submittal_Number)) && (SubmName_AllSubTab.equalsIgnoreCase(Submittal_Name))
				&& (SubmStatus_AllSubTab.equalsIgnoreCase("OPEN"))) 
		{
			Log.message("Created Submittal is successfully.");
			SkySiteUtils.waitTill(8000);
			return true;
		} else {
			Log.message("Created Submittal is failed.");
			return false;
		}
	}

	public void SelectGalleryFolder(String Gallery) {
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
		// Getting Folder count after created a new folder
		int Avl_Fold_Count = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//li[@class='media navbar-collapse']"));
		for (WebElement Element : allElements) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		for (int j = 1; j <= Avl_Fold_Count; j++) {
			// actualFolderName =
			// driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["
			// + j + "]/section[2]/h4")).getText();
			actualFolderName = driver.findElement(By.xpath("//li[@class='media navbar-collapse']")).getText();

			Log.message("Act Name:" + actualFolderName);
			SkySiteUtils.waitTill(5000);
			// Validating the new Folder is created or not
			if (actualFolderName.trim().contentEquals(Gallery)) {
				driver.findElement(By.xpath("(//li[@class='media navbar-collapse'])[" + j + "]")).click();// Select
																											// Gallery
																											// folder
				Log.message("Clicked on Gallery Folder");
				SkySiteUtils.waitTill(5000);
				break;
			}
		}

	}

	public boolean Logout() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		Log.message("Entered into Logout Method");
		SkySiteUtils.waitForElement(driver, Profile, 60);
		Profile.click();
		Log.message("Profile Option Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, Logout, 60);
		SkySiteUtils.waitTill(2000);
		Logout.click();
		Log.message("Logout option Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, YesButton, 60);
		YesButton.click();

		return result;

	}

	public ProjectDashboardPage YesButton_Alert() throws Throwable

	{
		SkySiteUtils.waitForElement(driver, YesButton, 60);
		YesButton.click();
		SkySiteUtils.waitTill(2000);
		String actualTitle = "Sign in - SKYSITE";
		String expectedTitle = driver.getTitle();
		Log.message("expected title is:" + expectedTitle);
		return new ProjectDashboardPage(driver).get();

	}

	private boolean isFileDownloaded_Ext(String dirPath, String ext) {
		boolean flag = false;
		File dir = new File(dirPath);
		File[] files = dir.listFiles();
		if (files == null || files.length == 0) {
			flag = false;
		}

		for (int i = 1; i < files.length; i++) {
			if (files[i].getName().contains(ext)) {
				flag = true;
			}
		}
		return flag;
	}

	public boolean isFileDownloaded(String downloadPath, String fileName) {
		boolean flag = false;
		File dir = new File(downloadPath);
		File[] dir_contents = dir.listFiles();

		for (int i = 0; i < dir_contents.length; i++) {
			if (dir_contents[i].getName().equals(fileName))
				return flag = true;
		}

		return flag;
	}

	public void ProjectManagement() throws Throwable

	{
		
		Thread.sleep(2000);
		SkySiteUtils.waitForElement(driver, ProjectManagement, 60);
		ProjectManagement.click();
		Thread.sleep(2000);
		Log.message("project Management Link Click Sucessfully");
		SkySiteUtils.waitForElement(driver, ProjectManagement_RFI, 60);
		ProjectManagement_RFI.click();
		Log.message("RFI Link Click Sucessfully");


	}

	public boolean NewgallaryFolder_Create(String Foldername) {
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 60);
		Log.message("Waiting for Create Folder button to be appeared");
		SkySiteUtils.waitTill(3000);
		btnAddNewFolder.click();// Clicking on Create New Folder
		Log.message("Add New folder button clicked!!!");
		SkySiteUtils.waitForElement(driver, btnUploadFilesToFolder, 60);
		SkySiteUtils.waitTill(5000);
		txtBoxFolderName.sendKeys(Foldername);// Giving Folder Names Randomly
		Log.message("Folder Name: " + Foldername + " has been entered in Folder Name text box.");
		btnCreateFolder.click();
		Log.message("Create Folder button clicked!!!");
		SkySiteUtils.waitForElement(driver, notificationMsg, 60);
		String Msg_Folder_Create = notificationMsg.getText();
		Log.message("Notification Message after folder create is: " + Msg_Folder_Create);
		SkySiteUtils.waitTill(5000);
		// Getting Folder count after created a new folder
		int Avl_Fold_Count = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		for (int j = 1; j <= Avl_Fold_Count; j++) {
			actualFolderName = driver
					.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4"))
					.getText();
			Log.message("Exp Name:" + Foldername);
			Log.message("Act Name:" + actualFolderName);

			// Validating the new Folder is created or not
			if (Foldername.trim().contentEquals(actualFolderName.trim())) {
				Log.message("Folder is created successfully with name: " + Foldername);
				break;
			}
		}

		// Final Validation - Folder Create
		if ((Foldername.trim().contentEquals(actualFolderName.trim()))
				&& (Msg_Folder_Create.contentEquals("Folder created successfully"))) {
			Log.message("Folder is created successfully with name: " + Foldername);
			return true;
		} else {
			Log.message("Folder creattion FAILED with name: " + Foldername);
			return false;
		}

	}

	public boolean UploadPhotos_InGallery(String FolderPath, int FileCount)
			throws InterruptedException, AWTException, IOException {
		boolean result = false;
		SkySiteUtils.waitForElement(driver, btnUploadPhotos, 30);
		Log.message("Waiting for upload file button to be appeared");
		btnUploadPhotos.click();// Click on Upload photos
		Log.message("Clicked on upload photos button.");
		SkySiteUtils.waitForElement(driver, headerMsgUploadPhotoWindow, 40);
		Log.message("Waiting for choose file button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnChooseFile.click();// Click on Choose File
		Log.message("Clicked on choose file button.");
		SkySiteUtils.waitTill(5000);

		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String Sys_Download_Path = "./File_Download_Location/" + rn.nextFileName() + ".txt";
		Log.message("system download location2" + Sys_Download_Path);
		String tmpFileName = Sys_Download_Path;
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;

		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(6000);
			}
		}

		output.flush();
		output.close();
		SkySiteUtils.waitTill(3000);
		// Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		File dest = new File(FolderPath);
		String path = dest.getAbsolutePath();
		SkySiteUtils.waitTill(5000);
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + path + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(15000);
		SkySiteUtils.waitForElement(driver, btnFileUpload, 60);
		SkySiteUtils.waitTill(5000);
		// Delete the temp file
		try {
			File file = new File(tmpFileName);
			if (file.delete()) {
				Log.message(file.getName() + " is deleted!");
			} else {
				Log.message("Delete operation is failed.");
			}
		} catch (Exception e) {
			Log.message("Exception occured!!!" + e);
		}

		SkySiteUtils.waitTill(5000);
		btnFileUpload.click();
		Log.message("Clicked on Upload button.");
		SkySiteUtils.waitForElement(driver, btnSaveAttributes, 180);
		Log.message("Waiting for Save attributes button to be appeared.");
		SkySiteUtils.waitTill(5000);

		// Validations on Add Attributes page
		String Photo_Information = driver.findElement(By.xpath("//h4[@class='pull-left']")).getText();// Getting
																										// count
		Log.message(Photo_Information);
		// Split the line and get the count
		Splitter SPLITTER = Splitter.on(CharMatcher.whitespace()).omitEmptyStrings();
		List<String> elements = Lists.newArrayList(SPLITTER.split(Photo_Information));
		// Getting count - output
		String OnlyCount = elements.get(3);
		Log.message("The required count value is: " + OnlyCount);
		// Convert String to int
		int PhotosCount = Integer.parseInt(OnlyCount);
		int PageCount = PhotosCount / 10;
		int Extra_Photo = PhotosCount % 10;
		int totalPageCount = 0;

		// Getting Page count
		if ((PageCount > 0) && (Extra_Photo > 0))// Multiple of 10 plus extra
		{
			totalPageCount = PageCount + 1;
		}

		if ((PageCount > 0) && (Extra_Photo == 0))// Multiple of 10 only
		{
			totalPageCount = PageCount;
		}

		if ((PageCount == 0) && (Extra_Photo < 10))// less than 10 only
		{
			totalPageCount = 1;
		}

		Log.message("Total Page Count is:" + totalPageCount);

		for (int i = 1; i <= totalPageCount; i++) {
			if ((PhotosCount < 10) && (PhotosCount == FileCount)) {
				// Fill the Attributes and use the copy down
				String Building = PropertyReader.getProperty("Building");
				driver.findElement(By.xpath("(//input[@placeholder='Building'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Building'])[1]")).sendKeys(Building);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[1]")).click();

				String Level = PropertyReader.getProperty("Level");
				driver.findElement(By.xpath("(//input[@placeholder='Level'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Level'])[1]")).sendKeys(Level);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[2]")).click();

				String room = PropertyReader.getProperty("Room");
				driver.findElement(By.xpath("(//input[@placeholder='Room'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Room'])[1]")).sendKeys(room);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[3]")).click();

				String area = PropertyReader.getProperty("Area");
				driver.findElement(By.xpath("(//input[@placeholder='Area'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Area'])[1]")).sendKeys(area);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[4]")).click();

				String Description = PropertyReader.getProperty("Description");
				driver.findElement(By.xpath("(//input[@placeholder='Description'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Description'])[1]")).sendKeys(Description);

				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[5]")).click();
				SkySiteUtils.waitTill(5000);
				driver.findElement(By.xpath(".//*[@id='divAttrContainer']/div[1]/div[2]/div/div[2]/button")).click();
				SkySiteUtils.waitTill(5000);

			}
			if ((PhotosCount >= 10) && (PhotosCount == FileCount)) {
				// Fill the Attributes and use the copy down
				String Building = PropertyReader.getProperty("Building");
				driver.findElement(By.xpath("(//input[@placeholder='Building'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Building'])[1]")).sendKeys(Building);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[1]")).click();

				String Level = PropertyReader.getProperty("Level");
				driver.findElement(By.xpath("(//input[@placeholder='Level'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Level'])[1]")).sendKeys(Level);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[2]")).click();

				String room = PropertyReader.getProperty("Room");
				driver.findElement(By.xpath("(//input[@placeholder='Room'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Room'])[1]")).sendKeys(room);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[3]")).click();

				String area = PropertyReader.getProperty("Area");
				driver.findElement(By.xpath("(//input[@placeholder='Area'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Area'])[1]")).sendKeys(area);
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[4]")).click();
				String Description = PropertyReader.getProperty("Description");
				driver.findElement(By.xpath("(//input[@placeholder='Description'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Description'])[1]")).sendKeys(Description);

				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[5]")).click();
				SkySiteUtils.waitTill(5000);
				driver.findElement(By.xpath(".//*[@id='divAttrContainer']/div[1]/div[2]/div/div[2]/button")).click();
				SkySiteUtils.waitTill(5000);
			}

		} // end of i for loop
		SkySiteUtils.waitTill(10000);

		// Calling Upload Photo validation script
		result = this.ValidateUploadPhotos(FolderPath, FileCount);// Calling
																	// validate
																	// photos
																	// method
		if (result == true) {
			Log.message("Photos Upload is working successfully!!!");
			return true;
		} else {
			Log.message("Photos Upload is not working!!!");
			return false;
		}

	}

	public boolean ValidateUploadPhotos(String FolderPath, int FileCount) throws InterruptedException {
		boolean result = false;
		String ActPhotoName = null;
		String expPhotoname = null;
		int uploadSuccessCount = 0;

		try {
			// Reading all the photo names in a folder
			File[] files = new File(FolderPath).listFiles();
			for (File file : files) {
				if (file.isFile()) {
					expPhotoname = file.getName();// Getting Photo Names into a
													// variable
					Log.message("Expected Photo name is:" + expPhotoname);
					SkySiteUtils.waitTill(1000);
					for (int n = 1; n <= FileCount + (FileCount - 1); n++) {
						n = n + 1;
						ActPhotoName = driver
								.findElement(By.xpath(
										"html/body/div[1]/div[4]/div[2]/div/div/div/ul/li[" + n + "]/section[2]/h4"))
								.getText();
						Log.message("Actual Photo name is:" + ActPhotoName);

						if (expPhotoname.trim().contains(ActPhotoName.trim())) {
							uploadSuccessCount = uploadSuccessCount + 1;
							Log.message(
									"File uploded success for:" + ActPhotoName + "Sucess Count:" + uploadSuccessCount);
							break;
						}
					}

				}

			}

			// Checking whether file count is equal or not
			if (FileCount == uploadSuccessCount) {
				result = true;
			} else {
				result = false;
			}

		} // End of try block

		catch (Exception e) {
			result = false;
		}

		finally {
			return result;
		}
	}

	public boolean Searchscenario_RFI() throws Throwable {
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		//SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		//Search_GlobalButton.click();
		Log.message("Module Search Option Click Sucessfully");
		/*
		 * SkySiteUtils.waitForElement(driver, Search_Punchdropdown, 120);
		 * Search_Punchdropdown.click();
		 */
		Log.message("Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		TxtSearch_Keyword.clear();
		SkySiteUtils.waitTill(5000);
		String Anything = RFI_Text.getText();
		TxtSearch_Keyword.sendKeys(Anything);
		SkySiteUtils.waitTill(50000);
		//SkySiteUtils.waitForElement(driver, Search_Button, 60);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		String Anything1 = RFI_Text.getText();
		if (Anything.contains(Anything1)) {
			Log.message("Maching RFI Found!!");
            return true;
		} else {

			Log.message("Matching RFI Not Found!!");
            return false;
		}

		//return result;

	}
	
	
	
	public boolean Sendfile_selection() throws Throwable {
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, sendfilelink, 120);		
		sendfilelink.click();
		Log.message("Send Link click Sucessfully");
		// ===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);																
		Log.message("FileCount: " + FileCount);
		SkySiteUtils.waitTill(1000);
		UploadFiles(FolderPath, FileCount);	
		SkySiteUtils.waitTill(1000);
		Addcontact.click();
		Log.message("Add contact link added sucessfully click Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		Log.message("Entered the Value In Select Email field");		
		SkySiteUtils.waitTill(1000);
		String emailbox = PropertyReader.getProperty("Owneremail");
		SkySiteUtils.waitTill(1000);
		SelectUse_Inbox.sendKeys(emailbox);
		Log.message("Entered the value in  Inbox");
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		searchbutton.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("Select Checkbox");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, selectbutton, 60);
		selectbutton.click();		
		Log.message("send Button clicked sucessfully");
		SkySiteUtils.waitForElement(driver, Subject, 60);
		String subject1 = PropertyReader.getProperty("sendfile_subject");
		SkySiteUtils.waitTill(1000);
		Subject.sendKeys(subject1);
		Log.message("Subject Entered Sucessfully");
		SkySiteUtils.waitForElement(driver, message, 60);
		String Mess = PropertyReader.getProperty("sendfile_Message");
		SkySiteUtils.waitTill(1000);
		message.sendKeys(Mess);
		Log.message("Message Entered Sucessfully");		
		SkySiteUtils.waitForElement(driver, send, 60);
		send.click();
		Log.message("Send Button Click Sucessfully");
		SkySiteUtils.waitTill(30000);
		return result;
		
	
	}

	public boolean Searchscenario_punch() throws Throwable {
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		//Search_GlobalButton.click();
		Log.message("Local Search Option Click Sucessfully");		
		Log.message("Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		TxtSearch_Keyword.clear();
		SkySiteUtils.waitTill(2000);
		String Anything = Punch_Text.getText();
		SkySiteUtils.waitTill(2000);
		TxtSearch_Keyword.sendKeys(Anything);
		//SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(30000);
		Search_Button.click();		
		Log.message("Search Button Clicked Sucessfully");
		String description = PropertyReader.getProperty("Punch_Subject");
		SkySiteUtils.waitTill(5000);
		if (Anything.trim().contentEquals(description.trim())) {
			Log.message("Maching Punch Found!!");
			SkySiteUtils.waitTill(2000);
			return true;
		} else {

			Log.message("Matching Punch Not Found!!");
           return false;
		}
		//return result;

		

	}
	
	
	public boolean Verify_SelectAn_Album(String Album_Name)throws InterruptedException, AWTException, IOException 
	{
		boolean result1 = false;
		
		SkySiteUtils.waitForElement(driver, btnAddNewAlbum, 20);
		Log.message("Add New Album button is available.");
		//Counting available Albums
		int Album_Count=0;
		int i = 1;
		List<WebElement> allElements = driver.findElements(By.xpath("//section[1]/i"));
		for (WebElement Element : allElements)
		{ 
			Album_Count = Album_Count+1; 	
		}
		Log.message("Available Albums in a gallery is: "+Album_Count);
		Album_Count=Album_Count+(Album_Count-1);
		Log.message("Album Count after added more is: "+Album_Count);
		//Getting Album Names and matching
		for(i = 1;i<=Album_Count;i++)
		{
			String Act_AlbumName = driver.findElement(By.xpath("//li["+i+"]/section[2]/h4")).getText();
			if(Act_AlbumName.contentEquals(Album_Name))
			{
				result1=true;
				break;
			}		
		}
		if(result1==true)
		{
			driver.findElement(By.xpath("//li["+i+"]/section[2]/h4")).click();
			SkySiteUtils.waitForElement(driver, btnUploadPhotos, 30);
			Log.message("Expected Album is selected successfully!!!");
			return true;
		}
		else
		{
			Log.message("Expected Album is not available!!!");
			return false;
		}
	}
	
	
	public boolean Select_Album(String Album_Name)throws InterruptedException, AWTException, IOException 
	{
		boolean result1 = false;
		
		SkySiteUtils.waitForElement(driver, btnAddNewAlbum, 20);
		Log.message("Add New Album button is available.");
		//Counting available Albums
		int Album_Count=0;
		int i = 1;
		List<WebElement> allElements = driver.findElements(By.xpath("//section[1]/i"));
		for (WebElement Element : allElements)
		{ 
			Album_Count = Album_Count+1; 	
		}
		Log.message("Available Albums in a gallery is: "+Album_Count);
		Album_Count=Album_Count+(Album_Count-1);
		Log.message("Album Count after added more is: "+Album_Count);
		//Getting Album Names and matching
		for(i = 1;i<=Album_Count;i++)
		{
			String Act_AlbumName = driver.findElement(By.xpath("//li["+i+"]/section[2]/h4")).getText();
			if(Act_AlbumName.contentEquals(Album_Name))
			{
				result1=true;
				break;
			}		
		}
		if(result1==true)
		{
			//driver.findElement(By.xpath("//li["+i+"]/section[2]/h4")).click();
			SkySiteUtils.waitForElement(driver, btnUploadPhotos, 30);
			Log.message("Expected Album is selected successfully!!!");
			return true;
		}
		else
		{
			Log.message("Expected Album is not available!!!");
			return false;
		}
	}
	
	
	
	public boolean FilterSearch_Photoscenario(String Commonvalue, String Flag) throws Throwable {
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		if (Flag.equalsIgnoreCase("Search_Building")) {
			SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
			AdvanceSearch_Link.click();
			Log.message("Advance Search Option Clicked Sucessfully");
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, Filter_Building, 120);
			Filter_Building.sendKeys(Commonvalue);
			Log.message("Entered the Value In Building field:-" + Commonvalue);
			SkySiteUtils.waitTill(30000);
			// SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
			// SkySiteUtils.waitTill(5000);
			SearchButton_Advance.click();
			Log.message("Search Button Clicked Sucessfully");
			SkySiteUtils.waitTill(15000);
			SkySiteUtils.waitForElement(driver, PhotoMore_Option, 120);
			PhotoMore_Option.click();
			Log.message("Photo More Option Clicked  Sucessfully");
			SkySiteUtils.waitForElement(driver, PhotoMore_edit, 120);
			SkySiteUtils.waitTill(5000);
			PhotoMore_edit.click();
			SkySiteUtils.waitTill(5000);
			Log.message("Edit Attribute Option Clicked  Sucessfully");
			String building = BuildingText.getAttribute("value");
			SkySiteUtils.waitTill(5000);
			// String Building = PropertyReader.getProperty("Building");
			if (Commonvalue.contains(building.trim())) {
				Log.message("Building Verified Sucessfully");
				return true;

			} else {
				Log.message("Building Not Verified Sucessfully");
				return false;
			}

		} else if (Flag.equalsIgnoreCase("Search_level")) {

			SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
			AdvanceSearch_Link.click();
			Log.message("Advance Search Option Clicked Sucessfully");
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, Filter_level, 120);
			Filter_level.sendKeys(Commonvalue);
			Log.message("Entered the Value In level field:-" + Commonvalue);
			SkySiteUtils.waitTill(50000);
			// SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
			// SkySiteUtils.waitTill(5000);
			SearchButton_Advance.click();
			Log.message("Search Button Clicked Sucessfully");

			SkySiteUtils.waitTill(15000);
			SkySiteUtils.waitForElement(driver, PhotoMore_Option, 120);
			PhotoMore_Option.click();
			Log.message("Photo More Option Clicked  Sucessfully");
			SkySiteUtils.waitForElement(driver, PhotoMore_edit, 120);
			SkySiteUtils.waitTill(5000);
			PhotoMore_edit.click();
			SkySiteUtils.waitTill(8000);
			Log.message("Edit Attribute Option Clicked  Sucessfully");
			String level = levelText.getAttribute("value");
			Log.message("level Name:--"+level);
			if (Commonvalue.contains(level.trim())) {
				Log.message("level Verified Sucessfully");
				return true;
			} else {
				Log.message("Level Not Verified Sucessfully");
				return false;
			}

		}

		else if (Flag.equalsIgnoreCase("Search_Room")) {
			SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
			AdvanceSearch_Link.click();
			Log.message("Advance Search Option Clicked Sucessfully");
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, Filter_level, 120);
			Filter_Room.sendKeys(Commonvalue);
			Log.message("Entered the Value In Room field:-" + Commonvalue);
			SkySiteUtils.waitTill(30000);
			// SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
			// SkySiteUtils.waitTill(5000);
			SearchButton_Advance.click();
			Log.message("Search Button Clicked Sucessfully");
			SkySiteUtils.waitTill(15000);
			SkySiteUtils.waitForElement(driver, PhotoMore_Option, 120);
			PhotoMore_Option.click();
			Log.message("Photo More Option Clicked  Sucessfully");
			SkySiteUtils.waitForElement(driver, PhotoMore_edit, 120);
			SkySiteUtils.waitTill(5000);
			PhotoMore_edit.click();
			SkySiteUtils.waitTill(5000);
			Log.message("Edit Attribute Option Clicked  Sucessfully");
			String room = RoomText.getAttribute("value");
			SkySiteUtils.waitTill(5000);
			if (Commonvalue.contains(room.trim())) {
				Log.message("room Verified Sucessfully");
				return true;
			} else {
				Log.message("room Not Verified Sucessfully");
				return false;
			}
		}

		else if (Flag.equalsIgnoreCase("Search_Area")) {
			SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
			AdvanceSearch_Link.click();
			Log.message("Advance Search Option Clicked Sucessfully");
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, Filter_level, 120);
			Filter_Area.sendKeys(Commonvalue);
			Log.message("Entered the Value In Room field:-" + Commonvalue);
			SkySiteUtils.waitTill(50000);
			// SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
			// SkySiteUtils.waitTill(5000);
			SearchButton_Advance.click();
			Log.message("Search Button Clicked Sucessfully");
			SkySiteUtils.waitTill(15000);
			SkySiteUtils.waitForElement(driver, PhotoMore_Option, 120);
			PhotoMore_Option.click();
			SkySiteUtils.waitTill(5000);
			Log.message("Photo More Option Clicked  Sucessfully");
			SkySiteUtils.waitForElement(driver, PhotoMore_edit, 120);
			
			PhotoMore_edit.click();
			SkySiteUtils.waitTill(5000);
			Log.message("Edit Attribute Option Clicked  Sucessfully");
			String Area = AreaText.getAttribute("value");
			SkySiteUtils.waitTill(5000);
			if (Commonvalue.contains(Area.trim())) {
				Log.message("Area Verified Sucessfully");
				return true;

			} else {
				Log.message("Area Not Verified Sucessfully");
				return false;

			}
		}
		else if (Flag.equalsIgnoreCase("Search_Description")) {
			SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
			AdvanceSearch_Link.click();
			Log.message("Advance Search Option Clicked Sucessfully");
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, Filter_level, 120);
			Filter_Description_n.sendKeys(Commonvalue);
			Log.message("Entered the Value In Room field:-" + Commonvalue);
			SkySiteUtils.waitTill(50000);
			// SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
			// SkySiteUtils.waitTill(5000);
			SearchButton_Advance.click();
			Log.message("Search Button Clicked Sucessfully");
			SkySiteUtils.waitTill(15000);
			SkySiteUtils.waitForElement(driver, PhotoMore_Option, 120);
			PhotoMore_Option.click();
			Log.message("Photo More Option Clicked  Sucessfully");
			SkySiteUtils.waitForElement(driver, PhotoMore_edit, 120);
			SkySiteUtils.waitTill(5000);
			PhotoMore_edit.click();
			SkySiteUtils.waitTill(5000);
			Log.message("Edit Attribute Option Clicked  Sucessfully");
			String Desc = Description_text.getAttribute("value");
			SkySiteUtils.waitTill(5000);
			if (Commonvalue.contains(Desc.trim())) {
				Log.message("Description Verified Sucessfully");
				return true;
			} else {
				Log.message("Description Not Verified Sucessfully");
				return false;
			}
		}
		
		else if (Flag.equalsIgnoreCase("Search_Date")){			
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, gridview, 120);
			gridview.click();
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, Photo_text_Date, 120);
			String date = Photo_text_Date.getText();
			String textStr[] = date.split("\\s", 10);
			String myDate = textStr[4] + " " + textStr[5] + " " + textStr[6];
			int myDate1 = Integer.parseInt(textStr[4]);
			
			Log.message(myDate);
			
			SkySiteUtils.waitForElement(driver, gridview, 120);
			gridview.click();
			
			SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
			AdvanceSearch_Link.click();
			Log.message("Advance Search Option Clicked Sucessfully");
			
			SkySiteUtils.waitForElement(driver, Date_Link, 120);
			Date_Link.click();
			
			String dateLoc = String.format("//td[text()='%d' and @class='day']",myDate1);
			Log.message("The locator is:"+dateLoc);
			driver.findElement(By.xpath(dateLoc)).click();
			
			SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
			SkySiteUtils.waitTill(5000);
			SearchButton_Advance.click();
			/*if (Commonvalue.contains(Desc.trim())) {
				Log.message("Description Verified Sucessfully");
				return true;
			} else {
				Log.message("Description Not Verified Sucessfully");
				return false;
			}*/
		}
		return result;
		
	}
	
	
	public boolean FilterSearch_Date(String Flag) throws Throwable {
		boolean result = false;	
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, photo, 120);
	photo.click();
	Log.message("Photo Image Clicked Sucessfully");	
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, Photo_text_Date, 120);
	String date = Photo_text_Date.getText();
	SkySiteUtils.waitTill(5000);
	Log.message("Date Text:---"+date);	
	String textStr[] = date.split("\\s", 10);
	String myDate1 = textStr[0];	
	String myDate = textStr[0] + " " + textStr[1] + " " + textStr[2];
	Log.message("After Truncate Date Text:---"+myDate);	
	Log.message(myDate);
	SkySiteUtils.waitForElement(driver, Done_Button, 120);
	Done_Button.click();
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
	AdvanceSearch_Link.click();
	Log.message("Advance Search Option Clicked Sucessfully");	
	SkySiteUtils.waitForElement(driver, Date_Link, 120);
	Date_Link.click();
	SkySiteUtils.waitTill(5000);
	String dateLoc = String.format("//td[text()='%d' and @class='day']",myDate1);
	SkySiteUtils.waitTill(5000);
	Log.message("The locator is:"+dateLoc);
	driver.findElement(By.xpath(dateLoc)).click();
	SkySiteUtils.waitTill(5000);	
	SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	SkySiteUtils.waitTill(5000);
	SearchButton_Advance.click();
	if (myDate.contains(myDate.trim())) {
		Log.message("Date Verified Sucessfully");
		return true;
	} else {
		Log.message("Date Not Verified Sucessfully");
		return false;
	}
	//return result;
	}
	
	
	public boolean Searchscenario_olddata(String Anything) throws Throwable {
		boolean result = false;
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		Search_GlobalButton.click();
		Log.message("Local Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Everywhere_AllContents, 120);
		search.click();
		Log.message("Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		TxtSearch_Keyword.clear();
		SkySiteUtils.waitTill(1000);
		TxtSearch_Keyword.sendKeys(Anything);	
		SkySiteUtils.waitTill(50000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");		
		SkySiteUtils.waitTill(5000);
		
		return result;
		
	   }

	public boolean Searchscenario(String Anything) throws Throwable {
		boolean result = false;
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		Search_GlobalButton.click();
		Log.message("Local Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Everywhere_AllContents, 120);
		search.click();
		Log.message("Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		TxtSearch_Keyword.clear();
		SkySiteUtils.waitTill(1000);
		TxtSearch_Keyword.sendKeys(Anything);
		//SkySiteUtils.waitForElement(driver, Search_Button,120);
		SkySiteUtils.waitTill(10000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		//SkySiteUtils.waitTill(5000);
		//Search_Button.click();
		SkySiteUtils.waitTill(5000);
		/*SkySiteUtils.waitTill(5000);
		String SubmittalNumber = SubmittalN0.getText();
		Log.message("Submittal Number is:" + SubmittalN0);
		
		if (SubmittalNumber.trim().contentEquals(Anything.trim())) {
			Log.message("Maching Submittal Number Found!!");
            // return true;
		} else {

			Log.message("Matching Submittal Number Not Found!!");
            // return false;
		}*/
		return result;
		
	   }
	
	
	public boolean Searchscenario_validation() throws Throwable {
		boolean result = false;
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		Search_GlobalButton.click();
		Log.message("Local Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Everywhere_AllContents, 120);
		search.click();
		Log.message("Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		TxtSearch_Keyword.clear();
		SkySiteUtils.waitTill(1000);
		String photo= PropertyReader.getProperty("photoName1");			
		TxtSearch_Keyword.sendKeys(photo);
		//SkySiteUtils.waitForElement(driver, Search_Button,120);
		SkySiteUtils.waitTill(10000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		//SkySiteUtils.waitTill(5000);
		//Search_Button.click();
		SkySiteUtils.waitTill(5000);
	    SkySiteUtils.waitTill(5000);
		String Phototext = PhotoText.getText();
		Log.message("Photo Name :" + SubmittalN0);
		
		if (Phototext.contains(photo.trim())) {
			Log.message("Maching Photo Found!!");
            return true;
		} else {

			Log.message("Maching Photo Not  Found!!");
            return false;
		}
	
		
	   }
	
	public boolean global_PhotoSearchscenario(String Anything) throws Throwable {
		boolean result = false;
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		Search_GlobalButton.click();
		Log.message("Local Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Everywhere_AllContents, 120);
		Everywhere_AllContents.click();
		Log.message("Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		TxtSearch_Keyword.clear();
		SkySiteUtils.waitTill(1000);
		TxtSearch_Keyword.sendKeys(Anything);
		//SkySiteUtils.waitForElement(driver, Search_Button,120);
		SkySiteUtils.waitTill(10000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");		
		SkySiteUtils.waitTill(5000);		
		String Photoalbum = Global_Album.getText();
		Log.message("Album Name is:" + Photoalbum);
		
		if (Photoalbum.trim().contentEquals(Anything.trim())) {
			Log.message("Maching Album name Found!!");
            return true;
		} else {

			Log.message("Matching Album Name Not Found!!");
            return false;
		}
		
		
	   }
	
	
	
	
	public boolean Searchscenario1(String Anything) throws Throwable {
		boolean result = false;
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		Search_GlobalButton.click();
		Log.message("Local Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Everywhere_AllContents, 120);
		search.click();
		Log.message("Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		TxtSearch_Keyword.clear();
		SkySiteUtils.waitTill(1000);
		TxtSearch_Keyword.sendKeys(Anything);
		//SkySiteUtils.waitForElement(driver, Search_Button,120);
		SkySiteUtils.waitTill(50000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		Search_Button.click();
		
		SkySiteUtils.waitTill(5000);
		String SubmittalNumber = SubmittalN0.getText();
		Log.message("Submittal Number is:" + SubmittalN0);
		
		if (SubmittalNumber.trim().contentEquals(Anything.trim())) {
			Log.message("Maching Submittal Number Found!!");
            return true;
		} else {

			Log.message("Matching Submittal Number Not Found!!");
             return false;
		}
		
		
	   }
	
	
	public boolean Searchscenario_submittal_ModuleSearch(String Flag) throws Throwable {
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		if(Flag.equalsIgnoreCase("Submittal_Number_search")){
		String SubmittalNumberbefore = SubmittalN0.getText();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		Search_GlobalButton.click();
		Log.message("Local Search Option Click Sucessfully");
		/*SkySiteUtils.waitForElement(driver, SubmittalLink, 120);		
		JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", SubmittalLink);
		*/
		//SubmittalLink.click();
		SkySiteUtils.waitTill(1000);
		Log.message("Submittal Link Click Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		TxtSearch_Keyword.clear();
		SkySiteUtils.waitTill(1000);
		TxtSearch_Keyword.sendKeys(SubmittalNumberbefore);
		SkySiteUtils.waitTill(30000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");		
		SkySiteUtils.waitTill(5000);
		String SubmittalNumberafter = SubmittalN0.getText();
		Log.message("Submittal Number is:" + SubmittalN0);
		
		if (SubmittalNumberbefore.trim().contains(SubmittalNumberafter.trim())) {
			Log.message("Maching Submittal Number Found!!");
            return true;
		} else {

			Log.message("Matching Submittal Number Not Found!!");
             return false;
		}
		}
		
		if(Flag.equalsIgnoreCase("Search_Submittal_Name")){
			SkySiteUtils.waitTill(5000);
			String SubmittalNamebefore = SubmittalName_t.getText();
			SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
			Search_GlobalButton.click();
			Log.message("Local Search Option Click Sucessfully");
			//SkySiteUtils.waitForElement(driver, SubmittalLink, 120);
			//SubmittalLink.click();
			Log.message("Submittal Link Click Sucessfully");
			SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
			TxtSearch_Keyword.clear();
			SkySiteUtils.waitTill(1000);
			TxtSearch_Keyword.sendKeys(SubmittalNamebefore);
			SkySiteUtils.waitTill(30000);
			Search_Button.click();
			Log.message("Search Button Clicked Sucessfully");		
			SkySiteUtils.waitTill(5000);
			String SubmittalNameafter = SubmittalName_t.getText();
			Log.message("Submittal Name is:====" + SubmittalName);
			
			if (SubmittalNamebefore.trim().contentEquals(SubmittalNameafter.trim())) {
				Log.message("Maching Submittal Name Found!!");
	            return true;
			} else {

				Log.message("Matching Submittal Name Not Found!!");
	             return false;
			}
			}
		
		if(Flag.equalsIgnoreCase("Search_Status")){
			SkySiteUtils.waitTill(5000);
			String Statusbefore = SubmittalStatus.getText();
			SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
			Search_GlobalButton.click();
			Log.message("Local Search Option Click Sucessfully");
			//SkySiteUtils.waitForElement(driver, SubmittalLink, 120);
			//SubmittalLink.click();
			//Log.message("Submittal Link Click Sucessfully");
			SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
			TxtSearch_Keyword.clear();
			SkySiteUtils.waitTill(1000);
			TxtSearch_Keyword.sendKeys(Statusbefore);
			SkySiteUtils.waitTill(30000);
			Search_Button.click();
			Log.message("Search Button Clicked Sucessfully");		
			SkySiteUtils.waitTill(5000);
			String Statusafter = SubmittalStatus.getText();
			Log.message("Status  is:====" + Statusafter);
			
			if (Statusbefore.trim().contentEquals(Statusafter.trim())) {
				Log.message("Maching Status Found!!");
	            return true;
			} else {

				Log.message("Matching STatus Not Found!!");
	             return false;
			}
			}
		return result;
		
	   }
	
	
	
	
	
	
	public boolean SubmittalStatus_Validation(String Anything) throws Throwable {
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		String Status = submOpenStatusUnderList.getText();
		Log.message("Status is:" + Status);
		if (Status.trim().contentEquals(Anything.trim())) {
			Log.message("Maching Status Found!!");

		} else {

			Log.message("Matching Status Not Found!!");

		}
		return result;

	}


	public boolean Searchprojectvalidation(String Anything) throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		String Exp_ProjName = Staticproject.getText();
		Log.message("Project name is:" + Exp_ProjName);
		if (Exp_ProjName.trim().contentEquals(Anything.trim())) {
			Log.message("Maching Project Found!!");
			SkySiteUtils.waitTill(1000);
			return true;
		} else {
			Log.message("Matching project Not Found!!");
			return false;
		}
		
		
		
		

	}

	public boolean SearchFoldervalidation(String Anything) throws Throwable

	{
		boolean result = false;

		SkySiteUtils.waitTill(5000);

		String Exp_folderName = folder_Number.getText();
		Log.message("Folder name is:" + Exp_folderName);
		SkySiteUtils.waitTill(5000);
		if (Exp_folderName.trim().contentEquals(Anything.trim())) {
			Log.message("Maching folder Found!!");
			SkySiteUtils.waitTill(3000);
			return true;
		} else {

			Log.message("Matching Folder Not Found!!");
			return false;
		}
		

	}

	public boolean Searchfolderphotovalidation() throws Throwable

	{
		boolean result = true;

		SkySiteUtils.waitTill(5000);
		String FileName1 = Photo_Olddata.getText();
		Log.message("File name is1:" + FileName1);
		//Log.message("File name is2:" + Anything);
		
		String Photo_Name1 = PropertyReader.getProperty("Photo_Name");
		if (FileName1.contains(Photo_Name1.trim())) {
			Log.message("Matching photo Folder  Found!!");
			SkySiteUtils.waitTill(1000);
       return true;
		} else {

			Log.message("Matching photo Folder  Not Found!!");
			return false;
		}
		
	    }
	
	
	public boolean Searchfolderphotovalidation_1(String Anything) throws Throwable

	{
		boolean result = true;
		SkySiteUtils.waitTill(5000);
		String FileName1 = Photo.getText();
		Log.message("File name is1:" + FileName1);
		Log.message("File name is2:" + Anything);
		
		//String Photo_Name1 = PropertyReader.getProperty("Photo_Name");
		if (FileName1.contains(Anything.trim())) {
			Log.message("Matching photo Folder  Found!!");
			SkySiteUtils.waitTill(1000);
       return true;
		} else {

			Log.message("Matching photo Folder  Not Found!!");
			return false;
		}
		
	    }

	public boolean SearchFilevalidation(String Anything) throws Throwable

	{
		boolean result = false;

		SkySiteUtils.waitTill(5000);

		String FileName1 = FileName.getText();
		Log.message("File name is:" + FileName1);
		SkySiteUtils.waitTill(5000);
		if (FileName1.contentEquals(Anything.trim())) {
			Log.message("Maching File Found!!");
			SkySiteUtils.waitTill(5000);
            return true;
		} else {

			Log.message("Matching File Not Found!!");
			return false;
		}
		
	    }

	public boolean AdvanceSearch_building(String building) throws Throwable {
		boolean result = false;
		Log.message("Enter Into  filter By search status:---"+ building);
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		AdvanceSearch_Link.click();
		Log.message("Advance Search Option Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, Building, 60);
		SkySiteUtils.waitTill(5000);
		Building.sendKeys(building);
		Log.message("Entered The Value In search Inbox Text Field: - " + building);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 60);
		SkySiteUtils.waitTill(5000);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");		
		SkySiteUtils.waitTill(35000);
		return result;

	}
	
	
	
	public boolean SearchFilevalidation_building(String Anything) throws Throwable

	{
		boolean result = false;

		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, PhotoThree_dot, 60);
		PhotoThree_dot.click();
		SkySiteUtils.waitTill(8000);
		EditAttribute.click();
		SkySiteUtils.waitTill(8000);
		String Building = Building_Value.getAttribute("value");
		Log.message("File name is:" + Building);
		SkySiteUtils.waitTill(8000);

		if (Building.contains(Anything)) {
			Log.message("Maching File value  Found!!");
			SkySiteUtils.waitTill(5000);
            return true;
		} else {

			Log.message("Matching File  valueNot Found!!");
			return false;
		}
		
	    }
	
	
	
	
	
	
	public boolean AdvanceSearch_Status(String SubmittalName) throws Throwable {
		boolean result = false;
		Log.message("Enter Into  filter By search status");
		SkySiteUtils.waitTill(500);
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		AdvanceSearch_Link.click();
		Log.message("Advance Search Option Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, Submital_NumberInbox_filter, 60);
		SkySiteUtils.waitTill(3000);
		Submital_NameInbox_filter.sendKeys(SubmittalName);
		Log.message("Entered The Value In search Inbox Text Field: - " + SubmittalName);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 60);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		return result;

	}
	
	
	public boolean AdvanceSearch_SubmittalType() throws Throwable {
		boolean result = false;
		Log.message("Enter Into  filter By search status");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		AdvanceSearch_Link.click();
		Log.message("Advance Search Option Clicked Sucessfully");		
		Select Dropdown = new Select(driver.findElement(By.xpath("//*[@id='ddlSubmittalType_filter']")));
		Dropdown.selectByVisibleText("LEED");	
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 60);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		return result;

	}
	
	
	
	/*public static void ytyut(WebElement element, String Value){
		ele.sendKeys("hkjhjfd");
		
	}*/
	
	       
	
	public boolean Contact_FirstName() throws Throwable {
		boolean result = false;
		SkySiteUtils.waitForElement(driver, Contacts, 60);
		Contacts.click();
		/*String Contact_FirstName = PropertyReader.getProperty("Contact_FirstName");
		
		String Cont_FirstName = Name.getText();
		String[] parts = Cont_FirstName.split(" ");
		String part1 = parts[0]; // 004
		
		if (part1.trim().contentEquals(Contact_FirstName.trim())) {
			Log.message("First Name Verified Sucessfully");	

		} else {
			Log.message("First Name Not Verified Sucessfully");

		}
		*/
		return result;
	
	
	}
	        
	        public boolean Sendfile_Method() throws Throwable {
	    		boolean result = false;
	    		SkySiteUtils.waitForElement(driver, DragAnddropLocation, 60);
	    		Contacts.click();
	    		String Contact_FirstName = PropertyReader.getProperty("Contact_LastName");
	    		
	    		String Cont_FirstName = RFI_Number.getText();
	    		String[] parts = Cont_FirstName.split(" ");
	    		String part2 = parts[1]; // 004
	    		
	    		if (part2.trim().contentEquals(Contact_FirstName.trim())) {
	    			Log.message("First Name Verified Sucessfully");	

	    		} else {
	    			Log.message("First Name Not Verified Sucessfully");

	    		}
	    		
	    		return result;
	    	
	    	
	    	}
	        
	        
	        
	        
	        public boolean AdvanceSearch_Inbox(String CommonValue,String Flag) throws Throwable 
	         {  		
	        	boolean result = false;
	    		SkySiteUtils.waitTill(2000);
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
	    		AdvanceSearch_Link.click();
	    		Log.message("Advance Search Option Clicked Sucessfully");	    		
	    		SkySiteUtils.waitTill(5000);
	    		if(Flag.equalsIgnoreCase("Inbox_ExactOrder")){
	    		SkySiteUtils.waitTill(5000);
	    		SkySiteUtils.waitForElement(driver, Filter_Exactorder, 120);
	    		Filter_Exactorder.sendKeys(CommonValue);
	    		Log.message("Entered the Value In Exact Order field:-"+CommonValue);	
	    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SkySiteUtils.waitTill(5000);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");
	    		
	    		String ProjectName = Staticproject.getText();	    		
	    		String proName = PropertyReader.getProperty("ProjectName");
	    		if (proName.contentEquals(ProjectName.trim())) {
	    			Log.message("Project Name Verified Sucessfully");	

	    		} else {
	    			Log.message("Project name Not Verified Sucessfully");

	    		}
	    		
	    		}	    				
	    		else if(Flag.equalsIgnoreCase("Search_ProjectNumber")){
	    		SkySiteUtils.waitForElement(driver, Filter_ProjectNumber,120);
	    		Filter_ProjectNumber.sendKeys(CommonValue);
	    		SkySiteUtils.waitTill(5000);
	    		Log.message("Entered the Value In Project Number field:-"+CommonValue);
	    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");		
	    		
	    		String ProjectNo = Project_NumberText.getText();	    		
	    		String[] parts = ProjectNo.split("\\s+");	    		
	    		String part1 = parts[0]; 
	    		Log.message("Project Number"+part1);	
	    		SkySiteUtils.waitTill(5000);
	    		if (part1.trim().contains(CommonValue.trim())) {
	    			Log.message("Project Number Verified Sucessfully");	

	    		} else {
	    			Log.message("Project Number Not Verified Sucessfully");

	    		}
	    		}
	    		
	    		
	    		
				return result;
	        
	        }
	        
	
	        public boolean AdvanceSearch_Sendfile_Inbox(String Flag) throws Throwable {
	    		boolean result = false;	    		
	    		
	    		if(Flag.equalsIgnoreCase("Search_exactorder")){
	    		SkySiteUtils.waitTill(5000);
		    	SkySiteUtils.waitForElement(driver, sendfilelink, 120);		
		    	sendfilelink.click();
	    		SkySiteUtils.waitForElement(driver, Filter_exactorderText, 120);
		    	String exactorder = Filter_exactorderText.getText();
		    	SkySiteUtils.waitTill(3000);
		    	String[] parts = exactorder.split("\\s+");	    		
		    	String Part1 = parts[1]; 
		    	
		    	//SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		    	SkySiteUtils.waitTill(30000);
		    	AdvanceSearch_Link.click();
		    	Log.message("Advance Search Option Clicked Sucessfully");	    		
		    	SkySiteUtils.waitTill(5000);	    		
	    		SkySiteUtils.waitForElement(driver, Filter_Exactorder, 120);
	    		SkySiteUtils.waitTill(5000);
	    		Filter_Exactorder.sendKeys(Part1);	    		
	    		Log.message("Entered the Value In exact order field:-"+Part1);	
	    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SkySiteUtils.waitTill(30000);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");	    			    		
	    		SkySiteUtils.waitTill(5000);
	    		String exactorder1 = Filter_exactorderText.getText();
	    		String[] parts1 = exactorder.split("\\s+");	    		
	    		String Part2 = parts[1];   		
	    		SkySiteUtils.waitTill(5000);
	    		if (Part1.contentEquals(Part2.trim())) {
	    			Log.message("Exact Order Verified Sucessfully");
	    			SkySiteUtils.waitTill(1000);
                     return true;
	    		} else {
	    			Log.message("Exact Order Not Verified Sucessfully");
	    			 return false;
	    		}
	    		
	    		}	    				
	    		else if(Flag.equalsIgnoreCase("Search_date"))	    			
	    		{   	    			
	    			SkySiteUtils.waitForElement(driver, sendfilelink, 120);		
			    	sendfilelink.click();
	    			SkySiteUtils.waitTill(5000);	    			
	    			SkySiteUtils.waitForElement(driver, inbox_DateText, 120);
	    			String date = inbox_DateText.getText();
		    		String[] a1 = date.split("\\s+");	    		
		    		String Part3 = a1 [1]+" "+a1[2]+" "+a1[3];
		    		Log.message("before comparision:----"+Part3);		    	
		    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		    		SkySiteUtils.waitTill(10000);
			    	AdvanceSearch_Link.click();
			    	Log.message("Advance Search Option Clicked Sucessfully");	    		
			    	SkySiteUtils.waitTill(5000);			    	
			    	int myDate1 = Integer.parseInt(a1 [1]);					
					Log.message("date:== "+myDate1);
					SkySiteUtils.waitForElement(driver, Date_Link, 120);
					Date_Link.click();
					Log.message("Date POPUP Clicked Sucessfully");
					SkySiteUtils.waitTill(5000);
					/*Calendar cal = Calendar.getInstance();
					int currentDate = cal.get(Calendar.DATE);
					cal.add(Calendar.DATE, 0);
					int selectDate = cal.get(Calendar.DATE);*/					
					String dateLoc = String.format("//td[text()='%d' and @class='day active today']",myDate1);
					Log.message("The locator is:"+dateLoc);
					SkySiteUtils.waitTill(15000);					
					driver.findElement(By.xpath(dateLoc)).click();
					//JavascriptExecutor executor = (JavascriptExecutor)driver;
		    	   //executor.executeScript("arguments[0].click();", dateLoc);	
		    	    //SkySiteUtils.waitTill(15000);
		    	    Log.message("Paticular Date Clicked Sucessfully");
		    	   //String dateInbox =driver.findElement(By.xpath(".//*[@id='txtOrderDate_filter']")).getAttribute("value");		    	    
		    	  // Log.message("Date Selected :=="+dateInbox);
					SkySiteUtils.waitTill(30000);
					SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);		
					SkySiteUtils.waitTill(1000);
					SearchButton_Advance.click();					
					SkySiteUtils.waitTill(5000);   			
	    			
	    		    Log.message("Search Button Clicked Sucessfully");
	    		    
	    		    //String value = return (String) ((JavascriptExecutor) this.webDriver).executeScript("angular.element($('.txtContractDueDate_filter')).text()");;
	    		    
	    		   // String value =  (String)((JavascriptExecutor) driver).executeScript("angular.element($('.txtContractDueDate_filter')).text()");
	    		   // Log.message("Search Button Clicked Sucessfully:---"+value);
	    		    
	    		    if (Part3.trim().contains(Part3.trim())) {
		    			Log.message("Date Verified Sucessfully");	
	                     return true;
		    		} else {
		    			Log.message("Date Not Verified Sucessfully");
		    			return false;
		    		}
	    		  
	    	            }
	    		
	    		else if(Flag.equalsIgnoreCase("Search_Subject")){
	    			SkySiteUtils.waitTill(5000);
		    		SkySiteUtils.waitForElement(driver, sendfilelink, 120);		
		    		sendfilelink.click();
	    			SkySiteUtils.waitTill(5000);
	    			SkySiteUtils.waitForElement(driver, Filter_subjectText, 120);	    			
	    			String sub = Filter_subjectText.getText();
	    			String[] value_split = sub.split("\\|");    		
	    			String Part = value_split[1]; 
	    			//String Part1 = value_split[2];
		    		Log.message("before comparision:-"+ Part);
		    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		    		SkySiteUtils.waitTill(5000);
			    	AdvanceSearch_Link.click();
			    	Log.message("Advance Search Option Clicked Sucessfully");
			    	SkySiteUtils.waitTill(5000);		
		    		SkySiteUtils.waitForElement(driver, Filter_Exactorder, 120);
		    		Filter_Inboxsubject.sendKeys(Part);
		    		SkySiteUtils.waitTill(5000);
		    		Log.message("Entered the Value In subject field:-"+Part);	
		    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		    		SkySiteUtils.waitTill(50000);
		    		SearchButton_Advance.click();
		    		Log.message("Search Button Clicked Sucessfully");	    			    		
		    		SkySiteUtils.waitTill(5000);
		    		String sub1 = Filter_subjectText.getText();
	    			String[] value_split1 = sub1.split("\\|");    		
	    			String Part2 = value_split1[1]; 
	    			//String Part1 = value_split[2];
		    		Log.message("before comparision:-"+ Part2);  		
		    		
		    		if (Part.contains(Part2)) {
		    			Log.message("subject Verified Sucessfully");
		    			 return true;

		    		} else {
		    			Log.message("Subject  Not Verified Sucessfully");
		    			 return false;

		    		}
	    			
	    		
	    		
	    		}
	    		
	    		
	    		
	    		else if(Flag.equalsIgnoreCase("Search_senderEmail")){
	    			SkySiteUtils.waitTill(5000);
		    		SkySiteUtils.waitForElement(driver, sendfilelink, 120);		
		    		sendfilelink.click();
	    			SkySiteUtils.waitTill(8000);
	    			SkySiteUtils.waitForElement(driver, Filter_SenderEmailText, 120);
	    			String email = Filter_SenderEmailText.getText();
	    			String[] value_split = email.split("\\s+");    		
	    			String Part = value_split[1]; 
	    			//String Part1 = value_split[2];
		    		Log.message("before comparision:-"+ Part);
		    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
			    	AdvanceSearch_Link.click();
			    	Log.message("Advance Search Option Clicked Sucessfully");	    		
			    	SkySiteUtils.waitTill(5000);		    		
		    		SkySiteUtils.waitForElement(driver, Filter_Exactorder, 120);
		    		Filter_InboxSendermail.sendKeys(Part);
		    		SkySiteUtils.waitTill(5000);
		    		Log.message("Entered the Value In subject field:-"+Part);	
		    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		    		
		    		SearchButton_Advance.click();
		    		Log.message("Search Button Clicked Sucessfully");	    			    		
		    		SkySiteUtils.waitTill(50000);
		    		String email1 = Filter_SenderEmailText.getText();
	    			String[] value_split1 = email1.split("\\s+");    		
	    			String Part2 = value_split1[1]; 
	    			SkySiteUtils.waitTill(5000);
		    		Log.message("after comparision:-"+ Part2);  		
		    		
		    		if (Part.contentEquals(Part2.trim())) {
		    			Log.message("sender Email Verified Sucessfully");
		    			 return true;

		    		} else {
		    			Log.message("Sender email  Not Verified Sucessfully");
		    			 return false;

		    		}	
	    		
	    		
	    		}
				return result;
				}
	        
	        
	        
	        
	        
	        
	     public boolean AdvanceSearch_Sendfile_Date(String Flag) throws Throwable {
	         boolean result = false;    
	        
	        SkySiteUtils.waitForElement(driver, sendfilelink, 120);		
	    	sendfilelink.click();
			SkySiteUtils.waitTill(5000);	    			
			SkySiteUtils.waitForElement(driver, inbox_DateText, 120);
			String date = inbox_DateText.getText();
    		String[] a1 = date.split("\\s+");	    		
    		String Part3 = a1 [1]+" "+a1[2]+" "+a1[3];
    		Log.message("before comparision:----"+Part3);		    	
    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
    		SkySiteUtils.waitTill(10000);
	    	AdvanceSearch_Link.click();
	    	Log.message("Advance Search Option Clicked Sucessfully");	    		
	    	SkySiteUtils.waitTill(5000);			    	
	    	int myDate1 = Integer.parseInt(a1 [1]);					
			Log.message("date:== "+myDate1);
			SkySiteUtils.waitForElement(driver, Date_Link, 120);
			Date_Link.click();
			Log.message("Date POPUP Clicked Sucessfully");
			SkySiteUtils.waitTill(5000);
			/*Calendar cal = Calendar.getInstance();
			int currentDate = cal.get(Calendar.DATE);
			cal.add(Calendar.DATE, 0);
			int selectDate = cal.get(Calendar.DATE);*/					
			String dateLoc = String.format("//td[text()='%d' and @class='day']",myDate1);
			Log.message("The locator is:"+dateLoc);
			SkySiteUtils.waitTill(15000);					
			driver.findElement(By.xpath(dateLoc)).click();
			//JavascriptExecutor executor = (JavascriptExecutor)driver;
    	   //executor.executeScript("arguments[0].click();", dateLoc);	
    	    //SkySiteUtils.waitTill(15000);
    	    Log.message("Paticular Date Clicked Sucessfully");
    	   //String dateInbox =driver.findElement(By.xpath(".//*[@id='txtOrderDate_filter']")).getAttribute("value");		    	    
    	  // Log.message("Date Selected :=="+dateInbox);
			SkySiteUtils.waitTill(30000);
			SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);		
			SkySiteUtils.waitTill(1000);
			SearchButton_Advance.click();			
		    Log.message("Search Button Clicked Sucessfully");
		    SkySiteUtils.waitTill(10000); 
		    String date1 = inbox_DateText.getText();
    		String[] a2 = date1.split("\\s+");	    		
    		String Part4 = a2 [1]+" "+a2[2]+" "+a2[3];
    		Log.message("before comparision:----"+Part4);	
		    
		    //String value = return (String) ((JavascriptExecutor) this.webDriver).executeScript("angular.element($('.txtContractDueDate_filter')).text()");;
		    
		   // String value =  (String)((JavascriptExecutor) driver).executeScript("angular.element($('.txtContractDueDate_filter')).text()");
		   // Log.message("Search Button Clicked Sucessfully:---"+value);
		    
		    if (Part4.trim().contains(Part3.trim())) {
    			Log.message("Date Verified Sucessfully");	
    			return result = true;
    		} else {
    			Log.message("Date Not Verified Sucessfully");
    			return result = false;
    		}
			 
	            
                }

	        
	        public boolean AdvanceSearch_Sendfile_Tracking(String Flag) throws Throwable {
	    		boolean result = false;	    		
	    		    			    		
	    		
	    		if(Flag.equalsIgnoreCase("Search_exactorder")){		    			
	    		SkySiteUtils.waitTill(5000);
		    	SkySiteUtils.waitForElement(driver, sendfilelink, 120);		
		    	sendfilelink.click();		    		
		    	SkySiteUtils.waitForElement(driver, TrackingTab, 120);
		    	SkySiteUtils.waitTill(5000);
		    	WebElement element2 = TrackingTab;
		    	JavascriptExecutor executor2 = (JavascriptExecutor)driver;
		    	executor2.executeScript("arguments[0].click();", element2);
		    		//TrackingTab.click();
		        Log.message("Tracking Tab Clicked Sucessfully");
	    		SkySiteUtils.waitTill(5000);
		    	String exactorder = Filter_track_ExactorderText.getText();
		    	Log.message("tran id :"+exactorder);
		    	SkySiteUtils.waitTill(5000);
		    	String[] parts = exactorder.split(" ");	
		    	String Part1 = parts[1]; 	    		
		    	SkySiteUtils.waitTill(2000);
		    	int Trackingid=Integer.parseInt(Part1);
		    	Log.message("Tracking id is:"+Trackingid);
		    	SkySiteUtils.waitTill(2000);
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		    	AdvanceSearch_Link.click();
		    	Log.message("Advance Search Option Clicked Sucessfully");	    		
		    	SkySiteUtils.waitTill(10000);
	    		SkySiteUtils.waitForElement(driver, Filter_Exactorder, 120);
	    		Filter_Exactorder.sendKeys(Part1);
	    		SkySiteUtils.waitTill(5000);
	    		Log.message("Entered the Value In exact order field:-"+Part1);	
	    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SkySiteUtils.waitTill(50000);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");	    			    		
	    		SkySiteUtils.waitTill(5000);
	    		String exactorder1 = Filter_track_ExactorderText.getText();
	    		Log.message("tran id :"+exactorder1);
	    		SkySiteUtils.waitTill(5000);
	    		String[] parts1 = exactorder1.split(" ");	
	    		String Part2 = parts1[1]; 	    		
	    		SkySiteUtils.waitTill(2000);
	    		int Trackingid1=Integer.parseInt(Part2);
	    		//Log.message("Tracking id1 is:"+Trackingid1);
	    		SkySiteUtils.waitTill(2000);
	    		if(Trackingid==Trackingid1)
	    		{
	    			Log.message("Exact Order Verified Sucessfully");
	    			return true;
	    		} 
	    		else 
	    		{
	    			Log.message("Exact Order Not Verified Sucessfully");
	    			return false;
	    		}
	    		
	    		}	    				
	    		else if(Flag.equalsIgnoreCase("Search_date"))
	    			
	    		{   
	    			SkySiteUtils.waitForElement(driver, TrackingTab, 120);
		    		SkySiteUtils.waitTill(5000);
		    		WebElement element2 = TrackingTab;
		    	    JavascriptExecutor executor2 = (JavascriptExecutor)driver;
		    	    executor2.executeScript("arguments[0].click();", element2);	    			
	    			SkySiteUtils.waitTill(5000);
	    			SkySiteUtils.waitForElement(driver, Filter_Track_DateText, 120);	    			
	    			String date = Filter_Track_DateText.getText();
	    			Log.message("Date:"+ date);
	    			String[] dates = date.split(" ");	
		    		String date1 = dates[1]+" "+ dates[2]+" "+ dates[3]; 	    		
		    		SkySiteUtils.waitTill(2000);
		    		//int TrackDate=Integer.parseInt(date1);
		    		Log.message("Tracking date is:"+date1);			    		
		    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
			    	AdvanceSearch_Link.click();
			    	Log.message("Advance Search Option Clicked Sucessfully");	    		
			    	SkySiteUtils.waitTill(10000);		    	
	    			driver.findElement(By.xpath(".//*[@id='divSearchFilter']/div[1]/div[2]/div/div/span/span")).click();
	    			Log.message("Date icon clicked");
	    			SkySiteUtils.waitTill(5000);	
	    			
	    			WebElement element1 = driver.findElement(By.xpath("(//td[@class='day active today'])[2]"));
		    	    JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		    	    executor1.executeScript("arguments[0].click();", element1);
		    	 	    	
	    			Log.message("Take today date.....");
	    			SkySiteUtils.waitTill(50000);
	    			
	    			WebElement element3 = SearchButton_Advance;
		    	    JavascriptExecutor executor3= (JavascriptExecutor)driver;
		    	    executor3.executeScript("arguments[0].click();", element3); 
		    	    
	    		   Log.message("Search Button Clicked Sucessfully");		
	    		   SkySiteUtils.waitTill(3000);
	    		   String[] dates2 = date.split(" ");	
		    		String Date2 = dates2 [1]+" "+dates2[2]+" "+ dates2[3]; 	    		
		    	 		//int TrackDate1=Integer.parseInt(Date2);
		    		Log.message("Tracking date2 is:"+Date2);		    			
	    		   SkySiteUtils.waitTill(5000);
	    		if (date1.contains(Date2))
				{
	    			Log.message("Date Verified Sucessfully");	
	    			return true;
	    		}
	    		else
	    		{
	    			Log.message("Date Not Verified Sucessfully");
	    			return false;
	    		}
	    		}
	    		
	    		else if(Flag.equalsIgnoreCase("Search_Subject")){
	    			SkySiteUtils.waitTill(8000);
	    			//SkySiteUtils.waitTill(5000);
		    		SkySiteUtils.waitForElement(driver, sendfilelink, 120);		
		    		sendfilelink.click();		    		
		    		SkySiteUtils.waitForElement(driver, TrackingTab, 120);
		    		SkySiteUtils.waitTill(5000);
		    		WebElement element2 = TrackingTab;
		    	    JavascriptExecutor executor2 = (JavascriptExecutor)driver;
		    	    executor2.executeScript("arguments[0].click();", element2);
		    		//TrackingTab.click();
		    		Log.message("Tracking Tab Clicked Sucessfully");
	    		    SkySiteUtils.waitTill(5000);
	    			String exactorder = Filter_track_ExactorderText.getText();
			    	Log.message("tran id :"+exactorder);
			    	SkySiteUtils.waitTill(5000);
			    	String[] parts = exactorder.split(" ");	
			    	String Part1 = parts[3]+" "+ parts[4]+" " + parts[5] +" "+ parts[6]; 	    		
			    	SkySiteUtils.waitTill(2000);
			    	//int Trackingid=Integer.parseInt(Part1);
			    	Log.message("Tracking id is:"+Part1);
			    	SkySiteUtils.waitTill(2000);
		    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
			    	AdvanceSearch_Link.click();
			    	Log.message("Advance Search Option Clicked Sucessfully");	    		
			    	SkySiteUtils.waitTill(5000);
		    		SkySiteUtils.waitForElement(driver, Filter_Exactorder, 120);
		    		Filter_Inboxsubject.sendKeys(Part1);
		    		SkySiteUtils.waitTill(5000);
		    		Log.message("Entered the Value In subject field:-"+Part1);	
		    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		    		SkySiteUtils.waitTill(50000);
		    		SearchButton_Advance.click();
		    		Log.message("Search Button Clicked Sucessfully");	    			    		
		    		SkySiteUtils.waitTill(5000);
		    		String exactorder1 = Filter_track_ExactorderText.getText();
			    	Log.message("tran id 1:"+exactorder1);
			    	SkySiteUtils.waitTill(5000);
			    	String[] parts1 = exactorder.split(" ");	
			    	String Part2 = parts1[3]+" "+ parts1[4]+" " + parts1[5] +" "+ parts1[6];  
	    			//String Part1 = value_split[2];
		    		Log.message("before comparision:-"+ Part2);  		
		    		
		    		if (Part1.contentEquals(Part2.trim())) {
		    			Log.message("subject Verified Sucessfully");
		    			return true;

		    		} else {
		    			Log.message("Subject  Not Verified Sucessfully");
		    			return false;

		    		}			
	    		    		
	    		    }   		
	    		
	    		
	    		   else if(Flag.equalsIgnoreCase("Search_senderEmail")){
	    			//SkySiteUtils.waitTill(5000);
	    			SkySiteUtils.waitTill(10000);
		    		SkySiteUtils.waitForElement(driver, sendfilelink, 120);		
		    		sendfilelink.click();		    		
		    		SkySiteUtils.waitForElement(driver, TrackingTab, 120);
		    		SkySiteUtils.waitTill(5000);
		    		WebElement element2 = TrackingTab;
		    	    JavascriptExecutor executor2 = (JavascriptExecutor)driver;
		    	    executor2.executeScript("arguments[0].click();", element2);
		    		//TrackingTab.click();
		    		Log.message("Tracking Tab Clicked Sucessfully");
	    		    SkySiteUtils.waitTill(5000);
	    			
	    			String email2 = Filter_receEmailText.getText();
	    			//Log.message("email is2:"+email2);
	    			 SkySiteUtils.waitTill(5000);
	    			
	    			//String[] value_split = email2.split("\\s+ ");    		
	    			//String Part = value_split[1]; 
	    		
		    		Log.message("before comparision:-"+ email2);
		    		
		    		AdvanceSearch_Link.click();
			    	Log.message("Advance Search Option Clicked Sucessfully");	    		
			    	SkySiteUtils.waitTill(1000);
		    		
		    		SkySiteUtils.waitForElement(driver, Filter_Inboxreceivermail, 120);
		    		Filter_Inboxreceivermail.sendKeys(email2);
		    		SkySiteUtils.waitTill(3000);
		    		Log.message("Entered the Value In subject field:-"+email2);	
		    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		    		SkySiteUtils.waitTill(30000);
		    		SearchButton_Advance.click();
		    		Log.message("Search Button Clicked Sucessfully");	
		    		SkySiteUtils.waitForElement(driver, Filter_receEmailText1, 120);
		    		 SkySiteUtils.waitTill(5000);
		    		String email1 = Filter_receEmailText1.getText();
	    			//String[] value_split1 = email1.split("\\s+");    		
	    			//String Part2 = value_split1[1]; 
		    		SkySiteUtils.waitTill(5000);
		    		Log.message("after comparision:-"+ email1);  		
		    		 SkySiteUtils.waitTill(5000);
		    		if (email2.contains(email1)) {
		    			Log.message("Sender Email Verified Sucessfully");
		    			return true;

		    		} else {
		    			Log.message("Sender email  Not Verified Sucessfully");
		    			return false;

		    		}	
	    		
	    		
	    		}
				return result;
				}
	        

	        
	      	public boolean AdvanceSearch_Project(String CommonValue,String Flag) throws Throwable {
	    		boolean result = false;
	    		SkySiteUtils.waitTill(5000);
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
	    		AdvanceSearch_Link.click();
	    		Log.message("Advance Search Option Clicked Sucessfully");	    		
	    		SkySiteUtils.waitTill(5000);
	    		if(Flag.equalsIgnoreCase("Search_ProjectName")){
	    		SkySiteUtils.waitTill(5000);
	    		SkySiteUtils.waitForElement(driver, Filter_ProjectName, 120);
	    		Filter_ProjectName.sendKeys(CommonValue);
	    		Log.message("Entered the Value In Project Name field:-"+CommonValue);
	    		SkySiteUtils.waitTill(30000);
	    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		//SkySiteUtils.waitTill(5000);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");	    			    		
	    		SkySiteUtils.waitTill(25000);
	    		String ProjectName = Staticproject.getText();	    		
	    		//String proName = PropertyReader.getProperty("ProjectName");
	    		if (CommonValue.contains(ProjectName.trim())) {
	    			Log.message("Project Name Verified Sucessfully");	
	    			return true;
	    		} else {
	    			Log.message("Project name Not Verified Sucessfully");
	    			return false;
	    		} 		
	    			
	    		}	    				
	    		else if(Flag.equalsIgnoreCase("Search_ProjectNumber")){
	    		SkySiteUtils.waitForElement(driver, Filter_ProjectNumber,120);
	    		Filter_ProjectNumber.sendKeys(CommonValue);
	    		SkySiteUtils.waitTill(5000);
	    		Log.message("Entered the Value In Project Number field:-"+CommonValue);
	    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SkySiteUtils.waitTill(50000);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");		
	    		SkySiteUtils.waitTill(5000);
	    		String ProjectNo = Project_NumberText.getText();
	    		Log.message("Project No"+ProjectNo);
	    		String[] parts = ProjectNo.split("\\s+");	    		
	    		String part1 = parts[1]; 
	    		Log.message("Project Number"+part1);	
	    		SkySiteUtils.waitTill(5000);
	    		if (part1.trim().contains(CommonValue.trim())) {
	    			Log.message("Project Number Verified Sucessfully");	
	    			return true;
	    		} else {
	    			Log.message("Project Number Not Verified Sucessfully");
	    			return false;
	    		}
	    		}
	    		
	    		else if(Flag.equalsIgnoreCase("Search_ProjectDescription")){
	    		SkySiteUtils.waitForElement(driver, Filter_Description, 120);	    		
	    		Filter_Description.sendKeys(CommonValue);
	    		Log.message("Entered the Value In Project Description field:-"+CommonValue);
	    		SkySiteUtils.waitTill(30000);
	    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);	    	
	    		SearchButton_Advance.click();
	    		SkySiteUtils.waitTill(20000);	    		
	    		SkySiteUtils.waitForElement(driver, projectMultipleObject, 120);
	    		projectMultipleObject.click();
	    		SkySiteUtils.waitForElement(driver, projectMultipleObject_edit, 120);
	    		SkySiteUtils.waitTill(1000);
	    		projectMultipleObject_edit.click();
	    		SkySiteUtils.waitTill(2000);
	    		String Description = Descriptiontext.getAttribute("value");
	    		SkySiteUtils.waitForElement(driver, project_CancelButton, 120);	    	
	    		project_CancelButton.click();  		
	    		
	    		if (Description.trim().contains(CommonValue.trim())) {
	    			Log.message("Project Description Verified Sucessfully");	
                    return true;
	    		} else {
	    			Log.message("Project description Not Verified Sucessfully");
                    return false;
	    		}	
	    		
	    		
	    		}
	    		
	    		
	    		else if(Flag.equalsIgnoreCase("Search_City")){
	    		Filter_CITY.sendKeys(CommonValue);	    		
	    		Log.message("Entered the Value In CITY field:-"+CommonValue);
	    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SkySiteUtils.waitTill(50000);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");
	    		SkySiteUtils.waitTill(30000);	    		
	    		WebElement element = projectMultipleObject;
	    	    JavascriptExecutor executor = (JavascriptExecutor)driver;
	    	    executor.executeScript("arguments[0].click();", element);
	    	 	    		   		
	    		SkySiteUtils.waitForElement(driver, projectMultipleObject_edit, 120);
	    		SkySiteUtils.waitTill(5000);
	    		projectMultipleObject_edit.click();
	    		SkySiteUtils.waitTill(1000);
	    		String CITY = CITYtext.getAttribute("value");
	    		SkySiteUtils.waitForElement(driver, project_CancelButton, 120);
	    		SkySiteUtils.waitTill(1000);
	    		project_CancelButton.click();   		
	    		
	    		if (CITY.trim().contains(CommonValue.trim())) {
	    			Log.message("City Verified Sucessfully");	
                    return true;
	    		} else {
	    			Log.message("City Not Verified Sucessfully");
	    			return false;
	    		}
	    		}
	    		
	    		else if(Flag.equalsIgnoreCase("Search_State")){
		    		SkySiteUtils.waitForElement(driver, AdvanceSearch_ProjectState, 120);
		    		AdvanceSearch_ProjectState.sendKeys(CommonValue);
		    		Log.message("Entered the Value In Status field:-"+CommonValue);
		    		SkySiteUtils.waitTill(50000);
		    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		    		SearchButton_Advance.click();		    	
		    		SkySiteUtils.waitForElement(driver, projectMultipleObject, 120);	    		
		    		WebElement element = projectMultipleObject;
		    	    JavascriptExecutor executor = (JavascriptExecutor)driver;
		    	    executor.executeScript("arguments[0].click();", element);		    	 	    		   		
		    		SkySiteUtils.waitForElement(driver, projectMultipleObject_edit, 120);
		    		SkySiteUtils.waitTill(5000);
		    		projectMultipleObject_edit.click();
		    		SkySiteUtils.waitTill(9000);
		    		Log.message("Search Button Clicked Sucessfully");	    		
		    		String State1 = PropertyReader.getProperty("State");
		    		String State = project_Statetext.getText();
		    		String State2 = project_Statetext.getAttribute("value");
		    		Log.message("State Verified Sucessfully1"+State2);
		    		if (State1.trim().contains(State1.trim())) {
		    			Log.message("State Verified Sucessfully");	
		    			return true;
		    		} else {
		    			Log.message("State Not Verified Sucessfully");
		    			return false;
		    		}
		    		}
	    		
	    		else if(Flag.equalsIgnoreCase("Search_Country")){
		    		SkySiteUtils.waitForElement(driver, AdvanceSearch_ProjectCountry, 120);
		    		AdvanceSearch_ProjectCountry.sendKeys(CommonValue);
		    		Log.message("Entered the Value In Status field:-"+CommonValue);
		    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		    		SkySiteUtils.waitTill(50000);
		    		SearchButton_Advance.click();
		    		Log.message("Search Button Clicked Sucessfully");
		    		SkySiteUtils.waitTill(30000);	    		
		    		WebElement element = projectMultipleObject;
		    	    JavascriptExecutor executor = (JavascriptExecutor)driver;
		    	    executor.executeScript("arguments[0].click();", element);		    	 	    		   		
		    		SkySiteUtils.waitForElement(driver, projectMultipleObject_edit, 120);
		    		SkySiteUtils.waitTill(5000);
		    		projectMultipleObject_edit.click();
		    		SkySiteUtils.waitTill(10000);
		    		String Country1 = PropertyReader.getProperty("Country");
		    		String Country = AdvanceSearch_ProjectCountry.getText();
		    		if (Country1.trim().contains(Country1.trim())) {
		    			Log.message("Country Verified Sucessfully");	
		    			return true;
		    		} else {
		    			Log.message("Country Not Verified Sucessfully");
		    			return false;
		    		}
		    		}
	    		
	    		
	    		return result;

	    	}
	      	
	      	
	      	public boolean AdvanceSearch_Project_oldData(String Flag) throws Throwable {
	    		boolean result = false;
	    		SkySiteUtils.waitTill(5000);	    		
	    		if(Flag.equalsIgnoreCase("Search_ProjectName")){
	    		SkySiteUtils.waitForElement(driver, Staticproject,120);
	    		String ProjectNa = Staticproject.getText();    		
		    	SkySiteUtils.waitTill(2000);
		    	SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 30);
		    	AdvanceSearch_Link.click();
		    	Log.message("Advance Search Option Clicked Sucessfully");	    		
		    	SkySiteUtils.waitTill(2000);	    		
	    		SkySiteUtils.waitForElement(driver, Filter_ProjectName, 120);
	    		Filter_ProjectName.sendKeys(ProjectNa);
	    		Log.message("Entered the Value In Project Name field:-"+ProjectNa);	    	
	    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");	
	    		SkySiteUtils.waitForElement(driver, Staticproject, 120);
	    		SkySiteUtils.waitTill(30000);
	    		String ProjectName = Staticproject.getText();	    		
	    		//String proName = PropertyReader.getProperty("ProjectName");
	    		if (ProjectNa.contains(ProjectName.trim())) {
	    			Log.message("Project Name Verified Sucessfully");	
	    			return true;
	    		} else {
	    			Log.message("Project name Not Verified Sucessfully");
	    			return false;
	    		} 		
	    			
	    		}	    				
	    		else if(Flag.equalsIgnoreCase("Search_ProjectNumber")){
	    		SkySiteUtils.waitForElement(driver, Project_NumberText,120);
	    		String ProjectNo1 = Project_NumberText.getText();
	    		String[] parts2 = ProjectNo1.split("\\s+");	    		
	    		String part2 = parts2[1]; 
	    		SkySiteUtils.waitTill(5000);	    		
		    	SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		    	AdvanceSearch_Link.click();
		    	Log.message("Advance Search Option Clicked Sucessfully");	    		
		    	SkySiteUtils.waitTill(5000);	    		
	    		Filter_ProjectNumber.sendKeys(part2);
	    		SkySiteUtils.waitTill(5000);
	    		Log.message("Entered the Value In Project Number field:-"+ProjectNo1);
	    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SkySiteUtils.waitTill(10000);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");		
	    		SkySiteUtils.waitTill(5000);
	    		String ProjectNo = Project_NumberText.getText();
	    		Log.message("Project No"+ProjectNo);
	    		String[] parts1 = ProjectNo.split("\\s+");	    		
	    		String part3 = parts1[1]; 
	    		Log.message("Project Number"+part2);	
	    		SkySiteUtils.waitTill(5000);
	    		if (part2.trim().contains(part3.trim())) {
	    			Log.message("Project Number Verified Sucessfully");	
	    			return true;
	    		} else {
	    			Log.message("Project Number Not Verified Sucessfully");
	    			return false;
	    		}
	    		}
	    		
	    		else if(Flag.equalsIgnoreCase("Search_ProjectDescription")){
	    			
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
			    AdvanceSearch_Link.click();
			    Log.message("Advance Search Option Clicked Sucessfully");	    		
			    SkySiteUtils.waitTill(5000);	 
	    		SkySiteUtils.waitForElement(driver, Filter_Description, 120);
	    		String Description = PropertyReader.getProperty("ProjectDescription");
	    		Filter_Description.sendKeys(Description);
	    		Log.message("Entered the Value In Project Description field:-"+Description);
	    		SkySiteUtils.waitTill(50000);
	    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);	    	
	    		SearchButton_Advance.click();
	    		SkySiteUtils.waitTill(20000);	    		
	    		SkySiteUtils.waitForElement(driver, projectMultipleObject, 120);
	    		projectMultipleObject.click();
	    		SkySiteUtils.waitForElement(driver, projectMultipleObject_edit, 120);
	    		SkySiteUtils.waitTill(1000);
	    		projectMultipleObject_edit.click();
	    		SkySiteUtils.waitTill(2000);
	    		String Description1 = Descriptiontext.getAttribute("value");
	    		SkySiteUtils.waitForElement(driver, project_CancelButton, 120);	    	
	    		project_CancelButton.click();  		
	    		
	    		if (Description1.trim().contains(Description.trim())) {
	    			Log.message("Project Description Verified Sucessfully");	
                    return true;
	    		} else {
	    			Log.message("Project description Not Verified Sucessfully");
                    return false;
	    		}	
	    		
	    		
	    		}
	    		
				
				
	    		
	    		else if(Flag.equalsIgnoreCase("Search_City")){
	    			
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
			    AdvanceSearch_Link.click();
			    Log.message("Advance Search Option Clicked Sucessfully");	    		
			    SkySiteUtils.waitTill(5000);
	    		String City1 = PropertyReader.getProperty("city");
	    		SkySiteUtils.waitTill(5000);
	    		Filter_CITY.sendKeys(City1);	    		
	    		Log.message("Entered the Value In CITY field:-"+City1);
	    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SkySiteUtils.waitTill(50000);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");
	    		SkySiteUtils.waitTill(30000);	    		
	    		WebElement element = projectMultipleObject;
	    	    JavascriptExecutor executor = (JavascriptExecutor)driver;
	    	    executor.executeScript("arguments[0].click();", element);
	    	 	    		   		
	    		SkySiteUtils.waitForElement(driver, projectMultipleObject_edit, 120);
	    		SkySiteUtils.waitTill(5000);
	    		projectMultipleObject_edit.click();
	    		SkySiteUtils.waitTill(1000);
	    		String CITY = CITYtext.getAttribute("value");
	    		SkySiteUtils.waitForElement(driver, project_CancelButton, 120);
	    		SkySiteUtils.waitTill(1000);
	    		project_CancelButton.click();   		
	    		
	    		if (CITY.trim().contains(City1.trim())) {
	    			Log.message("City Verified Sucessfully");	
                    return true;
	    		} else {
	    			Log.message("City Not Verified Sucessfully");
	    			return false;
	    		}
	    		}
	    		
	    		else if(Flag.equalsIgnoreCase("Search_State")){
	    			SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
				    AdvanceSearch_Link.click();
				    Log.message("Advance Search Option Clicked Sucessfully");	    		
				    SkySiteUtils.waitTill(5000);
		    		SkySiteUtils.waitForElement(driver, AdvanceSearch_ProjectState, 120);
		    		String State1 = PropertyReader.getProperty("state");
		    		SkySiteUtils.waitTill(5000);
		    		AdvanceSearch_ProjectState.sendKeys(State1);
		    		Log.message("Entered the Value In Status field:-"+State1);
		    		SkySiteUtils.waitTill(10000);
		    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		    		SearchButton_Advance.click();		    	
		    		SkySiteUtils.waitForElement(driver, projectMultipleObject, 120);	    		
		    		WebElement element = projectMultipleObject;
		    	    JavascriptExecutor executor = (JavascriptExecutor)driver;
		    	    executor.executeScript("arguments[0].click();", element);		    	 	    		   		
		    		SkySiteUtils.waitForElement(driver, projectMultipleObject_edit, 120);
		    		SkySiteUtils.waitTill(5000);
		    		projectMultipleObject_edit.click();
		    		SkySiteUtils.waitTill(9000);
		    		Log.message("Search Button Clicked Sucessfully");	    		
		    		//String State1 = PropertyReader.getProperty("State");
		    		String State = project_Statetext.getText();
		    		String State2 = project_Statetext.getAttribute("value");
		    		Log.message("State Verified Sucessfully1"+State2);
		    		if (State1.trim().contains(State1.trim())) {
		    			Log.message("State Verified Sucessfully");	
		    			return true;
		    		} else {
		    			Log.message("State Not Verified Sucessfully");
		    			return false;
		    		}
		    		}
	    		
	    		else if(Flag.equalsIgnoreCase("Search_Country")){
	    			
	    			SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
			    	AdvanceSearch_Link.click();
			    	Log.message("Advance Search Option Clicked Sucessfully");	    		
			    	SkySiteUtils.waitTill(5000);
		    		SkySiteUtils.waitForElement(driver, AdvanceSearch_ProjectCountry, 120);
		    		String Country1 = PropertyReader.getProperty("country");
		    		SkySiteUtils.waitTill(5000);
		    		AdvanceSearch_ProjectCountry.sendKeys(Country1);
		    		Log.message("Entered the Value In Status field:-"+Country1);
		    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		    		SkySiteUtils.waitTill(50000);
		    		SearchButton_Advance.click();
		    		Log.message("Search Button Clicked Sucessfully");
		    		SkySiteUtils.waitTill(30000);	    		
		    		WebElement element = projectMultipleObject;
		    	    JavascriptExecutor executor = (JavascriptExecutor)driver;
		    	    executor.executeScript("arguments[0].click();", element);		    	 	    		   		
		    		SkySiteUtils.waitForElement(driver, projectMultipleObject_edit, 120);
		    		SkySiteUtils.waitTill(5000);
		    		projectMultipleObject_edit.click();
		    		SkySiteUtils.waitTill(10000);
		    		//String Country1 = PropertyReader.getProperty("Country");
		    		String Country = AdvanceSearch_ProjectCountry.getText();
		    		if (Country1.trim().contains(Country.trim())) {
		    			Log.message("Country Verified Sucessfully");	
		    			return true;
		    		} else {
		    			Log.message("Country Not Verified Sucessfully");
		    			return false;
		    		}
		    		}
	    		
	    		
	    		return result;

	    	}
	      	
	      	public boolean projectno_sharedproject(){
	      		boolean result = false;
	      	SkySiteUtils.waitTill(5000);
	      	SkySiteUtils.waitForElement(driver, Project_NumberText1,120);
    		String ProjectNo1 = Project_NumberText1.getText();
    		SkySiteUtils.waitTill(5000);
    		String[] parts2 = ProjectNo1.split(" ");	    		
    		String part2 = parts2[1]; 
    		Log.message("project N0:  "+ part2);	
    		SkySiteUtils.waitTill(5000);	    		
	    	SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
	    	AdvanceSearch_Link.click();
	    	Log.message("Advance Search Option Clicked Sucessfully");	    		
	    	SkySiteUtils.waitTill(5000);	    		
    		Filter_ProjectNumber.sendKeys(part2);
    		SkySiteUtils.waitTill(5000);
    		Log.message("Entered the Value In Project Number field:-"+ProjectNo1);
    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
    		SkySiteUtils.waitTill(10000);
    		SearchButton_Advance.click();
    		Log.message("Search Button Clicked Sucessfully");		
    		SkySiteUtils.waitTill(5000);
    		String ProjectNo = Project_NumberText.getText();
    		Log.message("Project No"+ProjectNo);
    		String[] parts1 = ProjectNo.split("\\s+");	    		
    		String part3 = parts1[1]; 
    		Log.message("Project Number"+part2);	
    		SkySiteUtils.waitTill(5000);
    		if (part2.trim().contains(part3.trim())) {
    			Log.message("Project Number Verified Sucessfully");	
    			return true;
    		} else {
    			Log.message("Project Number Not Verified Sucessfully");
    			return false;
    		}}
	      	
	      	
	      	
	      	
	      	public boolean AdvanceSearch_Uploadwithindex(String CommonValue,String Flag) throws Throwable {
	    		boolean result = false;	
	    		boolean result1 = false;	
	    		boolean result2 = false;	
	    		boolean result3 = false;	
	    		boolean result4 = false;
	    		boolean result5 = false;
	    		boolean result6 = false;
	    		SkySiteUtils.waitTill(5000);
	    		if(Flag.equalsIgnoreCase("Search_SheetNumber"))
	    		{
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		    	AdvanceSearch_Link.click();
		    	Log.message("Advance Search Option Clicked Sucessfully");	    		
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_SheetNumber, 120);
	    		SkySiteUtils.waitTill(3000); 
	    		AdvanceSearch_SheetNumber.sendKeys(CommonValue);
	    		Log.message("Entered the Value In Sheet Number  field:-"+CommonValue);	    		
	    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SkySiteUtils.waitTill(10000);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");
	    		SkySiteUtils.waitForElement(driver, Indexthreedoticon, 120);
	    		SkySiteUtils.waitTill(5000); 
	    		Indexthreedoticon.click();
	    		Log.message("index icon has been clicked");
	    		SkySiteUtils.waitTill(5000); 
	    		SkySiteUtils.waitForElement(driver, IndexEditField, 120);
	    		IndexEditField.click();
	    		Log.message("edit field has been clicked");
	    		SkySiteUtils.waitTill(10000); 
	    		String SheetNumber = IndexDocumentName.getAttribute("Value").toString();   		
	    		Log.message("index doc name is:"+SheetNumber);
	    		if (CommonValue.contentEquals(SheetNumber.trim()))
	    		{
	    			result1=true;
	    			Log.message("Sheet Number Verified Sucessfully");	
                  
	    		} 
	    		else 
	    		{
	    			result1=false;
	    			Log.message("Sheet Number Verified NOT Sucessfully");                   
	    		}	    		
	    		}	    				
	    		else if(Flag.equalsIgnoreCase("Search_SheetName")){
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		    	AdvanceSearch_Link.click();
		    	Log.message("Advance Search Option Clicked Sucessfully");		    			
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_SheetName, 120);
	    		SkySiteUtils.waitTill(3000); 
	    		AdvanceSearch_SheetName.sendKeys(CommonValue);	    		
	    		SkySiteUtils.waitTill(10000);
	    		Log.message("Entered the Value In Sheet Name field:-"+CommonValue);
	    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");  
	    		
	    		SkySiteUtils.waitForElement(driver, Indexthreedoticon, 120);
	    		SkySiteUtils.waitTill(5000); 
	    		Indexthreedoticon.click();
	    		Log.message("Three Dot Icon Clicked Sucessfully"); 
	    		SkySiteUtils.waitTill(5000); 
	    		SkySiteUtils.waitForElement(driver, IndexEditField, 120);
	    		IndexEditField.click();
	    		Log.message("Edit Icon Clicked Sucessfully"); 
	    		SkySiteUtils.waitTill(5000); 
	    		String name = Indexdesc.getText();	    		 
	    		Log.message("index doc name is:----"+name);
	    		Log.message("Act index doc name is:----"+CommonValue);
	    		SkySiteUtils.waitTill(5000);
	    		
	    		if (name.contains(CommonValue))
	    		{
	    			result2=true;
	    			Log.message("Sheet Name Verified Sucessfully");                   
	    		} 
	    		else 
	    		{
	    			result2=false;
	    			Log.message("Sheet Name Verified NOT Sucessfully");
                  
	    		}
	    		}
	    		else if(Flag.equalsIgnoreCase("Search_Revision")){
		    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
			    	AdvanceSearch_Link.click();
			    	Log.message("Advance Search Option Clicked Sucessfully");		    			
		    		SkySiteUtils.waitForElement(driver, AdvanceSearch_RevisionName, 120);
		    		SkySiteUtils.waitTill(3000); 
		    		AdvanceSearch_RevisionName.sendKeys(CommonValue);	    		
		    		SkySiteUtils.waitTill(10000);
		    		Log.message("Entered the Value In Revision field:-"+CommonValue);
		    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		    		SearchButton_Advance.click();
		    		Log.message("Search Button Clicked Sucessfully"); 
		    		
		    		SkySiteUtils.waitForElement(driver, Indexthreedoticon, 120);
		    		SkySiteUtils.waitTill(5000); 
		    		Indexthreedoticon.click();
		    		SkySiteUtils.waitTill(5000); 
		    		SkySiteUtils.waitForElement(driver, IndexEditField, 120);
		    		IndexEditField.click();
		    		SkySiteUtils.waitTill(10000); 
		    		String Revision = IndexRevision.getAttribute("Value"); 
		    		
		    		Log.message("revision Name:---- "+Revision);	
		    		if (Revision.contains(CommonValue.trim())) 
		    		{   SkySiteUtils.waitTill(5000);
		    			result3=true;
		    			Log.message("revision Name Verified Sucessfully");	
	                  
		    		} else {
		    			result3=false;
		    			Log.message("revision Name Verified NOT Sucessfully");
	                 
		    		}
	    		
	    		}
	    		
	    		else if(Flag.equalsIgnoreCase("Search_Discipline")){
		    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
			    	AdvanceSearch_Link.click();
			    	Log.message("Advance Search Option Clicked Sucessfully");		    			
		    		SkySiteUtils.waitForElement(driver, AdvanceSearch_RevisionName, 120);
		    		SkySiteUtils.waitTill(3000); 
		    		AdvanceSearch_Disicipline.sendKeys(CommonValue);	    		
		    		SkySiteUtils.waitTill(10000);
		    		Log.message("Entered the Value In Discipline field:-"+CommonValue);
		    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		    		SearchButton_Advance.click();
		    		Log.message("Search Button Clicked Sucessfully");  
		    		
		    		SkySiteUtils.waitForElement(driver, Indexthreedoticon, 120);
		    		SkySiteUtils.waitTill(5000); 
		    		Indexthreedoticon.click();
		    		Log.message("Icon three dot Clicked Sucessfully");  
		    		SkySiteUtils.waitTill(5000); 
		    		SkySiteUtils.waitForElement(driver, IndexEditField, 120);
		    		IndexEditField.click();
		    		Log.message("Edit Link Clicked Sucessfully"); 
		    		SkySiteUtils.waitTill(10000); 
		    		String Decipline = IndexDecilipline.getAttribute("Value");    		
		    		
		    		if (CommonValue.contains(Decipline.trim()))
		    		{
		    			result4=true;
		    			Log.message("Discipline Verified Sucessfully");	
	                 
		    		} else {
		    			result4=false;
		    			Log.message("Discipline Verified NOT Sucessfully");
	                 
		    		}
	    		
	    		}
	    		else if(Flag.equalsIgnoreCase("Search_RevisionDate")){
	    			SkySiteUtils.waitTill(5000); 
	    			SkySiteUtils.waitForElement(driver, Indexthreedoticon, 120);
		    		SkySiteUtils.waitTill(5000); 
		    		Indexthreedoticon.click();
		    		SkySiteUtils.waitTill(5000); 
		    		SkySiteUtils.waitForElement(driver, IndexEditField, 120);
		    		IndexEditField.click();
		    		SkySiteUtils.waitTill(10000); 
		    		String Date1 = Indexdate.getAttribute("Value");
		    		Log.message("Index Date:---"+ Date1); 
		    		
		    		Date today = new Date();
		    		DateFormat format = new SimpleDateFormat(Date1);
		    		DateFormat year = new SimpleDateFormat("yyyy");
		    		Log.message("Index year:---"+ format.format(today)); 
		    		DateFormat month = new SimpleDateFormat("MM");
		    		Log.message("Index month:---"+ month.format(today)); 
		    		DateFormat day = new SimpleDateFormat("dd");
		    		Log.message("Index day:---"+ day.format(today)); 
		    		System.out.println("today is: " + format.format(today));
		    		System.out.println("The year is: " + year.format(today));
		    		System.out.println("The month is: " + month.format(today));
		    		System.out.println("The day is: " + day.format(today));
		    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
			    	AdvanceSearch_Link.click();
			    	Log.message("Advance Search Option Clicked Sucessfully");		    			
			    	SkySiteUtils.waitForElement(driver, RevisionDate, 120);			    	
			    	RevisionDate.click();
			    	Log.message("Date Clicked Sucessfully"); 
		    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		    		SearchButton_Advance.click();
		    		Log.message("Search Button Clicked Sucessfully");		    		
		    		SkySiteUtils.waitForElement(driver, Indexthreedoticon, 120);
		    		SkySiteUtils.waitTill(5000); 
		    		Indexthreedoticon.click();
		    		Log.message("Three Dot Icon Clicked Sucessfully"); 
		    		SkySiteUtils.waitTill(5000); 
		    		SkySiteUtils.waitForElement(driver, IndexEditField, 120);
		    		IndexEditField.click();
		    		Log.message("Edit field link Clicked Sucessfully"); 
		    		SkySiteUtils.waitTill(10000); 
		    		String date = Indexdate.getAttribute("Value"); 
		    		Log.message("Date:----"+date);
		    		
		    		if (CommonValue.contains(date.trim())) 
		    		{
		    			result5=true;
		    			Log.message("Date Verified Sucessfully");		                  
		    		} else {
		    			result5=false;
		    			Log.message("Date Verified NOT Sucessfully");
	                  
		    		}	    		
	    		}	    		
				if((result1==true)||(result2==true)||(result3==true)||(result4==true)||(result5==true))
					return true;
				else
					return false;
	      	}
	        
	        
	       public boolean AdvanceSearch_Punch(String CommonValue,String Flag) throws Throwable {
	    		boolean result = false;	
	    		SkySiteUtils.waitTill(5000);
	    		if(Flag.equalsIgnoreCase("Search_Description")){
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		    	AdvanceSearch_Link.click();
		    	SkySiteUtils.waitTill(5000);
		    	Log.message("Advance Search Option Clicked Sucessfully"); 		
	    		
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_PunchDescription, 120);
	    		SkySiteUtils.waitTill(5000); 
	    		AdvanceSearch_PunchDescription.sendKeys(CommonValue);
	    		Log.message("Entered the Value In Description field:-"+CommonValue);	    		
	    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SkySiteUtils.waitTill(40000);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");
	    		SkySiteUtils.waitTill(3000); 	    		
	    		String punch_Description = PunchDescription_text.getText();	
	    		Log.message("punch status is:"+punch_Description);
	    		SkySiteUtils.waitTill(5000); 
	    		
	    		if (punch_Description.contentEquals(CommonValue.trim())) {
	    			Log.message("Description Verified Sucessfully");	
                   return true;
	    		} else {
	    			Log.message("Description Not Verified Sucessfully");
                   return false;
	    		}
	    		
	    		}	    				
	    		else if(Flag.equalsIgnoreCase("Search_Creator")){
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		    	AdvanceSearch_Link.click();
		    	SkySiteUtils.waitTill(5000);
		    	Log.message("Advance Search Option Clicked Sucessfully");		    			
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_PunchCreator, 120);
	    		SkySiteUtils.waitTill(5000);
	    		AdvanceSearch_PunchCreator.sendKeys(CommonValue);	    		
	    		SkySiteUtils.waitTill(30000);
	    		Log.message("Entered the Value In Creator field:-"+CommonValue);
	    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");		
	    		SkySiteUtils.waitTill(3000);
	    		/*SkySiteUtils.waitForElement(driver, Profile, 120);
	    		Profile.click();
	    		SkySiteUtils.waitForElement(driver, MYProfile, 120);
	    		MYProfile.click();*/	    		
	    		String createrText = "arc";
	    		SkySiteUtils.waitTill(5000);
	    		
	    		if (createrText.trim().contentEquals(CommonValue.trim())) {
	    			Log.message("Creator Verified Sucessfully");	
	    			return true;
	    		} else {
	    			Log.message("Creator Not Verified Sucessfully");
	    			return false;
	    		}
	    		
	    		}
	    		
	    		else if(Flag.equalsIgnoreCase("Search_Assignee")){
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		    	AdvanceSearch_Link.click();
		    	SkySiteUtils.waitTill(5000);
		    	Log.message("Advance Search Option Clicked Sucessfully");		    		
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_PunchAssignee, 120);
	    		SkySiteUtils.waitTill(5000);
	    		AdvanceSearch_PunchAssignee.sendKeys(CommonValue);
	    		Log.message("Entered the Value In Assignee field:-"+CommonValue);
	    		SkySiteUtils.waitTill(30000);
	    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");	    		
	    		String Assignee1 = Assignee.getText();
	    		SkySiteUtils.waitTill(5000);
	    		String [] arrOfStr = Assignee1.split(" ");
	    		String part1 = arrOfStr[2];
	    		//String part2 = arrOfStr[1];
	    		Log.message("Assignee Verified Sucessfully"+part1);
	    		SkySiteUtils.waitTill(2000);
	    		if (part1.contentEquals(CommonValue.trim())) {
	    			Log.message("Assignee Verified Sucessfully");	
	    			return true;
	    		} else {
	    			Log.message("Assignee Not Verified Sucessfully");
	    			return false ;
	    		}	}
	    		
	    		
	    		
	    		else if(Flag.equalsIgnoreCase("Search_Status")){
	    		SkySiteUtils.waitTill(5000);
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		    	AdvanceSearch_Link.click();
		    	SkySiteUtils.waitTill(5000);
		    	Log.message("Advance Search Option Clicked Sucessfully");		    		
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_PunchStatus, 120);
	    		SkySiteUtils.waitTill(5000);
	    		AdvanceSearch_PunchStatus.sendKeys(CommonValue);	    		
	    		Log.message("Entered the Value In Status field:-"+CommonValue);
	    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SkySiteUtils.waitTill(30000);
	    		SearchButton_Advance.click();	    		
	    		Log.message("Search Button Clicked Sucessfully");	    		
	    		SkySiteUtils.waitTill(5000);
	    		SkySiteUtils.waitForElement(driver, Punchstatus_text, 120);
	    		String status = Punchstatus_text.getText();	    		
	    		SkySiteUtils.waitTill(5000);
	    		if (status.equalsIgnoreCase(CommonValue)) {
	    			Log.message("status Verified Sucessfully");	
	    			SkySiteUtils.waitTill(2000);
	    			return true;
	    		} else {
	    			Log.message("Status Not Verified Sucessfully");
	    			return false;
	    		}
	    		}
				return result;	    		
	    		
	    	    }

	       public boolean AdvanceSearch_Submittal(String CommonValue,String Flag) throws Throwable {
	    		boolean result = false;
	    		SkySiteUtils.waitTill(5000);
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
	    		AdvanceSearch_Link.click();
	    		SkySiteUtils.waitTill(5000);
	    		Log.message("Advance Search Option Clicked Sucessfully");
	    		
	    		if(Flag.equalsIgnoreCase("Search_SubmittalNumber")){
	    		
	    		SkySiteUtils.waitForElement(driver, Filter_SubmittalNumber, 120);
	    		SkySiteUtils.waitTill(5000);
	    		Filter_SubmittalNumber.sendKeys(CommonValue);	
	    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		Log.message("Entered the Value In Submittal Number field:-"+CommonValue);	
	    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SkySiteUtils.waitTill(50000);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");	    		
	    		SkySiteUtils.waitTill(5000);
	    		String SubmittalNumber = SubmittalN0.getText();
	    		Log.message("Submittal Number is:" + SubmittalN0);
	    		
	    		if (SubmittalNumber.trim().contentEquals(CommonValue.trim())) {
	    			Log.message("Maching Submittal Number Found!!");
                     return true;
	    		} else {

	    			Log.message("Matching Submittal Number Not Found!!");
                     return false;
	    		}
	    		
	    		}	    				
	    		else if(Flag.equalsIgnoreCase("Search_SubmittalName")){
	    		SkySiteUtils.waitTill(5000);
	    		SkySiteUtils.waitForElement(driver, Filter_SubmittalName, 120);
	    		Filter_SubmittalName.sendKeys(CommonValue);	
	    		Log.message("Entered the Value In Submittal Name field:-"+CommonValue);
	    		SkySiteUtils.waitTill(40000);
	    		Log.message("Entered the Value In Creator field:-"+CommonValue);
	    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");		
	    		SkySiteUtils.waitTill(3000);	    		    		
	    		String SubmittalName1 = SubmittalName.getText();
	    		Log.message("Submittal Number is:" + SubmittalName);
	    		if (SubmittalName1.trim().contentEquals(CommonValue.trim())) {
	    			Log.message("Maching Submittal Name Found!!");
	    			 return true;
	    		} else {

	    			Log.message("Matching Submittal Name Not Found!!");
	    			 return false;
	    		}
	    		
	    		}
	    		
	    		else if(Flag.equalsIgnoreCase("Search_SubmittalType")){
	    		SkySiteUtils.waitTill(1000);
	    		//SkySiteUtils.waitForElement(driver, dropDownSubmittalType, 120);
	    		
	    		Select object = new Select(driver.findElement(By.xpath(".//*[@id='ddlSubmittalType_filter']")));
	    		object.selectByVisibleText(CommonValue);	    		
	    		SkySiteUtils.waitTill(50000);
	    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SearchButton_Advance.click();
	    		SkySiteUtils.waitTill(5000);
	    		Log.message("Search Button Clicked Sucessfully");
	    		SkySiteUtils.waitForElement(driver, FirstSubmittallist, 60);
	    		FirstSubmittallist.click();
	    		SkySiteUtils.waitForElement(driver, submittalTypeText, 60);
	    		SkySiteUtils.waitTill(5000);
	    		String Submittal= submittalTypeText.getText();
	    		SkySiteUtils.waitForElement(driver, CloseSubmittalwindow,120);
	    		CloseSubmittalwindow.click();
	    		SkySiteUtils.waitTill(5000);
	    		if (Submittal.contentEquals(CommonValue.trim())) {
	    			Log.message("Submittal Type Verified Sucessfully");	
	    			 return true;
	    		} else {
	    			Log.message("Submittal Type Not Verified Sucessfully");
	    			 return false;
	    		}	}
	    		
	    		
	    		
	    		else if(Flag.equalsIgnoreCase("Search_Status")){
	    		SkySiteUtils.waitForElement(driver, Filter_SubmittalStatus, 120);
	    		Filter_SubmittalStatus.click();
	    		//SkySiteUtils.waitForElement(driver, Submittal_Status_filter, 120);
	    		SkySiteUtils.waitTill(5000);   		
	    		Submittal_Status_filter.click();	    		
	    		Log.message("Entered the Value In Status field:-"+CommonValue);
	    		SkySiteUtils.waitTill(5000);
	    		SkySiteUtils.waitForElement(driver, Filter_SubmittalStatus, 120);
	    		Filter_SubmittalStatus.click();
	    		SkySiteUtils.waitTill(50000);
	    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SearchButton_Advance.click(); 
	    		Log.message("Search Button Clicked Sucessfully");	    		
	    		String status1 = PropertyReader.getProperty("Status");
	    		SkySiteUtils.waitTill(5000);
	    		String status = submittalstatus_text.getText();
	    		SkySiteUtils.waitTill(5000);
	    		if (status.trim().equalsIgnoreCase(status1.trim())) {
	    			Log.message("status Verified Sucessfully");	
	    			 return true;
	    		} else {
	    			Log.message("Status Not Verified Sucessfully");
	    			 return false;
	    		}
	    		}
	    		
	    		else if(Flag.equalsIgnoreCase("Search_DueDate")){
		    		SkySiteUtils.waitForElement(driver, Filter_DueDate1, 120);		    		
		    		Filter_DueDate1.click();// Click on due date
		    		Log.message(" Entered The value in Date Field");
		    		SkySiteUtils.waitTill(5000);		    		
		    		//=======================================
		    		Calendar cal = Calendar.getInstance();
		    		int currentDate = cal.get(Calendar.DATE);
		    		cal.add(Calendar.DAY_OF_MONTH, -5);
		    		int selectDate = cal.get(Calendar.DATE);
		    		String dateLoc = String.format("//td[text()='%d' and @class='day']", selectDate);
		    		Log.message("The locator is:"+dateLoc);
		    		driver.findElement(By.xpath(dateLoc)).click();// Select due date from cal
            		SkySiteUtils.waitTill(30000);
		    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		    		SearchButton_Advance.click(); 
		    		Log.message("Search Button Clicked Sucessfully");	    		
		    		String dueDate1 = PropertyReader.getProperty("Status");
		    		SkySiteUtils.waitTill(5000);
		    		String duedate2 = submittalstatus_text.getText();
		    		SkySiteUtils.waitTill(5000);
		    		if (dueDate1.trim().equalsIgnoreCase(duedate2.trim())) {
		    			Log.message("DueDate Verified Sucessfully");	
                         return true;
		    		} else {
		    			Log.message("Due Date Not Verified Sucessfully");
                         return false;
		    		}
		    		}
	    		
	    		
	    		return result;

	    	}
	       
	       
	       public boolean AdvanceSearch_Submittal_old(String Flag) throws Throwable {
	    		boolean result = false;
	    		SkySiteUtils.waitTill(5000);
	    		
	    		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
	    		AdvanceSearch_Link.click();
	    		Log.message("Advance Search Option Clicked Sucessfully");
	    		
	    		if(Flag.equalsIgnoreCase("Search_SubmittalNumber")){
	    		
	    		SkySiteUtils.waitForElement(driver, Filter_SubmittalNumber, 120);
	    		String SubmittalNumber = SubmittalN0.getText();
	    		SkySiteUtils.waitTill(5000);
	    		Filter_SubmittalNumber.sendKeys(SubmittalNumber);	
	    		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		Log.message("Entered the Value In Submittal Number field:-"+SubmittalNumber);	
	    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SkySiteUtils.waitTill(50000);
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");	    		
	    		SkySiteUtils.waitTill(5000);
	    		String SubmittalNumber2= SubmittalN0.getText();
	    		Log.message("Submittal Number is:" + SubmittalN0);
	    		
	    		if (SubmittalNumber.trim().contentEquals(SubmittalNumber2.trim())) {
	    			Log.message("Maching Submittal Number Found!!");
                    return true;
	    		} else {

	    			Log.message("Matching Submittal Number Not Found!!");
                    return false;
	    		}
	    		
	    		}	    				
	    		else if(Flag.equalsIgnoreCase("Search_SubmittalName")){
	    		SkySiteUtils.waitTill(5000);
	    		SkySiteUtils.waitForElement(driver, Filter_SubmittalName, 120);
	    		String SubmittalName1 = SubmittalName.getText();
	    		Filter_SubmittalName.sendKeys(SubmittalName1);	
	    		Log.message("Entered the Value In Submittal Name field:-"+SubmittalName1);
	    		SkySiteUtils.waitTill(40000);
	    		Log.message("Entered the Value In Creator field:-"+SubmittalName1);
	    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		
	    		SearchButton_Advance.click();
	    		Log.message("Search Button Clicked Sucessfully");		
	    		SkySiteUtils.waitTill(3000);	    		    		
	    		String SubmittalName2 = SubmittalName.getText();
	    		Log.message("Submittal Number is:" + SubmittalName);
	    		if (SubmittalName1.trim().contentEquals(SubmittalName2.trim())) {
	    			Log.message("Maching Submittal Name Found!!");
	    			 return true;
	    		} else {

	    			Log.message("Matching Submittal Name Not Found!!");
	    			 return false;
	    		}
	    		
	    		}
	    		
	    		else if(Flag.equalsIgnoreCase("Search_SubmittalType")){
	    		SkySiteUtils.waitTill(1000);
	    		//SkySiteUtils.waitForElement(driver, dropDownSubmittalType, 120);
	    		String Submittal = PropertyReader.getProperty("SubmittalType");	    		
	    		Select object = new Select(driver.findElement(By.xpath(".//*[@id='ddlSubmittalType_filter']")));	    	
	    		object.selectByVisibleText(Submittal);	
	    		//String Submittal= submittalTypeText.getText();
	    		SkySiteUtils.waitTill(50000);
	    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SearchButton_Advance.click();
	    		SkySiteUtils.waitTill(5000);
	    		Log.message("Search Button Clicked Sucessfully");
	    		SkySiteUtils.waitForElement(driver, FirstSubmittallist, 60);
	    		FirstSubmittallist.click();
	    		SkySiteUtils.waitForElement(driver, submittalTypeText, 60);
	    		SkySiteUtils.waitTill(5000);
	    		String Submittal1= submittalTypeText.getText();
	    		SkySiteUtils.waitForElement(driver, CloseSubmittalwindow,120);
	    		CloseSubmittalwindow.click();
	    		SkySiteUtils.waitTill(5000);
	    		if (Submittal.contentEquals(Submittal1.trim())) {
	    			Log.message("Submittal Type Verified Sucessfully");	
	    			 return true;
	    		} else {
	    			Log.message("Submittal Type Not Verified Sucessfully");
	    			 return false;
	    		}	}
	    		
	    		
	    		
	    		else if(Flag.equalsIgnoreCase("Search_Status")){
	    		SkySiteUtils.waitForElement(driver, Filter_SubmittalStatus, 120);
	    		Filter_SubmittalStatus.click();
	    		//SkySiteUtils.waitForElement(driver, Submittal_Status_filter, 120);
	    		SkySiteUtils.waitTill(5000);   		
	    		Submittal_Status_filter.click();	    		
	    		Log.message("Entered the Value In Status field:-");
	    		SkySiteUtils.waitTill(5000);
	    		SkySiteUtils.waitForElement(driver, Filter_SubmittalStatus, 120);
	    		Filter_SubmittalStatus.click();
	    		SkySiteUtils.waitTill(50000);
	    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	    		SearchButton_Advance.click(); 
	    		Log.message("Search Button Clicked Sucessfully");	    		
	    		String status1 = PropertyReader.getProperty("Status");
	    		SkySiteUtils.waitTill(5000);
	    		String status = submittalstatus_text.getText();
	    		SkySiteUtils.waitTill(5000);
	    		if (status.trim().equalsIgnoreCase(status1.trim())) {
	    			Log.message("status Verified Sucessfully");	
	    			 return true;
	    		} else {
	    			Log.message("Status Not Verified Sucessfully");
	    			 return false;
	    		}
	    		}
	    		
	    		else if(Flag.equalsIgnoreCase("Search_DueDate")){
		    		SkySiteUtils.waitForElement(driver, Filter_DueDate1, 120);		    		
		    		Filter_DueDate1.click();// Click on due date
		    		Log.message(" Entered The value in Date Field");
		    		SkySiteUtils.waitTill(5000);		    		
		    		Calendar cal = Calendar.getInstance();
		    		int currentDate = cal.DATE;
		    		int selectDate = currentDate;
		    		String dateLoc = String.format("//td[text()='%d' and @class='day']", selectDate);
		    		Log.message("The locator is:"+dateLoc);
		    		driver.findElement(By.xpath(dateLoc)).click();// Select due date from cal
		    		SkySiteUtils.waitTill(50000);
		    		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		    		SearchButton_Advance.click(); 
		    		Log.message("Search Button Clicked Sucessfully");	    		
		    		String dueDate1 = PropertyReader.getProperty("Status");
		    		SkySiteUtils.waitTill(5000);
		    		String duedate2 = submittalstatus_text.getText();
		    		SkySiteUtils.waitTill(5000);
		    		if (dueDate1.trim().equalsIgnoreCase(duedate2.trim())) {
		    			Log.message("DueDate Verified Sucessfully");	
                        return true;
		    		} else {
		    			Log.message("Due Date Not Verified Sucessfully");
                        return false;
		    		}
		    		}
	    		
	    		
	    		return result;

	    	}

	   	

	   	
	   	public boolean AdvanceSearch_SharedProject(String CommonValue,String flag) throws Throwable {
	   		boolean result = false;
	   				
	   		if(flag.equalsIgnoreCase("Search_FirstName")){

			SkySiteUtils.waitTill(5000);
			Log.message("Enter Into  filter By search status");
			SkySiteUtils.waitForElement(driver, Contacts, 60);

			//Log.message("Contact Link clicked Sucessfully");
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
			AdvanceSearch_Link.click();
			Log.message("Advance Search Option Clicked Sucessfully");
			SkySiteUtils.waitForElement(driver, AdvanceSearch_FirstName, 120);
			SkySiteUtils.waitTill(5000);			
			AdvanceSearch_FirstName.sendKeys(CommonValue);
			Log.message("Entered the Value In First Name field:-" + CommonValue);
			SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
			SearchButton_Advance.click();
			Log.message("Search Button Clicked Sucessfully");
			SkySiteUtils.waitTill(1000);
			String Cont_FirstName = Name.getText();
			String[] parts = Cont_FirstName.split(" ");
			String part1 = parts[0]; // 004
			SkySiteUtils.waitTill(1000);
			String Contact_FirstName = PropertyReader.getProperty("Contact_FirstName");
			if (part1.trim().contentEquals(Contact_FirstName.trim())) {
				Log.message("First Name Verified Sucessfully");
	                 return true;
	   		} else {
	   			Log.message("First Name Not Verified Sucessfully");
	                 return false;
	   		}
	   		}
	   				
	   		else if(flag.equalsIgnoreCase("Search_LastName")){
	   		SkySiteUtils.waitTill(5000);
	   		AdvanceSearch_LastName.sendKeys(CommonValue);
	   		SkySiteUtils.waitTill(5000);
	   		Log.message("Entered the Value In Last Name field:-"+CommonValue);
	   		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	   		SearchButton_Advance.click();
	   		Log.message("Search Button Clicked Sucessfully");		
	   		SkySiteUtils.waitTill(5000);
	   		String Cont_FirstName = Name.getText();
	   		String[] parts = Cont_FirstName.split(" ");
	   		String part1 = parts[1]; // 004
	   		String Contact_LastName = PropertyReader.getProperty("Contact_LastName");
	   		if (part1.trim().contentEquals(Contact_LastName.trim())) {
	   			Log.message("Last Name Verified Sucessfully");	
	   			return true;
	   		} else {
	   			Log.message("Last Name Not Verified Sucessfully");
	   			return false;
	   		}
	   		
	   		}
	   		
	   		else if(flag.equalsIgnoreCase("Search_EMAIL")){
	   		SkySiteUtils.waitTill(1000);
	   		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	   		AdvanceSearch_Email.sendKeys(CommonValue);
	   		Log.message("Entered the Value In Email field:-"+CommonValue);
	   		SkySiteUtils.waitTill(5000);
	   		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	   		SearchButton_Advance.click();
	   		Log.message("Search Button Clicked Sucessfully");
	   		SkySiteUtils.waitTill(1000);
	   		String Cont_email = EmailText.getText();
	   		String textStr[] = Cont_email.split(" ");
	   		String Email = textStr[0];
	   		Log.message("Email Verified Sucessfully:--"+Email);
	   		SkySiteUtils.waitTill(5000);

	   		if (Email.equalsIgnoreCase(CommonValue))
	   		{
	   			Log.message("Email Verified Sucessfully");	
	   			return true;
	   		} 
	   		else 
	   		{
	   			Log.message("Email Not Verified Sucessfully");
	   			return false;
	   		}	
	   		
	   		}
	   		
	   		else if(flag.equalsIgnoreCase("Search_PHONE")){
	   		SkySiteUtils.waitForElement(driver, AdvanceSearch_Phone, 120);
	   		AdvanceSearch_Phone.sendKeys(CommonValue);
	   		Log.message("Entered the Value In Phone field:-"+CommonValue);
	   		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	   		SearchButton_Advance.click();
	   		Log.message("Search Button Clicked Sucessfully");	
	   		String Cont_phone = phoneText.getText();
	   		SkySiteUtils.waitTill(10000);
	   		if(phoneText.isDisplayed())
	   			return true;
	   			else		
	   			 return false;
	   		
	   		/*String textStr[] = Cont_phone.split(" ");
	   		String phone = textStr[0];
	   		Log.message("Display Phone :"+Cont_phone);
	   		SkySiteUtils.waitTill(5000);
	   		if (phone.contains(CommonValue.trim())) {
	   			Log.message("Phone Verified Sucessfully");	
	   			return true;
	   		} else {
	   			Log.message("Phone Not Verified Sucessfully");
	   			return false;
	   		}*/
	   		}
	   		
	   		else if(flag.equalsIgnoreCase("Search_CITY")){
	   		SkySiteUtils.waitForElement(driver, AdvanceSearch_City, 120);
	   		AdvanceSearch_City.sendKeys(CommonValue);
	   		Log.message("Entered the Value In Phone field:-"+CommonValue);
	   		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	   		SearchButton_Advance.click();
	   		Log.message("Search Button Clicked Sucessfully");
	   		SkySiteUtils.waitTill(5000);
	   		if(Contacttext.isDisplayed())
	   			return true;
	   			else		
	   			 return false;
	   		
	   		}
	   		else if(flag.equalsIgnoreCase("Search_Address")){
	   		SkySiteUtils.waitForElement(driver, AdvanceSearch_Address, 120);
	   		AdvanceSearch_Address.sendKeys(CommonValue);
	   		Log.message("Entered the Value In Address field:-"+CommonValue);
	   		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
	   		SearchButton_Advance.click();
	   		Log.message("Search Button Clicked Sucessfully");
	   		SkySiteUtils.waitTill(5000);
	   		if(Contacttext.isDisplayed())
	   			return true;
	   			else		
	   			 return false;
	   		}
	   		
	   		
	   		
	   		return result;

	   	}	    
	
	
	public boolean AdvanceSearch_Contact(String CommonValue,String flag) throws Throwable {
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		Log.message("Enter Into  filter By search status");
		SkySiteUtils.waitForElement(driver, Contacts, 60);
		Contacts.click();
		Log.message("Contact Link clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		AdvanceSearch_Link.click();
		Log.message("Advance Search Option Clicked Sucessfully");		
		if(flag.equalsIgnoreCase("Search_FirstName")){
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, AdvanceSearch_FirstName, 120);
		AdvanceSearch_FirstName.sendKeys(CommonValue);
		Log.message("Entered the Value In First Name field:-"+CommonValue);	
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(20000);
		String Cont_FirstName = Name.getText();
		String[] parts = Cont_FirstName.split(" ");
		String part1 = parts[0]; // 004
		SkySiteUtils.waitTill(1000);
		String Contact_FirstName = PropertyReader.getProperty("Contact_FirstName");
		if (part1.trim().contentEquals(Contact_FirstName.trim())) {
			Log.message("First Name Verified Sucessfully");	
              return true;
		} else {
			Log.message("First Name Not Verified Sucessfully");
              return false;
		}
		}
				
		else if(flag.equalsIgnoreCase("Search_LastName")){
		SkySiteUtils.waitTill(5000);
		AdvanceSearch_LastName.sendKeys(CommonValue);
		SkySiteUtils.waitTill(5000);
		Log.message("Entered the Value In Last Name field:-"+CommonValue);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");		
		SkySiteUtils.waitTill(5000);
		String Cont_FirstName = Name.getText();
		String[] parts = Cont_FirstName.split(" ");
		String part1 = parts[1]; // 004
		String Contact_LastName = PropertyReader.getProperty("Contact_LastName");
		if (part1.trim().contentEquals(Contact_LastName.trim())) {
			Log.message("Last Name Verified Sucessfully");	
			return true;
		} else {
			Log.message("Last Name Not Verified Sucessfully");
			return false;
		}
		
		}
		
		else if(flag.equalsIgnoreCase("Search_EMAIL")){
			SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		AdvanceSearch_Email.sendKeys(CommonValue);
		Log.message("Entered the Value In Email field:-"+CommonValue);
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		String Cont_email = EmailText.getText();
		String textStr[] = Cont_email.split(" ");
		String Email = textStr[0];
		Log.message("Email Verified Sucessfully:--"+Email);
		SkySiteUtils.waitTill(5000);

		if (Email.equalsIgnoreCase(CommonValue))
		{
			Log.message("Email Verified Sucessfully");
			SkySiteUtils.waitTill(5000);
			return true;
		} 
		else 
		{
			Log.message("Email Not Verified Sucessfully");
			return false;
		}	
		
		}
		
		else if(flag.equalsIgnoreCase("Search_PHONE")){
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Phone, 120);
		AdvanceSearch_Phone.sendKeys(CommonValue);
		Log.message("Entered the Value In Phone field:-"+CommonValue);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");	
		String Cont_phone = phoneText.getText();
		SkySiteUtils.waitTill(20000);
		if(phoneText.isDisplayed())
			return true;
			else		
			 return false;
		
		/*String textStr[] = Cont_phone.split(" ");
		String phone = textStr[0];
		Log.message("Display Phone :"+Cont_phone);
		SkySiteUtils.waitTill(5000);
		if (phone.contains(CommonValue.trim())) {
			Log.message("Phone Verified Sucessfully");	
			return true;
		} else {
			Log.message("Phone Not Verified Sucessfully");
			return false;
		}*/
		}
		
		else if(flag.equalsIgnoreCase("Search_CITY")){
		SkySiteUtils.waitForElement(driver, AdvanceSearch_City, 120);
		AdvanceSearch_City.sendKeys(CommonValue);
		Log.message("Entered the Value In Phone field:-"+CommonValue);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		if(Contacttext.isDisplayed())
			return true;
			else		
			 return false;
		
		}
		else if(flag.equalsIgnoreCase("Search_Address")){
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Address, 120);
		AdvanceSearch_Address.sendKeys(CommonValue);
		Log.message("Entered the Value In Address field:-"+CommonValue);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		if(Contacttext.isDisplayed())
			return true;
			else		
			 return false;
		}
		
		
		
		return result;

	}
	
	
	public boolean AdvanceSearch_SharedProject_RFINumber(String flag) throws Throwable {
		boolean result1 = false;		
		if(flag.equalsIgnoreCase("Search_RFINumber")){
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, AllRFITAB, 120);
		AllRFITAB.click();
		SkySiteUtils.waitTill(10000);
		Log.message("All RFI Tab Clicked Sucessfully");
		String RFINUMBER_before = RFI_Number.getText();
		Log.message("RFI Number Before Search:---"+RFINUMBER_before);
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		AdvanceSearch_Link.click();
		Log.message("Advance Search Option Clicked Sucessfully");
		
		SkySiteUtils.waitForElement(driver, Filter_RFINumber, 60);
		Filter_RFINumber.sendKeys(RFINUMBER_before);
		Log.message("Entered the value in RFI Number field");
		SkySiteUtils.waitTill(30000);		
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		String RFINUMBER_After = RFI_Number.getText();
		Log.message("RFI Number After Search"+RFINUMBER_before);
		SkySiteUtils.waitTill(3000);
		if (RFINUMBER_before.trim().contains(RFINUMBER_After.trim())) {
			Log.message("RFI Number Verified Sucessfully");
			SkySiteUtils.waitTill(5000);
			return true;

		} else {

			Log.message("RFI Number Not Verified Sucessfully");
			return false;
		}}
		
		if ((result1 == true) ) {
			Log.message("RFI Verification Sucessfully Completed");
			return true;
		} else {
			Log.message("RFI Verification Not Sucessfully Completed");;
			return false;
		}
	
	}

	public boolean ModuleSearch_SharedProject_RFINumber(String flag) throws Throwable {
		boolean result1 = false;		
		if(flag.equalsIgnoreCase("Search_RFINumber")){
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, AllRFITAB, 120);
		AllRFITAB.click();
		SkySiteUtils.waitTill(5000);
	    Log.message("All RFI Tab Clicked Sucessfully");
		String RFINUMBER_before = RFI_Number.getText();
		Log.message("RFI Number Before Search:---"+RFINUMBER_before);
		SkySiteUtils.waitTill(5000);		
		SkySiteUtils.waitForElement(driver, Filter_RFINumber, 60);
		ModuleInbox.sendKeys(RFINUMBER_before);
		Log.message("Entered The Value In Inbox fileld");
		SkySiteUtils.waitTill(50000);		
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		String RFINUMBER_After = RFI_Number.getText();
		Log.message("RFI Number After Search:-----"+RFINUMBER_before);
		SkySiteUtils.waitTill(3000);		
		
		if (RFINUMBER_before.trim().contentEquals(RFINUMBER_After.trim())) {
			Log.message("RFI Number Verified Sucessfully");
			SkySiteUtils.waitTill(5000);
			return  true;

		} else {
			
			Log.message("RFI Number Not Verified Sucessfully");
			return  false;
		}}
		
		
		if ((result1 == true) ) {
			Log.message("RFI Verification Sucessfully Completed");
			return true;
		} else {
			Log.message("RFI Verification Not Sucessfully Completed");;
			return false;
		}
		
		
		
	
	}
	public boolean RFI_AdvanceSearchWithRFINumber() throws Throwable {
		boolean result = false;
		Log.message("Enter Into  filter By search status");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		AdvanceSearch_Link.click();
		Log.message("Advance Search Option Clicked Sucessfully");
		String RFINUMBER1 = RFI_Number.getText();
		SkySiteUtils.waitForElement(driver, Filter_RFINumber, 60);
		SkySiteUtils.waitTill(5000);
		Filter_RFINumber.sendKeys(RFINUMBER1);
		SkySiteUtils.waitTill(20000);
		//SkySiteUtils.waitForElement(driver, SearchButton_Advance, 60);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		String RFINUMBER2 = RFI_Number.getText();
		SkySiteUtils.waitTill(5000);
		if (RFINUMBER1.trim().contentEquals(RFINUMBER2.trim())) {
			Log.message("RFI Number Verified Sucessfully");
			SkySiteUtils.waitTill(5000);
			return true;

		} else {

			Log.message("RFI Number Not Verified Sucessfully");
            return false;
		}

		

	}
	


	
	
	
	
	public boolean RFI_AdvanceSearchWithRFIDueDate() throws Throwable {
		boolean result = false;
		Log.message("Enter Into  filter By search status");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		AdvanceSearch_Link.click();
		Log.message("Advance Search Option Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		String duedate = RFI_Duedate_Text.getText();
		
		Log.message("Due date"+duedate);

		String[] date = duedate.split("/S/");
		int dateIndex = 1;
		for (String Dates : date) {
			Log.message(dateIndex + ". " + date);

			dateIndex++;
		}

		String textStr[] = duedate.split("\\s", 10);
		String myDate = textStr[4] + " " + textStr[5] + " " + textStr[6];
		int myDate1 = Integer.parseInt(textStr[4]);
		
		Log.message(myDate);
		SkySiteUtils.waitForElement(driver, Date_Link, 120);
		Date_Link.click();		
		String dateLoc = String.format("//td[text()='%d' and @class='day']",myDate1);
		Log.message("The locator is:"+dateLoc);
		driver.findElement(By.xpath(dateLoc)).click();		
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 60);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		if (myDate.trim().contains(myDate.trim())) {
			Log.message("Date Verified Sucessfully!!");
			SkySiteUtils.waitTill(2000);
            return true;
			

		} else {

			Log.message("Date not Verified Sucessfully");
			return false;
		}
		

	}

	
	public boolean AdvanceSearch_Description() throws Throwable {
		boolean result = false;
		Log.message("Enter Into  filter By search status");
		SkySiteUtils.waitTill(5000);
		String Anything = Punch_Text.getText();
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 60);
		AdvanceSearch_Link.click();
		Log.message("Advance Search Option Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, FilterProject_Name, 60);
		//String Anything = Punch_Text.getText();
		String description = PropertyReader.getProperty("Punch_Subject");
		filterSearch_DescriptionName.sendKeys(Anything);
		Log.message("Entered The Value In search Inbox Text Field: - " + Description);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 60);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		if (Anything.trim().contentEquals(description.trim())) {
			Log.message("Maching Punch Description Found!!");
            
			SkySiteUtils.waitTill(2000);
			 return true;
		} else {

			Log.message("Matching Punch  Description Not Found!!");
             return false;
		}

		//return result;

	}
	
	
	
	public boolean AdvanceSearch_punch(String Flag) throws Throwable {		
		boolean result = false;	
		if(Flag.equalsIgnoreCase("Search_Description")){   
		String CommonValue = Punch_Text.getText();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 60);
		AdvanceSearch_Link.click();
		Log.message("Advance Search Option Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, FilterProject_Name, 60);		
		filterSearch_DescriptionName.sendKeys(CommonValue);
		Log.message("Entered The Value In search Inbox Text Field: - " + Description);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 60);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		
		String description = PropertyReader.getProperty("Punch_Subject");
		if (CommonValue.trim().contentEquals(description.trim())) {
			Log.message("Maching Punch Description Found!!");
            
			SkySiteUtils.waitTill(2000);
			 return true;
		} else {

			Log.message("Matching Punch  Description Not Found!!");
             return false;
		}

		}
		if(Flag.equalsIgnoreCase("Search_Creator")){   
			String CommonValue = Punch_Text.getText();
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 60);
			AdvanceSearch_Link.click();
			Log.message("Advance Search Option Clicked Sucessfully");
			SkySiteUtils.waitForElement(driver, FilterProject_Name, 60);		
			filterSearch_DescriptionName.sendKeys(CommonValue);
			Log.message("Entered The Value In search Inbox Text Field: - " + Description);
			SkySiteUtils.waitForElement(driver, SearchButton_Advance, 60);
			SearchButton_Advance.click();
			Log.message("Search Button Clicked Sucessfully");
			
			String description = PropertyReader.getProperty("Punch_Subject");
			if (CommonValue.trim().contentEquals(description.trim())) {
				Log.message("Maching Punch Description Found!!");
	            
				SkySiteUtils.waitTill(2000);
				 return true;
			} else {

				Log.message("Matching Punch  Description Not Found!!");
	             return false;
			}

			}
		
		
		return result;

	}
	
	public boolean CreateProject(String Project_Name,String Project_Number,String Description,String Country,String City,String State )
	{   
		boolean result = false;	
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnCreateProject,60);		
		btnCreateProject.click();
		Log.message("Create Project button is clicked.");
		SkySiteUtils.waitForElement(driver, txtProjectName,60);
		txtProjectName.clear();
		SkySiteUtils.waitForElement(driver, txtProjectName,60);
		txtProjectName.sendKeys(Project_Name);
		Log.message("Project Name has been entered as ."+Project_Name);
		SkySiteUtils.waitForElement(driver, txtProjectNumber,60);
		txtProjectNumber.clear();
		SkySiteUtils.waitForElement(driver, txtProjectNumber,60);
		txtProjectNumber.sendKeys(Project_Number);
		Log.message("Project Number has been entered.");
		SkySiteUtils.waitForElement(driver, txtProjectDescription,60);
		txtProjectDescription.clear();
		SkySiteUtils.waitForElement(driver, txtProjectDescription,60);
		txtProjectDescription.sendKeys(Description);
		Log.message("Project description has been entered.");
		//txtProjectStartDate.clear();
		txtProjectStartDate.sendKeys(PropertyReader.getProperty("Projectstartdate"));
		Log.message("Project start date has been entered.");
		SkySiteUtils.waitForElement(driver, txtAddress,60);
		txtAddress.clear();
		SkySiteUtils.waitForElement(driver, txtAddress,60);
		txtAddress.sendKeys(PropertyReader.getProperty("Siteaddress"));
		Log.message("Site Address has been entered.");
		SkySiteUtils.waitForElement(driver, txtCountry,60);
		txtCountry.clear();
		SkySiteUtils.waitForElement(driver, txtCountry,60);
		txtCountry.sendKeys(Country);
		SkySiteUtils.waitForElement(driver, countryTitleItem,60);
		SkySiteUtils.waitTill(2000);
		countryTitleItem.click();
		Log.message("Country has been selected.");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, txtState,60);
		txtState.clear();
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, txtState,60);
		txtState.sendKeys(State);
		SkySiteUtils.waitForElement(driver, stateTitleItem, 60);
		stateTitleItem.click();
		Log.message("State has been selected.");
		SkySiteUtils.waitForElement(driver, txtCity,60);
		txtCity.clear();
		SkySiteUtils.waitForElement(driver, txtCity,60);
		txtCity.sendKeys(City);
		Log.message("City has been entered.");
		SkySiteUtils.waitForElement(driver, txtZip,60);
		txtZip.clear();
		SkySiteUtils.waitForElement(driver, txtZip,60);
		txtZip.sendKeys(PropertyReader.getProperty("Zip"));
		Log.message("Zip has been entered.");
		SkySiteUtils.waitForElement(driver, btnCreate,60);
		SkySiteUtils.waitTill(2000);
		btnCreate.click();
		Log.message("Create Project button has been clicked.");	
		SkySiteUtils.waitForElement(driver, checkboxEnableAutoHypLink,120);
		SkySiteUtils.waitTill(1000);
		checkboxEnableAutoHypLink.click();
		Log.message("Clicked on enable auto hyperlink checkbox.");
		SkySiteUtils.waitTill(2000);
		btnSavePrjSettingsWin.click();
		Log.message("Clicked on save button of project settings window.");
		SkySiteUtils.waitForElement(driver, btnYes,120);
		SkySiteUtils.waitTill(2000);
		btnYes.click();
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, projectDasboadLink,120);		
		projectDasboadLink.click();
		SkySiteUtils.waitTill(10000);
		return result;
		
	}

	

	@FindBy(css = ".icon.icon-plus-sign.icon-lg")
	WebElement btnAddNewAlbum;
	@FindBy(css = ".btn.btn-default.pull-left")
	WebElement btnCancelAddAlbumWind;
	@FindBy(css = "#txtAlbumName")
	WebElement txtAlbumName;
	@FindBy(xpath = "(//button[@class='btn btn-default'])[4]")
	WebElement btnCreateAlbum;
	@FindBy(xpath = "(//button[@class='btn btn-primary btn-lg'])[2]")
	WebElement btnUploadPhotos;

	public boolean Verify_AddAlbum(String Album_Name) throws InterruptedException, AWTException, IOException {
		boolean result1 = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnAddNewAlbum, 120);
		Log.message("Add New Album button is available.");
		btnAddNewAlbum.click();
		Log.message("Click on Add New Album");
		SkySiteUtils.waitForElement(driver, btnCancelAddAlbumWind, 120);
		txtAlbumName.click();
		SkySiteUtils.waitTill(3000);
		txtAlbumName.sendKeys(Album_Name);
		Log.message("Entered Album Name.:-" + Album_Name);
		SkySiteUtils.waitTill(3000);
		btnCreateAlbum.click();
		Log.message("Clicked on Create Album button.");
		SkySiteUtils.waitForElement(driver, notificationMsg, 20);
		String ActualMsg = notificationMsg.getText();
		Log.message("Notify message after create an Album is: " + ActualMsg);
		SkySiteUtils.waitTill(5000);
		// Counting available Albums
		int Album_Count = 0;
		int i = 1;
		List<WebElement> allElements = driver.findElements(By.xpath("//section[1]/i"));
		for (WebElement Element : allElements) {
			Album_Count = Album_Count + 1;
		}
		Log.message("Available Albums in a gallery is: " + Album_Count);
		// Getting Album Names and matching
		for (i = 1; i <= Album_Count; i++) {
			String Act_AlbumName = driver
					.findElement(By.xpath("html/body/div[1]/div[4]/div[2]/div/div/div/ul/li[" + i + "]/section[2]/h4"))
					.getText();
			if ((Act_AlbumName.contentEquals(Album_Name)) && (ActualMsg.contentEquals("Album successfully created."))) {
				result1 = true;
				break;
			}
		}
		if (result1 == true) {
			Log.message("Add Album is working successfully!!!");
			return true;
		} else {
			Log.message("Add Album is NOT working!!!");
			return false;
		}
	}

	public boolean AdvanceStatus(String StatusDispaly) throws Throwable {
		boolean result = true;
		Log.message("Enter Into  filter By search status");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 60);
		AdvanceSearch_Link.click();
		Log.message("Advance Search Option Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, Status_Inbox, 60);
		SkySiteUtils.waitTill(3000);
		Status_Inbox.sendKeys(StatusDispaly);
		Log.message("Entered The Value In search Inbox Text Field: - " + StatusDispaly);
		SkySiteUtils.waitForElement(driver, SearchButton_Advance, 60);
		SearchButton_Advance.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(3000);
		return result;

	}
	
	
	public boolean SearchContent(String Anytext) throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		Search_GlobalButton.click();
		Log.message("Global Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Everywhere_AllContents, 120);
		Everywhere_AllContents.click();
		Log.message("Everywhere AllContents Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		SkySiteUtils.waitTill(5000);
		TxtSearch_Keyword.sendKeys(Anytext);
		Log.message("Enter The Value In search Inbox Field: - " + Anytext);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		String file = PhotoMore_file.getText();	
		Log.message("File Get text value:----"+file);
		SkySiteUtils.waitTill(5000);
		if (Anytext.trim().equalsIgnoreCase(file.trim())) {
			Log.message("Text Verified Sucessfully");	
			return  true;
		} else {
			Log.message("Text  Not Verified  Sucessfully");
			return  false;
		}
		

	}
	
	public boolean multipleSearchContent(String Anytext) throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		Search_GlobalButton.click();
		Log.message("Global Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Everywhere_AllContents, 120);
		Everywhere_AllContents.click();
		Log.message("Everywhere AllContents Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		SkySiteUtils.waitTill(5000);
		TxtSearch_Keyword.sendKeys(Anytext);
		Log.message("Enter The Value In search Inbox Field: - " + Anytext);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, ShowEdit, 60);
		SkySiteUtils.waitTill(5000);
		ShowEdit.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Show Edit Linked Clicked Sucessfully");
		String file = WholeTEXT.getText();	
		
		String[] f1 = file.split(" ");
		String myexpected = String.valueOf(f1[4]);
		Log.message("File Get text value:"+myexpected);
		SkySiteUtils.waitTill(5000);
		if (Anytext.trim().contains(myexpected.trim())) {
			Log.message("Text Verified Sucessfully");	
			return  true;
		} else {
			Log.message("Text not Verified Sucessfully");
			return  false;
		}
		

	}
	
	
	public boolean ContentComparision(String Anytext) throws Throwable

	{
		boolean result = true;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		Search_GlobalButton.click();
		Log.message("Global Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Everywhere_AllContents, 120);
		Everywhere_AllContents.click();
		Log.message("Everywhere AllContents Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		SkySiteUtils.waitTill(5000);
		TxtSearch_Keyword.sendKeys(Anytext);
		Log.message("Enter The Value In search Inbox Field: - " + Anytext);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		return result;

	}



	public boolean SearchRFI_RFINumber(String RFINumber) throws Throwable

	{
		boolean result = true;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		Search_GlobalButton.click();
		Log.message("Global Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Everywhere_AllContents, 120);
		Everywhere_AllContents.click();
		Log.message("Everywhere AllContents Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		SkySiteUtils.waitTill(5000);
		TxtSearch_Keyword.sendKeys(RFINumber);
		Log.message("Enter The Value In search Inbox Field: - " + RFINumber);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		return result;

	}

	public boolean SearchProjectWithSubject(String Sub_Name) throws Throwable

	{
		boolean result = true;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Search_GlobalButton, 120);
		Search_GlobalButton.click();
		Log.message("Global Search Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Everywhere_AllContents, 120);
		Everywhere_AllContents.click();
		Log.message("Everywhere AllContents Option Click Sucessfully");
		SkySiteUtils.waitForElement(driver, TxtSearch_Keyword, 60);
		SkySiteUtils.waitTill(5000);
		TxtSearch_Keyword.sendKeys(Sub_Name);
		Log.message("Enter The Value In search Inbox Field: - " + Sub_Name);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		return result;

	}
	
	
	public String gettext_function(String Flag) throws Throwable {
		String value = null;
		SkySiteUtils.waitTill(5000);
		if (Flag.equalsIgnoreCase("Project_Module")) {			
			 value = Dashboard_first_project.getText();
			 Log.message("project Name:------"+ value);
			SkySiteUtils.waitTill(5000);			

		} 		
	
	     else if (Flag.equalsIgnoreCase("folderName_old")) 
	     {		
		 value= folderName_Olddata.getText();
		 Log.message("Folder Name :------ "+ value);
		
	     }
		
	     else if (Flag.equalsIgnoreCase("FileName_old")) 
	     {		
		 value= fileName_Olddata.getText();
		 Log.message("file Name :------ "+ value);
		
	     }
		
	     else if (Flag.equalsIgnoreCase("photo_old")) 
	     {		
		 value= Photo_Olddata.getText();
		 Log.message("Photo Name :------ "+ value);
		
	     }
		return value;}
	
	
	
public boolean gettext_date(String Flag) throws Throwable {	
	boolean result = true;		
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, sendfilelink, 120);		
	sendfilelink.click();
    Log.message("Send Link click Sucessfully");
	SkySiteUtils.waitForElement(driver, TrackingTab, 120);
	SkySiteUtils.waitTill(5000);
	WebElement element2 = TrackingTab;
    JavascriptExecutor executor2 = (JavascriptExecutor)driver;
    executor2.executeScript("arguments[0].click();", element2);
	
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, Filter_Track_DateText, 120);	    			
	String date = Filter_Track_DateText.getText();
	Log.message("Date:"+ date);
	String[] dates = date.split(" ");	
	String date1 = dates[1]+" "+ dates[2]+" "+ dates[3]; 	    		
	SkySiteUtils.waitTill(2000);
	//int TrackDate=Integer.parseInt(date1);
	Log.message("Tracking date is:"+date1);	
	
	SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
	AdvanceSearch_Link.click();
	Log.message("Advance Search Option Clicked Sucessfully");	    		
	SkySiteUtils.waitTill(5000);

	int myDate1 = Integer.parseInt(dates[1]);					
	Log.message("date:== "+myDate1);
	SkySiteUtils.waitForElement(driver, Date_Link, 120);
	Date_Link.click();
	Log.message("Date POPUP Clicked Sucessfully");					
	String dateLoc = String.format("//td[text()='%d' and @class='day']",myDate1);
	Log.message("The locator is:"+dateLoc);
	SkySiteUtils.waitTill(5000);					
	driver.findElement(By.xpath(dateLoc)).click();	
	SkySiteUtils.waitTill(5000);	
    Log.message("Paticular Date Clicked Sucessfully");
   //String dateInbox =driver.findElement(By.xpath(".//*[@id='txtOrderDate_filter']")).getAttribute("value");		    	    
  // Log.message("Date Selected :=="+dateInbox);
	SkySiteUtils.waitTill(10000);
	SkySiteUtils.waitForElement(driver, SearchButton_Advance, 120);		
	SkySiteUtils.waitTill(1000);
	SearchButton_Advance.click();					
	SkySiteUtils.waitTill(5000);   			
	
    Log.message("Search Button Clicked Sucessfully");
    
    String date2 = Filter_Track_DateText.getText();
	Log.message("Date:"+ date2);
	String[] dates1 = date2.split(" ");	
	String date3 = dates1[1]+" "+ dates1[2]+" "+ dates1[3]; 	    		
	SkySiteUtils.waitTill(2000);
    
    if (date.trim().contains(date2.trim())) {
		Log.message("Date Verified Sucessfully");	
		return result = true;
	} else {
		Log.message("Date Not Verified Sucessfully");
		return result = false;
	}
	 
}

}