package com.arc.projects.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arc.projects.utils.randomFileName;
import com.arcautoframe.utils.Log;
import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

public class Gallery_Photo_Page extends LoadableComponent<Gallery_Photo_Page>
{
	WebDriver driver;
	private boolean isPageLoaded;

	ProjectDashboardPage projectDashboardPage;
	
	/**
	 * Identifying web elements using FindBy annotation.
	 */

	@FindBy(css = "#aPrjAddFolder")
	WebElement btnAddNewFolder;
	
	@FindBy(xpath = "(//a[@class='send-folder link-event' and @data-type='folder'])[1]")
	WebElement btnGallerySendLink;
	
	@FindBy(css = "#btnSendLink")
	WebElement btnSendLink;
	
	@FindBy(css = "#txtCopyToClipboard")
	WebElement txtCopyToClipboard;

	@FindBy(css = "#btnAddressBook")
	WebElement btnContact;

	@FindBy(css = "#txtFileSendMessage")
	WebElement txtMessage;

	@FindBy(css = "#btnAddContact")
	WebElement btnSelectContact;
	
	@FindBy(css = ".noty_text")
	WebElement notificationMsg;
	
	//@FindBy(css="#add-folder")  -  Modified on 8th Aug Naresh
	@FindBy(xpath="//i[@class='icon icon-plus-sign icon-lg']")
	WebElement btnAddNewAlbum;
	
	
	@FindBy(css=".btn.btn-default.pull-left")
	WebElement btnCancelAddAlbumWind;
	@FindBy(css="#txtAlbumName")
	WebElement txtAlbumName;
	@FindBy(xpath="(//button[@class='btn btn-default'])[4]")
	WebElement btnCreateAlbum;
	@FindBy(xpath="//button[@class='btn btn-primary' and @data-placement='bottom']")
	WebElement btnModifyAlbum;
	@FindBy(xpath = "(//button[@class='btn btn-primary btn-lg'])[2]")
	WebElement btnUploadPhotos;
	@FindBy(css="#button-1")
	WebElement btnYESAlertWind;
	
	@FindBy(xpath="//button[@class='btn btn-default btnCancel']")
	WebElement btnCancelEditAttributeWind;
	
	@FindBy(xpath="//button[@class='btn btn-primary' and @data-bind='click: SaveAttributes']")
	WebElement btnSaveAttributes;
	
	
	@FindBy(xpath="//button[@class='btn btn-primary' and @data-bind='click: UpdatePhotoAttributes']")
	WebElement btnSaveEditAttributeWind;
	@FindBy(css = ".icon.icon-lg.icon-th-list")
	WebElement iconListView;
	@FindBy(css = ".icon.icon-lg.icon-th-large")
	WebElement iconGridView;

	@FindBy(css = ".nav.navbar-nav.navbar-left>li>input")
	WebElement chkboxAllPhoto;

	@FindBy(css = ".dropdown.has-tooltip>a")
	WebElement btnDropdownMultPhotoSel;
	
	@FindBy(css=".leaflet-draw-draw-photo")
	WebElement Photo_Annotation_Icon;
	
	@FindBy(xpath="//div[@class='image-viewer leaflet-container leaflet-fade-anim']")
	WebElement Viewer_DropPlace;
	
	@FindBy(css="#photo-upload-button")
	WebElement btnUploadPhotoViewer;
	
	@FindBy(css="#back-project-button")
	WebElement btnBackToProjectGallery;
	
	//@FindBy(xpath="//input[@type='file' and @name='qqfile']")//Modified on Aug 8th
	@FindBy(xpath="//button[@id='btnSelectFiles']")
	WebElement btnChooseFile;
	
	@FindBy(css="#btnViewerFileUpload")
	WebElement btnViewerFileUpload;
	
	@FindBy(css="#photoAnnName")
	WebElement editphotoAnnName;
	
	@FindBy(xpath="//button[@data-bind='click: SaveAnnotation, text: BtnAnnoSaveText()']")
	WebElement btnSaveAnnotation;
	
	@FindBy(css="#btnLoadPhoto>a")
	WebElement btnAllAnnotationsDrop;
	
	@FindBy(css=".btn.btn-primary.btn-block.load-all-photo")
	WebElement btnViewAllAnnotations;
	
	@FindBy(xpath="//div[@class='col-sm-11 has-tooltip']")
	WebElement firstAnnotationFromList;
	
	@FindBy(xpath="//*[@id='imageViewer']/div[1]/div[2]/div[3]/div")
	WebElement photoAnnotationRoundSymbal;
	
	@FindBy(css=".pop-attribute-button")
	WebElement btnEditAttribute;
	
	@FindBy(xpath="//button[@class='btn btn-primary tabindex' and @data-bind='click: EditAttr']")
	WebElement btnSaveEditAttrPhotoAnnot;
	
	@FindBy(css=".pop-add-photo-button")
	WebElement btnAddPhotoForPhotoAnnotation;
	
	@FindBy(xpath="//div[@class='image-counter']")
	WebElement btnImgCountPhotoAnnotation;
	
	@FindBy(css=".photo-tap-remove")
	WebElement btnRemovePhoto;
	
	@FindBy(css="#button-0")
	WebElement btnAlertOKRemovePhoto;
	
	
	
	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 20);

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory
	 * class.
	 * @param driver
	 * @return
	 */
	public Gallery_Photo_Page(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	/**
	 * Method written for validate Gallery folder is available
	 * Scipted By: Naresh Babu
	 * 
	 * @return
	 */
	public FolderPage Verify_GalleryFolder_IsAvailable() 
	{
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 20);
		Log.message("Create folder button is available.");
		SkySiteUtils.waitTill(5000);
		// Getting Folder count after created a new folder
		int Avl_Fold_Count = 0;
		int Pass_Count = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) 
		{
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);
		for (int j = 1; j <= Avl_Fold_Count; j++) 
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4")).getText();
			Log.message("Act Name:" + actualFolderName);
			if (actualFolderName.trim().contentEquals("Gallery"))
			{
				Pass_Count = Pass_Count+1;
				break;
			}
		}
			if (Pass_Count==1)
			Log.message("Gallery Folder is available.");
		return new FolderPage(driver).get();
	}
		
	/**
	 * Method written for Gallery Download 
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_Gallery_Download(String Sys_Download_Path, String Project_Name)throws InterruptedException, AWTException, IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 20);
		Log.message("Create folder button is available.");
		// Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		// Getting Folder count after created a new folder
		int Avl_Fold_Count = 0;
		int i = 1;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) 
		{
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: "+Avl_Fold_Count);
		for (i = 1; i <= Avl_Fold_Count; i++) 
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+i+"]/section[2]/h4")).getText();
			Log.message("Act Name:" + actualFolderName);
			if (actualFolderName.trim().contentEquals("Gallery"))
			{
				result1 = true;
				break;
			}
		}
		if(result1==true)
		{
			Log.message("Gallery Folder is available...you can download.");
			driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal media-object icon-lg'])["+i+"]")).click();
			Log.message("Clicked on Gallery folder Drop list.");
			SkySiteUtils.waitTill(2000);
			driver.findElement(By.xpath("(//a[@class='folderDownload' and @data-itype='2'])["+i+"]")).click();
			Log.message("Clicked on Gallery Download tab.");
			SkySiteUtils.waitTill(40000);

			// Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: " + browserName);
			if (browserName.contains("firefox")) 
			{
				// Handling Download PopUp of firefox browser using robot
				Robot robot = null;
				robot = new Robot();

				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(30000);
			}	
			// After Download, checking whether Downloaded file is existed under
			String ActualFilename = null;
			File[] files = new File(Sys_Download_Path).listFiles();

			for (File file : files) 
			{
				if (file.isFile()) 
				{
					ActualFilename = file.getName();// Getting File Names into a variable
					Log.message("Actual File name is:" + ActualFilename);
					Long ActualFileSize = file.length();
					Log.message("Actual File size is:" + ActualFileSize);
					if ((ActualFilename.contains("Gallery-"+Project_Name)) && (ActualFileSize != 0)) 
					{
						Log.message("Gallery Folder is available in downloads!!!");
						result2 = true;
						break;
					}
				}
			}
		}					
		if((result1==true) && (result2==true))
		{
			Log.message("Downloading Gallery Folder is working successfully!!!");
			return true;
		}
		else 
		{
			Log.message("Downloading Gallery Folder is not working!!!");
			return false;
		}	
	}
	
	/**
	 * Method written for Gallery Send Link 
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_Gallery_SendLink()throws InterruptedException, AWTException, IOException 
	{
		boolean result1 = false;
		
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 20);
		Log.message("Create folder button is available.");
		// Getting Folder count after created a new folder
		int Avl_Fold_Count = 0;
		int i = 1;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) 
		{
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: "+Avl_Fold_Count);
		for (i = 1; i <= Avl_Fold_Count; i++) 
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+i+"]/section[2]/h4")).getText();
			Log.message("Act Name:" + actualFolderName);
			if (actualFolderName.trim().contentEquals("Gallery"))
			{
				result1 = true;
				break;
			}
		}
		if(result1==true)
		{
			Log.message("Gallery Folder is available...you can download.");
			driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal media-object icon-lg'])["+i+"]")).click();
			Log.message("Clicked on Gallery folder Drop list.");
			SkySiteUtils.waitTill(2000);
			btnGallerySendLink.click();
			Log.message("Clicked on Gallery Send Link tab.");
			SkySiteUtils.waitForElement(driver, btnSendLink, 30);
			Log.message("Waiting for send link window");
			SkySiteUtils.waitTill(5000);
			String Act_SendLinkURL = txtCopyToClipboard.getAttribute("value");// Copy the URL
			Log.message("Generated Send Link is: " + Act_SendLinkURL);
			btnContact.click();
			Log.message("Clicked on contact-address book.");
			SkySiteUtils.waitForElement(driver, btnSelectContact, 30);
			Log.message("Waiting for Select Contact window");
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath("(//input[@class='pull-left chklist'])[1]")).click();// Select 1st contact check box 
			btnSelectContact.click();// Click on select button
			SkySiteUtils.waitTill(2000);
			txtMessage.click();
			txtMessage.sendKeys("Testing from QA_Team @Automation");
			btnSendLink.click();// Click on send
			SkySiteUtils.waitTill(1000);
			SkySiteUtils.waitForElement(driver, notificationMsg, 30);
			Log.message("Waiting for notify message could display");
			String NotMsg_SendLink = notificationMsg.getText();
			Log.message("Notification message after Gallery send link is: "+NotMsg_SendLink);
			SkySiteUtils.waitTill(5000);
			
			//Opening send folder Link in the browser
			driver.get(Act_SendLinkURL);
			SkySiteUtils.waitTill(2000);
			String Expected_WindowTitle="Download Gallery - SKYSITE Projects";
			String Actual_WindowTitle=driver.getTitle();
			System.out.println("Actual Page title is:"+Actual_WindowTitle);
			if ((NotMsg_SendLink.contentEquals("Link successfully sent")) && (Actual_WindowTitle.equals(Expected_WindowTitle)))
			{
				Log.message("Gallery sendlink is done successfully");
				result1=true;
			}
		}
		if(result1==true)
		{
			Log.message("Gallery sendlink is working successfully!!!");
			return true;
		}
		else
		{
			Log.message("Gallery sendlink is NOT working!!!");
			return false;
		}
	}

	/**
	 * Method written for Add Album
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_Add_Album(String Album_Name)throws InterruptedException, AWTException, IOException 
	{
		boolean result1 = false;
		
		SkySiteUtils.waitForElement(driver, btnAddNewAlbum, 20);
		Log.message("Add New Album button is available.");
		btnAddNewAlbum.click();
		Log.message("Click on Add New Album");
		SkySiteUtils.waitForElement(driver, btnCancelAddAlbumWind, 20);
		txtAlbumName.click();
		txtAlbumName.sendKeys(Album_Name);
		Log.message("Entered Album Name.");
		btnCreateAlbum.click();
		Log.message("Clicked on Create Album button.");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, notificationMsg, 20);
		String ActualMsg = notificationMsg.getText();
		Log.message("Notify message after create an Album is: "+ActualMsg);
		SkySiteUtils.waitTill(10000);
		//Counting available Albums
		int Album_Count=0;
		int i = 1;
		List<WebElement> allElements = driver.findElements(By.xpath("//section[1]/i"));
		for (WebElement Element : allElements)
		{ 
			Album_Count = Album_Count+1; 	
		}
		Log.message("Available Albums in a gallery is: "+Album_Count);	
		Album_Count=Album_Count+(Album_Count-1);
		Log.message("Album Count after added more is: "+Album_Count);
		//Getting Album Names and matching
		for(i = 1;i<=Album_Count;i++)
		{
			String Act_AlbumName = driver.findElement(By.xpath("//li["+i+"]/section[2]/h4")).getText();
			Log.message("Expected Album After create is: "+Act_AlbumName);
			if((Act_AlbumName.contentEquals(Album_Name)) && (ActualMsg.contentEquals("Album successfully created.")))
			{
				result1=true;
				break;
			}		
		}
		if(result1==true)
		{
			Log.message("Add Album is working successfully!!!");
			return true;
		}
		else
		{
			Log.message("Add Album is NOT working!!!");
			return false;
		}
	}
	
	/**
	 * Method written for Select an Album
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_SelectAn_Album(String Album_Name)throws InterruptedException, AWTException, IOException 
	{
		boolean result1 = false;
		
		SkySiteUtils.waitForElement(driver, btnAddNewAlbum, 20);
		Log.message("Add New Album button is available.");
		//Counting available Albums
		int Album_Count=0;
		int i = 1;
		List<WebElement> allElements = driver.findElements(By.xpath("//section[1]/i"));
		for (WebElement Element : allElements)
		{ 
			Album_Count = Album_Count+1; 	
		}
		Log.message("Available Albums in a gallery is: "+Album_Count);
		Album_Count=Album_Count+(Album_Count-1);
		Log.message("Album Count after added more is: "+Album_Count);
		//Getting Album Names and matching
		for(i = 1;i<=Album_Count;i++)
		{
			String Act_AlbumName = driver.findElement(By.xpath("//li["+i+"]/section[2]/h4")).getText();
			if(Act_AlbumName.contentEquals(Album_Name))
			{
				result1=true;
				break;
			}		
		}
		if(result1==true)
		{
			driver.findElement(By.xpath("//li["+i+"]/section[2]/h4")).click();
			SkySiteUtils.waitForElement(driver, btnUploadPhotos, 30);
			Log.message("Expected Album is selected successfully!!!");
			return true;
		}
		else
		{
			Log.message("Expected Album is not available!!!");
			return false;
		}
	}
	
	/**
	 * Method written for Album Download 
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_Album_Download(String Sys_Download_Path, String Album_Name)throws InterruptedException, AWTException, IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		
		SkySiteUtils.waitForElement(driver, btnAddNewAlbum, 20);
		Log.message("Add New Album button is available.");
		// Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		//Counting available Albums
		int Album_Count=0;
		int i = 1;
		List<WebElement> allElements = driver.findElements(By.xpath("//section[1]/i"));
		for (WebElement Element : allElements)
		{ 
			Album_Count = Album_Count+1; 	
		}
		Log.message("Available Albums in a gallery is: "+Album_Count);		
		//Getting Album Names and matching
		for(i = 1;i<=Album_Count;i++)
		{
			String Act_AlbumName = driver.findElement(By.xpath("html/body/div[1]/div[4]/div[2]/div/div/div/ul/li["+i+"]/section[2]/h4")).getText();
			if(Act_AlbumName.contentEquals(Album_Name))
			{
				result1=true;
				break;
			}	
			
		}
		if(result1==true)
		{
			Log.message("Expected Album is available...you can download.");
			driver.findElement(By.xpath("html/body/div[1]/div[4]/div[2]/div/div/div/ul/li["+i+"]/section[4]/ul/li[4]/a/i")).click();
			Log.message("Click on Album droplist.");
			SkySiteUtils.waitTill(2000);
			WebElement Album_Download = driver.findElement(By.xpath("(//a[contains(text(),'Download')])["+i+"]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",Album_Download);
			Log.message("Click on Album Download tab.");
			SkySiteUtils.waitTill(30000);

			// Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: " + browserName);
			if (browserName.contains("firefox")) 
			{
				// Handling Download PopUp of firefox browser using robot
				Robot robot = null;
				robot = new Robot();

				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(30000);
			}	
			// After Download, checking whether Downloaded file is existed under
			String ActualFilename = null;
			File[] files = new File(Sys_Download_Path).listFiles();

			for (File file : files) 
			{
				if (file.isFile()) 
				{
					ActualFilename = file.getName();// Getting File Names into a variable
					Log.message("Actual File name is:" + ActualFilename);
					Long ActualFileSize = file.length();
					Log.message("Actual File size is:" + ActualFileSize);
					if ((ActualFilename.contains(Album_Name)) && (ActualFileSize != 0)) 
					{
						Log.message("Album is available in downloads!!!");
						result2 = true;
						break;
					}
				}
			}
		}					
		if((result2==true) && (result2==true))
		{
			Log.message("Downloading Album Folder is working successfully!!!");
			return true;
		}
		else 
		{
			Log.message("Downloading Album Folder is not working!!!");
			return false;
		}	
	}
	
	/**
	 * Method written for Album Send Link 
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_Album_SendLink(String Album_Name)throws InterruptedException, AWTException, IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		
		SkySiteUtils.waitForElement(driver, btnAddNewAlbum, 20);
		Log.message("Add New Album button is available.");
		//Counting available Albums
		int Album_Count=0;
		int i = 1;
		List<WebElement> allElements = driver.findElements(By.xpath("//section[1]/i"));
		for (WebElement Element : allElements)
		{ 
			Album_Count = Album_Count+1; 	
		}
		Log.message("Available Albums in a gallery is: "+Album_Count);		
		//Getting Album Names and matching
		for(i = 1;i<=Album_Count;i++)
		{
			String Act_AlbumName = driver.findElement(By.xpath("html/body/div[1]/div[4]/div[2]/div/div/div/ul/li["+i+"]/section[2]/h4")).getText();
			if(Act_AlbumName.contentEquals(Album_Name))
			{
				result1=true;
				break;
			}	
			
		}
		if(result1==true)
		{
			Log.message("Expected Album is available...you can download.");
			driver.findElement(By.xpath("html/body/div[1]/div[4]/div[2]/div/div/div/ul/li["+i+"]/section[4]/ul/li[4]/a/i")).click();
			Log.message("Click on Album droplist.");
			SkySiteUtils.waitTill(2000);
			WebElement Album_Download = driver.findElement(By.xpath("(//a[contains(text(),'Send')])["+i+"]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",Album_Download);
			Log.message("Clicked on Album Send Link tab.");
			SkySiteUtils.waitForElement(driver, btnSendLink, 30);
			Log.message("Waiting for send link window");
			SkySiteUtils.waitTill(5000);
			String Act_SendLinkURL = txtCopyToClipboard.getAttribute("value");// Copy the URL
			Log.message("Generated Send Link is: " + Act_SendLinkURL);
			btnContact.click();
			Log.message("Clicked on contact-address book.");
			SkySiteUtils.waitForElement(driver, btnSelectContact, 30);
			Log.message("Waiting for Select Contact window");
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath("(//input[@class='pull-left chklist'])[1]")).click();// Select 1st contact check box 
			btnSelectContact.click();// Click on select button
			SkySiteUtils.waitTill(2000);
			txtMessage.click();
			txtMessage.sendKeys("Testing from QA_Team @Automation");
			btnSendLink.click();// Click on send
			SkySiteUtils.waitTill(1000);
			SkySiteUtils.waitForElement(driver, notificationMsg, 30);
			Log.message("Waiting for notify message could display");
			String NotMsg_SendLink = notificationMsg.getText();
			Log.message("Notification message after Album send link is: "+NotMsg_SendLink);
			SkySiteUtils.waitTill(5000);
			
			//Opening send folder Link in the browser
			driver.get(Act_SendLinkURL);
			SkySiteUtils.waitTill(2000);
			String Expected_WindowTitle="Download Album - SKYSITE Projects";
			String Actual_WindowTitle=driver.getTitle();
			System.out.println("Actual Page title is:"+Actual_WindowTitle);
			if ((NotMsg_SendLink.contentEquals("Link successfully sent")) && (Actual_WindowTitle.equals(Expected_WindowTitle)))
			{
				Log.message("Album sendlink is done successfully");
				result2=true;
			}
		}
		if((result1==true)&&(result2==true))
		{
			Log.message("Album sendlink is working successfully!!!");
			return true;
		}
		else
		{
			Log.message("Album sendlink is NOT working!!!");
			return false;
		}
	}
	
	/**
	 * Method written for Album Edit 
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_Album_Edit(String Album_Name, String Edit_Album_Name)throws InterruptedException, AWTException, IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		
		SkySiteUtils.waitForElement(driver, btnAddNewAlbum, 20);
		Log.message("Add New Album button is available.");
		//Counting available Albums
		int Album_Count=0;
		int i = 1;
		List<WebElement> allElements = driver.findElements(By.xpath("//section[1]/i"));
		for (WebElement Element : allElements)
		{ 
			Album_Count = Album_Count+1; 	
		}
		Log.message("Available Albums in a gallery is: "+Album_Count);
		Album_Count=Album_Count+(Album_Count-1);
		Log.message("Album Count after added more is: "+Album_Count);
		//Getting Album Names and matching
		for(i = 1;i<=Album_Count;i++)
		{
			String Act_AlbumName = driver.findElement(By.xpath("//li["+i+"]/section[2]/h4")).getText();
			Log.message("Expected Album name is: "+Act_AlbumName);
			if(Act_AlbumName.contentEquals(Album_Name))
			{
				result1=true;
				break;
			}	
			
		}
		if(result1==true)
		{
			//i=i+(i-1);
			Log.message("Expected Album is available...you can Edit.");
			driver.findElement(By.xpath("//li["+i+"]/section[4]/ul/li[4]/a/i")).click();
			Log.message("Click on Album droplist.");
			SkySiteUtils.waitTill(2000);
			driver.findElement(By.xpath("(//a[@class='edit-properties'])["+i+"]")).click();
			Log.message("Clicked on Album Edit tab.");
			SkySiteUtils.waitForElement(driver, btnCancelAddAlbumWind, 30);
			Log.message("Waiting for Modify Album window");
			SkySiteUtils.waitTill(2000);
			txtAlbumName.clear();
			txtAlbumName.sendKeys(Edit_Album_Name);
			Log.message("Entered new Album name in edit album field.");
			btnModifyAlbum.click();
			Log.message("Clicked on Modify Album button.");
			SkySiteUtils.waitTill(1000);
			SkySiteUtils.waitForElement(driver, notificationMsg, 30);
			String NotMsg_ModifyAlbum = notificationMsg.getText();
			Log.message("Notification message after Edit Album is: "+NotMsg_ModifyAlbum);
			SkySiteUtils.waitTill(5000);
			
			//Identify an Album after edit
			for(i = 1;i<=Album_Count;i++)
			{
				String Act_EditAlbumName = driver.findElement(By.xpath("//li["+i+"]/section[2]/h4")).getText();
				if((Act_EditAlbumName.contentEquals(Edit_Album_Name)) && (NotMsg_ModifyAlbum.contentEquals("Album successfully updated.")))
				{
					result2=true;
					break;
				}	
				
			}
			
		}
		if((result1==true)&&(result2==true))
		{
			Log.message("Album Edit is working successfully!!!");
			return true;
		}
		else
		{
			Log.message("Album Edit is NOT working!!!");
			return false;
		}
	}
	
	/**
	 * Method written for Album Delete 
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_Album_Delete(String Edit_Album_Name)throws InterruptedException, AWTException, IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		
		SkySiteUtils.waitForElement(driver, btnAddNewAlbum, 20);
		Log.message("Add New Album button is available.");
		//Counting available Albums
		int Album_Count=0;
		int i = 1;
		List<WebElement> allElements = driver.findElements(By.xpath("//section[1]/i"));
		for (WebElement Element : allElements)
		{ 
			Album_Count = Album_Count+1; 	
		}
		Log.message("Available Albums in a gallery is: "+Album_Count);
		Album_Count=Album_Count+(Album_Count-1);
		Log.message("Album Count after added more is: "+Album_Count);
		//Getting Album Names and matching
		for(i = 1;i<=Album_Count;i++)
		{
			String Act_AlbumName = driver.findElement(By.xpath("//li["+i+"]/section[2]/h4")).getText();
			Log.message("Expected Album to be delete is: "+Act_AlbumName);
			if(Act_AlbumName.contentEquals(Edit_Album_Name))
			{
				result1=true;
				break;
			}	
			
		}
		if(result1==true)
		{
			Log.message("Expected Album is available...you can Delete.");
			driver.findElement(By.xpath("//li["+i+"]/section[4]/ul/li[4]/a/i")).click();
			Log.message("Click on Album droplist.");
			SkySiteUtils.waitTill(2000);
			WebElement Album_Delete = driver.findElement(By.xpath("(//a[contains(text(),'Delete')])["+i+"]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",Album_Delete);
			Log.message("Clicked on Album Delete tab.");
			SkySiteUtils.waitTill(2000);
			SkySiteUtils.waitForElement(driver, btnYESAlertWind, 30);
			btnYESAlertWind.click();
			SkySiteUtils.waitTill(2000);
			SkySiteUtils.waitForElement(driver, notificationMsg, 30);
			String NotMsg_DeleteAlbum = notificationMsg.getText();
			Log.message("Notification message after Delete Album is: "+NotMsg_DeleteAlbum);
			//SkySiteUtils.waitTill(5000);
			if((NotMsg_DeleteAlbum.contentEquals("Album '"+Edit_Album_Name+"' deleted successfully")))
			{
				result2=true;
			}	
			
		}
		if((result1==true)&&(result2==true))
		{
			Log.message("Album Delete is working successfully!!!");
			return true;
		}
		else
		{
			Log.message("Album Delete is NOT working!!!");
			return false;
		}
	}
	
	/**
	 * Method written for Download Photo from gallery
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_Photo_DownloadOfGallery(String Sys_Download_Path, String Photo_Name)throws InterruptedException, AWTException, IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		//Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		//Counting available Photos
		int Photo_Count=0;
		int i = 1;
		List<WebElement> allElements = driver.findElements(By.xpath("//section[4]/ul/li[5]/a/i"));
		for (WebElement Element : allElements)
		{ 
			Photo_Count = Photo_Count+1; 	
		}
		System.out.println("Available Photos in Gallery is: "+Photo_Count);
		
		int j=2;
		for(i=1;i<=Photo_Count;i++)
		{
			String Act_PhotoName = driver.findElement(By.xpath("//li["+j+"]/section[2]/h4/span[1]")).getText();		
			Log.message("Photo name from Album is: "+Act_PhotoName);
			if(Act_PhotoName.contentEquals(Photo_Name))
			{
				result1=true;
				Log.message("Expected Photo is available in Gallery");
				break;
			}
			j=j+2;
		}
		if(result1==true)
		{
			SkySiteUtils.waitTill(3000);
			driver.findElement(By.xpath("(//section[4]/ul/li[5]/a/i)["+i+"]")).click();
			Log.message("Clicked on Expected photo droplist.");
			SkySiteUtils.waitTill(2000);	
			WebElement Photo_Download = driver.findElement(By.xpath("(//a[contains(text(),'Download')])["+j+"]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",Photo_Download);
			Log.message("Clicked on Downlaod option.");
			SkySiteUtils.waitTill(25000);

			// Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: " + browserName);
			if (browserName.contains("firefox")) 
			{
				// Handling Download PopUp of firefox browser using robot
				Robot robot = null;
				robot = new Robot();

				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(30000);
			}	
			// After Download, checking whether Downloaded file is existed under
			String ActualPhotoname = null;
			File[] files = new File(Sys_Download_Path).listFiles();

			for (File file : files) 
			{
				if (file.isFile()) 
				{
					ActualPhotoname = file.getName();//Getting File Names into a variable
					Log.message("Actual Photo name is:" + ActualPhotoname);
					Long ActualFileSize = file.length();
					Log.message("Actual Photo size is:" + ActualFileSize);
					if ((ActualPhotoname.contains(Photo_Name)) && (ActualFileSize != 0)) 
					{
						Log.message("Photo is available in downloads!!!");
						result2 = true;
						break;
					}
				}
			}
			
		}
		if((result1==true)&&(result2==true))
		{
			Log.message("Expected Photo Download is working successfully!!!");
			return true;
		}
		else
		{
			Log.message("Expected Photo Download is NOT working!!!");
			return false;
		}
		
	}
	
	/**
	 * Method written for Send Link of a Photo and download
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_Photo_SendLink_And_Download(String Sys_Download_Path, String Photo_Name)throws InterruptedException, AWTException, IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		//Calling "Deleting download folder files" method
		SkySiteUtils.waitTill(5000);
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		//Counting available Photos
		int Photo_Count=0;
		int i = 1;
		List<WebElement> allElements = driver.findElements(By.xpath("//section[4]/ul/li[5]/a/i"));
		for (WebElement Element : allElements)
		{ 
			Photo_Count = Photo_Count+1; 	
		}
		System.out.println("Available Photos in Gallery is: "+Photo_Count);
		
		int j=2;
		for(i=1;i<=Photo_Count;i++)
		{
			String Act_PhotoName = driver.findElement(By.xpath("//li["+j+"]/section[2]/h4/span[1]")).getText();		
			Log.message("Photo name from Album is: "+Act_PhotoName);
			if(Act_PhotoName.contentEquals(Photo_Name))
			{
				result1=true;
				Log.message("Expected Photo is available in Gallery");
				break;
			}
			j=j+2;
		}
		if(result1==true)
		{	
			Log.message("Expected Photo is available....You can do Sendlink");
			driver.findElement(By.xpath("(//section[4]/ul/li[5]/a/i)["+i+"]")).click();
			Log.message("Clicked on Expected photo droplist.");
			SkySiteUtils.waitTill(2000);	
			WebElement Photo_SendLink = driver.findElement(By.xpath("(//a[contains(text(),'Send')])["+j+"]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",Photo_SendLink);
			Log.message("Clicked on Send Link option.");
			SkySiteUtils.waitForElement(driver, btnSendLink, 30);
			Log.message("Waiting for send link window");
			SkySiteUtils.waitTill(5000);
			String Act_SendLinkURL = txtCopyToClipboard.getAttribute("value");// Copy the URL
			Log.message("Generated Send Link is: " + Act_SendLinkURL);
			btnContact.click();
			Log.message("Clicked on contact-address book.");
			SkySiteUtils.waitForElement(driver, btnSelectContact, 30);
			Log.message("Waiting for Select Contact window");
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath("(//input[@class='pull-left chklist'])[1]")).click();// Select 1st contact check box 
			btnSelectContact.click();// Click on select button
			SkySiteUtils.waitTill(2000);
			txtMessage.click();
			txtMessage.sendKeys("Testing from QA_Team @Automation");
			btnSendLink.click();// Click on send
			SkySiteUtils.waitTill(1000);
			SkySiteUtils.waitForElement(driver, notificationMsg, 50);
			Log.message("Waiting for notify message could display");
			String NotMsg_SendLink = notificationMsg.getText();
			Log.message("Notification message after Album send link is: "+NotMsg_SendLink);
			SkySiteUtils.waitTill(5000);
			
			//Opening send folder Link in the browser
			driver.get(Act_SendLinkURL);
			SkySiteUtils.waitTill(2000);
			String Expected_WindowTitle="Download - SKYSITE Projects";
			String Actual_WindowTitle=driver.getTitle();
			System.out.println("Actual Page title is:"+Actual_WindowTitle);
			if ((NotMsg_SendLink.contentEquals("Link successfully sent")) && (Actual_WindowTitle.equals(Expected_WindowTitle)))
			{
				Log.message("Photo sendlink is done successfully");
				result2=true;
			}

		}
		if((result1==true)&&(result2==true))
		{
			Log.message("Expected Photo Download is working successfully!!!");
			return true;
		}
		else
		{
			Log.message("Expected Photo Download is NOT working!!!");
			return false;
		}
	}
	
	/**
	 * Method written for Photo Edit Attributes under Gallery
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_Photo_EditAttributes(String Photo_Name, String Edit_PhotoName)throws InterruptedException, AWTException, IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;
		//Counting available Photos
		SkySiteUtils.waitTill(3000);
		int Photo_Count=0;
		int i = 1;
		List<WebElement> allElements = driver.findElements(By.xpath("//section[4]/ul/li[5]/a/i"));
		for (WebElement Element : allElements)
		{ 
			Photo_Count = Photo_Count+1; 	
		}
		System.out.println("Available Photos in Gallery is: "+Photo_Count);
		
		int j=2;
		for(i=1;i<=Photo_Count;i++)
		{
			String Act_PhotoName = driver.findElement(By.xpath("//li["+j+"]/section[2]/h4/span[1]")).getText();		
			Log.message("Photo name from Album is: "+Act_PhotoName);
			if(Act_PhotoName.contentEquals(Photo_Name))
			{
				result1=true;
				Log.message("Expected Photo is available in Gallery");
				break;
			}
			j=j+2;
		}
		if(result1==true)
		{	
			Log.message("Expected Photo is available....You can Edit");
			driver.findElement(By.xpath("(//section[4]/ul/li[5]/a/i)["+i+"]")).click();
			Log.message("Clicked on Expected photo droplist.");
			SkySiteUtils.waitTill(2000);	
			WebElement EditAttributes_Photo = driver.findElement(By.xpath("(//a[contains(text(),'Edit attributes')])"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",EditAttributes_Photo);
			Log.message("Clicked on Edit Attributes option.");
			SkySiteUtils.waitForElement(driver, btnCancelEditAttributeWind, 30);
			SkySiteUtils.waitTill(3000);
			driver.findElement(By.xpath("//input[@id='txtImageName']")).clear();//Clear name
			driver.findElement(By.xpath("//input[@id='txtImageName']")).sendKeys(Edit_PhotoName);
			driver.findElement(By.xpath("(//input[@placeholder='Building'])[2]")).clear();
			driver.findElement(By.xpath("(//input[@placeholder='Building'])[2]")).sendKeys("Building_Name");
			driver.findElement(By.xpath("(//input[@placeholder='Level'])[2]")).clear();
			driver.findElement(By.xpath("(//input[@placeholder='Level'])[2]")).sendKeys("Level-1");
			driver.findElement(By.xpath("(//input[@placeholder='Room'])[2]")).clear();
			driver.findElement(By.xpath("(//input[@placeholder='Room'])[2]")).sendKeys("Room-2");
			driver.findElement(By.xpath("(//input[@placeholder='Area'])[2]")).clear();
			driver.findElement(By.xpath("(//input[@placeholder='Area'])[2]")).sendKeys("Kolkata");
			driver.findElement(By.xpath("(//input[@placeholder='Description'])[2]")).clear();
			driver.findElement(By.xpath("(//input[@placeholder='Description'])[2]")).sendKeys("Modified-plan");
			btnSaveEditAttributeWind.click();
			Log.message("Clicked on Save button");
			SkySiteUtils.waitTill(1000);
			SkySiteUtils.waitForElement(driver, notificationMsg, 30);
			String NotMsg_SendLink = notificationMsg.getText();
			Log.message("Notification message after Album send link is: "+NotMsg_SendLink);
			SkySiteUtils.waitTill(5000);
			//Validate after Edited the attributes
			String Modified_PhotoName = driver.findElement(By.xpath("//li["+j+"]/section[2]/h4/span[1]")).getText();		
			System.out.println("Photo name After Edit is: "+Modified_PhotoName);
			if(Modified_PhotoName.contentEquals(Edit_PhotoName))
			{
				result2=true;
				Log.message("Photo name is Edited Successfully from: "+Photo_Name+" To: "+Modified_PhotoName);
				SkySiteUtils.waitTill(2000);	
				driver.findElement(By.xpath("(//section[4]/ul/li[5]/a/i)["+i+"]")).click();
				Log.message("Clicked on Expected photo droplist.");
				SkySiteUtils.waitTill(2000);	
				WebElement EditAttributes_Photo2 = driver.findElement(By.xpath("(//a[contains(text(),'Edit attributes')])"));
				((JavascriptExecutor) driver).executeScript("arguments[0].click();",EditAttributes_Photo2);
				SkySiteUtils.waitTill(2000);
				Log.message("Clicked on Edit Attributes option.");
				SkySiteUtils.waitForElement(driver, btnCancelEditAttributeWind, 30);
				SkySiteUtils.waitTill(3000);
				//Get the existed Attribute values 
				String Act_PhotoName=driver.findElement(By.xpath("//input[@id='txtImageName']")).getAttribute("value");
				System.out.println(Act_PhotoName);
				String Act_Building=driver.findElement(By.xpath("(//input[@placeholder='Building'])[2]")).getAttribute("value");
				System.out.println(Act_Building);
				String Act_Level=driver.findElement(By.xpath("(//input[@placeholder='Level'])[2]")).getAttribute("value");
				System.out.println(Act_Level);
				String Act_Room=driver.findElement(By.xpath("(//input[@placeholder='Room'])[2]")).getAttribute("value");
				System.out.println(Act_Room);
				String Act_Area=driver.findElement(By.xpath("(//input[@placeholder='Area'])[2]")).getAttribute("value");
				System.out.println(Act_Area);
				String Act_Description=driver.findElement(By.xpath("(//input[@placeholder='Description'])[2]")).getAttribute("value");
				System.out.println(Act_Description);
				SkySiteUtils.waitTill(2000);
				//Comparing with Expected Attribute values
				if((Act_PhotoName.contentEquals(Edit_PhotoName)) && (Act_Building.contentEquals("Building_Name")) && (Act_Level.contentEquals("Level-1"))
						&& (Act_Room.contentEquals("Room-2")) && (Act_Area.contentEquals("Kolkata")) && (Act_Description.contentEquals("Modified-plan")))
				{
					result3=true;
					Log.message("Edited Attributes for photo is success!!!");
					btnCancelEditAttributeWind.click();
					Log.message("Clicked on Close button of edit window");
				}
				else
				{
					result3=false;
					Log.message("Edited Attributes for photo is Failed!!!");
				}
			}

		}
		if((result1==true)&&(result2==true)&&(result3==true))
		{
			Log.message("Expected Photo Edit working successfully!!!");
			return true;
		}
		else
		{
			Log.message("Expected Photo Edit is NOT working!!!");
			return false;
		}
		
	}
	
	/**
	 * Method written for Photo Delete under Gallery
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_Photo_Delete(String Photo_Name)throws InterruptedException, AWTException, IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		SkySiteUtils.waitTill(3000);
		//Counting available Photos
		int Photo_Count=0;
		int i = 1;
		List<WebElement> allElements = driver.findElements(By.xpath("//section[4]/ul/li[5]/a/i"));
		for (WebElement Element : allElements)
		{ 
			Photo_Count = Photo_Count+1; 	
		}
		System.out.println("Available Photos in Gallery is: "+Photo_Count);
		
		int j=2;
		for(i=1;i<=Photo_Count;i++)
		{
			String Act_PhotoName = driver.findElement(By.xpath("//li["+j+"]/section[2]/h4/span[1]")).getText();		
			Log.message("Photo name from Album is: "+Act_PhotoName);
			if(Act_PhotoName.contentEquals(Photo_Name))
			{
				result1=true;
				Log.message("Expected Photo is available in Gallery");
				break;
			}
			j=j+2;
		}
		if(result1==true)
		{	
			Log.message("Expected Photo is available....You can Edit");
			driver.findElement(By.xpath("(//section[4]/ul/li[5]/a/i)["+i+"]")).click();
			Log.message("Clicked on Expected photo droplist.");
			SkySiteUtils.waitTill(2000);	
			WebElement Photo_Delete = driver.findElement(By.xpath("(//a[contains(text(),'Delete')])["+j+"]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",Photo_Delete);
			SkySiteUtils.waitTill(1000);
			SkySiteUtils.waitForElement(driver, btnYESAlertWind, 20);
			btnYESAlertWind.click();
			SkySiteUtils.waitTill(1000);
			SkySiteUtils.waitForElement(driver, notificationMsg, 30);
			String NotMsg_DeletePhoto = notificationMsg.getText();
			Log.message("Notification message after Delete Photo is: "+NotMsg_DeletePhoto);
			SkySiteUtils.waitTill(5000);
			
			if((driver.findElement(By.xpath("(//button[@class='btn btn-primary btn-lg'])[2]")).isDisplayed())
					&&(NotMsg_DeletePhoto.contentEquals("Photo '"+Photo_Name+"' deleted successfully")))
			{
				result2=true;
				Log.message("Photo deleted successfully: "+Photo_Name);	
			}
			else
			{
				result2=false;
				Log.message("Photo delete Failed: "+Photo_Name);	
			}

		}
		if((result1==true)&&(result2==true))
		{
			Log.message("Expected Photo Delete working successfully!!!");
			return true;
		}
		else
		{
			Log.message("Expected Photo Delete is NOT working!!!");
			return false;
		}
		
	}
	
	/**
	 * Method written for Select multiple Photos and Delete
	 * Scipted By: Naresh Babu
	 * @return
	 */

	public boolean Verify_Delete_BySelectMultPhotos()
			throws InterruptedException, AWTException 
	{
		SkySiteUtils.waitForElement(driver, iconListView, 30);
		Log.message("Waiting for List View button to be appeared");
		SkySiteUtils.waitTill(5000);
		iconListView.click();
		Log.message("Clicked on List View button.");	
		SkySiteUtils.waitForElement(driver, chkboxAllPhoto, 30);
		Log.message("Waiting for select all photos checkbox to be appeared");
		// Counting number of files available
		int Avl_File_Count = 0;
		int PhotoCount_AfterDelete = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'media navbar-collapse pw-photo')]"));
		for (WebElement Element : allElements) 
		{
			Avl_File_Count = Avl_File_Count + 1;
		}
		Log.message("Availble number of photos in folder is: " + Avl_File_Count);
		SkySiteUtils.waitTill(2000);
		// Select first two files from the list
		if (Avl_File_Count >= 2) 
		{
			for (int i = 1; i <= 2; i++) 
			{
				// Log.message("Select a checkbox");
	driver.findElement(By
	.xpath("(//input[@type='checkbox' and @data-bind='click: ItemChkboxClick, clickBubble: false, checked: IsChecked'])["+i+"]")).click();
			}
		}
		SkySiteUtils.waitTill(2000);
		btnDropdownMultPhotoSel.click();
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//i[@class='icon icon-ellipsis-horizontal media-object icon-lg' and @data-placement='left']/../following-sibling::ul/li[3]//span")).click();
		Log.message("Clicked on Delete Tab for the Photos.");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, btnYESAlertWind, 20);
		btnYESAlertWind.click();
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, notificationMsg, 30);
		String NotMsg_DeletePhoto = notificationMsg.getText();
		Log.message("Notification message after Delete Photo is: "+NotMsg_DeletePhoto);
		SkySiteUtils.waitTill(5000);
		//Getting photo count after delete some photos
		List<WebElement> allElements1 = driver.findElements(By.xpath("//*[contains(@class, 'media navbar-collapse pw-photo')]"));
		for (WebElement Element : allElements1) 
		{
			PhotoCount_AfterDelete = PhotoCount_AfterDelete + 1;
		}
		Log.message("Availble photo count after delete some photos is: " + PhotoCount_AfterDelete);
		
		iconGridView.click();
		Log.message("Clicked on Grid View.");
		SkySiteUtils.waitTill(5000);

		if ((NotMsg_DeletePhoto.contentEquals("Photo(s) deleted successfully")) && ((Avl_File_Count-2)==PhotoCount_AfterDelete)) 
		{
			Log.message("Multiple photo select and Delete is working.");
			return true;
		} else {
			Log.message("Multiple photo select and Delete is NOT working.");
			return false;
		}

	}
	
	/**
	 * Method written for Album Edit Attributes
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_Album_Edit_Attributes(String Album_Name, int PhotoCount)throws InterruptedException, AWTException, IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		
		String Building_Name = "Building_"+Generate_Random_Number.generateRandomValue();
		String Level = "Level_"+Generate_Random_Number.generateRandomValue();
		String Room = "Room_"+Generate_Random_Number.generateRandomValue();
		String Area = "Area_"+Generate_Random_Number.generateRandomValue();
		String Description = "Description_"+Generate_Random_Number.generateRandomValue();
		String Act_Building = null;
		String Act_Level = null;
		String Act_Room = null;
		String Act_Area = null;
		String Act_Description = null;
		int PassCount = 0;
		
		SkySiteUtils.waitForElement(driver, btnAddNewAlbum, 20);
		Log.message("Add New Album button is available.");
		//Counting available Albums
		int Album_Count=0;
		int i = 1;
		int n = 1;
		List<WebElement> allElements = driver.findElements(By.xpath("//section[1]/i"));
		for (WebElement Element : allElements)
		{ 
			Album_Count = Album_Count+1; 	
		}
		Log.message("Available Albums in a gallery is: "+Album_Count);
		Album_Count=Album_Count+(Album_Count-1);
		Log.message("Album Count after added more is: "+Album_Count);
		//Getting Album Names and matching
		for(n = 1;n<=Album_Count;n++)
		{
			String Act_AlbumName = driver.findElement(By.xpath("//li["+n+"]/section[2]/h4")).getText();
			Log.message("Expected Album name is: "+Act_AlbumName);
			if(Act_AlbumName.contentEquals(Album_Name))
			{
				result1=true;
				break;
			}	
			
		}
		if(result1==true)
		{
			Log.message("Expected Album is available...you can do Edit Attributes.");
			driver.findElement(By.xpath("//li["+n+"]/section[4]/ul/li[4]/a/i")).click();
			Log.message("Click on Album droplist.");
			SkySiteUtils.waitTill(2000);
			int Req_Val = n-1;
			WebElement Photo_Delete = driver.findElement(By.xpath("(//a[contains(text(),'Edit photo attributes')])["+Req_Val+"]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",Photo_Delete);
			Log.message("Clicked on Album Edit Attributes tab.");
			SkySiteUtils.waitForElement(driver, btnSaveAttributes, 30);
			SkySiteUtils.waitTill(3000);
			//Validations on Add Attributes page
			String Photo_Information = driver.findElement(By.xpath("//h4[@class='pull-left']")).getText();//Getting count
			System.out.println(Photo_Information);
			// Split the line and get the count
			Splitter SPLITTER = Splitter.on(CharMatcher.whitespace()).omitEmptyStrings();
			List<String> elements = Lists.newArrayList(SPLITTER.split(Photo_Information));					
			// Getting count - output
			String OnlyCount = elements.get(3);
			System.out.println("The required count value is: "+OnlyCount);	
			//Convert String to int
			int PhotosCount=Integer.parseInt(OnlyCount);	
			int PageCount = PhotosCount/10;
			int Extra_Photo = PhotosCount%10;
			int totalPageCount=0;
			
			
			//Getting Page count
			if((PageCount>0)&&(Extra_Photo>0))//Multiple of 10 plus extra
			{     
				totalPageCount=PageCount+1;  		
			}
		
			if((PageCount>0)&&(Extra_Photo==0))//Multiple of 10 only
			{	
				totalPageCount=PageCount;   		
			}

			if((PageCount==0)&&(Extra_Photo<10))//less than  10 only
			{
				totalPageCount=1;	   		
			}	
		
			Log.message("Total Page Count is:"+totalPageCount); 
			
	//Edit the Attributes
			for(i = 1;i<=totalPageCount;i++)
			{
				if((PhotosCount<10) && (PhotosCount==PhotoCount))
				{
					//Fill the Attributes and use the copy down
					driver.findElement(By.xpath("(//input[@placeholder='Building'])[1]")).clear();
					driver.findElement(By.xpath("(//input[@placeholder='Building'])[1]")).sendKeys(Building_Name);
					driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[1]")).click();
					SkySiteUtils.waitTill(2000);
					driver.findElement(By.xpath("(//input[@placeholder='Level'])[1]")).clear();
					driver.findElement(By.xpath("(//input[@placeholder='Level'])[1]")).sendKeys(Level);
					driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[2]")).click();
					SkySiteUtils.waitTill(2000);
					driver.findElement(By.xpath("(//input[@placeholder='Room'])[1]")).clear();
					driver.findElement(By.xpath("(//input[@placeholder='Room'])[1]")).sendKeys(Room);
					driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[3]")).click();
					SkySiteUtils.waitTill(2000);
					driver.findElement(By.xpath("(//input[@placeholder='Area'])[1]")).clear();
					driver.findElement(By.xpath("(//input[@placeholder='Area'])[1]")).sendKeys(Area);
					driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[4]")).click();
					SkySiteUtils.waitTill(2000);
					driver.findElement(By.xpath("(//input[@placeholder='Description'])[1]")).clear();
					driver.findElement(By.xpath("(//input[@placeholder='Description'])[1]")).sendKeys(Description);
					driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[5]")).click();
					SkySiteUtils.waitTill(2000);
					btnSaveAttributes.click();
					Log.message("Clicked on save Attributes button.");
						
				}
				if((PhotosCount>=10) && (PhotosCount==PhotoCount))
				{
					//Fill the Attributes and use the copy down
					driver.findElement(By.xpath("(//input[@placeholder='Building'])[1]")).clear();
					driver.findElement(By.xpath("(//input[@placeholder='Building'])[1]")).sendKeys(Building_Name);
					driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[1]")).click();
					SkySiteUtils.waitTill(2000);
					driver.findElement(By.xpath("(//input[@placeholder='Level'])[1]")).clear();
					driver.findElement(By.xpath("(//input[@placeholder='Level'])[1]")).sendKeys(Level);
					driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[2]")).click();
					SkySiteUtils.waitTill(2000);
					driver.findElement(By.xpath("(//input[@placeholder='Room'])[1]")).clear();
					driver.findElement(By.xpath("(//input[@placeholder='Room'])[1]")).sendKeys(Room);
					driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[3]")).click();
					SkySiteUtils.waitTill(2000);
					driver.findElement(By.xpath("(//input[@placeholder='Area'])[1]")).clear();
					driver.findElement(By.xpath("(//input[@placeholder='Area'])[1]")).sendKeys(Area);
					driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[4]")).click();
					SkySiteUtils.waitTill(2000);
					driver.findElement(By.xpath("(//input[@placeholder='Description'])[1]")).clear();
					driver.findElement(By.xpath("(//input[@placeholder='Description'])[1]")).sendKeys(Description);
					driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[5]")).click();
					SkySiteUtils.waitTill(10000);
					btnSaveAttributes.click();
					Log.message("Clicked on save Attributes button.");
				}
							
			}//end of i for loop
		
			SkySiteUtils.waitForElement(driver, btnAddNewAlbum, 20);
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath("//li["+n+"]/section[4]/ul/li[4]/a/i")).click();
			Log.message("Click on Album droplist.");
			SkySiteUtils.waitTill(2000);
			WebElement Photo_Delete1 = driver.findElement(By.xpath("(//a[contains(text(),'Edit photo attributes')])["+Req_Val+"]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",Photo_Delete1);
			Log.message("Clicked on Album Edit Attributes tab.");
			SkySiteUtils.waitForElement(driver, btnSaveAttributes, 30);
			SkySiteUtils.waitTill(3000);
			
	//Validate Edit Attributes are proper or not
			int j=0;
			for(i = 1;i<=totalPageCount;i++)
			{
				if((PhotosCount<=10))
				{
					for(j=1;j<=PhotosCount;j++)
					{
						//Get the existed Attribute values 
						Act_Building=driver.findElement(By.xpath("(//input[@placeholder='Building'])["+j+"]")).getAttribute("value");						
						Act_Level=driver.findElement(By.xpath("(//input[@placeholder='Level'])["+j+"]")).getAttribute("value");
						Act_Room=driver.findElement(By.xpath("(//input[@placeholder='Room'])["+j+"]")).getAttribute("value");
						Act_Area=driver.findElement(By.xpath("(//input[@placeholder='Area'])["+j+"]")).getAttribute("value");
						Act_Description=driver.findElement(By.xpath("(//input[@placeholder='Description'])["+j+"]")).getAttribute("value");
						//Compairing with Expected Attribute values
						if((Act_Building.contentEquals(Building_Name)) && (Act_Level.contentEquals(Level)) && (Act_Room.contentEquals(Room))
								&& (Act_Area.contentEquals(Area)) && (Act_Description.contentEquals(Description)))
						{
							PassCount=PassCount+1;
						}
							
					}
					btnSaveAttributes.click();
					Log.message("Clicked on save attributes");
					SkySiteUtils.waitTill(5000);
				}
				
				if((PhotosCount>10))
				{
					for(j=1;j<=10;j++)
					{
						//Get the existed Attribute values 
						Act_Building=driver.findElement(By.xpath("(//input[@placeholder='Building'])["+j+"]")).getAttribute("value");					
						Act_Level=driver.findElement(By.xpath("(//input[@placeholder='Level'])["+j+"]")).getAttribute("value");									
						Act_Room=driver.findElement(By.xpath("(//input[@placeholder='Room'])["+j+"]")).getAttribute("value");										
						Act_Area=driver.findElement(By.xpath("(//input[@placeholder='Area'])["+j+"]")).getAttribute("value");					
						Act_Description=driver.findElement(By.xpath("(//input[@placeholder='Description'])["+j+"]")).getAttribute("value");
						//Compairing with Expected Attribute values
						if((Act_Building.contentEquals(Building_Name)) && (Act_Level.contentEquals(Level)) && (Act_Room.contentEquals(Room))
								&& (Act_Area.contentEquals(Area)) && (Act_Description.contentEquals(Description)))
						{
							PassCount=PassCount+1;
						}
							
					}		
					if(PassCount==10)
					{
						PhotosCount=PhotosCount-10;
						Log.message("Remaining photos count when page value: "+i+" :is : "+PhotosCount);
						btnSaveAttributes.click();
						Log.message("Clicked on save attributes");
						SkySiteUtils.waitTill(5000);
					}
										
				}
								
			}//End of i for loop
			
			Log.message("Pass Count is: "+PassCount);
			Log.message("File Count is: "+PhotoCount);
			if(PassCount==PhotoCount)
			{
				result2=true;
			}
							
			if(result2==true)
			{
				Log.message("Edit Attributes are success!!!");
			}
			else
			{
				result2=false;
				Log.message("Edit Attributes are failed!!!");
			}	
		}
		if((result1==true)&&(result2==true))
		{
			Log.message("Album Edit Attributes is working successfully!!!");
			return true;
		}
		else
		{
			Log.message("Album Edit Attributes is NOT working!!!");
			return false;
		}
	}
	
	/**
	 * Method written for create a Photo Annotation with attachments.
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_Photo_Annotation_With_Attachments(String FolderPath, String Annotation_Name)throws InterruptedException, AWTException, IOException 
	{
		boolean result1 = false;
		SkySiteUtils.waitTill(5000);
		
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles())
		{
			  driver.switchTo().window(winHandle);
		}
		SkySiteUtils.waitForElement(driver, Photo_Annotation_Icon, 30);
		Photo_Annotation_Icon.click();
		Log.message("Clicked on Photo Annotation Icon.");
		SkySiteUtils.waitTill(2000);
		Viewer_DropPlace.click();
		Log.message("Annotation Dropped on left side phase of the viewer.");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, btnUploadPhotoViewer, 30);
		SkySiteUtils.waitTill(9000);
		btnUploadPhotoViewer.click();
		Log.message("Clicked on Upload Photo button.");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnBackToProjectGallery, 30);
		SkySiteUtils.waitTill(10000);
		btnChooseFile.click(); 
		Log.message("Clicked on on choose files button.");
		SkySiteUtils.waitTill(10000);		 
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(1000);
			}
		}

		output.flush();
		output.close();

		// Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(20000);			
		SkySiteUtils.waitForElement(driver, btnViewerFileUpload, 30);
		try
		{
			 File file = new File(tmpFileName);
			 if(file.delete())
			 {
				 Log.message(file.getName() + " is deleted!");
			 }
			 else
			 {
				 Log.message("Delete operation is failed.");
			 }
		}
		catch(Exception e)
		{
			Log.message("Exception occured!!!"+e);	
		}
				 
		SkySiteUtils.waitTill(8000);		 
		btnViewerFileUpload.click();
		Log.message("Clicked on Upload button.");
		SkySiteUtils.waitForElement(driver, editphotoAnnName, 50);
		SkySiteUtils.waitTill(15000);
		editphotoAnnName.clear();
		editphotoAnnName.sendKeys(Annotation_Name);
		SkySiteUtils.waitTill(2000);
		btnSaveAnnotation.click();
		Log.message("Clicked on Save button.");
		//Need to add Notification message validation
		SkySiteUtils.waitTill(5000);
		btnAllAnnotationsDrop.click();
		Log.message("Clicked on view all markups Drop Down");
		SkySiteUtils.waitForElement(driver, btnViewAllAnnotations, 30);
		SkySiteUtils.waitTill(3000);
		String New_AnnotName=driver.findElement(By.xpath("(//div[@class='col-sm-11 has-tooltip']//h4/span)[1]")).getText();
		Log.message("Annotation After Successfull creation is: "+New_AnnotName);			 
		if((New_AnnotName.contains(Annotation_Name)))
		{
			result1=true;
			Log.message("Photo Annotaion created Sucessfully");	
		}
		else
		{
			result1=false;
			Log.message("Photo Annotaion creation Failed");
		}
		driver.close();
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(winHandleBefore);
		SkySiteUtils.waitTill(3000);

		if((result1==true))
		{
			Log.message("Photo Annotaion with attachments is working successfully!!!");
			return true;
		}
		else
		{
			Log.message("Photo Annotaion with attachments is NOT working!!!");
			return false;
		}	
	}
	
	/**
	 * Method written for Edit Attributes of Photo Annotation.
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_Photo_Annotation_EditAttributes(String Annotation_Name)throws InterruptedException, AWTException, IOException 
	{
		boolean result1 = false;
		SkySiteUtils.waitTill(5000);
		String Building_Name = "Building_"+Generate_Random_Number.generateRandomValue();
		String Level = "Level_"+Generate_Random_Number.generateRandomValue();
		String Room = "Room_"+Generate_Random_Number.generateRandomValue();
		String Area = "Area_"+Generate_Random_Number.generateRandomValue();
		String Description = "Description_"+Generate_Random_Number.generateRandomValue();
		
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles())
		{
			  driver.switchTo().window(winHandle);
		}
		SkySiteUtils.waitForElement(driver, btnAllAnnotationsDrop, 30);
		btnAllAnnotationsDrop.click();
		Log.message("Clicked on view all markups Drop Down");
		SkySiteUtils.waitForElement(driver, btnViewAllAnnotations, 30);
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, firstAnnotationFromList, 30);
		firstAnnotationFromList.click();
		Log.message("Clicked on First located photo annotation from the list.");
		SkySiteUtils.waitForElement(driver, photoAnnotationRoundSymbal, 30);
		SkySiteUtils.waitTill(3000);
		photoAnnotationRoundSymbal.click();
		Log.message("Selected photo annotation round symbol.");
		SkySiteUtils.waitForElement(driver, btnEditAttribute, 30);
		SkySiteUtils.waitTill(2000);
		btnEditAttribute.click();
		Log.message("clicked on Edit Attribute button");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, btnSaveEditAttrPhotoAnnot, 30);
		SkySiteUtils.waitTill(4000);
		driver.findElement(By.xpath("(//input[@class='form-control'])[3]")).clear();
		driver.findElement(By.xpath("(//input[@class='form-control'])[3]")).sendKeys(Annotation_Name);
		driver.findElement(By.xpath("(//input[@class='form-control'])[4]")).clear();
		driver.findElement(By.xpath("(//input[@class='form-control'])[4]")).sendKeys(Building_Name);
		driver.findElement(By.xpath("(//input[@class='form-control'])[5]")).clear();
		driver.findElement(By.xpath("(//input[@class='form-control'])[5]")).sendKeys(Level);
		driver.findElement(By.xpath("(//input[@class='form-control'])[6]")).clear();
		driver.findElement(By.xpath("(//input[@class='form-control'])[6]")).sendKeys(Room);
		driver.findElement(By.xpath("(//input[@class='form-control'])[7]")).clear();
		driver.findElement(By.xpath("(//input[@class='form-control'])[7]")).sendKeys(Area);
		driver.findElement(By.xpath("(//input[@class='form-control'])[8]")).clear();
		driver.findElement(By.xpath("(//input[@class='form-control'])[8]")).sendKeys(Description);
		btnSaveEditAttrPhotoAnnot.click();
		Log.message("Clicked on Save Button.");
		Thread.sleep(5000);
		//If required need to add Notify message validation
		SkySiteUtils.waitForElement(driver, btnEditAttribute, 30);
		SkySiteUtils.waitTill(2000);
		btnEditAttribute.click();
		Log.message("clicked on Edit Attribute button");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, btnSaveEditAttrPhotoAnnot, 30);
		SkySiteUtils.waitTill(4000);
		String Modified_PhotoName =driver.findElement(By.xpath("//*[@id='divViewerAttrContainer']/div/div[2]/div/ul/li[1]/div/div[1]/div/div[2]/h3/span")).getText();
		Log.message("Modified Photo Name from header is:"+Modified_PhotoName );
		String Act_PhotoName=driver.findElement(By.xpath("(//input[@class='form-control'])[3]")).getAttribute("value");
		Log.message("Photo name is Edited To: "+Act_PhotoName);
		String Act_Building=driver.findElement(By.xpath("(//input[@class='form-control'])[4]")).getAttribute("value");
		String Act_Level=driver.findElement(By.xpath("(//input[@class='form-control'])[5]")).getAttribute("value");
		String Act_Room=driver.findElement(By.xpath("(//input[@class='form-control'])[6]")).getAttribute("value");
		String Act_Area=driver.findElement(By.xpath("(//input[@class='form-control'])[7]")).getAttribute("value");
		String Act_Description=driver.findElement(By.xpath("(//input[@class='form-control'])[8]")).getAttribute("value");
		SkySiteUtils.waitTill(3000);
		
		driver.close();
		Log.message("Closing newly opened browser window.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(winHandleBefore);
		Log.message("Navigating back to parent window.");
		SkySiteUtils.waitTill(3000);
			 
		if((Modified_PhotoName.contentEquals(Annotation_Name)) && ((Act_PhotoName.contentEquals(Annotation_Name)))
				&& (Act_Building.contentEquals(Building_Name)) && (Act_Level.contentEquals(Level)) 
				&& (Act_Room.contentEquals(Room)) && (Act_Area.contentEquals(Area)) && (Act_Description.contentEquals(Description)))
		{	
			result1=true;
			Log.message("Edited Attributes for photo is success!!!");	
		}
		else
		{
			result1=false;
			Log.message("Edited Attributes for photo is failed!!!");
		}

		if((result1==true))
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
	
	/**
	 * Method written for Add Photo to Photo Annotation.
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_AddPhoto_ToPhotoAnnotation(String FolderPath)throws InterruptedException, AWTException, IOException 
	{
		boolean result1 = false;
		SkySiteUtils.waitTill(5000);
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles())
		{
			  driver.switchTo().window(winHandle);
		}
		SkySiteUtils.waitForElement(driver, btnAllAnnotationsDrop, 30);
		btnAllAnnotationsDrop.click();
		Log.message("Clicked on view all markups Drop Down");
		SkySiteUtils.waitForElement(driver, btnViewAllAnnotations, 30);
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, firstAnnotationFromList, 30);
		firstAnnotationFromList.click();
		Log.message("Clicked on First located photo annotation from the list.");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, photoAnnotationRoundSymbal, 30);
		SkySiteUtils.waitTill(5000);
		photoAnnotationRoundSymbal.click();
		Log.message("Selected photo annotation round symbol.");
		SkySiteUtils.waitForElement(driver, btnAddPhotoForPhotoAnnotation, 30);
		SkySiteUtils.waitTill(15000);
		
		SkySiteUtils.waitForElement(driver, btnImgCountPhotoAnnotation, 20);
		String Before_ImgCount = btnImgCountPhotoAnnotation.getText();
		Log.message("Image count before add is: "+Before_ImgCount);
		// Split the line and get the count
		Splitter SPLITTER = Splitter.on(CharMatcher.whitespace()).omitEmptyStrings();
		List<String> elements = Lists.newArrayList(SPLITTER.split(Before_ImgCount));					
		String PhotoCount1 = elements.get(2);
		int Before_PhotoCount=Integer.parseInt(PhotoCount1);
		Log.message("Available Photo Count before add is: "+Before_PhotoCount);
		
		btnAddPhotoForPhotoAnnotation.click();
		Log.message("clicked on Add Photo button");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, btnUploadPhotoViewer, 30);
		SkySiteUtils.waitTill(9000);
		btnUploadPhotoViewer.click();
		Log.message("Clicked on Upload Photo button.");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, btnBackToProjectGallery, 30);
		SkySiteUtils.waitTill(5000);
		btnChooseFile.click(); 
		Log.message("Clicked on on choose files button.");
		SkySiteUtils.waitTill(15000);		 
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(1000);
			}
		}

		output.flush();
		output.close();

		// Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(15000);			
		SkySiteUtils.waitForElement(driver, btnViewerFileUpload, 60);
		try
		{
			 File file = new File(tmpFileName);
			 if(file.delete())
			 {
				 Log.message(file.getName() + " is deleted!");
			 }
			 else
			 {
				 Log.message("Delete operation is failed.");
			 }
		}
		catch(Exception e)
		{
			Log.message("Exception occured!!!"+e);	
		}
				 
		SkySiteUtils.waitTill(8000);		 
		btnViewerFileUpload.click();
		Log.message("Clicked on Upload button.");
		SkySiteUtils.waitForElement(driver, editphotoAnnName, 60);
		SkySiteUtils.waitTill(15000);
		SkySiteUtils.waitForElement(driver, btnImgCountPhotoAnnotation, 20);
		String ImgPhotoCount = btnImgCountPhotoAnnotation.getText();
		Log.message("Image count is: "+ImgPhotoCount);
		// Split the line and get the count
		List<String> elements1 = Lists.newArrayList(SPLITTER.split(ImgPhotoCount));					
		String PhotoCount2 = elements1.get(2);
		int Available_PhotoCount=Integer.parseInt(PhotoCount2);
		Log.message("Available Photo Count After add is: "+Available_PhotoCount);
		SkySiteUtils.waitTill(2000);
		
		driver.close();
		Log.message("Closing newly opened browser window.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(winHandleBefore);
		Log.message("Navigating back to parent window.");
		SkySiteUtils.waitTill(3000);
			 
		if(Available_PhotoCount==(Before_PhotoCount+1))
		{	
			result1=true;
			Log.message("Add Photo to photo annotation is success!!!");	
		}
		else
		{
			result1=false;
			Log.message("Add Photo to photo annotation is failed!!!");
		}

		if((result1==true))
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
	
	/**
	 * Method written for Remove a Photo from Photo Annotation.
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_Remove_APhoto_FromPhotoAnnotation()throws InterruptedException, AWTException, IOException 
	{
		boolean result1 = false;
		SkySiteUtils.waitTill(5000);
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles())
		{
			  driver.switchTo().window(winHandle);
		}
		SkySiteUtils.waitForElement(driver, btnAllAnnotationsDrop, 30);
		btnAllAnnotationsDrop.click();
		Log.message("Clicked on view all markups Drop Down");
		SkySiteUtils.waitForElement(driver, btnViewAllAnnotations, 30);
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, firstAnnotationFromList, 30);
		firstAnnotationFromList.click();
		Log.message("Clicked on First located photo annotation from the list.");
		SkySiteUtils.waitForElement(driver, photoAnnotationRoundSymbal, 30);
		SkySiteUtils.waitTill(3000);
		photoAnnotationRoundSymbal.click();
		Log.message("Selected photo annotation round symbol.");
		SkySiteUtils.waitForElement(driver, btnImgCountPhotoAnnotation, 20);
		SkySiteUtils.waitTill(5000);
		//Getting Photo Count before remove
		String Before_ImgCount = btnImgCountPhotoAnnotation.getText();
		Log.message("Image count before add is: "+Before_ImgCount);
		// Split the line and get the count
		Splitter SPLITTER = Splitter.on(CharMatcher.whitespace()).omitEmptyStrings();
		List<String> elements = Lists.newArrayList(SPLITTER.split(Before_ImgCount));					
		String PhotoCount1 = elements.get(2);
		int Before_PhotoCount=Integer.parseInt(PhotoCount1);
		Log.message("Available Photo Count before Remove is: "+Before_PhotoCount);
		btnRemovePhoto.click();
		Log.message("clicked on Remove Photo button");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, btnAlertOKRemovePhoto, 30);
		btnAlertOKRemovePhoto.click();
		Log.message("clicked on OK button to remove Photo");
		SkySiteUtils.waitTill(5000);
		//Need to add Notify message validation 
		//Counting available photos after delete 
		SkySiteUtils.waitForElement(driver, btnImgCountPhotoAnnotation, 20);
		String ImgPhotoCount = btnImgCountPhotoAnnotation.getText();
		Log.message("Image count is: "+ImgPhotoCount);
		// Split the line and get the count
		List<String> elements1 = Lists.newArrayList(SPLITTER.split(ImgPhotoCount));					
		String PhotoCount2 = elements1.get(2);
		int Available_PhotoCount=Integer.parseInt(PhotoCount2);
		Log.message("Available Photo Count After Remove is: "+Available_PhotoCount);
		SkySiteUtils.waitTill(2000);
		
		driver.close();
		Log.message("Closing newly opened browser window.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(winHandleBefore);
		Log.message("Navigating back to parent window.");
		SkySiteUtils.waitTill(3000);
			 
		if(Available_PhotoCount==(Before_PhotoCount-1))
		{	
			result1=true;
			Log.message("Remove a Photo from photo annotation is success!!!");	
		}
		else
		{
			result1=false;
			Log.message("Remove a Photo from photo annotation is failed!!!");
		}

		if((result1==true))
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
	
	/**
	 * Method written for Delete all Photo from Photo Annotation.
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_DeleteAll_Photos_FromPhotoAnnotation()throws InterruptedException, AWTException, IOException 
	{
		boolean result1 = false;
		SkySiteUtils.waitTill(5000);
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles())
		{
			  driver.switchTo().window(winHandle);
		}
		SkySiteUtils.waitForElement(driver, btnAllAnnotationsDrop, 30);
		btnAllAnnotationsDrop.click();
		Log.message("Clicked on view all markups Drop Down");
		SkySiteUtils.waitForElement(driver, btnViewAllAnnotations, 30);
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, firstAnnotationFromList, 30);
		
		//Getting Photo annotations count before delete all photos
		int Before_PhotoAnnotCount = 0;
		int AfterDelete_PhotoAnnotCount=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//i[@class='icon icon-trash icon-lg']"));
		for (WebElement Element : allElements) 
		{
			Before_PhotoAnnotCount = Before_PhotoAnnotCount + 1;
		}
		Log.message("Available Photo Annotation Count before Delete is: "+Before_PhotoAnnotCount);		
		
		firstAnnotationFromList.click();
		Log.message("Clicked on First located photo annotation from the list.");
		SkySiteUtils.waitForElement(driver, photoAnnotationRoundSymbal, 30);
		SkySiteUtils.waitTill(3000);
		photoAnnotationRoundSymbal.click();
		Log.message("Selected photo annotation round symbol.");
		SkySiteUtils.waitForElement(driver, btnImgCountPhotoAnnotation, 20);
		SkySiteUtils.waitTill(5000);
		btnRemovePhoto.click();
		Log.message("clicked on Remove Photo button");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, btnYESAlertWind, 30);
		btnYESAlertWind.click();
		Log.message("clicked on Delete All button to remove Photo Annotation");
		SkySiteUtils.waitTill(5000);
		//Need to add Notify message validation 
		//Counting available photos Annotations after delete
		if(btnAllAnnotationsDrop.isDisplayed())
		{
			SkySiteUtils.waitForElement(driver, btnAllAnnotationsDrop, 30);
			btnAllAnnotationsDrop.click();
			Log.message("Clicked on view all markups Drop Down");
			SkySiteUtils.waitForElement(driver, firstAnnotationFromList, 30);
			List<WebElement> allElements1 = driver.findElements(By.xpath("//i[@class='icon icon-trash icon-lg']"));
			for (WebElement Element : allElements1) 
			{
				AfterDelete_PhotoAnnotCount = AfterDelete_PhotoAnnotCount + 1;
			}
			Log.message("Available Photo Annotation Count After Remove is: "+AfterDelete_PhotoAnnotCount);
			SkySiteUtils.waitTill(2000);
			if(AfterDelete_PhotoAnnotCount==(Before_PhotoAnnotCount-1))
			{
				result1 = true;
				Log.message("All Photos are deleted successfully.");
			}
		}
		else
		{
			result1 = true;
			Log.message("Available single annotation was deleted successfully.");
		}
		driver.close();
		Log.message("Closing newly opened browser window.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(winHandleBefore);
		Log.message("Navigating back to parent window.");
		SkySiteUtils.waitTill(3000);
		
		if((result1==true))
		{
			Log.message("Remove All Photos from photo annotation is success!!!");
			return true;
		}
		else
		{
			Log.message("Remove All Photos from photo annotation is failed!!!");
			return false;
		}	
	}
	
	/**
	 * Method written for Delete a Photo Annotation from the list.
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_Delete_APhotoAnnotation(String Annotation_Name)throws InterruptedException, AWTException, IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		SkySiteUtils.waitTill(5000);
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles())
		{
			  driver.switchTo().window(winHandle);
		}
		SkySiteUtils.waitForElement(driver, btnAllAnnotationsDrop, 30);
		btnAllAnnotationsDrop.click();
		Log.message("Clicked on view all markups Drop Down");
		SkySiteUtils.waitForElement(driver, btnViewAllAnnotations, 30);
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, firstAnnotationFromList, 30);
		
		//Getting Photo annotations count before delete all photos
		int Before_PhotoAnnotCount = 0;
		int AfterDelete_PhotoAnnotCount=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//i[@class='icon icon-trash icon-lg']"));
		for (WebElement Element : allElements) 
		{
			Before_PhotoAnnotCount = Before_PhotoAnnotCount + 1;
		}
		Log.message("Available Photo Annotation Count before Delete is: "+Before_PhotoAnnotCount);		
		
		int i=0;
		for(i=1;i<=Before_PhotoAnnotCount;i++)
		{
			String Exp_AnnotName = driver.findElement(By.xpath("(//div[@class='col-sm-11 has-tooltip']/h4/span)["+i+"]")).getText();
			Log.message("Exp Annot Name is: "+Exp_AnnotName);
			if(Exp_AnnotName.equalsIgnoreCase(Annotation_Name))
			{
				result1=true;
				Log.message("Expected Annotation is available.");
				break;
			}
		}
		
		if(result1==true)
		{
			Log.message("Expected Annotation is available...You can delete.");
			driver.findElement(By.xpath("(//i[@class='icon icon-trash icon-lg'])["+i+"]")).click();
			Log.message("Clicked on Delete Icon of an expected photo annotation from the list.");
			SkySiteUtils.waitTill(1000);
			SkySiteUtils.waitForElement(driver, btnYESAlertWind, 30);
			btnYESAlertWind.click();
			Log.message("clicked on Yes button to Delete Photo Annotation");
			SkySiteUtils.waitTill(10000);
			//Need to add Notify message validation 
			//Counting available photos Annotations after delete
			if(btnAllAnnotationsDrop.isDisplayed())
			{
				SkySiteUtils.waitForElement(driver, btnAllAnnotationsDrop, 30);
				btnAllAnnotationsDrop.click();
				Log.message("Clicked on view all markups Drop Down");
				SkySiteUtils.waitForElement(driver, firstAnnotationFromList, 30);
				List<WebElement> allElements1 = driver.findElements(By.xpath("//i[@class='icon icon-trash icon-lg']"));
				for (WebElement Element : allElements1) 
				{
					AfterDelete_PhotoAnnotCount = AfterDelete_PhotoAnnotCount + 1;
				}
				Log.message("Available Photo Annotation Count After Remove is: "+AfterDelete_PhotoAnnotCount);
				SkySiteUtils.waitTill(2000);
				if(AfterDelete_PhotoAnnotCount==(Before_PhotoAnnotCount-1))
				{
					result2 = true;
					Log.message("Photo Annotation deleted successfully.");
				}
			}
			else
			{
				result2 = true;
				Log.message("Available Single Photo Annotation deleted successfully.");
			}
		}

		driver.close();
		Log.message("Closing newly opened browser window.");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(winHandleBefore);
		Log.message("Navigating back to parent window.");
		SkySiteUtils.waitTill(3000);
		
		if((result1==true)&&(result2==true))
		{
			Log.message("Expected photo annotation is Deleted success!!!");
			return true;
		}
		else
		{
			Log.message("Expected photo annotation Deletion is failed!!!");
			return false;
		}	
	}
	
	

}
