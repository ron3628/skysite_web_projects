package com.arc.projects.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arcautoframe.utils.Log;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arc.projects.utils.randomFileName;

import com.arc.projects.pages.ProjectDashboardPage;

public class ProjectLatestDocumentsPage extends LoadableComponent<ProjectLatestDocumentsPage> 
{
	
	WebDriver driver;
	private boolean isPageLoaded;

	@FindBy(xpath="//*[@id='docsExport']/a")
	WebElement btnExportToCSV;
		
	@FindBy(xpath="//*[@id='breadCrumb']/div[2]/ul/li[4]")
	WebElement breadcrumbLatestDocuments;
	
	@FindBy(xpath="//*[@id='aSortOptionName']")
	WebElement btnLDSortBy;
	
	@FindBy(xpath="//*[@id='aSortName']")
	WebElement btnLDSortByName;
	
	@FindBy(xpath="//*[@id='aSortingArrow']")
	WebElement btnLDSortOrder;
	
	@FindBy(xpath="//*[@id='switch-view']")
	WebElement btnLDSwitchGridListView;
	
	@FindBy(xpath="//*[@id='aSortCreateDate']")
	WebElement btnLDSortByCreateDate;
	
	@FindBy(xpath="//*[@id='aSortDiscipline']")
	WebElement btnLDSortByDiscipline;
	
	@FindBy(xpath="//*[@id='aSortOrdinalNumber']")
	WebElement btnLDSortByOrdinalNumber;
	
	@FindBy(css="#btnLogin")
	WebElement btnLogin;
	
	@FindBy(css="#button-1")
	WebElement btnYes;
	
	@FindBy(xpath="//a[contains(text(), 'Sign out')]")
	WebElement btnLogout;
	
	@FindBy(xpath="//*[@id='mainPageContainer']/div[1]/ul[1]/li[1]/a")
	WebElement btnProfile;
	
	@FindBy(xpath="//*[@id='breadCrumb']/div[2]/ul/li[3]")
	WebElement btnBreadcrumbProjectRootLevel;
	
	@FindBy(xpath="//*[text()='Latest documents']")
	WebElement latestDocuments;
	
	@FindBy(xpath="//*[@id='hrefCartLink']/a")
	WebElement btnPrintCart;	

	@FindBy(xpath="//*[text()='Select']")
	WebElement btnSelect;
	
	@FindBy(xpath="//*[text()='Select all files']")
	WebElement btnSelectAllFiles;	
	
	@FindBy(xpath="//*[@id='divHeader']/div[2]/ul[2]/li[6]/a")
	WebElement btnFileMenu;
	
	@FindBy(xpath="//*[@id='sel-FolderItem']")
	WebElement addToPrintCart;
	
	@FindBy(xpath="//*[text()='Clear selection']")
	WebElement btnClearSelection;
	
	@FindBy(xpath="//*[@id='Cartcount']")
	WebElement cartItemsCount;
	
	@Override
	protected void load() 
	{
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnExportToCSV, 20);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		if (!isPageLoaded) 
		{
			Assert.fail();
		}		
	}
	
	/**
	 * Declaring constructor for initializing web elements using PageFactory
	 * class.
	 * 
	 * @param driver
	 * @return
	 */
	public ProjectLatestDocumentsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	
	/** 
	 * Method for validating entry in Latest Documents
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public void latestDocumentEntryValidation() 
	{
		
		SkySiteUtils.waitForElement(driver, breadcrumbLatestDocuments, 30);
		Log.message("Breadcrumb is displayed");
		
		String actualBreadcrumb = breadcrumbLatestDocuments.getText();
		Log.message("Actual breadcrumb text is:- " +actualBreadcrumb);
		
		String expectedBreadcrumb= PropertyReader.getProperty("EXPECTEDBREADCRUMB");
		Log.message("Expected breadcrumb text is:- " +expectedBreadcrumb);
		
		if(actualBreadcrumb.contains(expectedBreadcrumb))
		{
			Log.message("Latest Documents entry successfull");
			
		}
		else
		{
			Log.message("Latest Documents entry failed!!!");
		}
	}

	
	/** 
	 * Method for validating descending order sorting for Files in Latest Documents with NAME 
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean descendingOrderSortingInLatestDocumentsByFileName() 
	{
		
		SkySiteUtils.waitForElement(driver, btnLDSortBy, 30);
		Log.message("Folder Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Latest Documents Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnLDSortByName, 30);
		Log.message("Sort by Folder Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='aSortName']"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Latest Documents Sort by Name button");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnLDSortOrder, 30);
		String sortOrder=btnLDSortOrder.getAttribute("data-original-title");
	    Log.message("Latest Document sort order is:-" +sortOrder);
	    
	    ArrayList<String> obtainedLDFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='pw-file-name']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedLDFileList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Latest Document files count before sorting:- " +obtainedLDFileList.size());		 
		Log.message("Obtained Latest Document files are:- " +obtainedLDFileList);
		
		String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Latest Document file sort order is:- " +expectedSortOrder);
		
		if(sortOrder.contains(expectedSortOrder))
		{
			btnLDSortOrder.click();
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not ascending");
		}
		SkySiteUtils.waitTill(8000);
		
		ArrayList<String> sortedLDFileList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@class='pw-file-name']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				sortedLDFileList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Latest Document files count after sorting:- " +sortedLDFileList.size());		 
		Log.message("Obtained Latest Document files are:- " +sortedLDFileList);
		
		if(obtainedLDFileList.get(0).contains(sortedLDFileList.get(sortedLDFileList.size()-1)) && obtainedLDFileList.get(obtainedLDFileList.size()-1).contains(sortedLDFileList.get(0)))
	    {	
			return true;
			
	    }
	    else
	    {
	    	return false;
	    	
	    }
	}

	
	/** 
	 * Method for restoring default sort settings for Files In Latest Documents
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public void resetToDefaultLatestDocumentsSortSettings() 
	{
		
		SkySiteUtils.waitForElement(driver, btnLDSortBy, 30);
		Log.message("Folder Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Latest Documents Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnLDSortByName, 30);
		Log.message("Sort by Folder Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='aSortName']"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Latest Documents Sort by Name button");
		SkySiteUtils.waitTill(5000);
		
		String sortOrder=btnLDSortOrder.getAttribute("data-original-title");
		String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
		
		if(sortOrder.contains(expectedSortOrder))
		{			
			WebElement element2 = driver.findElement(By.xpath("//*[@id='aSortingArrow']"));
			JavascriptExecutor executor2 = (JavascriptExecutor)driver;
			executor2.executeScript("arguments[0].click();", element2);
			Log.message("Clicked on order by button");
			SkySiteUtils.waitTill(8000);
			
		}
			
		Log.message("Default sorting settings restored");
	
		
	}

	
	/** 
	 * Method for validating ascending order sorting for Files in Latest Documents with NAME 
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean ascendingOrderSortingInLatestDocumentsByFileName() 
	{
		
		SkySiteUtils.waitForElement(driver, btnLDSortBy, 30);
		Log.message("Folder Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Latest Documents Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnLDSortByName, 30);
		Log.message("Sort by Folder Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='aSortName']"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Latest Documents Sort by Name button");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnLDSortOrder, 30);
		String sortOrder=btnLDSortOrder.getAttribute("data-original-title");
	    Log.message("Latest Document sort order is:-" +sortOrder);
	    
	    String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder);
		if(sortOrder.contains(expectedSortOrder))
		{
		
			WebElement element2 = driver.findElement(By.xpath("//*[@id='aSortingArrow']"));
			JavascriptExecutor executor2 = (JavascriptExecutor)driver;
			executor2.executeScript("arguments[0].click();", element2);			
			SkySiteUtils.waitTill(5000);
			
		}
	    
		ArrayList<String> obtainedLDFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='pw-file-name']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedLDFileList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Latest Document files count before sorting:- " +obtainedLDFileList.size());		 
		Log.message("Obtained Latest Document files are:- " +obtainedLDFileList);
		
		SkySiteUtils.waitForElement(driver, btnLDSortOrder, 30);
		String sortOrder1=btnLDSortOrder.getAttribute("data-original-title");
	    Log.message("Present Latest Document files sort order is:-" +sortOrder1);
		
		String expectedSortOrder1= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
		Log.message("Expected Latest Document files sort order is:- " +expectedSortOrder1);
		
		if(sortOrder1.contains(expectedSortOrder1))
		{
			btnLDSortOrder.click();
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not descending");
		}
		SkySiteUtils.waitTill(8000);
		
		ArrayList<String> sortedLDFileList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@class='pw-file-name']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				sortedLDFileList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Latest Document files count after sorting:- " +sortedLDFileList.size());		 
		Log.message("Obtained Latest Document files are:- " +sortedLDFileList);
		
		if(obtainedLDFileList.get(0).contains(sortedLDFileList.get(sortedLDFileList.size()-1)) && obtainedLDFileList.get(obtainedLDFileList.size()-1).contains(sortedLDFileList.get(0)))
	    {	
			return true;
			
	    }
	    else
	    {
	    	return false;
	    	
	    }
		
	}

	
	
	/** 
	 * Method for changing Latest Documents file view from grid to list
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public void changeLDViewGridToList() 
	{
		
		SkySiteUtils.waitForElement(driver, btnLDSwitchGridListView, 30);
		Log.message("Switch file view button for Latest Documents is displayed");
		
		SkySiteUtils.waitTill(5000);
		String actualLDFileView = btnLDSwitchGridListView.getAttribute("data-type");		
		String expectedLDFileView = PropertyReader.getProperty("GRIDTOLISTVIEW");
		
		if(actualLDFileView.contains(expectedLDFileView))
		{
			btnLDSwitchGridListView.click();
			Log.message("Clicked on switch file view button for Latest Documents");
			SkySiteUtils.waitTill(8000);
			
		}
		else
		{
			btnLDSwitchGridListView.click();
			SkySiteUtils.waitTill(8000);
			btnLDSwitchGridListView.click();
			Log.message("Clicked on switch file view button for Latest Documents");
			SkySiteUtils.waitTill(8000);
			
		}
		
		String actualLatestLDFileView = btnLDSwitchGridListView.getAttribute("data-type");
		String expectedLatestLDFileView = PropertyReader.getProperty("LISTTOGRIDVIEW");
		
		if(actualLatestLDFileView.contains(expectedLatestLDFileView))
		{
			Log.message("Grid to List view change is successfull");
			
		}
		else
		{
			Log.message("Grid to List view change failed");
			
		}

	}
	
	
	
	/** 
	 * Method for changing Latest Documents file view from list to grid
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public void changeLDViewListToGrid() 
	{
			
		SkySiteUtils.waitForElement(driver, btnLDSwitchGridListView, 30);
		Log.message("Switch file view button for Latest Documents is displayed");
		
		String actualLDFileView = btnLDSwitchGridListView.getAttribute("data-type");
		String expectedLDFileView = PropertyReader.getProperty("LISTTOGRIDVIEW");

		if(actualLDFileView.contains(expectedLDFileView))
		{
			btnLDSwitchGridListView.click();
			Log.message("Clicked on Latest Document switch file view button");
			SkySiteUtils.waitTill(8000);
			
		}
		
		String actualLDFileView1 = btnLDSwitchGridListView.getAttribute("data-type");
		String expectedLDFileView2 = PropertyReader.getProperty("GRIDTOLISTVIEW");
	
		if(actualLDFileView1.contains(expectedLDFileView2))
		{
			Log.message("List to Grid view change is successfull");
			
		}
		else
		{
			Log.message("List to Grid view change failed");
			
		}
		
	}
	
	
	
	/** 
	 * Method for validating descending order sorting for Files in Latest Documents with CREATE DATE 
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean descendingOrderSortingInLatestDocumentsByCreateDate() 
	{
		
		SkySiteUtils.waitForElement(driver, btnLDSortBy, 30);
		Log.message("Folder Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Latest Documents Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnLDSortByCreateDate, 30);
		Log.message("Sort by Folder Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='aSortCreateDate']"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Latest Documents Sort by Create Date button");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnLDSortOrder, 30);
		String sortOrder=btnLDSortOrder.getAttribute("data-original-title");
	    Log.message("Latest Document sort order is:-" +sortOrder);
	    
	    ArrayList<String> obtainedLDFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h5_rev']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedLDFileList.add(we.getText().split("\\|")[2]); 
		     
			}
		}
		Log.message("Obtained Latest Document files count before sorting:- " +obtainedLDFileList.size());		 
		Log.message("Obtained Latest Document files Create Dates are:- " +obtainedLDFileList);
		
		String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Latest Document file sort order is:- " +expectedSortOrder);
		
		
		if(sortOrder.contains(expectedSortOrder))
		{
			
			WebElement element2 = driver.findElement(By.xpath("//*[@id='aSortingArrow']"));
			JavascriptExecutor executor2 = (JavascriptExecutor)driver;
			executor2.executeScript("arguments[0].click();", element2);
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not ascending");
		}
		SkySiteUtils.waitTill(8000);
		
		ArrayList<String> sortedLDFileList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@class='h5_rev']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				sortedLDFileList.add(we.getText().split("\\|")[2]); 
		     
			}
		}
		Log.message("Obtained Latest Document files count after sorting:- " +sortedLDFileList.size());		 
		Log.message("Obtained Latest Document files Create Dates are:- " +sortedLDFileList);
		
		if(obtainedLDFileList.get(0).contains(sortedLDFileList.get(sortedLDFileList.size()-1)) && obtainedLDFileList.get(obtainedLDFileList.size()-1).contains(sortedLDFileList.get(0)))
	    {	
			return true;
			
	    }
	    else
	    {
	    	return false;
	    	
	    }
	}

	
	
	/** 
	 * Method for validating ascending order sorting for Files in Latest Documents with CREATE DATE 
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean ascendingOrderSortingInLatestDocumentsByCreateDate() 
	{
		
		SkySiteUtils.waitForElement(driver, btnLDSortBy, 30);
		Log.message("Folder Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Latest Documents Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnLDSortByCreateDate, 30);
		Log.message("Sort by Folder Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='aSortCreateDate']"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Latest Documents Sort by Create Date button");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnLDSortOrder, 30);
		String sortOrder=btnLDSortOrder.getAttribute("data-original-title");
	    Log.message("Latest Document sort order is:-" +sortOrder);
		
	    String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Latest Docuement file sort order is:- " +expectedSortOrder);
		if(sortOrder.contains(expectedSortOrder))
		{
		
			WebElement element2 = driver.findElement(By.xpath("//*[@id='aSortingArrow']"));
			JavascriptExecutor executor2 = (JavascriptExecutor)driver;
			executor2.executeScript("arguments[0].click();", element2);			
			SkySiteUtils.waitTill(5000);
			
		}
	    
		ArrayList<String> obtainedLDFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h5_rev']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedLDFileList.add(we.getText().split("\\|")[2]); 
		     
			}
		}
		Log.message("Obtained Latest Document files count before sorting:- " +obtainedLDFileList.size());		 
		Log.message("Obtained Latest Document files Create Dates are:- " +obtainedLDFileList);
		
		SkySiteUtils.waitForElement(driver, btnLDSortOrder, 30);
		String sortOrder1=btnLDSortOrder.getAttribute("data-original-title");
	    Log.message("Present Latest Document files sort order is:-" +sortOrder1);
		
		String expectedSortOrder1= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
		Log.message("Expected Latest Document files sort order is:- " +expectedSortOrder1);
		
		if(sortOrder1.contains(expectedSortOrder1))
		{
			WebElement element3 = driver.findElement(By.xpath("//*[@id='aSortingArrow']"));
			JavascriptExecutor executor3 = (JavascriptExecutor)driver;
			executor3.executeScript("arguments[0].click();", element3);
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not descending");
		}
		SkySiteUtils.waitTill(8000);
		
		ArrayList<String> sortedLDFileList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@class='h5_rev']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				sortedLDFileList.add(we.getText().split("\\|")[2]); 
		     
			}
		}
		Log.message("Obtained Latest Document files count after sorting:- " +sortedLDFileList.size());		 
		Log.message("Obtained Latest Document files Create Dates are:- " +sortedLDFileList);
		
		if(obtainedLDFileList.get(0).contains(sortedLDFileList.get(sortedLDFileList.size()-1)) && obtainedLDFileList.get(obtainedLDFileList.size()-1).contains(sortedLDFileList.get(0)))
	    {	
			return true;
			
	    }
	    else
	    {
	    	return false;
	    	
	    }
	}

	
	/** 
	 * Method for validating descending order sorting for Files in Latest Documents with DISCIPLINE 
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean descendingOrderSortingInLatestDocumentsByCreateDateDiscipline() 
	{

		SkySiteUtils.waitForElement(driver, btnLDSortBy, 30);
		Log.message("Folder Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Latest Documents Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnLDSortByDiscipline, 30);
		Log.message("Sort by Folder Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='aSortDiscipline']"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Latest Documents Sort by Discipline button");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnLDSortOrder, 30);
		String sortOrder=btnLDSortOrder.getAttribute("data-original-title");
	    Log.message("Latest Document sort order is:-" +sortOrder);
	    
	    ArrayList<String> obtainedLDFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h4_disc']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedLDFileList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Latest Document files count before sorting:- " +obtainedLDFileList.size());		 
		Log.message("Obtained Latest Document files Disciplines are:- " +obtainedLDFileList);
		
		String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Latest Document file sort order is:- " +expectedSortOrder);
		
		
		if(sortOrder.contains(expectedSortOrder))
		{
			
			WebElement element2 = driver.findElement(By.xpath("//*[@id='aSortingArrow']"));
			JavascriptExecutor executor2 = (JavascriptExecutor)driver;
			executor2.executeScript("arguments[0].click();", element2);
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not ascending");
		}
		SkySiteUtils.waitTill(8000);
		
		ArrayList<String> sortedLDFileList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@class='h4_disc']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				sortedLDFileList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Latest Document files count after sorting:- " +sortedLDFileList.size());		 
		Log.message("Obtained Latest Document files Disciplines are:- " +sortedLDFileList);
		
		if(obtainedLDFileList.get(0).contains(sortedLDFileList.get(sortedLDFileList.size()-1)) && obtainedLDFileList.get(obtainedLDFileList.size()-1).contains(sortedLDFileList.get(0)))
	    {	
			return true;
			
	    }
	    else
	    {
	    	return false;
	    	
	    }
	}

	
	/** 
	 * Method for validating ascending order sorting for Files in Latest Documents with DISCIPLINE 
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean ascendingOrderSortingInLatestDocumentsByCreateDateDiscipline() 
	{
		
		SkySiteUtils.waitForElement(driver, btnLDSortBy, 30);
		Log.message("Folder Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Latest Documents Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnLDSortByDiscipline, 30);
		Log.message("Sort by Folder Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='aSortDiscipline']"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Latest Documents Sort by Discipline button");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnLDSortOrder, 30);
		String sortOrder=btnLDSortOrder.getAttribute("data-original-title");
	    Log.message("Latest Document sort order is:-" +sortOrder);
		
	    String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Latest Docuement file sort order is:- " +expectedSortOrder);
		if(sortOrder.contains(expectedSortOrder))
		{
		
			WebElement element2 = driver.findElement(By.xpath("//*[@id='aSortingArrow']"));
			JavascriptExecutor executor2 = (JavascriptExecutor)driver;
			executor2.executeScript("arguments[0].click();", element2);			
			SkySiteUtils.waitTill(5000);
			
		}
	    
		ArrayList<String> obtainedLDFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h4_disc']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedLDFileList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Latest Document files count before sorting:- " +obtainedLDFileList.size());		 
		Log.message("Obtained Latest Document files DIsciplines are:- " +obtainedLDFileList);
		
		SkySiteUtils.waitForElement(driver, btnLDSortOrder, 30);
		String sortOrder1=btnLDSortOrder.getAttribute("data-original-title");
	    Log.message("Present Latest Document files sort order is:-" +sortOrder1);
		
		String expectedSortOrder1= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
		Log.message("Expected Latest Document files sort order is:- " +expectedSortOrder1);
		
		if(sortOrder1.contains(expectedSortOrder1))
		{
			WebElement element3 = driver.findElement(By.xpath("//*[@id='aSortingArrow']"));
			JavascriptExecutor executor3 = (JavascriptExecutor)driver;
			executor3.executeScript("arguments[0].click();", element3);
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not descending");
		}
		SkySiteUtils.waitTill(8000);
		
		ArrayList<String> sortedLDFileList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@class='h4_disc']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				sortedLDFileList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Latest Document files count after sorting:- " +sortedLDFileList.size());		 
		Log.message("Obtained Latest Document files Disciplines are:- " +sortedLDFileList);
		
		if(obtainedLDFileList.get(0).contains(sortedLDFileList.get(sortedLDFileList.size()-1)) && obtainedLDFileList.get(obtainedLDFileList.size()-1).contains(sortedLDFileList.get(0)))
	    {	
			return true;
			
	    }
	    else
	    {
	    	return false;
	    	
	    }
	}


	
	/** 
	 * Method for validating decending order sorting for Files in Latest Documents with ORDINAL NUMBER 
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean descendingOrderSortingInLatestDocumentsByCreateDateOrdinalNumber(ArrayList<String> arr1) 
	{
		SkySiteUtils.waitForElement(driver, btnLDSortBy, 30);
		Log.message("Folder Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Latest Documents Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnLDSortByOrdinalNumber, 30);
		Log.message("Sort by Folder Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='aSortOrdinalNumber']"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Latest Documents Sort by Ordinal Number button");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnLDSortOrder, 30);
		String sortOrder=btnLDSortOrder.getAttribute("data-original-title");
	    Log.message("Latest Document sort order is:-" +sortOrder);
		
	    ArrayList<String> obtainedLDFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='pw-file-name']"));		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedLDFileList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Latest Document files count before sorting:- " +obtainedLDFileList.size());		 
		Log.message("Obtained Latest Document files are:- " +obtainedLDFileList);
	    
		String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Latest Document file sort order is:- " +expectedSortOrder);		
		
		if(sortOrder.contains(expectedSortOrder))
		{
			
			WebElement element2 = driver.findElement(By.xpath("//*[@id='aSortingArrow']"));
			JavascriptExecutor executor2 = (JavascriptExecutor)driver;
			executor2.executeScript("arguments[0].click();", element2);
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not ascending");
		}
		SkySiteUtils.waitTill(8000);
		
		ArrayList<String> sortedLDFileList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@class='pw-file-name']"));		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				sortedLDFileList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Latest Document files count after sorting:- " +sortedLDFileList.size());		 
		Log.message("Obtained Latest Document files are:- " +sortedLDFileList);
		
		if(arr1.get(0).contains(sortedLDFileList.get(sortedLDFileList.size()-1)) && arr1.get(arr1.size()-1).contains(sortedLDFileList.get(0)))
	    {	
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}

	
	/** 
	 * Method for validating ascending order sorting for Files in Latest Documents with ORDINAL NUMBER 
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean ascendingOrderSortingInLatestDocumentsByCreateDateOrdinalNumber(ArrayList<String> arr1) 
	{
		
		SkySiteUtils.waitForElement(driver, btnLDSortBy, 30);
		Log.message("Folder Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Latest Documents Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnLDSortByOrdinalNumber, 30);
		Log.message("Sort by Folder Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='aSortOrdinalNumber']"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Latest Documents Sort by Ordinal Number button");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnLDSortOrder, 30);
		String sortOrder=btnLDSortOrder.getAttribute("data-original-title");
	    Log.message("Latest Document sort order is:-" +sortOrder);
		
	    String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Latest Document sort order is:- " +expectedSortOrder);
		
		if(sortOrder.contains(expectedSortOrder))
		{
			
			WebElement element2 = driver.findElement(By.xpath("//*[@id='aSortingArrow']"));
			JavascriptExecutor executor2 = (JavascriptExecutor)driver;
			executor2.executeScript("arguments[0].click();", element2);
			SkySiteUtils.waitTill(8000);
			
		}
		
		ArrayList<String> obtainedLDFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='pw-file-name']"));		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedLDFileList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Latest Document files count before sorting:- " +obtainedLDFileList.size());		 
		Log.message("Obtained Latest Document files are:- " +obtainedLDFileList);
		
		SkySiteUtils.waitForElement(driver, btnLDSortOrder, 30);
		String sortOrder1=btnLDSortOrder.getAttribute("data-original-title");
	    Log.message("Latest Document sort order is:-" +sortOrder1);
		
	    String expectedSortOrder1= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
		Log.message("Expected Latest Document sort order is:- " +expectedSortOrder1);
		
		if(sortOrder1.contains(expectedSortOrder1))
		{
			btnLDSortOrder.click();
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not descending");
		}
		SkySiteUtils.waitTill(8000);
		
		ArrayList<String> sortedLDFileList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@class='pw-file-name']"));		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				sortedLDFileList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Latest Document files count after sorting:- " +sortedLDFileList.size());		 
		Log.message("Obtained Latest Document files are:- " +sortedLDFileList);
		
		if(arr1.equals(sortedLDFileList))
		{
			return true;
		}
		else
		{
			return false;			
		}
	}

	
	/** 
	 * Method to change sorting order and save data to be validated later in Latest Documents
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public ArrayList<String> sortOrderSave() 
	{
		SkySiteUtils.waitForElement(driver, btnLDSortBy, 30);
		Log.message("Folder Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Latest Documents Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnLDSortByName, 30);
		Log.message("Sort by Folder Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='aSortName']"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Latest Documents Sort by Name button");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnLDSortOrder, 30);
		String sortOrder=btnLDSortOrder.getAttribute("data-original-title");
	    Log.message("Latest Document sort order is:-" +sortOrder);
	    
	    String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Latest Document file sort order is:- " +expectedSortOrder);
		
		if(sortOrder.contains(expectedSortOrder))
		{
			btnLDSortOrder.click();
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not ascending");
		}
		SkySiteUtils.waitTill(8000);
		
	    ArrayList<String> obtainedLDFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='pw-file-name']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedLDFileList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Latest Document files count before sorting:- " +obtainedLDFileList.size());		 
		Log.message("Obtained Latest Document files are:- " +obtainedLDFileList);
		
		return obtainedLDFileList;
		
	}

	
	
	/** 
	 * Method to verify sorting order saved earlier after Log In in Latest Documents
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean sortOrderSaveAfterLogIn(ArrayList<String> arr1) 
	{
		
		SkySiteUtils.waitForElement(driver, btnLDSortOrder, 30);
		String sortBy=btnLDSortOrder.getAttribute("data-original-title");
	    Log.message("Present Latest Document files sort by order is:-" +sortBy);
		
		String expectedSortBy= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
		Log.message("Expected Latest Document files sort by order is:- " +expectedSortBy);
		
		SkySiteUtils.waitForElement(driver, btnLDSortBy, 30);
		String sortOrder = btnLDSortBy.getText();
	    Log.message("Actual Latest Documents sort order is:-" +sortOrder);
	    
	    String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE3");
		Log.message("Expected Latest Documents sort order is:- " +expectedSortOrder);
		
		ArrayList<String> obtainedLDFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='pw-file-name']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedLDFileList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Latest Document files count before sorting:- " +obtainedLDFileList.size());		 
		Log.message("Obtained Latest Document files are:- " +obtainedLDFileList);
		
		if(sortBy.contains(expectedSortBy) && sortOrder.contains(expectedSortOrder) && arr1.equals(obtainedLDFileList))
		{
			return true;
			
		}
		else 
		{
			return false;
			
		}
	}

	
	
	/** 
	 * Method written for Logout from Projects Latest Documents
	 * Scripted By: Ranadeep
	 * @return
	 */
	public boolean Logout_Projects() 
	{
		SkySiteUtils.waitForElement(driver, btnProfile, 20);
		btnProfile.click();
		Log.message("Clicked on Profile.");
		SkySiteUtils.waitForElement(driver, btnLogout, 20);
		SkySiteUtils.waitTill(3000);
		btnLogout.click();
		SkySiteUtils.waitForElement(driver, btnYes, 20);
		btnYes.click();
		SkySiteUtils.waitForElement(driver, btnLogin, 30);
		
		Log.message("Checking whether Login button is present?");
		if(btnLogin.isDisplayed())
			return true;
		else
			return false;
		
	}
	
	
	/** 
	 * Method for verifying latest revision in Latest Documents
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean revisionVerifyInLatestDocuments(ArrayList<String> arr) 
	{
		
		SkySiteUtils.waitTill(8000);
		ArrayList<String> obtainedFileRevisionList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h5_rev']/span"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFileRevisionList.add(we.getText());
		     
			}
		}
		
		Log.message("Obtained Files count before sorting:- " +obtainedFileRevisionList.size());		 
		Log.message("Obtained File Revisions are:- " +obtainedFileRevisionList);
		
		Log.message("Actual Revision list in:- " +arr);
			
		if(arr.contains(obtainedFileRevisionList.get(0)))
		{
			return true;
		}
		else
		{
			return false;

		}
		
	}


	
	/** 
	 * Method for navigating back to the root folder
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public void backToRootFolder() 
	{

		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnBreadcrumbProjectRootLevel, 30);
		btnBreadcrumbProjectRootLevel.click();
		Log.message("Clicked on project name in breadcrumb to navigate to Project Root Level");
		
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, latestDocuments, 30);
		if(latestDocuments.isDisplayed())
		{
			Log.message(" Navigation to Project root level is successfull ");
		}
		else
		{
			Log.message(" Navigation to Project root level failed!!! ");
		}
		
	}

	
	/** 
	 * Method for adding Files in Latest documents to Print cart and fetching the count
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public int addAllFilesInFolderToPrintCart() 
	{

		WebElement element = driver.findElement(By.xpath("//*[text()='Select']"));
	    JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);
	    
	    SkySiteUtils.waitForElement(driver, btnSelectAllFiles, 30);
		btnSelectAllFiles.click();
		Log.message("Clicked on Select all files button");
		
		SkySiteUtils.waitForElement(driver, btnFileMenu, 30);
		btnFileMenu.click();
		Log.message("Clicked on file menu button");
		
		SkySiteUtils.waitForElementLoadTime(driver, addToPrintCart, 30);
		addToPrintCart.click();
		Log.message("Clicked on Add to Print cart button");
		
		SkySiteUtils.waitForElementLoadTime(driver, btnClearSelection, 30);
		WebElement element1 = driver.findElement(By.xpath("//*[text()='Back']"));
	    JavascriptExecutor executor1 = (JavascriptExecutor)driver;
	    executor1.executeScript("arguments[0].click();", element1);
		SkySiteUtils.waitTill(6000);
		
		SkySiteUtils.waitForElementLoadTime(driver, cartItemsCount, 30);
		String count=cartItemsCount.getText();
		int count1= Integer.parseInt(count);
		
		ArrayList<String> obtainedLDFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='pw-file-name']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedLDFileList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Latest Document files count before sorting:- " +obtainedLDFileList.size());		 
		
		if(count1 == obtainedLDFileList.size() )
		{
			Log.message("All files are added to Print Cart");
			
		}
		else
		{
			Log.message("All files are not added to Print Cart");
			
		}
		
		return count1;
		
	}
	
	
	/** 
	 * Method for adding Files in Latest documents to Print Cart and fetching the Files list
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public ArrayList<String> addFilesToCart() 
	{
	
		ArrayList<String> obtainedLDFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h4_docname document-preview-event']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedLDFileList.add(we.getText()); 
		     
			}
		}
		
		Log.message("Obtained files count is:- " +obtainedLDFileList.size());		 
		Log.message("Obtained Files are:- " +obtainedLDFileList);
		
		SkySiteUtils.waitTill(5000);
		
		return obtainedLDFileList;
		
	}

	
	/** 
	 * Method for navigating to print cart
	 * Scripted By: Ranadeep
	 *  
	 */
	public ProjectPrintCartPage enterPrintCart() 
	{
		
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElementLoadTime(driver, btnPrintCart, 30);
		btnPrintCart.click();
		Log.message(" Clicked on Print cart button ");		
		
		return new ProjectPrintCartPage(driver).get();
		
	}

	
}