package com.arc.projects.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arc.projects.utils.randomFileName;
import com.arcautoframe.utils.Log;



public class PunchPage extends LoadableComponent<PunchPage>
{
	
	WebDriver driver;
	private boolean isPageLoaded;
	ProjectDashboardPage projectDashboardPage;
	FolderPage folderPage;
	
	/** Identifying web elements using FindBy annotation*/
	
	@FindBy(xpath="//*[@id='createNewPunch']")
	WebElement btnCreatePunch;
	
	@FindBy(css="#btnCreate")
	WebElement btnCreatePunchPopover;
	
	@FindBy(xpath="//*[@id='closeAddNewPunchStamp']")
	WebElement btnSelectPunchStampDropdown;
	
	@FindBy(xpath="//*[@id='ulStampList']/li/a/label")
	WebElement btnSelectPunchStamp;
	
	//@FindBy(xpath="//*[@id='txtShortDescription']")
	@FindBy(css="#txtShortDescription")
	WebElement textBoxStamp;
	
	//@FindBy(xpath="//*[@id='txtTitleDescription']")
	@FindBy(css="#txtTitleDescription")
	WebElement textBoxStampTitle;
	
	@FindBy(xpath="//*[@id='divaddPunchTitle']/div/div[2]/div/span/button[1]")
	WebElement btnCreatePunchStampOK;
	
	@FindBy(xpath="(//i[@class='icon icon-app-contact icon-lg'])[2]")
	WebElement btnAssignToContactList;
	
	@FindBy(xpath="(//i[@class='icon icon-app-contact icon-lg'])[3]")
	WebElement btnCCContactList;
	
	@FindBy(xpath="(//i[@class='icon icon-app-contact icon-lg'])[1]")
	WebElement btnAssignToPunchAnnotation;
	
	@FindBy(xpath="(//i[@class='icon icon-app-contact icon-lg'])[2]")
	WebElement btnCCPunchAnnotation;
	
	@FindBy(xpath="//*[@id='divProjectUsers']/div/div[3]/button[2]")
	WebElement btnSelectAssignPunchUser;
	
	@FindBy(xpath="//*[@id='divUserListView']/ul/li")
	WebElement selectAssignPunchUser;
	
	@FindBy(xpath="//*[@id='punchDueDate']/span/span")
	WebElement punchDueDate;
	
	@FindBy(xpath="//*[@class='day active today']")
	WebElement activePunchDate;
	
	@FindBy(xpath="//*[@id='ulPunchList']/li[1]/div[2]/div[1]/div[2]/h4/span[1]")
	WebElement punchTitle;
	
	@FindBy(xpath="//*[@id='liActivityMain']/a/span[1]")
	WebElement btnProjectActivity;
	
	@FindBy(xpath="//*[@id='liActivity']/a/span")
	WebElement btnDocumentActivity;
	
	//Create punch
	@FindBy(css = ".dropdown-toggle.aPunchrfimenu")
	WebElement btnPrjManagement;
	
	@FindBy(css = ".aPunch")
	WebElement tabPunch;
	
	@FindBy(css = "#createNewPunch")
	WebElement btncreateNewPunch;
	
	@FindBy(css="#closeAddNewPunchStamp")
	WebElement dropdownAddNewPunchStamp;
	
	@FindBy(xpath="//button[@class='btn btn-default btn-block' and @data-bind='click: AddNewPunchStamp']")
	WebElement btnAddNewPunchStamp;
	
	@FindBy(css=".punch-stamp.punch-stamp-small.pull-right")
	WebElement symbolPunchStamp;
	
	@FindBy(xpath="//input[@class='form-control' and @placeholder='Stamp']")
	WebElement symbolPunchTitle;
	
	@FindBy(css="#txtUserSearchKeyword")
	WebElement contactSearchField;
	
	@FindBy(xpath="//i[@class='icon icon-search icon-lg']")
	WebElement iconSearchSelectUser;
	
	@FindBy(xpath="(//h4[@data-bind='text: UserName()'])[1]")
	WebElement nameFromSearchResult;
	
	@FindBy(xpath="(//a[@data-bind='text: Email()'])[1]")
	WebElement emailFromSearchResult;
	
	@FindBy(xpath="(//input[@class='pull-left'])[1]")
	WebElement checkboxSearchResult;
	
	@FindBy(xpath="//button[@class='btn btn-primary' and @data-bind='click: SelectContactEvent']")
	WebElement btnSelectUser;
	
	@FindBy(css=".glyphicon.glyphicon-calendar.icon-lg.icon.icon-calendar")
	WebElement iconCalenderDueDate;
	
	@FindBy(xpath="//textarea[@data-bind='value: PunchDescription']")
	WebElement txtPunchDescription;
	
	@FindBy(css="#btn-Load-attachment")
	WebElement btnAddAttachment;
	
	@FindBy(css="#btnDraft")
	WebElement btnDraft;
	
	@FindBy(xpath="(//div[@class='punch-cc punch-stamp float-icon pull-left punch-open'])[1]")
	WebElement infoFirstPunchNumber;
	
	@FindBy(xpath="(//div[@class='punch-cc punch-stamp float-icon pull-left punch-draft'])[1]")
	WebElement infoFirstPunchNumberOfDraft;
	
	@FindBy(xpath="(//span[@class='yellow-txt'])[1]")
	WebElement OpenStatus;
	
	@FindBy(xpath="(//span[@class='draft-txt'])[1]")
	WebElement DraftStatus;
	
	@FindBy(xpath="(//strong[@data-bind='text:AttachmentCount()'])[1]")
	WebElement attachmentCount;	
	
	@FindBy(xpath="//a[@id='liThirdTab' and @title='All punchlist']")
	WebElement tabAllPpunchList;
	
	@FindBy(css=".btn.btn-primary.set-files")
	WebElement btnSelectFilesPunchWindow;
	
	@FindBy(css="#lblFileCount")
	WebElement txtNoOfFilesSelectedPunchWindow;
	
	@FindBy(xpath="//span[@data-bind='text: TotalAttachmentCount()']")
	WebElement TotalAttachmentCount;
	
	@FindBy(xpath="//a[@id='punchAttachedFiles']")
	WebElement punchAttachedFiles;
	
	@FindBy(xpath="//button[@id='add-project-file']")
	WebElement btnProjectFiles;
	
	@FindBy(xpath="//i[@class='icon icon-3x icon-folder-close']")
	WebElement folderIconProjectFiles;
	
	@FindBy(xpath="//input[@id='txtDocSearchKeyword']")
	WebElement txtDocSearchProjectFiles;
	
	@FindBy(xpath="//i[@class='icon icon-search icon-lg']")
	WebElement iconDocSearchProjectFiles;
	
	//Edit stamp and delete
	@FindBy(xpath="//button[@id='btnCancel' and @class='btn btn-default pull-left']")
	WebElement btnCancelPunchWindow;
	
	@FindBy(css="#manageStamps")
	WebElement btnmanageStamps;
	
	@FindBy(css="#btnPunchTitleDone")
	WebElement btnOKmanageStamp;
	
	@FindBy(xpath="//input[@id='txtShortDescription']")
	WebElement txtStampNoWhileEdit;
	
	@FindBy(xpath="//input[@id='txtTitleDescription']")
	WebElement txtStampNameWhileEdit;
	
	@FindBy(xpath="//i[@class='icon icon-ok']")
	WebElement iconRightWhileStampEdit;
	
	@FindBy(css="#button-1")
	WebElement btnYES;
	
	@FindBy(xpath="//span[@class='noty_text']")
	WebElement notificationMessage;
	
	//Viewer level punch
	
	@FindBy(xpath="//a[@class='leaflet-draw-draw-punch']")
	WebElement iconPunch_ViewerMenu;
	@FindBy(xpath=".//*[@id='liPunchContainer']/a/b")
	WebElement btnPunchList;
	@FindBy(xpath="(//div[@class='punch-stamp punch-stamp-small punch-open'])[1]")
	WebElement infoFirstPunchNOViewer;
	@FindBy(xpath="(//span[@class='label label-warning pull-right pw-status pw-status-small'])[1]")
	WebElement OpenStatusViewer;
	@FindBy(xpath="(//span[@class='label label-info pull-right pw-status pw-status-small'])[1]")
	WebElement CompletedStatusTxtViewer;
	@FindBy(xpath="(//span[@class='label label-success pull-right pw-status pw-status-small'])[1]")
	WebElement ClosedStatusTxtViewer;
	@FindBy(xpath="(//span[@class='label label-default pull-right pw-status pw-status-small'])[1]")
	WebElement DraftStatusViewer;
	@FindBy(xpath="(//div[@class='punch-stamp punch-stamp-small punch-draft'])[1]")
	WebElement infoDraftPunchNOViewer;
	
	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnCreatePunch, 20);

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		
	}
	
	/**
	 * Declaring constructor for initializing web elements using PageFactory
	 * class.
	 * @param driver
	 * @return
	 */
	public PunchPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	/** 
	 * Method for creating Punch for Activity tracking
	 * Scripted By: Ranadeep
	 * @return 
	 */
	
	public ProjectActivitiesPage createPunch()	 
	{
		SkySiteUtils.waitForElement(driver, btnCreatePunch, 40);
		WebElement element = driver.findElement(By.xpath("//*[@id='createNewPunch']"));
	    JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on create punch button");
		
		SkySiteUtils.waitForElement(driver, btnSelectPunchStampDropdown, 40);
		WebElement element4 = driver.findElement(By.xpath("//*[@id='closeAddNewPunchStamp']"));
	    JavascriptExecutor executor4 = (JavascriptExecutor)driver;
	    executor4.executeScript("arguments[0].click();", element4);
		Log.message("Clicked on select Punch Stamp button dropdown");
	
		SkySiteUtils.waitForElement(driver, btnSelectPunchStamp, 40);		
		WebElement element1 = driver.findElement(By.xpath("//*[@id='ulStampList']/li/a/label"));
	    JavascriptExecutor executor1 = (JavascriptExecutor)driver;
	    executor1.executeScript("arguments[0].click();", element1);
		Log.message("Punch stamp selected");
		
		WebElement element2 = driver.findElement(By.xpath("//*[@id='divPunchAssignee']/div/a/i"));
	    JavascriptExecutor executor2 = (JavascriptExecutor)driver;
	    executor2.executeScript("arguments[0].click();", element2);
		Log.message("Clicked on assign Punch user dropdown button");	
		
		SkySiteUtils.waitForElement(driver, selectAssignPunchUser, 20); 
		WebElement element3 = driver.findElement(By.xpath("//*[@id='divUserListView']/ul/li"));
	    JavascriptExecutor executor3 = (JavascriptExecutor)driver;
	    executor3.executeScript("arguments[0].click();", element3);
		Log.message("Selected assign Punch user");
		
		SkySiteUtils.waitForElement(driver, btnSelectAssignPunchUser, 20);
		WebElement element7 = driver.findElement(By.xpath("//*[@id='divProjectUsers']/div/div[3]/button[2]"));
	    JavascriptExecutor executor7 = (JavascriptExecutor)driver;
	    executor7.executeScript("arguments[0].click();", element7);
		Log.message("Clicked on Select User button");
		
		SkySiteUtils.waitForElement(driver, punchDueDate, 20);
		punchDueDate.click();
		Log.message("Clicked to select punch due date");
		
		SkySiteUtils.waitForElement(driver, activePunchDate, 20);
		activePunchDate.click();
		Log.message("Punch due date selected");
		
		SkySiteUtils.waitForElement(driver, btnCreatePunchPopover, 20);
		btnCreatePunchPopover.click();
		Log.message("Clicked on create Punch button");
		SkySiteUtils.waitTill(8000);
		
		
		String expectedPunchStampTitle = PropertyReader.getProperty("StampTitle");
		Log.message("Expected Stamp Title is:-" + expectedPunchStampTitle);
		
		SkySiteUtils.waitForElement(driver, punchTitle, 20);
		String actualPunchTitle=punchTitle.getText();
		Log.message("The created punch title is: " +actualPunchTitle);
		
		if(actualPunchTitle.equals(expectedPunchStampTitle))
		{
			Log.message("Punch created successfully");
			
			SkySiteUtils.waitForElement(driver, btnProjectActivity, 20);
			WebElement element5 = driver.findElement(By.xpath("//*[@id='liActivityMain']/a/span[1]"));
		    JavascriptExecutor executor5 = (JavascriptExecutor)driver;
		    executor5.executeScript("arguments[0].click();", element5);
			Log.message("Clicked on Project Activity button");
			
			SkySiteUtils.waitTill(5000);
			
			SkySiteUtils.waitForElement(driver, btnDocumentActivity, 20);
			WebElement element6 = driver.findElement(By.xpath("//*[@id='liActivity']/a/span"));
		    JavascriptExecutor executor6 = (JavascriptExecutor)driver;
		    executor6.executeScript("arguments[0].click();", element6);
			Log.message("Clicked on Document Activity button");
			
			
			return new ProjectActivitiesPage(driver).get();	
		}		
		else
		{
			Log.message("Punch creation failed");
			return null;
		}
			
	}
	
	
	/**
	 * Method written for Select Punch List from Project Management Tab
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public FolderPage SelectPunchList() 
	{
		SkySiteUtils.waitForElement(driver, btnPrjManagement, 20);
		btnPrjManagement.click();
		Log.message("Clicked on Project Management tab.");
		SkySiteUtils.waitForElement(driver, tabPunch, 20);
		tabPunch.click();
		Log.message("Clicked on Punch List tab.");
		SkySiteUtils.waitForElement(driver, btncreateNewPunch, 20);

		if (btncreateNewPunch.isDisplayed())
			Log.message("Create new Punch button is available.");
		return new FolderPage(driver).get();
	}
	
	/**
	 * Method written for Create Punch without attachments 
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Create_Punch_NoAttachments_AndValidate(String Stamp_Number,String Employee_Name,String MailId, String CC_UserName, String Punch_Description) 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;
		
		SkySiteUtils.waitForElement(driver, btncreateNewPunch, 30);
		btncreateNewPunch.click();
		Log.message("Clicked on create punch button.");
		SkySiteUtils.waitForElement(driver, btnCreatePunchPopover, 30);
		SkySiteUtils.waitTill(2000);
		dropdownAddNewPunchStamp.click();
		Log.message("Clicked on drop down button of stamp");
		SkySiteUtils.waitTill(2000);
		btnAddNewPunchStamp.click();
		Log.message("Clicked on Add new button");
		SkySiteUtils.waitTill(2000);
		textBoxStamp.sendKeys(Stamp_Number);
		textBoxStampTitle.sendKeys(Stamp_Number);
		btnCreatePunchStampOK.click();
		Log.message("Clicked on Right symbol After entered Stamp number and title.");
		SkySiteUtils.waitForElement(driver, symbolPunchStamp, 30);
		SkySiteUtils.waitTill(2000);
		String PunchNumber_AfterCreate= symbolPunchStamp.getText();
		String PunchName_AfterCreate = symbolPunchTitle.getAttribute("value");
		//Validate After Stamp Create	
		if((PunchNumber_AfterCreate.contentEquals(Stamp_Number)) && (PunchName_AfterCreate.contentEquals(Stamp_Number)))
		{
			result1=true;
			Log.message("Stamp Created Successfully with name: "+Stamp_Number);
			btnAssignToContactList.click();
			Log.message("Clicked on Assign to contact icon.");
			SkySiteUtils.waitForElement(driver, contactSearchField, 30);
			SkySiteUtils.waitTill(2000);
			contactSearchField.sendKeys(Employee_Name);
			iconSearchSelectUser.click();
			Log.message("Clicked on Search icon by entering input");
			SkySiteUtils.waitTill(3000);
			String Name_AfterSearch = nameFromSearchResult.getText();
			Log.message("Name After Search: "+Name_AfterSearch);
			String Mail_AfterSearch = emailFromSearchResult.getText();
			Log.message("Mail After Search: "+Mail_AfterSearch);
			if((Name_AfterSearch.contains(Employee_Name)) && (Mail_AfterSearch.contentEquals(MailId)))
			{
				Log.message("Search Results are coming properly!!!");
				checkboxSearchResult.click();
				btnSelectUser.click();
				Log.message("Clicked on Select user.");
				SkySiteUtils.waitTill(5000);
				//Adding CC user
				btnCCContactList.click();
				Log.message("Clicked on CC contact icon.");
				SkySiteUtils.waitForElement(driver, contactSearchField, 30);
				SkySiteUtils.waitTill(2000);
				contactSearchField.sendKeys(CC_UserName);
				iconSearchSelectUser.click();
				Log.message("Clicked on Search icon by entering input");
				SkySiteUtils.waitTill(5000);
				checkboxSearchResult.click();
				btnSelectUser.click();
				Log.message("Clicked on Select user.");
				SkySiteUtils.waitTill(2000);
				iconCalenderDueDate.click();
				Log.message("Clicked on Due Date Calendar icon.");
				SkySiteUtils.waitTill(2000);
				driver.findElement(By.xpath("//div/div[1]/table/thead/tr[1]/th[3]")).click();//Click on next month
				SkySiteUtils.waitTill(2000);
				driver.findElement(By.xpath("(//div/div[1]/table/tbody/tr[2]/td[4])[2]")).click();//Pick date
				SkySiteUtils.waitTill(2000);
				txtPunchDescription.sendKeys(Punch_Description);
				SkySiteUtils.waitTill(2000);
				btnCreatePunchPopover.click();
				Log.message("clicked on create punch button after enter required details.");
				SkySiteUtils.waitTill(10000);
				String Punch_Number_AfterCreate=infoFirstPunchNumber.getText();
				Log.message("Punch Number After Create: "+Punch_Number_AfterCreate);			
				String AssignedTo=driver.findElement(By.xpath("//ul[@id='ulPunchList']/li[1]/div[2]/div[2]/h4[2]/span")).getText();
				Log.message("Assigned To: "+AssignedTo);
				String Status_AfterCreate = OpenStatus.getText();
				Log.message("Status After Create: "+Status_AfterCreate);
				String Attachment_Count = attachmentCount.getText();
				Log.message("Attchment count is: "+Attachment_Count);
				
				if((Attachment_Count.contentEquals("0")) && (Punch_Number_AfterCreate.contentEquals(Stamp_Number)) 
						&& (AssignedTo.contains(Employee_Name))	&& (Status_AfterCreate.equalsIgnoreCase("Open")))
				{
					result2=true;
					Log.message("Punch Created successfully in MyPunchList with name: "+Stamp_Number);
					//Validations from All Punch List Tab
					tabAllPpunchList.click();
					Log.message("Clicked on All Punch List tab.");
					SkySiteUtils.waitTill(10000);
					Punch_Number_AfterCreate=infoFirstPunchNumber.getText();
					Log.message("Punch Number After Create: "+Punch_Number_AfterCreate);			
					AssignedTo=driver.findElement(By.xpath("//ul[@id='ulPunchList']/li[1]/div[2]/div[2]/h4[2]/span")).getText();
					Log.message("Assigned To: "+AssignedTo);
					Status_AfterCreate = OpenStatus.getText();
					Log.message("Status After Create: "+Status_AfterCreate);
					Attachment_Count = attachmentCount.getText();
					Log.message("Attchment count is: "+Attachment_Count);
					
					if((Attachment_Count.contentEquals("0")) && (Punch_Number_AfterCreate.contentEquals(Stamp_Number)) 
							&& (AssignedTo.contains(Employee_Name))	&& (Status_AfterCreate.equalsIgnoreCase("Open")))
					{
						result3=true;
						Log.message("Punch Created successfully in AllPunchList with name: "+Stamp_Number);
					}
					else
					{
						result3=false;
						Log.message("Punch Creation Failed in AllPunchList with name: "+Stamp_Number);
					}
				}
				else
				{
					result2=false;
					Log.message("Punch Creation Failed in MyPunchList with name: "+Stamp_Number);
				}
			}
						
		}
		else
		{
			result1=false;
			Log.message("Stamp Creation Failed with name: "+Stamp_Number);
		}

		if ((result1==true) && (result2==true) && (result3==true)) 
		{
			Log.message("Creating a punch and validate is working successfully.");
			return true;
		} else {
			Log.message("Creating a punch and validate is failed.");
			return false;
		}
	}
	
	/**
	 * Method written for Create Punch with attachments(Internal and External)
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Create_Punch_WithAttachments_AndValidate(String Stamp_Number,String Employee_Name,String MailId, String FolderPath, String Punch_Description,String File_Name) throws IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;
		
		SkySiteUtils.waitForElement(driver, btncreateNewPunch, 30);
		btncreateNewPunch.click();
		Log.message("Clicked on create punch button.");
		SkySiteUtils.waitForElement(driver, btnCreatePunchPopover, 30);
		SkySiteUtils.waitTill(2000);
		dropdownAddNewPunchStamp.click();
		Log.message("Clicked on drop down button of stamp");
		SkySiteUtils.waitTill(2000);
		btnAddNewPunchStamp.click();
		Log.message("Clicked on Add new button");
		SkySiteUtils.waitTill(2000);
		textBoxStamp.sendKeys(Stamp_Number);
		textBoxStampTitle.sendKeys(Stamp_Number);
		btnCreatePunchStampOK.click();
		Log.message("Clicked on Right symbol After entered Stamp number and title.");
		SkySiteUtils.waitForElement(driver, symbolPunchStamp, 30);
		SkySiteUtils.waitTill(2000);
		String PunchNumber_AfterCreate= symbolPunchStamp.getText();
		String PunchName_AfterCreate = symbolPunchTitle.getAttribute("value");
		//Validate After Stamp Create	
		if((PunchNumber_AfterCreate.contentEquals(Stamp_Number)) && (PunchName_AfterCreate.contentEquals(Stamp_Number)))
		{
			result1=true;
			Log.message("Stamp Created Successfully with name: "+Stamp_Number);
			btnAssignToContactList.click();
			Log.message("Clicked on Assign to contact icon.");
			SkySiteUtils.waitForElement(driver, contactSearchField, 30);
			SkySiteUtils.waitTill(2000);
			contactSearchField.sendKeys(Employee_Name);
			iconSearchSelectUser.click();
			Log.message("Clicked on Search icon by entering input");
			SkySiteUtils.waitTill(3000);
			String Name_AfterSearch = nameFromSearchResult.getText();
			Log.message("Name After Search: "+Name_AfterSearch);
			String Mail_AfterSearch = emailFromSearchResult.getText();
			Log.message("Mail After Search: "+Mail_AfterSearch);
			if((Name_AfterSearch.contains(Employee_Name)) && (Mail_AfterSearch.contentEquals(MailId)))
			{
				Log.message("Search Results are coming properly!!!");
				checkboxSearchResult.click();
				btnSelectUser.click();
				Log.message("Clicked on Select user.");
				SkySiteUtils.waitTill(2000);
				iconCalenderDueDate.click();
				Log.message("Clicked on Due Date Calendar icon.");
				SkySiteUtils.waitTill(2000);
				driver.findElement(By.xpath("//div/div[1]/table/thead/tr[1]/th[3]")).click();//Click on next month
				SkySiteUtils.waitTill(2000);
				driver.findElement(By.xpath("(//div/div[1]/table/tbody/tr[2]/td[4])[2]")).click();//Pick date
				SkySiteUtils.waitTill(2000);
				txtPunchDescription.sendKeys(Punch_Description);
				SkySiteUtils.waitTill(2000);
				btnAddAttachment.click();
				Log.message("Clicked on Add Attachment button.");
				SkySiteUtils.waitTill(10000);//Added due to loading problem
				SkySiteUtils.waitForElement(driver, btnSelectFilesPunchWindow, 20);
				btnSelectFilesPunchWindow.click();
				Log.message("Clicked on Select files button.");
				SkySiteUtils.waitTill(10000);
				// Writing File names into a text file for using in AutoIT Script
				BufferedWriter output;
				randomFileName rn = new randomFileName();
				String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
				output = new BufferedWriter(new FileWriter(tmpFileName, true));
				String expFilename = null;
				File[] files = new File(FolderPath).listFiles();
				for (File file : files) {
					if (file.isFile()) {
						expFilename = file.getName();// Getting File Names into a variable
						Log.message("Expected File name is:" + expFilename);
						output.append('"' + expFilename + '"');
						output.append(" ");
						SkySiteUtils.waitTill(1000);
					}
				}
				output.flush();
				output.close();

				// Executing .exe autoIt file
				String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
				Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
				Log.message("AutoIT Script Executed!!");
				SkySiteUtils.waitTill(10000);			
				SkySiteUtils.waitForElement(driver, txtNoOfFilesSelectedPunchWindow, 30);
				try
				{
					 File file = new File(tmpFileName);
					 if(file.delete())
					 {
						 Log.message(file.getName() + " is deleted!");
					 }
					 else
					 {
						 Log.message("Delete operation is failed.");
					 }
				}
				catch(Exception e)
				{
					Log.message("Exception occured!!!"+e);	
				}
				SkySiteUtils.waitTill(5000);		 
				String Attachment_Count=TotalAttachmentCount.getText();
				Log.message("Attachment Count: "+Attachment_Count);
				if(Attachment_Count.equals("3"))
				{
					Log.message("Files are selected to grid!!!");
					punchAttachedFiles.click();
					Log.message("clicked on dropdown attachment files.");
					SkySiteUtils.waitTill(2000);
					driver.findElement(By.xpath("(//i[@class='icon icon-remove'])[4]")).click();
					Log.message("Removing third file from the list.");
					SkySiteUtils.waitTill(2000);
					Attachment_Count=TotalAttachmentCount.getText();
					Log.message("Attachment Count after remove: "+Attachment_Count);
					if(Attachment_Count.equals("2"))
					{
						Log.message("Attachment got removes successfully.");
						btnProjectFiles.click();
						Log.message("Clciked on project files button");
						SkySiteUtils.waitTill(2000);
						SkySiteUtils.waitForElement(driver, folderIconProjectFiles, 20);
						folderIconProjectFiles.click();
						Log.message("Clicked on Folder available in project.");
						SkySiteUtils.waitForElement(driver, txtDocSearchProjectFiles, 20);
						SkySiteUtils.waitTill(2000);
						txtDocSearchProjectFiles.sendKeys(File_Name);
						iconDocSearchProjectFiles.click();
						SkySiteUtils.waitTill(10000);
						driver.findElement(By.xpath("(//input[@type='checkbox'])[1]")).click();//Select checkbox
						SkySiteUtils.waitTill(2000);
						btnCreatePunchPopover.click();
						Log.message("clicked on create punch button after enter required details.");
						SkySiteUtils.waitTill(20000);
						String Punch_Number_AfterCreate=infoFirstPunchNumber.getText();
						Log.message("Punch Number After Create: "+Punch_Number_AfterCreate);			
						String AssignedTo=driver.findElement(By.xpath("//ul[@id='ulPunchList']/li[1]/div[2]/div[2]/h4[2]/span")).getText();
						Log.message("Assigned To: "+AssignedTo);
						String Status_AfterCreate = OpenStatus.getText();
						Log.message("Status After Create: "+Status_AfterCreate);
						Attachment_Count = attachmentCount.getText();
						Log.message("Attchment count is: "+Attachment_Count);
						
						if((Attachment_Count.contentEquals("3")) && (Punch_Number_AfterCreate.contentEquals(Stamp_Number)) 
								&& (AssignedTo.contains(Employee_Name))	&& (Status_AfterCreate.equalsIgnoreCase("Open")))
						{
							result2=true;
							Log.message("Punch Created successfully in MyPunchList with name: "+Stamp_Number);
							//Validations from All Punch List Tab
							tabAllPpunchList.click();
							Log.message("Clicked on All Punch List tab.");
							SkySiteUtils.waitTill(10000);
							Punch_Number_AfterCreate=infoFirstPunchNumber.getText();
							Log.message("Punch Number After Create: "+Punch_Number_AfterCreate);			
							AssignedTo=driver.findElement(By.xpath("//ul[@id='ulPunchList']/li[1]/div[2]/div[2]/h4[2]/span")).getText();
							Log.message("Assigned To: "+AssignedTo);
							Status_AfterCreate = OpenStatus.getText();
							Log.message("Status After Create: "+Status_AfterCreate);
							Attachment_Count = attachmentCount.getText();
							Log.message("Attchment count is: "+Attachment_Count);
							
							if((Attachment_Count.contentEquals("3")) && (Punch_Number_AfterCreate.contentEquals(Stamp_Number)) 
									&& (AssignedTo.contains(Employee_Name))	&& (Status_AfterCreate.equalsIgnoreCase("Open")))
							{
								result3=true;
								Log.message("Punch Created successfully in AllPunchList with name: "+Stamp_Number);
							}
							else
							{
								result3=false;
								Log.message("Punch Creation Failed in AllPunchList with name: "+Stamp_Number);
							}
						}
						else
						{
							result2=false;
							Log.message("Punch Creation Failed in MyPunchList with name: "+Stamp_Number);
						}
					}
						
				}
				
			}
								
		}
		else
		{
			result1=false;
			Log.message("Stamp Creation Failed with name: "+Stamp_Number);
		}

		if ((result1==true) && (result2==true) && (result3==true)) 
		{
			Log.message("Creating a punch and validate is working successfully.");
			return true;
		} else {
			Log.message("Creating a punch and validate is failed.");
			return false;
		}
	}
	
	/**
	 * Method written for Create Punch with attachments(From the computer)
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Create_Punch_WithAttachments_FromOutside(String Stamp_Number,String Employee_Name,String MailId, String FolderPath, String Punch_Description) throws IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		
		SkySiteUtils.waitForElement(driver, btncreateNewPunch, 30);
		btncreateNewPunch.click();
		Log.message("Clicked on create punch button.");
		SkySiteUtils.waitForElement(driver, btnCreatePunchPopover, 30);
		SkySiteUtils.waitTill(2000);
		dropdownAddNewPunchStamp.click();
		Log.message("Clicked on drop down button of stamp");
		SkySiteUtils.waitTill(2000);
		btnAddNewPunchStamp.click();
		Log.message("Clicked on Add new button");
		SkySiteUtils.waitTill(2000);
		textBoxStamp.sendKeys(Stamp_Number);
		textBoxStampTitle.sendKeys(Stamp_Number);
		btnCreatePunchStampOK.click();
		Log.message("Clicked on Right symbol After entered Stamp number and title.");
		SkySiteUtils.waitForElement(driver, symbolPunchStamp, 30);
		SkySiteUtils.waitTill(2000);
		String PunchNumber_AfterCreate= symbolPunchStamp.getText();
		String PunchName_AfterCreate = symbolPunchTitle.getAttribute("value");
		//Validate After Stamp Create	
		if((PunchNumber_AfterCreate.contentEquals(Stamp_Number)) && (PunchName_AfterCreate.contentEquals(Stamp_Number)))
		{
			result1=true;
			Log.message("Stamp Created Successfully with name: "+Stamp_Number);
			btnAssignToContactList.click();
			Log.message("Clicked on Assign to contact icon.");
			SkySiteUtils.waitForElement(driver, contactSearchField, 30);
			SkySiteUtils.waitTill(2000);
			contactSearchField.sendKeys(Employee_Name);
			iconSearchSelectUser.click();
			Log.message("Clicked on Search icon by entering input");
			SkySiteUtils.waitTill(3000);
			String Name_AfterSearch = nameFromSearchResult.getText();
			Log.message("Name After Search: "+Name_AfterSearch);
			String Mail_AfterSearch = emailFromSearchResult.getText();
			Log.message("Mail After Search: "+Mail_AfterSearch);
			if((Name_AfterSearch.contains(Employee_Name)) && (Mail_AfterSearch.contentEquals(MailId)))
			{
				Log.message("Search Results are coming properly!!!");
				checkboxSearchResult.click();
				btnSelectUser.click();
				Log.message("Clicked on Select user.");
				SkySiteUtils.waitTill(2000);
				iconCalenderDueDate.click();
				Log.message("Clicked on Due Date Calendar icon.");
				SkySiteUtils.waitTill(2000);
				driver.findElement(By.xpath("//div/div[1]/table/thead/tr[1]/th[3]")).click();//Click on next month
				SkySiteUtils.waitTill(2000);
				driver.findElement(By.xpath("(//div/div[1]/table/tbody/tr[2]/td[4])[2]")).click();//Pick date
				SkySiteUtils.waitTill(2000);
				txtPunchDescription.sendKeys("Creating Punch with attachments - Internal & External.");
				SkySiteUtils.waitTill(2000);
				btnAddAttachment.click();
				Log.message("Clicked on Add Attachment button.");
				SkySiteUtils.waitForElement(driver, btnSelectFilesPunchWindow, 20);
				SkySiteUtils.waitTill(5000);
				btnSelectFilesPunchWindow.click();
				Log.message("Clicked on Select files button.");
				SkySiteUtils.waitTill(10000);
				// Writing File names into a text file for using in AutoIT Script
				BufferedWriter output;
				randomFileName rn = new randomFileName();
				String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
				output = new BufferedWriter(new FileWriter(tmpFileName, true));
				String expFilename = null;
				File[] files = new File(FolderPath).listFiles();
				for (File file : files) {
					if (file.isFile()) {
						expFilename = file.getName();// Getting File Names into a variable
						Log.message("Expected File name is:" + expFilename);
						output.append('"' + expFilename + '"');
						output.append(" ");
						SkySiteUtils.waitTill(1000);
					}
				}
				output.flush();
				output.close();

				// Executing .exe autoIt file
				String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
				Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
				Log.message("AutoIT Script Executed!!");
				SkySiteUtils.waitTill(10000);			
				SkySiteUtils.waitForElement(driver, txtNoOfFilesSelectedPunchWindow, 30);
				try
				{
					 File file = new File(tmpFileName);
					 if(file.delete())
					 {
						 Log.message(file.getName() + " is deleted!");
					 }
					 else
					 {
						 Log.message("Delete operation is failed.");
					 }
				}
				catch(Exception e)
				{
					Log.message("Exception occured!!!"+e);	
				}
				SkySiteUtils.waitTill(5000);		 
				String Attachment_Count=TotalAttachmentCount.getText();
				Log.message("Attachment Count: "+Attachment_Count);
				btnCreatePunchPopover.click();
				Log.message("clicked on create punch button after enter required details.");
				SkySiteUtils.waitTill(20000);
				String Punch_Number_AfterCreate=infoFirstPunchNumber.getText();
				Log.message("Punch Number After Create: "+Punch_Number_AfterCreate);			
				String AssignedTo=driver.findElement(By.xpath("//ul[@id='ulPunchList']/li[1]/div[2]/div[2]/h4[2]/span")).getText();
				Log.message("Assigned To: "+AssignedTo);
				String Status_AfterCreate = OpenStatus.getText();
				Log.message("Status After Create: "+Status_AfterCreate);
				Attachment_Count = attachmentCount.getText();
				Log.message("Attchment count is: "+Attachment_Count);
						
				if((Punch_Number_AfterCreate.contentEquals(Stamp_Number)) 
						&& (AssignedTo.contains(Employee_Name))	&& (Status_AfterCreate.equalsIgnoreCase("Open")))
				{
					result2=true;
					Log.message("Punch Created successfully in MyPunchList with name: "+Stamp_Number);
				}
				else
				{
					result2=false;
					Log.message("Punch Creation Failed in MyPunchList with name: "+Stamp_Number);
				}
			}
								
		}
		else
		{
			result1=false;
			Log.message("Stamp Creation Failed with name: "+Stamp_Number);
		}

		if ((result1==true) && (result2==true)) 
		{
			Log.message("Creating a punch by attaching photo is working successfully.");
			return true;
		} else {
			Log.message("Creating a punch by attaching photo is failed.");
			return false;
		}
	}
	
	
	/**
	 * Method written for Create, Edit and Delete stamp from Manage Stamp
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Create_Edit_DeleteStamp_FromManageStamp(String Stamp_Number, String Stamp_Creater) 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;
		boolean result4 = false;
		
		SkySiteUtils.waitForElement(driver, btncreateNewPunch, 30);
		btncreateNewPunch.click();
		Log.message("Clicked on create punch button.");
		SkySiteUtils.waitForElement(driver, btnCreatePunchPopover, 30);
		SkySiteUtils.waitTill(2000);
		dropdownAddNewPunchStamp.click();
		Log.message("Clicked on drop down button of stamp");
		SkySiteUtils.waitTill(2000);
		btnAddNewPunchStamp.click();
		Log.message("Clicked on Add new button");
		SkySiteUtils.waitTill(2000);
		textBoxStamp.sendKeys(Stamp_Number);
		textBoxStampTitle.sendKeys(Stamp_Number);
		btnCreatePunchStampOK.click();
		Log.message("Clicked on Right symbol After entered Stamp number and title.");
		SkySiteUtils.waitForElement(driver, symbolPunchStamp, 30);
		SkySiteUtils.waitTill(2000);
		String PunchNumber_AfterCreate= symbolPunchStamp.getText();
		String PunchName_AfterCreate = symbolPunchTitle.getAttribute("value");
		//Validate After Stamp Create	
		if((PunchNumber_AfterCreate.contentEquals(Stamp_Number)) && (PunchName_AfterCreate.contentEquals(Stamp_Number)))
		{
			result1=true;
			Log.message("Stamp Created Successfully with name: "+Stamp_Number);
			btnCancelPunchWindow.click();
			Log.message("Clicked on cancel button of Create Punch window.");
			SkySiteUtils.waitTill(2000);
			btnmanageStamps.click();
			Log.message("Clicked on Manage Stamp button.");
			SkySiteUtils.waitForElement(driver, btnOKmanageStamp, 30);
			SkySiteUtils.waitTill(5000);
			//Counting available Stamps
			int Stamp_Count=0;
			int i = 1;
			List<WebElement> allElements = driver.findElements(By.xpath("//div[@class='punch-stamp punch-open']"));
			for (WebElement Element : allElements)
			{ 
				Stamp_Count = Stamp_Count+1; 	
			}
			Log.message("Number of stamps are: "+Stamp_Count);
			
			for(i=1;i<=Stamp_Count;i++)
			{
				String StampNumber_ManageStamp=driver.findElement(By.xpath("(//div[@class='punch-stamp punch-open'])["+i+"]")).getText();
				String StampName_ManageStamp=driver.findElement(By.xpath("//div[2]/div[2]/ul/li["+i+"]/div/div[2]/h4")).getText();
				String Stamp_CreatedBy=driver.findElement(By.xpath("//div[2]/div[2]/ul/li["+i+"]/div/div[2]/h5/span[1]")).getText();
				if((StampNumber_ManageStamp.contentEquals(Stamp_Number)) && (StampName_ManageStamp.contentEquals(Stamp_Number))
						&& (Stamp_CreatedBy.contains(Stamp_Creater)))
				{
					result2=true;
					break;
				}
				
			}
			if(result2==true)
			{
				Log.message("Expected Stamp is available: "+Stamp_Number);
				driver.findElement(By.xpath("(//i[@class='icon icon-edit icon-lg'])["+i+"]")).click();
				Log.message("Clicked on required stamp edit icon.");
				SkySiteUtils.waitTill(3000);
				String StmNumber_WhileEdit=txtStampNoWhileEdit.getAttribute("value");
				String StmName_WhileEdit=txtStampNameWhileEdit.getAttribute("value");
				if((StmNumber_WhileEdit.contentEquals(Stamp_Number)) && (StmName_WhileEdit.contentEquals(Stamp_Number)))
				{
					Log.message("Details are proper for Editing Stamp!!!");
					txtStampNoWhileEdit.clear();
					txtStampNoWhileEdit.sendKeys("abc");
					txtStampNameWhileEdit.clear();
					txtStampNameWhileEdit.sendKeys("abc");
					iconRightWhileStampEdit.click();
					Log.message("Enter new stamp details and click on Right icon.");
					SkySiteUtils.waitTill(5000);
					//Counting available Stamps after edit a stamp
					Stamp_Count=0;
					int j = 1;
					List<WebElement> allElements1 = driver.findElements(By.xpath("//div[@class='punch-stamp punch-open']"));
					for (WebElement Element : allElements1)
					{ 
						Stamp_Count = Stamp_Count+1; 	
					}
					Log.message("Number of stamps after edit is: "+Stamp_Count);
					
					for(j=1;j<=Stamp_Count;j++)
					{
						String StampNumber_AfterEdit=driver.findElement(By.xpath("(//div[@class='punch-stamp punch-open'])["+j+"]")).getText();
						String StampName_AfterEdit=driver.findElement(By.xpath("//div[2]/div[2]/ul/li["+j+"]/div/div[2]/h4")).getText();
							
						if((StampNumber_AfterEdit.equalsIgnoreCase("abc")) && (StampName_AfterEdit.equalsIgnoreCase("abc")))
						{
							result3=true;
							break;
						}
						
					}
				
					if(result3=true)
					{
						Log.message("Stamp is Edited Successfully from: "+Stamp_Number+" To: abc");
						//Going to delete stamp
						driver.findElement(By.xpath("(//i[@class='icon icon-trash icon-lg'])["+j+"]")).click();
						Log.message("Clicked on delete icon of expected stamp.");
						SkySiteUtils.waitForElement(driver, btnYES, 20);
						btnYES.click();
						SkySiteUtils.waitTill(1000);
						SkySiteUtils.waitForElement(driver, notificationMessage, 20);
						String Notifi_MsgAfterDelete=notificationMessage.getText();
						Log.message("Notifi Msg After Delete: "+Notifi_MsgAfterDelete);
						
						if(Stamp_Count==1)
						{
							if((Notifi_MsgAfterDelete.contentEquals("Stamp deleted successfully")))
							{
								result4=true;
								Log.message("Stamp is Deleted Successfully!!!");
								SkySiteUtils.waitTill(5000);//Page load issue
								btnOKmanageStamp.click();//Close Manage Stamp window
							}
						}
						if(Stamp_Count>1)
						{
							//Counting available Stamps
							int Stamp_Count_AfterDelete=0;
							List<WebElement> allElements2 = driver.findElements(By.xpath("//div[@class='punch-stamp punch-open']"));
							for (WebElement Element : allElements2)
							{ 
								Stamp_Count_AfterDelete = Stamp_Count_AfterDelete+1; 	
							}
							Log.message("Number of stamps After Delete a stamp is: "+Stamp_Count_AfterDelete);
							
							if((Notifi_MsgAfterDelete.contentEquals("Stamp deleted successfully")) && (Stamp_Count_AfterDelete==Stamp_Count-1))
							{
								result4=true;
								Log.message("Stamp is Deleted Successfully!!!");
								btnOKmanageStamp.click();//Close Manage Stamp window
							}
							else
							{
								result4=false;
								Log.message("Stamp is Failed to Delete!!!");
							}
						}
																	
					}
					else
					{
						result3=false;
						Log.message("Stamp is Failed to Edit from: "+Stamp_Number+" To: abc");
					}
				}
			}
			else
			{
				result2=false;
				Log.message("Expected Stamp is NOT available: "+Stamp_Number);
			}			
		}
		else
		{
			result1=false;
			Log.message("Stamp Creation Failed with name: "+Stamp_Number);
		}

		if ((result1==true) && (result2==true) && (result3==true) && (result4==true)) 
		{
			Log.message("Creating a stamp, Edit & Delete is working successfully.");
			return true;
		} else {
			Log.message("Creating a stamp, Edit & Delete is failed.");
			return false;
		}
	}
	
	/**
	 * Method written for verify punch from receiver side
	 * Scipted By: Naresh Babu
	 * @return
	 */
	@FindBy(css="#liSecondTab")
	WebElement tabPunchlistAssignToMe;
	
	@FindBy(xpath="(//span[@data-bind='text: PunchDescription()'])[1]")
	WebElement punchDescription;
	
	public boolean Verify_ExpectedPunch_FromReceiver(String Stamp_Number, String Punch_Creater,String Punch_Description, String Attachment_Count) 
	{
		boolean result1 = false;
		boolean result2 = false;
		
		SkySiteUtils.waitForElement(driver, btncreateNewPunch, 30);
		tabPunchlistAssignToMe.click();
		Log.message("Clicked on Punch list assigned to me tab.");
		SkySiteUtils.waitForElement(driver, infoFirstPunchNumber, 30);
		SkySiteUtils.waitTill(5000);
		String Punch_Number_AfterCreate=infoFirstPunchNumber.getText();
		Log.message("Punch Number After Create: "+Punch_Number_AfterCreate);			
		String AssignedBy=driver.findElement(By.xpath("//ul[@id='ulPunchList']/li[1]/div[2]/div[2]/h4[2]/span")).getText();
		Log.message("Assigned By: "+AssignedBy);
		String Status_AfterCreate = OpenStatus.getText();
		Log.message("Status After Create: "+Status_AfterCreate);
		String Attachment_Count_App = attachmentCount.getText();
		Log.message("Attchment count is: "+Attachment_Count_App);
		String Actual_Description = punchDescription.getText();
		Log.message("Punch Description is: "+Actual_Description);
		
		if((Attachment_Count_App.contentEquals(Attachment_Count)) && (Punch_Number_AfterCreate.contentEquals(Stamp_Number)) 
				&& (AssignedBy.contains(Punch_Creater))	&& (Status_AfterCreate.equalsIgnoreCase("Open"))
				&& (Actual_Description.contentEquals(Punch_Description)))
		{
			result1=true;
			Log.message("Expected Punch available under punch list assigned to me list: "+Stamp_Number);
			//Validations from All Punch List Tab
			tabAllPpunchList.click();
			Log.message("Clicked on All Punch List tab.");
			SkySiteUtils.waitTill(10000);
			Punch_Number_AfterCreate=infoFirstPunchNumber.getText();
			Log.message("Punch Number After Create: "+Punch_Number_AfterCreate);			
			AssignedBy=driver.findElement(By.xpath("//ul[@id='ulPunchList']/li[1]/div[2]/div[2]/h4[2]/span")).getText();
			Log.message("Assigned By: "+AssignedBy);
			Status_AfterCreate = OpenStatus.getText();
			Log.message("Status After Create: "+Status_AfterCreate);
			Attachment_Count_App = attachmentCount.getText();
			Log.message("Attchment count is: "+Attachment_Count_App);
			
			if((Attachment_Count_App.contentEquals(Attachment_Count)) && (Punch_Number_AfterCreate.contentEquals(Stamp_Number)) 
					&& (AssignedBy.contains(Punch_Creater))	&& (Status_AfterCreate.equalsIgnoreCase("Open")))
			{
				result2=true;
				Log.message("Expected Punch available under All punch list: "+Stamp_Number);
			}
			else
			{
				result2=false;
				Log.message("Expected Punch is not available under All punch list: "+Stamp_Number);
			}
		}
		else
		{
			result1=false;
			Log.message("Expected Punch is NOT available in receiver side.");
		}

		if ((result1==true) && (result2==true)) 
		{
			Log.message("Expected punch is available for user.");
			return true;
		} else {
			Log.message("Expected punch is NOT available for user.");
			return false;
		}
	}
	
	/**
	 * Method written for Select fist punch under punch list assign to me from receiver side
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_SelectFistPunch_UnderAssignToMeTab_FromReceivr() 
	{	
		SkySiteUtils.waitForElement(driver, btncreateNewPunch, 30);
		tabPunchlistAssignToMe.click();
		Log.message("Clicked on Punch list assigned to me tab.");
		SkySiteUtils.waitForElement(driver, infoFirstPunchNumber, 30);
		SkySiteUtils.waitTill(10000);
		infoFirstPunchNumber.click();
		Log.message("Clicked on punch stamp icon to open the details.");
		SkySiteUtils.waitForElement(driver, btnCompletePunchDetails, 30);
		SkySiteUtils.waitTill(3000);
		if (btnCompletePunchDetails.isDisplayed()) 
		{
			Log.message("First punch is Selectes successfully.");
			return true;
		} else {
			Log.message("First punch is failed to select.");
			return false;
		}
	}
	
	/**
	 * Method written for Select fist punch from creater side
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_SelectFistPunch_FromCreater() 
	{	
		SkySiteUtils.waitForElement(driver, btncreateNewPunch, 30);
		//infoFirstPunchNumber.click();
		driver.findElement(By.xpath("(//li[@class='update-punch has-tooltip'])[1]")).click();
		Log.message("Clicked on punch stamp icon to open the details.");
		SkySiteUtils.waitForElement(driver, txtCommentPunchDetails, 30);
		SkySiteUtils.waitTill(3000);
		if (attachmentPunchHistory.isDisplayed()) 
		{
			Log.message("First punch is Selectes successfully.");
			return true;
		} else {
			Log.message("First punch is failed to select.");
			return false;
		}
	}
	
	/**
	 * Method written for download attachments from receiver
	 * Scipted By: Naresh Babu
	 * @return
	 */
	
	@FindBy(css=".btn.btn-info")
	WebElement btnCompletePunchDetails;
	
	@FindBy(xpath="(//span[@data-bind='text: DocumentName()'])[2]")
	WebElement attachmentNamePunchDetails;
	
	@FindBy(xpath="(//i[@class='icon icon-download-alt'])[2]")
	WebElement iconDownloadattachmentPunchDetails;
	public boolean Verify_download_attachments_from_receiver(String Sys_Download_Path) throws AWTException, InterruptedException 
	{
		boolean result1 = false;
		
		SkySiteUtils.waitForElement(driver, btnCompletePunchDetails, 30);
		//Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(8000);
		String Attachment_Name = attachmentNamePunchDetails.getText();
		Log.message("Attachment Name is: "+Attachment_Name);
		iconDownloadattachmentPunchDetails.click();
		Log.message("Clicked on download of an attachment.");
		SkySiteUtils.waitTill(20000);

		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);
		if (browserName.contains("firefox")) 
		{
			// Handling Download PopUp of firefox browser using robot
			Robot robot = null;
			robot = new Robot();

			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(30000);
		}	
		// After Download, checking file Downloaded or not
		String ActualFilename = null;
		File[] files = new File(Sys_Download_Path).listFiles();

		for (File file : files) 
		{
			if (file.isFile()) 
			{
				ActualFilename = file.getName();// Getting File Names into a variable
				Log.message("Actual File name is:" + ActualFilename);
				Long ActualFileSize = file.length();
				Log.message("Actual File size is:" + ActualFileSize);
				if ((ActualFilename.contains(Attachment_Name)) && (ActualFileSize != 0)) 
				{
					Log.message("File is available in downloads!!!");
					result1 = true;
					break;
				}
			}
		}
		if((result1==true))
		{
			Log.message("Attachment download is working successfully!!!");
			return true;
		}
		else 
		{
			Log.message("Attachment download is not working!!!");
			return false;
		}	
	}
	
	/**
	 * Method written for download attachments from punch history - Owner
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_download_attachments_from_PunchHistory(String Sys_Download_Path) throws AWTException, InterruptedException 
	{
		boolean result1 = false;
		SkySiteUtils.waitForElement(driver, attachmentPunchHistory, 30);
		//Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(5000);
		String AutoComment_Complete_PunchHistory = TrackCommentPunchDetails.getText();
		Log.message("Auto Comment Complete Punch History is: "+AutoComment_Complete_PunchHistory);
		String Attachment_Name = attachmentPunchHistory.getText();
		Log.message("Attachment Name is: "+Attachment_Name);
		attachmentPunchHistory.click();
		Log.message("Clicked on download of an attachment.");
		SkySiteUtils.waitTill(20000);

		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);
		if (browserName.contains("firefox")) 
		{
			// Handling Download PopUp of firefox browser using robot
			Robot robot = null;
			robot = new Robot();

			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(30000);
		}	
		// After Download, checking file Downloaded or not
		String ActualFilename = null;
		File[] files = new File(Sys_Download_Path).listFiles();

		for (File file : files) 
		{
			if (file.isFile()) 
			{
				ActualFilename = file.getName();// Getting File Names into a variable
				Log.message("Actual File name is:" + ActualFilename);
				Long ActualFileSize = file.length();
				Log.message("Actual File size is:" + ActualFileSize);
				if ((ActualFilename.contains(Attachment_Name)) && (ActualFileSize != 0)) 
				{
					Log.message("File is available in downloads!!!");
					result1 = true;
					break;
				}
			}
		}
		if((result1==true) && (AutoComment_Complete_PunchHistory.contentEquals("Changed punch status to Completed")))
		{
			Log.message("Attachment download is working successfully!!!");
			return true;
		}
		else 
		{
			Log.message("Attachment download is not working!!!");
			return false;
		}	
	}
	
	/**
	 * Method written for commenting as receiver and validate
	 * Scipted By: Naresh Babu
	 * @return
	 */
	@FindBy(css="#txtComment")
	WebElement txtCommentPunchDetails;
	@FindBy(css="#btnSubmit")
	WebElement btnSubmitPunchDetails;
	@FindBy(css="#btnAttachement")
	WebElement btnAttachementPunchDetails;
	
	@FindBy(xpath="(//span[@data-bind='html: TrackComment()'])[1]")
	WebElement TrackCommentPunchDetails;
	public boolean Verify_PunchComment_AndValidate(String Receiver_Comment) 
	{
		SkySiteUtils.waitForElement(driver, btnCompletePunchDetails, 30);
		txtCommentPunchDetails.sendKeys(Receiver_Comment);
		btnSubmitPunchDetails.click();
		Log.message("Entering comment and clicked on Submit button.");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, notificationMessage, 30);
		String Comment_NotifyMsg = notificationMessage.getText();
		Log.message("Notify message after submit a comment is: "+Comment_NotifyMsg);
		SkySiteUtils.waitTill(5000);
		String TrackOf_Comment = TrackCommentPunchDetails.getText();
		Log.message("Track Of the recent user Comment is: "+TrackOf_Comment);
		
		if((TrackOf_Comment.contentEquals(Receiver_Comment)) && (Comment_NotifyMsg.contentEquals("Comment is added successfully")))
		{
			Log.message("Punch Comment And Validate is working successfully!!!");
			return true;
		}
		else 
		{
			Log.message("Punch Comment And Validate is not working!!!");
			return false;
		}	
	}
	
	/**
	 * Method written for commenting as receiver with attachments and validate
	 * Scipted By: Naresh Babu
	 * @return
	 * @throws IOException 
	 */
	
	@FindBy(xpath="(//a[@class='attachment doc-view'])[1]")
	WebElement attachmentPunchHistory;
	public boolean Verify_PunchComment_WithAttachments_AndValidate(String Receiver_Comment, String FolderPath) throws IOException 
	{	
		SkySiteUtils.waitForElement(driver, btncreateNewPunch, 30);
		tabPunchlistAssignToMe.click();
		Log.message("Clicked on Punch list assigned to me tab.");
		SkySiteUtils.waitForElement(driver, infoFirstPunchNumber, 30);
		SkySiteUtils.waitTill(5000);
		infoFirstPunchNumber.click();
		Log.message("Clicked on punch stamp icon to open the details.");
		SkySiteUtils.waitForElement(driver, txtCommentPunchDetails, 30);
		SkySiteUtils.waitTill(5000);
		txtCommentPunchDetails.sendKeys(Receiver_Comment);
		SkySiteUtils.waitTill(2000);
		btnAttachementPunchDetails.click();
		Log.message("Clicked on attachment button while comment.");
		SkySiteUtils.waitForElement(driver, btnSelectFilesPunchWindow, 20);
		btnSelectFilesPunchWindow.click();
		Log.message("Clicked on Select files button.");
		SkySiteUtils.waitTill(10000);
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(1000);
			}
		}
		output.flush();
		output.close();

		// Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(10000);			
		SkySiteUtils.waitForElement(driver, txtNoOfFilesSelectedPunchWindow, 30);
		try
		{
			 File file = new File(tmpFileName);
			 if(file.delete())
			 {
				 Log.message(file.getName() + " is deleted!");
			 }
			 else
			 {
				 Log.message("Delete operation is failed.");
			 }
		}
		catch(Exception e)
		{
			Log.message("Exception occured!!!"+e);	
		}
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnCreatePunchPopover, 30);
		btnCreatePunchPopover.click();
		Log.message("Clicked on Upload and Attach button.");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, notificationMessage, 40);
		String Comment_NotifyMsg = notificationMessage.getText();
		Log.message("Notify message after submit a comment with attachment is: "+Comment_NotifyMsg);
		SkySiteUtils.waitTill(5000);
		String TrackOf_Comment = TrackCommentPunchDetails.getText();
		Log.message("Track Of the recent user Comment is: "+TrackOf_Comment);
		
		if((TrackOf_Comment.contentEquals(Receiver_Comment)) && (Comment_NotifyMsg.contentEquals("Comment is added successfully"))
				&& (attachmentPunchHistory.isDisplayed()))
		{
			Log.message("Punch Comment with attachment is working successfully!!!");
			return true;
		}
		else 
		{
			Log.message("Punch Comment with attachment is not working!!!");
			return false;
		}	
	}
	
	/**
	 * Method written for CC user is not getting any status change related options.
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_CCUser_NotGettingStatusButtons() 
	{
		SkySiteUtils.waitForElement(driver, txtCommentPunchDetails, 30);
		if(btnCompletePunchDetails.isDisplayed())
		{
			Log.message("Status buttons are Displayed for CC user.");
			return false;
		}
		else 
		{
			Log.message("Non of the status buttons are Displayed.");
			return true;
		}	
	}
	
	/**
	 * Method written for Receiver is changing the status of punch to Completed and validate
	 * Scipted By: Naresh Babu
	 * @return
	 */
	
	@FindBy(xpath="(//span[@class='resolved-txt'])[1]")
	WebElement CompletedStatusTxt;
	
	public boolean Verify_StatusChange_ToCompleted_AndValidate() 
	{
		SkySiteUtils.waitForElement(driver, btnCompletePunchDetails, 30);
		btnCompletePunchDetails.click();
		Log.message("Clicked on Complete button.");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, notificationMessage, 30);
		String ClickComplete_NotifyMsg = notificationMessage.getText();
		Log.message("Notify message after click on Complete button is: "+ClickComplete_NotifyMsg);
		SkySiteUtils.waitTill(5000);
		String Status_Completed = CompletedStatusTxt.getText();
		Log.message("Status after click on complete button is: "+Status_Completed);
		
		if((Status_Completed.equalsIgnoreCase("Completed")) && (ClickComplete_NotifyMsg.contentEquals("Status updated successfully")))
		{
			Log.message("Punch Status changed to COMPLETED successfully!!!");
			return true;
		}
		else 
		{
			Log.message("Punch Status changed to COMPLETED is Failed!!!");
			return false;
		}	
	}
	
	
	/**
	 * Method written for Create Punch and save as draft.
	 * Scipted By: Naresh Babu
	 * @return
	 */
	
	@FindBy(css="#btnDraft")
	WebElement btnSaveAsDraft;
	
	public boolean Create_Punch_SaveAsDraft(String Stamp_Number,String Employee_Name,String MailId, String Punch_Description) 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;
		
		SkySiteUtils.waitForElement(driver, btncreateNewPunch, 30);
		btncreateNewPunch.click();
		Log.message("Clicked on create punch button.");
		SkySiteUtils.waitForElement(driver, btnCreatePunchPopover, 30);
		SkySiteUtils.waitTill(2000);
		dropdownAddNewPunchStamp.click();
		Log.message("Clicked on drop down button of stamp");
		SkySiteUtils.waitTill(2000);
		btnAddNewPunchStamp.click();
		Log.message("Clicked on Add new button");
		SkySiteUtils.waitTill(2000);
		textBoxStamp.sendKeys(Stamp_Number);
		textBoxStampTitle.sendKeys(Stamp_Number);
		btnCreatePunchStampOK.click();
		Log.message("Clicked on Right symbol After entered Stamp number and title.");
		SkySiteUtils.waitForElement(driver, symbolPunchStamp, 30);
		SkySiteUtils.waitTill(2000);
		String PunchNumber_AfterCreate= symbolPunchStamp.getText();
		String PunchName_AfterCreate = symbolPunchTitle.getAttribute("value");
		//Validate After Stamp Create	
		if((PunchNumber_AfterCreate.contentEquals(Stamp_Number)) && (PunchName_AfterCreate.contentEquals(Stamp_Number)))
		{
			result1=true;
			Log.message("Stamp Created Successfully with name: "+Stamp_Number);
			btnAssignToContactList.click();
			Log.message("Clicked on Assign to contact icon.");
			SkySiteUtils.waitForElement(driver, contactSearchField, 30);
			SkySiteUtils.waitTill(2000);
			contactSearchField.sendKeys(Employee_Name);
			iconSearchSelectUser.click();
			Log.message("Clicked on Search icon by entering input");
			SkySiteUtils.waitTill(3000);
			String Name_AfterSearch = nameFromSearchResult.getText();
			Log.message("Name After Search: "+Name_AfterSearch);
			String Mail_AfterSearch = emailFromSearchResult.getText();
			Log.message("Mail After Search: "+Mail_AfterSearch);
			if((Name_AfterSearch.contains(Employee_Name)) && (Mail_AfterSearch.contentEquals(MailId)))
			{
				Log.message("Search Results are coming properly!!!");
				checkboxSearchResult.click();
				btnSelectUser.click();
				Log.message("Clicked on Select user.");
				SkySiteUtils.waitTill(2000);
				iconCalenderDueDate.click();
				Log.message("Clicked on Due Date Calendar icon.");
				SkySiteUtils.waitTill(2000);
				driver.findElement(By.xpath("//div/div[1]/table/thead/tr[1]/th[3]")).click();//Click on next month
				SkySiteUtils.waitTill(2000);
				driver.findElement(By.xpath("(//div/div[1]/table/tbody/tr[2]/td[4])[2]")).click();//Pick date
				SkySiteUtils.waitTill(2000);
				txtPunchDescription.sendKeys(Punch_Description);
				SkySiteUtils.waitTill(2000);
				btnSaveAsDraft.click();
				Log.message("clicked on Save as Draft button after enter required details.");
				SkySiteUtils.waitTill(10000);
				String Punch_Number_AfterDraft=infoFirstPunchNumberOfDraft.getText();
				Log.message("Punch Number After Draft: "+Punch_Number_AfterDraft);			
				String AssignedTo=driver.findElement(By.xpath("//ul[@id='ulPunchList']/li[1]/div[2]/div[2]/h4[2]/span")).getText();
				Log.message("Assigned To: "+AssignedTo);
				String Status_AfterDraft = DraftStatus.getText();
				Log.message("Status After Draft: "+Status_AfterDraft);
				String Attachment_Count = attachmentCount.getText();
				Log.message("Attchment count is: "+Attachment_Count);
				String Actual_Description = punchDescription.getText();
				Log.message("Punch Description is: "+Actual_Description);
				
				if((Attachment_Count.contentEquals("0")) && (Punch_Number_AfterDraft.contentEquals(Stamp_Number)) 
						&& (AssignedTo.contains(Employee_Name))	&& (Status_AfterDraft.equalsIgnoreCase("Draft"))
						&& (Actual_Description.contentEquals(Punch_Description)))
				{
					result2=true;
					Log.message("Punch Saved as Draft successfully in MyPunchList with name: "+Stamp_Number);
					//Validations from All Punch List Tab
					tabAllPpunchList.click();
					Log.message("Clicked on All Punch List tab.");
					SkySiteUtils.waitTill(10000);
					Punch_Number_AfterDraft=infoFirstPunchNumberOfDraft.getText();
					Log.message("Punch Number After Draft: "+Punch_Number_AfterDraft);			
					AssignedTo=driver.findElement(By.xpath("//ul[@id='ulPunchList']/li[1]/div[2]/div[2]/h4[2]/span")).getText();
					Log.message("Assigned To: "+AssignedTo);
					Status_AfterDraft = DraftStatus.getText();
					Log.message("Status After Draft: "+Status_AfterDraft);
					Attachment_Count = attachmentCount.getText();
					Log.message("Attchment count is: "+Attachment_Count);
					
					if((Attachment_Count.contentEquals("0")) && (Punch_Number_AfterDraft.contentEquals(Stamp_Number)) 
							&& (AssignedTo.contains(Employee_Name))	&& (Status_AfterDraft.equalsIgnoreCase("Draft")))
					{
						result3=true;
						Log.message("Punch Saved as Draft successfully in AllPunchList with name: "+Stamp_Number);
					}
					else
					{
						result3=false;
						Log.message("Punch Saved as Draft Failed in AllPunchList with name: "+Stamp_Number);
					}
				}
				else
				{
					result2=false;
					Log.message("Punch Saved as Draft Failed in MyPunchList with name: "+Stamp_Number);
				}
			}
						
		}
		else
		{
			result1=false;
			Log.message("Stamp Creation Failed with name: "+Stamp_Number);
		}

		if ((result1==true) && (result2==true) && (result3==true)) 
		{
			Log.message("Draft a punch and validate is working successfully.");
			return true;
		} else {
			Log.message("Draft a punch and validate is failed.");
			return false;
		}
	}
	
	/**
	 * Method written for Assign the drafted punch and validate
	 * Scipted By: Naresh Babu
	 * @return
	 */

	public boolean Verify_Assign_DraftedPunch(String Stamp_Number, String Employee_Name) throws AWTException, InterruptedException 
	{	
		SkySiteUtils.waitForElement(driver, infoFirstPunchNumberOfDraft, 30);
		infoFirstPunchNumberOfDraft.click();
		Log.message("Clicked on punch stamp icon to open the details.");
		SkySiteUtils.waitTill(8000);
		SkySiteUtils.waitForElement(driver, btnCreatePunchPopover, 30);
		btnCreatePunchPopover.click();
		Log.message("Clicked on Create Punch button.");
		SkySiteUtils.waitTill(10000);
		String Punch_Number_AfterCreate=infoFirstPunchNumber.getText();
		Log.message("Punch Number After Create: "+Punch_Number_AfterCreate);			
		String AssignedTo=driver.findElement(By.xpath("//ul[@id='ulPunchList']/li[1]/div[2]/div[2]/h4[2]/span")).getText();
		Log.message("Assigned To: "+AssignedTo);
		String Status_AfterCreate = OpenStatus.getText();
		Log.message("Status After Create: "+Status_AfterCreate);
		String Attachment_Count = attachmentCount.getText();
		Log.message("Attchment count is: "+Attachment_Count);
		
		if((Attachment_Count.contentEquals("0")) && (Punch_Number_AfterCreate.contentEquals(Stamp_Number)) 
				&& (AssignedTo.contains(Employee_Name))	&& (Status_AfterCreate.equalsIgnoreCase("Open")))
		{
			Log.message("Drafted Punch Assigned successfully.");
			return true;
		}
		else
		{
			Log.message("Drafted Punch Assigned Failed.");
			return false;
		}
		
	}
	
	/**
	 * Method written for download Punch Report
	 * Scipted By: Naresh Babu
	 * @return
	 */
	
	@FindBy(css=".icon.icon-generate-report")
	WebElement iconGeneratePunchReport;
	public boolean Verify_download_Punch_Report(String Sys_Download_Path) throws AWTException, InterruptedException 
	{
		boolean result1 = false;
		
		SkySiteUtils.waitForElement(driver, btncreateNewPunch, 30);
		//Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(8000);
		iconGeneratePunchReport.click();
		Log.message("Clicked on Generate report icon.");
		SkySiteUtils.waitTill(30000);

		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);
		if (browserName.contains("firefox")) 
		{
			// Handling Download PopUp of firefox browser using robot
			Robot robot = null;
			robot = new Robot();

			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(30000);
		}	
		// After Download, checking file Downloaded or not
		String ActualFilename = null;
		File[] files = new File(Sys_Download_Path).listFiles();

		for (File file : files) 
		{
			if (file.isFile()) 
			{
				ActualFilename = file.getName();// Getting File Names into a variable
				Log.message("Actual File name is:" + ActualFilename);
				Long ActualFileSize = file.length();
				Log.message("Actual File size is:" + ActualFileSize);
				if ((ActualFilename.contains("report-")) && (ActualFileSize != 0)) 
				{
					Log.message("File is available in downloads!!!");
					result1 = true;
					break;
				}
			}
		}
		if((result1==true))
		{
			Log.message("Punch report download is working successfully!!!");
			return true;
		}
		else 
		{
			Log.message("Punch report download is not working!!!");
			return false;
		}	
	}
	
	/**
	 * Method written for Owner is changing the status of punch to Closed and validate
	 * Scipted By: Naresh Babu
	 * @return
	 */
	@FindBy(xpath="//button[@class='btn btn-info']")
	WebElement btnClosePunchItem;
	
	@FindBy(xpath="(//span[@class='closed-txt'])[1]")
	WebElement StatusClosePunch;
	
	@FindBy(xpath="(//div[@class='punch-cc punch-stamp float-icon pull-left punch-close'])[1]")
	WebElement FirstNumberClosePunch;
	
	public boolean Verify_StatusChange_ToClosed_AndValidate(String Stamp_Number) 
	{
		SkySiteUtils.waitForElement(driver, btnClosePunchItem, 30);
		btnClosePunchItem.click();
		Log.message("Clicked on Close Punch Item button.");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, btnYES, 30);
		btnYES.click();
		Log.message("Yes want to close punch.");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, notificationMessage, 30);
		String ClickClose_NotifyMsg = notificationMessage.getText();
		Log.message("Notify message after click on Close punch item button is: "+ClickClose_NotifyMsg);
		SkySiteUtils.waitTill(5000);
		String StampNumber_ClosedStatusPunch = FirstNumberClosePunch.getText();
		Log.message("Punch Number After Closed is: "+StampNumber_ClosedStatusPunch);
		String Status_Closed = StatusClosePunch.getText();
		Log.message("Status after click on Close punch item button is: "+Status_Closed);
		
		if((Status_Closed.equalsIgnoreCase("Closed")) && (ClickClose_NotifyMsg.contentEquals("Punch is closed successfully"))
				&& (StampNumber_ClosedStatusPunch.contentEquals(Stamp_Number)))
		{
			Log.message("Punch Status changed to CLOSED successfully!!!");
			return true;
		}
		else 
		{
			Log.message("Punch Status changed to CLOSED is Failed!!!");
			return false;
		}	
	}
	
	/**
	 * Method written for Verify punch from employee who is not punch owner, not in TO & CC
	 * Scipted By: Naresh Babu
	 * @return
	 */
	
	@FindBy(xpath="(//div[@class='text-center panel-body']//h2)[2]")
	WebElement infoNoPunchHasBeenAssignedToYou;
	
	@FindBy(css=".btn.btn-primary.btn-lg.new-rfi")
	WebElement btnCreatePunch_AllPunchListTab;
	public boolean Verify_PunchFrom_Employee_OtherThanAssignee_ToAndCC() 
	{
		boolean result1 = false;
		boolean result2 = false;
		SkySiteUtils.waitForElement(driver, btncreateNewPunch, 30);
		tabPunchlistAssignToMe.click();
		Log.message("Clicked on Punch list assigned to me tab.");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, infoNoPunchHasBeenAssignedToYou, 30);
		if(infoNoPunchHasBeenAssignedToYou.isDisplayed())
		{
			Log.message("Employee not getting any punch list without having any relation with punch - punch List assigned to me.");
			result1 = true;
			SkySiteUtils.waitTill(5000);
			tabAllPpunchList.click();
			Log.message("Clicked on All punch list tab.");
			SkySiteUtils.waitTill(2000);
			SkySiteUtils.waitForElement(driver, btnCreatePunch_AllPunchListTab, 30);
			if(btnCreatePunch_AllPunchListTab.isDisplayed())
			{
				Log.message("Employee not getting any punch list without having any relation with punch - All punch List.");
				result2 = true;
			}
		}
		
		if((result1==true) && (result2==true))
		{
			Log.message("Employee who is not punch owner, not in TO & CC is not getting any punch details.");
			return true;
		}
		else 
		{
			Log.message("Employee who is not punch owner, not in TO & CC still getting punch details.");
			return false;
		}	
	}
	
	/**
	 * Method written for Select Punch Annotation from viewer menu Tab
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public FolderPage SelectPunchAnnotation() 
	{
		SkySiteUtils.waitForElement(driver, iconPunch_ViewerMenu, 30);
		iconPunch_ViewerMenu.click();
		Log.message("Clicked on punch markup.");
		SkySiteUtils.waitTill(2000);
	 	driver.findElement(By.xpath(".//*[@id='imageViewer']/div[1]/div[1]/div/div[2]/img[2]")).click();
	 	SkySiteUtils.waitTill(3000);
		Log.message("Selected location on viewer to create a punch.");
		SkySiteUtils.waitForElement(driver, btnCreatePunchPopover, 30);
		SkySiteUtils.waitTill(2000);

		if (btnCreatePunchPopover.isDisplayed())
			Log.message("Punch details window is available.");
		return new FolderPage(driver).get();
	}
	
	/**
	 * Method written for Create Punch without attachments and validate in viewer 
	 * Scipted By: Naresh Babu
	 * Staging
	 * @return
	 */
	public boolean Create_Punch_NoAttachments_AndValidate_InViewer(String Stamp_Number,String Employee_Name,String MailId, String CC_UserName,String Punch_Description) 
	{
		boolean result1 = false;
		boolean result2 = false;

		SkySiteUtils.waitForElement(driver, btnCreatePunchPopover, 30);
		SkySiteUtils.waitTill(2000);
		dropdownAddNewPunchStamp.click();
		Log.message("Clicked on drop down button of stamp");
		SkySiteUtils.waitTill(2000);
		btnAddNewPunchStamp.click();
		Log.message("Clicked on Add new button");
		SkySiteUtils.waitTill(2000);
		textBoxStamp.sendKeys(Stamp_Number);
		textBoxStampTitle.sendKeys(Stamp_Number);
		btnCreatePunchStampOK.click();
		Log.message("Clicked on Right symbol After entered Stamp number and title.");
		SkySiteUtils.waitForElement(driver, symbolPunchStamp, 30);
		SkySiteUtils.waitTill(2000);
		String PunchNumber_AfterCreate= symbolPunchStamp.getText();
		String PunchName_AfterCreate = symbolPunchTitle.getAttribute("value");
		//Validate After Stamp Create	
		if((PunchNumber_AfterCreate.contentEquals(Stamp_Number)) && (PunchName_AfterCreate.contentEquals(Stamp_Number)))
		{
			result1=true;
			Log.message("Stamp Created Successfully with name: "+Stamp_Number);
			//Adding TO user
			btnAssignToPunchAnnotation.click();
			Log.message("Clicked on Assign to contact icon.");
			SkySiteUtils.waitForElement(driver, contactSearchField, 30);
			SkySiteUtils.waitTill(2000);
			contactSearchField.sendKeys(Employee_Name);
			iconSearchSelectUser.click();
			Log.message("Clicked on Search icon by entering input");
			SkySiteUtils.waitTill(3000);
			String Name_AfterSearch = nameFromSearchResult.getText();
			Log.message("Name After Search: "+Name_AfterSearch);
			String Mail_AfterSearch = emailFromSearchResult.getText();
			Log.message("Mail After Search: "+Mail_AfterSearch);
			if((Name_AfterSearch.contains(Employee_Name)) && (Mail_AfterSearch.contentEquals(MailId)))
			{
				Log.message("Search Results are coming properly!!!");
				checkboxSearchResult.click();
				btnSelectUser.click();
				Log.message("Clicked on Select user.");
				SkySiteUtils.waitTill(2000);
				//Adding CC user
				btnCCPunchAnnotation.click();
				Log.message("Clicked on CC contact icon.");
				SkySiteUtils.waitForElement(driver, contactSearchField, 30);
				SkySiteUtils.waitTill(2000);
				contactSearchField.sendKeys(CC_UserName);
				iconSearchSelectUser.click();
				Log.message("Clicked on Search icon by entering input");
				SkySiteUtils.waitTill(5000);
				checkboxSearchResult.click();
				btnSelectUser.click();
				Log.message("Clicked on Select user.");
				SkySiteUtils.waitTill(2000);
				iconCalenderDueDate.click();
				Log.message("Clicked on Due Date Calendar icon.");
				SkySiteUtils.waitTill(2000);
				driver.findElement(By.xpath("//div/div[1]/table/thead/tr[1]/th[3]")).click();//Click on next month
				SkySiteUtils.waitTill(2000);
				driver.findElement(By.xpath("(//div/div[1]/table/tbody/tr[4]/td[5])[1]")).click();//Pick date
				SkySiteUtils.waitTill(2000);
				txtPunchDescription.sendKeys(Punch_Description);
				SkySiteUtils.waitTill(2000);
				btnCreatePunchPopover.click();
				Log.message("clicked on create punch button after enter required details.");
				SkySiteUtils.waitTill(2000);
				SkySiteUtils.waitForElement(driver, notificationMessage, 30);
				String NotifyMsg_Aftr_AddedPunch_Annot = notificationMessage.getText();
				Log.message("Msg After Added Punch Annot is: "+NotifyMsg_Aftr_AddedPunch_Annot);
				SkySiteUtils.waitTill(10000);
				btnPunchList.click();
				Log.message("Clicked on dropdown button of punch list.");
				SkySiteUtils.waitTill(2000);
				String Punch_Number_AfterCreate=infoFirstPunchNOViewer.getText();
				Log.message("Punch Number After Create: "+Punch_Number_AfterCreate);			
				String Status_AfterCreate = OpenStatusViewer.getText();
				Log.message("Status After Create: "+Status_AfterCreate);
				//Close the current viewer page and navigate back to home page
				ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
				Log.message("No Of Driver windows are: "+tabs2);
				driver.switchTo().window(tabs2.get(1));
				SkySiteUtils.waitTill(5000);
				driver.close();
				SkySiteUtils.waitTill(5000);
				driver.switchTo().window(tabs2.get(0));
				Log.message("Viewr is closed and navigated back to main page.");
				SkySiteUtils.waitTill(5000);
				if((Punch_Number_AfterCreate.contentEquals(Stamp_Number)) && (Status_AfterCreate.equalsIgnoreCase("Open"))
						 && (NotifyMsg_Aftr_AddedPunch_Annot.contentEquals("Punch annotation saved successfully")))
				{
					result2=true;
					Log.message("Punch Annotation Created successfully in with name: "+Stamp_Number);
				}
				else
				{
					result2=false;
					Log.message("Punch Annotation Creation Failed!!!");
				}
			}
						
		}
		else
		{
			result1=false;
			Log.message("Stamp Creation Failed with name: "+Stamp_Number);
		}
		if ((result1==true) && (result2==true)) 
		{
			Log.message("Creating a punch Annotation and validate is working successfully.");
			return true;
		} else {
			Log.message("Creating a punch Annotation and validate is failed.");
			return false;
		}
	}
	
	/**
	 * Method written for Create Punch with attachments and validate in viewer 
	 * Scipted By: Naresh Babu
	 * Staging
	 * @return
	 * @throws IOException 
	 */
	
	public boolean Create_Punch_WithAttachments_AndValidate_InViewer(String Stamp_Number,String Employee_Name,String MailId, String CC_UserMailID,String Punch_Description,String FolderPath,String File_Name) throws IOException 
	{
		boolean result1 = false;
		boolean result2 = false;

		SkySiteUtils.waitForElement(driver, btnCreatePunchPopover, 30);
		SkySiteUtils.waitTill(2000);
		dropdownAddNewPunchStamp.click();
		Log.message("Clicked on drop down button of stamp");
		SkySiteUtils.waitTill(2000);
		btnAddNewPunchStamp.click();
		Log.message("Clicked on Add new button");
		SkySiteUtils.waitTill(2000);
		textBoxStamp.sendKeys(Stamp_Number);
		textBoxStampTitle.sendKeys(Stamp_Number);
		btnCreatePunchStampOK.click();
		Log.message("Clicked on Right symbol After entered Stamp number and title.");
		SkySiteUtils.waitForElement(driver, symbolPunchStamp, 30);
		SkySiteUtils.waitTill(2000);
		String PunchNumber_AfterCreate= symbolPunchStamp.getText();
		String PunchName_AfterCreate = symbolPunchTitle.getAttribute("value");
		//Validate After Stamp Create	
		if((PunchNumber_AfterCreate.contentEquals(Stamp_Number)) && (PunchName_AfterCreate.contentEquals(Stamp_Number)))
		{
			result1=true;
			Log.message("Stamp Created Successfully with name: "+Stamp_Number);
			//Adding TO user
			btnAssignToPunchAnnotation.click();
			Log.message("Clicked on Assign to contact icon.");
			SkySiteUtils.waitForElement(driver, contactSearchField, 30);
			SkySiteUtils.waitTill(2000);
			contactSearchField.sendKeys(Employee_Name);
			iconSearchSelectUser.click();
			Log.message("Clicked on Search icon by entering input");
			SkySiteUtils.waitTill(3000);
			String Name_AfterSearch = nameFromSearchResult.getText();
			Log.message("Name After Search: "+Name_AfterSearch);
			String Mail_AfterSearch = emailFromSearchResult.getText();
			Log.message("Mail After Search: "+Mail_AfterSearch);
			if((Name_AfterSearch.contains(Employee_Name)) && (Mail_AfterSearch.contentEquals(MailId)))
			{
				Log.message("Search Results are coming properly!!!");
				checkboxSearchResult.click();
				btnSelectUser.click();
				Log.message("Clicked on Select user.");
				SkySiteUtils.waitTill(2000);
				//Adding CC user
				btnCCPunchAnnotation.click();
				Log.message("Clicked on CC contact icon.");
				SkySiteUtils.waitForElement(driver, contactSearchField, 30);
				SkySiteUtils.waitTill(2000);
				contactSearchField.sendKeys(CC_UserMailID);
				iconSearchSelectUser.click();
				Log.message("Clicked on Search icon by entering input");
				SkySiteUtils.waitTill(5000);
				checkboxSearchResult.click();
				btnSelectUser.click();
				Log.message("Clicked on Select user.");
				SkySiteUtils.waitTill(2000);
				iconCalenderDueDate.click();
				Log.message("Clicked on Due Date Calendar icon.");
				SkySiteUtils.waitTill(2000);
				driver.findElement(By.xpath("//div/div[1]/table/thead/tr[1]/th[3]")).click();//Click on next month
				SkySiteUtils.waitTill(2000);
				driver.findElement(By.xpath("(//div/div[1]/table/tbody/tr[4]/td[5])[1]")).click();//Pick date
				SkySiteUtils.waitTill(2000);
				txtPunchDescription.sendKeys(Punch_Description);
				SkySiteUtils.waitTill(2000);
				btnAddAttachment.click();
				Log.message("Clicked on Add Attachment button.");
				SkySiteUtils.waitTill(10000);//Added due to loading problem
				SkySiteUtils.waitForElement(driver, btnSelectFilesPunchWindow, 20);
				btnSelectFilesPunchWindow.click();
				Log.message("Clicked on Select files button.");
				SkySiteUtils.waitTill(10000);
				// Writing File names into a text file for using in AutoIT Script
				BufferedWriter output;
				randomFileName rn = new randomFileName();
				String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
				output = new BufferedWriter(new FileWriter(tmpFileName, true));
				String expFilename = null;
				File[] files = new File(FolderPath).listFiles();
				for (File file : files) {
					if (file.isFile()) {
						expFilename = file.getName();// Getting File Names into a variable
						Log.message("Expected File name is:" + expFilename);
						output.append('"' + expFilename + '"');
						output.append(" ");
						SkySiteUtils.waitTill(1000);
					}
				}
				output.flush();
				output.close();

				// Executing .exe autoIt file
				String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
				Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
				Log.message("AutoIT Script Executed!!");
				SkySiteUtils.waitTill(10000);			
				SkySiteUtils.waitForElement(driver, txtNoOfFilesSelectedPunchWindow, 30);
				try
				{
					 File file = new File(tmpFileName);
					 if(file.delete())
					 {
						 Log.message(file.getName() + " is deleted!");
					 }
					 else
					 {
						 Log.message("Delete operation is failed.");
					 }
				}
				catch(Exception e)
				{
					Log.message("Exception occured!!!"+e);	
				}
				SkySiteUtils.waitTill(5000);		 
				String Attachment_Count=TotalAttachmentCount.getText();
				Log.message("Attachment Count: "+Attachment_Count);
				if(Attachment_Count.equals("3"))
				{
					Log.message("Files are selected to grid!!!");
					punchAttachedFiles.click();
					Log.message("clicked on dropdown attachment files.");
					SkySiteUtils.waitTill(2000);
					driver.findElement(By.xpath("(//i[@class='icon icon-remove'])[4]")).click();
					Log.message("Removing third file from the list.");
					SkySiteUtils.waitTill(2000);
					Attachment_Count=TotalAttachmentCount.getText();
					Log.message("Attachment Count after remove: "+Attachment_Count);
					if(Attachment_Count.equals("2"))
					{
						Log.message("Attachment got removes successfully.");
						btnProjectFiles.click();
						Log.message("Clciked on project files button");
						SkySiteUtils.waitTill(5000);
						SkySiteUtils.waitForElement(driver, folderIconProjectFiles, 20);
						folderIconProjectFiles.click();
						Log.message("Clicked on Folder available in project.");
						SkySiteUtils.waitForElement(driver, txtDocSearchProjectFiles, 20);
						SkySiteUtils.waitTill(5000);
						txtDocSearchProjectFiles.sendKeys(File_Name);
						iconDocSearchProjectFiles.click();
						SkySiteUtils.waitTill(5000);
						driver.findElement(By.xpath("(//input[@type='checkbox'])[2]")).click();//Select checkbox
						SkySiteUtils.waitTill(2000);
						btnCreatePunchPopover.click();
						Log.message("clicked on create punch button after enter required details.");
						SkySiteUtils.waitTill(2000);
						SkySiteUtils.waitForElement(driver, notificationMessage, 30);
						String NotifyMsg_Aftr_AddedPunch_Annot = notificationMessage.getText();
						Log.message("Msg After Added Punch Annot is: "+NotifyMsg_Aftr_AddedPunch_Annot);
						SkySiteUtils.waitTill(10000);
						btnPunchList.click();
						Log.message("Clicked on dropdown button of punch list.");
						SkySiteUtils.waitTill(2000);
						String Punch_Number_AfterCreate=infoFirstPunchNOViewer.getText();
						Log.message("Punch Number After Create: "+Punch_Number_AfterCreate);			
						String Status_AfterCreate = OpenStatusViewer.getText();
						Log.message("Status After Create: "+Status_AfterCreate);
						ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
						Log.message("No Of Driver windows are: "+tabs2);
						driver.switchTo().window(tabs2.get(1));
						SkySiteUtils.waitTill(5000);
						driver.close();
						SkySiteUtils.waitTill(5000);
						driver.switchTo().window(tabs2.get(0));
						Log.message("Viewr is closed and navigated back to main page.");
						SkySiteUtils.waitTill(5000);
						if((Punch_Number_AfterCreate.contentEquals(Stamp_Number)) && (Status_AfterCreate.equalsIgnoreCase("Open"))
								&& (NotifyMsg_Aftr_AddedPunch_Annot.contentEquals("Punch annotation saved successfully")))
						{
							result2=true;
							Log.message("Punch Annotation Created successfully in with name: "+Stamp_Number);
						}
						else
						{
							result2=false;
							Log.message("Punch Annotation Creation Failed!!!");
						}
					}
				}
			}
						
		}
		else
		{
			result1=false;
			Log.message("Stamp Creation Failed with name: "+Stamp_Number);
		}
		if ((result1==true) && (result2==true)) 
		{
			Log.message("Creating a punch Annotation and validate is working successfully.");
			return true;
		} else {
			Log.message("Creating a punch Annotation and validate is failed.");
			return false;
		}
	}
	
	
	/**
	 * Method to validate Punch page is landed
	 * Scripted By: Trinanjwan Sarkar
	 * @return
	 */
	
	@FindBy(xpath="//a[@id='createNewPunch']")
	WebElement createPunchbtn;
	
	
	public boolean punchPagelanded()
	
	{
		SkySiteUtils.waitForElement(driver, createPunchbtn, 50);
		if(createPunchbtn.isDisplayed())
		{
			Log.message("Create Punch button is displayed");
			return true;
		}
		else
		{
			Log.message("Create Punch button is not displayed");
			return false;
		}

	}
	
	/**
	 * Method written for Create, Edit and Delete stamp from Manage Stamp - Viewer level
	 * Scipted By: Naresh Babu
	 * Staging
	 * @return
	 */
	public boolean Create_Edit_DeleteStamp_FromManageStamp_Viewer(String Stamp_Number, String Stamp_Creater) 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;
		boolean result4 = false;
		
		SkySiteUtils.waitForElement(driver, btnCreatePunchPopover, 30);
		SkySiteUtils.waitTill(2000);
		dropdownAddNewPunchStamp.click();
		Log.message("Clicked on drop down button of stamp");
		SkySiteUtils.waitTill(2000);
		btnAddNewPunchStamp.click();
		Log.message("Clicked on Add new button");
		SkySiteUtils.waitTill(2000);
		textBoxStamp.sendKeys(Stamp_Number);
		textBoxStampTitle.sendKeys(Stamp_Number);
		btnCreatePunchStampOK.click();
		Log.message("Clicked on Right symbol After entered Stamp number and title.");
		SkySiteUtils.waitForElement(driver, symbolPunchStamp, 30);
		SkySiteUtils.waitTill(2000);
		String PunchNumber_AfterCreate= symbolPunchStamp.getText();
		String PunchName_AfterCreate = symbolPunchTitle.getAttribute("value");
		//Validate After Stamp Create	
		if((PunchNumber_AfterCreate.contentEquals(Stamp_Number)) && (PunchName_AfterCreate.contentEquals(Stamp_Number)))
		{
			result1=true;
			Log.message("Stamp Created Successfully with name: "+Stamp_Number);
			btnCancelPunchWindow.click();
			Log.message("Clicked on cancel button of Create Punch window.");
			SkySiteUtils.waitTill(2000);
			//Close the viewer
			ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
			Log.message("No Of Driver windows are: "+tabs2);
			driver.switchTo().window(tabs2.get(1));
			SkySiteUtils.waitTill(5000);
			driver.close();
			SkySiteUtils.waitTill(5000);
			driver.switchTo().window(tabs2.get(0));
			Log.message("Viewr is closed and navigated back to main page.");
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, btnPrjManagement, 20);
			btnPrjManagement.click();
			Log.message("Clicked on Project Management tab.");
			SkySiteUtils.waitForElement(driver, tabPunch, 20);
			tabPunch.click();
			Log.message("Clicked on Punch List tab.");
			SkySiteUtils.waitForElement(driver, btncreateNewPunch, 20);
			SkySiteUtils.waitTill(5000);
			btnmanageStamps.click();
			Log.message("Clicked on Manage Stamp button.");
			SkySiteUtils.waitForElement(driver, btnOKmanageStamp, 30);
			SkySiteUtils.waitTill(5000);
			//Counting available Stamps
			int Stamp_Count=0;
			int i = 1;
			List<WebElement> allElements = driver.findElements(By.xpath("//div[@class='punch-stamp punch-open']"));
			for (WebElement Element : allElements)
			{ 
				Stamp_Count = Stamp_Count+1; 	
			}
			Log.message("Number of stamps are: "+Stamp_Count);
			
			for(i=1;i<=Stamp_Count;i++)
			{
				String StampNumber_ManageStamp=driver.findElement(By.xpath("(//div[@class='punch-stamp punch-open'])["+i+"]")).getText();
				String StampName_ManageStamp=driver.findElement(By.xpath("//div[2]/div[2]/ul/li["+i+"]/div/div[2]/h4")).getText();
				String Stamp_CreatedBy=driver.findElement(By.xpath("//div[2]/div[2]/ul/li["+i+"]/div/div[2]/h5/span[1]")).getText();
				if((StampNumber_ManageStamp.contentEquals(Stamp_Number)) && (StampName_ManageStamp.contentEquals(Stamp_Number))
						&& (Stamp_CreatedBy.contains(Stamp_Creater)))
				{
					result2=true;
					break;
				}
				
			}
			if(result2==true)
			{
				Log.message("Expected Stamp is available: "+Stamp_Number);
				driver.findElement(By.xpath("(//i[@class='icon icon-edit icon-lg'])["+i+"]")).click();
				Log.message("Clicked on required stamp edit icon.");
				SkySiteUtils.waitTill(3000);
				String StmNumber_WhileEdit=txtStampNoWhileEdit.getAttribute("value");
				String StmName_WhileEdit=txtStampNameWhileEdit.getAttribute("value");
				if((StmNumber_WhileEdit.contentEquals(Stamp_Number)) && (StmName_WhileEdit.contentEquals(Stamp_Number)))
				{
					Log.message("Details are proper for Editing Stamp!!!");
					txtStampNoWhileEdit.clear();
					txtStampNoWhileEdit.sendKeys("abc");
					txtStampNameWhileEdit.clear();
					txtStampNameWhileEdit.sendKeys("abc");
					iconRightWhileStampEdit.click();
					Log.message("Enter new stamp details and click on Right icon.");
					SkySiteUtils.waitTill(5000);
					//Counting available Stamps after edit a stamp
					Stamp_Count=0;
					int j = 1;
					List<WebElement> allElements1 = driver.findElements(By.xpath("//div[@class='punch-stamp punch-open']"));
					for (WebElement Element : allElements1)
					{ 
						Stamp_Count = Stamp_Count+1; 	
					}
					Log.message("Number of stamps after edit is: "+Stamp_Count);
					
					for(j=1;j<=Stamp_Count;j++)
					{
						String StampNumber_AfterEdit=driver.findElement(By.xpath("(//div[@class='punch-stamp punch-open'])["+j+"]")).getText();
						String StampName_AfterEdit=driver.findElement(By.xpath("//div[2]/div[2]/ul/li["+j+"]/div/div[2]/h4")).getText();
							
						if((StampNumber_AfterEdit.equalsIgnoreCase("abc")) && (StampName_AfterEdit.equalsIgnoreCase("abc")))
						{
							result3=true;
							break;
						}
						
					}
				
					if(result3=true)
					{
						Log.message("Stamp is Edited Successfully from: "+Stamp_Number+" To: abc");
						//Going to delete stamp
						driver.findElement(By.xpath("(//i[@class='icon icon-trash icon-lg'])["+j+"]")).click();
						Log.message("Clicked on delete icon of expected stamp.");
						SkySiteUtils.waitForElement(driver, btnYES, 20);
						btnYES.click();
						SkySiteUtils.waitTill(1000);
						SkySiteUtils.waitForElement(driver, notificationMessage, 20);
						String Notifi_MsgAfterDelete=notificationMessage.getText();
						Log.message("Notifi Msg After Delete: "+Notifi_MsgAfterDelete);
						
						if(Stamp_Count==1)
						{
							if((Notifi_MsgAfterDelete.contentEquals("Stamp deleted successfully")))
							{
								result4=true;
								Log.message("Stamp is Deleted Successfully!!!");
								SkySiteUtils.waitTill(5000);//Page load issue
								btnOKmanageStamp.click();//Close Manage Stamp window
							}
						}
						if(Stamp_Count>1)
						{
							//Counting available Stamps
							int Stamp_Count_AfterDelete=0;
							List<WebElement> allElements2 = driver.findElements(By.xpath("//div[@class='punch-stamp punch-open']"));
							for (WebElement Element : allElements2)
							{ 
								Stamp_Count_AfterDelete = Stamp_Count_AfterDelete+1; 	
							}
							Log.message("Number of stamps After Delete a stamp is: "+Stamp_Count_AfterDelete);
							
							if((Notifi_MsgAfterDelete.contentEquals("Stamp deleted successfully")) && (Stamp_Count_AfterDelete==Stamp_Count-1))
							{
								result4=true;
								Log.message("Stamp is Deleted Successfully!!!");
								btnOKmanageStamp.click();//Close Manage Stamp window
							}
							else
							{
								result4=false;
								Log.message("Stamp is Failed to Delete!!!");
							}
						}
																	
					}
					else
					{
						result3=false;
						Log.message("Stamp is Failed to Edit from: "+Stamp_Number+" To: abc");
					}
				}
			}
			else
			{
				result2=false;
				Log.message("Expected Stamp is NOT available: "+Stamp_Number);
			}			
		}
		else
		{
			result1=false;
			Log.message("Stamp Creation Failed with name: "+Stamp_Number);
		}

		if ((result1==true) && (result2==true) && (result3==true) && (result4==true)) 
		{
			Log.message("Creating a stamp, Edit & Delete is working successfully.");
			return true;
		} else {
			Log.message("Creating a stamp, Edit & Delete is failed.");
			return false;
		}
	}
	
	/**
	 * Method written to select expected Punch from punch list in viewer 
	 * Scipted By: Naresh Babu
	 * Staging
	 * @return
	 * @throws IOException 
	 */
	
	@FindBy(xpath="(//h5[@data-toggle='tooltip'])[1]")
	WebElement DescriptionViewer;
	
	public boolean Select_ExpectedPunch_InViewer(String Stamp_Number, String Punch_Description) 
	{
		boolean result1 = false;
		boolean result2 = false;
		SkySiteUtils.waitForElement(driver, btnPunchList, 30);
		btnPunchList.click();
		Log.message("Clicked on dropdown button of punch list.");
		SkySiteUtils.waitTill(2000);
		String Punch_Number_AfterCreate=infoFirstPunchNOViewer.getText();
		Log.message("Punch Number After Create: "+Punch_Number_AfterCreate);			
		String Status_AfterCreate = OpenStatusViewer.getText();
		Log.message("Status After Create: "+Status_AfterCreate);
		String Description_AfterCreate=DescriptionViewer.getText();
		Log.message("Description After Create: "+Description_AfterCreate);	
		SkySiteUtils.waitTill(5000);
		if((Punch_Number_AfterCreate.contentEquals(Stamp_Number)) && (Status_AfterCreate.equalsIgnoreCase("Open"))
				&& (Description_AfterCreate.equalsIgnoreCase(Punch_Description)))
		{
			result1=true;
			Log.message("Expected Punch available under punch list: "+Stamp_Number);
			infoFirstPunchNOViewer.click();
			Log.message("Clicked on punch from the list to open it.");
			SkySiteUtils.waitTill(10000);
			driver.findElement(By.cssSelector("circle.leaflet-clickable")).click();
			Log.message("clicked on annotation image");
			SkySiteUtils.waitTill(2000);
			SkySiteUtils.waitForElement(driver, txtCommentPunchDetails, 30);
			if(txtCommentPunchDetails.isDisplayed())
			{
				result2=true;
				Log.message("Punch details window opened successfully.");
			}
			else
			{
				result2=false;
				Log.message("Punch details window was failed to load.");
			}
		}
		else
		{
			result1=false;
			Log.message("Expected Punch is NOT available in receiver side.");
		}

		if ((result1==true) && (result2==true)) 
		{
			Log.message("Expected punch is available and Selected successfully.");
			return true;
		} else {
			Log.message("Expected punch is NOT available for user.");
			return false;
		}
	}
	
	/**
	 * Method written for download attachments from receiver - Viewer
	 * Scipted By: Naresh Babu
	 * Staging
	 * @return
	 */
	public boolean Verify_download_attachments_from_receiver_Viewer(String Sys_Download_Path) throws AWTException, InterruptedException 
	{
		boolean result1 = false;
		
		SkySiteUtils.waitForElement(driver, txtCommentPunchDetails, 30);
		//Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		String Attachment_Name = attachmentNamePunchDetails.getText();
		Log.message("Attachment Name is: "+Attachment_Name);
		driver.findElement(By.xpath("(//i[@class='icon icon-download-alt'])[4]")).click();
		Log.message("Clicked on download of an attachment.");
		SkySiteUtils.waitTill(20000);

		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);
		if (browserName.contains("firefox")) 
		{
			// Handling Download PopUp of firefox browser using robot
			Robot robot = null;
			robot = new Robot();

			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(30000);
		}	
		// After Download, checking file Downloaded or not
		String ActualFilename = null;
		File[] files = new File(Sys_Download_Path).listFiles();

		for (File file : files) 
		{
			if (file.isFile()) 
			{
				ActualFilename = file.getName();// Getting File Names into a variable
				Log.message("Actual File name is:" + ActualFilename);
				Long ActualFileSize = file.length();
				Log.message("Actual File size is:" + ActualFileSize);
				if ((ActualFilename.contains(Attachment_Name)) && (ActualFileSize != 0)) 
				{
					Log.message("File is available in downloads!!!");
					result1 = true;
					break;
				}
			}
		}
		if((result1==true))
		{
			Log.message("Attachment download is working successfully!!!");
			return true;
		}
		else 
		{
			Log.message("Attachment download is not working!!!");
			return false;
		}	
	}
	
	/**
	 * Method written for validate change status of punch as Completed - Viewer
	 * Scipted By: Naresh Babu
	 * Staging
	 * @return
	 */
	public boolean Verify_StatusChange_ToCompleted_Viewer() 
	{
		SkySiteUtils.waitForElement(driver, btnCompletePunchDetails, 30);
		btnCompletePunchDetails.click();
		Log.message("Clicked on Complete button.");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, notificationMessage, 30);
		String ClickComplete_NotifyMsg = notificationMessage.getText();
		Log.message("Notify message after click on Complete button is: "+ClickComplete_NotifyMsg);
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnPunchList, 30);
		btnPunchList.click();
		Log.message("Clicked on dropdown button of punch list.");
		SkySiteUtils.waitTill(2000);
		String Status_Completed = CompletedStatusTxtViewer.getText();
		Log.message("Status after click on complete button is: "+Status_Completed);
		//Close the current viewer page and navigate back to home page
		ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
		Log.message("No Of Driver windows are: "+tabs2);
		driver.switchTo().window(tabs2.get(1));
		SkySiteUtils.waitTill(5000);
		driver.close();
		SkySiteUtils.waitTill(5000);
		driver.switchTo().window(tabs2.get(0));
		Log.message("Viewr is closed and navigated back to main page.");
		SkySiteUtils.waitTill(5000);
		if((Status_Completed.equalsIgnoreCase("Completed")) && (ClickComplete_NotifyMsg.contentEquals("Punch annotation saved successfully")))
		{
			Log.message("Punch Status changed to COMPLETED successfully!!!");
			return true;
		}
		else 
		{
			Log.message("Punch Status changed to COMPLETED is Failed!!!");
			return false;
		}	
	}
	
	/**
	 * Method written for Owner is changing the status of punch to Closed and validate in viewer
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_StatusChange_ToClosed_AndValidate_Viewer(String Stamp_Number) 
	{
		SkySiteUtils.waitForElement(driver, btnClosePunchItem, 30);
		btnClosePunchItem.click();
		Log.message("Clicked on Close Punch Item button.");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, btnYES, 30);
		btnYES.click();
		Log.message("Yes want to close punch.");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, notificationMessage, 30);
		String ClickClose_NotifyMsg = notificationMessage.getText();
		Log.message("Notify message after click on Close punch item button is: "+ClickClose_NotifyMsg);
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnPunchList, 30);
		btnPunchList.click();
		Log.message("Clicked on dropdown button of punch list.");
		SkySiteUtils.waitTill(2000);
		String Status_Closed = ClosedStatusTxtViewer.getText();
		Log.message("Status after click on Close button is: "+Status_Closed);
		String StampNumber_ClosedStatusPunch = infoFirstPunchNOViewer.getText();
		Log.message("Punch Number After Closed is: "+StampNumber_ClosedStatusPunch);
		//Close the current viewer page and navigate back to home page
		ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
		Log.message("No Of Driver windows are: "+tabs2);
		driver.switchTo().window(tabs2.get(1));
		SkySiteUtils.waitTill(5000);
		driver.close();
		SkySiteUtils.waitTill(5000);
		driver.switchTo().window(tabs2.get(0));
		Log.message("Viewr is closed and navigated back to main page.");
		SkySiteUtils.waitTill(5000);
		if((Status_Closed.equalsIgnoreCase("Completed")) && (ClickClose_NotifyMsg.contentEquals("Punch annotation saved successfully"))
				&& (StampNumber_ClosedStatusPunch.contentEquals(Stamp_Number)))
		{
			Log.message("Punch Status changed to CLOSED successfully!!!");
			return true;
		}
		else 
		{
			Log.message("Punch Status changed to CLOSED is Failed!!!");
			return false;
		}	
	}
	
	/**
	 * Method written for Create Punch and save as draft in viewer level.
	 * Scipted By: Naresh Babu
	 * Staging
	 * @return
	 */
	public boolean Create_Punch_SaveAsDraft_Viewer(String Stamp_Number,String Employee_Name,String MailId, String Punch_Description) 
	{
		boolean result1 = false;
		boolean result2 = false;
		SkySiteUtils.waitForElement(driver, btnCreatePunchPopover, 30);
		SkySiteUtils.waitTill(2000);
		dropdownAddNewPunchStamp.click();
		Log.message("Clicked on drop down button of stamp");
		SkySiteUtils.waitTill(2000);
		btnAddNewPunchStamp.click();
		Log.message("Clicked on Add new button");
		SkySiteUtils.waitTill(2000);
		textBoxStamp.sendKeys(Stamp_Number);
		textBoxStampTitle.sendKeys(Stamp_Number);
		btnCreatePunchStampOK.click();
		Log.message("Clicked on Right symbol After entered Stamp number and title.");
		SkySiteUtils.waitForElement(driver, symbolPunchStamp, 30);
		SkySiteUtils.waitTill(2000);
		String PunchNumber_AfterCreate= symbolPunchStamp.getText();
		String PunchName_AfterCreate = symbolPunchTitle.getAttribute("value");
		//Validate After Stamp Create	
		if((PunchNumber_AfterCreate.contentEquals(Stamp_Number)) && (PunchName_AfterCreate.contentEquals(Stamp_Number)))
		{
			result1=true;
			Log.message("Stamp Created Successfully with name: "+Stamp_Number);
			//Adding TO user
			btnAssignToPunchAnnotation.click();
			Log.message("Clicked on Assign to contact icon.");
			SkySiteUtils.waitForElement(driver, contactSearchField, 30);
			SkySiteUtils.waitTill(2000);
			contactSearchField.sendKeys(Employee_Name);
			iconSearchSelectUser.click();
			Log.message("Clicked on Search icon by entering input");
			SkySiteUtils.waitTill(3000);
			String Name_AfterSearch = nameFromSearchResult.getText();
			Log.message("Name After Search: "+Name_AfterSearch);
			String Mail_AfterSearch = emailFromSearchResult.getText();
			Log.message("Mail After Search: "+Mail_AfterSearch);
			if((Name_AfterSearch.contains(Employee_Name)) && (Mail_AfterSearch.contentEquals(MailId)))
			{
				Log.message("Search Results are coming properly!!!");
				checkboxSearchResult.click();
				btnSelectUser.click();
				Log.message("Clicked on Select user.");
				SkySiteUtils.waitTill(2000);
				iconCalenderDueDate.click();
				Log.message("Clicked on Due Date Calendar icon.");
				SkySiteUtils.waitTill(2000);
				driver.findElement(By.xpath("//div/div[1]/table/thead/tr[1]/th[3]")).click();//Click on next month
				SkySiteUtils.waitTill(2000);
				driver.findElement(By.xpath("(//div/div[1]/table/tbody/tr[4]/td[5])[1]")).click();//Pick date
				SkySiteUtils.waitTill(2000);
				txtPunchDescription.sendKeys(Punch_Description);
				SkySiteUtils.waitTill(2000);
				btnSaveAsDraft.click();
				Log.message("clicked on Save As Draft button after enter required details.");
				SkySiteUtils.waitTill(2000);
				SkySiteUtils.waitForElement(driver, notificationMessage, 30);
				String NotifyMsg_Aftr_AddedPunch_Annot = notificationMessage.getText();
				Log.message("Msg After click on save as draft is: "+NotifyMsg_Aftr_AddedPunch_Annot);
				SkySiteUtils.waitTill(10000);
				btnPunchList.click();
				Log.message("Clicked on dropdown button of punch list.");
				SkySiteUtils.waitTill(2000);
				String Punch_Number_AfterCreate=infoDraftPunchNOViewer.getText();
				Log.message("Punch Number After Create: "+Punch_Number_AfterCreate);			
				String Status_AfterCreate = DraftStatusViewer.getText();
				Log.message("Status After Create: "+Status_AfterCreate);
				SkySiteUtils.waitTill(5000);
				if((Punch_Number_AfterCreate.contentEquals(Stamp_Number)) && (Status_AfterCreate.equalsIgnoreCase("Draft"))
						 && (NotifyMsg_Aftr_AddedPunch_Annot.contentEquals("Punch annotation saved successfully")))
				{
					result2=true;
					Log.message("Punch Annotation saved as draft successfully in with name: "+Stamp_Number);
				}
				else
				{
					result2=false;
					Log.message("Punch Annotation Creation Failed!!!");
				}
			}
						
		}
		else
		{
			result1=false;
			Log.message("Stamp Creation Failed with name: "+Stamp_Number);
		}

		if ((result1==true) && (result2==true)) 
		{
			Log.message("Draft a punch and validate is working successfully.");
			return true;
		} else {
			Log.message("Draft a punch and validate is failed.");
			return false;
		}
	}
	
	/**
	 * Method written to verify Assigning the drafted punch in viewer 
	 * Scipted By: Naresh Babu
	 * Staging
	 * @return
	 * @throws IOException 
	 */
	public boolean Verify_Assign_DraftedPunch_InViewer(String Stamp_Number) 
	{
		SkySiteUtils.waitForElement(driver, btnPunchList, 30);
		btnPunchList.click();
		Log.message("Clicked on dropdown button of punch list.");
		SkySiteUtils.waitTill(2000);
		infoDraftPunchNOViewer.click();
		Log.message("Clicked on Drafted punch from the list to open it.");
		SkySiteUtils.waitTill(10000);
		driver.findElement(By.cssSelector("circle.leaflet-clickable")).click();
		Log.message("clicked on annotation image");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnCreatePunchPopover, 30);
		btnCreatePunchPopover.click();
		Log.message("Clicked on Create punch button.");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, notificationMessage, 30);
		String NotifyMsg_Aftr_AddedPunch_Annot = notificationMessage.getText();
		Log.message("Msg After Added Punch Annot is: "+NotifyMsg_Aftr_AddedPunch_Annot);
		SkySiteUtils.waitTill(10000);
		btnPunchList.click();
		Log.message("Clicked on dropdown button of punch list.");
		SkySiteUtils.waitTill(2000);
		String Punch_Number_AfterCreate=infoFirstPunchNOViewer.getText();
		Log.message("Punch Number After Create: "+Punch_Number_AfterCreate);			
		String Status_AfterCreate = OpenStatusViewer.getText();
		Log.message("Status After Create: "+Status_AfterCreate);
		ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
		Log.message("No Of Driver windows are: "+tabs2);
		driver.switchTo().window(tabs2.get(1));
		SkySiteUtils.waitTill(5000);
		driver.close();
		SkySiteUtils.waitTill(5000);
		driver.switchTo().window(tabs2.get(0));
		Log.message("Viewr is closed and navigated back to main page.");
		SkySiteUtils.waitTill(5000);
		if((Punch_Number_AfterCreate.contentEquals(Stamp_Number)) && (Status_AfterCreate.equalsIgnoreCase("Open"))
				&& (NotifyMsg_Aftr_AddedPunch_Annot.contentEquals("Punch annotation saved successfully")))
		{
			Log.message("Drafted Punch assigned successfully in with name: "+Stamp_Number);
			return true;
		}
		else
		{
			Log.message("Drafted Punch assigning Failed!!!");
			return false;
		}
	}
	
	/**
	 * Method written for Create Punch with outside attachments and validate in viewer 
	 * Scipted By: Naresh Babu
	 * Staging
	 * @return
	 * @throws IOException 
	 */
	public boolean Create_Punch_OutSideAttachments_AndValidate_InViewer(String Stamp_Number,String Employee_Name,String MailId,String Punch_Description,String FolderPath) throws IOException 
	{
		boolean result1 = false;
		boolean result2 = false;

		SkySiteUtils.waitForElement(driver, btnCreatePunchPopover, 30);
		SkySiteUtils.waitTill(2000);
		dropdownAddNewPunchStamp.click();
		Log.message("Clicked on drop down button of stamp");
		SkySiteUtils.waitTill(2000);
		btnAddNewPunchStamp.click();
		Log.message("Clicked on Add new button");
		SkySiteUtils.waitTill(2000);
		textBoxStamp.sendKeys(Stamp_Number);
		textBoxStampTitle.sendKeys(Stamp_Number);
		btnCreatePunchStampOK.click();
		Log.message("Clicked on Right symbol After entered Stamp number and title.");
		SkySiteUtils.waitForElement(driver, symbolPunchStamp, 30);
		SkySiteUtils.waitTill(2000);
		String PunchNumber_AfterCreate= symbolPunchStamp.getText();
		String PunchName_AfterCreate = symbolPunchTitle.getAttribute("value");
		//Validate After Stamp Create	
		if((PunchNumber_AfterCreate.contentEquals(Stamp_Number)) && (PunchName_AfterCreate.contentEquals(Stamp_Number)))
		{
			result1=true;
			Log.message("Stamp Created Successfully with name: "+Stamp_Number);
			//Adding TO user
			btnAssignToPunchAnnotation.click();
			Log.message("Clicked on Assign to contact icon.");
			SkySiteUtils.waitForElement(driver, contactSearchField, 30);
			SkySiteUtils.waitTill(2000);
			contactSearchField.sendKeys(Employee_Name);
			iconSearchSelectUser.click();
			Log.message("Clicked on Search icon by entering input");
			SkySiteUtils.waitTill(3000);
			String Name_AfterSearch = nameFromSearchResult.getText();
			Log.message("Name After Search: "+Name_AfterSearch);
			String Mail_AfterSearch = emailFromSearchResult.getText();
			Log.message("Mail After Search: "+Mail_AfterSearch);
			if((Name_AfterSearch.contains(Employee_Name)) && (Mail_AfterSearch.contentEquals(MailId)))
			{
				Log.message("Search Results are coming properly!!!");
				checkboxSearchResult.click();
				btnSelectUser.click();
				Log.message("Clicked on Select user.");
				SkySiteUtils.waitTill(2000);
				iconCalenderDueDate.click();
				Log.message("Clicked on Due Date Calendar icon.");
				SkySiteUtils.waitTill(2000);
				driver.findElement(By.xpath("//div/div[1]/table/thead/tr[1]/th[3]")).click();//Click on next month
				SkySiteUtils.waitTill(2000);
				driver.findElement(By.xpath("(//div/div[1]/table/tbody/tr[4]/td[5])[1]")).click();//Pick date
				SkySiteUtils.waitTill(2000);
				txtPunchDescription.sendKeys(Punch_Description);
				SkySiteUtils.waitTill(2000);
				btnAddAttachment.click();
				Log.message("Clicked on Add Attachment button.");
				SkySiteUtils.waitTill(10000);//Added due to loading problem
				SkySiteUtils.waitForElement(driver, btnSelectFilesPunchWindow, 20);
				btnSelectFilesPunchWindow.click();
				Log.message("Clicked on Select files button.");
				SkySiteUtils.waitTill(10000);
				// Writing File names into a text file for using in AutoIT Script
				BufferedWriter output;
				randomFileName rn = new randomFileName();
				String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
				output = new BufferedWriter(new FileWriter(tmpFileName, true));
				String expFilename = null;
				File[] files = new File(FolderPath).listFiles();
				for (File file : files) {
					if (file.isFile()) {
						expFilename = file.getName();// Getting File Names into a variable
						Log.message("Expected File name is:" + expFilename);
						output.append('"' + expFilename + '"');
						output.append(" ");
						SkySiteUtils.waitTill(1000);
					}
				}
				output.flush();
				output.close();

				// Executing .exe autoIt file
				String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
				Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
				Log.message("AutoIT Script Executed!!");
				SkySiteUtils.waitTill(10000);			
				SkySiteUtils.waitForElement(driver, txtNoOfFilesSelectedPunchWindow, 30);
				try
				{
					 File file = new File(tmpFileName);
					 if(file.delete())
					 {
						 Log.message(file.getName() + " is deleted!");
					 }
					 else
					 {
						 Log.message("Delete operation is failed.");
					 }
				}
				catch(Exception e)
				{
					Log.message("Exception occured!!!"+e);	
				}
				SkySiteUtils.waitTill(5000);		 
				String Attachment_Count=TotalAttachmentCount.getText();
				Log.message("Attachment Count: "+Attachment_Count);
				if(Attachment_Count.equals("1"))
				{
					Log.message("Files are selected to grid!!!");
					SkySiteUtils.waitTill(2000);
					btnCreatePunchPopover.click();
					Log.message("clicked on create punch button after enter required details.");
					SkySiteUtils.waitTill(2000);
					SkySiteUtils.waitForElement(driver, notificationMessage, 30);
					String NotifyMsg_Aftr_AddedPunch_Annot = notificationMessage.getText();
					Log.message("Msg After Added Punch Annot is: "+NotifyMsg_Aftr_AddedPunch_Annot);
					SkySiteUtils.waitTill(10000);
					btnPunchList.click();
					Log.message("Clicked on dropdown button of punch list.");
					SkySiteUtils.waitTill(2000);
					String Punch_Number_AfterCreate=infoFirstPunchNOViewer.getText();
					Log.message("Punch Number After Create: "+Punch_Number_AfterCreate);			
					String Status_AfterCreate = OpenStatusViewer.getText();
					Log.message("Status After Create: "+Status_AfterCreate);
					ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
					Log.message("No Of Driver windows are: "+tabs2);
					driver.switchTo().window(tabs2.get(1));
					SkySiteUtils.waitTill(5000);
					driver.close();
					SkySiteUtils.waitTill(5000);
					driver.switchTo().window(tabs2.get(0));
					Log.message("Viewr is closed and navigated back to main page.");
					SkySiteUtils.waitTill(5000);
					if((Punch_Number_AfterCreate.contentEquals(Stamp_Number)) && (Status_AfterCreate.equalsIgnoreCase("Open"))
							&& (NotifyMsg_Aftr_AddedPunch_Annot.contentEquals("Punch annotation saved successfully")))
					{
						result2=true;
						Log.message("Punch Annotation Created successfully in with name: "+Stamp_Number);
					}
					else
					{
						result2=false;
						Log.message("Punch Annotation Creation Failed!!!");
					}
				}
			}
						
		}
		else
		{
			result1=false;
			Log.message("Stamp Creation Failed with name: "+Stamp_Number);
		}
		if ((result1==true) && (result2==true)) 
		{
			Log.message("Creating a punch Annotation and validate is working successfully.");
			return true;
		} else {
			Log.message("Creating a punch Annotation and validate is failed.");
			return false;
		}
	}
	
}