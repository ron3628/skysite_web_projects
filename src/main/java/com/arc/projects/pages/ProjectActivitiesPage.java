package com.arc.projects.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arc.projects.utils.randomFileName;
import com.arcautoframe.utils.Log;
import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import com.arc.projects.pages.ProjectGalleryPage;


public class ProjectActivitiesPage extends LoadableComponent<ProjectActivitiesPage>{
	
	WebDriver driver;
	private boolean isPageLoaded;
	
	/** Identifying web elements using FindBy annotation*/
	
	@FindBy(css = "#liActivityExport > a:nth-child(1) > span:nth-child(2)")
	WebElement btnExportToCSV;
	
	@FindBy(css = "#ulActivityList > li:nth-child(1) > div:nth-child(3) > a:nth-child(1)")
	WebElement btnViewDetails;
	
	@FindBy(xpath = "//*[@id=\"aProjHeaderLink\"]")
	WebElement btnBreadcrumbProject;
	
	@FindBy(css= "#ulActivityList > li:nth-child(1) > div:nth-child(2) > h4:nth-child(2) > span:nth-child(1)")
	WebElement txtViewedProject;	
	
	@FindBy(css= "#activity-deletefile-modal > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > h4:nth-child(2)")
	WebElement viewDetailsPopupHeader;
	
	@FindBy(css="#activity-deletefile-modal > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > button:nth-child(1)")
	WebElement btnCloseViewDetailsPopover;
	
	@FindBy(css="#ulActivityList > li:nth-child(1) > div:nth-child(2) > h4:nth-child(2) > span:nth-child(1)")
	WebElement txtViewFile; 
	
	@FindBy(css=".pw-modal-scroll > dl:nth-child(1) > dd:nth-child(2) > ol:nth-child(1) > li:nth-child(1)")
	WebElement filename;
	
	@FindBy(css=".pw-modal-scroll > dl:nth-child(1) > dd:nth-child(2) > ol:nth-child(1) > li:nth-child(1)")
	WebElement photoname;
	
	@FindBy(xpath="//*[@id='ulActivityList']/li[1]/div[2]/h4[2]/span")	
	WebElement createdPunchDetails;
	
	@FindBy(xpath="//*[@id='ulActivityList']/li[1]/div[2]/h4[2]/span")
	WebElement createdRFIDetails;
	
	@FindBy(xpath="//*[@id='ulActivityList']/li[1]/div[2]/h4[2]/span")
	WebElement createdSubmittalDetails;
	
	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnExportToCSV, 20);

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		

	}
	
	/**
	 * Declaring constructor for initializing web elements using PageFactory
	 * class.
	 * 
	 * @param driver
	 * @return
	 */
	public ProjectActivitiesPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	/** 
	 * Method for validating View Details Button for Photo Upload in Gallery
	 * Scripted By: Ranadeep
	 * @return 
	 */	
	
	
	public boolean viewDetailsPhoto() {
		
		 SkySiteUtils.waitForElement(driver, btnExportToCSV, 30);
		 btnViewDetails.click();
		 Log.message("Clicked on View Details button");
		 SkySiteUtils.waitForElement(driver, viewDetailsPopupHeader, 30);
		 String PopoverHeader=viewDetailsPopupHeader.getText();
		 Log.message("View details popover header is:- " +PopoverHeader);		 
		 String actualPhotoName=PropertyReader.getProperty("photoname");
		 Log.message("Actual photo name is:- " +actualPhotoName);		 
		 String uploadedPhotoName=photoname.getText();
		 Log.message("Uploaded Photo name is:- " +uploadedPhotoName);
		 String actualPopoverheader="Upload photo(s) details";
		 btnCloseViewDetailsPopover.click();
		 Log.message("Clicked on close button in view details popover");
		 SkySiteUtils.waitTill(5000);
		 
		 
		 if (PopoverHeader.equals(actualPopoverheader) && uploadedPhotoName.equals(actualPhotoName))
		 {
			 return true;
		 } 
		 
		 else
		 {
			 return false;
		 }
		
	}
	
	
	/** 
	 * Method for validating View Details Button for File Upload in Folder
	 * Scripted By: Ranadeep
	 * @return 
	 */	
	
	
	public boolean viewDetailsFile() 
	{
		
		 SkySiteUtils.waitForElement(driver, btnExportToCSV, 30);
		 btnViewDetails.click();
		 Log.message("Clicked on View Details button");
		 SkySiteUtils.waitForElement(driver, viewDetailsPopupHeader, 30);
		 String PopoverHeader=viewDetailsPopupHeader.getText();
		 Log.message("View details popover header is:- " +PopoverHeader);		 
		 String actualFilename=PropertyReader.getProperty("Filename");
		 Log.message("Actual filename is:- " +actualFilename);
		 
		 String uploadedFilename=filename.getText();
		 Log.message("Uploaded filename is:- " +uploadedFilename);
		 String actualPopoverheader="Upload file(s) details";
		 btnCloseViewDetailsPopover.click();
		 Log.message("Clicked on close button in view details popover");
		 SkySiteUtils.waitTill(5000);
		 
		 if (PopoverHeader.equals(actualPopoverheader) && uploadedFilename.equals(actualFilename))
		 {
			 return true;
		 } 
		 
		 else
		 {
			 return false;
		 }
		
		 
		 
	}
	
	
	
	/** 
	 * Method for navigating to folder page from Projects Activity Page
	 * Scripted By: Ranadeep
	 * @return 
	 */	
	
	public FolderPage enterProjectFromBreadcrumb() 
	{
		
		 SkySiteUtils.waitForElement(driver, btnExportToCSV, 30);
		 btnBreadcrumbProject.click();
		 Log.message("Clicked to enter project from breadcrumb");
		 SkySiteUtils.waitTill(3000);
		 
		 return new FolderPage(driver).get();
		 
	}
	
	
	/** 
	 * Method for validating Project View 
	 * Scripted By: Ranadeep
	 * @return 
	 */	
	
	 public boolean viewProject() 
	 {
			
			 SkySiteUtils.waitForElement(driver, btnExportToCSV, 30);
			 String Actual=txtViewedProject.getText();
			 Log.message("Actual text is " +Actual);
			 String Expected= "Viewed project";
			 Log.message("Expected text is " +Expected);
			 
			 if (Actual.contentEquals(Expected)) 
			 	{
					return true;
				} 
			 
			 else{
					return false;
				}
	 }
	 
	 /** 
	  * Method for validating Project view
	  * Scripted By: Ranadeep
	  * @return 
	  */
	 
	 public boolean deleteFileActivity() 
	 {
			
			 SkySiteUtils.waitForElement(driver, btnExportToCSV, 30);
			 btnViewDetails.click();
			 Log.message("Clicked on View Details button");
			 SkySiteUtils.waitForElement(driver, viewDetailsPopupHeader, 30);
			 String PopoverHeader=viewDetailsPopupHeader.getText();
			 Log.message("View details popover header is:- " +PopoverHeader);
			 
			 String actualFilename=PropertyReader.getProperty("Filename");
			 Log.message("Actual filename is:- " +actualFilename);
			 String deletedFilename=filename.getText();
			 Log.message("Deleted filename is:- " +deletedFilename);
			 String actualPopoverheader="Deleted document(s) details";
			 if (PopoverHeader.equals(actualPopoverheader) && actualFilename.equals(deletedFilename))
			 {
				 return true;
			 } 
			 
			 else
			 {
				 return false;
			 }
			 			 
	 }
	 
	 /** 
	  * Method for file view
	  * Scripted By: Ranadeep
	  * @return 
	  */
	 
	 public boolean viewFileActivity() 
	 {
		 SkySiteUtils.waitTill(3000);
		 		 
		 String viewFileText = txtViewFile.getText();
		 Log.message("Text displayed after file view:-" +viewFileText);
		 String actualViewFileText = "Viewed document 'A1.pdf', in folder 'TP_Folder_01'";
		 
		 if (viewFileText.equals(actualViewFileText))
		 {
			 return true;
		 } 
		 
		 else
		 {
			 return false;
		 }
	 }
	 
	 
	 	/** 
		 * Method for validating Punch creation
		 * Scripted By: Ranadeep
		 * @return 
		 */	
		
		
		public boolean createPunchActivity() 
		{
			
			SkySiteUtils.waitForElement(driver, btnExportToCSV, 30);
			SkySiteUtils.waitTill(5000);
			
			String expectedPunchStamp = PropertyReader.getProperty("Stamp");
			String expectedPunchStampTitle = PropertyReader.getProperty("StampTitle");
			
			Log.message("Expected Punch Stamp is:-" +expectedPunchStamp);
			Log.message("Expected Punch Stamp Title is:-" +expectedPunchStampTitle);
			
			String actualPunchDetails = createdPunchDetails.getText();
			Log.message("Created Punch details:-" +actualPunchDetails);
			
			if(actualPunchDetails.contains(expectedPunchStamp) && actualPunchDetails.contains(expectedPunchStampTitle))
			{
				return true;
			}
			else
			{
				return false;
			}		 
			
		}
		
		/** 
		 * Method for validating RFI creation
		 * Scripted By: Ranadeep
		 * @return 
		 */	
		
		
		public boolean createRFIActivity() 
		{
			
			SkySiteUtils.waitForElement(driver, btnExportToCSV, 30);
			SkySiteUtils.waitTill(5000);
			
			String RFIdetails=createdRFIDetails.getText();
			Log.message("RFI details:- " +RFIdetails);
					
			if(RFIdetails.contains("Created RFI"))
			{
				return true;
			}
			else
			{
				return false;
			}		 
				
			
		}

		

		/** 
		 * Method for validating Submittal creation
		 * Scripted By: Ranadeep
		 * @return 
		 */	

		public boolean createSubmittalActivity(String randonSubmittalNumber) 
		{
			SkySiteUtils.waitForElement(driver, btnExportToCSV, 30);
			SkySiteUtils.waitTill(5000);
			
			String Submittaldetails=createdSubmittalDetails.getText();
			Log.message("RFI details:- " +Submittaldetails);
					
			if(Submittaldetails.contains("Created Submittal") && Submittaldetails.contains(randonSubmittalNumber))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
}
	 
	 




