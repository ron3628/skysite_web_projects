package com.arc.projects.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;

import org.testng.Assert;

import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arc.projects.utils.randomFileName;

import com.arcautoframe.utils.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



public class ContactsPage extends LoadableComponent<ContactsPage>{
	
	WebDriver driver;
	private boolean isPageLoaded;
	
	ProjectDashboardPage projectDashboardPage;
	
	/**
	 * Identifying web elements using FindBy annotation.
	 */
	
	@FindBy(css=".icon.icon-app-contact.icon-lg.pw-icon-white")
	WebElement btnContacts;
	
	@FindBy(css="#add-contact")
	WebElement btnAddNewContact;
	
	@FindBy(css="#btnCompany")
	WebElement btnCompany;
	
	@FindBy(xpath="//li[@class='media grp-dtls cmp-grp-dtls active']")
	WebElement objActiveCompany;
	
	@FindBy(css=".pw-sel-contact.pw-sel-contact-delete")
	WebElement btnDeleteBySelectContacts;
	
	@FindBy(css="#btnCreate")
	WebElement btnSaveAddContactWind;

	@FindBy(css=".btn.btn-default.pw-cntct-cancel.clear-and-close")
	WebElement btnCancelAddContactWind;
	
	@FindBy(css="#txtFirstName")
	WebElement txtFirstName;
	
	@FindBy(css="#txtLastName")
	WebElement txtLastName;
	
	@FindBy(css="#txtCompany")
	WebElement txtCompany;
	
	@FindBy(css="#txtAddress")
	WebElement txtAddress;
	
	@FindBy(css="#txtCity")
	WebElement txtCity;
	
	@FindBy(css="#txtCountry")
	WebElement txtCountry;
	
	@FindBy(css="#txtState")
	WebElement txtState;
	
	@FindBy(css="#txtZip")
	WebElement txtZip;
	
	@FindBy(css="#txtPhone")
	WebElement txtPhone;
	
	@FindBy(css="#txtEmail")
	WebElement txtEmail;
	
	@FindBy(css="#txtFax")
	WebElement txtFax;
	
	@FindBy(css=".noty_text")
	WebElement ContactCreationSuccessMsg;
	
	@FindBy(css="#btnUpdate")
	WebElement btnUpdateContact;
	
	@FindBy(css="#Export-Contact")
	WebElement btnExportToCSV;
	
	@FindBy(css="#btnContactGroup")
	WebElement btnContGroup;
	
	@FindBy(css="#add-contact-group")
	WebElement btnAddNewGroup;
	
	@FindBy(xpath="//div[@class='row pw-cntct-details']/h3")
	WebElement AccNameShareWindow;
	
	@FindBy(css="#btnShareContact")
	WebElement btnShareContact;
	
	@FindBy(css="#txtShareEmail")
	WebElement txtShareEmail;
	
	//Import Contacts
	@FindBy(css="#import-contacts")
	WebElement btnImportContact;
	@FindBy(css="#btnSelectFileNext")
	WebElement btnNextImportContacts;
	@FindBy(xpath="//input[@name='qqfile']")
	WebElement btnChooseFile;
	@FindBy(css="#btnMapImportImport")
	WebElement btnImportImportContacts;
	@FindBy(css="#btnContactResultClose")
	WebElement btnContactResultClose;
	
	@FindBy(css="#button-1")
	WebElement btnYesAlertMsg;
	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnContacts, 20);
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}
	
	/**
	 *  Declaring constructor for initializing web elements using PageFactory class.
	 * @param driver
	 * @return 
	 */
	public ContactsPage(WebDriver driver) 
	{
		   this.driver = driver;
		   PageFactory.initElements(this.driver, this);
	}
	
	/** 
	 * Method written for Add a new Contact
	 * Scipted By: Naresh Babu
	 * @return 
	 * @return
	 */
	public boolean Add_New_Contact(String Inp_FirstName)
	{
		SkySiteUtils.waitForElement(driver, btnAddNewContact, 20);
		btnAddNewContact.click();
		Log.message("Clicked on Add New Contact button");
		SkySiteUtils.waitForElement(driver, btnSaveAddContactWind, 20);
		
		//Generating all input values randomly
		String Random_Number = Generate_Random_Number.generateRandomValue();
		//String Inp_FirstName = "FstName_"+Random_Number;
		String Inp_LastName = "LstName_"+Random_Number;
		String Inp_Address = "Address_"+Random_Number;
		String Inp_City = "City_"+Random_Number;
		String Inp_Zip = Random_Number;
		String Inp_state = null;
		String Inp_Country = null;
		String Inp_Phone = Random_Number+Random_Number;
		String Inp_Email = Inp_FirstName+"@yopmail.com";
		String Inp_Fax = Random_Number+Random_Number;
		
		SkySiteUtils.waitTill(3000);
		txtFirstName.sendKeys(Inp_FirstName);
		txtLastName.sendKeys(Inp_LastName);
		//String Exp_Company = txtCompany.getAttribute("value");
		txtAddress.sendKeys(Inp_Address);
		txtCity.sendKeys(Inp_City);
		
		//Generating a random value in between 1 to 200
		int Max = 201;
		int Min = 1;
		//Create Instance of Random Class
		Random ranomNum = new Random();
		int Gen_Random_Val = Min + ranomNum.nextInt(Max);
		Log.message("Genarated Random Value is: "+Gen_Random_Val);
		
		
		driver.findElement(By.xpath("//div[@id='ddlCountrydrop']")).click();
		SkySiteUtils.waitTill(2000);
		driver.findElement(By.xpath("(//a[@class='Country-Item'])["+Gen_Random_Val+"]")).click();//Select Any Country
		SkySiteUtils.waitTill(2000);
		Inp_Country = txtCountry.getAttribute("value");
		Log.message("Selected Country is: "+Inp_Country);
	
		SkySiteUtils.waitTill(2000);
		driver.findElement(By.xpath("//div[@id='ddlstatedrop']")).click();
		SkySiteUtils.waitTill(2000);
		driver.findElement(By.xpath("(//a[@class='State-Item'])[1]")).click();//Select Any State
		SkySiteUtils.waitTill(2000);
		Inp_state = txtState.getAttribute("value");
		Log.message("Selected State is: "+Inp_state);
		
		txtZip.sendKeys(Inp_Zip);
		txtPhone.sendKeys(Inp_Phone);
		txtEmail.sendKeys(Inp_Email);
		txtFax.sendKeys(Inp_Fax);
		
		btnSaveAddContactWind.click();
		Log.message("Clicked on save button");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, ContactCreationSuccessMsg, 30);
		String expectedContactSuccessMsg = ContactCreationSuccessMsg.getText();
		String actualContactSuccessMsg = "Contact created successfully";
		if(expectedContactSuccessMsg.contains(actualContactSuccessMsg))
			return true;
			else
			return false;	
			
	}
	
	/** 
	 * Method written for Add a new Contact by changing company name
	 * Scipted By: Naresh Babu
	 * Staging
	 * @return 
	 * @return
	 */
	public boolean Add_NewContact_ByChangingCompanyName(String Inp_FirstName, String Company_Name)
	{
		SkySiteUtils.waitForElement(driver, btnAddNewContact, 20);
		btnAddNewContact.click();
		Log.message("Clicked on Add New Contact button");
		SkySiteUtils.waitForElement(driver, btnSaveAddContactWind, 20);
		
		//Generating all input values randomly
		String Random_Number = Generate_Random_Number.generateRandomValue();
		//String Inp_FirstName = "FstName_"+Random_Number;
		String Inp_LastName = "LstName_"+Random_Number;
		String Inp_Address = "Address_"+Random_Number;
		String Inp_City = "City_"+Random_Number;
		String Inp_Zip = Random_Number;
		String Inp_state = null;
		String Inp_Country = null;
		String Inp_Phone = Random_Number+Random_Number;
		String Inp_Email = Inp_FirstName+"@yopmail.com";
		String Inp_Fax = Random_Number+Random_Number;
		
		SkySiteUtils.waitTill(3000);
		txtFirstName.sendKeys(Inp_FirstName);
		txtLastName.sendKeys(Inp_LastName);
		txtCompany.clear();
		Log.message("Clear the existed company.");
		txtCompany.sendKeys(Company_Name);
		txtAddress.sendKeys(Inp_Address);
		txtCity.sendKeys(Inp_City);
		
		//Generating a random value in between 1 to 200
		int Max = 201;
		int Min = 1;
		//Create Instance of Random Class
		Random ranomNum = new Random();
		int Gen_Random_Val = Min + ranomNum.nextInt(Max);
		Log.message("Genarated Random Value is: "+Gen_Random_Val);
		
		
		driver.findElement(By.xpath("//div[@id='ddlCountrydrop']")).click();
		SkySiteUtils.waitTill(2000);
		driver.findElement(By.xpath("(//a[@class='Country-Item'])["+Gen_Random_Val+"]")).click();//Select Any Country
		SkySiteUtils.waitTill(2000);
		Inp_Country = txtCountry.getAttribute("value");
		Log.message("Selected Country is: "+Inp_Country);
	
		SkySiteUtils.waitTill(2000);
		driver.findElement(By.xpath("//div[@id='ddlstatedrop']")).click();
		SkySiteUtils.waitTill(2000);
		driver.findElement(By.xpath("(//a[@class='State-Item'])[1]")).click();//Select Any State
		SkySiteUtils.waitTill(2000);
		Inp_state = txtState.getAttribute("value");
		Log.message("Selected State is: "+Inp_state);
		
		txtZip.sendKeys(Inp_Zip);
		txtPhone.sendKeys(Inp_Phone);
		txtEmail.sendKeys(Inp_Email);
		txtFax.sendKeys(Inp_Fax);
		
		btnSaveAddContactWind.click();
		Log.message("Clicked on save button");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, ContactCreationSuccessMsg, 30);
		String expectedContactSuccessMsg = ContactCreationSuccessMsg.getText();
		String actualContactSuccessMsg = "Contact created successfully";
		if(expectedContactSuccessMsg.contains(actualContactSuccessMsg))
			return true;
			else
			return false;	
			
	}
	
	/** 
	 * Method written for validate new company from companies list
	 * Scipted By: Naresh Babu
	 * Staging
	 * @return 
	 * @return
	 */
	public boolean Validate_CompanyName_UnderCompanies_AndDelete(String Company_Name)
	{
		boolean result1 = false;
		boolean result2 = false;
		
		SkySiteUtils.waitForElement(driver, btnAddNewContact, 20);
		btnCompany.click();
		Log.message("Clicked on Companies button");
		SkySiteUtils.waitForElement(driver, objActiveCompany, 30);
		SkySiteUtils.waitTill(5000);
		//Count of available companies list
		int NoOf_Comanies = driver.findElements(By.xpath(".//*[@id='grpul']/li/section[2]/h4")).size();
		Log.message("Available no of companies are: "+NoOf_Comanies);
		int i = 0;
		for(i=1; i<=NoOf_Comanies; i++)
		{
			String Act_CompName = driver.findElement(By.xpath("//*[@id='grpul']/li["+i+"]/section[2]/h4")).getText();
			if(Act_CompName.contentEquals(Company_Name))
			{
				result1=true;
				Log.message("Expected company available in the list.");
				break;
			}
			
		}
		if(result1==true)
		{
			driver.findElement(By.xpath("//*[@id='grpul']/li["+i+"]/section[2]/h4")).click();
			Log.message("Clicked on expected company from the list.");
			SkySiteUtils.waitTill(10000);
			driver.findElement(By.xpath("(//input[@id='chkSeletAll'])[2]")).click();
			SkySiteUtils.waitTill(3000);
			String InfoAfter_ContactSelect = driver.findElement(By.xpath(".//*[@id='divMainCompanyCount']/div")).getText();
			Log.message("Info after sealect a contact is: "+InfoAfter_ContactSelect);
			btnDeleteBySelectContacts.click();
			Log.message("Clicked on Delete button.");
			SkySiteUtils.waitForElement(driver, btnYesAlertMsg, 20);
			btnYesAlertMsg.click();
			SkySiteUtils.waitTill(2000);
			SkySiteUtils.waitForElement(driver, ContactCreationSuccessMsg, 20);
			String Msg_afterDeleteContacts = ContactCreationSuccessMsg.getText();
			Log.message("Msg after Delete Contact is: "+Msg_afterDeleteContacts);
			//Count of available companies list after delete one
			int NoOf_Comanies_AfterDelete = driver.findElements(By.xpath(".//*[@id='grpul']/li/section[2]/h4")).size();
			Log.message("Available companies after delete is: "+NoOf_Comanies_AfterDelete);
			if((NoOf_Comanies_AfterDelete==(NoOf_Comanies-1)) && (Msg_afterDeleteContacts.contentEquals("Contact(s) deleted successfully"))
					&& (InfoAfter_ContactSelect.contentEquals("1 contact on this page is selected.")))
			{
				result2 = true;
				Log.message("Company is deleted successfully.");
			}
		}
		
		if((result1==true) && (result2==true))
			return true;
			else
			return false;	
			
	}
	
	/** 
	 * Method written for Share Contact Details and Validate
	 * Scipted By: Naresh Babu Kavuru
	 * Staging
	 * @return
	 */
	public boolean Share_Contact_Details_AndValidate(String Exp_ContName, String Email_ToShare) throws AWTException, InterruptedException, IOException
	{
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnExportToCSV, 30);
		SkySiteUtils.waitTill(5000);
		
	//Getting contacts count 
		int Available_Contacts=0;
		int i =1;
		String Act_ContactName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'lbl-Email')]"));
		for (WebElement Element : allElements)
		{ 
			Available_Contacts = Available_Contacts+1; 	
		}
		Log.message("Available contacts are: "+Available_Contacts);
		for(i=1;i<=Available_Contacts;i++)
		{
			Act_ContactName = driver.findElement(By.xpath("(//h4[@class='lbl-ContactName'])["+i+"]")).getText();
			//Log.message("Exp Contact Name is: "+ExpContactName);
			if(Act_ContactName.contains(Exp_ContName))
			{
				Log.message("Expected Contact is available in the list.");
				break;
			}
		}
		
		driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal icon-lg'])["+i+"]")).click();
		Log.message("Clicked on expected contact more option");
		//i=i+1;
		WebElement ShareLink = driver.findElement(By.xpath("(//a[@id='share-contactDDL'])["+i+"]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",ShareLink);
		Log.message("Clicked on Share Tab for the contact.");
		SkySiteUtils.waitForElement(driver, btnShareContact, 20);
		SkySiteUtils.waitTill(5000);
		String NameFrom_ShareWindow=AccNameShareWindow.getText();
		Log.message("Name From Share Window is: "+NameFrom_ShareWindow);
		String contactShareMsg = null;
		if(NameFrom_ShareWindow.contains(Exp_ContName))
		{
			Log.message("Account details in contact share window is proper.");
			txtShareEmail.sendKeys(Email_ToShare);
			Log.message("Entered email id to whome want to share.");
			btnShareContact.click();
			Log.message("Clicked on Share button.");
			SkySiteUtils.waitTill(2000);
			SkySiteUtils.waitForElement(driver, ContactCreationSuccessMsg, 20);
			contactShareMsg = ContactCreationSuccessMsg.getText();
			Log.message("Notify message after Share a contact is: "+contactShareMsg);
			SkySiteUtils.waitTill(5000);
		}
	
	//Final Validation
		if(contactShareMsg.equalsIgnoreCase("Shared successfully"))
		{
			Log.message("Contact is Shared successfully.");
			return true;
		}
		else
		{
			Log.message("Contact Sharing Failed!!!");
			return false;
		}	
			
	}
	
	/** 
	 * Method written for Add a new Contact and export
	 * Scipted By: Naresh Babu Kavuru
	 * @return
	 */
	public boolean Add_New_Contact_AndExport(String Sys_Download_Path,String Inp_FirstName,String csvFileToRead) throws AWTException, InterruptedException, IOException
	{
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnExportToCSV, 30);
		SkySiteUtils.waitTill(5000);
		
	//Getting contacts count 
		int Available_Contacts=0;
		int i =1;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'lbl-Email')]"));
		for (WebElement Element : allElements)
		{ 
			Available_Contacts = Available_Contacts+1; 	
		}
		
		Log.message("Available contacts are: "+Available_Contacts);
		
		for(i=1;i<=Available_Contacts;i++)
		{
			String ExpContactName = driver.findElement(By.xpath("(//h4[@class='lbl-ContactName'])["+i+"]")).getText();
			//Log.message("Exp Contact Name is: "+ExpContactName);
			if(ExpContactName.contains(Inp_FirstName))
			{
				Log.message("Expected Contact is available in the list.");
				break;
			}
		}
		
		driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal icon-lg'])["+i+"]")).click();
		Log.message("Clicked on expected contact more option");
		//Available_Contacts=Available_Contacts-1;
		WebElement EditLink = driver.findElement(By.xpath("(//a[contains(text(),'Edit')])["+i+"]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",EditLink);//JSExecutor to click edit
		Log.message("Clicked on Edit Tab for the contact.");
		SkySiteUtils.waitForElement(driver, btnUpdateContact, 30);//Wait for save button of edit fields window
		SkySiteUtils.waitTill(2000);
		
		
		//Getting all expected values from edit contact window
		String Exp_FirstName = txtFirstName.getAttribute("value");
		String Exp_LastName = txtLastName.getAttribute("value");
		String Exp_Company = txtCompany.getAttribute("value");
		String Exp_Address = txtAddress.getAttribute("value");
		String Exp_City = txtCity.getAttribute("value");
		String Exp_Zip = txtZip.getAttribute("value");
		String Exp_state = txtState.getAttribute("value");
		String Exp_Country = txtCountry.getAttribute("value");
		String Exp_Phone = txtPhone.getAttribute("value");
		String Exp_Email = txtEmail.getAttribute("value");
		String Exp_Fax = txtFax.getAttribute("value");
		
		btnCancelAddContactWind.click();
		Log.message("Clicked on cancel button from Edit Contact window");
		
	//Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(10000);
					
		btnExportToCSV.click();
		Log.message("Clicked on Export to csv button!!!");
		SkySiteUtils.waitTill(10000);
					
		//Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: "+browserName);
					
		if(browserName.contains("firefox"))
		{
			//Handling Download PopUp of firefox browser using robot
					Robot robot=null;
					robot=new Robot();
						
					robot.keyPress(KeyEvent.VK_ALT);
					SkySiteUtils.waitTill(2000);
					robot.keyPress(KeyEvent.VK_S);
					SkySiteUtils.waitTill(2000);
					robot.keyRelease(KeyEvent.VK_ALT);
					robot.keyRelease(KeyEvent.VK_S);
					SkySiteUtils.waitTill(3000);
					robot.keyPress(KeyEvent.VK_ENTER);
					robot.keyRelease(KeyEvent.VK_ENTER);
					SkySiteUtils.waitTill(20000);
		}
				
		//Validating the CSV file from download folder	
		BufferedReader br = null;
		String line = ""; 
		String splitBy = ",";
		int count = 0;
		String Act_FirstName = null;
		String Act_LastName = null;
		String Act_Company = null;
		String Act_Address = null;
		String Act_City = null;
		String Act_Zip = null;
		String Act_state = null;
		String Act_Country = null;
		String Act_Phone = null;
		String Act_Email = null;
		String Act_Fax = null;
		int Match_Count = 0;
		
		br = new BufferedReader(new FileReader(csvFileToRead)); 
		while ((line = br.readLine()) != null) 
		{  
			count=count+1;
			Log.message("Downloaded csv file have data in : "+count+ " rows");
			String[] ActValue = line.split(splitBy);
			Act_FirstName = ActValue[0];
			Act_FirstName = Act_FirstName.replace("\"", "");
			//Log.message("Contact First Name from csv file is: "+ Act_FirstName);
			
			Act_LastName = ActValue[1];
			Act_LastName = Act_LastName.replace("\"", "");
			//Log.message("Contact Last Name from csv file is: "+ Act_LastName);
			
			Act_Company = ActValue[2];
			Act_Company = Act_Company.replace("\"", "");
			//Log.message("Contact Company Name from csv file is: "+ Act_Company);
			
			Act_Address = ActValue[3];
			Act_Address = Act_Address.replace("\"", "");
			//Log.message("Contact Address from csv file is: "+ Act_Address);
			
			Act_Email = ActValue[4];
			Act_Email = Act_Email.replace("\"", "");
			// Log.message("Contact Email from csv file is: "+ Act_Email);
			
			Act_City = ActValue[5];
			Act_City = Act_City.replace("\"", "");
			//Log.message("Contact City Name from csv file is: "+ Act_City);
			
			Act_Country = ActValue[6];
			Act_Country = Act_Country.replace("\"", "");
			//Log.message("Contact Country Name from csv file is: "+ Act_Country);
			
			Act_state = ActValue[7];
			Act_state = Act_state.replace("\"", "");
			//Log.message("Contact Sate Name from csv file is: "+ Act_state);
			
			Act_Zip = ActValue[8];
			Act_Zip = Act_Zip.replace("\"", "");
			//Log.message("Contact Zip Number from csv file is: "+ Act_Zip);
			
			Act_Fax = ActValue[9];
			Act_Fax = Act_Fax.replace("\"", "");
			//Log.message("Contact Fax Number from csv file is: "+ Act_Fax);
			
			Act_Phone = ActValue[10];
			Act_Phone = Act_Phone.replace("\"", "");
			//Log.message("Contact Phone Number from csv file is: "+ Act_Phone);
		
			if((Act_FirstName.contains(Inp_FirstName))&&(Act_LastName.contentEquals(Exp_LastName))&&(Act_Company.contentEquals(Exp_Company))
					&&(Act_Address.contentEquals(Exp_Address))&&(Act_Email.contentEquals(Exp_Email))&&((Act_City).contentEquals(Exp_City))
					&&(Act_Country.contentEquals(Exp_Country))&&(Act_state.contentEquals(Exp_state))&&(Act_Zip.contentEquals(Exp_Zip))
					&&(Act_Phone.contentEquals(Exp_Phone))&&(Act_Fax.contentEquals(Exp_Fax)))
			{
				Match_Count = Match_Count+1;
			}
		}
		Log.message("Downloaded csv file have data in : "+count+ " rows");
		Available_Contacts=Available_Contacts+1;
		Log.message("Avl data rows"+Available_Contacts);
		
		br.close();//Newly Added
	
	//Final Validation
		if((count==Available_Contacts)&&(Match_Count == 1))
		{
			Log.message("Contacts Exported File is having data properly!!!");
			return true;
		}
		else
		{
			Log.message("Contacts Exported File is NOT having data properly!!!");
			return false;
		}	
			
	}
	
	
	/** 
	 * Method written for Contacts Group Level export
	 * Scipted By: Naresh Babu
	 * @return 
	 */
	public boolean Contacts_GroupLevel_Export(String Sys_Download_Path,String csvFileToRead) throws AWTException, InterruptedException, IOException
	{
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnContGroup, 30);
		SkySiteUtils.waitTill(5000);
		btnContGroup.click();
		Log.message("Clicked on Groups button.");
		SkySiteUtils.waitForElement(driver, btnAddNewGroup, 30);
		SkySiteUtils.waitTill(3000);
		
	//Getting contacts count 
		int Available_Contacts=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'lbl-Email')]"));
		for (WebElement Element : allElements)
		{ 
			Available_Contacts = Available_Contacts+1; 	
		}		
		Log.message("Available contacts are: "+Available_Contacts);
		
		//Getting all expected values from edit contact window
		String ExpContactName = driver.findElement(By.xpath("(//h4[@class='lbl-ContactName'])["+Available_Contacts+"]")).getText();
		//Log.message("Exp Contact Name is: "+ExpContactName);
		String ExpCompanyName = driver.findElement(By.xpath("(//h5[@class='lbl-CompanyName'])["+Available_Contacts+"]")).getText();
		//Log.message("Exp Company Name is: "+ExpCompanyName);
		String ExpMailId = driver.findElement(By.xpath("(//a[@class='lbl-Email'])["+Available_Contacts+"]")).getText();
		//Log.message("Exp Mail Id is: "+ExpMailId);
		
	//Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(10000);
							
		btnExportToCSV.click();
		Log.message("Clicked on Export to csv button!!!");
		SkySiteUtils.waitTill(10000);
							
	//Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: "+browserName);
							
		if(browserName.contains("firefox"))
		{
			//Handling Download PopUp of firefox browser using robot
				Robot robot=null;
				robot=new Robot();
								
				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(20000);
		}
							
		// Validating the CSV file from download folder	
		BufferedReader br = null;
		String line = ""; 
		String splitBy = ",";
		int count = 0;
		String ActContactName = null;
		String ActCompanyName = null;
		String ActMailId = null;
		int Match_Count = 0;
		
		br = new BufferedReader(new FileReader(csvFileToRead)); 
		while ((line = br.readLine()) != null) 
		{  
			count=count+1;
			//Log.message("Downloaded csv file have data in : "+count+ " rows");
			String[] ActValue = line.split(splitBy);
			ActContactName = ActValue[0];
			ActContactName = ActContactName.replace("\"", "");
			//Log.message("Contact First Name from csv file is: "+ Act_FirstName);
			
			ActCompanyName = ActValue[2];
			ActCompanyName = ActCompanyName.replace("\"", "");
			
			ActMailId = ActValue[4];
			ActMailId = ActMailId.replace("\"", "");
				
			if((ExpContactName.contains(ActContactName))&&(ActCompanyName.contentEquals(ExpCompanyName))&&(ActMailId.contentEquals(ExpMailId)))
			{
				Match_Count = Match_Count+1;
			}
		}
		Log.message("Downloaded csv file have data in : "+count+ " rows");
		Available_Contacts=Available_Contacts+1;
		
		br.close();//Newly Added
	
	//Final Validation
		if((count==Available_Contacts)&&(Match_Count == 1))
		{
			Log.message("Contacts Group Exported File is having data properly!!!");
			return true;
		}
		else
		{
			Log.message("Contacts Group Exported File is NOT having data properly!!!");
			return false;
		}	
			
	}
	
	/** 
	 * Method written for Write new values into Import Contacts file
	 * Scipted By: Naresh Babu
	 * @return 
	 */
	public void Write_newContactDetails_IntoImportFile() throws IOException
	{
		//Getting Random user name and mailid and Insert to contacts list file
		//Getting Random number using time stamp
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		String TimeFormat = sdf.format(cal.getTime());
		//System.out.println("The present system time is: "+TimeFormat);
		String[] Y= TimeFormat.split(":");
		String OnlyHours=Y[0];
		String OnlyMins=Y[1];
		String SystemTime_OnlyHouSec=OnlyHours+OnlyMins;
		String ContactName="Contact_"+SystemTime_OnlyHouSec;
		Log.message("First Name is: "+ContactName);
		String ContactMailID=ContactName+"@yopmail.com";
		Log.message("Mail Id is: "+ContactMailID);
		
		String Import_Contacts_FilePath = PropertyReader.getProperty("ImpContacts_FilePath");
		FileInputStream input_document = new FileInputStream(new File(Import_Contacts_FilePath));
		 XSSFWorkbook my_xls_workbook = new XSSFWorkbook(input_document);//Access the workbook
         XSSFSheet my_worksheet = my_xls_workbook.getSheetAt(0);//Access the worksheet, so that we can update / modify it.	
         // Access the cell first to update the value
         Cell cell = null; 
         cell = my_worksheet.getRow(1).getCell(0); 
         if (cell == null)
         cell = my_worksheet.getRow(1).createCell((short)0);
         cell.setCellType(CellType.STRING);
         cell.setCellValue(ContactName);
         
         Cell cell1=null;
         cell1 = my_worksheet.getRow(1).getCell(4); 
         if (cell1 == null)
         cell1 = my_worksheet.getRow(1).createCell((short)4);
         cell.setCellType(CellType.STRING);
         cell1.setCellValue(ContactMailID);
         
         input_document.close();
         FileOutputStream output_file =new FileOutputStream(new File(Import_Contacts_FilePath));
         my_xls_workbook.write(output_file);//write changes
         output_file.close();//close the stream
	}
	
	/** 
	 * Method written for Import Contacts using .XLSX and validate
	 * Scipted By: Naresh Babu 
	 */
	public boolean Import_Contact_FromXLSX_Validate(String ImpContact_FoldPath) throws IOException
	{
		boolean result1=false;
		boolean result2=false;
		
		//Reading Values from .XLSX file
		String Import_Contacts_FilePath = PropertyReader.getProperty("ImpContacts_FilePath");
		FileInputStream input_document = new FileInputStream(new File(Import_Contacts_FilePath));
		XSSFWorkbook my_xls_workbook = new XSSFWorkbook(input_document);//Access the workbook
        XSSFSheet my_worksheet = my_xls_workbook.getSheetAt(0);//Access the worksheet, so that we can update / modify it.
        
        Cell cell1=my_worksheet.getRow(1).getCell(0);
        String FirstName=cell1.getStringCellValue();
        Log.message("FirstName Cell value is: "+FirstName);    
        Cell cell2=my_worksheet.getRow(1).getCell(2);
        String Company=cell2.getStringCellValue();
        Log.message("Company Cell value is: "+Company);        
        Cell cell3=my_worksheet.getRow(1).getCell(4);
        String Email=cell3.getStringCellValue();
        Log.message("Email Cell value is: "+Email);			
		Cell cell4=my_worksheet.getRow(1).getCell(10);
        String Phone_Number=cell4.getStringCellValue();
        Log.message("Phone Cell value is: "+Phone_Number);        
        input_document.close();//Close the stream
       
        SkySiteUtils.waitForElement(driver, btnImportContact, 30);
      //Getting contacts count Before Import
      	int Before_Import=0;
      	List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'lbl-Email')]"));
      	for (WebElement Element : allElements)
      	{ 
      		Before_Import = Before_Import+1; 	
      	}
      	Log.message("Available contacts - Before Import is: "+Before_Import);
      		
      	btnImportContact.click();
      	Log.message("Clicked on Import Contact button.");
      	SkySiteUtils.waitForElement(driver, btnNextImportContacts, 30);
      	SkySiteUtils.waitTill(15000);
      	btnChooseFile.click();
      	Log.message("Clicked on Choose File button.");
      	SkySiteUtils.waitTill(10000);
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));

		String expFilename = null;
		File[] files = new File(ImpContact_FoldPath).listFiles();

		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a
				// variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(500);
			}
		}

		output.flush();
		output.close();

		// Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path +" "+ImpContact_FoldPath +" "+ tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(20000);
		try {
			File file = new File(tmpFileName);
			if (file.delete()) {
				Log.message(file.getName() + " is deleted!");
			} else {
				Log.message("Delete operation is failed.");
			}
		} catch (Exception e) {
			Log.message("Exception occured!!!" + e);

		}
		SkySiteUtils.waitForElement(driver, btnNextImportContacts, 30);
		btnNextImportContacts.click();
		Log.message("Clicked on Next button from Import Contacts window.");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnImportImportContacts, 30);
		SkySiteUtils.waitTill(2000);
		btnImportImportContacts.click();
		Log.message("Clicked om Import button from Import Contacts window.");
		SkySiteUtils.waitForElement(driver, btnContactResultClose, 30);
		SkySiteUtils.waitTill(10000);
		String No_Of_Cont_Found=driver.findElement(By.xpath("(//div[@class='text-info'])[2]")).getText();
		String No_Of_SuccessImport=driver.findElement(By.xpath("(//div[@class='text-success'])[2]")).getText();
		if((No_Of_Cont_Found.contentEquals("1"))&&(No_Of_SuccessImport.contentEquals("1")))
		{
			result1=true;
			Log.message("Contacts Count to be import is Proper!!!");
			btnContactResultClose.click();
			Log.message("Clicked on Close buttom of results Window.");
			//Validations _ After import a new contact
			//Getting Contacts Count After Import
			SkySiteUtils.waitTill(5000);
			int After_Import=0;
			List<WebElement> allElements1 = driver.findElements(By.xpath("//*[contains(@class, 'lbl-Email')]"));
			for (WebElement Element : allElements1)
			{ 
				After_Import = After_Import+1; 	
			}
			System.out.println("Available contacts - After Import is: "+After_Import);
			
			for(int i=1;i<=After_Import;i++)
			{
				String ContactName_AftImport = driver.findElement(By.xpath("(//h4[@class='lbl-ContactName'])["+i+"]")).getText();
				System.out.println("Exp Contact Name is: "+ContactName_AftImport);
				String CompanyName_AftImport = driver.findElement(By.xpath("(//h5[@class='lbl-CompanyName'])["+i+"]")).getText();
				System.out.println("Exp Company Name is: "+CompanyName_AftImport);
				String MailId_AftImport = driver.findElement(By.xpath("(//a[@class='lbl-Email'])["+i+"]")).getText();
				System.out.println("Exp Mail Id is: "+MailId_AftImport);
				String PhoneNo_AftImport = driver.findElement(By.xpath("(//h5[@class='lbl-Phone'])["+i+"]")).getText();
				System.out.println("Exp Phone Number is: "+PhoneNo_AftImport);
				
				if((ContactName_AftImport.contains(FirstName)) && (CompanyName_AftImport.contentEquals(Company))
						&& (MailId_AftImport.contentEquals(Email)) && (PhoneNo_AftImport.contentEquals(Phone_Number))
						&& (After_Import==Before_Import+1))
				{
					result2=true;
					break;
				}	
			}
			
			if(result2==true)
			{
				Log.message("New Contacts Imported successfully!!!");
			}
			else
			{
				result2=false;
				Log.message("New Contacts are NOT Imported successfully!!!");
			}
		}
		else
		{
			result1=false;
			Log.message("Contacts Count to be import is NOT Proper!!!");
		}
        if((result1==true) && (result2==true))
        {
        	Log.message("Import a contact and validate is working successfully.");
        	 return true;
        }
        else
        {
        	Log.message("Import a contact and validate is NOT working.");
        	return false;
        }
	}
	
	/** 
	 * Method written for Edit Contact and Validate
	 * Scipted By: Naresh Babu Kavuru
	 * @return
	 */
	public boolean Edit_Contact_AndValidate(String Exp_ContName, String New_ContactName) throws AWTException, InterruptedException, IOException
	{
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnExportToCSV, 30);
		SkySiteUtils.waitTill(5000);
		
	//Getting contacts count 
		int Available_Contacts=0;
		int i =1;
		String Act_ContactName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'lbl-Email')]"));
		for (WebElement Element : allElements)
		{ 
			Available_Contacts = Available_Contacts+1; 	
		}
		Log.message("Available contacts are: "+Available_Contacts);
		for(i=1;i<=Available_Contacts;i++)
		{
			Act_ContactName = driver.findElement(By.xpath("(//h4[@class='lbl-ContactName'])["+i+"]")).getText();
			//Log.message("Exp Contact Name is: "+ExpContactName);
			if(Act_ContactName.contains(Exp_ContName))
			{
				Log.message("Expected Contact is available in the list.");
				break;
			}
		}
		
		driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal icon-lg'])["+i+"]")).click();
		Log.message("Clicked on expected contact more option");
		//Available_Contacts=Available_Contacts-1;
		WebElement EditLink = driver.findElement(By.xpath("(//a[contains(text(),'Edit')])["+i+"]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",EditLink);//JSExecutor to click edit
		Log.message("Clicked on Edit Tab for the contact.");
		SkySiteUtils.waitForElement(driver, btnUpdateContact, 30);//Wait for save button of edit fields window
		SkySiteUtils.waitTill(2000);
		
		//Edit Existed fields of an expected contact
		//Generating all input values randomly
		String Random_Number = Generate_Random_Number.generateRandomValue();
		String Inp_LastName = "LstName_"+Random_Number;
		String Inp_Address = "Address_"+Random_Number;
		String Inp_City = "City_"+Random_Number;
		String Inp_Zip = Random_Number;
		String Inp_state = null;
		String Inp_Country = null;
		String Inp_Phone = Random_Number+Random_Number;
		String Inp_Email = New_ContactName+"@yopmail.com";
		String Inp_Fax = Random_Number+Random_Number;
				
		SkySiteUtils.waitTill(3000);
		txtFirstName.clear();
		txtFirstName.sendKeys(New_ContactName);
		txtLastName.clear();
		txtLastName.sendKeys(Inp_LastName);
		txtAddress.clear();
		txtAddress.sendKeys(Inp_Address);
		txtCity.clear();
		txtCity.sendKeys(Inp_City);
				
		//Generating a random value in between 1 to 200
		int Max = 201;
		int Min = 1;
		//Create Instance of Random Class
		Random ranomNum = new Random();
		int Gen_Random_Val = Min + ranomNum.nextInt(Max);
		Log.message("Genarated Random Value is: "+Gen_Random_Val);
				
		driver.findElement(By.xpath("//div[@id='ddlCountrydrop']")).click();
		SkySiteUtils.waitTill(2000);
		driver.findElement(By.xpath("(//a[@class='Country-Item'])["+Gen_Random_Val+"]")).click();//Select Any Country
		SkySiteUtils.waitTill(2000);
		Inp_Country = txtCountry.getAttribute("value");
		Log.message("Selected Country is: "+Inp_Country);
		SkySiteUtils.waitTill(2000);
		driver.findElement(By.xpath("//div[@id='ddlstatedrop']")).click();
		SkySiteUtils.waitTill(2000);
		driver.findElement(By.xpath("(//a[@class='State-Item'])[1]")).click();//Select Any State
		SkySiteUtils.waitTill(2000);
		Inp_state = txtState.getAttribute("value");
		Log.message("Selected State is: "+Inp_state);
		txtZip.clear();	
		txtZip.sendKeys(Inp_Zip);
		txtPhone.clear();
		txtPhone.sendKeys(Inp_Phone);
		txtEmail.clear();
		txtEmail.sendKeys(Inp_Email);
		txtFax.clear();
		txtFax.sendKeys(Inp_Fax);
				
		btnUpdateContact.click();
		Log.message("Clicked on Update button");
		SkySiteUtils.waitTill(10000);
		
		Log.message("Available contacts After edit is: "+Available_Contacts);
		for(i=1;i<=Available_Contacts;i++)
		{
			Act_ContactName = driver.findElement(By.xpath("(//h4[@class='lbl-ContactName'])["+i+"]")).getText();
			//Log.message("Exp Contact Name is: "+ExpContactName);
			if(Act_ContactName.contains(New_ContactName))
			{
				Log.message("Expected Contact is available in the list.");
				break;
			}
		}
		
		driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal icon-lg'])["+i+"]")).click();
		Log.message("Clicked on expected contact more option");
		SkySiteUtils.waitTill(5000);
		//Available_Contacts=Available_Contacts-1;
		WebElement EditLink1 = driver.findElement(By.xpath("(//a[contains(text(),'Edit')])["+i+"]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",EditLink1);//JSExecutor to click edit
		Log.message("Clicked on Edit Tab for the contact.");
		SkySiteUtils.waitForElement(driver, btnUpdateContact, 30);//Wait for save button of edit fields window
		SkySiteUtils.waitTill(2000);
		
		//Getting all expected values from edit contact window
		String Exp_FirstName = txtFirstName.getAttribute("value");
		Log.message("Exp First Name: "+Exp_FirstName);
		Log.message("New Contact Name: "+New_ContactName);
		String Exp_LastName = txtLastName.getAttribute("value");
		String Exp_Address = txtAddress.getAttribute("value");
		String Exp_City = txtCity.getAttribute("value");
		String Exp_Zip = txtZip.getAttribute("value");
		String Exp_state = txtState.getAttribute("value");
		String Exp_Country = txtCountry.getAttribute("value");
		String Exp_Phone = txtPhone.getAttribute("value");
		String Exp_Email = txtEmail.getAttribute("value");
		String Exp_Fax = txtFax.getAttribute("value");
		
		btnCancelAddContactWind.click();
		Log.message("Clicked on cancel button from Edit Contact window");
	
		int Match_Count = 0;
		if((Exp_FirstName.contains(New_ContactName))&&(Inp_LastName.contentEquals(Exp_LastName))
				&&(Inp_Address.contentEquals(Exp_Address))&&(Inp_Email.contentEquals(Exp_Email))&&((Inp_City).contentEquals(Exp_City))
				&&(Inp_Country.contentEquals(Exp_Country))&&(Inp_state.contentEquals(Exp_state))&&(Inp_Zip.contentEquals(Exp_Zip))
				&&(Inp_Phone.contentEquals(Exp_Phone))&&(Inp_Fax.contentEquals(Exp_Fax)))
		{
				Match_Count = Match_Count+1;
		}
	
	//Final Validation
		if(Match_Count == 1)
		{
			Log.message("Contact Edit is working properly!!!");
			return true;
		}
		else
		{
			Log.message("Contact Edit is Failed!!!");
			return false;
		}		
	}
	
	/** 
	 * Method written for Delete Contact and Validate
	 * Scipted By: Naresh Babu Kavuru
	 * @return
	 */
	public boolean Delete_Contact_AndValidate(String Exp_ContName) throws AWTException, InterruptedException, IOException
	{
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnExportToCSV, 30);
		SkySiteUtils.waitTill(5000);
		
	//Getting contacts count 
		int Available_Contacts=0;
		int i =1;
		String Act_ContactName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'lbl-Email')]"));
		for (WebElement Element : allElements)
		{ 
			Available_Contacts = Available_Contacts+1; 	
		}
		Log.message("Available contacts are: "+Available_Contacts);
		for(i=1;i<=Available_Contacts;i++)
		{
			Act_ContactName = driver.findElement(By.xpath("(//h4[@class='lbl-ContactName'])["+i+"]")).getText();
			//Log.message("Exp Contact Name is: "+ExpContactName);
			if(Act_ContactName.contains(Exp_ContName))
			{
				Log.message("Expected Contact is available in the list.");
				break;
			}
		}
		
		driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal icon-lg'])["+i+"]")).click();
		Log.message("Clicked on expected contact more option");
		WebElement DeleteLink = driver.findElement(By.xpath("(//a[contains(text(),'Delete')])["+i+"]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",DeleteLink);
		Log.message("Clicked on Delete Tab for the contact.");
		SkySiteUtils.waitForElement(driver, btnYesAlertMsg, 20);
		btnYesAlertMsg.click();
		Log.message("Clicked on Yes button want to delete.");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, ContactCreationSuccessMsg, 20);
		String expectedContactDeleteMsg = ContactCreationSuccessMsg.getText();
		Log.message("Notify message after delete a contact is: "+expectedContactDeleteMsg);
		SkySiteUtils.waitTill(5000);
		//Getting contacts count after delete
		int Available_Contacts_AfterDelete=0;
		List<WebElement> allElements1 = driver.findElements(By.xpath("//*[contains(@class, 'lbl-Email')]"));
		for (WebElement Element : allElements1)
		{ 
			Available_Contacts_AfterDelete = Available_Contacts_AfterDelete+1; 	
		}
		Log.message("Available contacts after delete a contact is: "+Available_Contacts_AfterDelete);
	
	//Final Validation
		if((Available_Contacts_AfterDelete == Available_Contacts-1)&&(expectedContactDeleteMsg.equalsIgnoreCase("Contact deleted successfully")))
		{
			Log.message("Contact is Deleted successfully!!!");
			return true;
		}
		else
		{
			Log.message("Contact Delete Failed!!!");
			return false;
		}	
			
	}
	
	/** 
	 * Method written for Add a new Group
	 * Scipted By: Naresh Babu Kavuru
	 * @return
	 */
	
	@FindBy(css="#btnContactGroup")
	WebElement btnContactGroup;
	
	@FindBy(css="#txtGroupName")
	WebElement txtGroupName;
	
	@FindBy(css="#btnCreateGroup")
	WebElement btnCreateGroup;
	
	@FindBy(css="#btnUpdateGroup")
	WebElement btnUpdateGroup;
	
	public boolean Add_NewGroup_AndValidate(String Group_Name) throws AWTException, InterruptedException, IOException
	{
		boolean result = false;
		SkySiteUtils.waitForElement(driver, btnContactGroup, 30);
		btnContactGroup.click();
		Log.message("Clicked on Groups button.");
		SkySiteUtils.waitForElement(driver, btnAddNewGroup, 20);
		//btnAddNewGroup.click();
		//new modified
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", btnAddNewGroup);
		Log.message("Add new group is clicked");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, txtGroupName, 20);
		txtGroupName.click();
		txtGroupName.sendKeys(Group_Name);
		btnCreateGroup.click();
		Log.message("Clicked on Create Group button.");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, ContactCreationSuccessMsg, 20);
		String addGroupMsg = ContactCreationSuccessMsg.getText();
		Log.message("Notify message after Add a Group is: "+addGroupMsg);
		SkySiteUtils.waitTill(5000);
	//Getting contacts count after delete
		int Available_Groups=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//a[@class='pw-grp-delete']/i"));
		for (WebElement Element : allElements)
		{ 
			Available_Groups = Available_Groups+1; 	
		}
		Log.message("Available number of Groups are: "+Available_Groups);
		for(int i=1;i<=Available_Groups;i++)
		{
			String Act_GroupName = driver.findElement(By.xpath("(//section[@class='media-body pull-left']/h4)["+i+"]")).getText();
			if(Act_GroupName.contains(Group_Name))
			{
				result = true;
				Log.message("Expected Group is available in the list.");
				break;
			}
		}
	//Final Validation
		if((result == true)&&(addGroupMsg.equalsIgnoreCase("Group created successfully")))
		{
			Log.message("Group is Created successfully!!!");
			return true;
		}
		else
		{
			Log.message("Group Creationg Failed!!!");
			return false;
		}		
	}
	
	/** 
	 * Method written for Edit a Group
	 * Scipted By: Naresh Babu Kavuru
	 * @return
	 */
	public boolean Edit_Group_AndValidate(String Group_Name, String Edit_GroupName) throws AWTException, InterruptedException, IOException
	{
		boolean result1 = false;
		boolean result2 = false;
		SkySiteUtils.waitForElement(driver, btnContactGroup, 30);
		btnContactGroup.click();
		Log.message("Clicked on Groups button.");
		SkySiteUtils.waitForElement(driver, btnAddNewGroup, 20);
		SkySiteUtils.waitTill(5000);
		//Identifying expected group and Edit
		int Available_Groups=0;
		int i = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//a[@class='pw-grp-delete']/i"));
		for (WebElement Element : allElements)
		{ 
			Available_Groups = Available_Groups+1; 	
		}
		Log.message("Available number of Groups are: "+Available_Groups);
		for(i=1;i<=Available_Groups;i++)
		{
			String Act_GroupName = driver.findElement(By.xpath("(//section[@class='media-body pull-left']/h4)["+i+"]")).getText();
			if(Act_GroupName.contains(Group_Name))
			{
				result1 = true;
				Log.message("Expected Group is available in the list.");
				break;
			}
		}
		if(result1==true)
		{
			Log.message("Expected Group is available, You can edit.");
			driver.findElement(By.xpath("(//a[@class='pw-grp-edit']/i)["+i+"]")).click();
			Log.message("Clicked on edit icon for the group.");
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, txtGroupName, 20);
			txtGroupName.clear();
			txtGroupName.sendKeys(Edit_GroupName);
			btnUpdateGroup.click();
			Log.message("Clicked on Update Group button.");
			SkySiteUtils.waitTill(1000);
			SkySiteUtils.waitForElement(driver, ContactCreationSuccessMsg, 20);
			String Edit_GroupMsg = ContactCreationSuccessMsg.getText();
			Log.message("Notify message after Edit a Group is: "+Edit_GroupMsg);
			SkySiteUtils.waitTill(5000);
			for(i=1;i<=Available_Groups;i++)
			{
				String Act_GroupName = driver.findElement(By.xpath("(//section[@class='media-body pull-left']/h4)["+i+"]")).getText();
				if((Act_GroupName.contains(Edit_GroupName))&&(Edit_GroupMsg.equalsIgnoreCase("Group updated successfully")))
				{
					result2 = true;
					Log.message("Edited Group is available in the list.");
					break;
				}
			}
		}
		
	//Final Validation
		if((result1==true)&&(result2==true))
		{
			Log.message("Group Edit is successfully!!!");
			return true;
		}
		else
		{
			Log.message("Group Edit Failed!!!");
			return false;
		}		
	}
	
	/** 
	 * Method written for Delete a Group
	 * Scipted By: Naresh Babu Kavuru
	 * @return
	 */
	public boolean Delete_Group_AndValidate(String Group_Name) throws AWTException, InterruptedException, IOException
	{
		boolean result1 = false;
		boolean result2 = false;
		SkySiteUtils.waitForElement(driver, btnContactGroup, 30);
		SkySiteUtils.waitTill(2000);
		//Identifying expected group and Delete
		int Available_Groups=0;
		int i = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//a[@class='pw-grp-delete']/i"));
		for (WebElement Element : allElements)
		{ 
			Available_Groups = Available_Groups+1; 	
		}
		Log.message("Available number of Groups are: "+Available_Groups);
		for(i=1;i<=Available_Groups;i++)
		{
			String Act_GroupName = driver.findElement(By.xpath("(//section[@class='media-body pull-left']/h4)["+i+"]")).getText();
			if(Act_GroupName.contains(Group_Name))
			{
				result1 = true;
				Log.message("Expected Group is available in the list.");
				break;
			}
		}
		if(result1==true)
		{
			Log.message("Expected Group is available, You can Delete.");
			driver.findElement(By.xpath("(//a[@class='pw-grp-delete']/i)["+i+"]")).click();
			Log.message("Clicked on Delete icon for the group.");
			SkySiteUtils.waitForElement(driver, btnYesAlertMsg, 20);
			SkySiteUtils.waitTill(2000);
			btnYesAlertMsg.click();
			Log.message("Clicked on YES want to delete Group.");
			SkySiteUtils.waitTill(1000);
			SkySiteUtils.waitForElement(driver, ContactCreationSuccessMsg, 20);
			String Delete_GroupMsg = ContactCreationSuccessMsg.getText();
			Log.message("Notify message after Delete a Group is: "+Delete_GroupMsg);
			SkySiteUtils.waitTill(5000);
			//Getting Group count after delete
			int Available_Groups_AfterDelete=0;
			List<WebElement> allElements1 = driver.findElements(By.xpath("//a[@class='pw-grp-delete']/i"));
			for (WebElement Element : allElements1)
			{ 
				Available_Groups_AfterDelete = Available_Groups_AfterDelete+1; 	
			}
			Log.message("Available Groups after delete is: "+Available_Groups_AfterDelete);
			if((Available_Groups_AfterDelete == Available_Groups-1)&&(Delete_GroupMsg.equalsIgnoreCase("Group deleted successfully")))
			{
				result2 = true;
				Log.message("Group is Deleted successfully!!!");
			}
		}
		
	//Final Validation
		if((result1==true)&&(result2==true))
		{
			Log.message("Group Delete is successfully!!!");
			return true;
		}
		else
		{
			Log.message("Group Delete was Failed!!!");
			return false;
		}		
	}
	
}
