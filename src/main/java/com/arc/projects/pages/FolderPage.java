package com.arc.projects.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.lang.*;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import atu.testrecorder.media.avi.AVIWriter;

import com.arc.projects.utils.AnalyzeLog;
import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arc.projects.utils.getCurrentDate;
import com.arc.projects.utils.randomFileName;
import com.arcautoframe.utils.Log;
import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.arc.projects.pages.ProjectGalleryPage;


public class FolderPage extends LoadableComponent<FolderPage> {

	WebDriver driver;
	private boolean isPageLoaded;

	ProjectDashboardPage projectDashboardPage;
	SubmitalPage submitalPage;
	
	/**
	 * Identifying web elements using FindBy annotation.
	 */

	@FindBy(xpath = "//h4[text()='Gallery']")
	WebElement buttonGallery;
	
	@FindBy(xpath="//*[text()='TP_Folder_01']")
	WebElement buttonTP_Folder_01;
	
	@FindBy(xpath="//*[text()='A_Folder']")
	WebElement buttonA_Folder;
	
	@FindBy(xpath="//*[text()='B_Folder']")
	WebElement buttonB_Folder;
	
	@FindBy(xpath="//*[text()='C_Folder']")
	WebElement buttonC_Folder;
	
	@FindBy(xpath="//*[text()='A1.pdf']")
	WebElement fileA1pdf;
	
	@FindBy(xpath="//*[@id='a_manageOrdinal']")
	WebElement btnManageFileOrder;
	
	@FindBy(xpath=".//*[@id='breadCrumb']/div/ul/li[4]")
	WebElement breadcrumbManageFileOrder;
	
	@FindBy (xpath=".//*[@id='breadCrumb']/div/ul/li[3]/span/a")
	WebElement breadcrumbProjectRootFolderLevel;
	
	@FindBy(xpath= "//*[@id=\"btnViewAllGlobal\"]")
	WebElement btnViewAll;
	
	@FindBy(xpath="//*[@data-docname='A1.pdf']")
	WebElement tabViewerFileA1pdf;
	
	@FindBy(xpath = "//button[text()='Choose files']")
	WebElement buttonChooseFile;
	
	@FindBy(xpath = "//*[@id='DonotIndexFiles']")
	WebElement btnUploadWithoutIndexing;
	
	@FindBy(css = "#liActivityMain > a:nth-child(1)")
	WebElement btnProjectActivity;
	
	@FindBy(css = "#liActivity > a:nth-child(1)")
	WebElement btnDocumentActivity;
	
	@FindBy(xpath="//*[@id='lirfiPunch']/a/i")
	WebElement btnDocumentManagement;
	
	@FindBy(xpath="//*[@id='liSecondTab']")
	WebElement RFIAssigntometab;
	
	
	@FindBy(xpath="//*[@id='liPunch']/a/span")
	WebElement btnPunch;
	
	@FindBy(xpath="//*[@id='a_showRFIList']")
	WebElement btnRFI;
	
	@FindBy(xpath="//*[@id='a_showSUBMITTALList']")
	WebElement btnSubmittal;
	
	@FindBy(css = ".dynamic-down > i:nth-child(1)")
	WebElement btnFileOptions;
	
	@FindBy(css = ".btnDeletedoc")
	WebElement btnFileDelete;
	
	@FindBy(xpath = "//*[@id=\"button-1\"]")
	WebElement btnDeleteFileYes;

	@FindBy(css = "#aPrjAddFolder")
	WebElement btnAddNewFolder;

	@FindBy(css = "#txtSearchKeyword")
	WebElement txtSearchField;

	@FindBy(css = "#btnSearch")
	WebElement btnGlobalSearch;

	@FindBy(css = "#txtFolderName")
	WebElement txtBoxFolderName;

	@FindBy(css = ".chkExcludedrawing")
	WebElement chkBoxExcludedrawing;

	@FindBy(css = ".chkExcludedrawing")
	WebElement chkBoxExcludeLD;

	@FindBy(css = "#btnFolderCreate")
	WebElement btnCreateFolder;

	@FindBy(css = "#btnFolderCreateThisFolder")
	WebElement btnUploadFilesToFolder;

	@FindBy(css = ".noty_text")
	WebElement notificationMsg;

	@FindBy(css = ".sel-gridview")
	WebElement handIconSelect;

	@FindBy(css = "#docsExport")
	WebElement btnExportToCSV;

	@FindBy(css = ".aProjHeaderLink")
	WebElement ParentFold_BreadCrumb;

	@FindBy(css = ".draggablefolder")
	WebElement objLDFolder;

	@FindBy(css = ".panel-body.pw-icon-lg-grey")
	WebElement objNoLatestDocsAvailable;

	// Elements From File Edit window
	@FindBy(css = "#txtDocName")
	WebElement txtDocName;

	@FindBy(css = "#txtRevDate")
	WebElement txtRevisionDate;

	@FindBy(css = "#btnEditDocAttr")
	WebElement btnSaveEditDocWindow;

	@FindBy(css = ".btn.btn-default.btnCancel")
	WebElement btnCancelEditDocWindow;

	// Elements From Edit attributes window

	@FindBy(css = "#txtImageName")
	WebElement txtPhotoName;

	@FindBy(xpath = "//button[@class='btn btn-default btnCancel']")
	WebElement butCancelEditAttriWind;

	// Elements for upload and publish

	@FindBy(xpath="//*[@id='aPrjUpldFile']")
	WebElement btnUploadfile;

	@FindBy(xpath="//*[@id='aPrjUpldFile2']")
	WebElement btnUploadfile2;
	
	@FindBy(xpath = "//i[@class='icon icon-upload-alt icon-lg']")
	WebElement btnUploadfile1;

	@FindBy(css = "#btnKloudless")
	WebElement btnCloudAccount;
	
	@FindBy(css = "#btnSelectFiles")
	WebElement btnChooseFile;

	@FindBy(css = "#DonotIndexFiles")
	WebElement btnUploadWithoutIndex;

	@FindBy(css = "#btnAutoOCR")
	WebElement btnUploadWithIndex;

	@FindBy(linkText="Process completed")
	WebElement linkProcessCompleted;
	
	@FindBy(linkText="Publish now")
	WebElement linkPublishnow;
	
	@FindBy(xpath = "//li[@class='shared-list panel']")
	WebElement Panel;
	
	@FindBy(xpath = "(//span [@data-bind='text: RFINumber()'])[1]")
	WebElement RFINumber_Display;
	
	@FindBy(xpath="//span[@class='icon icon-trash icon-lg']")
	WebElement iconDeletePublishLogPage;
	
	@FindBy(css="#aPrivateProjects")
	WebElement PrivateProjectsTab;
	
	@FindBy(xpath="//button[@id='btnPublishNew']")
	WebElement btnPublishNew;
	
	@FindBy(xpath="//input[@class='form-control customwidth']")
	WebElement txtRevisionInput;
	
	@FindBy(xpath="//input[@id='txtRevisionDate']")
	WebElement infoRevisionDate;
	
	@FindBy(xpath="(//button[@class='btn btn-default dropdown-toggle'])[1]")
	WebElement dropdownIndxType;	
	
	@FindBy(xpath="//a[@data-text='Index by file name' and @data-val='2']")
	WebElement droplistIndxByFileName;
	
	// Objects related to file sendlink URL
	@FindBy(css = "#aZipProgress")
	WebElement btnSingFileDownload;

	@FindBy(css = "#aTotalZipDownload")
	WebElement btnToatalZipDownload;

	@FindBy(xpath = "(//a[@class='preview document-preview-event'])[1]")
	WebElement thumbFirstlocatFile;

	// Objects related to viewer level send link
	@FindBy(css = "#ddlRevisions>a")
	WebElement moreIconFileView;

	@FindBy(css = "#aSend>a")
	WebElement tabSendLinkFileView;

	@FindBy(css = "#txtCopyToClipboard")
	WebElement txtCopyToClipboard;

	@FindBy(css = "#btnAddressBook")
	WebElement btnContact;

	@FindBy(css = "#txtFileSendMessage")
	WebElement txtMessage;

	@FindBy(css = "#btnAddContact")
	WebElement btnSelectContact;

	@FindBy(css = "#btnSendLink")
	WebElement btnSendFileView;

	@FindBy(css = "#aShareLinks")
	WebElement tabSendMultFile;

	@FindBy(css = ".icon.icon-zip.pw-icon-brown.icon-lg.icon-2x")
	WebElement iconZip_AfterSelctFiles;

	@FindBy(css = ".btn.btn-default.cancelMulSel")
	WebElement btnCanMultFileDownSelection;

	@FindBy(xpath = "//button[@id='aTotalZipDownload' and @class='btn btn-primary hidden-xs multiFileDownload']")
	WebElement btnMultFileSelDownload;

	@FindBy(xpath = "//*[@id='aPrjUpldFile']")
	WebElement btnMycomputer;
	
	//Move Folder and Files
	@FindBy(css = "#btnCancelMoveFolder")
	WebElement btnCancelMoveFolder;
	
	@FindBy(css = "#btnCancelMoveFile")
	WebElement btnCancelMoveFile;

	@FindBy(css = "#btnMoveFolder")
	WebElement btnSaveMoveFolder;
	
	@FindBy(css = "#btnMoveFile")
	WebElement btnSaveMoveFile;

	@FindBy(css = "#button-0")
	WebElement btnInfoMsgNo;

	@FindBy(css = "#button-1")
	WebElement btnInfoMsgYes;

	// Elements for multiple file select and download
	@FindBy(css=".sel-gridview>a")
	WebElement btnSelect_Gridview;
	
	@FindBy(css=".selAll-gridview>a")
	WebElement btnSelectAllFiles_Gridview;
	
	@FindBy(xpath="//span[@id='divShowDocSelected']")
	WebElement infoNoOfFilesSelected;
	
	@FindBy(xpath = "//a[@id='switch-view']/i")
	WebElement iconListView;

	@FindBy(css = ".icon.icon-lg.icon-th-large")
	WebElement iconGridView;

	@FindBy(css = "#chkFileFolderAll")
	WebElement chkboxAllFileSel;

	@FindBy(css = "#divShowDocSelected")
	WebElement msgforselectedfiles;

	@FindBy(css = ".dropdown.pw-sel-filefolder>a")
	WebElement btnDropdownMultFileSel;

	@FindBy(css = "#shdFolder")
	WebElement eleSubFoldURL;
	
	@FindBy(xpath = "(//div[1]/h4)[1]")
	WebElement eleFoldNameURL;

	@FindBy(css = "#BackFolderFile")
	WebElement btnBackFold;

	@FindBy(css = ".nav.navbar-nav.navbar-left>li>input")
	WebElement chkboxAllPhoto;

	@FindBy(css = ".dropdown.has-tooltip>a")
	WebElement btnDropdownMultPhotoSel;

	@FindBy(xpath = "(//button[@class='btn btn-primary btn-lg'])[2]")
	WebElement btnUploadPhotos;

	@FindBy(css = "#btnFileUpload")
	WebElement btnFileUpload;

	@FindBy(xpath = "//button[@class='btn btn-primary' and @data-bind='click: SaveAttributes']")
	WebElement btnSaveAttributes;
	
	@FindBy(xpath="//em[@class='pull-left pw-icon-orange']")
	WebElement headerMsgUploadPhotoWindow;	

	@FindBy (xpath="//*[@id='aSortOptionName']")
	WebElement btnFolderSortBy;
	
	@FindBy (xpath="//*[@id='aSortName']")
	WebElement btnSortByFolderName;
	
	@FindBy (xpath="//*[@id='aSortCreateDate']")
	WebElement btnSortByFolderCreateDate;
	
	@FindBy (xpath="//*[@id='aSortRevisionDate']")
	WebElement btnSortByFileRevisionDate;
	
	@FindBy (xpath="//*[@id='aSortDiscipline']")
	WebElement btnSortByFileDiscipline;
	
	@FindBy (xpath="//*[@id='aSortOrdinalNumber']")
	WebElement btnSortByOrdinalNumber;
	
	@FindBy (xpath=".//*[@id='aSortingArrow']")
	WebElement btnFolderSortOrder;
	
	@FindBy (xpath="//*[@id='switch-view']")
	WebElement btnSwitchGridListView;

	@FindBy (xpath="//*[@id='mainPageContainer']/div[1]/ul[1]/li[1]/a")
	WebElement btnProfile;
	
	@FindBy(xpath="//a[contains(text(), 'Sign out')]")
	WebElement btnLogout;
	
	@FindBy(css="#button-1")
	WebElement btnYes;
	
	@FindBy(css="#btnLogin")
	WebElement btnLogin;

	@FindBy(xpath="//*[@id='switch-view']")
	WebElement btnSwitchView;
	
	@FindBy(xpath="//*[text()='Latest documents']")
	WebElement latestDocuments;
	

	@FindBy(xpath="//*[text()='Select']")
	WebElement btnSelect;
	
	@FindBy(xpath="//*[text()='Select all files']")
	WebElement btnSelectAllFiles;
	
	@FindBy(xpath="//*[@id='divHeader']/div[2]/ul[2]/li[6]/a")
	WebElement btnFileMenu;
	
	@FindBy(xpath="//*[@id='Delete']/a")
	WebElement btnDeleteFiles;
	
	@FindBy(xpath="//*[@id='button-1']")
	WebElement btnYesFileOperations;
	
	@FindBy(xpath="//*[@id='breadCrumb']/div[2]/ul/li[3]")
	WebElement btnBreadcrumbProjectRootLevel;

	@FindBy(xpath="//*[@class='chkElements chkfileelements']")
	WebElement checkboxFile;
	
	@FindBy(xpath="//*[@id='divHeader']/div[2]/ul[2]/li[6]/a")
	WebElement dotedMenuFiles;
	
	@FindBy(xpath="//*[@id='aDownload']")
	WebElement btnDownlodFile;
	
	@FindBy(xpath=".//*[@id='hrefCartLink']/a")
	WebElement btnPrintCart;
	
	@FindBy(xpath="//*[@class='noty_text']")
	WebElement validationMessage;
	

	@FindBy(xpath="//*[@id='sel-FolderItem']")
	WebElement addToPrintCart;
	
	@FindBy(xpath= ".//*[@id='Cartcount']")
	WebElement cartItemsCount;
	
	@FindBy(xpath="//*[text()='Back']")
	WebElement btnBackClearSelection;
	
	@FindBy(xpath="//div[@class='image-viewer leaflet-container leaflet-fade-anim']")
	WebElement Viewer_DropPlace;
	
	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 20);

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory
	 * class.
	 * 
	 * @param driver
	 * @return
	 */
	public FolderPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	
	
	/**
	 * Method written for Add a new folder 
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean New_Folder_Create(String Foldername) 
	{
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 60);
		Log.message("Waiting for Create Folder button to be appeared");
		SkySiteUtils.waitTill(3000);
		btnAddNewFolder.click();// Clicking on Create New Folder
		Log.message("Add New folder button clicked!!!");
		SkySiteUtils.waitForElement(driver, btnUploadFilesToFolder, 60);
		SkySiteUtils.waitTill(5000);
		txtBoxFolderName.sendKeys(Foldername);// Giving Folder Names Randomly
		Log.message("Folder Name: " + Foldername + " has been entered in Folder Name text box.");
		btnCreateFolder.click();
		Log.message("Create Folder button clicked!!!");
		SkySiteUtils.waitForElement(driver, notificationMsg, 60);
		String Msg_Folder_Create = notificationMsg.getText();
		Log.message("Notification Message after folder create is: " + Msg_Folder_Create);
		SkySiteUtils.waitTill(5000);
		// Getting Folder count after created a new folder
		int Avl_Fold_Count = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		for (int j = 1; j <= Avl_Fold_Count; j++) {
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4")).getText();
			Log.message("Exp Name:" + Foldername);
			Log.message("Act Name:" + actualFolderName);

			// Validating the new Folder is created or not
			if (Foldername.trim().contentEquals(actualFolderName.trim())) {
				Log.message("Folder is created successfully with name: " + Foldername);
				break;
			}
		}

		// Final Validation - Folder Create
		if ((Foldername.trim().contentEquals(actualFolderName.trim()))
				&& (Msg_Folder_Create.contentEquals("Folder created successfully"))) {
			Log.message("Folder is created successfully with name: " + Foldername);
			return true;
		} else {
			Log.message("Folder creattion FAILED with name: " + Foldername);
			return false;
		}

	}

	/**
	 * Method written for Edit a folder 
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Expected_Folder_Edit(String Foldername, String Edit_FoldName) 
	{
		boolean result1 = false;
		boolean result2 = false;
		//Getting Folder count after created a new folder
		int Avl_Fold_Count = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		int j = 0;
		for (j = 1; j <= Avl_Fold_Count; j++) 
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4")).getText();
			Log.message("Exp Name:" + Foldername);
			Log.message("Act Name:" + actualFolderName);
			// Validating the Expected Folder is available or not
			if (Foldername.trim().contentEquals(actualFolderName.trim())) 
			{
				result1 = true;
				Log.message("Expected Folder is available with name: "+Foldername);
				break;
			}
		}
		if(result1 == true)
		{
			driver.findElement(By.xpath("(//ul[@id='ulDocsFolders']/li/section[4]/ul/li[4]/a/i)["+j+"]")).click();
			Log.message("Clicked on folder drop list.");
			SkySiteUtils.waitTill(3000);
			j=j-1;
			WebElement EditLink = driver.findElement(By.xpath("(//a[contains(text(),'Edit')])["+j+"]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",EditLink);//JSExecutor
			Log.message("Clicked on Edit button from the list.");
			SkySiteUtils.waitForElement(driver, btnCreateFolder, 20);
			SkySiteUtils.waitTill(2000);
			txtBoxFolderName.clear();
			txtBoxFolderName.sendKeys(Edit_FoldName);
			Log.message("Folder Name: " + Edit_FoldName + " has been entered in Folder Name text box.");
			btnCreateFolder.click();
			Log.message("Modify Folder button clicked!!!");
			SkySiteUtils.waitForElement(driver, notificationMsg, 30);
			String Msg_Folder_Modify = notificationMsg.getText();
			Log.message("Notification Message after folder modify is: " + Msg_Folder_Modify);
			SkySiteUtils.waitTill(5000);
			for (int i = 1; i <= Avl_Fold_Count; i++) 
			{
				actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+i+"]/section[2]/h4")).getText();
				Log.message("Exp Name:" + Edit_FoldName);
				Log.message("Act Name:" + actualFolderName);
				// Validating the Modified Folder is available or not
				if ((Edit_FoldName.trim().contentEquals(actualFolderName.trim())) && (Msg_Folder_Modify.contentEquals("Folder updated successfully"))) 
				{
					result2 = true;
					Log.message("Folder is Modified successfully with name: " + Edit_FoldName);
					break;
				}
			}
		}
		if ((result1 == true) && (result2 == true)) 
		{
			Log.message("Folder is Modified successfully with name: " + Edit_FoldName);
			return true;
		} else {
			Log.message("Folder Modification FAILED with name: " + Edit_FoldName);
			return false;
		}

	}
	
	/** 
	 * Method written for Validate Folder OrderPrints
	 * Scripted By: Naresh Babu 
	 * @return
	 */
	@FindBy(css="#order-print")	
	WebElement btnOrderPrintsSubmit;
	public boolean validate_OrderPrints_FolderLevel(String Foldername,int fileCount) throws InterruptedException, AWTException, IOException
	{	
		boolean result1 = false;
		boolean result2 = false;
		//Getting Folder count and identify the folder
		int Avl_Fold_Count = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		int j = 0;
		for (j = 1; j <= Avl_Fold_Count; j++) 
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4")).getText();
			Log.message("Exp Name:" + Foldername);
			Log.message("Act Name:" + actualFolderName);
			// Validating the Expected Folder is available or not
			if (Foldername.trim().contentEquals(actualFolderName.trim())) 
			{
				result1 = true;
				Log.message("Expected Folder is available with name: "+Foldername);
				break;
			}
		}
		if(result1 == true)
		{
			driver.findElement(By.xpath("(//ul[@id='ulDocsFolders']/li/section[4]/ul/li[4]/a/i)["+j+"]")).click();
			Log.message("Clicked on folder drop list.");
			SkySiteUtils.waitTill(3000);
			j = j-1;
			WebElement FolderOrderPrintsLink = driver.findElement(By.xpath("(//a[contains(text(),'Order prints')])["+j+"]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",FolderOrderPrintsLink);//JSExecutor
			Log.message("Clicked on Order Prints button from the list.");
			SkySiteUtils.waitForElement(driver, btnOrderPrintsSubmit, 30);
			
			projectDashboardPage = new ProjectDashboardPage(driver).get();
			result2 = projectDashboardPage.validate_Create_OrderPrints_AndDetails(fileCount);
		}
			
		if((result1==true) && (result2==true))
		{
			Log.message("Order Prints for a Folder is working successfully.");
			return true;
		}
		else
		{
			Log.message("Order Prints for a Folder is NOT working.");
			return false;
		}
		
	}
	
	/** 
	 * Method written for Validate Empty Folder OrderPrints
	 * Scripted By: Naresh Babu 
	 * @return
	 */
	public boolean validate_OrderPrints_EmptyFolder(String Foldername) throws InterruptedException, AWTException, IOException
	{	
		boolean result1 = false;
		boolean result2 = false;
		//Getting Folder count and identify the folder
		int Avl_Fold_Count = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		int j = 0;
		for (j = 1; j <= Avl_Fold_Count; j++) 
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4")).getText();
			Log.message("Exp Name:" + Foldername);
			Log.message("Act Name:" + actualFolderName);
			// Validating the Expected Folder is available or not
			if (Foldername.trim().contentEquals(actualFolderName.trim())) 
			{
				result1 = true;
				Log.message("Expected Folder is available with name: "+Foldername);
				break;
			}
		}
		if(result1 == true)
		{
			driver.findElement(By.xpath("(//ul[@id='ulDocsFolders']/li/section[4]/ul/li[4]/a/i)["+j+"]")).click();
			Log.message("Clicked on folder drop list.");
			SkySiteUtils.waitTill(3000);
			j = j-1;
			WebElement FolderOrderPrintsLink = driver.findElement(By.xpath("(//a[contains(text(),'Order prints')])["+j+"]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",FolderOrderPrintsLink);//JSExecutor
			Log.message("Clicked on Order Prints button from the list.");
			SkySiteUtils.waitTill(1000);
			SkySiteUtils.waitForElement(driver, notificationMsg, 60);
			String Msg_EmptyFolder_Order = notificationMsg.getText();
			Log.message("Notification Message for Empty folder Order is: " + Msg_EmptyFolder_Order);
			SkySiteUtils.waitTill(2000);
			if(Msg_EmptyFolder_Order.contentEquals("No published file exists to order prints."))
			{
				result2=true;
				Log.message("Unable to do order for empty folder.");
			}
			else
			{
				result2=false;
				Log.message("Able to do order for empty folder.");
			}
		}
			
		if((result1==true) && (result2==true))
		{
			Log.message("Order Prints can't be do for an Empty Folder - TRUE.");
			return true;
		}
		else
		{
			Log.message("Able to do Order Prints for an Empty Folder - FALSE.");
			return false;
		}
		
	}

	/** 
	 * Method written for Validate Empty Folder Send Link
	 * Scripted By: Naresh Babu 
	 * @return
	 */
	public boolean validate_SendLink_EmptyFolder(String Foldername) throws InterruptedException, AWTException, IOException
	{	
		boolean result1 = false;
		boolean result2 = false;
		//Getting Folder count and identify the folder
		int Avl_Fold_Count = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		int j = 0;
		for (j = 1; j <= Avl_Fold_Count; j++) 
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4")).getText();
			Log.message("Exp Name:" + Foldername);
			Log.message("Act Name:" + actualFolderName);
			// Validating the Expected Folder is available or not
			if (Foldername.trim().contentEquals(actualFolderName.trim())) 
			{
				result1 = true;
				Log.message("Expected Folder is available with name: "+Foldername);
				break;
			}
		}
		if(result1 == true)
		{
			driver.findElement(By.xpath("(//ul[@id='ulDocsFolders']/li/section[4]/ul/li[4]/a/i)["+j+"]")).click();
			Log.message("Clicked on folder drop list.");
			SkySiteUtils.waitTill(3000);
			WebElement FolderOrderPrintsLink = driver.findElement(By.xpath("(//a[contains(text(),'Send')])["+j+"]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",FolderOrderPrintsLink);//JSExecutor
			Log.message("Clicked on Send button from the list.");
			SkySiteUtils.waitTill(1000);
			SkySiteUtils.waitForElement(driver, notificationMsg, 60);
			String Msg_EmptyFolder_Send = notificationMsg.getText();
			Log.message("Notification Message for Empty folder Send is: " + Msg_EmptyFolder_Send);
			SkySiteUtils.waitTill(2000);
			if(Msg_EmptyFolder_Send.contentEquals("No published file exists to send link."))
			{
				result2=true;
				Log.message("Unable to do Send Link for empty folder.");
			}
			else
			{
				result2=false;
				Log.message("Able to do Send Link for empty folder.");
			}
		}
			
		if((result1==true) && (result2==true))
		{
			Log.message("Send Link can't be do for an Empty Folder - TRUE.");
			return true;
		}
		else
		{
			Log.message("Able to do Send Link for an Empty Folder - FALSE.");
			return false;
		}
		
	}
	
	/** 
	 * Method written for Validate Empty Folder Download
	 * Scripted By: Naresh Babu 
	 * @return
	 */
	public boolean validate_Download_EmptyFolder(String Foldername) throws InterruptedException, AWTException, IOException
	{	
		boolean result1 = false;
		boolean result2 = false;
		//Getting Folder count and identify the folder
		int Avl_Fold_Count = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		int j = 0;
		for (j = 1; j <= Avl_Fold_Count; j++) 
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4")).getText();
			Log.message("Exp Name:" + Foldername);
			Log.message("Act Name:" + actualFolderName);
			// Validating the Expected Folder is available or not
			if (Foldername.trim().contentEquals(actualFolderName.trim())) 
			{
				result1 = true;
				Log.message("Expected Folder is available with name: "+Foldername);
				break;
			}
		}
		if(result1 == true)
		{
			driver.findElement(By.xpath("(//ul[@id='ulDocsFolders']/li/section[4]/ul/li[4]/a/i)["+j+"]")).click();
			Log.message("Clicked on folder drop list.");
			SkySiteUtils.waitTill(3000);
			driver.findElement(By.xpath("(//a[@class='folderDownload'])["+j+"]")).click();
			//WebElement FolderOrderPrintsLink = driver.findElement(By.xpath("(//a[contains(text(),'Send')])["+j+"]"));
			//((JavascriptExecutor) driver).executeScript("arguments[0].click();",FolderOrderPrintsLink);//JSExecutor
			Log.message("Clicked on Download button from the list.");
			SkySiteUtils.waitTill(1000);
			SkySiteUtils.waitForElement(driver, notificationMsg, 60);
			String Msg_EmptyFolder_Download = notificationMsg.getText();
			Log.message("Notification Message for Empty folder Download is: " + Msg_EmptyFolder_Download);
			if(Msg_EmptyFolder_Download.contentEquals("No file or folder exists to download."))
			{
				result2=true;
				Log.message("Unable to do Download for empty folder.");
			}
			else
			{
				result2=false;
				Log.message("Able to do Download for empty folder.");
			}
		}
			
		if((result1==true) && (result2==true))
		{
			Log.message("Download can't be do for an Empty Folder - TRUE.");
			return true;
		}
		else
		{
			Log.message("Able to do Download for an Empty Folder - FALSE.");
			return false;
		}
		
	}
	
	/**
	 * Method written for Delete a folder 
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Expected_Folder_Delete(String Foldername) 
	{
		boolean result1 = false;
		boolean result2 = false;
		//Getting Folder count after created a new folder
		int Avl_Fold_Count = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		int j = 0;
		for (j = 1; j <= Avl_Fold_Count; j++) 
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4")).getText();
			Log.message("Exp Name:" + Foldername);
			Log.message("Act Name:" + actualFolderName);
			// Validating the Expected Folder is available or not
			if (Foldername.trim().contentEquals(actualFolderName.trim())) 
			{
				result1 = true;
				Log.message("Expected Folder is available with name: "+Foldername);
				break;
			}
		}
		if(result1 == true)
		{
			driver.findElement(By.xpath("(//ul[@id='ulDocsFolders']/li/section[4]/ul/li[4]/a/i)["+j+"]")).click();
			Log.message("Clicked on folder drop list.");
			SkySiteUtils.waitTill(3000);
			WebElement FolderDeleteLink = driver.findElement(By.xpath("(//a[contains(text(),'Delete')])["+j+"]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",FolderDeleteLink);//JSExecutor
			Log.message("Clicked on Delete button from the list.");
			SkySiteUtils.waitForElement(driver, btnInfoMsgYes, 20);
			btnInfoMsgYes.click();
			Log.message("Clicked on YES button to delete Folder.");
			SkySiteUtils.waitTill(2000);
			SkySiteUtils.waitForElement(driver, notificationMsg, 30);
			String Msg_Folder_Delete = notificationMsg.getText();
			Log.message("Notification Message after folder Delete is: " + Msg_Folder_Delete);
			SkySiteUtils.waitTill(5000);
			if ((Msg_Folder_Delete.contentEquals("Folder deleted successfully"))) 
			{
				result2 = true;
				Log.message("Folder is Deleted successfully.");
			}
		}
		if ((result1 == true) && (result2 == true)) 
		{
			Log.message("Folder is Deleted successfully with name: " + Foldername);
			return true;
		} else {
			Log.message("Folder Delete FAILED with name: " + Foldername);
			return false;
		}

	}
	
	/**
	 * Method written for Add a new folder exclude LD 
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean New_Folder_Create_ExcludeLD(String Foldername) {
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 60);
		Log.message("Waiting for Create Folder button to be appeared");
		SkySiteUtils.waitTill(3000);
		btnAddNewFolder.click();// Clicking on Create New Folder
		Log.message("Add New folder button clicked!!!");
		SkySiteUtils.waitForElement(driver, btnUploadFilesToFolder, 60);
		SkySiteUtils.waitTill(5000);
		txtBoxFolderName.sendKeys(Foldername);// Giving Folder Names Randomly
		Log.message("Folder Name: " + Foldername + " has been entered in Folder Name text box.");
		SkySiteUtils.waitTill(3000);
		chkBoxExcludedrawing.click();
		Log.message("Clicked on checkbox of exclude LD!!!");
		SkySiteUtils.waitTill(2000);
		btnCreateFolder.click();
		Log.message("Create Folder button clicked!!!");
		SkySiteUtils.waitForElement(driver, notificationMsg, 60);
		String Msg_Folder_Create = notificationMsg.getText();
		Log.message("Notification Message after folder create is: " + Msg_Folder_Create);
		SkySiteUtils.waitTill(5000);
		// Getting Folder count after created a new folder
		int Avl_Fold_Count = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		for (int j = 1; j <= Avl_Fold_Count; j++) {
			actualFolderName = driver
					.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4"))
					.getText();
			Log.message("Exp Name:" + Foldername);
			Log.message("Act Name:" + actualFolderName);

			// Validating the new Folder is created or not
			if (Foldername.trim().contentEquals(actualFolderName.trim())) {
				Log.message("Folder is created successfully with name: " + Foldername);
				break;
			}
		}

		// Final Validation - Folder Create
		if ((Foldername.trim().contentEquals(actualFolderName.trim()))
				&& (Msg_Folder_Create.contentEquals("Folder created successfully"))) {
			Log.message("Folder is created successfully with name: " + Foldername);
			return true;
		} else {
			Log.message("Folder creattion FAILED with name: " + Foldername);
			return false;
		}

	}

	/**
	 * Method written for Select a folder
	 * Scripted By: Naresh Babu 
	 */
	public void Select_Folder(String Foldername) {
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 120);
		Log.message("Waiting for Create Folder button to be appeared");
		// Getting Folder count after created a new folder
		int Avl_Fold_Count = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		for (int j = 1; j <= Avl_Fold_Count; j++) 
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]/section[2]/h4")).getText();
			//Log.message("Exp Name:" + Foldername);
			//Log.message("Act Name:" + actualFolderName);

			// Validating the new Folder is created or not
			if (Foldername.trim().contentEquals(actualFolderName.trim())) 
			{
				driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]")).click();
				Log.message("Expected Folder is clicked successfully with name: " + Foldername);
				break;
			}
		}

	}
	
	/**
	 * Method written for Select a folder _ Recurring upload
	 * Scripted By: Naresh Babu 
	 */
	public boolean Select_Folder_RecurringUpload(String Foldername) 
	{
		boolean result1 = false;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 120);
		Log.message("Waiting for Create Folder button to be appeared");
		// Getting Folder count after created a new folder
		int Avl_Fold_Count = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		for (int j = 1; j <= Avl_Fold_Count; j++) 
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]/section[2]/h4")).getText();
			Log.message("Exp Name:" + Foldername);
			Log.message("Act Name:" + actualFolderName);

			// Validating the new Folder is created or not
			if (Foldername.trim().contentEquals(actualFolderName.trim())) 
			{
				driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]")).click();
				Log.message("Expected Folder is clicked successfully with name: " + Foldername);
				result1=true;
				break;
			}
		}
		
		if(result1==true)
		return true;
		else
			return false;

	}
	
	
	/**
	 * Method written for Select a folder Scripted By: Ranjan
	 */
	public void SelectFolder(String Foldername) {
		//SkySiteUtils.waitForElement(driver, btnAddNewFolder, 120);
		Log.message("Waiting for Create Folder button to be appeared");
		// Getting Folder count after created a new folder
		int Avl_Fold_Count = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		for (int j = 1; j <= Avl_Fold_Count; j++) {
			actualFolderName = driver
					.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4"))
					.getText();
			Log.message("Exp Name:" + Foldername);
			Log.message("Act Name:" + actualFolderName);

			// Validating the new Folder is created or not
			if (Foldername.trim().contentEquals(actualFolderName.trim())) {
				driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4"))
						.click();// Select a folder
				Log.message("Expected Folder is clicked successfully with name: " + Foldername);
				break;
			}
		}

	}


	/**
	 * Method written for Select Gallery folder 
	 * Scripted By: Naresh Babu
	 */
	public void Select_Gallery_Folder() 
	{
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
		// Getting Folder count after created a new folder
		int Avl_Fold_Count = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) 
		{
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		for (int j = 1; j <= Avl_Fold_Count; j++) 
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]/section[2]/h4")).getText();
			Log.message("Act Name:" + actualFolderName);

			// Validating the new Folder is created or not
			if (actualFolderName.trim().contentEquals("Gallery")) 
			{
				driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]")).click();
				SkySiteUtils.waitTill(5000);
				Log.message("Clicked on Gallery Folder");
				break;
			}
		}

	}

	/** 
	 * Method for entering Latest Documents
	 * Scripted By: Ranadeep
	 * @return 
	 */
	
	public ProjectLatestDocumentsPage enterLatestDocuments() 
	{
		
		SkySiteUtils.waitForElement(driver, latestDocuments, 30); 
		Log.message("Latest Documents is displayed");
		
	    WebElement element = driver.findElement(By.xpath("//*[text()='Latest documents']"));
	    JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);    	
		Log.message("Clicked to enter Latest documents"); 
		SkySiteUtils.waitTill(5000);
		
		return new ProjectLatestDocumentsPage(driver).get();
		
	}
	
	/** 
	 * Method for entering Gallery
	 * Scripted By: Ranadeep
	 * @return 
	 */
	
	public ProjectGalleryPage enterGallery()
	{
		
		//SkySiteUtils.waitForElement(driver, buttonGallery, 30);
		 SkySiteUtils.waitTill(3000);  
		buttonGallery.click();
		Log.message("Clicked on Gallery");      
		
		
		return new ProjectGalleryPage(driver).get();
		
	}
	
	/** 
	 * Method for entering Folder TP_Folder_01
	 * Scripted By: Ranadeep
	 * @return 
	 */
	
	public void enterFolder() 
	{
		
		SkySiteUtils.waitForElement(driver, buttonTP_Folder_01, 30); 
		Log.message("Folder TP_Folder_01 is displayed");
		
	    WebElement element = driver.findElement(By.xpath("//*[text()='TP_Folder_01']"));
	    JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);    	
		Log.message("Clicked to enter folder TP_Folder_01"); 
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 20);
		Log.message("Add new folder button is displayed");
		if (btnAddNewFolder.isDisplayed()) 
		{
			
			Log.message("Entered folder TP_Folder_01");		
		} 
		
		else
		{
			Log.message("Folder entry failed");
		}
		
	}
	
	
	/** 
	 * Method for entering Folder A_Folder
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public void enterFolder1() 
	{
		
		SkySiteUtils.waitForElement(driver, buttonA_Folder, 30); 
		Log.message("Folder A_folder is displayed");
		
	    WebElement element = driver.findElement(By.xpath("//*[text()='A_Folder']"));
	    JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);  
		Log.message("Clicked to enter folder A_Folder"); 
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 20);
		Log.message("Add new folder button is displayed");
		
		if (btnAddNewFolder.isDisplayed()) 
		{
			
			Log.message("Entered folder A_Folder");		
		} 
		
		else
		{
			Log.message("Folder entry failed");
		}
		
	}


	/** 
	 * Method for entering Folder B_Folder
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public void enterFolder2() 
	{
		
		SkySiteUtils.waitForElement(driver, buttonB_Folder, 30); 
		Log.message("Folder B_folder is displayed");
		
	    WebElement element = driver.findElement(By.xpath("//*[text()='B_Folder']"));
	    JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);  
		Log.message("Clicked to enter folder B_Folder"); 
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 20);
		Log.message("Add new folder button is displayed");
		
		if (btnAddNewFolder.isDisplayed()) 
		{
			
			Log.message("Entered folder B_Folder");		
		} 
		
		else
		{
			Log.message("Folder entry failed");
		}
		
	}
	
	/** 
	 * Method for entering Folder C_Folder
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public void enterFolder3() 
	{
		
		SkySiteUtils.waitForElement(driver, buttonC_Folder, 30); 
		Log.message("Folder C_folder is displayed");
		
	    WebElement element = driver.findElement(By.xpath("//*[text()='C_Folder']"));
	    JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);  
		Log.message("Clicked to enter folder C_Folder"); 
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 20);
		Log.message("Add new folder button is displayed");
		
		if (btnAddNewFolder.isDisplayed()) 
		{
			
			Log.message("Entered folder C_Folder");		
		} 
		
		else
		{
			Log.message("Folder entry failed");
		}
		
	}
	
	
	/** 
	 * Method for entering Manage File Order screen
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public void enterManageFileOrderScreen() 
	{
		
		SkySiteUtils.waitForElement(driver, btnManageFileOrder, 30);
		Log.message("Manage File Order button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='a_manageOrdinal']"));
	    JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Manage file order button");
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, breadcrumbManageFileOrder, 30);
		String actualManageFileOrderBreadcrumb = breadcrumbManageFileOrder.getText();
		Log.message("Actual Manage File Order breadcrumb text:- " +actualManageFileOrderBreadcrumb);
		
		String expectedManageFileOrderBreadcrumb= PropertyReader.getProperty("MANAGEFILEORDER");
		Log.message("Actual Manage File Order breadcrumb text:- " +expectedManageFileOrderBreadcrumb);
		
		if(actualManageFileOrderBreadcrumb.contains(expectedManageFileOrderBreadcrumb))
		{
			Log.message("Landing in Manage File Order screen is successfull");
		}
		else
		{
			Log.message("Landing in Manage File Order screen is failed!!!");
		}
		
	}
	
	
	/** 
	 * Method for opening a file
	 * Scripted By: Ranadeep
	 * @return 
	 */	
	
	 public void openFile()
	 {
		 SkySiteUtils.waitTill(6000);
		 String actualFilename=PropertyReader.getProperty("Filename");
		 Log.message("Actual filename is:- " +actualFilename);
		 
		 String expectedFileName=fileA1pdf.getText();
		 Log.message("Expected File Name:- " +expectedFileName);		 
		 
		 
		 if (expectedFileName.equals(actualFilename))
		 {
			 fileA1pdf.click();
			 Log.message("Clicked to open file " +actualFilename);
			 SkySiteUtils.waitTill(3000);
			 
			 String MainWindow=driver.getWindowHandle();
			 Log.message(MainWindow);
			 for(String childwindow:driver.getWindowHandles())
			 {
				//swtich to child window	
				 driver.switchTo().window(childwindow);					 
			 }
			 
			 SkySiteUtils.waitForElement(driver, tabViewerFileA1pdf, 30);
			 Log.message("Document viewer tab element found");
			 String viewerTabFileName=tabViewerFileA1pdf.getText();			 		
			 
			 if (viewerTabFileName.contains(actualFilename))
			 {
				 Log.message("File opened in viewer is " +viewerTabFileName);
				 driver.switchTo().window(MainWindow);
			 }
			 else 
			 {
				 Log.message("Wrong file opened!!");
				 
			 }
			 
		 } 
		 
		 else
		 {
			 Log.message("File name mismath!!");
		 }			
			
	 }
	
	
	
	/** 
	 * Method for deleting file
	 * Scripted By: Ranadeep
	 * @return 
	 */	
	
	public boolean deleteFile() {
		
		SkySiteUtils.waitTill(6000);
		btnFileOptions.click();
		Log.message("Clicked on File options button");
		SkySiteUtils.waitTill(3000);
		btnFileDelete.click();
		Log.message("Clicked on File Delete option");
		SkySiteUtils.waitTill(3000);
		btnDeleteFileYes.click();
		Log.message("Clicked on YES option in the Delete File popover");
		SkySiteUtils.waitTill(5000);
		
		if (btnUploadfile.isDisplayed()) {
			Log.message("File is deleted");
			return true;
		} 
	 
	 else{
			Log.message("File deletion failed");
			return false;
		}
		
	}
		
	
		
		
	/** 
	 * Method for Uploading file
	 * Scripted By: Ranadeep
	 * @return 
	 */	
	public PublishLogPage fileUpload(String filepath, String tempfilepath) throws AWTException, IOException
	{
				
		SkySiteUtils.waitForElement(driver, btnUploadfile, 40);
		WebElement element = driver.findElement(By.xpath("//*[@id='aPrjUpldFile']"));
	    JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element); 
		Log.message("Clicked on Upload File button");	
		
		SkySiteUtils.waitForElement(driver, buttonChooseFile, 30);	
		SkySiteUtils.waitTill(5000);
		buttonChooseFile.click();
		Log.message("Clicked on Choose File button");
		
		
		//Writing File names into a text file for using in AutoIT Script		                  
        BufferedWriter output;                  
        randomFileName rn=new randomFileName();
        String tmpFileName=tempfilepath+rn.nextFileName()+".txt";                
        output = new BufferedWriter(new FileWriter(tmpFileName,true));
        
        String expFilename=null;
        File[] files = new File(filepath).listFiles();
        
        for(File file : files)
        {
              if(file.isFile()) 
              {
                  expFilename=file.getName();//Getting File Names into a variable
                  Log.message("Expected File name is:"+expFilename);  
                  output.append('"' + expFilename + '"');
                  output.append(" ");
                  SkySiteUtils.waitTill(5000);     
  
              }      
        }      
        
        output.flush();
        output.close();
        
        String AutoItexe_Path = PropertyReader.getProperty("AutoITPath");
        //Executing .exe autoIt file            
        Runtime.getRuntime().exec(AutoItexe_Path+" "+ filepath+" "+tmpFileName);                  

        Log.message("AutoIT Script Executed!!");                           
        SkySiteUtils.waitTill(20000);
                     
        try
        {
              File file = new File(tmpFileName);
              if(file.delete())
              {
                     Log.message(file.getName() + " is deleted!");
              }      
              else
              {
                     Log.message("Delete operation is failed.");
              }
        }
        catch(Exception e)
        {                    
              Log.message("Exception occured!!!"+e);
        } 
        
        SkySiteUtils.waitTill(8000);
        SkySiteUtils.waitForElement(driver, btnUploadWithoutIndexing, 30);
        SkySiteUtils.waitTill(5000);
        btnUploadWithoutIndexing.click();
        Log.message("Clicked on Upload Without Indexing button"); 
		SkySiteUtils.waitTill(5000);
		
		return new PublishLogPage(driver).get();
        
	}
	
	
	
	/** 
	 * Method for navigating to Document Activity page
	 * Scripted By: Ranadeep
	 * @return 
	 */	
	public ProjectActivitiesPage documentActivity() 
	{
		
		 SkySiteUtils.waitTill(5000);		 
		 btnProjectActivity.click();
		 Log.message("Clicked on Project Activity button");
		 SkySiteUtils.waitTill(3000);
		 btnDocumentActivity.click();
		 Log.message("Clicked on Document Activity button");      
		
		 return new ProjectActivitiesPage(driver).get();
	}
	
	
	/** 
	 * Method for validating descending order sorting by Folder Name
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean descendingOrderSortingByFolderName() 
	{
		
		SkySiteUtils.waitForElement(driver, btnFolderSortBy, 30);
		Log.message("Folder Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Folder Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByFolderName, 30);
		Log.message("Sort by Folder Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='aSortName']"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Sort by Folder Name button");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);
		String sortOrder=btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("Folder sort order is:-" +sortOrder);
	    
	    ArrayList<String> obtainedFolderList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@data-original-title='Project file/folder name']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFolderList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Project count before sorting:- " +obtainedFolderList.size());		 
		Log.message("Obtained Project numbers are:- " +obtainedFolderList);
		
		String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder);
		
		if(sortOrder.contains(expectedSortOrder))
		{
			btnFolderSortOrder.click();
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not ascending");
		}
		SkySiteUtils.waitTill(8000);
		
		ArrayList<String> sortedFolderList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@data-original-title='Project file/folder name']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				sortedFolderList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Project count before sorting:- " +sortedFolderList.size());		 
		Log.message("Obtained Project numbers are:- " +sortedFolderList);
		
		if(obtainedFolderList.get(0).contains(sortedFolderList.get(sortedFolderList.size()-1)) && obtainedFolderList.get(obtainedFolderList.size()-1).contains(sortedFolderList.get(0)))
	    {	
			return true;
			
	    }
	    else
	    {
	    	return false;
	    	
	    }
	
	}
	
	
	/** 
	 * Method for restoring default sort settings for Folder Level
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public void resetToDefaultFolderSortSettings()  	
	{
		
		SkySiteUtils.waitForElement(driver, btnFolderSortBy, 30);
		Log.message("Folder Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Folder Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByFolderName, 30);
		Log.message("Sort by Folder Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='aSortName']"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Sort by Folder Name button");
		SkySiteUtils.waitTill(8000);
		
		
		String sortOrder=btnFolderSortOrder.getAttribute("data-original-title");
		String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
		
		if(sortOrder.contains(expectedSortOrder))
		{
			btnFolderSortOrder.click();
			Log.message("Clicked on order by button");
			SkySiteUtils.waitTill(5000);
			
		}
			
		Log.message("Default sorting settings restored");
		
	}

	
	
	/** 
	 * Method for validating ascending order sorting by Folder Name
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean ascendingOrderSortingByFolderName() 
	{
		
		SkySiteUtils.waitForElement(driver, btnFolderSortBy, 30);
		Log.message("Folder Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Folder Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByFolderName, 30);
		Log.message("Sort by Folder Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='aSortName']"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Sort by Folder Name button");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);
		String sortOrder=btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("Folder sort order is:-" +sortOrder);
		
	    String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder);
		if(sortOrder.contains(expectedSortOrder))
		{
			btnFolderSortOrder.click();
			SkySiteUtils.waitTill(5000);
			
		}
		
		ArrayList<String> obtainedFolderList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@data-original-title='Project file/folder name']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFolderList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Project count before sorting:- " +obtainedFolderList.size());		 
		Log.message("Obtained Project numbers are:- " +obtainedFolderList);
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);
		String sortOrder1=btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("Present Folder sort order is:-" +sortOrder1);
		
		String expectedSortOrder1= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder1);
		
		if(sortOrder1.contains(expectedSortOrder1))
		{
			btnFolderSortOrder.click();
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not descending");
		}
		SkySiteUtils.waitTill(8000);
		
		ArrayList<String> sortedFolderList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@data-original-title='Project file/folder name']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				sortedFolderList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Project count before sorting:- " +sortedFolderList.size());		 
		Log.message("Obtained Project numbers are:- " +sortedFolderList);
		
		if(obtainedFolderList.get(0).contains(sortedFolderList.get(sortedFolderList.size()-1)) && obtainedFolderList.get(obtainedFolderList.size()-1).contains(sortedFolderList.get(0)))
	    {	
			return true;
			
	    }
	    else
	    {
	    	return false;
	    	
	    }
		
	}


	/** 
	 * Method to change sorting order and save data to be validated later in folder level
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public ArrayList<String> sortOrderSave() 
	{

		SkySiteUtils.waitForElement(driver, btnFolderSortBy, 30);
		Log.message("Folder Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Folder Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByFolderName, 30);
		Log.message("Sort by Folder Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='aSortName']"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Sort by Folder Name button");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);
		WebElement element2 = driver.findElement(By.xpath("//*[@id='aSortingArrow']"));
		JavascriptExecutor executor2 = (JavascriptExecutor)driver;
		executor2.executeScript("arguments[0].click();", element2);
		Log.message("Clicked on sort by button");
		SkySiteUtils.waitTill(8000);
		
		String sortOrder=btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("Folder sort order is:-" +sortOrder);
	    
	    ArrayList<String> obtainedFolderList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@data-original-title='Project file/folder name']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFolderList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Project count before sorting:- " +obtainedFolderList.size());		 
		Log.message("Obtained Project numbers are:- " +obtainedFolderList);
		
		return obtainedFolderList;
	
	}
	
	/** 
	 * Method written for Logout from Projects folder level
	 * Scripted By: Ranadeep
	 * @return
	 */
	public boolean Logout_Projects()
	{
		SkySiteUtils.waitForElement(driver, btnProfile, 20);
		btnProfile.click();
		Log.message("Clicked on Profile.");
		SkySiteUtils.waitForElement(driver, btnLogout, 20);
		SkySiteUtils.waitTill(3000);
		btnLogout.click();
		SkySiteUtils.waitForElement(driver, btnYes, 20);
		btnYes.click();
		SkySiteUtils.waitForElement(driver, btnLogin, 30);
		
		Log.message("Checking whether Login button is present?");
		if(btnLogin.isDisplayed())
			return true;
		else
			return true;
		
	}
	
	
	/** 
	 * Method to verify sorting order saved earlier after Log In in Folder level
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean sortOrderSaveAfterLogIn(ArrayList<String> arr1) 
	{
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);	
		String sortBy = btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("Actual Folder sort by order is:-" +sortBy);
	    
	    String expectedSortBy= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
		Log.message("Expected Folder sort by order is:- " +expectedSortBy);
	    
		SkySiteUtils.waitForElement(driver, btnFolderSortBy, 30);
		String sortOrder = btnFolderSortBy.getText();
	    Log.message("Actual Folder sort order is:-" +sortOrder);
	    
	    String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE3");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder);
	    
		ArrayList<String> obtainedList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@data-original-title='Project file/folder name']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
		       obtainedList.add(we.getText()); 
		     
			}
		}
		
		Log.message("Obtained Project count after login:- " +obtainedList.size());		 
		Log.message("Obtained Project list order is:- " +obtainedList);
		
		if(sortBy.contains(expectedSortBy) && sortOrder.contains(expectedSortOrder) && arr1.equals(obtainedList))
		{
			return true;
			
		}
		else 
		{
			return false;
			
		}
		
	}

	
	
	/** 
	 * Method for validating descending order sorting by Folder Create Date
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean descendingOrderSortingByFolderCreateDate() 
	{
		
		SkySiteUtils.waitForElement(driver, btnFolderSortBy, 30);
		Log.message("Folder Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Folder Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByFolderCreateDate, 30);
		Log.message("Sort by Folder Create Date button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='aSortCreateDate']"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Sort by Folder Create Date button");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);
		String sortOrder=btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("Folder sort order is:-" +sortOrder);
	    
	    ArrayList<String> obtainedFolderList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@data-original-title='Project file/folder name']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFolderList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Project count before sorting:- " +obtainedFolderList.size());		 
		Log.message("Obtained Project numbers are:- " +obtainedFolderList);
		
		String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder);
		
		if(sortOrder.contains(expectedSortOrder))
		{
			btnFolderSortOrder.click();
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not ascending");
		}
		SkySiteUtils.waitTill(8000);
		
		ArrayList<String> sortedFolderList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@data-original-title='Project file/folder name']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				sortedFolderList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Project count before sorting:- " +sortedFolderList.size());		 
		Log.message("Obtained Project numbers are:- " +sortedFolderList);
		
		if(obtainedFolderList.get(0).contains(sortedFolderList.get(sortedFolderList.size()-1)) && obtainedFolderList.get(obtainedFolderList.size()-1).contains(sortedFolderList.get(0)))
	    {	
			return true;
			
	    }
	    else
	    {
	    	return false;
	    	
	    }
		
	}
	
	
	/** 
	 * Method for validating ascending order sorting by Folder Create Date
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean ascendingOrderSortingByFolderCreateDate() 
	{
	
		SkySiteUtils.waitForElement(driver, btnFolderSortBy, 30);
		Log.message("Folder Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Folder Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByFolderCreateDate, 30);
		Log.message("Sort by Folder Create Date button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='aSortCreateDate']"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Sort by Folder Create Date button");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);
		String sortOrder=btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("Folder sort order is:-" +sortOrder);
		
	    String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder);
		if(sortOrder.contains(expectedSortOrder))
		{
		
			WebElement element2 = driver.findElement(By.xpath("//*[@id='aSortingArrow']"));
			JavascriptExecutor executor2 = (JavascriptExecutor)driver;
			executor2.executeScript("arguments[0].click();", element2);
			
			SkySiteUtils.waitTill(5000);
			
		}
		
		ArrayList<String> obtainedFolderList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@data-original-title='Project file/folder name']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFolderList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Project count before sorting:- " +obtainedFolderList.size());		 
		Log.message("Obtained Project numbers are:- " +obtainedFolderList);
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);
		String sortOrder1=btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("Present Folder sort order is:-" +sortOrder1);
		
		String expectedSortOrder1= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder1);
		
		if(sortOrder1.contains(expectedSortOrder1))
		{
			btnFolderSortOrder.click();
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not descending");
		}
		SkySiteUtils.waitTill(8000);
		
		ArrayList<String> sortedFolderList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@data-original-title='Project file/folder name']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				sortedFolderList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Project count before sorting:- " +sortedFolderList.size());		 
		Log.message("Obtained Project numbers are:- " +sortedFolderList);
		
		if(obtainedFolderList.get(0).contains(sortedFolderList.get(sortedFolderList.size()-1)) && obtainedFolderList.get(obtainedFolderList.size()-1).contains(sortedFolderList.get(0)))
	    {	
			return true;
			
	    }
	    else
	    {
	    	return false;
	    	
	    }
	
	}
	
	
	/** 
	 * Method for validating descending order sorting by File Name
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean descendingOrderSortingByFileName() 
	{
	
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Add new folder button is displayed");
			
		SkySiteUtils.waitForElement(driver, btnSwitchView, 30);
		Log.message("Switch view button is displayed");
		
		SkySiteUtils.waitForElement(driver, btnFolderSortBy, 30);
		Log.message("File Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on File Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByFolderName, 30);
		Log.message("Sort by File Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='aSortName']"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Sort by File Name");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);
		String sortOrder=btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("File sort order is:-" +sortOrder);
	    
	    ArrayList<String> obtainedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h4_docname document-preview-event']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFileList.add(we.getText()); 
		     
			}
		}
		
		Log.message("Obtained files count before sorting:- " +obtainedFileList.size());		 
		Log.message("Obtained Files are:- " +obtainedFileList);
		
		String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder);
		
		if(sortOrder.contains(expectedSortOrder))
		{
			btnFolderSortOrder.click();
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not ascending");
		}
		SkySiteUtils.waitTill(8000);
	    
		ArrayList<String> sortedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@class='h4_docname document-preview-event']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				sortedFileList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Files count after sorting:- " +sortedFileList.size());		 
		Log.message("Obtained Files are:- " +sortedFileList);
		
		if(obtainedFileList.get(0).contains(sortedFileList.get(sortedFileList.size()-1)) && obtainedFileList.get(obtainedFileList.size()-1).contains(sortedFileList.get(0)))
	    {	
			return true;
			
	    }
	    else
	    {
	    	return false;
	    	
	    }
	}
	
	
	/** 
	 * Method for validating ascending order sorting by File Name
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean ascendingOrderSortingByFileName() 
	{
		
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Add new folder button is displayed");
			
		SkySiteUtils.waitForElement(driver, btnSwitchView, 30);
		Log.message("Switch view button is displayed");
		
		SkySiteUtils.waitForElement(driver, btnFolderSortBy, 30);
		Log.message("File Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on File Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByFolderName, 30);
		Log.message("Sort by File Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='aSortName']"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Sort by File Name");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);
		String sortOrder=btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("Folder sort order is:-" +sortOrder);
		
	    String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder);
		if(sortOrder.contains(expectedSortOrder))
		{
		
			WebElement element2 = driver.findElement(By.xpath("//*[@id='aSortingArrow']"));
			JavascriptExecutor executor2 = (JavascriptExecutor)driver;
			executor2.executeScript("arguments[0].click();", element2);			
			SkySiteUtils.waitTill(5000);
			
		}
		
		ArrayList<String> obtainedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h4_docname document-preview-event']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFileList.add(we.getText()); 
		     
			}
		}
		
		Log.message("Obtained files count before sorting:- " +obtainedFileList.size());		 
		Log.message("Obtained Files are:- " +obtainedFileList);
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);
		String sortOrder1=btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("Present Folder sort order is:-" +sortOrder1);
		
		String expectedSortOrder1= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder1);
		
		if(sortOrder1.contains(expectedSortOrder1))
		{
			btnFolderSortOrder.click();
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not descending");
		}
		SkySiteUtils.waitTill(8000);
			
		ArrayList<String> sortedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@class='h4_docname document-preview-event']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				sortedFileList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Files count after sorting:- " +sortedFileList.size());		 
		Log.message("Obtained Files are:- " +sortedFileList);
		
		if(obtainedFileList.get(0).contains(sortedFileList.get(sortedFileList.size()-1)) && obtainedFileList.get(obtainedFileList.size()-1).contains(sortedFileList.get(0)))
	    {	
			return true;
			
	    }
	    else
	    {
	    	return false;
	    	
	    }
		
	}
	
	
	
	/** 
	 * Method for restoring default sort settings for File Level
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public void resetToDefaultFileSortSettings() 
	{
		
		SkySiteUtils.waitForElement(driver, btnFolderSortBy, 30);
		Log.message("File Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on File Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByFolderName, 30);
		Log.message("Sort by File Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='aSortName']"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Sort by File Name");
		SkySiteUtils.waitTill(8000);
		
		
		String sortOrder=btnFolderSortOrder.getAttribute("data-original-title");
		String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
		
		if(sortOrder.contains(expectedSortOrder))
		{
			
			WebElement element2 = driver.findElement(By.xpath("//*[@id='aSortingArrow']"));
			JavascriptExecutor executor2 = (JavascriptExecutor)driver;
			executor2.executeScript("arguments[0].click();", element2);
			//btnFolderSortOrder.click();
			Log.message("Clicked on order by button");
			SkySiteUtils.waitTill(8000);
			
		}
			
		Log.message("Default sorting settings restored");
		
	}
	
	
	/** 
	 * Method for changing folder view from grid to list
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public void changeFolderViewGridToList() 
	{
		
		SkySiteUtils.waitForElement(driver, btnSwitchGridListView, 30);
		Log.message("Switch file/folder view button is displayed");
		
		SkySiteUtils.waitTill(8000);
		String actualFolderView = btnSwitchGridListView.getAttribute("data-type");	
		String expectedFolderView = PropertyReader.getProperty("GRIDTOLISTVIEW");
		
		if(actualFolderView.equals(expectedFolderView))
		{

			WebElement element = driver.findElement(By.xpath("//*[@id='switch-view']"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
			Log.message("Clicked on switch file/folder view button");
			SkySiteUtils.waitTill(8000);
			
		}
		else
		{
			btnSwitchGridListView.click();
			SkySiteUtils.waitTill(8000);
			btnSwitchGridListView.click();
			Log.message("Clicked on switch file/folder view button");
			SkySiteUtils.waitTill(8000);
			
		}
		
		String actualLatestFolderView = btnSwitchGridListView.getAttribute("data-type");
		String expectedLatestFolderView = PropertyReader.getProperty("LISTTOGRIDVIEW");
		
		if(actualLatestFolderView.equals(expectedLatestFolderView))
		{
			Log.message("Grid to List view change is successfull");
			
		}
		else
		{
			Log.message("Grid to List view change failed");
			
		}

	}
	
	
	
	/** 
	 * Method for changing folder view from list to grid
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public void changeFolderViewListToGrid() 
	{
		
		SkySiteUtils.waitForElement(driver, btnSwitchGridListView, 30);
		Log.message("Switch file/folder view button is displayed");
		
		SkySiteUtils.waitTill(5000);
		String actualFolderView = btnSwitchGridListView.getAttribute("data-type");			
		String expectedFolderView = PropertyReader.getProperty("LISTTOGRIDVIEW");
		
		if(actualFolderView.equals(expectedFolderView))
		{
			btnSwitchGridListView.click();
			Log.message("Clicked on switch file/folder view button");
			SkySiteUtils.waitTill(8000);
			
		}
		
		String actualLatestFolderView = btnSwitchGridListView.getAttribute("data-type");
		String expectedLatestFolderView = PropertyReader.getProperty("GRIDTOLISTVIEW");
		
		if(actualLatestFolderView.equals(expectedLatestFolderView))
		{
			Log.message("List to Grid view change is successfull");
			
		}
		else
		{
			Log.message("List to Grid view change failed");
			
		}
		
	}
	
	
	/** 
	 * Method for validating descending order sorting by File Create Date
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean descendingOrderSortingByFileCreateDate() 
	{
		
		SkySiteUtils.waitForElement(driver, btnFolderSortBy, 30);
		Log.message("File/Folder Sort by button is displayed");		
		
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);		
		
		//btnFolderSortBy.click();
		Log.message("Clicked on File/Folder Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByFolderCreateDate, 20);
		btnSortByFolderCreateDate.click();
		Log.message("Clicked on sort by File Create Date option");
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);
		String sortOrder=btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("File sort order is:-" +sortOrder);
	    
		ArrayList<String> obtainedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h5_rev']"));
			    
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
		       obtainedFileList.add(we.getText().split("\\|")[2]); 
		     
			}
		}
		Log.message("Obtained Files count before sorting:- " +obtainedFileList.size());		 
		Log.message("Obtained File numbers are:- " +obtainedFileList);
		
		String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected File sort order is:- " +expectedSortOrder);
		
		if(sortOrder.contains(expectedSortOrder))
		{
			btnFolderSortOrder.click();
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not ascending");
		}
		SkySiteUtils.waitTill(8000);
	    
		ArrayList<String> sortedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@class='h5_rev']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				sortedFileList.add(we.getText().split("\\|")[2]); 
		     
			}
		}
		Log.message("Obtained Files count after sorting:- " +sortedFileList.size());		 
		Log.message("Obtained Files are:- " +sortedFileList);
		
		
	    if(obtainedFileList.get(0).contains(sortedFileList.get(sortedFileList.size()-1)) && obtainedFileList.get(obtainedFileList.size()-1).contains(sortedFileList.get(0)))
	    {	
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
		
	}
	
	
	/** 
	 * Method for validating ascending order sorting by File Create Date
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean ascendingOrderSortingByFileCreateDate() 
	{
		
		SkySiteUtils.waitForElement(driver, btnFolderSortBy, 30);
		Log.message("File/Folder Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		
		//btnFolderSortBy.click();
		Log.message("Clicked on File/Folder Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByFolderCreateDate, 20);
		btnSortByFolderCreateDate.click();
		Log.message("Clicked on sort by File Create Date option");
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);
		String sortOrder=btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("Folder sort order is:-" +sortOrder);
		
	    String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder);
		
		if(sortOrder.contains(expectedSortOrder))
		{
		
			WebElement element2 = driver.findElement(By.xpath("//*[@id='aSortingArrow']"));
			JavascriptExecutor executor2 = (JavascriptExecutor)driver;
			executor2.executeScript("arguments[0].click();", element2);			
			SkySiteUtils.waitTill(5000);
			
		}
		
		ArrayList<String> obtainedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h5_rev']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFileList.add(we.getText().split("\\|")[2]);
		     
			}
		}
		
		Log.message("Obtained Files count before sorting:- " +obtainedFileList.size());		 
		Log.message("Obtained Files are:- " +obtainedFileList);
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);
		String sortOrder1=btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("Present Folder sort order is:-" +sortOrder1);
		
		String expectedSortOrder1= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder1);
		
		if(sortOrder1.contains(expectedSortOrder1))
		{
			btnFolderSortOrder.click();
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not descending");
		}
		SkySiteUtils.waitTill(8000);
			
		ArrayList<String> sortedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@class='h5_rev']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				sortedFileList.add(we.getText().split("\\|")[2]);
		     
			}
		}
		Log.message("Obtained Files count after sorting:- " +sortedFileList.size());		 
		Log.message("Obtained Files are:- " +sortedFileList);
		
		if(obtainedFileList.get(0).contains(sortedFileList.get(sortedFileList.size()-1)) && obtainedFileList.get(obtainedFileList.size()-1).contains(sortedFileList.get(0)))
	    {	
			return true;
			
	    }
	    else
	    {
	    	return false;
	    	
	    }
		
	}
	
	
	
	/** 
	 * Method for validating descending order sorting by File Revision Date
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean descendingOrderSortingByFileRevisionDate() 
	{
		
		SkySiteUtils.waitForElement(driver, btnFolderSortBy, 30);
		Log.message("File/Folder Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		
		//btnFolderSortBy.click();
		Log.message("Clicked on File/Folder Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByFileRevisionDate, 20);
		btnSortByFileRevisionDate.click();
		Log.message("Clicked on sort by File Revision Date option");
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);
		String sortOrder=btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("File sort order is:-" +sortOrder);
	    
		ArrayList<String> obtainedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h5_rev']"));
			    
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
		       obtainedFileList.add(we.getText().split("\\|")[2]); 
		     
			}
		}
		Log.message("Obtained Files count before sorting:- " +obtainedFileList.size());		 
		Log.message("Obtained File numbers are:- " +obtainedFileList);
		
		String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected File sort order is:- " +expectedSortOrder);
		
		if(sortOrder.contains(expectedSortOrder))
		{
			btnFolderSortOrder.click();
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not ascending");
		}
		SkySiteUtils.waitTill(8000);
	    
		ArrayList<String> sortedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@class='h5_rev']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				sortedFileList.add(we.getText().split("\\|")[2]); 
		     
			}
		}
		Log.message("Obtained Files count after sorting:- " +sortedFileList.size());		 
		Log.message("Obtained Files are:- " +sortedFileList);
		
		
	    if(obtainedFileList.get(0).contains(sortedFileList.get(sortedFileList.size()-1)) && obtainedFileList.get(obtainedFileList.size()-1).contains(sortedFileList.get(0)))
	    {	
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	    
	}
	
	/** 
	 * Method for validating ascending order sorting by File Revision Date
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean ascendingOrderSortingByFileRevisionDate() 
	{
		
		SkySiteUtils.waitForElement(driver, btnFolderSortBy, 30);
		Log.message("File/Folder Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		
		//btnFolderSortBy.click();
		Log.message("Clicked on File/Folder Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByFileRevisionDate, 20);
		btnSortByFileRevisionDate.click();
		Log.message("Clicked on sort by File Revision Date option");
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);
		String sortOrder=btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("File sort order is:-" +sortOrder);
		
	    String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder);
		
		if(sortOrder.contains(expectedSortOrder))
		{
		
			WebElement element2 = driver.findElement(By.xpath("//*[@id='aSortingArrow']"));
			JavascriptExecutor executor2 = (JavascriptExecutor)driver;
			executor2.executeScript("arguments[0].click();", element2);			
			SkySiteUtils.waitTill(5000);
			
		}
		
		ArrayList<String> obtainedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h5_rev']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFileList.add(we.getText().split("\\|")[2]);
		     
			}
		}
		
		Log.message("Obtained Files count before sorting:- " +obtainedFileList.size());		 
		Log.message("Obtained Files are:- " +obtainedFileList);
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);
		String sortOrder1=btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("Present Folder sort order is:-" +sortOrder1);
		
		String expectedSortOrder1= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder1);
		
		if(sortOrder1.contains(expectedSortOrder1))
		{
			btnFolderSortOrder.click();
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not descending");
		}
		SkySiteUtils.waitTill(8000);
			
		ArrayList<String> sortedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@class='h5_rev']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				sortedFileList.add(we.getText().split("\\|")[2]);
		     
			}
		}
		Log.message("Obtained Files count after sorting:- " +sortedFileList.size());		 
		Log.message("Obtained Files are:- " +sortedFileList);
		
		if(obtainedFileList.get(0).contains(sortedFileList.get(sortedFileList.size()-1)) && obtainedFileList.get(obtainedFileList.size()-1).contains(sortedFileList.get(0)))
	    {	
			return true;
			
	    }
	    else
	    {
	    	return false;
	    	
	    }
		
	}
	
	/** 
	 * Method for validating descending order sorting by File Discipline
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean descendingOrderSortingByFileDiscipline() 
	{
		
		SkySiteUtils.waitForElement(driver, btnFolderSortBy, 30);
		Log.message("File/Folder Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		
		//btnFolderSortBy.click();
		Log.message("Clicked on File/Folder Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByFileDiscipline, 20);
		btnSortByFileDiscipline.click();
		Log.message("Clicked on sort by File Discipline option");
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);
		String sortOrder=btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("File sort order is:-" +sortOrder);
	    
		ArrayList<String> obtainedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h4_disc']"));
			    
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
		       obtainedFileList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Files count before sorting:- " +obtainedFileList.size());		 
		Log.message("Obtained File numbers are:- " +obtainedFileList);
		
		String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected File sort order is:- " +expectedSortOrder);
		
		if(sortOrder.contains(expectedSortOrder))
		{
			btnFolderSortOrder.click();
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not ascending");
		}
		SkySiteUtils.waitTill(8000);
	    
		ArrayList<String> sortedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@class='h4_disc']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				sortedFileList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Files count after sorting:- " +sortedFileList.size());		 
		Log.message("Obtained Files are:- " +sortedFileList);
		
		
	    if(obtainedFileList.get(0).contains(sortedFileList.get(sortedFileList.size()-1)) && obtainedFileList.get(obtainedFileList.size()-1).contains(sortedFileList.get(0)))
	    {	
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	    
	}
	
	
	/** 
	 * Method for validating ascending order sorting by File Discipline
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean ascendingOrderSortingByFileDiscipline() 
	{
		
		SkySiteUtils.waitForElement(driver, btnFolderSortBy, 30);
		Log.message("File/Folder Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		
		//btnFolderSortBy.click();
		Log.message("Clicked on File/Folder Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByFileDiscipline, 20);
		btnSortByFileDiscipline.click();
		Log.message("Clicked on sort by File Discipline option");
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);
		String sortOrder=btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("File sort order is:-" +sortOrder);
		
	    String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder);
		
		if(sortOrder.contains(expectedSortOrder))
		{
		
			WebElement element2 = driver.findElement(By.xpath("//*[@id='aSortingArrow']"));
			JavascriptExecutor executor2 = (JavascriptExecutor)driver;
			executor2.executeScript("arguments[0].click();", element2);			
			SkySiteUtils.waitTill(5000);
			
		}
		
		ArrayList<String> obtainedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h4_disc']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFileList.add(we.getText());
		     
			}
		}
		
		Log.message("Obtained Files count before sorting:- " +obtainedFileList.size());		 
		Log.message("Obtained Files are:- " +obtainedFileList);
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);
		String sortOrder1=btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("Present Folder sort order is:-" +sortOrder1);
		
		String expectedSortOrder1= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder1);
		
		if(sortOrder1.contains(expectedSortOrder1))
		{
			btnFolderSortOrder.click();
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not descending");
		}
		SkySiteUtils.waitTill(8000);
			
		ArrayList<String> sortedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@class='h4_disc']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				sortedFileList.add(we.getText());
		     
			}
		}
		Log.message("Obtained Files count after sorting:- " +sortedFileList.size());		 
		Log.message("Obtained Files are:- " +sortedFileList);
		
		if(obtainedFileList.get(0).contains(sortedFileList.get(sortedFileList.size()-1)) && obtainedFileList.get(obtainedFileList.size()-1).contains(sortedFileList.get(0)))
	    {	
			return true;
			
	    }
	    else
	    {
	    	return false;
	    	
	    }
	
	}
	
	
	/** 
	 * Method for fetching Manage File Sort Order
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public ArrayList<String> getManageFileSortOrder() 
	{
		
		SkySiteUtils.waitForElement(driver, breadcrumbManageFileOrder, 30);
		
		ArrayList<String> obtainedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='col-md-7']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFileList.add(we.getText());
		     
			}
		}
		
		Log.message("Obtained Files count in Manage File Order screen:- " +obtainedFileList.size());		 
		Log.message("Obtained Files are:- " +obtainedFileList);
		
		return obtainedFileList;
		
	}
	
	
	
	/** 
	 * Method for navigating to root folder level from Manage File Order screen
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public void rootFolderLevelNavigation() 
	{
		
		SkySiteUtils.waitForElement(driver, breadcrumbProjectRootFolderLevel, 30);
		breadcrumbProjectRootFolderLevel.click();
		Log.message("Clicked to navigate back to root folder level");
		SkySiteUtils.waitTill(8000);
		
	}
	
	
	
	/** 
	 * Method for validating descending order sorting by File Ordinal Number
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean descendingOrderSortingByFileOrdinalNumber(ArrayList<String> arr1) 
	{
		
		SkySiteUtils.waitForElement(driver, btnFolderSortBy, 30);
		Log.message("File/Folder Sort by button is displayed");
		btnFolderSortBy.click();
		Log.message("Clicked on File/Folder Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByOrdinalNumber, 20);
		btnSortByOrdinalNumber.click();
		Log.message("Clicked on sort by File Ordinal Number option");
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);
		String sortOrder=btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("File sort order is:-" +sortOrder);
	    
	    ArrayList<String> obtainedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h4_docname document-preview-event']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFileList.add(we.getText());
		     
			}
		}
		
		Log.message("Obtained Files count before sorting:- " +obtainedFileList.size());		 
		Log.message("Obtained Files are:- " +obtainedFileList);
		
		String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected File sort order is:- " +expectedSortOrder);
		
		if(sortOrder.contains(expectedSortOrder))
		{
			btnFolderSortOrder.click();
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not ascending");
		}
		SkySiteUtils.waitTill(8000);
		
		ArrayList<String> sortedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@class='h4_docname document-preview-event']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				sortedFileList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Files count after sorting:- " +sortedFileList.size());		 
		Log.message("Obtained Files are:- " +sortedFileList);
		
		if(arr1.get(0).contains(sortedFileList.get(sortedFileList.size()-1)) && arr1.get(arr1.size()-1).contains(sortedFileList.get(0)))
	    {	
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
		
	}
	
	
	/** 
	 * Method for validating ascending order sorting by File Ordinal Number
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean ascendingOrderSortingByFileOrdinalNumber(ArrayList<String> arr1) 
	{
		
		SkySiteUtils.waitForElement(driver, btnFolderSortBy, 30);
		Log.message("File/Folder Sort by button is displayed");
		btnFolderSortBy.click();
		Log.message("Clicked on File/Folder Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByOrdinalNumber, 20);
		btnSortByOrdinalNumber.click();
		Log.message("Clicked on sort by File Ordinal Number option");
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);
		String sortOrder=btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("File sort order is:-" +sortOrder);
		
	    String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder);
		
		if(sortOrder.contains(expectedSortOrder))
		{
		
			WebElement element2 = driver.findElement(By.xpath("//*[@id='aSortingArrow']"));
			JavascriptExecutor executor2 = (JavascriptExecutor)driver;
			executor2.executeScript("arguments[0].click();", element2);			
			SkySiteUtils.waitTill(5000);
			
		}
		
		ArrayList<String> obtainedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h4_docname document-preview-event']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFileList.add(we.getText());
		     
			}
		}
		
		Log.message("Obtained Files count before sorting:- " +obtainedFileList.size());		 
		Log.message("Obtained Files are:- " +obtainedFileList);

		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);
		String sortOrder1=btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("Present Folder sort order is:-" +sortOrder1);
		
		String expectedSortOrder1= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder1);
		
		if(sortOrder1.contains(expectedSortOrder1))
		{
			btnFolderSortOrder.click();
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not descending");
		}
		SkySiteUtils.waitTill(8000);
			
		ArrayList<String> sortedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@class='h4_docname document-preview-event']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				sortedFileList.add(we.getText());
		     
			}
		}
		Log.message("Obtained Files count after sorting:- " +sortedFileList.size());		 
		Log.message("Obtained Files are:- " +sortedFileList);
		
		if(arr1.equals(sortedFileList))
		{
			return true;
		}
		else
		{
			return false;
			
		}
		
	}
	
	
	/** 
	 * Method to change sorting order and save data to be validated later in file level
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public ArrayList<String> sortOrderSave1() 
	{
		
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Add new folder button is displayed");
			
		SkySiteUtils.waitForElement(driver, btnSwitchView, 30);
		Log.message("Switch view button is displayed");
		
		SkySiteUtils.waitForElement(driver, btnFolderSortBy, 30);
		Log.message("File Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='aSortOptionName']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on File Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByFolderName, 30);
		Log.message("Sort by File Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='aSortName']"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Sort by File Name");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);
		btnFolderSortOrder.click();
		Log.message("Clicked on sort order button");
		SkySiteUtils.waitTill(8000);
		String sortOrder=btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("File sort order is:-" +sortOrder);
	    
	    ArrayList<String> obtainedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h4_docname document-preview-event']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFileList.add(we.getText()); 
		     
			}
		}
		
		Log.message("Obtained files count before sorting:- " +obtainedFileList.size());		 
		Log.message("Obtained Files are:- " +obtainedFileList);
		
		
		return obtainedFileList;
		
	}
	
	
	
	/** 
	 * Method to verify sorting order saved earlier after Log In in File level
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean sortOrderSaveAfterLogIn1(ArrayList<String> arr1) 
	{
		
		SkySiteUtils.waitForElement(driver, btnFolderSortOrder, 30);	
		String sortBy = btnFolderSortOrder.getAttribute("data-original-title");
	    Log.message("Actual Folder sort by order is:-" +sortBy);
	    
	    String expectedSortBy= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
		Log.message("Expected Folder sort by order is:- " +expectedSortBy);
	    
		SkySiteUtils.waitForElement(driver, btnFolderSortBy, 30);
		String sortOrder = btnFolderSortBy.getText();
	    Log.message("Actual Folder sort order is:-" +sortOrder);
	    
	    String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE3");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder);
	    
		ArrayList<String> obtainedList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h4_docname document-preview-event']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
		       obtainedList.add(we.getText()); 
		     
			}
		}		
		Log.message("Obtained Project count after login:- " +obtainedList.size());		 
		Log.message("Obtained Project list order is:- " +obtainedList);
		
		if(sortBy.contains(expectedSortBy) && sortOrder.contains(expectedSortOrder) && arr1.equals(obtainedList))
		{
			return true;
			
		}
		else 
		{
			return false;
			
		}
		
	}

	
	
	/**
	 * Method written for Search a folder and export 
	 * Scripted By: Naresh Babu
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean SearchA_Folder_AndExport(String Sys_Download_Path,String Foldername,String csvFileToRead) throws InterruptedException, AWTException, IOException {
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 60);
		Log.message("Waiting for Create Folder button to be appeared");
		txtSearchField.sendKeys(Foldername);
		Log.message("Entered Folder Name: " + Foldername + " in search edit field.");
		btnGlobalSearch.click();
		Log.message("Clicked on Search button!!!");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, handIconSelect, 60);
		// Validate the search results
		String Search_Results = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]/section[2]/h4")).getText();
		if ((Search_Results.trim()).contentEquals(Foldername)) 
		{
			Log.message("Search results are proper with key of: " + Foldername);
		}

		// Calling "Deleting download folder files" method
		Log.message("Download Path is: " + Sys_Download_Path);
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(5000);

		btnExportToCSV.click();
		Log.message("Clicked on Export to csv button!!!");
		SkySiteUtils.waitTill(20000);

		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);

		if (browserName.contains("firefox")) {
			// Handling Download PopUp of firefox browser using robot
			Robot robot = null;
			robot = new Robot();

			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);
		}

		driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]")).click();
		Log.message("Search Folder is selected successfully!!!");
		SkySiteUtils.waitTill(10000);

		// Get The Count of available files in search folder
		int Avl_File_Count = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) {
			Avl_File_Count = Avl_File_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_File_Count);

		// Capturing All required validations from application
		String SubFolder_Name = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]/section[2]/h4")).getText();
		String Exp_Document_Path = Foldername + " " + ">>" + " " + SubFolder_Name;
		// Log.message("Exp Document Path is: "+Exp_Document_Path);

		driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]")).click();
		Log.message("Clicked on Sub folder to select.");
		SkySiteUtils.waitTill(10000);

		driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal media-object icon-lg'])[1]")).click();
		Log.message("Clicked on More options icon for the 1st file.");
		SkySiteUtils.waitTill(2000);
		WebElement EditLink = driver.findElement(By.xpath("(//a[contains(text(),'Edit')])[2]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", EditLink);// JSExecutor to click edit
																																										
		Log.message("Clicked on Edit Tab for the 1st file.");
		SkySiteUtils.waitForElement(driver, btnSaveEditDocWindow, 30);// Wait for save button of edit fields window
											
		String Exp_Document_Name = txtDocName.getAttribute("value");
		// Log.message("Exp Document Name is: "+Exp_Document_Name);
		String Exp_Revision_Date = txtRevisionDate.getAttribute("value");
		// Log.message("Exp_Revision_Date is: "+Exp_Revision_Date);
		String TimeSplitBy = " ";
		String[] Divide_Date = Exp_Revision_Date.split(TimeSplitBy);
		String Exp_Only_Date = Divide_Date[0];
		String Exp_Only_Time = Divide_Date[1];
		String Exp_Only_Noon = Divide_Date[2];
		// Log.message("Exp_OnlyRevision_Date is: "+Exp_Only_Date);
		// Log.message("Exp_OnlyRevision_Time is: "+Exp_Only_Time);
		// Log.message("Exp_OnlyRevision_AMorPM is: "+Exp_Only_Noon);

		btnCancelEditDocWindow.click();// Close the edit window

		// Validating the CSV file from download folder
		BufferedReader br = null;
		String line = null;
		String splitBy = ",";
		int count = 0;
		String ActFileName = null;
		String ActReviDate = null;
		String ActRevNumber = null;
		String ActDocuPath = null;
		String ActPublishStatus = null;
		int Match_Count = 0;

		br = new BufferedReader(new FileReader(csvFileToRead));
		while ((line = br.readLine()) != null) {
			count = count + 1;
			// Log.message("Downloaded csv file have data in : "+count+ "
			// rows");
			String[] ActValue = line.split(splitBy);
			ActFileName = ActValue[0];
			ActFileName = ActFileName.replace("\"", "");
			// Log.message("Document Name from csv file is: "+ ActFileName);
			ActReviDate = ActValue[1];
			ActReviDate = ActReviDate.replace("\"", "");
			// Log.message("Document Revision Date from csv file is: "+
			// ActReviDate);
			String[] Divide_Date_csv = ActReviDate.split(TimeSplitBy);
			String Act_Only_Date = Divide_Date_csv[0];
			String Act_Only_Time = Divide_Date_csv[1];
			// String Act_Only_Noon = Divide_Date_csv[2];
			// Log.message("Act_OnlyRevision_Date is: "+Act_Only_Date);
			// Log.message("Act_OnlyRevision_Time is: "+Act_Only_Time);
			// Log.message("Act_OnlyRevision_AMorPM is: "+Act_Only_Noon);
			ActRevNumber = ActValue[2];
			ActRevNumber = ActRevNumber.replace("\"", "");
			// Log.message("Document Revision Number from csv file is: "+
			// ActRevNumber);
			ActDocuPath = ActValue[3];
			ActDocuPath = ActDocuPath.replace("\"", "");
			// Log.message("Document Path from csv file is: "+ ActDocuPath);
			ActPublishStatus = ActValue[7];
			ActPublishStatus = ActPublishStatus.replace("\"", "");
			// Log.message("Is document published or nor from csv file is: "+
			// ActPublishStatus);

			if ((ActFileName.contains(Exp_Document_Name)) && (ActRevNumber.contentEquals("1"))
					&& (ActPublishStatus.equalsIgnoreCase("Yes")) && (Act_Only_Date.contentEquals(Exp_Only_Date))
					&& (Act_Only_Time.contains(Exp_Only_Time)) && (ActReviDate.contains(Exp_Only_Noon))
					&& (ActDocuPath.contentEquals(Exp_Document_Path))) 
			{
				Match_Count = Match_Count + 1;
			}
		}
		Log.message("Downloaded csv file have data in : " + count + " rows");
		Avl_File_Count = Avl_File_Count + 3;

		br.close();//Newly Added
		
		// Final Validation
		if ((count == Avl_File_Count) && (Match_Count == 1)) {
			Log.message("Search Folder Exported File is having data properly!!!");
			return true;
		} else {
			Log.message("Search Folder Exported File is NOT having data properly!!!");
			return false;
		}

	}

	/**
	 * Method written for Search a Sub folder(5th level) and export Scripted By:
	 * Naresh Babu Kavuru
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean SearchA_SubFolder_AndExport(String Sys_Download_Path,String Foldername,String csvFileToRead)throws InterruptedException, AWTException, IOException 
	{
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
		txtSearchField.sendKeys(Foldername);
		Log.message("Entered Folder Name: " + Foldername + " in search edit field.");
		btnGlobalSearch.click();
		Log.message("Clicked on Search button!!!");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, handIconSelect, 20);
		// Validate the search results
		String Search_Results = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]/section[2]/h4")).getText();
		if ((Search_Results.trim()).contentEquals(Foldername)) {
			Log.message("Search results are proper with key of: " + Foldername);
		}

		// Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(15000);

		btnExportToCSV.click();
		Log.message("Clicked on Export to csv button!!!");
		SkySiteUtils.waitTill(10000);

		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);

		if (browserName.contains("firefox")) {
			// Handling Download PopUp of firefox browser using robot
			Robot robot = null;
			robot = new Robot();

			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);
		}

		driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]")).click();
		Log.message("Search Folder is selected successfully!!!");
		SkySiteUtils.waitTill(10000);

		// Get The Count of available files in selected folder
		int Avl_File_Count = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Doc_')]"));
		for (WebElement Element : allElements) {
			Avl_File_Count = Avl_File_Count + 1;
		}
		Log.message("Available File Count is: " + Avl_File_Count);

		// Capturing All required validations from application
		String SubFolder_Name = driver
				.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div[1]/div/div[2]/ul/li[4]/span/a")).getText();
		String SubFolder1_Name = driver
				.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div[1]/div/div[2]/ul/li[5]/span/a")).getText();
		String SubFolder2_Name = driver
				.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div[1]/div/div[2]/ul/li[6]/span/a")).getText();
		String SubFolder3_Name = driver
				.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div[1]/div/div[2]/ul/li[7]/span/a")).getText();
		String SubFolder4_Name = driver
				.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div[1]/div/div[2]/ul/li[8]/span/a")).getText();

		String Exp_Document_Path = SubFolder_Name + " >> " + SubFolder1_Name + " >> " + SubFolder2_Name + " >> "
				+ SubFolder3_Name + " >> " + SubFolder4_Name + " >> " + Foldername;
		// Log.message("Exp Document Path is: "+Exp_Document_Path);

		String Exp_Document_Name = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]/section[2]/h4")).getText();
		// Log.message("Exp Document Name is: "+Exp_Document_Name);

		// Validating the CSV file from download folder
		BufferedReader br = null;
		String line = null;
		String splitBy = ",";
		int count = 0;
		String ActFileName = null;
		String ActRevNumber = null;
		String ActDocuPath = null;
		String ActPublishStatus = null;
		int Match_Count = 0;

		br = new BufferedReader(new FileReader(csvFileToRead));
		while ((line = br.readLine()) != null) {
			count = count + 1;
			// Log.message("Downloaded csv file have data in : "+count+ "
			// rows");
			String[] ActValue = line.split(splitBy);
			ActFileName = ActValue[0];
			ActFileName = ActFileName.replace("\"", "");
			// Log.message("Document Name from csv file is: "+ ActFileName);
			ActRevNumber = ActValue[2];
			ActRevNumber = ActRevNumber.replace("\"", "");
			// Log.message("Document Revision Number from csv file is: "+
			// ActRevNumber);
			ActDocuPath = ActValue[3];
			ActDocuPath = ActDocuPath.replace("\"", "");
			// Log.message("Document Path from csv file is: "+ ActDocuPath);
			ActPublishStatus = ActValue[7];
			ActPublishStatus = ActPublishStatus.replace("\"", "");
			// Log.message("Is document published or not from csv file is: "+
			// ActPublishStatus);

			if ((ActFileName.contains(Exp_Document_Name)) && (ActRevNumber.contentEquals("1"))
					&& (ActPublishStatus.equalsIgnoreCase("Yes")) && (ActDocuPath.contentEquals(Exp_Document_Path))) {
				Match_Count = Match_Count + 1;
			}
		}
		Log.message("Downloaded csv file have data in : " + count + " rows");
		Avl_File_Count = Avl_File_Count + 1;
		
		br.close();//Newly Added

		// Final Validation
		if ((count == Avl_File_Count) && (Match_Count == 1)) {
			Log.message("Search Sub Folder Exported File is having data properly!!!");
			return true;
		} else {
			Log.message("Search Sub Folder Exported File is NOT having data properly!!!");
			return false;
		}

	}

	/**
	 * Method written for export a folder contains unsupported files (.jpg, excel) 
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean Export_Folder_WithUnSupportFiles(String Sys_Download_Path,String csvFileToRead) throws InterruptedException, AWTException, IOException 
	{
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");

		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(10000);

		btnExportToCSV.click();
		Log.message("Clicked on Export to csv button!!!");
		SkySiteUtils.waitTill(10000);

		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);

		if (browserName.contains("firefox")) {
			// Handling Download PopUp of firefox browser using robot
			Robot robot = null;
			robot = new Robot();

			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);
		}

		String Foldername = PropertyReader.getProperty("Fold_UnSuportFiles");
		this.Select_Folder(Foldername);// Calling Select expected Folder
		SkySiteUtils.waitTill(10000);

		// Get The Count of available files in selected folder
		int Avl_File_Count = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Doc_')]"));
		for (WebElement Element : allElements) {
			Avl_File_Count = Avl_File_Count + 1;
		}
		Log.message("Available File Count is: " + Avl_File_Count);

		// Capturing All required validations from application
		String Exp_Document_Name = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]/section[2]/h4")).getText();
		Log.message("Exp Document Name is: " + Exp_Document_Name);

		// Validating the CSV file from download folder
		BufferedReader br = null;
		String line = null;
		String splitBy = ",";
		int count = 0;
		String ActFileName = null;
		String ActRevNumber = null;
		String ActDocuPath = null;
		String ActPublishStatus = null;
		int Match_Count = 0;

		br = new BufferedReader(new FileReader(csvFileToRead));
		while ((line = br.readLine()) != null) {
			count = count + 1;
			// Log.message("Downloaded csv file have data in : "+count+ "
			// rows");
			String[] ActValue = line.split(splitBy);
			ActFileName = ActValue[0];
			ActFileName = ActFileName.replace("\"", "");
			Log.message("Document Name from csv file is: " + ActFileName);
			ActRevNumber = ActValue[2];
			ActRevNumber = ActRevNumber.replace("\"", "");
			Log.message("Document Revision Number from csv file is: " + ActRevNumber);
			ActDocuPath = ActValue[3];
			ActDocuPath = ActDocuPath.replace("\"", "");
			Log.message("Document Path from csv file is: " + ActDocuPath);
			ActPublishStatus = ActValue[7];
			ActPublishStatus = ActPublishStatus.replace("\"", "");
			Log.message("Is document published or not from csv file is: " + ActPublishStatus);

			if ((ActFileName.contains(Exp_Document_Name)) && (ActRevNumber.contentEquals("1"))
					&& (ActPublishStatus.equalsIgnoreCase("Yes")) && (ActDocuPath.contentEquals(Foldername))) {
				Match_Count = Match_Count + 1;
			}
		}
		Log.message("Downloaded csv file have data in : " + count + " rows");
		Avl_File_Count = Avl_File_Count + 1;

		br.close();//Newly Added
		
		// Final Validation
		if ((count == Avl_File_Count) && (Match_Count == 1)) {
			Log.message("Folder having unsupported files Exported File is having data properly!!!");
			return true;
		} else {
			Log.message("Folder having unsupported files Exported File is NOT having data properly!!!");
			return false;
		}

	}

	/**
	 * Method written for Gallery folder export 
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean Gallery_Folder_Export(String Sys_Download_Path,String csvFileToRead) throws InterruptedException, AWTException, IOException 
	{
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");

		// Calling "Select Gallery Folder" Method
		this.Select_Gallery_Folder();
		SkySiteUtils.waitTill(5000);

		// Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(10000);

		btnExportToCSV.click();
		Log.message("Clicked on Export to csv button!!!");
		SkySiteUtils.waitTill(10000);

		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);

		if (browserName.contains("firefox")) {
			// Handling Download PopUp of firefox browser using robot
			Robot robot = null;
			robot = new Robot();

			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);
		}

		// Get The Count of available files in search folder
		int Avl_File_Count = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'photo-viewer preview')]"));
		for (WebElement Element : allElements) {
			Avl_File_Count = Avl_File_Count + 1;
		}
		Log.message("Available Items Count in Gallery is: " + Avl_File_Count);

		// Capturing All required validations from application
		driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal media-object icon-lg'])[5]")).click();
		Log.message("Clicked on More options icon for the 1st Photo.");
		SkySiteUtils.waitTill(2000);
		WebElement EditLink = driver.findElement(By.xpath("(//a[contains(text(),'Edit')])[6]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", EditLink);// JSExecutor to click edit
		Log.message("Clicked on Edit Tab for the 1st Photo.");
		SkySiteUtils.waitForElement(driver, txtPhotoName, 30);// Wait for save button of edit fields window
		SkySiteUtils.waitTill(2000);
		String Exp_Photo_Name = txtPhotoName.getAttribute("value");
		// Log.message("Exp Photo Name is: "+Exp_Photo_Name);
		String Exp_Building = driver.findElement(By.xpath("(//input[@id='txtphotoAttr'])[1]")).getAttribute("value");
		// Log.message("Exp Building Name is: "+Exp_Building);
		String Exp_Level = driver.findElement(By.xpath("(//input[@id='txtphotoAttr'])[2]")).getAttribute("value");
		// Log.message("Exp Level Name is: "+Exp_Level);
		String Exp_Room = driver.findElement(By.xpath("(//input[@id='txtphotoAttr'])[3]")).getAttribute("value");
		// Log.message("Exp room Name is: "+Exp_Room);
		String Exp_Area = driver.findElement(By.xpath("(//input[@id='txtphotoAttr'])[4]")).getAttribute("value");
		// Log.message("Exp Area is: "+Exp_Area);
		String Exp_Description = driver.findElement(By.xpath("(//input[@id='txtphotoAttr'])[5]")).getAttribute("value");
		// Log.message("Exp Description is: "+Exp_Description);

		butCancelEditAttriWind.click();// Close the edit attribute window

		// Validating the CSV file from download folder
		BufferedReader br = null;
		String line = null;
		String splitBy = ",";
		int count = 0;
		String ActPhotoName = null;
		String ActBuilding = null;
		String ActBuildLevel = null;
		String ActRoom = null;
		String ActArea = null;
		String ActDescription = null;
		int Match_Count = 0;

		br = new BufferedReader(new FileReader(csvFileToRead));
		while ((line = br.readLine()) != null) {
			count = count + 1;
			// Log.message("Downloaded csv file have data in : "+count+ "
			// rows");
			String[] ActValue = line.split(splitBy);
			ActPhotoName = ActValue[0];
			// Log.message("Photo Name from csv file is: "+ ActPhotoName);
			ActBuilding = ActValue[4];
			// Log.message("Building Name from csv file is: "+ ActBuilding);
			ActBuildLevel = ActValue[5];
			// Log.message("Building Level from csv file is: "+ ActBuildLevel);
			ActRoom = ActValue[6];
			// Log.message("Room Number From csv file is: "+ ActRoom);
			ActArea = ActValue[7];
			// Log.message("Room Area From csv file is: "+ ActArea);
			ActDescription = ActValue[8];
			// Log.message("Description of Building from csv file is: "+
			// ActDescription);

			if ((ActPhotoName.contains(Exp_Photo_Name)) && (ActBuilding.contentEquals(Exp_Building))
					&& (ActBuildLevel.contains(Exp_Level)) && (ActRoom.contains(Exp_Room))
					&& (ActArea.contentEquals(Exp_Area)) && (ActDescription.contentEquals(Exp_Description))) {
				Match_Count = Match_Count + 1;
			}
		}
		Log.message("Downloaded csv file have data in : " + count + " rows");
		Avl_File_Count = Avl_File_Count + 2;
		
		br.close();//Newly Added

		// Final Validation
		if ((count == Avl_File_Count) && (Match_Count == 1)) {
			Log.message("Gallery Folder Exported CSV File is having data properly!!!");
			return true;
		} else {
			Log.message("Gallery Folder Exported CSV File is NOT having data properly!!!");
			return false;
		}

	}

	/**
	 * Method written for Validate uploaded files from folder 
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	// Validate Files are uploaded properly or not
	public boolean ValidateUploadFiles(String FolderPath, int FileCount) throws InterruptedException {
		boolean result = false;
		try {
			String ActFileName = null;
			String expFilename = null;
			int uploadSuccessCount = 0;
			// Reading all the file names from input folder
			File[] files = new File(FolderPath).listFiles();

			for (File file : files) {
				if (file.isFile()) {
					expFilename = file.getName();// Getting File Names into a
													// variable
					Log.message("Expected File name is:" + expFilename);
					SkySiteUtils.waitTill(5000);
					for (int n = 2; n <= FileCount + 1; n++) {
						ActFileName = driver
								.findElement(By.xpath(
										"html/body/div[1]/div[3]/div[3]/div[2]/ul/li[" + n + "]/section[2]/h4/span[1]"))
								.getText();
						Log.message("Actual File name is:" + ActFileName);

						if (expFilename.trim().equalsIgnoreCase(ActFileName.trim())) {
							uploadSuccessCount = uploadSuccessCount + 1;
							Log.message("File uploded success for:" + ActFileName + "Sucess Count:" + uploadSuccessCount);
							break;
						}
					}

				}

			}

			// Checking whether file count is equal or not
			if (FileCount == uploadSuccessCount) {
				Log.message("All the files are available in folder.");
				result = true;
			} else {
				Log.message("All the files are NOT available in folder.");
				result = false;
			}

		} // End of try block

		catch (Exception e) {
			result = false;
		}

		finally {
			return result;
		}
	}

	/**
	 * Method written for Validate uploaded Photos to gallery 
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	// Validate Photos are uploaded properly or not
	public boolean ValidateUploadPhotos(String FolderPath, int FileCount) throws InterruptedException {
		boolean result = false;
		String ActPhotoName = null;
		String expPhotoname = null;
		int uploadSuccessCount = 0;

		try 
		{
			SkySiteUtils.waitTill(5000);
			// Reading all the photo names in a folder
			File[] files = new File(FolderPath).listFiles();
			for (File file : files) 
			{
				if (file.isFile()) 
				{
					expPhotoname = file.getName();// Getting Photo Names into a variable
					Log.message("Expected Photo name is:" + expPhotoname);
					SkySiteUtils.waitTill(1000);
					for (int n = 1; n <= FileCount + (FileCount-1); n++) 
					{
						n = n + 1;
						ActPhotoName = driver.findElement(By.xpath("html/body/div[1]/div[4]/div[2]/div/div/div/ul/li["+n+"]/section[2]/h4")).getText();
						Log.message("Actual Photo name is:" + ActPhotoName);

						if (expPhotoname.trim().contains(ActPhotoName.trim())) 
						{
							uploadSuccessCount = uploadSuccessCount + 1;
							Log.message("File uploded success for:" + ActPhotoName + "Sucess Count:" + uploadSuccessCount);
							break;
						}
					}

				}

			}

			// Checking whether file count is equal or not
			if (FileCount == uploadSuccessCount) {
				result = true;
			} else {
				result = false;
			}

		} // End of try block

		catch (Exception e) {
			result = false;
		}

		finally {
			return result;
		}
	}

	/**
	 * Method written for Validate LD folder is empty or not 
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */

	public boolean Validate_LD_FolderIsEmpty() throws InterruptedException {
		SkySiteUtils.waitForElement(driver, objLDFolder, 30);
		SkySiteUtils.waitTill(2000);
		objLDFolder.click();
		Log.message("Clicked on LD folder.");
		SkySiteUtils.waitForElement(driver, objNoLatestDocsAvailable, 30);
		if (objNoLatestDocsAvailable.isDisplayed()) {
			Log.message("LD Folder is not having any files.");
			SkySiteUtils.waitTill(5000);
			ParentFold_BreadCrumb.click();
			Log.message("Clicked on project header from breadcrum.");
			return true;
		} else {
			Log.message("Still LD Folder is having files after select exclude LD.");
			return false;

		}
	}

	/**
	 * Method written for upload files using without index 
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */

	public boolean UploadFiles_WithoutIndex(String FolderPath, int FileCount)
			throws InterruptedException, AWTException, IOException {
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnUploadfile, 30);
		Log.message("Waiting for upload file button to be appeared");
		SkySiteUtils.waitTill(3000);
		btnUploadfile.click();// Click on Upload file
		Log.message("Clicked on upload file button.");
		SkySiteUtils.waitForElement(driver, btnCloudAccount, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnChooseFile.click();// Click on Choose File
		Log.message("Clicked on choose file button.");
		SkySiteUtils.waitTill(15000);

		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		//String tmpFileName = "c:/" + rn.nextFileName() + ".txt";
		//String tmpFileName = "c:/AutoIT_TempFile/" + rn.nextFileName() + ".txt";
		String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a
												// variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(1000);
			}
		}

		output.flush();
		output.close();

		// Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(10000);

		SkySiteUtils.waitForElement(driver, btnUploadWithoutIndex, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(10000);
		// Delete the temp file
		try {
			File file = new File(tmpFileName);
			if (file.delete()) {
				Log.message(file.getName() + " is deleted!");
			} else {
				Log.message("Delete operation is failed.");
			}
		} catch (Exception e) {
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(10000);
		btnUploadWithoutIndex.click();// Click on Upload without index
		Log.message("Clicked on Upload without index button.");
		SkySiteUtils.waitTill(5000);
	/*	SkySiteUtils.waitForElement(driver, notificationMsg, 180);
		Log.message("Waiting for Notification Message to be appeared");
		String UploadSuccess_Message = notificationMsg.getText();
		Log.message("Notification Message after upload is: " + UploadSuccess_Message);
		SkySiteUtils.waitTill(5000);*/
		SkySiteUtils.waitForElement(driver, linkProcessCompleted, 180);
		linkProcessCompleted.click();
		Log.message("Clicked on Process Completed Link.");
		SkySiteUtils.waitTill(10000);

		result = this.ValidateUploadFiles(FolderPath, FileCount);// Calling validate files method

		if (result == true) {
			driver.findElement(By.xpath("//a[@class='aProjHeaderLink']")).click();
			SkySiteUtils.waitTill(10000);
			Log.message("Clicked on project header.");
			Log.message("Upload files using donot index is working successfully!!!");
			return true;
		} else {
			Log.message("Upload files using donot index is not working!!!");
			return false;
		}

	}
	
	/**
	 * Method written for Checking "Delete Icon" functionality from the conversion page 
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */

	public boolean Verify_DeleteIcon_From_ConversionPage(String FolderPath, int FileCount)
			throws InterruptedException, AWTException, IOException {
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnUploadfile, 30);
		Log.message("Waiting for upload file button to be appeared");
		SkySiteUtils.waitTill(3000);
		btnUploadfile.click();// Click on Upload file
		Log.message("Clicked on upload file button.");
		SkySiteUtils.waitForElement(driver, btnCloudAccount, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnChooseFile.click();// Click on Choose File
		Log.message("Clicked on choose file button.");
		SkySiteUtils.waitTill(10000);

		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		//String tmpFileName = "c:/" + rn.nextFileName() + ".txt";
		//String tmpFileName = "c:/AutoIT_TempFile/" + rn.nextFileName() + ".txt";
		String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a
												// variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(1000);
			}
		}

		output.flush();
		output.close();

		// Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(10000);

		SkySiteUtils.waitForElement(driver, btnUploadWithIndex, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(10000);
		// Delete the temp file
		try {
			File file = new File(tmpFileName);
			if (file.delete()) {
				Log.message(file.getName() + " is deleted!");
			} else {
				Log.message("Delete operation is failed.");
			}
		} catch (Exception e) {
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(10000);
		btnUploadWithIndex.click();// Click on Upload with index
		Log.message("Clicked on Upload without index button.");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, linkPublishnow, 180);
		SkySiteUtils.waitTill(5000);
		iconDeletePublishLogPage.click();
		Log.message("Clicked on Delete icon of the publish session.");
		SkySiteUtils.waitForElement(driver, btnInfoMsgYes, 30);
		btnInfoMsgYes.click();
		SkySiteUtils.waitForElement(driver, PrivateProjectsTab, 30);

		if (PrivateProjectsTab.isDisplayed()) 
		{
			Log.message("Files session got deleted successfully!!!");
			return true;
		} else {
			Log.message("Files session Failed to deleted!!!");
			return false;
		}

	}
	
	/**
	 * Method written for Checking "Delete All Button" functionality from the Publishing page 
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */

	@FindBy(xpath="//button[@value='Delete' and @type='button']")
	WebElement btnDeleteAllPublishPage;
	
	public boolean Verify_DeleteAllButton_From_PublishPage(String FolderPath, int FileCount)
			throws InterruptedException, AWTException, IOException {
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnUploadfile, 30);
		Log.message("Waiting for upload file button to be appeared");
		SkySiteUtils.waitTill(3000);
		btnUploadfile.click();// Click on Upload file
		Log.message("Clicked on upload file button.");
		SkySiteUtils.waitForElement(driver, btnCloudAccount, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnChooseFile.click();// Click on Choose File
		Log.message("Clicked on choose file button.");
		SkySiteUtils.waitTill(10000);

		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		//String tmpFileName = "c:/" + rn.nextFileName() + ".txt";
		//String tmpFileName = "c:/AutoIT_TempFile/" + rn.nextFileName() + ".txt";
		String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a
												// variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(1000);
			}
		}

		output.flush();
		output.close();

		// Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(10000);

		SkySiteUtils.waitForElement(driver, btnUploadWithIndex, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(10000);
		// Delete the temp file
		try {
			File file = new File(tmpFileName);
			if (file.delete()) {
				Log.message(file.getName() + " is deleted!");
			} else {
				Log.message("Delete operation is failed.");
			}
		} catch (Exception e) {
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(10000);
		btnUploadWithIndex.click();
		Log.message("Clicked on Upload without index button.");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, linkPublishnow, 180);
		SkySiteUtils.waitTill(2000);
		linkPublishnow.click();
		Log.message("Clicked on Publish now link.");
		SkySiteUtils.waitForElement(driver, btnDeleteAllPublishPage, 30);
		SkySiteUtils.waitTill(10000);
		btnDeleteAllPublishPage.click();
		Log.message("Clicked on Delete All button from the publish page.");
		SkySiteUtils.waitForElement(driver, btnInfoMsgYes, 30);
		btnInfoMsgYes.click();
		SkySiteUtils.waitForElement(driver, PrivateProjectsTab, 50);

		if (PrivateProjectsTab.isDisplayed()) 
		{
			Log.message("Files session got deleted successfully!!!");
			return true;
		} else {
			Log.message("Files session Failed to deleted!!!");
			return false;
		}

	}
	
	/**
	 * Method written for Checking "Publish later by Adding Set name" 
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */

	@FindBy(xpath="//input[@id='txtSessionName']")
	WebElement txtSessionName;
	
	@FindBy(xpath="//button[@id='btnPublishLater']")
	WebElement btnPublishLater;
	
	@FindBy(xpath="//strong[@class='info-trim']")
	WebElement infoSessionName;
	
	public boolean Verify_PublishLater_ByAdding_SetName(String FolderPath, int FileCount)
			throws InterruptedException, AWTException, IOException {
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnUploadfile, 30);
		Log.message("Waiting for upload file button to be appeared");
		SkySiteUtils.waitTill(3000);
		btnUploadfile.click();
		Log.message("Clicked on upload file button.");
		SkySiteUtils.waitForElement(driver, btnCloudAccount, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnChooseFile.click();
		Log.message("Clicked on choose file button.");
		SkySiteUtils.waitTill(10000);

		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		//String tmpFileName = "c:/" + rn.nextFileName() + ".txt";
		//String tmpFileName = "c:/AutoIT_TempFile/" + rn.nextFileName() + ".txt";
		String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a
												// variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(1000);
			}
		}

		output.flush();
		output.close();

		// Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(10000);

		SkySiteUtils.waitForElement(driver, btnUploadWithIndex, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(10000);
		// Delete the temp file
		try {
			File file = new File(tmpFileName);
			if (file.delete()) {
				Log.message(file.getName() + " is deleted!");
			} else {
				Log.message("Delete operation is failed.");
			}
		} catch (Exception e) {
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(10000);
		btnUploadWithIndex.click();
		Log.message("Clicked on Upload with index button.");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, txtSessionName, 20);
		SkySiteUtils.waitTill(1000);
		String Input_SessionName = "Session_"+Generate_Random_Number.generateRandomValue();
		txtSessionName.sendKeys(Input_SessionName);
		btnPublishLater.click();
		Log.message("Clicked on Publish Later button after entering session name.");
		SkySiteUtils.waitForElement(driver, linkPublishnow, 300);
		SkySiteUtils.waitTill(2000);
		String Act_SessionName = infoSessionName.getText();
		Log.message("Seession from the application is: "+Act_SessionName);
	
		if (Act_SessionName.contentEquals(Input_SessionName)) 
		{
			Log.message("Publishing later by giving session name is working fine!!!");
			return true;
		} else {
			Log.message("Publishing later by giving session name is not working!!!");
			return false;
		}

	}
	
	/**
	 * Method written for Checking "Skip" functionality from the Publishing page 
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */

	@FindBy(xpath="//button[@data-bind='click: SkipPublishDocs, clickBubble: false']")
	WebElement btnSkipPublish;
	
	public boolean Verify_Skip_From_PublishPage(String FolderPath, int FileCount)
			throws InterruptedException, AWTException, IOException {
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnUploadfile, 30);
		Log.message("Waiting for upload file button to be appeared");
		SkySiteUtils.waitTill(3000);
		btnUploadfile.click();// Click on Upload file
		Log.message("Clicked on upload file button.");
		SkySiteUtils.waitForElement(driver, btnCloudAccount, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnChooseFile.click();// Click on Choose File
		Log.message("Clicked on choose file button.");
		SkySiteUtils.waitTill(10000);

		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		//String tmpFileName = "c:/" + rn.nextFileName() + ".txt";
		//String tmpFileName = "c:/AutoIT_TempFile/" + rn.nextFileName() + ".txt";
		String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a
												// variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(1000);
			}
		}

		output.flush();
		output.close();

		// Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(10000);

		SkySiteUtils.waitForElement(driver, btnUploadWithIndex, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(10000);
		// Delete the temp file
		try {
			File file = new File(tmpFileName);
			if (file.delete()) {
				Log.message(file.getName() + " is deleted!");
			} else {
				Log.message("Delete operation is failed.");
			}
		} catch (Exception e) {
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(10000);
		btnUploadWithIndex.click();
		Log.message("Clicked on Upload without index button.");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, linkPublishnow, 180);
		SkySiteUtils.waitTill(2000);
		String Exp_SessionName = infoSessionName.getText();
		Log.message("Seession id while publish is: "+Exp_SessionName);
		linkPublishnow.click();
		Log.message("Clicked on Publish now link.");
		SkySiteUtils.waitForElement(driver, btnSkipPublish, 30);
		SkySiteUtils.waitTill(10000);
		btnSkipPublish.click();
		Log.message("Clicked on Skip button from the publish page.");
		SkySiteUtils.waitForElement(driver, infoSessionName, 30);
		String SessionName_AfterSkip = infoSessionName.getText();
		Log.message("Seession After skip is: "+SessionName_AfterSkip);

		if (SessionName_AfterSkip.contentEquals(Exp_SessionName)) 
		{
			Log.message("Session got Skipped successfully!!!");
			return true;
		} else {
			Log.message("Session Failed to Skip!!!");
			return false;
		}

	}
	
	
	/**
	 * Method written for Getting sheet count and details from excel
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws BiffException 
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	
	public String getSheetCount(String sheetCount_FilePath,String FileName) throws BiffException, IOException
	{	
		String sheetCount = null;
		FileInputStream sheetCountExcel = new FileInputStream(sheetCount_FilePath);
		Workbook FNames1 = Workbook.getWorkbook(sheetCountExcel);
		//Get the first sheet in excel workbook     
		Sheet s2 = FNames1.getSheet("AllSheetCounts");
		int ColCount1 = s2.getColumns();//Getting Column count from Excel
		int RowCount1 = s2.getRows();//Getting Row count from Excel             
		for(int j=1;j<RowCount1;j++)
		{
			for(int i=0;i<ColCount1;i++)
			{
				String TestCaseHeader=s2.getCell(i,0).getContents();
		        if(TestCaseHeader.equals("FileName"))
		         
		   {
		                 
		   if(s2.getCell(i,j).getContents().equalsIgnoreCase(FileName))
		          	
		          sheetCount=s2.getCell(i+1,j).getContents();
		   Log.message("sheetCount:"+sheetCount);
		        }
		      }//For Loop  	
		   }
		return sheetCount;
		}
	
	
	public String getSheetName(int expectedLineNumber,String tmpfilepath ) throws FileNotFoundException
	{
		LineIterator it;
		String line=null;
		String sheetname=null;
		it = IOUtils.lineIterator(new BufferedReader(new FileReader(tmpfilepath)));
		for (int lineNumber = 0; it.hasNext(); lineNumber++) 
		{
			line = (String) it.next();
			if (lineNumber == expectedLineNumber) 
			{
				sheetname=line;
			}
			   		    		    
		}
		return sheetname;
	}
	
	/**
	 * Method written for upload files using Index By File Name
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 * @throws BiffException 
	 */
	public boolean UploadFiles_IndexByFileName(String FolderPath, int FileCount,String sheetCount_FilePath)
			throws InterruptedException, AWTException, IOException, BiffException {
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnUploadfile, 30);
		Log.message("Waiting for upload file button to be appeared");
		SkySiteUtils.waitTill(3000);
		btnUploadfile.click();
		Log.message("Clicked on upload file button.");
		SkySiteUtils.waitForElement(driver, btnCloudAccount, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnChooseFile.click();
		Log.message("Clicked on choose file button.");
		SkySiteUtils.waitTill(10000);

		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(500);
			}
		}

		output.flush();
		output.close();

		// Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(10000);

		SkySiteUtils.waitForElement(driver, btnUploadWithIndex, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(10000);
		
		int index=0;
		int shtCount=0;
		String fname=null;
		String sheetcount=null;
		String sheetNms=null;
		
		int totalsheetcount=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//div//h5[@class='file-name']")); 
	    String tmpSheetFile="./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
	    Log.message("Temp sheet count file name is : "+tmpSheetFile);
		output = new BufferedWriter(new FileWriter(tmpSheetFile,true));
		
		for (WebElement Element : allElements) 
		{ 
			index = index+1;	
			fname=Element.getText();
			sheetcount=this.getSheetCount(sheetCount_FilePath,fname); //get sheet count of each file from excel sheet
			shtCount=Integer.parseInt(sheetcount);
			String sheetname=fname.substring(0,fname.indexOf("."));	
			String sheetNameUpperCase=sheetname.toUpperCase();
			for(int i=1;i<=shtCount;i++)
			{		
				sheetNms=sheetNameUpperCase+"-"+String.valueOf(i);
				output.append(sheetNms);// Writing sheet name of each file
				output.newLine();
			}
			totalsheetcount=totalsheetcount+shtCount;	
		}
		
		output.flush();
		output.close();	
		
		Log.message("Total File Count is "+index);
		Log.message("Total sheet Count is "+totalsheetcount);

	//keep all sheets in a HashMap with index
		Map allsheetsCollection = new HashMap();
		for(int k=0;k<totalsheetcount;k++)
		{	
			allsheetsCollection.put(k,this.getSheetName(k,tmpSheetFile)); 
		}
		Log.message("All Sheet collection is : "+allsheetsCollection);
		SkySiteUtils.waitTill(10000);
		btnUploadWithIndex.click();
		Log.message("Clicked on Upload with index button.");
		SkySiteUtils.waitTill(2000);
		
		/*SkySiteUtils.waitForElement(driver, notificationMsg, 180);
		String AfterFileUploads_NotiMsg=notificationMsg.getText();
		Log.message("Notification Msg is: "+AfterFileUploads_NotiMsg);
		if(AfterFileUploads_NotiMsg.contentEquals("Files processing is in progress."))
		{
			Log.message("Upload Process Completed Successfully and converion is started");
		}*/
		
		SkySiteUtils.waitForElement(driver, linkPublishnow, 180);
		SkySiteUtils.waitTill(2000);
		linkPublishnow.click();
		Log.message("Clicked on Publish now link.");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, btnPublishNew, 30);
		SkySiteUtils.waitTill(15000);	

		// Delete the AutoIT temp file
		try {
			File file = new File(tmpFileName);
			if (file.delete()) {
				Log.message(file.getName() + " is deleted!");
			} else {
				Log.message("Delete operation is failed.");
			}
		} catch (Exception e) {
			Log.message("Exception occured!!!" + e);
		}
		
		txtRevisionInput.sendKeys("Revision 1");
		String DateField=infoRevisionDate.getAttribute("value");
		Log.message("Actual Date "+DateField);
		dropdownIndxType.click();
		Log.message("Clicked on Index dropdown.");
		SkySiteUtils.waitTill(2000);
		droplistIndxByFileName.click();
		Log.message("Selected Index By File Name option from dropdown.");
		SkySiteUtils.waitTill(10000);
		
		int SheetCount =totalsheetcount;
		int totalPageCount=0;
		int pgcount=0;
		int extra=0;
		pgcount=SheetCount/20;
		extra=SheetCount%20;
	
		if(( pgcount>0)&&(extra>0))//Multiple of 20 plus extra
		{     
			totalPageCount=pgcount+1;  		
		}
	
		if(( pgcount>0)&&(extra==0))//Multiple of 20 only
		{	
			totalPageCount=pgcount;   		
		}

		if(( pgcount==0)&&(extra<20))//less than  20 only
		{
			totalPageCount=1;	   		
		}	
	
		Log.message("Total Page Count is:"+totalPageCount);        	

		int RemainingSheetCount=SheetCount;
		int c=0;
		int fieldValidationSuccessCount=0;
		for(int i=1;i<=totalPageCount;i++)
		{
			if(RemainingSheetCount<20)
			{
				Log.message("Total Sheet Cout is less than 20");
				for(int j=1;j<=RemainingSheetCount;j++)
				{

					String FirstSheetName = driver.findElement(By.xpath("(//input[@id='SheetName'])["+j+"]")).getAttribute("value");
					//driver.findElement(By.xpath("(//textarea[@class='form-control input-box txtDescription'])["+j+"]")).click();
					//driver.findElement(By.xpath("(//textarea[@class='form-control input-box txtDescription'])["+j+"]")).sendKeys(SheetDescription); //Enter Description
					SkySiteUtils.waitTill(2000);		
					String expectedDate=getCurrentDate.getPSTTime();
					Log.message("SheetName from App is: "+FirstSheetName);			
			//Convert Lower case string to upper case
					String  FirstSheetName_Uppercase = null;
					FirstSheetName_Uppercase = FirstSheetName.toUpperCase();
					Log.message("Actual Shhet Name is: "+FirstSheetName_Uppercase);
					//Log.message("Revision of the sheet: "+Rev_FirstSheet);
					
					Log.message("Expected Date "+expectedDate);
					c=(((i-1)*20)+j)-1;  // Get every sheet index  no per page

					//&&(DateField.contains(expectedDate)) - Removed date validation
					if(allsheetsCollection.containsValue(FirstSheetName_Uppercase))
					{
						fieldValidationSuccessCount=fieldValidationSuccessCount+1;	
						Log.message("PASS: Sheet Name and Date Field System generated Value validated for Sheet No :"+c);
					}
					else
					{
						Log.message("FAIL: Sheet Name and Date Field System generated Value Not Proper for Sheet No :"+c);
					}
				}//End of for(J) loop
			//publish the page
				btnPublishNew.click();
				Log.message("Clicked on Publish now button.");
				SkySiteUtils.waitForElement(driver, btnInfoMsgYes, 30);
				btnInfoMsgYes.click();

				if(i==totalPageCount)
				{
					SkySiteUtils.waitForElement(driver, iconListView, 30);
					Log.message("Files publish is done.");
				}
				RemainingSheetCount=RemainingSheetCount-20;
			}//End of If (RemainingSheetCount<20)

			if(RemainingSheetCount>=20)
			{
				Log.message("Remaining Sheet count is >=20");
				for(int j=1;j<=20;j++)
				{

					String FirstSheetName = driver.findElement(By.xpath("(//input[@id='SheetName'])["+j+"]")).getAttribute("value");
					//driver.findElement(By.xpath("(//textarea[@class='form-control input-box txtDescription'])["+j+"]")).click();
					//driver.findElement(By.xpath("(//textarea[@class='form-control input-box txtDescription'])["+j+"]")).sendKeys(SheetDescription); //Enter Description
					SkySiteUtils.waitTill(2000);		
					String expectedDate=getCurrentDate.getPSTTime(); //get current PST Time
					Log.message("Actual SheetName "+FirstSheetName);
					Log.message("Actual Date "+DateField);
					Log.message("Expected Date  "+expectedDate);
					c=(((i-1)*20)+j)-1;  // Get every sheet index  no per page

					if((allsheetsCollection.containsValue(FirstSheetName))&&(DateField.contains(expectedDate)))
					{
						fieldValidationSuccessCount=fieldValidationSuccessCount+1;		
						Log.message("PASS: Sheet Name and Date Field System generated Value validated for Sheet No :"+c);
					}

					else
					{
						Log.message("FAIL: Sheet Name and Date Field System generated Value Not Proper for Sheet No :"+c);
					}

				} //End of for loop

				//publish the page
				btnPublishNew.click();
				Log.message("Clicked on Publish now button.");
				SkySiteUtils.waitForElement(driver, btnInfoMsgYes, 30);
				btnInfoMsgYes.click();
				if(i==totalPageCount)
				{
					SkySiteUtils.waitForElement(driver, iconListView, 30);
					Log.message("Files publish is done.");
				}
				RemainingSheetCount=RemainingSheetCount-20;
			} //End of if(RemainingSheetCount>=20)

		} //End of For Loop
		// Validation in folder page after upload
		int cnt=1+SheetCount;
		String ActFileName=null;
		int uploadSuccessCount=0;
		for(int i=0;i<SheetCount;i++)
		{
			for(int n=2;n<=cnt;n++) 
			{
				ActFileName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li["+n+"]/section[2]/h4/span[1]")).getText(); 
				Log.message("Actual File name is:"+ActFileName);
				if((allsheetsCollection.get(i).toString()).equalsIgnoreCase(ActFileName.trim()))
				{	    			    	 
					uploadSuccessCount=uploadSuccessCount+1;
					Log.message("File uploded success for:" +ActFileName+" - Sucess Count:"+uploadSuccessCount);
					break;
				}
			}//End for(n) loop

		}//End for(i) loop

		Log.message("Total Upload Success Count is::-"+uploadSuccessCount);

		if((uploadSuccessCount==SheetCount)&&(fieldValidationSuccessCount==SheetCount))
		{
			Log.message("All sheets published Successfully!!!");
			driver.findElement(By.xpath("//a[@class='aProjHeaderLink']")).click();
			Log.message("Clicking on Project Name to navigate to folders list page");
			SkySiteUtils.waitTill(7000);
			result=true;	
		}
		else 
		{	
			Log.message("All sheets not  published Successfully!!!");
		}
		
		if (result == true) 
		{
			Log.message("Upload files using Index by File Name is working successfully!!!");
			return true;
		} else {
			Log.message("Upload files using Index by File Name is not working!!!");
			return false;
		}

	}
	
	/**
	 * Method written for upload files using Index By File Name
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 * @throws BiffException 
	 */
	
	public boolean UploadFiles_IndexBySheetNumber(String FolderPath, int FileCount,String sheetCount_FilePath)
			throws InterruptedException, AWTException, IOException, BiffException {
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnUploadfile, 30);
		Log.message("Waiting for upload file button to be appeared");
		SkySiteUtils.waitTill(3000);
		btnUploadfile.click();
		Log.message("Clicked on upload file button.");
		SkySiteUtils.waitForElement(driver, btnCloudAccount, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnChooseFile.click();
		Log.message("Clicked on choose file button.");
		SkySiteUtils.waitTill(10000);

		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(500);
			}
		}

		output.flush();
		output.close();

		// Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnUploadWithIndex, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(10000);
		
		//int sheetcount=0;
		String SheetNumber = null;
		
//Getting Expected sheet numbers from the excel and keeping in the map
		FileInputStream ExpFileNames = new FileInputStream(sheetCount_FilePath);
		Workbook FNumbers = Workbook.getWorkbook(ExpFileNames);

		Sheet s2 = FNumbers.getSheet(0);
		//int ColCount = s2.getColumns();//Getting Column count from Excel
		int RowCount = s2.getRows();//Getting Row count from Excel
		Log.message("Row Count is: "+RowCount);
		
		Map allsheetsCollection = new HashMap();
		
		for(int i=1;i<RowCount;i++)
		{
			SheetNumber=s2.getCell(0, i).getContents();
			Log.message("sheetnumber"+i+" is: "+SheetNumber);
			allsheetsCollection.put("sheetnumber"+i, SheetNumber);
		}
		
		Log.message("All sheet numbers are collected as: "+allsheetsCollection);
		//Thread.sleep(10000);
		driver.findElement(By.xpath("//button[@id='btnAutoOCR']")).click();
		Log.message("Clicked on Upload with index button");
		Thread.sleep(2000);//New
		/*SkySiteUtils.waitForElement(driver, notificationMsg, 30);
		String AfterFileUploads_NotiMsg=notificationMsg.getText();
		Log.message("Notification Msg is: "+AfterFileUploads_NotiMsg);
		if(AfterFileUploads_NotiMsg.contentEquals("Files processing is in progress."))
		{
			Log.message("Upload Process Completed Successfully");
		}*/
		SkySiteUtils.waitForElement(driver, linkPublishnow, 180);
		SkySiteUtils.waitTill(2000);
		linkPublishnow.click();
		Log.message("Clicked on Publish now link.");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, btnPublishNew, 30);
		SkySiteUtils.waitTill(15000);	

		// Delete the AutoIT temp file
		try {
			File file = new File(tmpFileName);
			if (file.delete()) {
				Log.message(file.getName() + " is deleted!");
			} else {
				Log.message("Delete operation is failed.");
			}
		} catch (Exception e) {
			Log.message("Exception occured!!!" + e);
		}
		
		txtRevisionInput.sendKeys("Revision 1");
		String DateField=infoRevisionDate.getAttribute("value");
		Log.message("Actual Date "+DateField);

//----------------------Publish page validations---------------------------
		int SheetCount =RowCount-1;
		int totalPageCount=0;
		int pgcount=0;
		int extra=0;
		pgcount=SheetCount/20;
		extra=SheetCount%20;
	
		if(( pgcount>0)&&(extra>0))//Multiple of 20 plus extra
		{     
			totalPageCount=pgcount+1;  		
		}
	
		if(( pgcount>0)&&(extra==0))//Multiple of 20 only
		{	
			totalPageCount=pgcount;   		
		}

		if(( pgcount==0)&&(extra<20))//less than  20 only
		{
			totalPageCount=1;	   		
		}	
	
		Log.message("Total Page Count is:"+totalPageCount);        	

		int RemainingSheetCount=SheetCount;
		Log.message("Remining sheet count is: "+RemainingSheetCount);
		Log.message("Total Page Count is: "+totalPageCount);
		//allsheetsCollection.containsValue(arg0);
		int c=0;
		int fieldValidationSuccessCount=0;
		for(int i=1;i<=totalPageCount;i++)
		{
			if(RemainingSheetCount<20)
			{
				Log.message("Inside the condition of Remaining Sheet cout < 20");
				for(int j=1;j<=RemainingSheetCount;j++)
				{
					String FirstSheetNumber = driver.findElement(By.xpath("(//input[@id='SheetName'])["+j+"]")).getAttribute("value");
					//driver.findElement(By.xpath("(//textarea[@class='form-control input-box txtDescription'])["+j+"]")).click();
					//driver.findElement(By.xpath("(//textarea[@class='form-control input-box txtDescription'])["+j+"]")).sendKeys(SheetDescription); //Enter Description
					Thread.sleep(2000);		
					Log.message("Actual SheetName "+FirstSheetNumber);
					Log.message("Actual Date "+DateField);
					//Log.message("Expected Date  "+expectedDate);
					c=(((i-1)*20)+j)-1;  // Get every sheet index  no per page

					if((allsheetsCollection.containsValue(FirstSheetNumber)) 
						&& (driver.findElement(By.xpath("//button[@type='button' and @value='Delete']")).isDisplayed()))
					{
						fieldValidationSuccessCount=fieldValidationSuccessCount+1;	
						Log.message("PASS: Sheet Number Field System generated Value validated for Sheet No :"+c);
					}
					else
					{
						Log.message("FAIL: Sheet Number Field System generated Value Not Proper for Sheet No :"+c);
					}
				}//End of for(J) loop
				
				Log.message("field Validation Success Count is: "+fieldValidationSuccessCount);
				btnPublishNew.click();
				Log.message("Clicked on Publish button.");
				SkySiteUtils.waitForElement(driver, btnInfoMsgYes, 30);
				btnInfoMsgYes.click();
				
				if(i==totalPageCount)
				{
					SkySiteUtils.waitForElement(driver, iconListView, 30);
					Log.message("Files publish is done.");
				}
					RemainingSheetCount=RemainingSheetCount-20;
					Log.message("Remaining Sheet Count is: "+RemainingSheetCount);
			}

			if(RemainingSheetCount>=20)
			{
				for(int j=1;j<=20;j++)
				{

				Thread.sleep(2000);
				String FirstSheetNumber = driver.findElement(By.xpath("(//input[@id='SheetName'])["+j+"]")).getAttribute("value");
				//driver.findElement(By.xpath("(//textarea[@class='form-control input-box txtDescription'])["+j+"]")).click();
				//driver.findElement(By.xpath("(//textarea[@class='form-control input-box txtDescription'])["+j+"]")).sendKeys(SheetDescription); //Enter Description
				//Thread.sleep(2000);		
				//String expectedDate=getCurrentDate.getPSTTime(); // get current PST Time
				Log.message("Actual SheetName "+FirstSheetNumber);
				//Log.message("Actual Date "+DateField);
				//Log.message("Expected Date  "+expectedDate);
				c=(((i-1)*20)+j)-1;  // Get every sheet index  no per page

				if((allsheetsCollection.containsValue(FirstSheetNumber)))
				{
					fieldValidationSuccessCount=fieldValidationSuccessCount+1;		
					Log.message("PASS: Sheet Number Field System generated Value validated for Sheet No :"+c);
				}
				else
				{
					Log.message("FAIL: Sheet Number Field System generated Value Not Proper for Sheet No :"+c);
				}

				}//For loop for J

				//publish the page
				btnPublishNew.click();
				Log.message("Clicked on Publish button.");
				SkySiteUtils.waitForElement(driver, btnInfoMsgYes, 30);
				btnInfoMsgYes.click();

				if(i==totalPageCount)
				{
			
					SkySiteUtils.waitForElement(driver, iconListView, 30);
					Log.message("Files publish is done.");
				}
				RemainingSheetCount=RemainingSheetCount-10;
			}

		}

	// Validation in folder page after upload
		int cnt=RowCount;
		String ActFileName=null;
		int uploadSuccessCount=0;
		for(int i=2;i<=RowCount;i++)
		{
			ActFileName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li["+i+"]/section[2]/h4/span[1]")).getText(); 
			Log.message("Actual File name is:"+ActFileName);
			if((allsheetsCollection.containsValue(ActFileName.trim())))
			{	    			    	 
				uploadSuccessCount=uploadSuccessCount+1;
				Log.message("File uploded success for:" +ActFileName+" - Sucess Count:"+uploadSuccessCount);
			}

		}//End for(i) loop

		Log.message("Total Upload Success Count is::-"+uploadSuccessCount);
		if((uploadSuccessCount==RowCount-1)&&(fieldValidationSuccessCount==RowCount-1))
		{
			driver.findElement(By.xpath("//a[@class='aProjHeaderLink']")).click();
			Log.message("Clicking on Project Name to navigate to folders list page");
			SkySiteUtils.waitTill(7000);
			result=true;	
		}
		else 
		{	
			Log.message("All sheets not  published Successfully!!!");
		}
		if (result == true) 
		{
			Log.message("Upload files using Index by Sheet Number is working successfully!!!");
			return true;
		} else {
			Log.message("Upload files using Index by Sheet Number is not working!!!");
			return false;
		}

	}
	
	
	/**
	 * Method written for Recurring upload files using without index 
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */

	@SuppressWarnings("finally")
	public boolean Recurring_UploadFiles_WithoutIndex(String FolderPath, int FileCount)
			throws InterruptedException, AWTException, IOException 
	{
		boolean result = false;
		
		try
		{
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, btnUploadfile, 30);
			Log.message("Waiting for upload file button to be appeared");
			SkySiteUtils.waitTill(3000);
			btnUploadfile.click();// Click on Upload file
			Log.message("Clicked on upload file button.");
			SkySiteUtils.waitForElement(driver, btnCloudAccount, 30);
			Log.message("Waiting for Cloud Account button to be appeared");
			SkySiteUtils.waitTill(5000);
			btnChooseFile.click();// Click on Choose File
			Log.message("Clicked on choose file button.");
			SkySiteUtils.waitTill(10000);

			// Writing File names into a text file for using in AutoIT Script
			BufferedWriter output;
			randomFileName rn = new randomFileName();
			//String tmpFileName = "c:/" + rn.nextFileName() + ".txt";
			//String tmpFileName = "c:/AutoIT_TempFile/" + rn.nextFileName() + ".txt";
			String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
			output = new BufferedWriter(new FileWriter(tmpFileName, true));
			String expFilename = null;
			File[] files = new File(FolderPath).listFiles();
			for (File file : files) {
				if (file.isFile()) {
					expFilename = file.getName();// Getting File Names into a
													// variable
					Log.message("Expected File name is:" + expFilename);
					output.append('"' + expFilename + '"');
					output.append(" ");
					SkySiteUtils.waitTill(1000);
				}
			}

			output.flush();
			output.close();

			// Executing .exe autoIt file
			String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
			Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
			Log.message("AutoIT Script Executed!!");
			SkySiteUtils.waitTill(10000);

			SkySiteUtils.waitForElement(driver, btnUploadWithoutIndex, 30);
			Log.message("Waiting for Cloud Account button to be appeared");
			SkySiteUtils.waitTill(10000);
			// Delete the temp file
			try {
				File file = new File(tmpFileName);
				if (file.delete()) {
					Log.message(file.getName() + " is deleted!");
				} else {
					Log.message("Delete operation is failed.");
				}
			} catch (Exception e) {
				Log.message("Exception occured!!!" + e);
			}
			SkySiteUtils.waitTill(10000);
			btnUploadWithoutIndex.click();// Click on Upload without index
			Log.message("Clicked on Upload without index button.");
			SkySiteUtils.waitTill(5000);
		/*	SkySiteUtils.waitForElement(driver, notificationMsg, 180);
			Log.message("Waiting for Notification Message to be appeared");
			String UploadSuccess_Message = notificationMsg.getText();
			Log.message("Notification Message after upload is: " + UploadSuccess_Message);
			SkySiteUtils.waitTill(5000);*/
			SkySiteUtils.waitForElement(driver, linkProcessCompleted, 300);
			linkProcessCompleted.click();
			Log.message("Clicked on Process Completed Link.");
			SkySiteUtils.waitTill(10000);

			result = this.ValidateUploadFiles(FolderPath, FileCount);// Calling validate files method

			if (result == true) {
				driver.findElement(By.xpath("//a[@class='aProjHeaderLink']")).click();
				SkySiteUtils.waitTill(10000);
				Log.message("Clicked on project header.");
				Log.message("Upload files using donot index is working successfully!!!");
				return true;
			} else {
				Log.message("Upload files using donot index is not working!!!");
				return false;
			}
		}
		catch(Exception e)
		{
			AnalyzeLog.analyzeLog(driver);
			e.printStackTrace();
		}
		finally
		{
			return result;
		}
	
	}
	
	/**
	 * Method written for Select a File - open file in viewer _ Validate
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_Select_Expected_File_ValidateInViewer(String File_Name)throws InterruptedException, AWTException, IOException 
	{
		SkySiteUtils.waitTill(10000);		
		int File_Count=0; 
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Doc_')]")); 
		for (WebElement Element : allElements) 
		{ 
			File_Count = File_Count+1; 	
		} 
		Log.message("Count of available files in a folder is: "+File_Count);
	  //Taking file names by passing variable into x_path
		String actualFileName=null;
		String winHandleBefore=null;
		for(int j=2;j<=File_Count+1;j++)
		{
			actualFileName = driver.findElement(By.xpath("//li["+j+"]/section[2]/h4")).getText();	
			Log.message("File Name from Application is: "+actualFileName);
		
			if(actualFileName.equalsIgnoreCase(File_Name))
			{	
				driver.findElement(By.xpath("//li["+j+"]/section[1]/a")).click();
				Log.message("Expected file is available....selected to open in viewer.");
				SkySiteUtils.waitTill(120000);
				winHandleBefore = driver.getWindowHandle();
				for(String winHandle : driver.getWindowHandles())
				{
					  driver.switchTo().window(winHandle);
				}
				SkySiteUtils.waitForElement(driver, Viewer_DropPlace, 120);
				break;
			}
		}		
		if(Viewer_DropPlace.isDisplayed())
		{
			SkySiteUtils.waitTill(20000);
			driver.close();
			SkySiteUtils.waitTill(3000);
			driver.switchTo().window(winHandleBefore);
			SkySiteUtils.waitTill(3000);
			Log.message("Selecting an expected file is working successfully!!!");
			return true;
		}
		else
		{
			Log.message("Selecting an expected file is NOT working!!!");
			return false;
		}
		
	}
	
	/**
	 * Method written for Open a File in viewer
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Open_Expected_File_InViewer(String File_Name)throws InterruptedException, AWTException, IOException 
	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);		
		int File_Count=0; 
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Doc_')]")); 
		for (WebElement Element : allElements) 
		{ 
			File_Count = File_Count+1; 	
		} 
		Log.message("Count of available files in a folder is: "+File_Count);
	  //Taking file names by passing variable into x_path
		String actualFileName=null;
		for(int j=2;j<=File_Count+1;j++)
		{
			actualFileName = driver.findElement(By.xpath("//li["+j+"]/section[2]/h4")).getText();	
			Log.message("File Name from Application is: "+actualFileName);
		
			if(actualFileName.equals(File_Name))
			{	
				driver.findElement(By.xpath("//li["+j+"]/section[1]/a")).click();
				Log.message("Expected file is available....selected to open in viewer.");
				SkySiteUtils.waitTill(30000);
				result=true;
				//String winHandleBefore = driver.getWindowHandle();
				for(String winHandle : driver.getWindowHandles())
				{
					  driver.switchTo().window(winHandle);
				}
				SkySiteUtils.waitForElement(driver, Viewer_DropPlace, 120);
				break;
			}
		}		
		if(result==true)
		{
			Log.message("Selecting an expected file is working successfully!!!");
			return true;
		}
		else
		{
			Log.message("Selecting an expected file is NOT working!!!");
			return false;
		}
		
	}
	
	public boolean UploadFilesWithoutIndex(String FolderPath, int FileCount)
			throws InterruptedException, AWTException, IOException {
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnUploadfile, 30);
		Log.message("Waiting for upload file button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnUploadfile.click();// Click on Upload file
		Log.message("Clicked on upload file button.");
		SkySiteUtils.waitForElement(driver, btnCloudAccount, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnChooseFile.click();// Click on Choose File
		Log.message("Clicked on choose file button.");
		SkySiteUtils.waitTill(15000);

		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a
												// variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(1000);
			}
		}

		output.flush();
		output.close();
		SkySiteUtils.waitTill(5000);
		// Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(10000);

		SkySiteUtils.waitForElement(driver, btnUploadWithoutIndex, 30);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(10000);
		// Delete the temp file
		try {
			File file = new File(tmpFileName);
			if (file.delete()) {
				Log.message(file.getName() + " is deleted!");
			} else {
				Log.message("Delete operation is failed.");
			}
		} catch (Exception e) {
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(10000);
		btnUploadWithoutIndex.click();// Click on Upload without index
		Log.message("Clicked on Upload without index button.");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, notificationMsg, 180);
		Log.message("Waiting for Notification Message to be appeared");
		String UploadSuccess_Message = notificationMsg.getText();
		Log.message("Notification Message after upload is: " + UploadSuccess_Message);
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, linkProcessCompleted, 180);
		linkProcessCompleted.click();
		Log.message("Clicked on Process Completed Link.");
		SkySiteUtils.waitTill(20000);

		result = this.ValidateUploadFiles(FolderPath, FileCount);

		if (result == true) {
			//driver.findElement(By.xpath("//a[@class='aProjHeaderLink']")).click();
			SkySiteUtils.waitTill(10000);
			Log.message("Clicked on project header.");
			Log.message("Upload files using donot index is working successfully!!!");
			return true;
		} else {
			Log.message("Upload files using donot index is not working!!!");
			return false;
		}

	}

	
	
	// scripted By Ranjan 
	
	public boolean Upload_WithoutIndex(String FolderPath, int FileCount)throws InterruptedException, AWTException, IOException {
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnUploadfile, 60);
		Log.message("Waiting for upload file button to be appeared");
		SkySiteUtils.waitTill(500);
		btnUploadfile.click();// Click on Upload file
		Log.message("Clicked on upload file button.");
		SkySiteUtils.waitForElement(driver, btnCloudAccount, 60);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitForElement(driver, btnChooseFile, 60);
		SkySiteUtils.waitTill(5000);
		btnChooseFile.click();
		/*JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", btnChooseFile);*/
		Log.message("Clicked on choose file button.");
		SkySiteUtils.waitTill(5000);
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String Sys_Download_Path= "./File_Download_Location/"+ rn.nextFileName()+".txt" ;
  		Log.message("system download location2"+Sys_Download_Path);
		String tmpFileName = Sys_Download_Path;
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;
		
		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(5000);
			}
		}

		output.flush();
		output.close();
		SkySiteUtils.waitTill(1000);
		// Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		File dest = new File(FolderPath);
		String path = dest.getAbsolutePath();
		SkySiteUtils.waitTill(5000);
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + path + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(15000);
		SkySiteUtils.waitForElement(driver, btnUploadWithoutIndex, 60);		
		SkySiteUtils.waitTill(5000);
		// Delete the temp file
		try {
			File file = new File(tmpFileName);
			if (file.delete()) {
				Log.message(file.getName() + " is deleted!");
			} else {
				Log.message("Delete operation is failed.");
			}
		} catch (Exception e) {
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(5000);
		btnUploadWithoutIndex.click();// Click on Upload without index
		Log.message("Clicked on Upload without index button.");
		//SkySiteUtils.waitTill(500);
		//SkySiteUtils.waitForElement(driver, notificationMsg, 180);
		//Log.message("Waiting for Notification Message to be appeared");
		//String UploadSuccess_Message = notificationMsg.getText();
		//Log.message("Notification Message after upload is: " + UploadSuccess_Message);
		SkySiteUtils.waitTill(3000);
		
		return result = true;			
	
        }


	public boolean UploadFiles_WithoutIndexMycomputer(String FolderPath, int FileCount)throws InterruptedException, AWTException, IOException {
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnUploadfile1, 60);
		Log.message("Waiting for upload file button to be appeared");
		SkySiteUtils.waitTill(200);
		btnUploadfile1.click();// Click on Upload file
		Log.message("Clicked on upload file button.");
		SkySiteUtils.waitForElement(driver, btnMycomputer, 60);
		btnMycomputer.click();
		Log.message("Clicked on Mycomputer button.");		
		SkySiteUtils.waitForElement(driver, btnChooseFile, 60);			
		SkySiteUtils.waitTill(5000);
		btnChooseFile.click();		
		Log.message("Clicked on choose file button.");
		SkySiteUtils.waitTill(500);

		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String Sys_Download_Path= "./File_Download_Location/"+ rn.nextFileName()+".txt" ;
  		Log.message("system download location2"+Sys_Download_Path);
		String tmpFileName = Sys_Download_Path;		
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a
												// variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(5000);
			}
		}

		output.flush();
		output.close();
		SkySiteUtils.waitTill(1000);
		// Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		File dest = new File(PropertyReader.getProperty("Upload_TestData_viewer_SingleFile").toString());
		String path = dest.getAbsolutePath();
		SkySiteUtils.waitTill(5000);
		
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + path + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnUploadWithoutIndex, 60);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(10000);
		// Delete the temp file
		try {
			File file = new File(tmpFileName);
			if (file.delete()) {
				Log.message(file.getName() + " is deleted!");
			} else {
				Log.message("Delete operation is failed.");
			}
		} catch (Exception e) {
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(10000);
		btnUploadWithoutIndex.click();// Click on Upload without index
		Log.message("Clicked on Upload without index button.");
		SkySiteUtils.waitTill(5000);
		//SkySiteUtils.waitForElement(driver, notificationMsg, 180);
		//Log.message("Waiting for Notification Message to be appeared");
		//String UploadSuccess_Message = notificationMsg.getText();
		//Log.message("Notification Message after upload is: " + UploadSuccess_Message);
		//SkySiteUtils.waitTill(5000);
		/*SkySiteUtils.waitForElement(driver, linkProcessCompleted, 180);
		linkProcessCompleted.click();
		Log.message("Clicked on Process Completed Link.");
		SkySiteUtils.waitTill(20000);
		result = this.ValidateUploadFiles(FolderPath, FileCount);

		if (result == true) {
			//driver.findElement(By.xpath("//a[@class='aProjHeaderLink']")).click();
			//SkySiteUtils.waitTill(10000);
			Log.message("Clicked on project header.");
			Log.message("Upload files using donot index is working successfully!!!");
			return true;
		} else {
			Log.message("Upload files using donot index is not working!!!");
			return false;
		}*/
		return result;

	}
	
	
	public boolean My_Mycomputer_upload()throws InterruptedException, AWTException, IOException {
		boolean result = false;	
	
	SkySiteUtils.waitTill(10000);
	SkySiteUtils.waitForElement(driver, btnUploadfile1, 120);
	//CommonMethod.fluentWait(btnUploadfile1, 120, 5);
	Log.message("Waiting for upload file button to be appeared");
	SkySiteUtils.waitTill(5000);
	btnUploadfile1.click();// Click on Upload file
	Log.message("Clicked on upload file button.");
	SkySiteUtils.waitForElement(driver, btnMycomputer, 120);
	//CommonMethod.fluentWait(btnMycomputer, 120, 5);
	btnMycomputer.click();
	Log.message("Clicked on Mycomputer button.");
	SkySiteUtils.waitTill(5000);
	return result;
	}
	
	//================ Scripted By ranjan=================================
	public boolean UploadFiles_WithoutIndex_Mycomputer(String FolderPath, int FileCount)
			throws InterruptedException, AWTException, IOException {
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnUploadfile1, 120);
		//CommonMethod.fluentWait(btnUploadfile1, 120, 2);
		Log.message("Waiting for upload file button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnUploadfile1.click();// Click on Upload file
		Log.message("Clicked on upload file button.");
		//CommonMethod.fluentWait(btnMycomputer, 120, 2);
		SkySiteUtils.waitForElement(driver, btnMycomputer, 120);
		btnMycomputer.click();
		Log.message("Clicked on Mycomputer button.");
		SkySiteUtils.waitForElement(driver, btnChooseFile, 120);
		//CommonMethod.fluentWait(btnChooseFile, 120, 2);
		Log.message("Waiting for Choose button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnChooseFile.click();// Click on Choose File
		Log.message("Clicked on choose file button.");
		SkySiteUtils.waitTill(15000);
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a
												// variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(1000);
			}
		}

		output.flush();
		output.close();

		// Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnUploadWithoutIndex, 120);
		//CommonMethod.fluentWait(btnUploadWithoutIndex, 120, 2);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(10000);
		// Delete the temp file
		try {
			File file = new File(tmpFileName);
			if (file.delete()) {
				Log.message(file.getName() + " is deleted!");
			} else {
				Log.message("Delete operation is failed.");
			}
		} catch (Exception e) {
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(10000);
		btnUploadWithoutIndex.click();// Click on Upload without index
		Log.message("Clicked on Upload without index button.");
		SkySiteUtils.waitForElement(driver, notificationMsg, 120);
		//CommonMethod.fluentWait(notificationMsg, 120, 2);
		Log.message("Waiting for Notification Message to be appeared");
		String UploadSuccess_Message = notificationMsg.getText();
		//Log.message("Notification Message after upload is: " + UploadSuccess_Message);
		/*SkySiteUtils.waitTill(5000);
		CommonMethod.fluentWait(linkProcessCompleted, 300, 5);
		linkProcessCompleted.click();
		Log.message("Clicked on Process Completed Link.");
		SkySiteUtils.waitTill(20000);

		result = this.ValidateUploadFiles(FolderPath, FileCount);

		if (result == true) {
			
			//SkySiteUtils.waitTill(10000);
			Log.message("Clicked on project header.");
			Log.message("Upload files using donot index is working successfully!!!");
			return true;
		} else {
			Log.message("Upload files using donot index is not working!!!");
			return false;
		}
*/
		return result;
	}

	/**
	 * Method written for upload photos in gallery 
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */

	@SuppressWarnings("deprecation")
	public boolean UploadPhotos_InGallery(String FolderPath, int FileCount)
			throws InterruptedException, AWTException, IOException {
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnUploadPhotos, 30);
		Log.message("Waiting for upload file button to be appeared");
		btnUploadPhotos.click();
		Log.message("Clicked on upload photos button.");
		SkySiteUtils.waitForElement(driver, headerMsgUploadPhotoWindow, 40);
		Log.message("Waiting for choose file button to be appeared");
		SkySiteUtils.waitTill(5000);
		btnChooseFile.click();
		Log.message("Clicked on choose file button.");
		SkySiteUtils.waitTill(5000);
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(1000);
			}
		}

		output.flush();
		output.close();

		// Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnFileUpload, 60);
		Log.message("Waiting for File Upload button to be appeared");
		SkySiteUtils.waitTill(10000);
		// Delete the temp file
		try {
			File file = new File(tmpFileName);
			if (file.delete()) {
				Log.message(file.getName() + " is deleted!");
			} else {
				Log.message("Delete operation is failed.");
			}
		} catch (Exception e) {
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(5000);
		btnFileUpload.click();
		Log.message("Clicked on Upload button.");
		SkySiteUtils.waitForElement(driver, btnSaveAttributes, 180);
		Log.message("Waiting for Save attributes button to be appeared.");
		SkySiteUtils.waitTill(5000);

		// Validations on Add Attributes page
		String Photo_Information = driver.findElement(By.xpath("//h4[@class='pull-left']")).getText();//Getting count
		Log.message(Photo_Information);
		// Split the line and get the count
		Splitter SPLITTER = Splitter.on(CharMatcher.whitespace()).omitEmptyStrings();
		List<String> elements = Lists.newArrayList(SPLITTER.split(Photo_Information));
		// Getting count - output
		String OnlyCount = elements.get(3);
		Log.message("The required count value is: " + OnlyCount);
		// Convert String to int
		int PhotosCount = Integer.parseInt(OnlyCount);
		int PageCount = PhotosCount / 10;
		int Extra_Photo = PhotosCount % 10;
		int totalPageCount = 0;

		// Getting Page count
		if ((PageCount > 0) && (Extra_Photo > 0))// Multiple of 10 plus extra
		{
			totalPageCount = PageCount + 1;
		}

		if ((PageCount > 0) && (Extra_Photo == 0))// Multiple of 10 only
		{
			totalPageCount = PageCount;
		}

		if ((PageCount == 0) && (Extra_Photo < 10))// less than 10 only
		{
			totalPageCount = 1;
		}

		Log.message("Total Page Count is:" + totalPageCount);

		for (int i = 1; i <= totalPageCount; i++) {
			if ((PhotosCount < 10) && (PhotosCount == FileCount)) {
				// Fill the Attributes and use the copy down
				driver.findElement(By.xpath("(//input[@placeholder='Building'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Building'])[1]")).sendKeys("ARC");
		
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[1]")).click();
				
				driver.findElement(By.xpath("(//input[@placeholder='Level'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Level'])[1]")).sendKeys("7th_Leve");
				
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[2]")).click();
			
				driver.findElement(By.xpath("(//input[@placeholder='Room'])[1]")).clear();
			
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[3]")).click();
				
				driver.findElement(By.xpath("(//input[@placeholder='Area'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Area'])[1]")).sendKeys("SectorV");
			
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[4]")).click();
			
				driver.findElement(By.xpath("(//input[@placeholder='Description'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Description'])[1]")).sendKeys("Kolkata_Branch");
			
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[5]")).click();
				SkySiteUtils.waitTill(5000);
				driver.findElement(By.xpath(".//*[@id='divAttrContainer']/div[1]/div[2]/div/div[2]/button")).click();
				SkySiteUtils.waitTill(5000);

			}
			if ((PhotosCount >= 10) && (PhotosCount == FileCount)) {
				// Fill the Attributes and use the copy down
				driver.findElement(By.xpath("(//input[@placeholder='Building'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Building'])[1]")).sendKeys("ARC");
				
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[1]")).click();
			
				driver.findElement(By.xpath("(//input[@placeholder='Level'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Level'])[1]")).sendKeys("7th_Leve");
		
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[2]")).click();
				driver.findElement(By.xpath("(//input[@placeholder='Room'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Room'])[1]")).sendKeys("Room_3");
				
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[3]")).click();
			
				driver.findElement(By.xpath("(//input[@placeholder='Area'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Area'])[1]")).sendKeys("SectorV");
				
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[4]")).click();
				
				driver.findElement(By.xpath("(//input[@placeholder='Description'])[1]")).clear();
				driver.findElement(By.xpath("(//input[@placeholder='Description'])[1]")).sendKeys("Kolkata_Branch");
				
				driver.findElement(By.xpath("(//i[@class='icon icon-level-down icon-lg'])[5]")).click();
				SkySiteUtils.waitTill(5000);
				driver.findElement(By.xpath(".//*[@id='divAttrContainer']/div[1]/div[2]/div/div[2]/button")).click();
				SkySiteUtils.waitTill(5000);
			}

		} // end of i for loop
		SkySiteUtils.waitTill(10000);

		// Calling Upload Photo validation script
		result = this.ValidateUploadPhotos(FolderPath, FileCount);// Calling validate photos method
		if (result == true) {
			Log.message("Photos Upload is working successfully!!!");
			return true;
		} else {
			Log.message("Photos Upload is not working!!!");
			return false;
		}

	}

	/**
	 * Method written for Validate Download by select multiple files - List View
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean ValidateDownload_BySelectMultFiles(String Sys_Download_Path,String Foldername) throws InterruptedException, AWTException {
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, iconListView, 30);
		Log.message("Waiting for List View button to be appeared");
		SkySiteUtils.waitTill(5000);
		iconListView.click();
		Log.message("Clicked on List View button.");
		SkySiteUtils.waitForElement(driver, chkboxAllFileSel, 30);
		Log.message("Waiting for select all files checkbox to be appeared");
		// Counting number of files available
		int Avl_File_Count = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'chkElements')]"));
		for (WebElement Element : allElements) {
			Avl_File_Count = Avl_File_Count + 1;
		}
		Log.message("Availble number of files in folder is: " + Avl_File_Count);
		SkySiteUtils.waitTill(10000);
		// Select first two files from the list
		if (Avl_File_Count >= 2) {
			for (int i = 1; i <= 2; i++) {
				// Log.message("Select a checkbox");
				driver.findElement(By.xpath("(//input[@class='chkElements chkfileelements'])[" + i + "]")).click();
			}
		}
		SkySiteUtils.waitTill(2000);

		// Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(5000);
		btnDropdownMultFileSel.click();
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("//a[@id='aDownload']")).click();
		Log.message("Clicked on Download selected documents.");
		SkySiteUtils.waitTill(40000);

		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);

		if (browserName.contains("firefox")) {
			// Handling Download PopUp of firefox browser using robot
			Robot robot = null;
			robot = new Robot();

			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(60000);
		}

		// After Download, checking whether Downloaded file is existed under
		// download folder or not
		String ActualFilename = null;
		File[] files = new File(Sys_Download_Path).listFiles();

		for (File file : files) {
			if (file.isFile()) {
				ActualFilename = file.getName();// Getting File Names into a
												// variable
				Log.message("Actual File name is:" + ActualFilename);
				SkySiteUtils.waitTill(1000);
				Long ActualFileSize = file.length();
				Log.message("Actual File size is:" + ActualFileSize);
				if ((ActualFilename.contains("2-files-" + Foldername)) && (ActualFileSize != 0)) {
					Log.message("Downloaded ZIP file is available in downloads!!!");
					result = true;
					break;
				}

			}

		}

		iconGridView.click();// Click on grid view
		SkySiteUtils.waitTill(5000);
		if (result == true)
			return true;
		else
			return false;
	}
	
	/**
	 * Method written for Validate select all files - Grid View
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean Validate_SelectAllFiles_GridView() throws InterruptedException, AWTException {
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnSelect_Gridview, 30);
	
		//Get The Count of available files under a folder
		int Avl_File_Count = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Doc_')]"));
		for (WebElement Element : allElements) {
			Avl_File_Count = Avl_File_Count + 1;
		}
		Log.message("Available File Count is: " + Avl_File_Count);
		btnSelect_Gridview.click();
		Log.message("Clicked on Select button.");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, notificationMsg, 30);
		String NotifyMsg_AfterClickSelect = notificationMsg.getText();
		Log.message("Notify Msg_After Click Select ia: "+NotifyMsg_AfterClickSelect);
		SkySiteUtils.waitForElement(driver, btnSelectAllFiles_Gridview, 30);
		SkySiteUtils.waitTill(5000);
		btnSelectAllFiles_Gridview.click();
		Log.message("Clicked on Select All Files button.");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, infoNoOfFilesSelected, 30);
		String infoSelectedFiles = infoNoOfFilesSelected.getText();
		Log.message("Info After Select Files is: "+infoSelectedFiles);
		SkySiteUtils.waitForElement(driver, btnDropdownMultFileSel, 30);
		SkySiteUtils.waitTill(2000);
		if ((NotifyMsg_AfterClickSelect.contentEquals("You are in select mode. Please press 'Back' to exit"))
				&& (infoSelectedFiles.contentEquals(Avl_File_Count+"  files are selected.")))
		{
			Log.message("Select all files is working in gridview");
			return true;
		}
		else
		{
			Log.message("Select all files is NOT working in gridview");
			return false;
		}
	}
	
	/**
	 * Method written for Validate Download by select multiple files - Grid View
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	@FindBy(css=".de-selAll-gridview>a")
	WebElement btnDeSelectAll_GridView;
	public boolean ValidateDownload_BySelectMultFiles_GridView(String Sys_Download_Path,String Foldername) throws InterruptedException, AWTException {
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnDeSelectAll_GridView, 30);
	
		//Get The Count of available files under a folder
		int Avl_File_Count = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Doc_')]"));
		for (WebElement Element : allElements) {
			Avl_File_Count = Avl_File_Count + 1;
		}
		Log.message("Available File Count is: " + Avl_File_Count);
		// Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(5000);
		btnDropdownMultFileSel.click();
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//a[@id='aDownload']")).click();
		Log.message("Clicked on Download selected documents.");
		SkySiteUtils.waitTill(40000);

		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);

		if (browserName.contains("firefox")) {
			// Handling Download PopUp of firefox browser using robot
			Robot robot = null;
			robot = new Robot();

			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(60000);
		}

		// After Download, checking whether Downloaded file is existed under
		// download folder or not
		String ActualFilename = null;
		File[] files = new File(Sys_Download_Path).listFiles();

		for (File file : files) {
			if (file.isFile()) {
				ActualFilename = file.getName();// Getting File Names into a
												// variable
				Log.message("Actual File name is:" + ActualFilename);
				SkySiteUtils.waitTill(1000);
				Long ActualFileSize = file.length();
				Log.message("Actual File size is:" + ActualFileSize);
				if ((ActualFilename.contains(Avl_File_Count+"-files-"+Foldername)) && (ActualFileSize != 0)) {
					Log.message("Downloaded ZIP file is available in downloads!!!");
					result = true;
					break;
				}

			}

		}

		if (result == true)
		{
			Log.message("Download is working by select multiple files in gridview");
			return true;
		}
		else
		{
			Log.message("Download is NOT working by select multiple files in gridview");
			return false;
		}
	}
	
	/**
	 * Method written for Validate Download Folder from send link
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean ValidateDownload_FolderFromLink(String Sys_Download_Path,String Foldername) throws InterruptedException, AWTException {
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnToatalZipDownload, 30);
		Log.message("Waiting for download button to be appeared from sendlink URL");
		String FoldName_URL = eleFoldNameURL.getText();
		Log.message("Folder from url is: " + FoldName_URL);

		// Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(10000);

		if((FoldName_URL).trim().contentEquals(Foldername))
		{
			btnToatalZipDownload.click();
			Log.message("Clicked on Folder download button.");
			SkySiteUtils.waitTill(30000);
			// Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: " + browserName);

			if (browserName.contains("firefox")) {
				// Handling Download PopUp of firefox browser using robot
				Robot robot = null;
				robot = new Robot();

				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(30000);
			}

			// After Download, checking whether Downloaded file is existed under
			// download folder or not
			String ActualFilename = null;
			File[] files = new File(Sys_Download_Path).listFiles();

			for (File file : files) 
			{
				if (file.isFile()) 
				{
					ActualFilename = file.getName();// Getting File Names into a variable
					Log.message("Actual File name is: " + ActualFilename);
					SkySiteUtils.waitTill(1000);
					Long ActualFileSize = file.length();
					Log.message("Actual File size is:" + ActualFileSize);
					if ((ActualFilename.contains(FoldName_URL.trim())) && (ActualFileSize != 0)) 
					{
						Log.message("Downloaded Zip Folder available in downloads!!!");
						result = true;
						break;
					}

				}

			}
		}

		if (result == true)
			return true;
		else
			return false;
	}

	/**
	 * Method written for Validate Download single file from file send link url
	 * Scripted By: Naresh Babu KAVURU
	 * 
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean Download_SingleFile_SendLinkForAFIle(String Sys_Download_Path,String FileName) throws InterruptedException, AWTException {
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnToatalZipDownload, 50);
		Log.message("Waiting for download button to be appeared from sendlink URL");
		String Act_FileName = driver.findElement(By.xpath("(//div[1]/h4)[2]")).getText();
		Log.message("File from url is: " + Act_FileName);

		// Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(10000);

		if (Act_FileName != null) {
			btnSingFileDownload.click();// Click on Download file
			Log.message("Clicked on single file download button.");
			SkySiteUtils.waitTill(20000);
			// Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: " + browserName);

			if (browserName.contains("firefox")) {
				// Handling Download PopUp of firefox browser using robot
				Robot robot = null;
				robot = new Robot();

				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(30000);
			}

			// After Download, checking whether Downloaded file is existed under download folder or not
			String ActualFilename = null;
			File[] files = new File(Sys_Download_Path).listFiles();

			for (File file : files) {
				if (file.isFile()) {
					ActualFilename = file.getName();// Getting File Names into a
													// variable
					Log.message("Actual File name is:" + ActualFilename);
					SkySiteUtils.waitTill(1000);
					Long ActualFileSize = file.length();
					Log.message("Actual File size is:" + ActualFileSize);
					if ((ActualFilename.contains(FileName)) && (ActualFileSize != 0)) {
						Log.message("Downloaded file is available in downloads!!!");
						result = true;
						break;
					}

				}

			}
		}

		if (result == true)
			return true;
		else
			return false;

	}

	/**
	 * Method written for Validate Download single file under sub folder from send link url 
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	// Download Single File from send link URL Page - Use for file send link
	public boolean Download_SingleFile_SendLinkSubFold(String Sys_Download_Path,String FileName) throws InterruptedException, AWTException 
	{
		boolean result = false;
		SkySiteUtils.waitForElement(driver, btnToatalZipDownload, 50);
		Log.message("Waiting for download button to be appeared from sendlink URL");
		String Act_FileName = driver.findElement(By.xpath(".//*[@id='sharedDocsFolderList']/li[1]/div/div[1]/h4")).getText();
		Log.message("File from url is: " + Act_FileName);

		// Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(5000);

		if (Act_FileName != null) 
		{
			driver.findElement(By.xpath("(//button[@id='downldFile'])[1]")).click();
			Log.message("Clicked on single file download button.");
			SkySiteUtils.waitTill(20000);
			// Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: " + browserName);

			if (browserName.contains("firefox")) {
				// Handling Download PopUp of firefox browser using robot
				Robot robot = null;
				robot = new Robot();

				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(30000);
			}

			// After Download, checking whether Downloaded file is existed under download folder or not
			String ActualFilename = null;
			File[] files = new File(Sys_Download_Path).listFiles();

			for (File file : files) {
				if (file.isFile()) {
					ActualFilename = file.getName();// Getting File Names into a
													// variable
					Log.message("Actual File name is:" + ActualFilename);
					SkySiteUtils.waitTill(1000);
					Long ActualFileSize = file.length();
					Log.message("Actual File size is:" + ActualFileSize);
					if ((ActualFilename.contains(FileName)) && (ActualFileSize != 0)) {
						Log.message("Downloaded file is available in downloads!!!");
						result = true;
						break;
					}

				}

			}
		}

		if (result == true)
			return true;
		else
			return false;

	}

	/**
	 * Method written for Validate Download Sub Folder from Folder send link url
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	// Download Single File from send link URL Page - Use for file send link
	public boolean Download_SubFolder_FoldSendLink(String Sys_Download_Path,String Sub_Fold_Name) throws InterruptedException, AWTException {
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnToatalZipDownload, 50);
		Log.message("Waiting for download button to be appeared from sendlink URL");
		String Sub_FoldName_URL = driver.findElement(By.xpath("//a[@id='shdFolder']")).getText();
		Log.message("Sub Folder from url is: " + Sub_FoldName_URL);

		// Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(10000);

		if (Sub_FoldName_URL != null) {
			btnSingFileDownload.click();// Click on Download file
			Log.message("Clicked on Sub Folder download button.");
			SkySiteUtils.waitTill(30000);
			// Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: " + browserName);

			if (browserName.contains("firefox")) {
				// Handling Download PopUp of firefox browser using robot
				Robot robot = null;
				robot = new Robot();

				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(30000);
			}

			// After Download, checking whether Downloaded file is existed under
			// download folder or not
			String ActualFilename = null;
			File[] files = new File(Sys_Download_Path).listFiles();

			for (File file : files) {
				if (file.isFile()) {
					ActualFilename = file.getName();// Getting File Names into a
													// variable
					Log.message("Actual File name is:" + ActualFilename);
					SkySiteUtils.waitTill(1000);
					Long ActualFileSize = file.length();
					Log.message("Actual File size is:" + ActualFileSize);
					if ((ActualFilename.contains(Sub_Fold_Name)) && (ActualFileSize != 0)) {
						Log.message("Downloaded Subfolder Zip File available in downloads!!!");
						result = true;
						break;
					}

				}

			}
		}

		if (result == true)
			return true;
		else
			return false;

	}

	/**
	 * Method written for Validate Direct Zip download for multiple file sendlink url 
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	// Download total zip File from send link URL Page
	public boolean Download_ZipFile_SendLinkForMultFIle(String Sys_Download_Path,String Exp_Zip_Name) throws InterruptedException, AWTException {
		boolean result = false;
		SkySiteUtils.waitForElement(driver, btnToatalZipDownload, 50);
		Log.message("Waiting for download button to be appeared from sendlink URL");
		String Act_FileName = driver.findElement(By.xpath("(//div[1]/h4)[2]")).getText();
		Log.message("File from url is: " + Act_FileName);

		// Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(5000);

		if (Act_FileName != null) 
		{
			btnToatalZipDownload.click();// Click on Download file
			Log.message("Clicked on total zip download button.");
			SkySiteUtils.waitTill(60000);
			// Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: " + browserName);

			if (browserName.contains("firefox")) {
				// Handling Download PopUp of firefox browser using robot
				Robot robot = null;
				robot = new Robot();

				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(40000);
			}

			// After Download, checking whether Downloaded file is existed under download folder or not
			String ActualFilename = null;
			File[] files = new File(Sys_Download_Path).listFiles();

			for (File file : files) {
				if (file.isFile()) {
					ActualFilename = file.getName();// Getting File Names into a variable
					Log.message("Actual File name is:" + ActualFilename);
					SkySiteUtils.waitTill(1000);
					Long ActualFileSize = file.length();
					Log.message("Actual File size is:" + ActualFileSize);
					if ((ActualFilename.contains(Exp_Zip_Name)) && (ActualFileSize != 0)) {
						Log.message("Downloaded Zip file is available in downloads!!!");
						result = true;
						break;
					}

				}

			}
		}

		if (result == true)
			return true;
		else
			return false;

	}

	/**
	 * Method written for Validate Zip download for multiple file select from send link url 
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	// Download zip File by select more than one file from send link URL Page
	public boolean Download_ZipFile_SendLinkBySelectMultFIle(String Sys_Download_Path,String Exp_Zip_Name)throws InterruptedException, AWTException 
	{
		boolean result = false;
		SkySiteUtils.waitForElement(driver, btnToatalZipDownload, 50);
		Log.message("Waiting for download button to be appeared from sendlink URL");

		// Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(5000);

		// Counting number of files available
		int Avl_File_Count = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@type, 'checkbox')]"));
		for (WebElement Element : allElements) {
			Avl_File_Count = Avl_File_Count + 1;
		}
		Log.message("Availble number of files in url is: " + Avl_File_Count);
		if (Avl_File_Count >= 1) 
		{
			driver.findElement(By.xpath("(//input[@type='checkbox'])[1]")).click();// Select 1st checkbox
			driver.findElement(By.xpath("(//input[@type='checkbox'])[2]")).click();// Select 2nd checkbox
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, btnCanMultFileDownSelection, 20);

			SkySiteUtils.waitTill(5000);
			btnMultFileSelDownload.click();// Click on Multiple file zip Download
			Log.message("Clicked on total zip download button.");
			SkySiteUtils.waitTill(60000);
			// Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: " + browserName);

			if (browserName.contains("firefox")) {
				// Handling Download PopUp of firefox browser using robot
				Robot robot = null;
				robot = new Robot();

				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(40000);
			}

			// After Download, checking whether Downloaded file is existed under download folder or not
			String ActualFilename = null;
			File[] files = new File(Sys_Download_Path).listFiles();

			for (File file : files) {
				if (file.isFile()) {
					ActualFilename = file.getName();// Getting File Names into a variable
					Log.message("Actual File name is:" + ActualFilename);
					SkySiteUtils.waitTill(1000);
					Long ActualFileSize = file.length();
					Log.message("Actual File size is:" + ActualFileSize);
					if ((ActualFilename.contains(Exp_Zip_Name)) && (ActualFileSize != 0)) {
						Log.message("Downloaded Multiple file Zip is available in downloads!!!");
						result = true;
						break;
					}

				}

			}
		}

		if (result == true)
			return true;
		else
			return false;

	}
	
	/**
	 * Method written for Select a File - open file in viewer
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Verify_Select_Expected_File(String File_Name)throws InterruptedException, AWTException, IOException 
	{
		boolean result1 = false;
		SkySiteUtils.waitTill(10000);		
		int File_Count=0; 
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Doc_')]")); 
		for (WebElement Element : allElements) 
		{ 
			File_Count = File_Count+1; 	
		} 
		Log.message("Count of available files in a folder is: "+File_Count);
	  //Taking file names by passing variable into x_path
		String actualFileName=null;
		for(int j=2;j<=File_Count+1;j++)
		{
			actualFileName = driver.findElement(By.xpath("//li["+j+"]/section[2]/h4")).getText();	
			Log.message("File Name from Application is: "+actualFileName);
		
			if(actualFileName.equalsIgnoreCase(File_Name))
			{	
				Log.message("Expected file is available....select to open in viewer.");
				result1=true;
				driver.findElement(By.xpath("//li["+j+"]/section[1]/a")).click();	
				break;
			}
		}		
		if((result1==true))
		{
			Log.message("Selecting an expected file is working successfully!!!");
			return true;
		}
		else
		{
			Log.message("Selecting an expected file is NOT working!!!");
			return false;
		}
		
	}

	/**
	 * Method written for Validate Download for viewer level send link 
	 * Scipted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */

	public boolean ValidateDownload_ViewerLevel_SendLink(String Sys_Download_Path) throws InterruptedException, AWTException 
	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);

		// Counting number of files available
		int Avl_File_Count = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'chkElements')]"));
		for (WebElement Element : allElements) {
			Avl_File_Count = Avl_File_Count + 1;
		}
		Log.message("Availble number of files in folder is: " + Avl_File_Count);
		if (Avl_File_Count >= 1) {
			String Exp_File_Name = driver
					.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[2]/section[2]/h4")).getText();
			Log.message("First located file is: " + Exp_File_Name);
			thumbFirstlocatFile.click();// Select a file to view
			SkySiteUtils.waitTill(10000);
			String winHandleBefore = driver.getWindowHandle();
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			SkySiteUtils.waitForElement(driver, moreIconFileView, 30);
			Log.message("Waiting for List View button to be appeared in file view");
			SkySiteUtils.waitTill(10000);
			moreIconFileView.click();
			Log.message("Clicked on more option.");
			SkySiteUtils.waitTill(3000);
			tabSendLinkFileView.click();
			Log.message("Clicked on Send link tab.");
			SkySiteUtils.waitForElement(driver, btnSendFileView, 30);
			Log.message("Waiting for send link window");
			SkySiteUtils.waitTill(5000);
			String Act_SendLinkURL = txtCopyToClipboard.getAttribute("value");// Copy the URL
			Log.message("Generated Send Link is: " + Act_SendLinkURL);
			btnContact.click();
			Log.message("Clicked on contact-address book.");
			SkySiteUtils.waitForElement(driver, btnSelectContact, 30);
			Log.message("Waiting for Select Contact window");
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath("(//input[@class='pull-left chklist'])[1]")).click();// Select 1st contact check box
			btnSelectContact.click();// Click on select button
			SkySiteUtils.waitTill(5000);
			txtMessage.click();
			txtMessage.sendKeys("Testing from QA_Team @Automation");
			btnSendFileView.click();// Click on send
			SkySiteUtils.waitForElement(driver, notificationMsg, 30);
			Log.message("Waiting for notify message could display");
			String NotMsg_SendLink = notificationMsg.getText();
			SkySiteUtils.waitTill(5000);
			if (NotMsg_SendLink.contentEquals("Link successfully sent")) 
			{
				Log.message("Viewer level sendlink is done successfully");
				driver.close();
				SkySiteUtils.waitTill(10000);
				driver.switchTo().window(winHandleBefore);// Navigate back to parent window
				SkySiteUtils.waitTill(10000);
				driver.get(Act_SendLinkURL);// Launch the URL copied from Clipboard
				SkySiteUtils.waitTill(5000);
				result = this.Download_SingleFile_SendLinkForAFIle(Sys_Download_Path,Exp_File_Name);// Validating download a file from URL																																									 																		
			}

		}

		if (result == true)
			return true;
		else
			return false;

	}

	/**
	 * Method written for Validate select multiple files and send link and download List View
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean ValidateDownload_FromSendLink_BySelectMultFiles(String Sys_Download_Path,String Foldername)throws InterruptedException, AWTException 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;

		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, iconListView, 30);
		Log.message("Waiting for List View button to be appeared");
		SkySiteUtils.waitTill(5000);
		iconListView.click();
		Log.message("Clicked on List View button.");
		SkySiteUtils.waitForElement(driver, chkboxAllFileSel, 30);
		Log.message("Waiting for select all files checkbox to be appeared");
		// Counting number of files available
		int Avl_File_Count = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'chkElements')]"));
		for (WebElement Element : allElements) {
			Avl_File_Count = Avl_File_Count + 1;
		}
		Log.message("Availble number of files in folder is: " + Avl_File_Count);
		SkySiteUtils.waitTill(10000);
		// Select first two files from the list
		if (Avl_File_Count >= 2) {
			for (int i = 1; i <= 2; i++) {
				// Log.message("Select a checkbox");
				driver.findElement(By.xpath("(//input[@class='chkElements chkfileelements'])[" + i + "]")).click();
			}
		}
		SkySiteUtils.waitTill(5000);

		// String Msg_No_Of_FilesSel = msgforselectedfiles.getText();
		btnDropdownMultFileSel.click();
		SkySiteUtils.waitTill(5000);
		tabSendMultFile.click();
		Log.message("Clicked on send link tab.");
		SkySiteUtils.waitForElement(driver, btnSendFileView, 30);
		Log.message("Waiting for send link window");
		SkySiteUtils.waitTill(5000);
		String Act_SendLinkURL = txtCopyToClipboard.getAttribute("value");// Copy the URL
		Log.message("Generated Send Link is: " + Act_SendLinkURL);
		btnContact.click();
		Log.message("Clicked on contact-address book.");
		SkySiteUtils.waitForElement(driver, btnSelectContact, 30);
		Log.message("Waiting for Select Contact window");
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//input[@class='pull-left chklist'])[1]")).click();// Select 1st contact check box 
		btnSelectContact.click();// Click on select button
		SkySiteUtils.waitTill(5000);
		txtMessage.click();
		txtMessage.sendKeys("Testing from QA_Team @Automation");
		btnSendFileView.click();// Click on send
		SkySiteUtils.waitForElement(driver, notificationMsg, 30);
		Log.message("Waiting for notify message could display");
		String NotMsg_SendLink = notificationMsg.getText();
		SkySiteUtils.waitTill(5000);
		iconGridView.click();// Click on grid view
		SkySiteUtils.waitTill(5000);
		if (NotMsg_SendLink.contentEquals("Link successfully sent")) {
			Log.message("Multiple file select and sendlink is done successfully");
			SkySiteUtils.waitTill(10000);

			driver.get(Act_SendLinkURL);// Launch the URL copied from Clip board
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, btnSingFileDownload, 50);
			Log.message("Waiting for download button to be appeared from sendlink URL");

			// Single File download from send link url
			String Exp_File_Name = driver.findElement(By.xpath("(//div[1]/h4)[2]")).getText();
			Log.message("First File from url is: " + Exp_File_Name);
			result1 = this.Download_SingleFile_SendLinkForAFIle(Sys_Download_Path,Exp_File_Name);// Validating download a file from URL

			// Total Zip download from send link url
			String Exp_Zip_Name = "2-files-" + Foldername;
			Log.message("Expected Zip is: " + Exp_Zip_Name);
			result2 = this.Download_ZipFile_SendLinkForMultFIle(Sys_Download_Path,Exp_Zip_Name);// Validate total zip download
			// Select more than one file and download the zip
			result3 = this.Download_ZipFile_SendLinkBySelectMultFIle(Sys_Download_Path,Exp_Zip_Name);// Validate multiple file zip download
		}

		if ((result1 == true) && (result2 == true) && (result3 == true)) 
		{
			Log.message("Multiple file send link and all types of downloads from url is working.");
			return true;
		} else {
			Log.message("Multiple file send link and all types of downloads from url is NOT working.");
			return false;
		}

	}
	
	/**
	 * Method written for Validate select All files and send link and download Grid View
	 * Scripted By: Naresh Babu
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean ValidateDownload_FromSendLink_BySelectAllFiles_GridView(String Sys_Download_Path,String Foldername)throws InterruptedException, AWTException 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;

		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnDeSelectAll_GridView, 30);
		//Get The Count of available files under a folder
		int Avl_File_Count = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Doc_')]"));
		for (WebElement Element : allElements) {
			Avl_File_Count = Avl_File_Count + 1;
		}
		Log.message("Available File Count is: " + Avl_File_Count);
		btnDropdownMultFileSel.click();
		SkySiteUtils.waitTill(5000);
		tabSendMultFile.click();
		Log.message("Clicked on send link tab.");
		SkySiteUtils.waitForElement(driver, btnSendFileView, 30);
		Log.message("Waiting for send link window");
		SkySiteUtils.waitTill(5000);
		String Act_SendLinkURL = txtCopyToClipboard.getAttribute("value");// Copy the URL
		Log.message("Generated Send Link is: " + Act_SendLinkURL);
		btnContact.click();
		Log.message("Clicked on contact-address book.");
		SkySiteUtils.waitForElement(driver, btnSelectContact, 30);
		Log.message("Waiting for Select Contact window");
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//input[@class='pull-left chklist'])[1]")).click();
		Log.message("Selected 1st contact check box"); 
		btnSelectContact.click();
		Log.message("Clicked on select button after select a contact."); 
		SkySiteUtils.waitTill(5000);
		txtMessage.click();
		txtMessage.sendKeys("Testing from QA_Team @Automation");
		btnSendFileView.click();
		Log.message("Clicked on send button.");
		SkySiteUtils.waitForElement(driver, notificationMsg, 30);
		Log.message("Waiting for notify message could display");
		String NotMsg_SendLink = notificationMsg.getText();
		SkySiteUtils.waitTill(5000);
		if (NotMsg_SendLink.contentEquals("Link successfully sent")) {
			Log.message("Multiple file select and sendlink is done successfully");
			SkySiteUtils.waitTill(10000);

			driver.get(Act_SendLinkURL);// Launch the URL copied from Clip board
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, btnSingFileDownload, 50);
			Log.message("Waiting for download button to be appeared from sendlink URL");

			// Single File download from send link url
			String Exp_File_Name = driver.findElement(By.xpath("(//div[1]/h4)[2]")).getText();
			Log.message("First File from url is: " + Exp_File_Name);
			result1 = this.Download_SingleFile_SendLinkForAFIle(Sys_Download_Path,Exp_File_Name);// Validating download a file from URL

			// Total Zip download from send link url
			String Exp_Zip_Name = Avl_File_Count+"-files-" + Foldername;
			Log.message("Expected Zip is: " + Exp_Zip_Name);
			result2 = this.Download_ZipFile_SendLinkForMultFIle(Sys_Download_Path,Exp_Zip_Name);// Validate total zip download
			// Select more than one file and download the zip
			String Exp_ZipNAme_For2Files = "2-files-" + Foldername;
			result3 = this.Download_ZipFile_SendLinkBySelectMultFIle(Sys_Download_Path,Exp_ZipNAme_For2Files);// Validate multiple file zip download
		}

		if ((result1 == true) && (result2 == true) && (result3 == true)) 
		{
			Log.message("Multiple file send link and all types of downloads from url is working.");
			return true;
		} else {
			Log.message("Multiple file send link and all types of downloads from url is NOT working.");
			return false;
		}

	}
	
	/**
	 * Method written for Validate select All files and Delete Grid View
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	
	@FindBy(css=".deleteitem")
	WebElement tabDeleteGridViewDropdown;
	public boolean Validate_Delete_BySelectAllFiles_GridView()throws InterruptedException, AWTException 
	{
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnDeSelectAll_GridView, 30);
		btnDropdownMultFileSel.click();
		SkySiteUtils.waitTill(5000);
		tabDeleteGridViewDropdown.click();
		Log.message("Clicked on Delete tab from dropdown.");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, btnYes, 30);
		btnYes.click();
		Log.message("Clicked on YES button.");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, notificationMsg, 30);
		Log.message("Waiting for notify message could display");
		String NotMsg_Delete = notificationMsg.getText();
		SkySiteUtils.waitTill(5000);
		//Get The Count of available files under a folder
		int Avl_File_Count = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Doc_')]"));
		for (WebElement Element : allElements) {
		Avl_File_Count = Avl_File_Count + 1;
		}
		Log.message("Available File Count is: " + Avl_File_Count);
		
		if ((NotMsg_Delete.contentEquals("File(s) deleted successfully")) && (Avl_File_Count==0)) 
		{
			Log.message("Multiple file select and Delete is done successfully");
			return true;
		}
		else {
			Log.message("Multiple file select and Delete is NOT working");
			return false;
		}

	}
	
	/**
	 * Method written for Validate select All files and Delete List View
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean Validate_Delete_BySelectAllFiles_ListView()throws InterruptedException, AWTException 
	{
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, iconListView, 30);
		Log.message("Waiting for List View button to be appeared");
		iconListView.click();
		Log.message("Clicked on List View button.");
		SkySiteUtils.waitForElement(driver, chkboxAllFileSel, 30);
		Log.message("Waiting for select all files checkbox to be appeared");
		// Counting number of files available
		int Avl_File_Count = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'chkElements')]"));
		for (WebElement Element : allElements) {
			Avl_File_Count = Avl_File_Count + 1;
		}
		Log.message("Availble number of files in folder is: " + Avl_File_Count);
		SkySiteUtils.waitTill(10000);
		// Select first two files from the list
		if (Avl_File_Count >= 2) {
			for (int i = 1; i <= 2; i++) {
				// Log.message("Select a checkbox");
				driver.findElement(By.xpath("(//input[@class='chkElements chkfileelements'])[" + i + "]")).click();
			}
		}
		SkySiteUtils.waitTill(2000);
		btnDropdownMultFileSel.click();
		SkySiteUtils.waitTill(5000);
		tabDeleteGridViewDropdown.click();
		Log.message("Clicked on Delete tab from dropdown.");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, btnYes, 30);
		btnYes.click();
		Log.message("Clicked on YES button.");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, notificationMsg, 30);
		Log.message("Waiting for notify message could display");
		String NotMsg_Delete = notificationMsg.getText();
		SkySiteUtils.waitTill(5000);
		//Get files Count after delete 2 files
		int Avl_File_Count_AftrDelete = 0;
		List<WebElement> allElements1 = driver.findElements(By.xpath("//*[contains(@id, 'li_Doc_')]"));
		for (WebElement Element : allElements1) {
			Avl_File_Count_AftrDelete = Avl_File_Count_AftrDelete + 1;
		}
		Log.message("Available File Count after delete is: " + Avl_File_Count_AftrDelete);
		iconGridView.click();// Click on grid view
		SkySiteUtils.waitTill(5000);
		
		if ((NotMsg_Delete.contentEquals("File(s) deleted successfully")) && (Avl_File_Count_AftrDelete==Avl_File_Count-2)) 
		{
			Log.message("Multiple file select and Delete is done successfully");
			return true;
		}
		else {
			Log.message("Multiple file select and Delete is NOT working");
			return false;
		}

	}

	/**
	 * Method written for Validate select multiple photos and send link and download
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */

	// Validate Send link by select multiple files and download
	public boolean ValidateDownload_FromSendLink_BySelectMultPhotos(String Sys_Download_Path,String Foldername, String Project_Name)
			throws InterruptedException, AWTException {
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;

		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, iconListView, 30);
		Log.message("Waiting for List View button to be appeared");
		SkySiteUtils.waitTill(5000);
		iconListView.click();
		Log.message("Clicked on List View button.");
		SkySiteUtils.waitForElement(driver, chkboxAllPhoto, 30);
		Log.message("Waiting for select all photos checkbox to be appeared");
		// Counting number of files available
		int Avl_File_Count = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'media navbar-collapse pw-photo')]"));
		for (WebElement Element : allElements) {
			Avl_File_Count = Avl_File_Count + 1;
		}
		Log.message("Availble number of photos in folder is: " + Avl_File_Count);
		SkySiteUtils.waitTill(10000);
		// Select first two files from the list
		if (Avl_File_Count >= 2) 
		{
			for (int i = 1; i <= 2; i++) 
			{
				// Log.message("Select a checkbox");
				driver.findElement(By.xpath("(//input[@type='checkbox' and @data-bind='click: ItemChkboxClick, clickBubble: false, checked: IsChecked'])["+i+"]")).click();
			}
		}
		SkySiteUtils.waitTill(5000);
		// String Msg_No_Of_FilesSel = msgforselectedfiles.getText();
		btnDropdownMultPhotoSel.click();
		SkySiteUtils.waitTill(5000);
		driver.findElement(By
				.xpath("//i[@class='icon icon-ellipsis-horizontal media-object icon-lg' and @data-placement='left']/../following-sibling::ul/li[2]//span"))
				.click();
		Log.message("Clicked on Send Link Tab for the Photos.");
		SkySiteUtils.waitForElement(driver, btnSendFileView, 30);
		Log.message("Waiting for send link window");
		SkySiteUtils.waitTill(5000);
		String Act_SendLinkURL = txtCopyToClipboard.getAttribute("value");// Copy the URL
		Log.message("Generated Send Link is: " + Act_SendLinkURL);
		btnContact.click();
		Log.message("Clicked on contact-address book.");
		SkySiteUtils.waitForElement(driver, btnSelectContact, 30);
		Log.message("Waiting for Select Contact window");
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//input[@class='pull-left chklist'])[1]")).click();// Select 1st contact check box
		btnSelectContact.click();// Click on select button
		SkySiteUtils.waitTill(5000);
		txtMessage.click();
		txtMessage.sendKeys("Testing from QA_Team @Automation");
		btnSendFileView.click();// Click on send
		SkySiteUtils.waitForElement(driver, notificationMsg, 30);
		Log.message("Waiting for notify message could ");
		String NotMsg_SendLink = notificationMsg.getText();
		SkySiteUtils.waitTill(5000);
		iconGridView.click();// Click on grid view
		SkySiteUtils.waitTill(5000);
		if (NotMsg_SendLink.contentEquals("Link successfully sent")) {
			Log.message("Multiple photo select and sendlink is done successfully");
			SkySiteUtils.waitTill(10000);

			driver.get(Act_SendLinkURL);// Launch the URL copied from Clip board
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, btnSingFileDownload, 50);
			Log.message("Waiting for download button to be appeared from sendlink URL");

			// Single photo download from send link url
			String Exp_File_Name = driver.findElement(By.xpath("(//div[1]/h4)[2]")).getText();
			Log.message("First Photo from url is: " + Exp_File_Name);
			result1 = this.Download_SingleFile_SendLinkForAFIle(Sys_Download_Path,Exp_File_Name);// Validating download a photo from URL

			// Total Zip download from send link url
			String Exp_Zip_Name = "2-photos-"+Foldername+"-"+Project_Name;
			Log.message("Expected Zip is: " + Exp_Zip_Name);
			result2 = this.Download_ZipFile_SendLinkForMultFIle(Sys_Download_Path,Exp_Zip_Name);// Validate total zip download
			// Select more than one photo and download the zip
			result3 = this.Download_ZipFile_SendLinkBySelectMultFIle(Sys_Download_Path,Exp_Zip_Name);// Validate multiple photo zip download
		}

		if ((result1 == true) && (result2 == true) && (result3 == true)) 
		{
			Log.message("Multiple photo send link and all types of downloads from url is working.");
			return true;
		} else {
			Log.message("Multiple photo send link and all types of downloads from url is NOT working.");
			return false;
		}

	}

	/**
	 * Method written for Validate Download functionality by select multiple photos
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean ValidateDownload_BySelectMultPhotos(String Sys_Download_Path, String Foldername, String Project_Name)
			throws InterruptedException, AWTException 
	{
		boolean result = false;

		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, iconListView, 30);
		Log.message("Waiting for List View button to be appeared");
		SkySiteUtils.waitTill(5000);
		iconListView.click();
		Log.message("Clicked on List View button.");
		SkySiteUtils.waitForElement(driver, chkboxAllPhoto, 30);
		Log.message("Waiting for select all photos checkbox to be appeared");
		// Counting number of files available
		int Avl_File_Count = 0;
		List<WebElement> allElements = driver
				.findElements(By.xpath("//*[contains(@class, 'media navbar-collapse pw-photo')]"));
		for (WebElement Element : allElements) {
			Avl_File_Count = Avl_File_Count + 1;
		}
		Log.message("Availble number of photos in folder is: " + Avl_File_Count);
		SkySiteUtils.waitTill(10000);
		// Select first two files from the list
		if (Avl_File_Count >= 2) {
			for (int i = 1; i <= 2; i++) {
				// Log.message("Select a checkbox");
				driver.findElement(By
						.xpath("(//input[@type='checkbox' and @data-bind='click: ItemChkboxClick, clickBubble: false, checked: IsChecked'])["
								+ i + "]"))
						.click();
			}
		}
		SkySiteUtils.waitTill(5000);

		// Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(10000);

		btnDropdownMultPhotoSel.click();
		SkySiteUtils.waitTill(5000);
		driver.findElement(By
				.xpath("//i[@class='icon icon-ellipsis-horizontal media-object icon-lg' and @data-placement='left']/../following-sibling::ul/li[1]//span"))
				.click();// Click on Download tab
		Log.message("Clicked on Download Tab for the Photos.");
		SkySiteUtils.waitTill(40000);

		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);

		if (browserName.contains("firefox")) {
			// Handling Download PopUp of firefox browser using robot
			Robot robot = null;
			robot = new Robot();

			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(60000);
		}

		// After Download, checking whether Downloaded file is existed under
		// download folder or not
		String ActualFilename = null;
		File[] files = new File(Sys_Download_Path).listFiles();

		for (File file : files) {
			if (file.isFile()) {
				ActualFilename = file.getName();// Getting File Names into a
												// variable
				Log.message("Actual File name is:" + ActualFilename);
				SkySiteUtils.waitTill(1000);
				Long ActualFileSize = file.length();
				Log.message("Actual File size is:" + ActualFileSize);
				String Exp_Zip_Name = "2-photos-" + Foldername + "-" + Project_Name;
				Log.message("Expected Zip is: " + Exp_Zip_Name);
				if ((ActualFilename.contains(Exp_Zip_Name)) && (ActualFileSize != 0)) {
					Log.message("Downloaded ZIP file is available in downloads!!!");
					result = true;
					break;
				}

			}

		}

		iconGridView.click();// Click on grid view
		SkySiteUtils.waitTill(5000);

		if (result == true) {
			Log.message("Multiple photo select and download is working.");
			return true;
		} else {
			Log.message("Multiple photo select and download is NOT working.");
			return false;
		}

	}

	/**
	 * Method written for Validate Folder send link and download sub Folder
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean ValidateDownload_SubFolder_FromFolderSendLink(String Sys_Download_Path,String Foldername)
			throws InterruptedException, AWTException 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;
		boolean result4 = false;

		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
		// Getting Folder count after created a new folder
		int Avl_Fold_Count = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		int j = 1;
		for (j = 1; j <= Avl_Fold_Count; j++) {
			actualFolderName = driver
					.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4"))
					.getText();
			Log.message("Exp Name:" + Foldername);
			Log.message("Act Name:" + actualFolderName);

			// Validating the new Folder is created or not
			if (Foldername.trim().contentEquals(actualFolderName.trim())) {
				result1 = true;
				Log.message("Expected Folder is Availble with name: " + Foldername);
				break;
			}
		}

		if (result1 == true) {
			SkySiteUtils.waitTill(5000);
			driver.findElement(
					By.xpath("(//i[@class='icon icon-ellipsis-horizontal media-object icon-lg'])[" + j + "]")).click();// Folder Dropdown
			Log.message("Clicked on More options icon for the Folder.");
			SkySiteUtils.waitTill(2000);
			WebElement EditLink = driver.findElement(By.xpath("(//a[contains(text(),'Send')])[" + j + "]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", EditLink);// JSExecutor to click send
			Log.message("Clicked on Send Link Tab for the Folder.");
			SkySiteUtils.waitForElement(driver, btnSendFileView, 30);
			Log.message("Waiting for send link window");
			SkySiteUtils.waitTill(5000);
			String Act_SendLinkURL = txtCopyToClipboard.getAttribute("value");// Copy the URL
			Log.message("Generated Send Link is: " + Act_SendLinkURL);
			btnContact.click();
			Log.message("Clicked on contact-address book.");
			SkySiteUtils.waitForElement(driver, btnSelectContact, 30);
			Log.message("Waiting for Select Contact window");
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath("(//input[@class='pull-left chklist'])[1]")).click();// Select 1st contact check box
			btnSelectContact.click();// Click on select button
			SkySiteUtils.waitTill(5000);
			txtMessage.click();
			txtMessage.sendKeys("Testing from QA_Team @Automation");
			btnSendFileView.click();// Click on send
			SkySiteUtils.waitForElement(driver, notificationMsg, 30);
			Log.message("Waiting for notify message could display");
			String NotMsg_SendLink = notificationMsg.getText();
			SkySiteUtils.waitTill(5000);
			if (NotMsg_SendLink.contentEquals("Link successfully sent")) {
				Log.message("Folder sendlink is done successfully");
				SkySiteUtils.waitTill(10000);
				driver.get(Act_SendLinkURL);// Launch the URL copied from Clip board
				SkySiteUtils.waitTill(5000);
				SkySiteUtils.waitForElement(driver, btnSingFileDownload, 50);
				Log.message("Waiting for download button to be appeared from sendlink URL");

				// Sub Folder download from send link url
				String Exp_SubFold_Name = eleSubFoldURL.getText();
				Log.message("Sub Folder from url is: " + Exp_SubFold_Name);
				result2 = this.Download_SubFolder_FoldSendLink(Sys_Download_Path,Exp_SubFold_Name);//Validating download a Sub Folder from URL
				eleSubFoldURL.click();
				Log.message("Clicked on subfolder.");
				SkySiteUtils.waitForElement(driver, btnBackFold, 30);
				Log.message("Waiting for back button to be appeared");

				// Single File download from send link url
				String Exp_File_Name = driver
						.findElement(By.xpath(".//*[@id='sharedDocsFolderList']/li[1]/div/div[1]/h4")).getText();
				Log.message("First File from url is: " + Exp_File_Name);
				result3 = this.Download_SingleFile_SendLinkSubFold(Sys_Download_Path,Exp_File_Name);//Validating download a file from URL
				SkySiteUtils.waitTill(5000);
				btnBackFold.click();
				SkySiteUtils.waitTill(5000);

				// Total Zip download from send link url
				result4 = this.Download_ZipFile_SendLinkForMultFIle(Sys_Download_Path,Foldername);//Validate total zip download
			}

		}

		if ((result1 == true) && (result2 == true) && (result3 == true) && (result4 == true)) {
			Log.message("Folder having subfolder send link and all types of downloads from url is working.");
			return true;
		} else {
			Log.message("Folder having subfolder send link and all types of downloads from url is NOT working.");
			return false;
		}

	}
	
	/**
	 * Method written for Validate Folder send link and Do Zip and single File
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean Validate_FolderSendLink_DownloadFromLink(String Sys_Download_Path,String Foldername)
			throws InterruptedException, AWTException 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;

		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
		// Getting Folder count after created a new folder
		int Avl_Fold_Count = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		int j = 1;
		for (j = 1; j <= Avl_Fold_Count; j++) 
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]/section[2]/h4")).getText();
			Log.message("Exp Name:" + Foldername);
			Log.message("Act Name:" + actualFolderName);
			// Validating the new Folder is created or not
			if (Foldername.trim().contentEquals(actualFolderName.trim())) 
			{
				result1 = true;
				Log.message("Expected Folder is Availble with name: " + Foldername);
				break;
			}
		}

		if (result1 == true) 
		{
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal media-object icon-lg'])["+j+"]")).click();
			Log.message("Clicked on More options icon for the Folder.");
			SkySiteUtils.waitTill(2000);
			WebElement EditLink = driver.findElement(By.xpath("(//a[contains(text(),'Send')])[" + j + "]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", EditLink);// JSExecutor to click send
			Log.message("Clicked on Send Link Tab for the Folder.");
			SkySiteUtils.waitForElement(driver, btnSendFileView, 30);
			Log.message("Waiting for send link window");
			SkySiteUtils.waitTill(5000);
			String Act_SendLinkURL = txtCopyToClipboard.getAttribute("value");
			Log.message("Generated Send Link is: " + Act_SendLinkURL);
			btnContact.click();
			Log.message("Clicked on contact-address book.");
			SkySiteUtils.waitForElement(driver, btnSelectContact, 30);
			Log.message("Waiting for Select Contact window");
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath("(//input[@class='pull-left chklist'])[1]")).click();// Select 1st contact check box
			btnSelectContact.click();// Click on select button
			SkySiteUtils.waitTill(5000);
			txtMessage.click();
			txtMessage.sendKeys("Testing from QA_Team @Automation");
			btnSendFileView.click();// Click on send
			SkySiteUtils.waitForElement(driver, notificationMsg, 30);
			Log.message("Waiting for notify message could display");
			String NotMsg_SendLink = notificationMsg.getText();
			SkySiteUtils.waitTill(5000);
			if (NotMsg_SendLink.contentEquals("Link successfully sent")) 
			{
				Log.message("Folder sendlink is done successfully");
				SkySiteUtils.waitTill(10000);
				driver.get(Act_SendLinkURL);// Launch the URL copied from Clip board
				SkySiteUtils.waitTill(5000);
				SkySiteUtils.waitForElement(driver, btnSingFileDownload, 50);
				Log.message("Waiting for download button to be appeared from sendlink URL");

				//ZIP Folder download from send link url
				result2 = this.ValidateDownload_FolderFromLink(Sys_Download_Path,Foldername);//Validating download a Folder from URL

				// Single File download from send link url
				String Exp_File_Name = driver.findElement(By.xpath(".//*[@id='sharedDocsFolderList']/li[1]/div/div[1]/h4")).getText();
				Log.message("First File from url is: " + Exp_File_Name);
				result3 = this.Download_SingleFile_SendLinkSubFold(Sys_Download_Path,Exp_File_Name);//Validating download a file from URL
			}

		}

		if ((result1 == true) && (result2 == true) && (result3 == true)) 
		{
			Log.message("Folder send link and all types of downloads from url is working.");
			return true;
		} else {
			Log.message("Folder send link and all types of downloads from url is NOT working.");
			return false;
		}

	}
	
	/**
	 * Method written for Validate File send link and Do Download File from link
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean Validate_FileSendLink_FileDownloadFromLink(String Sys_Download_Path,String FileName)
			throws InterruptedException, AWTException 
	{
		boolean result1 = false;
		boolean result2 = false;

		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
		//Get The Count of available files
		int Avl_File_Count = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Doc_')]"));
		for (WebElement Element : allElements) {
		Avl_File_Count = Avl_File_Count + 1;
		}
		Log.message("Available File Count is: " + Avl_File_Count);
		int j = 0;
		String actualFileName = null;
		for (j = 1; j <= Avl_File_Count; j++) 
		{
			actualFileName = driver.findElement(By.xpath("(//span[@class='h4_docname document-preview-event'])["+j+"]")).getText();
			Log.message("Exp Name:" + FileName);
			Log.message("Act Name:" + actualFileName);

			// Validating the file is available or not
			if (actualFileName.trim().equalsIgnoreCase(FileName.trim())) 
			{
				result1 = true;
				Log.message("Expected File is available with name: " + actualFileName);
				break;
			}
		}

		if (result1 == true) 
		{
			driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal media-object icon-lg'])["+j+"]")).click();
			Log.message("Click on file drop list.");
			SkySiteUtils.waitTill(2000);
			WebElement SendLink = driver.findElement(By.xpath("(//a[contains(text(),'Send')])[" + j + "]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", SendLink);// JSExecutor to click send
			Log.message("Clicked on Send Link Tab for the File.");
			SkySiteUtils.waitForElement(driver, btnSendFileView, 30);
			Log.message("Waiting for send link window");
			SkySiteUtils.waitTill(5000);
			String Act_SendLinkURL = txtCopyToClipboard.getAttribute("value");
			Log.message("Generated Send Link is: " + Act_SendLinkURL);
			btnContact.click();
			Log.message("Clicked on contact-address book.");
			SkySiteUtils.waitForElement(driver, btnSelectContact, 30);
			Log.message("Waiting for Select Contact window");
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath("(//input[@class='pull-left chklist'])[1]")).click();// Select 1st contact check box
			btnSelectContact.click();//Click on select button
			SkySiteUtils.waitTill(5000);
			txtMessage.click();
			txtMessage.sendKeys("Testing from QA_Team @Automation");
			btnSendFileView.click();//Click on send
			SkySiteUtils.waitForElement(driver, notificationMsg, 30);
			Log.message("Waiting for notify message could display");
			String NotMsg_SendLink = notificationMsg.getText();
			SkySiteUtils.waitTill(5000);
			if (NotMsg_SendLink.contentEquals("Link successfully sent")) 
			{
				Log.message("File sendlink is done successfully");
				SkySiteUtils.waitTill(10000);
				driver.get(Act_SendLinkURL);// Launch the URL copied from Clip board
				SkySiteUtils.waitTill(5000);
				SkySiteUtils.waitForElement(driver, btnSingFileDownload, 50);
				Log.message("Waiting for download button to be appeared from sendlink URL");

				// Single File download from send link url
				result2 = this.Download_SingleFile_SendLinkForAFIle(Sys_Download_Path, FileName);
			}

		}

		if ((result1 == true) && (result2 == true)) 
		{
			Log.message("File send link and file download from url is working.");
			return true;
		} else {
			Log.message("File send link and file download from url is NOT working.");
			return false;
		}

	}


	/**
	 * Method written for Validate File download from revision history 
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */

	// Validate Send link by select multiple files and download
	public boolean ValidateDownload_File_FromRevisionHistory(String Sys_Download_Path) throws InterruptedException, AWTException {
		boolean result1 = false;

		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
		SkySiteUtils.waitTill(5000);
		// Get The Count of available files in selected folder
		int Avl_File_Count = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Doc_')]"));
		for (WebElement Element : allElements) {
			Avl_File_Count = Avl_File_Count + 1;
		}
		Log.message("Available File Count is: " + Avl_File_Count);

		if (Avl_File_Count >= 1) 
		{
			String FirstFileName = driver.findElement(By.xpath("(//span[@class='h4_docname document-preview-event'])[1]")).getText();//First file name
			Log.message("Act Name:" + FirstFileName);
			driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal media-object icon-lg'])[1]")).click();
			Log.message("Click on file drop list.");
			SkySiteUtils.waitTill(2000);
			WebElement RevisionLink = driver.findElement(By.xpath("(//a[contains(text(),'Revision history')])[1]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", RevisionLink);// JSExecutor
			Log.message("Clicked on Revision history tab of a file.");
			// Calling "Deleting download folder files" method
			projectDashboardPage = new ProjectDashboardPage(driver).get();
			projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath("(//i[@class='icon icon-download-alt icon-lg'])[3]")).click();
			Log.message("Clicked on download icon from revision history.");
			SkySiteUtils.waitTill(20000);
			
			// Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: " + browserName);

			if (browserName.contains("firefox")) {
				// Handling Download PopUp of firefox browser using robot
				Robot robot = null;
				robot = new Robot();

				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(30000);
			}

			//driver.findElement(By.xpath("(//button[@class='close'])[2]")).click();
			//Log.message("Clicked on close button of revision window.");

			// After Download, checking whether Downloaded file is existed under download folder or not
			String ActualFilename = null;
			File[] files = new File(Sys_Download_Path).listFiles();

			for (File file : files) {
				if (file.isFile()) {
					ActualFilename = file.getName();// Getting File Names into a variable
					Log.message("Actual File name is:" + ActualFilename);
					SkySiteUtils.waitTill(1000);
					Long ActualFileSize = file.length();
					Log.message("Actual File size is:" + ActualFileSize);
					if ((ActualFilename.contains(FirstFileName)) && (ActualFileSize != 0)) {
						Log.message("Downloaded file is available in downloads!!!");
						result1 = true;
						break;
					}

				}

			}

		}

		if ((result1 == true)) {
			Log.message("File download from revision history window is working.");
			return true;
		} else {
			Log.message("File download from revision history window is NOT working.");
			return false;
		}

	}
	
	/**
	 * Method written for Validate File View from revision history 
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	@FindBy(css=".has-tooltip.pull-right.revHistoryView>a")
	WebElement iconrevHistoryView;
	public boolean Validate_FileView_FromRevisionHistory(String File_Name) throws InterruptedException, AWTException {
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
		SkySiteUtils.waitTill(5000);
		// Get The Count of available files in selected folder
		int Avl_File_Count = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Doc_')]"));
		for (WebElement Element : allElements) {
			Avl_File_Count = Avl_File_Count + 1;
		}
		Log.message("Available File Count is: " + Avl_File_Count);
		
		//Taking file names by passing variable into x_path
		String actualFileName=null;
		String winHandleBefore=null;
		for(int j=2;j<=Avl_File_Count+1;j++)
		{
			actualFileName = driver.findElement(By.xpath("//li["+j+"]/section[2]/h4")).getText();	
			Log.message("File Name from Application is: "+actualFileName);	
			if(actualFileName.equalsIgnoreCase(File_Name))
			{	
				j=j-1;
				driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal media-object icon-lg'])["+j+"]")).click();
				Log.message("Click on file drop list.");
				SkySiteUtils.waitTill(2000);
				WebElement RevisionLink = driver.findElement(By.xpath("(//a[contains(text(),'Revision history')])["+j+"]"));
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", RevisionLink);// JSExecutor
				Log.message("Clicked on Revision history tab of a file.");
				SkySiteUtils.waitTill(5000);
				SkySiteUtils.waitForElement(driver, iconrevHistoryView, 30);
				iconrevHistoryView.click();
				Log.message("Clicked on View icon from revision history.");
				SkySiteUtils.waitTill(60000);
				winHandleBefore = driver.getWindowHandle();
				for(String winHandle : driver.getWindowHandles())
				{
					driver.switchTo().window(winHandle);
				}
				SkySiteUtils.waitForElement(driver, Viewer_DropPlace, 120);
				break;
			}
		}
		if(Viewer_DropPlace.isDisplayed())
		{
			driver.close();
			SkySiteUtils.waitTill(3000);
			driver.switchTo().window(winHandleBefore);
			SkySiteUtils.waitTill(3000);
			Log.message("File view from revision history is working successfully!!!");
			return true;
		}
		else
		{
			Log.message("File view from revision history is NOT working!!!");
			return false;
		}

	}
	
	/**
	 * Method written for Validate File Edit
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean Validate_File_Edit(String Edit_FileName) throws InterruptedException, AWTException 
	{
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal media-object icon-lg'])[1]")).click();
		Log.message("Click on file drop list.");
		SkySiteUtils.waitTill(2000);
		WebElement File_EditTab = driver.findElement(By.xpath("(//a[contains(text(),'Edit fields')])[1]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", File_EditTab);// JSExecutor
		Log.message("Clicked on file edit field.");
		SkySiteUtils.waitForElement(driver, btnSaveEditDocWindow, 30);
		SkySiteUtils.waitTill(5000);
		txtDocName.clear();
		txtDocName.sendKeys(Edit_FileName);
		btnSaveEditDocWindow.click();
		Log.message("Clicked on save button after edit the file name.");
		SkySiteUtils.waitForElement(driver, btnInfoMsgYes, 30);
		SkySiteUtils.waitTill(2000);
		btnInfoMsgYes.click();
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, notificationMsg, 30);
		String Msg_AfterEditFile = notificationMsg.getText();
		Log.message("Notify message after edit file is: "+Msg_AfterEditFile);
		SkySiteUtils.waitTill(5000);
		String FirstFileName = driver.findElement(By.xpath("(//span[@class='h4_docname document-preview-event'])[1]")).getText();
		Log.message("Act Name:" + FirstFileName);
		if ((FirstFileName.equalsIgnoreCase(Edit_FileName)) && (Msg_AfterEditFile.contentEquals("Successfully saved."))) 
		{
			Log.message("Edit File and validate is working successfully.");
			return true;
		} else {
			Log.message("Edit File and validate is NOT working.");
			return false;
		}

	}



	/**
	 * Method written for Move a folder to a destination folder 
	 * Scripted By: Ranjan
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	// Validate Download file And size
	public boolean DownloadFiledownload(String Downloadpath) throws InterruptedException, AWTException, IOException {

		boolean result1 = false;
		SkySiteUtils.waitTill(10000);
		// Calling "Deleting download folder files" method
		String RFINo = RFINumber_Display.getText();
		String fileName ="RFI "+ RFINo +".pdf";
		Log.message("Download Path is: " + Downloadpath);
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Downloadpath);
		SkySiteUtils.waitTill(10000);
		driver.findElement(By.xpath("(//i [@class='icon icon-lg icon-download-alt'])[1]")).click();
		Log.message("Clicked on download icon from RFI creationed icon for download- Files Downloded Sucessfully");

		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);

		if (browserName.contains("firefox")) {
			// Handling Download PopUp of firefox browser using robot
			Robot robot = null;
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(30000);
		}
		SkySiteUtils.waitTill(40000);
		// After Download, checking whether Downloaded file is existed under
		// download folder or not
		String ActualFilename = null;
		File[] files = new File(Downloadpath).listFiles();
		
		//File dwFile = new File(Downloadpath) ;
		//Log.message("===============start clean ====================");
		//SkySiteUtils.waitTill(50000);
		//FileUtils.cleanDirectory(dwFile);
		
		for (File file : files)
		{
			if (file.isFile()) 
			{
				ActualFilename = file.getName();// Getting Folder Name into a variable
				Log.message("Actual Folder name is:" + ActualFilename);
				SkySiteUtils.waitTill(1000);
				Long ActualFolderSize = file.length();
				Log.message("Actual Folder size is:" + ActualFolderSize);
				SkySiteUtils.waitTill(5000);
				if (ActualFolderSize != 0) 
				{
					Log.message("Downloaded folder is available in downloads!!!");
				} 
				else
				{
					Log.message("Downloaded folder is NOT available in downloads!!!");
				}
			}
		}
		if (ActualFilename.contains(fileName))
			return true;
		else
			return false;		
	}	

	/**
	 * Method written for Move a folder to a destination folder 
	 * Scripted By: Naresh Babu
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean Move_Folder_ToDestinationFolder(String Source_Folder, String Desti_Folder)
			throws InterruptedException, AWTException, IOException {
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;
		boolean result4 = false;

		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
		// Getting Folder count after created a new folder
		int Avl_Fold_Count = 0;
		int j = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		for (j = 1; j <= Avl_Fold_Count; j++) {
			actualFolderName = driver
					.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4"))
					.getText();
			Log.message("Exp Name:" + Source_Folder);
			Log.message("Act Name:" + actualFolderName);

			// Validating the new Folder is created or not
			if (Source_Folder.trim().contentEquals(actualFolderName.trim())) {
				result1 = true;
				Log.message("Expected Source Folder is available with name: " + Source_Folder);
				break;
			}
		}

		int i = 0;
		if (result1 == true) {
			driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li["+j+"]/section[4]/ul/li[4]/a/i")).click();
			Log.message("Clicked on drop list of source folder.");
			SkySiteUtils.waitTill(3000);
			int MoveButPlace = j - 1;
			WebElement MoveTab = driver.findElement(By.xpath("(//a[contains(text(),'Move')])["+MoveButPlace+"]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", MoveTab);// JSExecutor to click Move
			Log.message("Clicked on Move Tab for the source folder.");
			SkySiteUtils.waitForElement(driver, btnCancelMoveFolder, 30);
			Log.message("Waiting for Move Folder Window cancel button to be appeared");
			SkySiteUtils.waitTill(15000);

			// Select a destination folder from the folder tree of move folder
			// window
			for (i = 2; i <= Avl_Fold_Count; i++) {
				String Act_DestFolder = driver.findElement(By.xpath("(//span[@class='standartTreeRow'])[" + i + "]"))
						.getText();// Getting Folder Name
				Log.message("Actual Destination Folder is: " + Act_DestFolder);
				if (Act_DestFolder.equalsIgnoreCase(Desti_Folder)) {
					result2 = true;
					Log.message("Expected destination folder is available under Folder tree.");
					break;
				}
			}

			if (result2 == true) {
				driver.findElement(By.xpath("(//span[@class='standartTreeRow'])[" + i + "]")).click();
				Log.message("clicked on Expected destination folder.");
				SkySiteUtils.waitTill(5000);
				btnSaveMoveFolder.click();
				Log.message("clicked on Save button.");
				SkySiteUtils.waitForElement(driver, btnInfoMsgNo, 30);
				Log.message("Waiting for Conformation window No button to be appeared");
				SkySiteUtils.waitTill(2000);
				btnInfoMsgYes.click();
				Log.message("clicked on Yes button for the conformation message.");
				SkySiteUtils.waitTill(15000);
				// SkySiteUtils.waitForElement(driver, notificationMsg, 30);
				// String MoveFold_NotifyConfMsg = notificationMsg.getText();
				// Log.message("Notify message after move a folder is:
				// "+MoveFold_NotifyConfMsg);
				for (j = 1; j <= Avl_Fold_Count - 1; j++) {
					actualFolderName = driver
							.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4"))
							.getText();
					Log.message("Exp Name:" + Desti_Folder);
					Log.message("Act Name:" + actualFolderName);

					// Validating the new Folder is created or not
					if (Desti_Folder.trim().contentEquals(actualFolderName.trim())) {
						result3 = true;
						Log.message("Expected Destination Folder is available with name: " + Desti_Folder);
						break;
					}
				}

				if (result3 == true) {
					driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4"))
							.click();// Select Dest Folder
					Log.message("Clicked on Destination folder.");
					SkySiteUtils.waitTill(10000);
					int Folders_Under_DestFold = 0;
					List<WebElement> allElements1 = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
					for (WebElement Element : allElements1) {
						Folders_Under_DestFold = Folders_Under_DestFold + 1;
					}
					Log.message("Source Folder is Available under destination folder is: " + Folders_Under_DestFold);

					for (j = 2; j <= Folders_Under_DestFold + 1; j++) {
						actualFolderName = driver
								.findElement(
										By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4"))
								.getText();
						Log.message("Exp Name:" + Source_Folder);
						Log.message("Act Name:" + actualFolderName);

						// Validating the new Folder is created or not
						if (Source_Folder.trim().contentEquals(actualFolderName.trim())) {
							result4 = true;
							Log.message("Source Folder is available under destination folder: " + Source_Folder);
							driver.findElement(By.xpath("//a[@class='aProjHeaderLink']")).click();
							Log.message("Clicked on project name from breadcrumb to navigate back to folder level.");
							SkySiteUtils.waitTill(10000);
							break;
						}
					}

				}

			}

		}

		// Final Validation
		if ((result1 == true) && (result2 == true) && (result3 == true) && (result4 == true)) {
			Log.message("Folder moved to destination folder successfully!!!");
			return true;
		} else {
			Log.message("Folder moved to destination folder FAILED!!!");
			return false;
		}

	}
	
	/**
	 * Method written for Move a file to a destination folder 
	 * Scripted By: Naresh Babu
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean Move_File_ToDestinationFolder(String Desti_Folder, String File_Name)
			throws InterruptedException, AWTException, IOException {
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;

		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
		// Get The Count of available files in selected folder
		int Avl_File_Count = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Doc_')]"));
		for (WebElement Element : allElements) {
			Avl_File_Count = Avl_File_Count + 1;
		}
		Log.message("Available File Count is: " + Avl_File_Count);	
		//Taking file names by passing variable into x_path
		String actualFileName=null;
		int j = 0;
		for(j=2;j<=Avl_File_Count+1;j++)
		{
			actualFileName = driver.findElement(By.xpath("//li["+j+"]/section[2]/h4")).getText();	
			Log.message("File Name from Application is: "+actualFileName);	
			if(actualFileName.equalsIgnoreCase(File_Name))
			{
				result1 = true;
				Log.message("Expected Source File is available with name: "+File_Name);
				break;
			}
		}
		int i = 0;
		if(result1 == true) 
		{
			j=j-1;
			driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal media-object icon-lg'])["+j+"]")).click();
			Log.message("Click on file drop list.");
			SkySiteUtils.waitTill(2000);
			WebElement MoveTab = driver.findElement(By.xpath("(//a[@class='move-file'])[1]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", MoveTab);// JSExecutor to click Move
			Log.message("Clicked on Move Tab for the source file.");
			SkySiteUtils.waitForElement(driver, btnCancelMoveFile, 30);
			Log.message("Waiting for Move file Window to be appeared");
			SkySiteUtils.waitTill(15000);
			// Select a destination folder from move folder window
			for (i = 2; i <= 3; i++) 
			{
				String Act_DestFolder = driver.findElement(By.xpath("(//span[@class='standartTreeRow'])["+i+"]")).getText();
				Log.message("Actual Destination Folder is: " + Act_DestFolder);
				if (Act_DestFolder.equalsIgnoreCase(Desti_Folder)) 
				{
					result2 = true;
					Log.message("Expected destination folder is available under Folder tree.");
					break;
				}
			}

			if (result2 == true) 
			{
				driver.findElement(By.xpath("(//span[@class='standartTreeRow'])[" + i + "]")).click();
				Log.message("clicked on Expected destination folder.");
				SkySiteUtils.waitTill(5000);
				btnSaveMoveFile.click();
				Log.message("clicked on Save button.");
				SkySiteUtils.waitForElement(driver, btnInfoMsgNo, 30);
				Log.message("Waiting for Conformation window No button to be appeared");
				SkySiteUtils.waitTill(2000);
				btnInfoMsgYes.click();
				Log.message("clicked on Yes button for the conformation message.");
				SkySiteUtils.waitTill(15000);
				// SkySiteUtils.waitForElement(driver, notificationMsg, 30);
				// String MoveFold_NotifyConfMsg = notificationMsg.getText();
				// Log.message("Notify message after move a folder is:
				// "+MoveFold_NotifyConfMsg);
				//File Count after move a single file
				int Avl_File_Count1 = 0;
				List<WebElement> allElements1 = driver.findElements(By.xpath("//*[contains(@id, 'li_Doc_')]"));
				for (WebElement Element : allElements1) {
					Avl_File_Count1 = Avl_File_Count1 + 1;
				}
				Log.message("Available File Count after move a file is: " + Avl_File_Count1);
				
				if((driver.findElement(By.xpath(".//*[@id='empty-container']/h2")).isDisplayed()) && (Avl_File_Count1==0))
				{
					Log.message("File was removed from the folder. That Means file moved to destination");
					driver.findElement(By.xpath("//a[@class='aProjHeaderLink']")).click();
					SkySiteUtils.waitTill(10000);
					Log.message("Clicked on project header.");
					result3 = true;
				}		
			}
		}

		// Final Validation
		if ((result1 == true) && (result2 == true) && (result3 == true)) {
			Log.message("File moved from Source folder successfully!!!");
			return true;
		} else {
			Log.message("File moved from Source folder FAILED!!!");
			return false;
		}

	}
	
	/**
	 * Method written for validate a moved file, from destination folder 
	 * Scripted By: Naresh Babu
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean Validate_ExpectedFile_FromAFolder(String File_Name)
			throws InterruptedException, AWTException, IOException {
		boolean result1 = false;

		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
		// Get The Count of available files in selected folder
		int Avl_File_Count = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Doc_')]"));
		for (WebElement Element : allElements) {
			Avl_File_Count = Avl_File_Count + 1;
		}
		Log.message("Available File Count is: " + Avl_File_Count);	
		//Taking file names by passing variable into x_path
		String actualFileName=null;
		int j = 0;
		for(j=2;j<=Avl_File_Count+1;j++)
		{
			actualFileName = driver.findElement(By.xpath("//li["+j+"]/section[2]/h4")).getText();	
			Log.message("File Name from Application is: "+actualFileName);	
			if(actualFileName.equalsIgnoreCase(File_Name))
			{
				result1 = true;
				Log.message("Expected Source File is available in Destination folder with name: "+File_Name);
				break;
			}
		}
		// Final Validation
		if ((result1 == true)) {
			Log.message("File available in Destination folder!!!");
			return true;
		} else {
			Log.message("File NOT available in Destination folder!!!");
			return false;
		}

	}

	/**
	 * Method written for Download a folder 
	 * Scripted By: Naresh Babu
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */

	public boolean Download_Folder(String Sys_Download_Path,String Folder_Name) throws InterruptedException, AWTException 
	{
		boolean result1 = false;
		boolean result2 = false;
		// Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(5000);

		// Getting Folder count after created a new folder
		int Avl_Fold_Count = 0;
		int j = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		for (j = 1; j <= Avl_Fold_Count; j++) 
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+j+"]/section[2]/h4")).getText();
			Log.message("Exp Name:" + Folder_Name);
			Log.message("Act Name:" + actualFolderName);

			// Validating the new Folder is created or not
			if (Folder_Name.trim().contentEquals(actualFolderName.trim())) 
			{
				result1 = true;
				Log.message("Expected Folder is available with name: " + Folder_Name);
				break;
			}
		}

		if (result1 == true) 
		{
			// Going to download destination folder
			driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li["+j+"]/section[4]/ul/li[4]/a/i")).click();
			Log.message("Clicked on drop list of expected folder.");
			SkySiteUtils.waitTill(3000);
			//j=j+1;//Newly added
			WebElement Download_Folder = driver.findElement(By.xpath("(//a[@class='folderDownload'])["+j+"]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", Download_Folder);
			Log.message("Clicked on Download Tab for the expected folder.");
			SkySiteUtils.waitTill(30000);

			// Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: " + browserName);

			if (browserName.contains("firefox")) {
				// Handling Download PopUp of firefox browser using robot
				Robot robot = null;
				robot = new Robot();

				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(40000);
			}

			// After Download, checking whether Downloaded file is existed under
			// download folder or not
			String ActualFilename = null;
			File[] files = new File(Sys_Download_Path).listFiles();

			for (File file : files) {
				if (file.isFile()) {
					ActualFilename = file.getName();// Getting File Names into a
													// variable
					Log.message("Actual File name is:" + ActualFilename);
					SkySiteUtils.waitTill(1000);
					Long ActualFileSize = file.length();
					Log.message("Actual File size is:" + ActualFileSize);
					if ((ActualFilename.contains(Folder_Name)) && (ActualFileSize != 0)) {
						Log.message("Downloaded Folder is available in System downloads!!!");
						result2 = true;
						break;
					}

				}

			}

		}
		if ((result1 == true) && (result2 == true))
			return true;
		else
			return false;

	}
	
	/**
	 * Method written for Download a File 
	 * Scripted By: Naresh Babu
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */

	public boolean Download_File(String Sys_Download_Path,String FileName) throws InterruptedException, AWTException 
	{
		boolean result1 = false;
		boolean result2 = false;
		// Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(5000);
		//Get The Count of available files
		int Avl_File_Count = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Doc_')]"));
		for (WebElement Element : allElements) {
		Avl_File_Count = Avl_File_Count + 1;
		}
		Log.message("Available File Count is: " + Avl_File_Count);
		int j=0;
		String actualFileName = null;
		for (j = 1; j <= Avl_File_Count; j++) 
		{
			actualFileName = driver.findElement(By.xpath("(//span[@class='h4_docname document-preview-event'])["+j+"]")).getText();
			Log.message("Exp Name:" + FileName);
			Log.message("Act Name:" + actualFileName);

			// Validating the file is available or not
			if (actualFileName.trim().equalsIgnoreCase(FileName.trim())) 
			{
				result1 = true;
				Log.message("Expected File is available with name: " + actualFileName);
				break;
			}
		}

		if (result1 == true) 
		{
			driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal media-object icon-lg'])["+j+"]")).click();
			Log.message("Click on file drop list.");
			SkySiteUtils.waitTill(2000);
			WebElement Download_FileTab = driver.findElement(By.xpath("(//a[@class='docDownload'])["+j+"]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", Download_FileTab);
			Log.message("Clicked on Download Tab for the expected file drop list.");
			SkySiteUtils.waitTill(30000);

			// Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: " + browserName);

			if (browserName.contains("firefox")) {
				// Handling Download PopUp of firefox browser using robot
				Robot robot = null;
				robot = new Robot();

				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(40000);
			}

			// Validate After Download
			String ActualFilename = null;
			File[] files = new File(Sys_Download_Path).listFiles();

			for (File file : files) {
				if (file.isFile()) {
					ActualFilename = file.getName();//Getting File Names into a variable
					Log.message("Actual File name is:" + ActualFilename);
					SkySiteUtils.waitTill(1000);
					Long ActualFileSize = file.length();
					Log.message("Actual File size is:" + ActualFileSize);
					if ((ActualFilename.contains(FileName)) && (ActualFileSize != 0)) {
						Log.message("Downloaded File is available in System downloads!!!");
						result2 = true;
						break;
					}
				}
			}
		}
		if ((result1 == true) && (result2 == true))
		{
			Log.message("File download is success!!!");
			return true;
		}
		else
		{
			Log.message("File download is Failed!!!");
			return false;
		}
	}
	
	/**
	 * Method written for Delete a File 
	 * Scripted By: Naresh Babu
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */

	public boolean Delete_File(String FileName) throws InterruptedException, AWTException 
	{
		boolean result1 = false;
		//Get The Count of available files
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, btnUploadfile, 30);
		int Avl_File_Count = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Doc_')]"));
		for (WebElement Element : allElements) {
		Avl_File_Count = Avl_File_Count + 1;
		}
		Log.message("Available File Count is: " + Avl_File_Count);
		int j=0;
		String actualFileName = null;
		for (j = 1; j <= Avl_File_Count; j++) 
		{
			actualFileName = driver.findElement(By.xpath("(//span[@class='h4_docname document-preview-event'])["+j+"]")).getText();
			Log.message("Exp Name:" + FileName);
			Log.message("Act Name:" + actualFileName);

			// Validating the file is available or not
			if (actualFileName.trim().equalsIgnoreCase(FileName.trim())) 
			{
				result1 = true;
				Log.message("Expected File is available with name: " + actualFileName);
				break;
			}
		}
		String Msg_AfterFileDelete = null;

		if (result1 == true) 
		{
			driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal media-object icon-lg'])["+j+"]")).click();
			Log.message("Click on file drop list.");
			SkySiteUtils.waitTill(2000);
			WebElement Delete_FileTab = driver.findElement(By.xpath("(//a[@class='btnDeletedoc'])["+j+"]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", Delete_FileTab);
			Log.message("Clicked on Delete Tab for the expected file drop list.");
			SkySiteUtils.waitTill(2000);
			SkySiteUtils.waitForElement(driver, btnYes, 30);
			btnYes.click();
			SkySiteUtils.waitTill(2000);
			SkySiteUtils.waitForElement(driver, notificationMsg, 30);
			Msg_AfterFileDelete = notificationMsg.getText();
			Log.message("Notification message after file delete is: "+Msg_AfterFileDelete);
		}
		String Expected_NotifyMsg = FileName+" deleted successfully";
		if ((result1 == true) && (Msg_AfterFileDelete.contentEquals(Expected_NotifyMsg))
				&& (driver.findElement(By.xpath(".//*[@id='empty-container']/h2")).isDisplayed()))
		{
			Log.message("File Deleted successfully!!!");
			return true;
		}
		else
		{
			Log.message("File Deletion failed!!!");
			return false;
		}
	}
	
	/**
	 * Method written for Download of Latest Documents folder 
	 * Scripted By: Naresh Babu
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean Download_LatestDocuments_Folder(String Sys_Download_Path, String ProjectName) throws InterruptedException, AWTException 
	{
		boolean result1 = false;
		boolean result2 = false;
		// Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(5000);
		String LatestDocumentname=driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[1]/section[2]/h4")).getText();
		if(LatestDocumentname.contentEquals("Latest documents"))
		{
			result1=true;
			Log.message("Folder: "+LatestDocumentname+" is available for download!!!");
			driver.findElement(By.xpath("(//ul[@id='ulDocsFolders']/li/section[4]/ul/li[4]/a/i)[1]")).click();//Click on Latest drop list
			Thread.sleep(2000);
			WebElement FileDownloadLink = driver.findElement(By.xpath("(//a[contains(text(),'Download document')])[2]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",FileDownloadLink);//JSExecutor
			Log.message("Clicked on Download Tab for the LD folder.");
			SkySiteUtils.waitTill(20000);

			// Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: " + browserName);

			if (browserName.contains("firefox")) {
				// Handling Download PopUp of firefox browser using robot
				Robot robot = null;
				robot = new Robot();

				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(40000);
			}

			// After Download, checking whether Downloaded file is existed under download folder or not
			String Expected_FolderName = "LD-"+ProjectName+".zip";
			String ActualFilename = null;
			File[] files = new File(Sys_Download_Path).listFiles();

			for (File file : files) {
				if (file.isFile()) {
					ActualFilename = file.getName();// Getting File Names into a variable
					Log.message("Actual File name is:" + ActualFilename);
					SkySiteUtils.waitTill(1000);
					Long ActualFileSize = file.length();
					Log.message("Actual File size is:" + ActualFileSize);
					if ((ActualFilename.contentEquals(Expected_FolderName)) && (ActualFileSize != 0)) {
						Log.message("Downloaded Folder is available in System downloads!!!");
						result2 = true;
						break;
					}

				}

			}

		}
		else
		{
			result1=false;
			Log.message("LD Folder is NOT available for download!!!");
		}
		if ((result1 == true) && (result2 == true))
			return true;
		else
			return false;

	}
	
	/**
	 * Method written for Validate LD Folder send link
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	public boolean Validate_LD_Folder_SendLink(String Sys_Download_Path, String Expected_FolderName)throws InterruptedException, AWTException 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;

		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 30);
		Log.message("Waiting for Create Folder button to be appeared");
		String LatestDocumentname=driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[1]/section[2]/h4")).getText();
		// Getting Folder count after created a new folder
		int Avl_Fold_Count = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) 
		{
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		int j = 1;
		for (j = 1; j <= Avl_Fold_Count; j++) 
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4")).getText();
			Log.message("Exp Name:" + LatestDocumentname);
			Log.message("Act Name:" + actualFolderName);
			// Validating the Expected Folder is available or not
			if (LatestDocumentname.trim().contentEquals(actualFolderName.trim())) 
			{
				result1 = true;
				Log.message("Expected Folder is Availble with name: " + LatestDocumentname);
				break;
			}
		}

		if (result1 == true) 
		{
			SkySiteUtils.waitTill(3000);
			driver.findElement(By.xpath("(//i[@class='icon icon-ellipsis-horizontal media-object icon-lg'])[" + j + "]")).click();
			Log.message("Clicked on More options icon for the Folder.");
			SkySiteUtils.waitTill(2000);
			WebElement EditLink = driver.findElement(By.xpath("(//a[contains(text(),'Send')])[" + j + "]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", EditLink);// JSExecutor to click send
			Log.message("Clicked on Send Link Tab for the Folder.");
			SkySiteUtils.waitForElement(driver, btnSendFileView, 30);
			Log.message("Waiting for send link window");
			SkySiteUtils.waitTill(3000);
			String Act_SendLinkURL = txtCopyToClipboard.getAttribute("value");//Copy the URL to string
			Log.message("Generated Send Link is: " + Act_SendLinkURL);
			btnContact.click();
			Log.message("Clicked on contact-address book.");
			SkySiteUtils.waitForElement(driver, btnSelectContact, 30);
			Log.message("Waiting for Select Contact window");
			SkySiteUtils.waitTill(2000);
			driver.findElement(By.xpath("(//input[@class='pull-left chklist'])[1]")).click();// Select 1st contact check box
			btnSelectContact.click();// Click on select button
			SkySiteUtils.waitTill(2000);
			txtMessage.click();
			txtMessage.sendKeys("Testing from QA_Team @Automation");
			btnSendFileView.click();// Click on send
			SkySiteUtils.waitTill(1000);
			SkySiteUtils.waitForElement(driver, notificationMsg, 30);
			Log.message("Waiting for notify message could display");
			String NotMsg_SendLink = notificationMsg.getText();
			if (NotMsg_SendLink.contentEquals("Link successfully sent")) 
			{
				Log.message("Folder sendlink is done successfully");
				SkySiteUtils.waitTill(3000);
				driver.get(Act_SendLinkURL);// Launch the URL copied from Clip board
				SkySiteUtils.waitForElement(driver, btnToatalZipDownload, 50);
				Log.message("Waiting for Zip download button to be appeared from sendlink URL");
				
				//Total Zip download from send link url
				result2 = this.Download_ZipFile_SendLinkForMultFIle(Sys_Download_Path,Expected_FolderName);//Validate total zip download
				
				//Single File download from send link url
				String Exp_File_Name = driver.findElement(By.xpath(".//*[@id='sharedDocsFolderList']/li[1]/div/div[1]/h4")).getText();
				Log.message("First File from url is: " + Exp_File_Name);
				result3 = this.Download_SingleFile_SendLinkSubFold(Sys_Download_Path,Exp_File_Name);//Validating download a file from URL
			}

		}

		if ((result1 == true) && (result2 == true) && (result3 == true))
		{
			Log.message("Zip and Single File downloads from url is working.");
			return true;
		} else {
			Log.message("Zip and Single File downloads from url is NOT working.");
			return false;
		}

	}
	
	/** 
	 * Method written for Validate Latest Documents Folder OrderPrints
	 * Scripted By: Naresh Babu 
	 * @return
	 */
	public boolean validate_OrderPrints_LD_Folder(int fileCount) throws InterruptedException, AWTException, IOException
	{	
		boolean result1 = false;
		boolean result2 = false;
		//Getting Folder count and identify the folder
		String LatestDocumentname=driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[1]/section[2]/h4")).getText();
		int Avl_Fold_Count = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		int j = 0;
		for (j = 1; j <= Avl_Fold_Count; j++) 
		{
			actualFolderName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4")).getText();
			Log.message("Exp Name:" + LatestDocumentname);
			Log.message("Act Name:" + actualFolderName);
			// Validating the Expected Folder is available or not
			if (LatestDocumentname.trim().contentEquals(actualFolderName.trim())) 
			{
				result1 = true;
				Log.message("Expected Folder is available with name: "+LatestDocumentname);
				break;
			}
		}
		if(result1 == true)
		{
			driver.findElement(By.xpath("(//ul[@id='ulDocsFolders']/li/section[4]/ul/li[4]/a/i)["+j+"]")).click();
			Log.message("Clicked on folder drop list.");
			SkySiteUtils.waitTill(3000);
			WebElement FolderOrderPrintsLink = driver.findElement(By.xpath("(//a[contains(text(),'Order prints')])["+j+"]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",FolderOrderPrintsLink);//JSExecutor
			Log.message("Clicked on Order Prints button from the list.");
			SkySiteUtils.waitForElement(driver, btnOrderPrintsSubmit, 30);
			
			projectDashboardPage = new ProjectDashboardPage(driver).get();
			result2 = projectDashboardPage.validate_Create_OrderPrints_AndDetails(fileCount);
		}
			
		if((result1==true) && (result2==true))
		{
			Log.message("Order Prints for a LD Folder is working successfully.");
			return true;
		}
		else
		{
			Log.message("Order Prints for a LD Folder is NOT working.");
			return false;
		}
		
	}
	

	/**
	 * Method written for Send Link of Download a folder 
	 * Scripted By: Naresh Babu
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */

	public boolean Download_Folder_ValidateInsideZip(String Sys_Download_Path, String Folder_Name, String FolderPath, int FileCount)
			throws InterruptedException, AWTException, IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;

		SkySiteUtils.waitTill(5000);
		// Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(10000);

		// Getting Folder count after created a new folder
		int Avl_Fold_Count = 0;
		int j = 0;
		String actualFolderName = null;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		for (j = 1; j <= Avl_Fold_Count; j++) {
			actualFolderName = driver
					.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4"))
					.getText();
			// Log.message("Exp Name:"+Folder_Name);
			// Log.message("Act Name:"+actualFolderName);

			// Validating the new Folder is created or not
			if (Folder_Name.trim().contentEquals(actualFolderName.trim())) {
				result1 = true;
				Log.message("Expected Folder is available with name: " + Folder_Name);
				break;
			}
		}

		if (result1 == true) {
			// Going to download destination folder
			driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li[" + j + "]/section[4]/ul/li[4]/a/i")).click();
			Log.message("Clicked on drop list of expected folder.");
			SkySiteUtils.waitTill(3000);
			WebElement EditLink = driver.findElement(By.xpath("(//a[contains(text(),'Download document')])[4]"));
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", EditLink);
			Log.message("Clicked on Download Tab for the expected folder.");
			SkySiteUtils.waitTill(120000);

			// Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: " + browserName);

			if (browserName.contains("firefox")) {
				// Handling Download PopUp of firefox browser using robot
				Robot robot = null;
				robot = new Robot();

				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(40000);
			}

			// After Download, checking whether Downloaded file is existed under
			// download folder or not
			String ActualFilename = null;
			File[] files = new File(Sys_Download_Path).listFiles();

			for (File file : files) {
				if (file.isFile()) {
					ActualFilename = file.getName();// Getting File Names into a
													// variable
					// Log.message("Actual File name is:"+ActualFilename);
					SkySiteUtils.waitTill(1000);
					Long ActualFileSize = file.length();
					// Log.message("Actual File size is:"+ActualFileSize);
					if ((ActualFilename.contains(Folder_Name)) && (ActualFileSize != 0)) {
						Log.message("Downloaded Folder is available in System downloads!!!");
						result2 = true;
						break;
					}

				}

			}

			if (result2 == true) {
				result3 = this.Validate_Files_FromZipFolder(Sys_Download_Path, ActualFilename, FolderPath, FileCount);
			}

		}

		if ((result1 == true) && (result2 == true) && (result3 == true))
			return true;
		else
			return false;

	}

	/**
	 * Method written for Validate files from ZIP folder 
	 * Scripted By: Naresh Babu KAVURU
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	// Validate Files are available in ZIP or not
	public boolean Validate_Files_FromZipFolder(String Sys_Download_Path, String ActualFilename, String FolderPath,
			int FileCount) throws InterruptedException {
		boolean result = false;
		try {
			// Zip File validation
			String ZIP_FILE_NAME = Sys_Download_Path + "\\" + ActualFilename;
			final ZipFile file = new ZipFile(ZIP_FILE_NAME);
			Log.message("Iterating over zip file is : " + ZIP_FILE_NAME);
			int uploadSuccessCount = 0;
			final Enumeration<? extends ZipEntry> entries = file.entries();
			while (entries.hasMoreElements()) {
				final ZipEntry entry = entries.nextElement();
				String FName_FrmZip = entry.getName();
				Log.message("File Name from Zip folder is: " + entry.getName());
				long FileSize_FrmZip = entry.getSize();
				// Log.message("Size of a file from Zip Folder is:
				// "+entry.getSize());
				// System.out.printf("File: %s Size %d %n", entry.getName(),
				// entry.getSize());
				// extractEntry(entry, file.getInputStream(entry));

				// Reading all the file names from input folder
				File[] files = new File(FolderPath).listFiles();
				for (File file1 : files) {
					if (file1.isFile()) {
						String expFilename = file1.getName();// Getting File
																// Names into a
																// variable
						// Log.message("Expected File name is:"+expFilename);
						SkySiteUtils.waitTill(500);

						if ((FName_FrmZip.contains(expFilename)) && (FileSize_FrmZip != 0)) {
							uploadSuccessCount = uploadSuccessCount + 1;
							Log.message("File : " + expFilename + " is available in ZIP Folder then Sucess Count:"
									+ uploadSuccessCount);
							break;
						}
					}

				}

			}

			// Checking whether file count is equal or not
			if (FileCount == uploadSuccessCount) {
				Log.message("All the files are available in ZIP folder.");
				result = true;
			} else {
				Log.message("All the files are NOT available in ZIP folder.");
				result = false;
			}

		} // End of try block

		catch (Exception e) {
			result = false;
		}

		finally {
			return result;
		}
	}

	
	/** 
	 * Method for entering Punch Page for Activity tracking
	 * Scripted By: Ranadeep
	 * @return 
	 */
	
	public PunchPage enterPunchPage() 
	{
		
		WebElement element = driver.findElement(By.xpath("//*[@id='lirfiPunch']/a/i"));
	    JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Document Management button");
		
		WebElement element1 = driver.findElement(By.xpath("//*[@id='liPunch']/a/span"));
	    JavascriptExecutor executor1 = (JavascriptExecutor)driver;
	    executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Punch button");

		return new PunchPage(driver).get();		
	}
	
	/** 
	 * Method for entering RFI Page for Activity tracking
	 * Scripted By: Ranadeep
	 * @return 
	 */
	
	public RFIPage enterRFIPage() 
	{
		
		WebElement element = driver.findElement(By.xpath("//*[@id='lirfiPunch']/a/i"));
	    JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Document Management button");
		
		WebElement element1 = driver.findElement(By.xpath("//*[@id='a_showRFIList']"));
	    JavascriptExecutor executor1 = (JavascriptExecutor)driver;
	    executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on RFI button");

		return new RFIPage(driver).get();		
	}
	
	
	/** 
	 * Method for entering Submittal Page for Activity tracking
	 * Scripted By: Ranadeep
	 * @return 
	 */	
	public SubmitalPage enterSubmittalPage() 
	{
		
		WebElement element = driver.findElement(By.xpath("//*[@id='lirfiPunch']/a/i"));
	    JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Document Management button");
		
		WebElement element1 = driver.findElement(By.xpath("//*[@id='a_showSUBMITTALList']"));
	    JavascriptExecutor executor1 = (JavascriptExecutor)driver;
	    executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Submittal button");

		return new SubmitalPage(driver).get();		
	}
	
	
	/** 
	 * Method for moving inside the file level
	 * Scripted By: Trinanjwan
	 */
	
	@FindBy(css=".icon-include-latest-documents-folder")
	@CacheLookup
	WebElement foldericon;
	
	//@FindBy(xpath = "(//img[@class='img-responsive img-thumbnail' and @alt='drawings'])[1]")
	@FindBy(xpath = "(//img[@class='lazy img-responsive img-thumbnail' and @alt='drawings'])[1]")
	@CacheLookup
	WebElement ImageClick;
	
	public ViewerScreenPage openingfileinViewer()
	
	{
		

		SkySiteUtils.waitForElement(driver, foldericon, 15);
		Log.message("The folder icon is displayed now");
		JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", foldericon);    
		Log.message("Click on folder icon to navigate inside the folder");
		
	
		SkySiteUtils.waitTill(20000);
		SkySiteUtils.waitForElement(driver, ImageClick, 60);
		ImageClick.click();
		SkySiteUtils.waitTill(25000);
		Log.message("Click on the file icon to open the file in the viewer");
		//Log.message("Switched to default content");
				//SkySiteUtils.waitTill(10000);

		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
		driver.switchTo().window(childwindow);
		}
		return new ViewerScreenPage(driver).get();	
		
	}
	
	/** 
	 * Method for opening the file for rotational test cases
	 * Scripted By: Arka Halder
	 */
	
	@FindBy(xpath = "(//img[@class='img-responsive img-thumbnail' and @alt='drawings'])[2]")
	//@FindBy(xpath = "(//img[@class='lazy img-responsive img-thumbnail' and @alt='drawings'])[2]")
	@CacheLookup
	WebElement fullImage;
	
	public ViewerScreenPage openingFullFileInViewer()
	{
		SkySiteUtils.waitForElement(driver, foldericon, 15);
		Log.message("The folder icon is displayed now");
		JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", foldericon);    
		Log.message("Click on folder icon to navigate inside the folder");
		SkySiteUtils.waitTill(20000);
		SkySiteUtils.waitForElement(driver, fullImage, 60);
		fullImage.click();
		SkySiteUtils.waitTill(25000);
		Log.message("Click on the file icon to open the file in the viewer");
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);
		}
		return new ViewerScreenPage(driver).get();
	}
	
	/** 
	 * Method for opening the file for rotational test cases
	 * Scripted By: Arka Halder
	 */
	
	@FindAll({ @FindBy(xpath = "//ul[@id='ulDocsFolders']/li") })
	@CacheLookup
	List<WebElement> allViewerItems;
	
	@FindBy(xpath = "//b[@id='bTotalViewCount']")
	WebElement totalViewCount;
	
	@FindBy(xpath = "//b[@id='bTotalRecordCount']")
	WebElement totalRecordCount;
			
	/** 
	 * Methods for Viewer Sanity Tests to extract the Console Logs after Zoom In
	 * Scripted By: Arka Halder
	 * @return 
	 */
	
	public boolean clickOnViewerSanityFolder() {
		for (int i = 0; i < allViewerItems.size(); i++) {
			if (allViewerItems.get(i).findElement(By.xpath("./section[2]/h4")).getText().equals("Viewer Sanity")) {
				allViewerItems.get(i).findElement(By.xpath("./section/span/i")).click();
				break;
			}
		}
		SkySiteUtils.waitTill(10000);
		return (Integer.valueOf(totalViewCount.getText()).equals(Integer.valueOf(totalRecordCount.getText())));
	}
	
	public boolean displayAllViewerSanityItems() {
		JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
		javascriptExecutor.executeScript("arguments[0].scrollIntoView();", allViewerItems.get(allViewerItems.size() - 1));
		return (Integer.valueOf(totalViewCount.getText()).equals(Integer.valueOf(totalRecordCount.getText())));
	}
	
	public void scrollToTopViewerElement() {
		JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
		javascriptExecutor.executeScript("window.scrollTo(0, 0);");
	}
	
	public int getViewerFileCount() {
		return driver.findElements(By.xpath("//img[@class='lazy img-responsive img-thumbnail']")).size();
	}
	
	public ViewerScreenPage openingViewerFileForConsole(int fileIndex)
	{
		String nameXPath = "//span[@class='h4_docname document-preview-event']";
		Log.message("Opening file in Viewer: " + driver.findElements(By.xpath(nameXPath)).get(fileIndex).getText());
		String imageXPath = "//img[@class='lazy img-responsive img-thumbnail']";
		WebElement thisElement = driver.findElements(By.xpath(imageXPath)).get(fileIndex);
		SkySiteUtils.waitForElement(driver, thisElement, 60);
		JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
		javascriptExecutor.executeScript("arguments[0].scrollIntoView();", thisElement);
		thisElement.click();
		SkySiteUtils.waitTill(25000);
		Log.message("Click on the file icon to open the file in the viewer");
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);
		}
		return new ViewerScreenPage(driver).get();
	}

	/** 
	 * Method for verifying revisions for same files uploaded in a same folder 
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean revisionVerifyInSameFolder() 
	{
		
		
		ArrayList<String> obtainedFileRevisionList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h5_rev']/span"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFileRevisionList.add(we.getText());
		     
			}
		}
		
		Log.message("Obtained Files count before sorting:- " +obtainedFileRevisionList.size());		 
		Log.message("Obtained File Revisions are:- " +obtainedFileRevisionList);
		
		String expectedRevision1= PropertyReader.getProperty("REV1");
		Log.message("Expected Revision 1 is:- " +expectedRevision1);
		
		String expectedRevision2= PropertyReader.getProperty("REV2");
		Log.message("Expected Revision 2 is:- " +expectedRevision2);
		
		String expectedRevision3= PropertyReader.getProperty("REV3");
		Log.message("Expected Revision 3 is:- " +expectedRevision3);
		
		if(expectedRevision1.contains(expectedRevision1) && obtainedFileRevisionList.contains(expectedRevision2) && obtainedFileRevisionList.contains(expectedRevision3))
		{
			return true;	
		}
		else
		{
			return false;
		}
		
	}

	
	/** 
	 * Method for deleting all Files
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public void deleteAllFiles() 
	{
		SkySiteUtils.waitTill(5000);
		
		if(btnUploadfile2.isDisplayed())
		{
			Log.message("No files to be deleted");
		}
		else
		{
			SkySiteUtils.waitForElement(driver, btnSelect, 30);
			WebElement element = driver.findElement(By.xpath("//*[text()='Select']"));
		    JavascriptExecutor executor = (JavascriptExecutor)driver;
		    executor.executeScript("arguments[0].click();", element);
			Log.message("Clicked on Select button");
			
			SkySiteUtils.waitForElement(driver, btnSelectAllFiles, 30);
			btnSelectAllFiles.click();
			Log.message("Clicked on Select all files button");
			
			SkySiteUtils.waitForElement(driver, btnFileMenu, 30);
			btnFileMenu.click();
			Log.message("Clicked on file menu button");
			
			SkySiteUtils.waitForElement(driver, btnDeleteFiles, 30);
			btnDeleteFiles.click();
			Log.message("Clicked on Delete Files button");
			
			SkySiteUtils.waitForElement(driver, btnYesFileOperations, 30);
			btnYesFileOperations.click();
			Log.message("Clicked on YES button to delete files");
			
			SkySiteUtils.waitForElement(driver, btnUploadfile2, 30);
			if(btnUploadfile2.isDisplayed())
			{
				Log.message("All files are deleted");
				
			}
			else
			{
				Log.message("Files deletion failed!!!");
				
			}
			
		}
		
	}

	
	
	/** 
	 * Method for navigating back to the root folder
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public void backToRootFolder() 
	{
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnBreadcrumbProjectRootLevel, 30);
		btnBreadcrumbProjectRootLevel.click();
		Log.message("Clicked on project name in breadcrumb to navigate to Project Root Level");
		
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, latestDocuments, 30);
		if(buttonGallery.isDisplayed())
		{
			Log.message(" Navigation to Project root level is successfull ");
		}
		else
		{
			Log.message(" Navigation to Project root level failed!!! ");
		}
		
	}

	
	/** 
	 * Method for verifying revisions for same files uploaded in a different folder 
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean revisionVerifyInDifferentFolder() 
	{
		
		ArrayList<String> obtainedFileRevisionList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h5_rev']/span"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFileRevisionList.add(we.getText());
		     
			}
		}
		
		Log.message("Obtained Files count before sorting:- " +obtainedFileRevisionList.size());		 
		Log.message("Obtained File Revisions are:- " +obtainedFileRevisionList);
		
		String expectedRevision = PropertyReader.getProperty("REV3");
		Log.message("Expected Revision 3 is:- " +expectedRevision);
		
		if(obtainedFileRevisionList.contains(expectedRevision))
		{
			return true;	
		}
		else
		{
			return false;
		}
		
	}

	
	/** 
	 * Method for fetching revision list 
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public ArrayList<String> fetchRevision() 
	{
		
		SkySiteUtils.waitTill(8000);
		ArrayList<String> obtainedFileRevisionList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h5_rev']/span"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFileRevisionList.add(we.getText());
		     
			}
		}
		
		Log.message("Obtained Files count before sorting:- " +obtainedFileRevisionList.size());		 
		Log.message("Obtained File Revisions are:- " +obtainedFileRevisionList);
		
		return obtainedFileRevisionList;
	}

	
	
	
	/** 
	 * Method for fetching revision list 
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public void deleteLatestRevision() 
	{
		
		SkySiteUtils.waitTill(8000);
		ArrayList<String> obtainedFileRevisionList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h5_rev']/span"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFileRevisionList.add(we.getText());
		     
			}
		}
		
		Log.message("Obtained Files count:- " +obtainedFileRevisionList.size());		 
		Log.message("Obtained File Revisions are:- " +obtainedFileRevisionList);
		
		String latestRevision = PropertyReader.getProperty("REV3");
		Log.message("The latest file revision is :- " +latestRevision);
	
		int count = obtainedFileRevisionList.size();
		
		for(int i=0; i<count; i++)
		{
			if(obtainedFileRevisionList.get(i).contains(latestRevision))
			{
				driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li["+count+"+"+1+"]/input")).click();
				Log.message("Clicked on latest revision checkbox");
			}
			
		}
			
		SkySiteUtils.waitForElement(driver, dotedMenuFiles, 30);
		dotedMenuFiles.click();
		Log.message("Clicked on dotted menu for Files");
		
		SkySiteUtils.waitForElement(driver, btnDeleteFiles, 30);
		btnDeleteFiles.click();
		Log.message("Clicked on Delete button to delete selected Files");
		
		SkySiteUtils.waitForElement(driver, btnYesFileOperations, 30);
		btnYesFileOperations.click();
		Log.message("Clicked on Yes button in the delete popover");
		
		SkySiteUtils.waitTill(8000);
		ArrayList<String> obtainedFileRevisionList1 = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@class='h5_rev']/span"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFileRevisionList1.add(we.getText());
		     
			}
		}
		
		Log.message("Obtained Files count after deletion:- " +obtainedFileRevisionList1.size());		 
		Log.message("Obtained File Revisions are:- " +obtainedFileRevisionList1);
			
		if(obtainedFileRevisionList1.size()<count && !obtainedFileRevisionList1.contains(latestRevision))
		{
			Log.message("Latest revision file is deleted");
		}
		else
		{
			Log.message("Latest revision file is deletion failed!!!");
		}
				
	}

	
	/** 
	 * Method for opening the source hyperlink file
	 * Scripted By: Trinanjwan
	 */

	@FindBy(xpath = "(//img[@class='img-responsive img-thumbnail' and @alt='drawings'])[2]")
	@CacheLookup
	WebElement ImageClick1;
	
	
	@FindBy(xpath = "//*[text()='Folder 1']")
	@CacheLookup
	WebElement folder1;
	
	public ViewerScreenPage openingsourcehyperlink()
	
	{
		

		SkySiteUtils.waitForElement(driver, folder1, 30);
		Log.message("The folder icon is displayed now");
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", folder1);    
		Log.message("Click on folder icon to navigate inside the folder");
		
	
		SkySiteUtils.waitTill(20000);
		SkySiteUtils.waitForElement(driver, ImageClick1, 60);
		ImageClick1.click();
		SkySiteUtils.waitTill(25000);
		Log.message("Click on the file icon to open the file in the viewer");
		//Log.message("Switched to default content");
				//SkySiteUtils.waitTill(10000);

		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
		driver.switchTo().window(childwindow);
		}
		return new ViewerScreenPage(driver).get();	
		
	}

	
	/** 
	 * Method for downloading latest revision
	 * Scripted By: Ranadeep
	 *  
	 */
	public void deleteAllFilesInDownloadFolder(String DownloadPath)
	{
	
		try 
		{ 
			Log.message("Going to Clean existed files from download folder!!!");
			
			File[] files = new File(DownloadPath).listFiles(); 
			for(File file : files)  
			{
				if (!file.isDirectory())
				{
					file.delete();
				}			        
			}
			
			Log.message("Available Folders/Files are deleted from download folder successfully!!!");
			
		}
		
		catch(Exception e)
		{
			Log.message("Unable to delete Available Folders/File from download folder!!!"+e);			
		}
		
	}
	
	
	/** 
	 * Method for downloading latest revision
	 * Scripted By: Ranadeep
	 * @return 
	 * @throws InterruptedException 
	 */
	public boolean latestRevisionDownload(String DownloadPath, String FolderName) throws InterruptedException 
	{
	
		boolean result = false;
		SkySiteUtils.waitTill(8000);
		ArrayList<String> obtainedFileRevisionList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h5_rev']/span"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFileRevisionList.add(we.getText());
		     
			}
		}
		
		Log.message("Obtained Files count:- " +obtainedFileRevisionList.size());		 
		Log.message("Obtained File Revisions are:- " +obtainedFileRevisionList);
		
		String latestRevision = PropertyReader.getProperty("REV3");
		Log.message("The latest file revision is :- " +latestRevision);
	
		int count = obtainedFileRevisionList.size();
		
		for(int i=0; i<count; i++)
		{
			if(obtainedFileRevisionList.get(i).contains(latestRevision))
			{
				driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li["+count+"+"+1+"]/input")).click();
				Log.message("Clicked on latest revision checkbox");			
			}
			
		}
		
		//***************Delete All Files from the Download Folder***************//
		this.deleteAllFilesInDownloadFolder(DownloadPath);
		SkySiteUtils.waitTill(8000);
	
		SkySiteUtils.waitForElement(driver, btnFileMenu, 30);
		btnFileMenu.click();
		Log.message("Clicked on file menu button");
		
		SkySiteUtils.waitForElement(driver, btnDownlodFile, 30);
		btnDownlodFile.click();
		Log.message("Clicked on Download File button");
		SkySiteUtils.waitTill(40000);

	    String ActualFilename = null;
	    File[] files = new File(DownloadPath).listFiles();

	    for(File file : files) 
	    {    	
	    	if (file.isFile()) 
	    	{	    				
	    		ActualFilename = file.getName();
	    		Log.message("Actual File name is:" + ActualFilename);
	    		SkySiteUtils.waitTill(1000);
	    		Long ActualFileSize = file.length();
	    		Log.message("Actual File size is:" + ActualFileSize);
	    		if ((ActualFilename.contains("1-files-" + FolderName)) && (ActualFileSize != 0)) 
	    		{	    					
	    			Log.message("Downloaded file is available in downloads!!!");
	    			result = true;
	    		}
	    		
	    	}
	    }
	    if (result == true)
			return true;
		else
			return false;
	}

	
	
	
	/** 
	 * Method for adding a folder to Print Cart
	 * Scripted By: Ranadeep
	 *  
	 */
	public String addFolderToPrintCart() 
	{
		SkySiteUtils.waitTill(5000);
		
		String expectedFolder = PropertyReader.getProperty("FolderSelectPrintCart");
		Log.message("The folder to be selected to add in Print Cart is :- " +expectedFolder);
		
		ArrayList<String> obtainedFolderList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@data-original-title='Project file/folder name']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFolderList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Folder count is:- " +obtainedFolderList.size());
		
		int count = obtainedFolderList.size();
		String actualFilesCountInFolder=null;
		
		this.changeFolderViewGridToList();
		
		for(int i=0; i<count; i++)
		{
			if(obtainedFolderList.get(i).contains(expectedFolder))
			{
				
				actualFilesCountInFolder= driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li["+i+"+"+3+"]/section[2]/h5")).getText();
				Log.message("Number of files in the selected folder is:- " +actualFilesCountInFolder);
				SkySiteUtils.waitTill(5000);

				driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li["+i+"+"+3+"]/section[4]/ul/li[2]/a/i")).click();
				Log.message("Clicked on Add to Print Cart button for folder " +expectedFolder);				
				SkySiteUtils.waitTill(10000);
				
			}
			
		}		
		
		this.changeFolderViewListToGrid();
		
		SkySiteUtils.waitForElementLoadTime(driver, btnPrintCart, 30);
		btnPrintCart.click();
		Log.message(" Clicked on Print cart button ");	
		
		
		return actualFilesCountInFolder.split("\\s")[0];
				
	}

	
	
	/** 
	 * Method for entering Print Cart page
	 * Scripted By: Ranadeep
	 *  
	 */
	public ProjectPrintCartPage enterPrintCart() 
	{
		SkySiteUtils.waitTill(8000);
		
		SkySiteUtils.waitForElementLoadTime(driver, btnPrintCart, 30);
		btnPrintCart.click();
		Log.message(" Clicked on Print cart button ");		
		
		return new ProjectPrintCartPage(driver).get();
	}
	
	
	
	/** 
	 * Method for adding all files in a folder to Print Cart
	 * Scripted By: Ranadeep
	 *  
	 */
	public int addAllFilesInFolderToPrintCart() 
	{
		
		SkySiteUtils.waitForElement(driver, btnSelect, 30);
		WebElement element = driver.findElement(By.xpath("//*[text()='Select']"));
	    JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Select button");
		
		SkySiteUtils.waitForElement(driver, btnSelectAllFiles, 30);
		btnSelectAllFiles.click();
		Log.message("Clicked on Select all files button");
		
		SkySiteUtils.waitForElement(driver, btnFileMenu, 30);
		btnFileMenu.click();
		Log.message("Clicked on file menu button");
		
		SkySiteUtils.waitForElementLoadTime(driver, addToPrintCart, 30);
		addToPrintCart.click();
		Log.message("Clicked on Add to Print cart button");
		
		SkySiteUtils.waitForElementLoadTime(driver, btnBackClearSelection, 30);
		WebElement element1 = driver.findElement(By.xpath("//*[text()='Back']"));
	    JavascriptExecutor executor1 = (JavascriptExecutor)driver;
	    executor1.executeScript("arguments[0].click();", element1);
		SkySiteUtils.waitTill(6000);
		
		SkySiteUtils.waitForElementLoadTime(driver, cartItemsCount, 30);
		String count=cartItemsCount.getText();
		int count1= Integer.parseInt(count);
		
		ArrayList<String> obtainedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h4_docname document-preview-event']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFileList.add(we.getText()); 
		     
			}
		}
		
		if(count1 == obtainedFileList.size() )
		{
			Log.message("All files are added to Print Cart");
			
		}
		else
		{
			Log.message("All files are not added to Print Cart");
			
		}
		
		return count1;
		
	}
	
	
	/** 
	 * Method for opening parent hyperlink file
	 * Scripted By: Trinanjwan
	 */

	//@FindBy(xpath = "(//img[@class='img-responsive img-thumbnail' and @alt='drawings'])[1]")
	@FindBy(xpath = "(//img[@class='lazy img-responsive img-thumbnail' and @alt='drawings'])[1]")
	@CacheLookup
	WebElement ImageClickparentfile;
	
	
	@FindBy(xpath = "//*[text()='Parent']")
	@CacheLookup
	WebElement parentfolder;
	
	public ViewerScreenPage openingsourcehyperlinkparent()
	
	{
		

		SkySiteUtils.waitForElement(driver, parentfolder, 30);
		Log.message("The folder icon is displayed now");
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", parentfolder);    
		Log.message("Click on folder icon to navigate inside the folder");
		
	
		SkySiteUtils.waitTill(20000);
		SkySiteUtils.waitForElement(driver, ImageClickparentfile, 60);
		ImageClickparentfile.click();
		SkySiteUtils.waitTill(25000);
		Log.message("Click on the file icon to open the file in the viewer");
		//Log.message("Switched to default content");
				//SkySiteUtils.waitTill(10000);

		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
		driver.switchTo().window(childwindow);
		}
		return new ViewerScreenPage(driver).get();	
		
	}

	
	/** 
	 * Method for adding all files in a folder to Print Cart
	 * Scripted By: Ranadeep
	 *  
	 */
	public ArrayList<String> filesAddedToPrintCart() 
	{
		
		ArrayList<String> obtainedFileList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@class='h4_docname document-preview-event']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFileList.add(we.getText()); 
		     
			}
		}
		
		Log.message("Obtained files count is:- " +obtainedFileList.size());		 
		Log.message("Obtained Files are:- " +obtainedFileList);
		
		SkySiteUtils.waitTill(5000);
		
		return obtainedFileList;
		
	}
	
	
	/** 
	 * Method to validate addition of empty folder in the Print Cart
	 * Scripted By: Ranadeep
	 *  
	 */
	public boolean addEmptyFolderToPrintCart() 
	{
		SkySiteUtils.waitTill(5000);
		
		String expectedFolder = PropertyReader.getProperty("EmptyFolderSelectPrint");
		Log.message("The folder to be selected to add in Print Cart is :- " +expectedFolder);
		
		ArrayList<String> obtainedFolderList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@data-original-title='Project file/folder name']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFolderList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Folder count is:- " +obtainedFolderList.size());
		
		int count = obtainedFolderList.size();
		String actualFilesCountInFolder=null;
		
		this.changeFolderViewGridToList();
		
		for(int i=0; i<count; i++)
		{
			if(obtainedFolderList.get(i).contains(expectedFolder))
			{
				
				actualFilesCountInFolder= driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li["+i+"+"+3+"]/section[2]/h5")).getText();
				Log.message("Number of files in the selected folder is:- " +actualFilesCountInFolder);
				SkySiteUtils.waitTill(5000);

				driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li["+i+"+"+3+"]/section[4]/ul/li[2]/a/i")).click();
				Log.message("Clicked on Add to Print Cart button for folder " +expectedFolder);				
				
				
			}
			
		}		
		
		SkySiteUtils.waitForElement(driver, validationMessage, 30);
		String actualFolderAddToCartValidationMessage= validationMessage.getText();
		Log.message(" Actual folder addition validation message:- " +actualFolderAddToCartValidationMessage);
		
		String expectedFolderAddToCartValidationMessage = PropertyReader.getProperty("EmptyFolderAddCartValidationMessage");
		Log.message(" Expected folder addition validation message :- " +expectedFolderAddToCartValidationMessage);
		
		this.changeFolderViewListToGrid();
		
		if(actualFolderAddToCartValidationMessage.equals(expectedFolderAddToCartValidationMessage) && !btnPrintCart.isDisplayed())
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}

	
	/** 
	 * Method to add of folders in the Print Cart and get count
	 * Scripted By: Ranadeep
	 *  
	 */
	public ArrayList<String> addFoldersToPrintCart() 
	{
		
		SkySiteUtils.waitTill(5000);
	
		ArrayList<String> obtainedFolderList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@data-original-title='Project file/folder name']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedFolderList.add(we.getText()); 
		     
			}
		}	
		Log.message("Obtained Folder count is:- " +obtainedFolderList.size());
		
		int count = obtainedFolderList.size();
		
		this.changeFolderViewGridToList();
		ArrayList<String> obtainedFolderList1 = new ArrayList<>();
		
		String arr = PropertyReader.getProperty("EmptyFolderFileContent");
		Log.message("Empty folder files comntent :- " +arr);
		
		for(int i=0; i<count; i++)
		{
			
			driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li["+i+"+"+3+"]/section[4]/ul/li[2]/a/i")).click();		
			SkySiteUtils.waitTill(8000);
			
			if(!driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li["+i+"+"+3+"]/section[2]/h5")).getText().contains("(Empty)"))
			{
				obtainedFolderList1.add(driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div[2]/ul/li["+i+"+"+3+"]/section[2]/h4")).getText());
				
			}
			
		}
						
		this.changeFolderViewListToGrid();
		Log.message("Folders added in the Print cart:- " +obtainedFolderList1);
		
		return obtainedFolderList1;
		
	}

	
	
	/** 
	 * Method for opening child hyperlink file, this will be the source file
	 * Scripted By: Trinanjwan
	 */

	//@FindBy(xpath = "(//img[@class='img-responsive img-thumbnail' and @alt='drawings'])[1]")
	@FindBy(xpath = "(//img[@class='lazy img-responsive img-thumbnail' and @alt='drawings'])[1]")
	@CacheLookup
	WebElement ImageClickchildfile;
	

	
	@FindBy(xpath = "//*[text()='Child']")
	@CacheLookup
	WebElement childfolder;
	
	public ViewerScreenPage openingsourcehyperlinkchild()
	
	{
		

		SkySiteUtils.waitForElement(driver, parentfolder, 30);
		Log.message("The folder icon is displayed now");
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", parentfolder);    
		Log.message("Click on folder icon to navigate inside the folder");
		
		SkySiteUtils.waitForElement(driver, childfolder, 30);
		Log.message("The folder icon is displayed now for the child folder");
		executor.executeScript("arguments[0].click();", childfolder);    
		Log.message("Click on folder icon to navigate inside the child folder");
		
		
	
		SkySiteUtils.waitTill(20000);
		SkySiteUtils.waitForElement(driver, ImageClickchildfile, 60);
		ImageClickchildfile.click();
		SkySiteUtils.waitTill(25000);
		Log.message("Click on the file icon to open the file in the viewer");
		//Log.message("Switched to default content");
				//SkySiteUtils.waitTill(10000);

		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
		driver.switchTo().window(childwindow);
		}
		return new ViewerScreenPage(driver).get();	
		
	}
	
	
	//Sample method by naresh
	public String samplevalue()
	{
		String v1 = "naresh";
		return v1;
	}

	/** 
	 * Method to validate project management is disabled for basic package
	 * Scripted By: Trinanjwan
	 */

	@FindBy(xpath = "//span[text()='Project management']")
	WebElement projectManagementbtn;
	
	@FindBy(xpath = "//span[text()='Punch']")
	WebElement punchProjectManagement;
	
	@FindBy(xpath = "//i[@class='icon icon-latest-documents icon-3x media-object']")
	WebElement latestDocumenticon;
    
	public boolean validateProjectManagement(WebElement prjmng)
	
	{
	SkySiteUtils.waitTill(15000);
	SkySiteUtils.waitForElement(driver, projectManagementbtn, 40);
	Log.message("Project Management is visible now");
	projectManagementbtn.click();
	Log.message("Clicked on Project Management button");
	SkySiteUtils.waitTill(5000);
	prjmng.click();
	Log.message("Clicked on the project management icon");
	SkySiteUtils.waitTill(5000);
	if(latestDocumenticon.isDisplayed())
	{
		Log.message("The latest document icon is displayed now");
		return true;
	}
	else
	{
		Log.message("The latest document icon is displayed now");
		return false;
	}
		
	}
	
	
	/** 
	 * Method to land into the Projects Settings Page from the Folder page
	 * Scripted By: Trinanjwan 
	 */	
	@FindBy(xpath="//img[@class='img-circle  img-responsive hoverZoomLink']")
	WebElement profileIcon;
	
	@FindBy(xpath="//i[@class='icon icon-cog']/..")
	WebElement settingsoption;
		
	public Projects_SettingsPage navigatetoSettings()
	{
		
		SkySiteUtils.waitTill(7000);
		Actions ac=new Actions(driver);
		ac.moveToElement(profileIcon).click(profileIcon).build().perform();
		Log.message("Profile icon is clicked now");
		SkySiteUtils.waitForElement(driver, settingsoption, 40);
		Log.message("Settings option is visible now");
		settingsoption.click();
		Log.message("Clicked on Settings option");
		
		return new Projects_SettingsPage(driver).get();	
		
	}
	
	
	/** 
	 * Method to validate gallery is disabled for basic package
	 * Scripted By: Trinanjwan
	 */
	
	@FindBy(xpath="//i[@class='icon icon-photo_mgmt icon-3x pw-icon-green media-object']")
	WebElement galleryicon;

	public boolean validateGallery() throws Exception
	{
	try 
	{
	SkySiteUtils.waitTill(15000);
	driver.findElement(By.xpath("//i[@class='icon icon-photo_mgmt icon-3x pw-icon-green media-object']"));
		return false;
	
	}
	catch (org.openqa.selenium.NoSuchElementException e) {
		Log.message("Element is not present");
        return true;

	}


}
	
	

	/** 
	 * Method to enter any folder
	 * Scripted By: Trinanjwan Sarkar
	 */
	public void enteringanyFolder(String foldername)
	
	{		
		SkySiteUtils.waitTill(15000);
		String folderpath = "//h4[text()='%s']";
		SkySiteUtils.waitTill(5000);
		WebElement folderpathelement=driver.findElement(By.xpath(String.format(folderpath, foldername)));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", folderpathelement);    
        Log.message(" Clicked on folder " + foldername + " to navigate inside ");

	}
	
	/** 
	 * Method to open any file in viewer by passing the file name as String
	 * Scripted By: Trinanjwan Sarkar
	 */

	
	public ViewerScreenPage openinganyfileinViewer(String filename)
	
	{

		SkySiteUtils.waitTill(20000);
		String fileicon = "//span[text()='%s']";
		SkySiteUtils.waitTill(5000);
		WebElement filepathelement=driver.findElement(By.xpath(String.format(fileicon, filename)));
		SkySiteUtils.waitForElement(driver, filepathelement, 60);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", filepathelement);
		SkySiteUtils.waitTill(25000);
		Log.message("Click on the file icon to open the file in the viewer");
		//Log.message("Switched to default content");
				//SkySiteUtils.waitTill(10000);

		String MainWindow = driver.getWindowHandle();
		Log.message("The main window is: "+MainWindow);
		System.setProperty("WindowID", MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
		driver.switchTo().window(childwindow);
		}
		return new ViewerScreenPage(driver).get();	
		
	}
	
	
	/** 
	 * Method to validate Autosave autohyperlink is disabled for basic package
	 * Scripted By: Trinanjwan
	 */
	
	@FindBy(xpath="//a[text()='Autosave autohyperlink']")
	WebElement Autosaveautohyperlinkbtn;

	public boolean validateAutosaveautohyperlink() throws Exception
	{
	try 
	{
	SkySiteUtils.waitTill(15000);
	driver.findElement(By.xpath("//a[text()='Autosave autohyperlink']"));
		return false;
	
	}
	catch (org.openqa.selenium.NoSuchElementException e) {
		Log.message("Element is not present");
        return true;

	}


}

	
	/** 
	 * Method to validate able to navigate to Punch module after clicking on Punch icon for enterprise package
	 * Scripted By: Trinanjwan
	 */

	@FindBy(xpath="//span[text()='Punch']")
	WebElement punchicon;
    
	public PunchPage validateProjectManagementPunch()
	
	{
	SkySiteUtils.waitTill(15000);
	SkySiteUtils.waitForElement(driver, projectManagementbtn, 40);
	Log.message("Project Management is visible now");
	projectManagementbtn.click();
	Log.message("Clicked on Project Management button");
	SkySiteUtils.waitTill(5000);
	punchicon.click();
	Log.message("Clicked on the Punch icon");
	SkySiteUtils.waitTill(10000);
	return new PunchPage(driver).get();

	}
	
	
	/** 
	 * Method to validate able to navigate to RFI module after clicking on RFI icon for enterprise package
	 * Scripted By: Trinanjwan
	 */

	@FindBy(xpath="//span[text()='RFI']")
	WebElement rfiicon;
    
	public RFIPage validateProjectManagementRFI()
	
	{
	SkySiteUtils.waitTill(15000);
	SkySiteUtils.waitForElement(driver, projectManagementbtn, 40);
	Log.message("Project Management is visible now");
	projectManagementbtn.click();
	Log.message("Clicked on Project Management button");
	SkySiteUtils.waitTill(5000);
	rfiicon.click();
	Log.message("Clicked on the RFI icon");
	SkySiteUtils.waitTill(10000);
	return new RFIPage(driver).get();

	}

		
	
	/** 
	 * Method to validate able to navigate to Submittal module after clicking on Submittal icon for enterprise package
	 * Scripted By: Trinanjwan
	 */

	@FindBy(xpath="//span[text()='Submittal']")
	WebElement submittalicon;
    
	public SubmitalPage validateProjectManagementSubmittal()
	
	{
	SkySiteUtils.waitTill(15000);
	SkySiteUtils.waitForElement(driver, projectManagementbtn, 40);
	Log.message("Project Management is visible now");
	projectManagementbtn.click();
	Log.message("Clicked on Project Management button");
	SkySiteUtils.waitTill(5000);
	submittalicon.click();
	Log.message("Clicked on the Submittal icon");
	SkySiteUtils.waitTill(10000);
	return new SubmitalPage(driver).get();

	}
	
	
	
	/** 
	 * Method to validate gallery is enabled for enterprise package
	 * Scripted By: Trinanjwan
	 */

	public boolean validateGalleryenterprise() throws Exception
	{
	
		SkySiteUtils.waitForElement(driver, galleryicon, 50);
		if(galleryicon.isDisplayed())
		{
		Log.message("Gallery folder is displayed");
		return true;
		}
		else
		{
		Log.message("Gallery folder is not displayed");
		return false;
		}
		
		
	}
	
	
	/** 
	 * Method to validate Email Archive is disabled for basic package
	 * Scripted By: Trinanjwan
	 */
	
	@FindBy(xpath="//i[@class='icon icon-3x icon-project-email pw-icon-dk-orange']")
	WebElement emailarchiveicon;

	public boolean validateemailarchive() throws Exception
	{
	try 
	{
	SkySiteUtils.waitTill(15000);
	driver.findElement(By.xpath("//i[@class='icon icon-3x icon-project-email pw-icon-dk-orange']"));
		return false;
	
	}
	catch (org.openqa.selenium.NoSuchElementException e) {
		Log.message("Element is not present");
        return true;

	}


}
	/** 
	 * Method to validate Email Archive and Autosave auto hyperlink is enabled for enterprise package
	 * Scripted By: Trinanjwan
	 */
	
	public boolean validateenterEmail_Auto(String xpathexp)
	
	{
		
		SkySiteUtils.waitTill(15000);
		WebElement element=driver.findElement(By.xpath(xpathexp));
		if(element.isDisplayed())
		{
		Log.message("Element is displayed");
		return true;
		}
		
		else
		{
		Log.message("Element is not displayed");
		return false;
			
		}

	}
	
	/** 
	 * Method to validate Upgrade package pop over is present
	 * Scripted By: Trinanjwan
	 */

	@FindBy(xpath="//button[text()='Upgrade now']")
	WebElement upgradenowbtn;
	
	@FindBy(xpath="//h4[text()='Upgrade package']//preceding-sibling::button")
	WebElement upgradepackagepopoverclosebutton;
	
	
	
	public boolean Upgradepackagepopoverispresent()
	
	
	{
		
		SkySiteUtils.waitForElement(driver, upgradenowbtn, 50);
		
		if(upgradenowbtn.isDisplayed())
		{
			upgradepackagepopoverclosebutton.click();
			Log.message("Clicked on close button");
			Log.message("Upgrade package popover is displayed");
			return true;
		}
		else
		{
			upgradepackagepopoverclosebutton.click();
			Log.message("Clicked on close button");
			Log.message("Upgrade package popover is not displayed");
			return false;	
	}
	}

	
	/** 
	 * Method to validate Autosave autohyperlink is enabled for enterprise package
	 * Scripted By: Trinanjwan
	 */
	
	

	public boolean validateAutosaveautohyperlinkispresent() throws Exception
	{
		SkySiteUtils.waitForElement(driver, Autosaveautohyperlinkbtn, 60);
		if(Autosaveautohyperlinkbtn.isDisplayed())
		{
		Log.message("Autosave autohyperlink btn is displayed");	
		return true;
		}
		else
		{
		Log.message("Autosave autohyperlink btn is not displayed");	
		return false;
			
		}
		
		
	}

	/**
	 * Method written for Move a folder to a destination folder 
	 * Scripted By: Ranjan
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */
	// Validate Download file And size
	public boolean Assignee_levelDownloadFiledownload(String Downloadpath) throws InterruptedException, AWTException, IOException {

		boolean result1 = false;
		SkySiteUtils.waitTill(10000);
		// Calling "Deleting download folder files" method
		SkySiteUtils.waitForElement(driver, RFIAssigntometab, 30);
		RFIAssigntometab.click();
		SkySiteUtils.waitTill(5000);
		String RFINo = RFINumber_Display.getText();
		String fileName ="RFI "+ RFINo +".pdf";
		Log.message("Download Path is: " + Downloadpath);
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Downloadpath);
		SkySiteUtils.waitTill(10000);
		driver.findElement(By.xpath("(//i [@class='icon icon-lg icon-download-alt'])[1]")).click();
		Log.message("Clicked on download icon from RFI creationed icon for download- Files Downloded Sucessfully");

		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);

		if (browserName.contains("firefox")) {
			// Handling Download PopUp of firefox browser using robot
			Robot robot = null;
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(30000);
		}
		SkySiteUtils.waitTill(40000);
		// After Download, checking whether Downloaded file is existed under
		// download folder or not
		String ActualFilename = null;
		File[] files = new File(Downloadpath).listFiles();
		
		//File dwFile = new File(Downloadpath) ;
		//Log.message("===============start clean ====================");
		//SkySiteUtils.waitTill(50000);
		//FileUtils.cleanDirectory(dwFile);
		
		for (File file : files)
		{
			if (file.isFile()) 
			{
				ActualFilename = file.getName();// Getting Folder Name into a variable
				Log.message("Actual Folder name is:" + ActualFilename);
				SkySiteUtils.waitTill(1000);
				Long ActualFolderSize = file.length();
				Log.message("Actual Folder size is:" + ActualFolderSize);
				SkySiteUtils.waitTill(5000);
				if (ActualFolderSize != 0) 
				{
					Log.message("Downloaded folder is available in downloads!!!");
				} 
				else
				{
					Log.message("Downloaded folder is NOT available in downloads!!!");
				}
			}
		}
		if (ActualFilename.contains(fileName))
			return true;
		else
			return false;		
	}	


}