package com.arc.projects.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;



public class ProjectsLoginPage extends LoadableComponent<ProjectsLoginPage> {
	
	WebDriver driver;
	private boolean isPageLoaded;
	
	int i=0;
	
	/**
	 * Identifying web elements using FindBy annotation.
	 */
	
	
	
	@FindBy(xpath="//button [@class='btn btn-primary center-block']")
	WebElement popUP;
	
	@FindBy(xpath="	//button[@ id='btnfeedbackClose' and @class='close']")
	WebElement feedback_popupclose;
	
	@FindBy(xpath="//*[@class='flipform']")
	WebElement btnRegister;
	
	//button[@ id='btnfeedbackClose' and @class='close']
	@FindBy(css="#UserID")
	
	WebElement txtBoxUserName;
	
	@FindBy(css="#Password")
	
	WebElement txtBoxPassword;
	
	@FindBy(css="#btnLogin")
	
	WebElement btnLogin;
	
	@FindBy(css=".btn.btn-primary.center-block")
	
	WebElement btnGotItBanner;
	
	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, txtBoxUserName, 20);
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}
	
	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * @param driver
	 * @return 
	 */
	public ProjectsLoginPage(WebDriver driver) 
	{
		   this.driver = driver;
		   PageFactory.initElements(this.driver, this);
	}
	
	
	
	/** 
	 * Method written for checking whether User Name text box is present?
	 * @return
	 */
	public boolean loginProjects()
	{
		SkySiteUtils.waitForElement(driver, txtBoxUserName, 20);
		Log.message("Waiting for Username text box to be appeared");
		if(txtBoxUserName.isDisplayed())
			return true;
			else
			return false;
	}
	
	/** 
	 * Method written for doing login with valid credential.
	 * Scripted By : NARESH BABU
	 * @throws AWTException 
	 */
	public ProjectDashboardPage loginWithValidCredential(String uName,String pWord) throws AWTException
	{
		
		  SkySiteUtils.waitTill(3000);
	        String currentURL = driver.getCurrentUrl();
	        Log.message("Current url is:- "+currentURL);
	        
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, txtBoxUserName, 30);
		SkySiteUtils.waitTill(3000);
		if(popUP.isDisplayed()){
		popUP.click();
		}
		 //SkySiteUtils.waitForElement(driver, btnGotItBanner, 30);
		 //SkySiteUtils.waitTill(3000);
		 //btnGotItBanner.click();
		 //Log.message("Clicked on Got it from Maintanance banner.");
		SkySiteUtils.waitForElement(driver, txtBoxUserName, 20);
		//String uName = PropertyReader.getProperty("Username");
		txtBoxUserName.clear();
		txtBoxUserName.sendKeys(uName);
		Log.message("Username: " + uName + " " + "has been entered in Username text box." );
		//String pWord = PropertyReader.getProperty("Password");
		txtBoxPassword.clear();
		txtBoxPassword.sendKeys(pWord);
		Log.message("Password: " + pWord + " " + "has been entered in Password text box." );
		SkySiteUtils.waitForElement(driver, btnLogin, 20);
		btnLogin.click();
		Log.message("LogIn button clicked.");
		SkySiteUtils.waitTill(15000);
		//new code to launch project module
		List<WebElement> ele = driver.findElements(By.xpath(".//*[@id='ProjectFiles']/a"));
		if(ele.size() > 0)
{
	        driver.findElement(By.xpath(".//*[@id='nvHomeAndSSProjLink']/li/a/span")).click();
	        Log.message("Drop down is clicked");
	        SkySiteUtils.waitTill(4000);
	        driver.findElement(By.xpath(".//*[@id='liSSProjLink']/a/i")).click();
	        Log.message("Project button is clicked");
	        SkySiteUtils.waitTill(4000);
}
		else {
			
			Log.message("Project page is landed");
		}
		int Feedback_Alert = driver.findElements(By.xpath("//span[@id='FeedbackClose']")).size();
		Log.message("Checking feedback alert is there or not : "+ Feedback_Alert);
		
		if(Feedback_Alert>0)
		{
			driver.findElement(By.xpath("//span[@id='FeedbackClose']")).click();
			Log.message("Clicked on 'Not right now' link from feedback!!!");
			SkySiteUtils.waitTill(5000);
		}
			
		return new ProjectDashboardPage(driver).get();
	}
	
	/** 
	 * Method written for doing login with export related credential.
	 * @throws AWTException 
	 */
	public ProjectDashboardPage loginToExportAccount() throws AWTException
	{
		 SkySiteUtils.waitTill(5000);
		//Robot robot = new Robot();
		//robot.keyPress(KeyEvent.VK_ESCAPE);
		SkySiteUtils.waitForElement(driver, txtBoxUserName, 20);
		String uName = PropertyReader.getProperty("Username_Export");
		txtBoxUserName.clear();
		txtBoxUserName.sendKeys(uName);
		Log.message("Username: " + uName + " " + "has been entered in Username text box." );
		String pWord = PropertyReader.getProperty("Password_Export");
		txtBoxPassword.clear();
		txtBoxPassword.sendKeys(pWord);
		Log.message("Password: " + pWord + " " + "has been entered in Password text box." );
		SkySiteUtils.waitForElement(driver, btnLogin, 20);
		btnLogin.click();
		Log.message("LogIn button clicked.");
		return new ProjectDashboardPage(driver).get();
	}
	
	
	/** 
	 * Method written for doing login with invalid credential.
	 * @return
	 * @throws AWTException 
	 */
	public boolean loginWithInvalidCredential() throws AWTException
	{
		SkySiteUtils.waitTill(5000);
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ESCAPE);
		SkySiteUtils.waitForElement(driver, txtBoxUserName, 20);
		String uName = PropertyReader.getProperty("Invalidusername");
		txtBoxUserName.clear();
		txtBoxUserName.sendKeys(uName);
		Log.message("Username: " + uName + " " + "has been engtered in Username text box." );
		String pWord = PropertyReader.getProperty("Invalidpassword");
		txtBoxPassword.clear();
		txtBoxPassword.sendKeys(pWord);
		Log.message("Password: " + pWord + " " + "has been engtered in Password text box." );
		btnLogin.click();
		Log.message("LogIn button clicked.");
		SkySiteUtils.waitTill(5000);
		String actualTitle = "Sign in - SKYSITE";
		String expectedTitle = driver.getTitle();
		if(actualTitle.equalsIgnoreCase(expectedTitle))
			return true;
		else
			return false;
	}

	/** Method to enter Registration screen
	 *  By Ranadeep
	 */
	public ProjectsRegistrationPage enterRegistrationScreen() 
	{
		
		SkySiteUtils.waitForElement(driver, btnRegister, 30);
		btnRegister.click();
		Log.message("Clicked on Registration button");
		
		return new ProjectsRegistrationPage(driver).get();
				
	}

	
	
}
