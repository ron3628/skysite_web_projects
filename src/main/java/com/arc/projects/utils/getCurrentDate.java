package com.arc.projects.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class getCurrentDate {

	public static String formatDateToString(Date date, String format,
            String timeZone) {
        // null check
        if (date == null) return null;
        // create SimpleDateFormat object with input format
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        // default system timezone if passed null or empty
        if (timeZone == null || "".equalsIgnoreCase(timeZone.trim())) {
            timeZone = Calendar.getInstance().getTimeZone().getID();
        }
        // set timezone to SimpleDateFormat
        sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
        // return Date in required format with timezone as String
        return sdf.format(date);
    }
	
	
	public static String getPSTTime()
	{
		
		Date date = new Date();
		String todaysDate=formatDateToString(date, "MM/dd/yyyy", "PST");	
		return todaysDate;
		
	}
	
	public static String getISTTime()
	{
		
		Date date = new Date();
		String todaysDate=formatDateToString(date, "MM-dd-yyyy", "IST");	
		return todaysDate;
		
	}
	
	
	
}
