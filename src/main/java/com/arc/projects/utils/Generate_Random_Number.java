package com.arc.projects.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;

public class Generate_Random_Number {

	public static String generateRandomValue() {
		Random r = new Random(System.currentTimeMillis());
		return String.valueOf((10000 + r.nextInt(20000)));
	}

	public static int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

	public static String generateRandomValue1() {
		Random r = new Random(System.currentTimeMillis());
		return String.valueOf((100 + r.nextInt(200)));
	}

	public static String RandomEmail() throws java.text.ParseException {
		String Email = null;

		Email = "IndiaArc" + "." + getRandomNumberInRange(0000, 1210) + "@" + "Disposible.com";
		System.out.println(Email);
		return Email;
	}

	public static String RandamName() throws java.text.ParseException {
		String name = null;

		name = "IndiaArc" + "." + getRandomNumberInRange(00000, 99999);
		System.out.println(name);
		return name;
	}

	public static String RandamName_text() throws java.text.ParseException {
		String name = null;

		name = "Arc Automation" + "." + getRandomNumberInRange(00000, 99999);
		System.out.println(name);
		return name;
	}

	public static String RandamName_CallOut() throws java.text.ParseException {
		String name = null;

		name = "Arc CallOut Automation" + "." + getRandomNumberInRange(00000, 99999);
		System.out.println(name);
		return name;
	}

	public static String ProjectName() throws java.text.ParseException {
		String ProjectName = null;

		ProjectName = "ARC_Document_Solutions" + "/" + MyDate();
		return ProjectName;
	}

	public static String AlbumName() throws java.text.ParseException {
		String AlbumName = null;

		AlbumName = "Album_" + getRandomNumberInRange(100, 1210);
		return AlbumName;
	}

	public static String formatDate(String dateFormat) throws java.text.ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date = sdf.parse(dateFormat);
		String st = sdf.format(date);
		return st;
	}

	public static void main(String[] args) throws ParseException {
		MyDate();	}

	public static String MyDate()  {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyhhmmss");
		Date date = new Date();
		String st = sdf.format(date);
		String Randamvalue = getRandomNumberInRange(900, 999) +""+st;
		System.out.println(Randamvalue);
		return Randamvalue;
	}

	public static String getRandomMobileNo() {
		String mobile;
		mobile = getRandomNumberInRange(98111, 99999) + "" + getRandomNumberInRange(11111, 99999);
		return mobile;
	}

	public static String StampTitle() {
		String StampTitle;
		StampTitle = getRandomNumberInRange(900, 999) + "" + getRandomNumberInRange(111, 999) + ""
				+ getRandomNumberInRange(111, 888);
		return StampTitle;
	}
	
	//Added By: Naresh
	public static String randomStampNumber()
	{
		Random rand = new Random();
		
		int pick = rand.nextInt(900)+100;
		String ThreeDigitRandom = String.valueOf(pick);
		//System.out.println("Three Digit Random Value is: "+ThreeDigitRandom);
		return ThreeDigitRandom;		
	}

	public static String getRandomText(int length) {
		return RandomStringUtils.randomAlphabetic(length);
	}
	
	public static String getRandomText1(int length1) {
		return RandomStringUtils.randomAlphabetic(length1);
	}

	public static String getCurrentDate() {
		Date date = new Date();
		SimpleDateFormat dt1 = new SimpleDateFormat("ddMMyy");
		// System.out.println(dt1.format(date));
		return dt1.format(date);

	}
}
