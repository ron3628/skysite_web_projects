package com.arc.projects.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.arcautoframe.utils.StopWatch;

public class InvisibleWait {

	public static boolean waitForInvisibility(WebDriver driver, WebElement element, int maxWait)
	{
		boolean statusOfElementToBeReturned = false;
        long startTime = StopWatch.startTime();
        WebDriverWait wait = new WebDriverWait(driver, maxWait);
        try {
        	statusOfElementToBeReturned = wait.until(ExpectedConditions.invisibilityOf(element));
        } catch (Exception e) {
            try {
				throw new Exception("Element is still visible after " + StopWatch.elapsedTime(startTime) + " sec ==> " + element.toString());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
        }
        return statusOfElementToBeReturned;
    }
	
}
