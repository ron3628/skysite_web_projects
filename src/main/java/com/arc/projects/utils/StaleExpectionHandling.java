package com.arc.projects.utils;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

public class StaleExpectionHandling {
	
	/**If you are facing stale exceptions while clicking any element
	 * Method written by Trinanjwan Sarkar
	 * @param element
	 * @return
	 */
	
	public static boolean  StateExceptionClick(WebElement element)

	
	{
		boolean result = false;
        int attempts = 0;
        while(attempts < 2) {
            try {
                 element.click();
                result = true;
                break;
            } catch(StaleElementReferenceException e) {
            }
            attempts++;
        }
        
        return result;
	}


	/**If you are facing stale exceptions while passing any keyword through a text field
	 * Method written by Trinanjwan Sarkar
	 * @param element
	 * @param Keyvalue
	 * @return
	 */
	

	public static boolean  StateExceptionSendKeys(WebElement element, String Keyvalue)
	
	{
		boolean result = false;
        int attempts = 0;
        while(attempts < 2) {
            try {
                 element.sendKeys(Keyvalue);
                result = true;
                break;
            } catch(StaleElementReferenceException e) {
            }
            attempts++;
        }
        
        return result;
	}
	
	}
	