package com.arc.projects.testscripts;

import org.testng.annotations.Test;

import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectGalleryPage;
import com.arc.projects.pages.ProjectLatestDocumentsPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.ProjectsRegistrationPage;
import com.arc.projects.pages.PublishLogPage;
import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.PropertyReader;
import com.arcautoframe.utils.Log;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

public class Project_Revision_Testcases 
{
	public static WebDriver driver;
	
	ProjectsLoginPage projectsLoginPage;
	ProjectDashboardPage projectDashboardPage; 
	ProjectsRegistrationPage projectRegistrationPage;
	FolderPage folderPage;
	PublishLogPage publishLogPage;
	ProjectLatestDocumentsPage projectLatestDocumentsPage;
	
	
	@Parameters("browser")
	@BeforeMethod
    public WebDriver beforeTest(String browser) {
        
    	if(browser.equalsIgnoreCase("firefox")) 
        {
               File dest = new File("./drivers/win/geckodriver.exe");
               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
               driver = new FirefoxDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
        
        else if (browser.equalsIgnoreCase("chrome")) 
        { 
        File dest = new File("./drivers/win/chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());  
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        options.setExperimentalOption("prefs", prefs);
        driver = new ChromeDriver( options );
        driver.get(PropertyReader.getProperty("SkysiteProdURL"));
 
        } 
        
        else if (browser.equalsIgnoreCase("safari"))
        {
               System.setProperty("webdriver.safari.noinstall", "false"); //To stop uninstall each time
               driver = new SafariDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
   
    	return driver;
    	
	}
	

	
	
	/** TC_001 (" Verify revisions uploading the same file in a particular folder in a Project ")*/
	
	@Test(priority = 0, enabled = true, description = " Verify revisions uploading the same file in a particular folder in a Project ")
	public void Verify_RevisionsInSameFolder() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify revisions uploading the same file in a particular folder in a Project ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			folderPage=projectDashboardPage.enteringanyProject("C_Test");
			
			folderPage.enterFolder1();
			folderPage.deleteAllFiles();
			folderPage.backToRootFolder();
			
			folderPage.enterFolder2();
			folderPage.deleteAllFiles();
			folderPage.backToRootFolder();
			
			folderPage.enterFolder3();
			folderPage.deleteAllFiles();
			folderPage.backToRootFolder();
			
			folderPage.enterFolder1();			
			for(int i=0;i<3;i++)
			{
				File fis=new  File(PropertyReader.getProperty("FolderPathFile2"));            
	            String filepath=fis.getAbsolutePath().toString();
	            File fis1=new  File(PropertyReader.getProperty("tempfilepath"));            
	            String tempfilepath=fis1.getAbsolutePath().toString();            
	            publishLogPage=folderPage.fileUpload(filepath,tempfilepath); 
	            folderPage=publishLogPage.publishLogProcessCompleted();
	            
			}
			 
			folderPage.changeFolderViewGridToList();
			
			Log.assertThat(folderPage.revisionVerifyInSameFolder(), "  Revision validation for same file uploaded in a same folder is successfull ", " Revision validation for same file uploaded in a same folder is unsuccessfull ", driver);					
			
			folderPage.changeFolderViewListToGrid();
			folderPage.deleteAllFiles();
					
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	

	
	/** TC_002 (" Verify revisions uploading the same file in different folder locations in a Project ")*/
	
	@Test(priority = 1, enabled = true, description = " Verify revisions uploading the same file in different folder locations in a Project ")
	public void Verify_RevisionsInDifferentFolder() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify revisions uploading the same file in different folder locations in a Project ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			folderPage=projectDashboardPage.enteringanyProject("C_Test");
			
			folderPage.enterFolder1();
			folderPage.deleteAllFiles();
			folderPage.backToRootFolder();
			
			folderPage.enterFolder2();
			folderPage.deleteAllFiles();
			folderPage.backToRootFolder();
			
			folderPage.enterFolder3();
			folderPage.deleteAllFiles();
			folderPage.backToRootFolder();
			
			folderPage.enterFolder1();			
			File fis=new  File(PropertyReader.getProperty("FolderPathFile2"));            
            String filepath=fis.getAbsolutePath().toString();
            File fis1=new  File(PropertyReader.getProperty("tempfilepath"));            
            String tempfilepath=fis1.getAbsolutePath().toString();            
            publishLogPage=folderPage.fileUpload(filepath,tempfilepath); 
            folderPage=publishLogPage.publishLogProcessCompleted(); 
            folderPage.backToRootFolder();
            
            folderPage.enterFolder2();			
            File fis2=new  File(PropertyReader.getProperty("FolderPathFile2"));            
            String filepath1=fis2.getAbsolutePath().toString();
            File fis3=new  File(PropertyReader.getProperty("tempfilepath"));            
            String tempfilepath1=fis3.getAbsolutePath().toString();            
            publishLogPage=folderPage.fileUpload(filepath1,tempfilepath1); 
            folderPage=publishLogPage.publishLogProcessCompleted();
            folderPage.backToRootFolder();
            
            folderPage.enterFolder3();			
            File fis4=new  File(PropertyReader.getProperty("FolderPathFile2"));            
            String filepath2=fis4.getAbsolutePath().toString();
            File fis5=new  File(PropertyReader.getProperty("tempfilepath"));            
            String tempfilepath2=fis5.getAbsolutePath().toString();            
            publishLogPage=folderPage.fileUpload(filepath2,tempfilepath2); 
            folderPage=publishLogPage.publishLogProcessCompleted();            
          
			folderPage.changeFolderViewGridToList();
			
			Log.assertThat(folderPage.revisionVerifyInDifferentFolder(), "  Revision validation for same file uploaded in a different folder locations is successfull ", " Revision validation for same file uploaded in a different folder locations is unsuccessfull ", driver);					
			
			folderPage.changeFolderViewListToGrid();
			folderPage.deleteAllFiles();
			folderPage.backToRootFolder();
			
			folderPage.enterFolder1();	
			folderPage.deleteAllFiles();
			folderPage.backToRootFolder();
			
			folderPage.enterFolder2();	
			folderPage.deleteAllFiles();
						
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }

	
	
	/** TC_003 (" Verify the latest Revision of a file should be displayed in the Latest Documents ")*/
	
	@Test(priority = 2, enabled = true, description = " Verify the latest Revision of a file should be displayed in the Latest Documents ")
	public void Verify_RevisionsInLatestDocuments() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify the latest Revision of a file should be displayed in the Latest Documents ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			folderPage=projectDashboardPage.enteringanyProject("C_Test");
			
			folderPage.enterFolder1();
			folderPage.deleteAllFiles();
			folderPage.backToRootFolder();
			
			folderPage.enterFolder2();
			folderPage.deleteAllFiles();
			folderPage.backToRootFolder();
			
			folderPage.enterFolder3();
			folderPage.deleteAllFiles();
			folderPage.backToRootFolder();
			
			folderPage.enterFolder1();
			
			for(int i=0;i<3;i++)
			{
				File fis=new  File(PropertyReader.getProperty("FolderPathFile2"));            
	            String filepath=fis.getAbsolutePath().toString();
	            File fis1=new  File(PropertyReader.getProperty("tempfilepath"));            
	            String tempfilepath=fis1.getAbsolutePath().toString();            
	            publishLogPage=folderPage.fileUpload(filepath,tempfilepath); 
	            folderPage=publishLogPage.publishLogProcessCompleted();
	            
			}
			
			folderPage.changeFolderViewGridToList();
			ArrayList<String> arr = folderPage.fetchRevision();
			folderPage.backToRootFolder(); 
			
			projectLatestDocumentsPage=folderPage.enterLatestDocuments();
			projectLatestDocumentsPage.latestDocumentEntryValidation();
					
			Log.assertThat(projectLatestDocumentsPage.revisionVerifyInLatestDocuments(arr), "  Latest Revision of the file is displayed in Latest Documents ", " Latest Revision of the file is not displayed in Latest Documents ", driver);					
			
			projectLatestDocumentsPage.changeLDViewListToGrid();
			projectLatestDocumentsPage.backToRootFolder();
			folderPage.enterFolder1();
			folderPage.deleteAllFiles();
					
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }

	
	
	/** TC_004 (" Verify deleting the latest revision of a file should be reflected in the Latest Documents ")*/
	
	@Test(priority = 3, enabled = true, description = " Verify deleting the latest revision of a file should be reflected in the Latest Documents ")
	public void Verify_LatestRevisionsInLatestDocumentsAfterDeletion() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify deleting the latest revision of a file should be reflected in the Latest Documents ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			folderPage=projectDashboardPage.enteringanyProject("C_Test");
			
			folderPage.enterFolder1();
			folderPage.deleteAllFiles();
			folderPage.backToRootFolder();
			
			folderPage.enterFolder2();
			folderPage.deleteAllFiles();
			folderPage.backToRootFolder();
			
			folderPage.enterFolder3();
			folderPage.deleteAllFiles();
			folderPage.backToRootFolder();
			
			folderPage.enterFolder1();
			
			for(int i=0;i<3;i++)
			{
				
				File fis=new  File(PropertyReader.getProperty("FolderPathFile2"));            
	            String filepath=fis.getAbsolutePath().toString();
	            File fis1=new  File(PropertyReader.getProperty("tempfilepath"));            
	            String tempfilepath=fis1.getAbsolutePath().toString();            
	            publishLogPage=folderPage.fileUpload(filepath,tempfilepath); 
	            folderPage=publishLogPage.publishLogProcessCompleted();
	            
			}
			
			folderPage.changeFolderViewGridToList();
			ArrayList<String> arr = folderPage.fetchRevision();
			folderPage.backToRootFolder(); 
			
			projectLatestDocumentsPage=folderPage.enterLatestDocuments();
			projectLatestDocumentsPage.latestDocumentEntryValidation();
			projectLatestDocumentsPage.revisionVerifyInLatestDocuments(arr);
			
			Log.assertThat(projectLatestDocumentsPage.revisionVerifyInLatestDocuments(arr), "  Latest Revision of the file is displayed in Latest Documents ", " Latest Revision of the file is not displayed in Latest Documents ", driver);					
			
			projectLatestDocumentsPage.backToRootFolder();
			folderPage.enterFolder1();
			folderPage.deleteLatestRevision();
			ArrayList<String> arr1 = folderPage.fetchRevision();
			folderPage.backToRootFolder();
			projectLatestDocumentsPage=folderPage.enterLatestDocuments();
			projectLatestDocumentsPage.latestDocumentEntryValidation();
			projectLatestDocumentsPage.revisionVerifyInLatestDocuments(arr1);
			
			Log.assertThat(projectLatestDocumentsPage.revisionVerifyInLatestDocuments(arr1), "  Previous Revision of the file is displayed in Latest Documents after deleting the latest revision from the folder ", " Previous Revision of the file is not displayed in Latest Documents after deleting the latest revision from the folder ", driver);					
					
			projectLatestDocumentsPage.changeLDViewListToGrid();
			projectLatestDocumentsPage.backToRootFolder();
			folderPage.enterFolder1();
			folderPage.deleteAllFiles();
					
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_005 (" Verify download for the latest revision of a file ")*/
	
	@Test(priority = 4, enabled = true, description = " Verify download for the latest revision of a file ")
	public void Verify_LatestRevisionDownload() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify download for the latest revision of a file ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			folderPage=projectDashboardPage.enteringanyProject("C_Test");
			
			folderPage.enterFolder1();
			folderPage.deleteAllFiles();
			folderPage.backToRootFolder();
			
			folderPage.enterFolder2();
			folderPage.deleteAllFiles();
			folderPage.backToRootFolder();
			
			folderPage.enterFolder3();
			folderPage.deleteAllFiles();
			folderPage.backToRootFolder();
			
			folderPage.enterFolder1();
			
			for(int i=0;i<3;i++)
			{
				
				File fis=new  File(PropertyReader.getProperty("FolderPathFile2"));            
	            String filepath=fis.getAbsolutePath().toString();
	            File fis1=new  File(PropertyReader.getProperty("tempfilepath"));            
	            String tempfilepath=fis1.getAbsolutePath().toString();            
	            publishLogPage=folderPage.fileUpload(filepath,tempfilepath); 
	            folderPage=publishLogPage.publishLogProcessCompleted();
	            
			}
			
			folderPage.changeFolderViewGridToList();
			
			String user = System.getProperty("user.name");     		
		    String DownloadPath = "C:\\" + "Users\\" + user + "\\Downloads";
		    
		    String FolderName = PropertyReader.getProperty("RevisionTestFolder");
			
			Log.assertThat(folderPage.latestRevisionDownload(DownloadPath,FolderName), "  Latest Revision of the file is downloaded successfully ", " Latest Revision of the file is download failed ", driver);					
					
			folderPage.changeFolderViewListToGrid();
			folderPage.deleteAllFiles();
					
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
}
