package com.arc.projects.testscripts;

import org.testng.annotations.Test;

import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.ProjectsRegistrationPage;
import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;


public class Projects_Registration_Testcases 
{

	public static WebDriver driver;
	
	ProjectsLoginPage projectsLoginPage;
	ProjectDashboardPage projectDashboardPage; 
	ProjectsRegistrationPage projectRegistrationPage;
	
	
	@Parameters("browser")
	@BeforeMethod
    public WebDriver beforeTest(String browser) {
        
    	if(browser.equalsIgnoreCase("firefox")) 
        {
               File dest = new File("./drivers/win/geckodriver.exe");
               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
               driver = new FirefoxDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
        
        else if (browser.equalsIgnoreCase("chrome")) 
        { 
        File dest = new File("./drivers/win/chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());  
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        options.setExperimentalOption("prefs", prefs);
        driver = new ChromeDriver( options );
        driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        
        SkySiteUtils.waitTill(3000);
        String currentURL = driver.getCurrentUrl();
        Log.message("Current url is:- "+currentURL);
 
        } 
        
        else if (browser.equalsIgnoreCase("safari"))
        {
               System.setProperty("webdriver.safari.noinstall", "false"); //To stop uninstall each time
               driver = new SafariDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
   
    	return driver;
    	
	}
	
	
	/** TC_001 (" Verify User is able to Register to the application ")*/
	
	@Test(priority = 0, enabled = true, description = " Verify User is able to Register to the application ")
	public void Verify_SuccessfulRegistration() throws Throwable
    {
		
		try
    	{
			
			Log.testCaseInfo(" Verify User is able to Register to the application ");
			
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectRegistrationPage=projectsLoginPage.enterRegistrationScreen();
			String emailId = projectRegistrationPage.RandomEmailId();
			projectDashboardPage=projectRegistrationPage.RegistrationWithValidCredentials(emailId);
			
			Log.assertThat(projectDashboardPage.registrationValidation(),"Registration successful !!", "Registration unsuccessful!! Sorry", driver);
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
		
    } 
	
	
	/** TC_002 ("Verify the default sorting order in the Project dashboard should be with PROJECT NAME and in Ascending order")*/
	
	@Test(priority = 1, enabled = true, description = "Verify the default sorting order in the Project dashboard should be with PROJECT NAME and in Ascending order")
	public void Verify_DefaultProjectSortingOrder() throws Throwable
    {
		
		try
    	{
			
			Log.testCaseInfo("Verify the default sorting order in the Project dashboard should be with PROJECT NAME and in Ascending order");
			
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectRegistrationPage=projectsLoginPage.enterRegistrationScreen();
			String emailId = projectRegistrationPage.RandomEmailId();
			projectDashboardPage=projectRegistrationPage.RegistrationWithValidCredentials(emailId);
			Log.assertThat(projectDashboardPage.registrationValidation(),"Registration successful !!", "Registration unsuccessful!! Sorry", driver);
			
			Log.assertThat(projectDashboardPage.defaultProjectSorting(), "Default Project sorting order by PROJECT NAME and in Ascending order", "Default Project sorting order is not by PROJECT NAME and in Ascending order", driver);
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
		
    }
	
	
}
