package com.arc.projects.testscripts;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.ITestResult;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.Gallery_Photo_Page;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.ProjectsRegistrationPage;
import com.arc.projects.pages.Projects_SettingsPage;
import com.arc.projects.pages.PunchPage;
import com.arc.projects.pages.RFIPage;
import com.arc.projects.pages.SubmitalPage;
import com.arc.projects.pages.ViewerScreenPage;
import com.arc.projects.utils.AnalyzeLog;
import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)

public class Package_Module {
	
	String webSite;
    ITestResult result;
    
    static WebDriver driver;
    ProjectsLoginPage projectsLoginPage;
    ProjectDashboardPage projectDashboardPage;
    FolderPage folderPage;
    Gallery_Photo_Page gallery_Photo_Page;
    Projects_SettingsPage projectsettingspage; 
	ProjectsRegistrationPage projectRegistrationPage;
	Projects_SettingsPage projectssettings;
	ViewerScreenPage viewerscreenpage;
	PunchPage punchpage;
	RFIPage rfipage;
	SubmitalPage submitalpage;
    
    @Parameters("browser")
    @BeforeMethod(groups = "naresh_test")
 public WebDriver beforeTest(String browser) {
        
        if(browser.equalsIgnoreCase("firefox")) 
        {
               File dest = new File("./drivers/win/geckodriver.exe");
               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
               driver = new FirefoxDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
        
        else if (browser.equalsIgnoreCase("chrome")) 
        { 
        	File dest = new File("./drivers/win/chromedriver.exe");
            System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());  
            Map<String, Object> prefs = new HashMap<String, Object>();
            prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
            
            prefs.put("safebrowsing.enabled", "true");//this condition should be f@lse every time
 
            ChromeOptions options = new ChromeOptions();
             options.addArguments("--start-maximized");
            options.setExperimentalOption("prefs", prefs);
            driver = new ChromeDriver( options );
            driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        
        } 
        
        else if (browser.equalsIgnoreCase("safari"))
        {
               System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
               driver = new SafariDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
   return driver;
    }
    
    
    
    /** TC_001(Package Module): Verify after fresh registration the default package will be selected as Enterprise.
     * Scripted By : TRINANJWAN SARKAR
     * Modified By : RANADEEP GHOSH
     * @throws Exception
     */
     @Test(priority = 0, enabled = true, description = "TC_001(Package Module): Verify after fresh registration the default package will be selected as Enterprise.")
     public void verifydefaultpackage() throws Exception
     {
     	try
     	{
     		Log.testCaseInfo("TC_001(Package Module): Verify after fresh registration the default package will be selected as Enterprise.");
     		
     		projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectRegistrationPage=projectsLoginPage.enterRegistrationScreen();
			String emailId = projectRegistrationPage.RandomEmailId();
			projectDashboardPage=projectRegistrationPage.RegistrationWithValidCredentials(emailId);
			projectDashboardPage.registrationValidation();			
			projectssettings=projectDashboardPage.navigatingtoSettingsPage();
			
			Log.assertThat(projectssettings.verifyDefaultPackage(), "The enterprise package is verified successfully after registration", "The enterprise package is not verified successfully after registration", driver);
			
     	}
         catch(Exception e)
         {
         	AnalyzeLog.analyzeLog(driver);
         	e.getCause();
         	Log.exception(e, driver);
         	
         }
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     	}
     	
     }
     
    
     
     /** TC_002(Package Module): The Project Management section(Punch) will not be available after changing the package from Enterprise to Basic.
      * Scripted By : TRINANJWAN SARKAR
      * Modified By : RANADEEP GHOSH
      * @throws Exception
      */
      @Test(priority = 1, enabled = true, description = "TC_002(Package Module): The Project Management section(Punch) will not be available after changing the package from Enterprise to Basic.")
      public void verifychangeentertobasic_punch() throws Exception
      {  	  
    	  try
    	  { 		
    		  Log.testCaseInfo("TC_002(Package Module): The Project Management section(Punch) will not be available after changing the package from Enterprise to Basic.");
      		
			  String uName = PropertyReader.getProperty("userpkg1");
			  String pWord = PropertyReader.getProperty("passtri");
			  projectsLoginPage = new ProjectsLoginPage(driver).get();
			  projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
 			  projectssettings=projectDashboardPage.navigatingtoSettingsPage();
 			  projectDashboardPage=projectssettings.changingentertobasic();
 			  folderPage=projectDashboardPage.enteringanyProject("Sample Project - Technology Center Renovation");
 			  WebElement elementpunchicon=driver.findElement(By.xpath("//span[text()='Punch']"));
 			
 			  Log.assertThat(folderPage.validateProjectManagement(elementpunchicon),"The Project Management section(Punch) is not available after changing the package from Enterprise to Basic.","The Project Management section(Punch) is available after changing the package from Enterprise to Basic.", driver);
 			
 			  Log.assertThat(folderPage.Upgradepackagepopoverispresent(), "The Upgrade package popover is displayed", "The Upgrade package popover is not displayed",driver);
		  
    	  }
          catch(Exception e)
          {
        	  CommonMethod.analyzeLog(driver);
          	  e.getCause();
          	  Log.exception(e, driver);
          }
      	  finally
      	  {
      		  Log.endTestCase();
      		  driver.quit();
      	  }
    	  
     }
     
 
  /** TC_003(Package Module): The Project Management section(RFI) will not be available after changing the package from Enterprise to Basic.
   * Scripted By : TRINANJWAN SARKAR
   * Modified By : RANADEEP GHOSH
   * @throws Exception
   */
   @Test(priority = 2, enabled = true, description = "TC_003(Package Module): The Project Management section(RFI) will not be available after changing the package from Enterprise to Basic.")
   public void verifychangeentertobasic_RFI() throws Exception
   {	   
	   try
	   {
		   Log.testCaseInfo("TC_003(Package Module): The Project Management section(RFI) will not be available after changing the package from Enterprise to Basic.");
       		
		   String uName = PropertyReader.getProperty("userpkg1");
		   String pWord = PropertyReader.getProperty("passtri");
		   projectsLoginPage = new ProjectsLoginPage(driver).get();
 		   projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
  		   projectssettings=projectDashboardPage.navigatingtoSettingsPage();
  		   projectDashboardPage=projectssettings.changingentertobasic();
  		   folderPage=projectDashboardPage.enteringanyProject("Sample Project - Technology Center Renovation");
  		   WebElement elementpunchicon=driver.findElement(By.xpath("//span[text()='RFI']"));
  			
  		   Log.assertThat(folderPage.validateProjectManagement(elementpunchicon),"The Project Management section(RFI) is not available after changing the package from Enterprise to Basic.", "The Project Management section(RFI) is available after changing the package from Enterprise to Basic.", driver);
  			
  		   Log.assertThat(folderPage.Upgradepackagepopoverispresent(), "The Upgrade package popover is displayed", "The Upgrade package popover is not displayed",driver);

       	}
        catch(Exception e)
        {
        	CommonMethod.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
           	
        }
       	finally
       	{
       		Log.endTestCase();
       		driver.quit();
       	}
	   
   }
       
       
   /** TC_004(Package Module): The Project Management section(Submittal) will not be available after changing the package from Enterprise to Basic.
    * Scripted By : TRINANJWAN SARKAR
    * Modified By : RANADEEP GHOSH
    * @throws Exception
    */
    @Test(priority = 3, enabled = true, description = "TC_004(Package Module): The Project Management section(Submittal) will not be available after changing the package from Enterprise to Basic.")
    public void verifychangeentertobasic_Submittal() throws Exception
    {
    	try
    	{
        	Log.testCaseInfo("TC_004(Package Module): The Project Management section(Submittal) will not be available after changing the package from Enterprise to Basic.");
        		
  			String uName = PropertyReader.getProperty("userpkg1");
  			String pWord = PropertyReader.getProperty("passtri");
  			projectsLoginPage = new ProjectsLoginPage(driver).get();
  			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
   			projectssettings=projectDashboardPage.navigatingtoSettingsPage();
   			projectDashboardPage=projectssettings.changingentertobasic();
   			folderPage=projectDashboardPage.enteringanyProject("Sample Project - Technology Center Renovation");
   			WebElement elementpunchicon=driver.findElement(By.xpath("//span[text()='Submittal']"));
   			
   			Log.assertThat(folderPage.validateProjectManagement(elementpunchicon),"The Project Management section(Submittal) is not available after changing the package from Enterprise to Basic.", "The Project Management section(Submittal) is available after changing the package from Enterprise to Basic.", driver);
   			
   			Log.assertThat(folderPage.Upgradepackagepopoverispresent(), "The Upgrade package popover is displayed", "The Upgrade package popover is not displayed",driver);

    	}
        catch(Exception e)
        {
        	CommonMethod.analyzeLog(driver);
        	e.getCause();
        	Log.exception(e, driver);
        }
    	finally
    	{
    		Log.endTestCase();
    		driver.quit();
    	}
    	
    }
        
        
    /** TC_005(Package Module): The Gallery will not be available after changing the package from Enterprise to Basic.
     * Scripted By : TRINANJWAN SARKAR
     * Modified By : RANADEEP GHOSH
     * @throws Exception
     */
     @Test(priority = 4, enabled = true, description = "TC_005(Package Module): The Gallery will not be available after changing the package from Enterprise to Basic.")
     public void verifychangeentertobasic_Gallery() throws Exception
     {
     	try
     	{
     		
         	Log.testCaseInfo("TC_005(Package Module): The Gallery will not be available after changing the package from Enterprise to Basic.");
         		
         	String uName = PropertyReader.getProperty("userpkg2");
   			String pWord = PropertyReader.getProperty("passtri");
   			projectsLoginPage = new ProjectsLoginPage(driver).get();
   			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
    		projectssettings=projectDashboardPage.navigatingtoSettingsPage();
    		projectDashboardPage=projectssettings.changingentertobasic();
    		folderPage=projectDashboardPage.enteringanyProject("Sample Project - Technology Center Renovation");
    		
    		Log.assertThat(folderPage.validateGallery(),"The Gallery is not available after changing the package from Enterprise to Basic.", "The Gallery is available after changing the package from Enterprise to Basic.", driver);

     	}
        catch(Exception e)
        {
        	CommonMethod.analyzeLog(driver);
         	e.getCause();
         	Log.exception(e, driver);
         	
        }
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     		
     	}
     	
     }

         
     /** TC_006(Package Module): The Document Management section(Punch) will not be available after changing the package from Enterprise to Basic.
     * Scripted By : TRINANJWAN SARKAR
     * Modified By : RANADEEP GHOSH
     * @throws Exception
     */
     @Test(priority = 5, enabled = true, description = "TC_006(Package Module): The Document Management section(Punch) will not be available after changing the package from Enterprise to Basic.")
     public void verifychangeentertobasic_docPunch() throws Exception
     {
    	 try
         {
    		 Log.testCaseInfo("TC_006(Package Module): The Document Management section(Punch) will not be available after changing the package from Enterprise to Basic.");
          		
      		 String uName = PropertyReader.getProperty("userpkg2");
    		 String pWord = PropertyReader.getProperty("passtri");
    		 projectsLoginPage = new ProjectsLoginPage(driver).get();
    		 projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
     		 projectssettings=projectDashboardPage.navigatingtoSettingsPage();
     		 projectDashboardPage=projectssettings.changingentertobasic();		
     		 folderPage=projectDashboardPage.enteringanyProject("Sample Project - Technology Center Renovation");
     		 folderPage.enteringanyFolder("00 Pre-Construction");
     		 viewerscreenpage=folderPage.openinganyfileinViewer("A11");
     		 String xpathexp="//a[@data-original-title='Create punch']";
     		 
     		 Log.assertThat(viewerscreenpage.validatedocumentmanagementNotPresent(xpathexp), "Punch is not present in the viewer section", "Punch is present in the viewer section", driver);
     		
     		 folderPage=viewerscreenpage.closingofViewer();

      	 }
         catch(Exception e)
         {
        	 CommonMethod.analyzeLog(driver);
          	 e.getCause();
          	 Log.exception(e, driver);
          	 
         }
      	 finally
      	 {
      		 Log.endTestCase();
      		 driver.quit();
          		
      	 }
    	 
     }
          
          
     /** TC_007(Package Module): The Document Management section(RFI) will not be available after changing the package from Enterprise to Basic.
     * Scripted By : TRINANJWAN SARKAR
     * Modified By : RANADEEP GHOSH
     * @throws Exception
     */
     @Test(priority = 6, enabled = true, description = "TC_007(Package Module): The Document Management section(RFI) will not be available after changing the package from Enterprise to Basic.")
     public void verifychangeentertobasic_docRFI() throws Exception
     {
    	 try
    	 {
    		 Log.testCaseInfo("TC_007(Package Module): The Document Management section(RFI) will not be available after changing the package from Enterprise to Basic.");
           		
           	 String uName = PropertyReader.getProperty("userpkg2");
     		 String pWord = PropertyReader.getProperty("passtri");
     		 projectsLoginPage = new ProjectsLoginPage(driver).get();
     		 projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
      		 projectssettings=projectDashboardPage.navigatingtoSettingsPage();
      		 projectDashboardPage=projectssettings.changingentertobasic();
      		 folderPage=projectDashboardPage.enteringanyProject("Sample Project - Technology Center Renovation");
      		 folderPage.enteringanyFolder("00 Pre-Construction");
      		 viewerscreenpage=folderPage.openinganyfileinViewer("A11");
      		 String xpathexp="//a[@data-original-title='Create RFI']";
      		 Log.assertThat(viewerscreenpage.validatedocumentmanagementNotPresent(xpathexp), "RFI is not present in the viewer section", "RFI is present in the viewer section", driver);
      		
      		 folderPage=viewerscreenpage.closingofViewer();

       	 }
         catch(Exception e)
         {
        	 CommonMethod.analyzeLog(driver);
           	 e.getCause();
           	 Log.exception(e, driver);
           	 
         }
       	 finally
       	 {
       		 Log.endTestCase();
       		 driver.quit();
       		 
       	 }
    	 
    }
     
    /** TC_008(Package Module): The Photo annotation in the viewer will not be available after changing the package from Enterprise to Basic.
    * Scripted By : TRINANJWAN SARKAR
    * Modified By : RANADEEP GHOSH
    * @throws Exception
    */
    @Test(priority = 7, enabled = true, description = "TC_008(Package Module): The Photo annotation in the viewer will not be available after changing the package from Enterprise to Basic.")
    public void verifychangeentertobasic_Photoannotation() throws Exception
    {
    	try
    	{
    		
            Log.testCaseInfo("TC_008(Package Module): The Photo annotation in the viewer will not be available after changing the package from Enterprise to Basic.");
            		
            String uName = PropertyReader.getProperty("userpkg2");
      		String pWord = PropertyReader.getProperty("passtri");
      		projectsLoginPage = new ProjectsLoginPage(driver).get();
      		projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
       		projectssettings=projectDashboardPage.navigatingtoSettingsPage();
       		projectDashboardPage=projectssettings.changingentertobasic();
       		folderPage=projectDashboardPage.enteringanyProject("Sample Project - Technology Center Renovation");
       		folderPage.enteringanyFolder("00 Pre-Construction");
       		viewerscreenpage=folderPage.openinganyfileinViewer("A11");
       		String xpathexp="//a[@data-original-title='Add photo']";
       		
       		Log.assertThat(viewerscreenpage.validatedocumentmanagementNotPresent(xpathexp), "Photo annotation is not present in the viewer section", "Photo annotation is present in the viewer section", driver);

       		folderPage=viewerscreenpage.closingofViewer();
       		
    	}
        catch(Exception e)
        {
        	CommonMethod.analyzeLog(driver);
        	e.getCause();
        	Log.exception(e, driver);
        	
        }
    	finally
    	{
    		Log.endTestCase();
    		driver.quit();
            		
    	}
    	
    }
            
            
            
    /** TC_009(Package Module): The RFI TAB should not present under Projects settings after changing the package from Enterprise to Basic.
    * Scripted By : TRINANJWAN SARKAR
    * Modified By : RANADEEP GHOSH
    * @throws Exception
    */
    @Test(priority = 8, enabled = true, description = "TC_009(Package Module): The RFI TAB should not present under Projects settings after changing the package from Enterprise to Basic.")
    public void verifychangeentertobasic_RFISettings() throws Exception
    {
    	try
        {
    		Log.testCaseInfo("TC_009(Package Module): The RFI TAB should not present under Projects settings after changing the package from Enterprise to Basic.");
             		
         	String uName = PropertyReader.getProperty("userpkg2");
   			String pWord = PropertyReader.getProperty("passtri");
   			projectsLoginPage = new ProjectsLoginPage(driver).get();
   			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
    		projectssettings=projectDashboardPage.navigatingtoSettingsPage();
    		projectDashboardPage=projectssettings.changingentertobasic();
        	String xpathexp="//li[@id='liRFI']";
    		
        	Log.assertThat(projectDashboardPage.verifypromngSettings(xpathexp), "RFI settings is not available in project settings", "RFI settings is available in project settings", driver);
    		
     	}
        catch(Exception e)
        {
        	CommonMethod.analyzeLog(driver);        
         	e.getCause();
         	Log.exception(e, driver);
         	
         }
     	 finally
     	 {
     		 Log.endTestCase();
     		 driver.quit();
             		
     	 }
    	
    }

             
             
    /** TC_0010(Package Module): The Punch TAB should not present under Projects settings after changing the package from Enterprise to Basic.
    * Scripted By : TRINANJWAN SARKAR
    * Modified By : RANADEEP GHOSH
    * @throws Exception
    */
    @Test(priority = 9, enabled = true, description = "TC_0010(Package Module): The Punch TAB should not present under Projects settings after changing the package from Enterprise to Basic.")
    public void verifychangeentertobasic_PunchSettings() throws Exception
    {
    	try
        {
    		Log.testCaseInfo("TC_0010(Package Module): The Punch TAB should not present under Projects settings after changing the package from Enterprise to Basic.");
              		
          	String uName = PropertyReader.getProperty("userpkg2");
    		String pWord = PropertyReader.getProperty("passtri");
    		projectsLoginPage = new ProjectsLoginPage(driver).get();
    		projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
     		projectssettings=projectDashboardPage.navigatingtoSettingsPage();
     		projectDashboardPage=projectssettings.changingentertobasic();
 			String xpathexp="//li[@id='liPunchlist']";
 			
     		Log.assertThat(projectDashboardPage.verifypromngSettings(xpathexp), "Punch settings is not available in project settings","Punch settings is available in project settings", driver);;
         		
      	 }
         catch(Exception e)
         {
        	 CommonMethod.analyzeLog(driver);
          	 e.getCause();
          	 Log.exception(e, driver);
          	 
         }
      	 finally
      	 {
      	 	 Log.endTestCase();
      		 driver.quit();
      		 
      	 }
    	
    }
              
              
    /** TC_0011(Package Module): The Submittal TAB should not present under Projects settings after changing the package from Enterprise to Basic.
    * Scripted By : TRINANJWAN SARKAR
    * Modified By : RANADEEP GHOSH
    * @throws Exception
    */
    @Test(priority = 10, enabled = true, description = "TC_0011(Package Module): The Submittal TAB should not present under Projects settings after changing the package from Enterprise to Basic.")
    public void verifychangeentertobasic_SubmittalSettings() throws Exception
    {
    	try
        {
    		Log.testCaseInfo("TC_0011(Package Module): The Submittal TAB should not present under Projects settings after changing the package from Enterprise to Basic.");
               		
           	String uName = PropertyReader.getProperty("userpkg2");
     		String pWord = PropertyReader.getProperty("passtri");
     		projectsLoginPage = new ProjectsLoginPage(driver).get();
     		projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
      		projectssettings=projectDashboardPage.navigatingtoSettingsPage();
      		projectDashboardPage=projectssettings.changingentertobasic();
      		String xpathexp="//li[@id='liSubmittal']";
      		
      		Log.assertThat(projectDashboardPage.verifypromngSettings(xpathexp), "Submittal settings is not available in project settings", "Submittal settings is available in project settings", driver);;
        
       	}
        catch(Exception e)
        {
        	CommonMethod.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
                   	
        }
       	finally
       	{
       		Log.endTestCase();
       		driver.quit();
       		
       	}
    	
    }
               
               
    /** TC_0012(Package Module): The enable autohyperlink should not present under Projects settings after changing the package from Enterprise to Basic.
    * Scripted By : TRINANJWAN SARKAR
    * Modified By : RANADEEP GHOSH
    * @throws Exception
    */
    @Test(priority = 11, enabled = true, description = "TC_0012(Package Module): The enable autohyperlink should not present under Projects settings after changing the package from Enterprise to Basic.")
    public void verifychangeentertobasic_AutohypSettings() throws Exception
    {
    	try
    	{
            Log.testCaseInfo("TC_0012(Package Module): The enable autohyperlink should not present under Projects settings after changing the package from Enterprise to Basic.");
                		
            String uName = PropertyReader.getProperty("userpkg2");
      		String pWord = PropertyReader.getProperty("passtri");
      		projectsLoginPage = new ProjectsLoginPage(driver).get();
      		projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
       		projectssettings=projectDashboardPage.navigatingtoSettingsPage();
       		projectDashboardPage=projectssettings.changingentertobasic();
       		String xpathexp="//strong[text()='Enable auto hyperlink']";
       		
       		Log.assertThat(projectDashboardPage.verifypromngSettings(xpathexp), "The enable autohyperlink is not available in project settings", "The enable autohyperlink settings is available in project settings", driver);;
           	
    	}
        catch(Exception e)
        {
        	CommonMethod.analyzeLog(driver);
        	e.getCause();
        	Log.exception(e, driver);
        	
        }
    	finally
    	{
    		Log.endTestCase();
    		driver.quit();
    		
    	}
    	
    }
    
                
    /** TC_0013(Package Module): The default email should not present under Projects settings after changing the package from Enterprise to Basic.
    * Scripted By : TRINANJWAN SARKAR
    * Modified By : RANADEEP GHOSH
    * @throws Exception
    */
    @Test(priority = 12, enabled = true, description = "TC_0013(Package Module): The default email should not present under Projects settings after changing the package from Enterprise to Basic.")
    public void verifychangeentertobasic_defaultEmailSettings() throws Exception
    {
    	try
        {
            Log.testCaseInfo("TC_0013(Package Module): The default email should not present under Projects settings after changing the package from Enterprise to Basic.");
                 		
            String uName = PropertyReader.getProperty("userpkg2");
       		String pWord = PropertyReader.getProperty("passtri");
       		projectsLoginPage = new ProjectsLoginPage(driver).get();
       		projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
        	projectssettings=projectDashboardPage.navigatingtoSettingsPage();
        	projectDashboardPage=projectssettings.changingentertobasic();
        	String xpathexp="//input[@id='txtProjectEmail']";
        	
        	Log.assertThat(projectDashboardPage.verifypromngSettings(xpathexp), "The default email is not available in project settings", "The default email is available in project settings", driver);;
            	
        }
        catch(Exception e)
        {
        	CommonMethod.analyzeLog(driver);
         	e.getCause();
         	Log.exception(e, driver);
         	
        }
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     		
     	}
    	
     }
       
    
    
    /** TC_0014(Package Module): The Email Archive folder should not be present in folder level after changing the package from Enterprise to Basic.
     * Scripted By : TRINANJWAN SARKAR
     * Modified By : RANADEEP GHOSH
     * @throws Exception
     */
    @Test(priority = 13, enabled = true, description = "TC_0014(Package Module): The Email Archive folder should not be present in folder level after changing the package from Enterprise to Basic.")
    public void verifychangeentertobasic_emailArchieve() throws Exception
    {
        try
        {
            Log.testCaseInfo("TC_0014(Package Module): The Email Archive folder should not be present in folder level after changing the package from Enterprise to Basic.");
                     		
            String uName = PropertyReader.getProperty("userpkg2");
       		String pWord = PropertyReader.getProperty("passtri");
       		projectsLoginPage = new ProjectsLoginPage(driver).get();
       		projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
        	projectssettings=projectDashboardPage.navigatingtoSettingsPage();
        	projectDashboardPage=projectssettings.changingentertobasic();
                	
        	folderPage=projectDashboardPage.enteringanyProject("Sample Project - Technology Center Renovation");
                	
        	Log.assertThat(folderPage.validateemailarchive(),"Email Archive folder is not displayed", "Email Archive folder is displayed", driver);

        }
        catch(Exception e)
     	{
        	CommonMethod.analyzeLog(driver); 
         	e.getCause();
         	Log.exception(e, driver);
         	
         }
     	 finally
     	 {
     	 	Log.endTestCase();
     		driver.quit();
     		
 		 }
        
     }
    
    
    
     /** TC_0015(Package Module): The Autosave autohyperlink should not be present in folder level after changing the package from Enterprise to Basic.
     * Scripted By : TRINANJWAN SARKAR
     * Modified By : RANADEEP GHOSH
     * @throws Exception
     */
     @Test(priority = 14, enabled = true, description = "TC_0015(Package Module): The Autosave autohyperlink should not be present in folder level after changing the package from Enterprise to Basic.")
     public void verifychangeentertobasic_Autosaveautohyperlink() throws Exception
     {
    	 try
    	 {
    		 Log.testCaseInfo("TC_0015(Package Module): The Autosave autohyperlink should not be present in folder level after changing the package from Enterprise to Basic.");
                 		
             String uName = PropertyReader.getProperty("userpkg2");
       		 String pWord = PropertyReader.getProperty("passtri");
       		 projectsLoginPage = new ProjectsLoginPage(driver).get();
       		 projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
        	 projectssettings=projectDashboardPage.navigatingtoSettingsPage();
        	 projectDashboardPage=projectssettings.changingentertobasic();
        	 folderPage=projectDashboardPage.enteringanyProject("Sample Project - Technology Center Renovation");
            	
        	 Log.assertThat(folderPage.validateAutosaveautohyperlink(), "The Autosave autohyperlink is not available in folder level", "The Autosave autohyperlink is available in folder level", driver);
            
         }
         catch(Exception e)
         {
        	 CommonMethod.analyzeLog(driver);
         	 e.getCause();
         	 Log.exception(e, driver);
         	 
         }
     	 finally
     	 {
     		 Log.endTestCase();
     		 driver.quit();
     		 
     	 }
    	 
    }    
                
                
            
    /** TC_0016(Package Module): The Project Management section(Punch) will be available after changing the package from Basic to Enterprise.
    * Scripted By : TRINANJWAN SARKAR
    * Modified By : RANADEEP GHOSH
    * @throws Exception
    */
    @Test(priority = 15, enabled = true, description = "TC_0016(Package Module): The Project Management section(Punch) will be available after changing the package from Basic to Enterprise.")
    public void verifychangebasictoenter_punch() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_0016(Package Module): The Project Management section(Punch) will be available after changing the package from Basic to Enterprise.");
                 		
   			String uName = PropertyReader.getProperty("userpkg4");
   			String pWord = PropertyReader.getProperty("passtri");
   			projectsLoginPage = new ProjectsLoginPage(driver).get();
   			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
    		projectssettings=projectDashboardPage.navigatingtoSettingsPage();
    		projectDashboardPage=projectssettings.changingbasictoenter();    	
    		folderPage=projectDashboardPage.enteringanyProject("Sample Project - Technology Center Renovation");
    		punchpage=folderPage.validateProjectManagementPunch();
            		
    		Log.assertThat(punchpage.punchPagelanded(),"The Project Management section(Punch) is available after changing the package from Basic to Enterprise.","The Project Management section(Punch) is not available after changing the package from Basic to Enterprise.", driver);
 
     	}
        catch(Exception e)
    	{
        	CommonMethod.analyzeLog(driver);
         	e.getCause();
         	Log.exception(e, driver);
                     	
         }
     	 finally
 		 {
 		 	Log.endTestCase();
            driver.quit();
                 		
     	 }
    	
    }        
                 
                 
     /** TC_0017(Package Module): The Project Management section(RFI) will be available after changing the package from Basic to Enterprise.
     * Scripted By : TRINANJWAN SARKAR
     * Modified By : RANADEEP GHOSH
     * @throws Exception
     */
     @Test(priority = 16, enabled = true, description = "TC_0017(Package Module): The Project Management section(RFI) will be available after changing the package from Basic to Enterprise.")
     public void verifychangebasictoenter_rfi() throws Exception
     {
    	 try
    	 {
    		 Log.testCaseInfo("TC_0017(Package Module): The Project Management section(RFI) will be available after changing the package from Basic to Enterprise.");
                  		
    		 String uName = PropertyReader.getProperty("userpkg4");
    		 String pWord = PropertyReader.getProperty("passtri");
    		 projectsLoginPage = new ProjectsLoginPage(driver).get();
    		 projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
     		 projectssettings=projectDashboardPage.navigatingtoSettingsPage();
     		 projectDashboardPage=projectssettings.changingbasictoenter();
     		 folderPage=projectDashboardPage.enteringanyProject("Sample Project - Technology Center Renovation");
     		 rfipage=folderPage.validateProjectManagementRFI();
             		
 		     Log.assertThat(rfipage.rfiPagelanded(),"The Project Management section(RFI) is available after changing the package from Basic to Enterprise.","The Project Management section(RFI) is not available after changing the package from Basic to Enterprise.", driver);
      	 
    	 }
         catch(Exception e)
         {
        	 CommonMethod.analyzeLog(driver);
          	 e.getCause();
             Log.exception(e, driver);
                      	
         }
      	 finally
      	 {
      	 	Log.endTestCase();
            driver.quit();
                  		
      	 }
    	 
     }                         
                
                  
     /** TC_0018(Package Module): The Project Management section(Submittal) will be available after changing the package from Basic to Enterprise.
     * Scripted By : TRINANJWAN SARKAR
     * Modified By : RANADEEP GHOSH
     * @throws Exception
     */
     @Test(priority = 17, enabled = true, description = "TC_0018(Package Module): The Project Management section(Submittal) will be available after changing the package from Basic to Enterprise.")
     public void verifychangebasictoenter_submittal() throws Exception
     {
    	 try
    	 {
    		 Log.testCaseInfo("TC_0018(Package Module): The Project Management section(Submittal) will be available after changing the package from Basic to Enterprise.");
                   		
     		 String uName = PropertyReader.getProperty("userpkg4");
     		 String pWord = PropertyReader.getProperty("passtri");
     		 projectsLoginPage = new ProjectsLoginPage(driver).get();
     		 projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
      		 projectssettings=projectDashboardPage.navigatingtoSettingsPage();
      		 projectDashboardPage=projectssettings.changingbasictoenter();
      		 folderPage=projectDashboardPage.enteringanyProject("Sample Project - Technology Center Renovation");
      		 submitalpage=folderPage.validateProjectManagementSubmittal();
              		
      		 Log.assertThat(submitalpage.submittalPagelanded(),"The Project Management section(Submittal) is available after changing the package from Basic to Enterprise.","The Project Management section(Submittal) is not available after changing the package from Basic to Enterprise.", driver);
              	

          }
    	  catch(Exception e)
          {
    		  CommonMethod.analyzeLog(driver);  
           	  e.getCause();
           	  Log.exception(e, driver);
           	  
       	  }
    	  finally
    	  {
			  Log.endTestCase();
              driver.quit();
              
       	  }
    	 
    }        

     
     
    /** TC_0019(Package Module): The Gallery will be available after changing the package from Basic to Enterprise.
    * Scripted By : TRINANJWAN SARKAR
    * Modified By : RANADEEP GHOSH
    * @throws Exception
    */
    @Test(priority = 18, enabled = true, description = "TC_0019(Package Module): The Gallery will be available after changing the package from Basic to Enterprise.")
    public void verifychangebasictoenter_gallery() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_0019(Package Module): The Gallery will be available after changing the package from Basic to Enterprise.");
                    		
      		String uName = PropertyReader.getProperty("userpkg4");
      		String pWord = PropertyReader.getProperty("passtri");
      		projectsLoginPage = new ProjectsLoginPage(driver).get();
      		projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
       		projectssettings=projectDashboardPage.navigatingtoSettingsPage();
       		projectDashboardPage=projectssettings.changingbasictoenter();
       		folderPage=projectDashboardPage.enteringanyProject("Sample Project - Technology Center Renovation");
       		
       		Log.assertThat(folderPage.validateGalleryenterprise(), "Gallery folder is displayed", "Gallery folder is not displayed", driver);

    	}
        catch(Exception e)
        {
        	CommonMethod.analyzeLog(driver);   
        	e.getCause();
        	Log.exception(e, driver);
        	
        }
    	finally
    	{
    		Log.endTestCase();
    		driver.quit();
                    		
    	}
    	
    }        
                    
                    
    /** TC_0020(Package Module): The Document Management section(Punch) will be available after changing the package from Basic to Enterprise.
     * Scripted By : TRINANJWAN SARKAR
     * Modified By : RANADEEP GHOSH
     * @throws Exception
     */
     @Test(priority = 19, enabled = true, description = "TC_0020(Package Module): The Document Management section(Punch) will be available after changing the package from Basic to Enterprise.")
     public void verifychangebasictoenter_Punch() throws Exception
     {
    	 try
    	 {
    		 Log.testCaseInfo("TC_0020(Package Module): The Document Management section(Punch) will be available after changing the package from Basic to Enterprise.");
                     		
         	 String uName = PropertyReader.getProperty("userpkg4");
   			 String pWord = PropertyReader.getProperty("passtri");
   			 projectsLoginPage = new ProjectsLoginPage(driver).get();
   			 projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
    		 projectssettings=projectDashboardPage.navigatingtoSettingsPage();
    		 projectDashboardPage=projectssettings.changingbasictoenter();
    		 folderPage=projectDashboardPage.enteringanyProject("Sample Project - Technology Center Renovation");
    		 folderPage.enteringanyFolder("00 Pre-Construction");
    		 viewerscreenpage=folderPage.openinganyfileinViewer("A11");
		     String xpathexp="//a[@data-original-title='Create punch']";
		     WebElement element=driver.findElement(By.xpath(xpathexp));
                		
    		 Log.assertThat(viewerscreenpage.validatedocumentmanagementIsPresent(element), "Punch is present in the viewer section", "Punch is not present in the viewer section", driver);

		     folderPage=viewerscreenpage.closingofViewer();
                		
         }
    	 catch(Exception e)
    	 {
    		 CommonMethod.analyzeLog(driver);  
             e.getCause();
             Log.exception(e, driver);
                         	
         }
    	 finally
    	 {
    		 Log.endTestCase();
    		 driver.quit();
                     		
         }
    	 
     }
                     
                     
     /** TC_0021(Package Module): The Document Management section(RFI) will be available after changing the package from Basic to Enterprise.
     * Scripted By : TRINANJWAN SARKAR
     * Modified By : RANADEEP GHOSH
     * @throws Exception
     */
     @Test(priority = 20, enabled = true, description = "TC_0021(Package Module): The Document Management section(RFI) will be available after changing the package from Basic to Enterprise.")
     public void verifychangebasictoenter_RFI() throws Exception
     {
    	 try
    	 {
    		 Log.testCaseInfo("TC_0021(Package Module): The Document Management section(RFI) will be available after changing the package from Basic to Enterprise.");
                      		
          	 String uName = PropertyReader.getProperty("userpkg4");
             String pWord = PropertyReader.getProperty("passtri");
             projectsLoginPage = new ProjectsLoginPage(driver).get();
             projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
             projectssettings=projectDashboardPage.navigatingtoSettingsPage();
             projectDashboardPage=projectssettings.changingbasictoenter();
     		 folderPage=projectDashboardPage.enteringanyProject("Sample Project - Technology Center Renovation");
     		 folderPage.enteringanyFolder("00 Pre-Construction");
     		 viewerscreenpage=folderPage.openinganyfileinViewer("A11");
     		 String xpathexp="//a[@data-original-title='Create RFI']";
     		 WebElement element=driver.findElement(By.xpath(xpathexp));
                 		
     		 Log.assertThat(viewerscreenpage.validatedocumentmanagementIsPresent(element), "RFI is present in the viewer section", "RFI is not present in the viewer section", driver);

     		 folderPage=viewerscreenpage.closingofViewer();
                 		
      	 }
         catch(Exception e)
         {
        	 CommonMethod.analyzeLog(driver);
             e.getCause();
          	 Log.exception(e, driver);
                          	
         }
      	 finally
      	 {
             Log.endTestCase();
             driver.quit();
                      		
         }
    }
                      
                      
     /** TC_0022(Package Module): The Photo annotation in the viewer will be available after changing the package from Basic to Enterprise.
     * Scripted By : TRINANJWAN SARKAR
     * Modified By : RANADEEP GHOSH
     * @throws Exception
     */
     @Test(priority = 21, enabled = true, description = "TC_0022(Package Module): The Photo annotation in the viewer will be available after changing the package from Basic to Enterprise.")
     public void verifychangebasictoenter_Photo() throws Exception
     {
    	 try
    	 {
    		 Log.testCaseInfo("TC_0022(Package Module): The Photo annotation in the viewer will be available after changing the package from Basic to Enterprise.");
                       		
           	 String uName = PropertyReader.getProperty("userpkg4");
     		 String pWord = PropertyReader.getProperty("passtri");
     		 projectsLoginPage = new ProjectsLoginPage(driver).get();
     		 projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
      		 projectssettings=projectDashboardPage.navigatingtoSettingsPage();
      		 projectDashboardPage=projectssettings.changingbasictoenter();
  		     folderPage=projectDashboardPage.enteringanyProject("Sample Project - Technology Center Renovation");
      		 folderPage.enteringanyFolder("00 Pre-Construction");
  		     viewerscreenpage=folderPage.openinganyfileinViewer("A11");
      		 String xpathexp="//a[@data-original-title='Add photo']";
      		 WebElement element=driver.findElement(By.xpath(xpathexp));
                  		
             Log.assertThat(viewerscreenpage.validatedocumentmanagementIsPresent(element), "Photo annotation is present in the viewer section", "Photo annotation is not present in the viewer section", driver);

      		 folderPage=viewerscreenpage.closingofViewer();
                  		
       	 }
         catch(Exception e)
         {
        	 CommonMethod.analyzeLog(driver);
             e.getCause();
             Log.exception(e, driver);
                           	
         }
    	 finally
    	 {
    	 	 Log.endTestCase();
        	 driver.quit();
        	 
    	 }            	
                       
    }
                       
                       
    /** TC_0023(Package Module): The RFI TAB should be present under Projects settings after changing the package from Basic to Enterprise.
    * Scripted By : TRINANJWAN SARKAR
    * Modified By : RANADEEP GHOSH
    * @throws Exception
    */
    @Test(priority = 22, enabled = true, description = "TC_0023(Package Module): The RFI TAB should be present under Projects settings after changing the package from Basic to Enterprise.")
    public void verifychangebasictoenter_RFISettings() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_0023(Package Module): The RFI TAB should be present under Projects settings after changing the package from Basic to Enterprise.");
                        		
            String uName = PropertyReader.getProperty("userpkg4");
      		String pWord = PropertyReader.getProperty("passtri");
      		projectsLoginPage = new ProjectsLoginPage(driver).get();
      		projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
       		projectssettings=projectDashboardPage.navigatingtoSettingsPage();
       		projectDashboardPage=projectssettings.changingbasictoenter();           		
       		String xpathexp="//li[@id='liRFI']";
   
       		Log.assertThat(projectDashboardPage.verifypromngSettingspresent(xpathexp), "RFI settings is available in project settings", "RFI settings is not available in project settings", driver);;
       		
    	}
        catch(Exception e)
        {
        	CommonMethod.analyzeLog(driver);
        	e.getCause();
        	Log.exception(e, driver);
        	
        }
    	finally
    	{
    		Log.endTestCase();
    		driver.quit();
    		
    	}
                        
    }

                        
    /** TC_0024(Package Module): The Punch TAB should be present under Projects settings after changing the package from Basic to Enterprise.
     * Scripted By : TRINANJWAN SARKAR
     * Modified By : RANADEEP GHOSH
     * @throws Exception
     */
     @Test(priority = 23, enabled = true, description = "TC_0024(Package Module): The Punch TAB should be present under Projects settings after changing the package from Basic to Enterprise.")
     public void verifychangebasictoenter_PunchSettings() throws Exception
     {
         try
         {
             Log.testCaseInfo("TC_0024(Package Module): The Punch TAB should be present under Projects settings after changing the package from Basic to Enterprise.");
                         		
             String uName = PropertyReader.getProperty("userpkg4");
       		 String pWord = PropertyReader.getProperty("passtri");
       	     projectsLoginPage = new ProjectsLoginPage(driver).get();
       		 projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
        	 projectssettings=projectDashboardPage.navigatingtoSettingsPage();
        	 projectDashboardPage=projectssettings.changingbasictoenter();
        	 String xpathexp="//li[@id='liPunchlist']";
    
        	 Log.assertThat(projectDashboardPage.verifypromngSettingspresent(xpathexp), "Punch settings is available in project settings", "Punch settings is not available in project settings", driver);;
                    	 
         }
         catch(Exception e)
         {
             CommonMethod.analyzeLog(driver);
         	 e.getCause();
         	 Log.exception(e, driver);
                             	
         }
         finally
         {
             Log.endTestCase();
             driver.quit();
                         		
         }
         
     }
                         
                         
     /**TC_0025(Package Module): The Submittal TAB should be present under Projects settings after changing the package from Basic to Enterprise.
     * Scripted By : TRINANJWAN SARKAR
     * Modified By : RANADEEP GHOSH
     * @throws Exception
     */
     @Test(priority = 24, enabled = true, description = "TC_0025(Package Module): The Submittal TAB should be present under Projects settings after changing the package from Basic to Enterprise.")
     public void verifychangebasictoenter_SubmittalSettings() throws Exception
     {
    	 try
    	 {
    		 Log.testCaseInfo("TC_0025(Package Module): The Submittal TAB should be present under Projects settings after changing the package from Basic to Enterprise.");
                          		
             String uName = PropertyReader.getProperty("userpkg4");
        	 String pWord = PropertyReader.getProperty("passtri");
        	 projectsLoginPage = new ProjectsLoginPage(driver).get();
        	 projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
         	 projectssettings=projectDashboardPage.navigatingtoSettingsPage();
         	 projectDashboardPage=projectssettings.changingbasictoenter();           	
         	 String xpathexp="//li[@id='liSubmittal']";
         	 
         	 Log.assertThat(projectDashboardPage.verifypromngSettingspresent(xpathexp), "Submittal settings is available in project settings", "Submittal settings is not available in project settings", driver);;
                     	 
      	 }
         catch(Exception e)
         {
        	 CommonMethod.analyzeLog(driver);
          	 e.getCause();
          	 Log.exception(e, driver);
          	 
         }
    	 finally
         {
             Log.endTestCase();
             driver.quit();
             
         }
    	 
     }
     
     
                          
     /**TC_0026(Package Module): The enable autohyperlink should be present under Projects settings after changing the package from Basic to Enterprise.
     * Scripted By : TRINANJWAN SARKAR
     * Modified By : RANADEEP GHOSH
     * @throws Exception
     */
     @Test(priority = 25, enabled = true, description = "TC_0026(Package Module): The enable autohyperlink should be present under Projects settings after changing the package from Basic to Enterprise.")
     public void verifychangebasictoenter_AutoHypSettings() throws Exception
     {
    	 try
    	 {
    		 Log.testCaseInfo("TC_0026(Package Module): The enable autohyperlink should be present under Projects settings after changing the package from Basic to Enterprise.");
                           		
             String uName = PropertyReader.getProperty("userpkg4");
         	 String pWord = PropertyReader.getProperty("passtri");
     	     projectsLoginPage = new ProjectsLoginPage(driver).get();
     	     projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
             projectssettings=projectDashboardPage.navigatingtoSettingsPage();
             projectDashboardPage=projectssettings.changingbasictoenter();
             String xpathexp="//strong[text()='Enable auto hyperlink']";
      
             Log.assertThat(projectDashboardPage.verifypromngSettingspresent(xpathexp), "Enable autohyperlink settings is available in project settings", "Enable autohyperlink settings is not available in project settings", driver);;
                      	 
       	 }
         catch(Exception e)
         {
        	 CommonMethod.analyzeLog(driver);
        	 e.getCause();
             Log.exception(e, driver);
             
         }
       	 finally
         {
       		 Log.endTestCase();
       		 driver.quit();
       		 
       	 }
    	 
    }
     
     
     
                          
    /**TC_0027(Package Module): The default email should be present under Projects settings after changing the package from Basic to Enterprise.
    * Scripted By : TRINANJWAN SARKAR
    * Modified By : RANADEEP GHOSH
    * @throws Exception
    */
    @Test(priority = 26, enabled = true, description = "TC_0027(Package Module): The default email should be present under Projects settings after changing the package from Basic to Enterprise.")
    public void verifychangebasictoenter_defaultEmailSettings() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_0027(Package Module): The default email should be present under Projects settings after changing the package from Basic to Enterprise.");
                            		
            String uName = PropertyReader.getProperty("userpkg4");
      	    String pWord = PropertyReader.getProperty("passtri");
      	    projectsLoginPage = new ProjectsLoginPage(driver).get();
      	    projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
       	    projectssettings=projectDashboardPage.navigatingtoSettingsPage();
       	    projectDashboardPage=projectssettings.changingbasictoenter();
            String xpathexp="//input[@id='txtProjectEmail']";
       
            Log.assertThat(projectDashboardPage.verifypromngSettingspresent(xpathexp), "Default email settings is available in project settings", "Default email settings is not available in project settings", driver);;
                       
    	}
    	catch(Exception e)
        {
        	CommonMethod.analyzeLog(driver);
        	e.getCause();
        	Log.exception(e, driver);
        	
        }
    	finally
    	{
    		Log.endTestCase();
    		driver.quit();
    		
    	}       
                                
    }
                           
    /** TC_0028(Package Module): The Email Archive folder should be present in folder level after changing the package from Basic to Enterprise.
     * Scripted By : TRINANJWAN SARKAR
     * Modified By : RANADEEP GHOSH
     * @throws Exception
     */
    @Test(priority = 27, enabled = true, description = "TC_0028(Package Module): The Email Archive folder should be present in folder level after changing the package from Basic to Enterprise.")
    public void verifychangebasictoenter_emailArchieve() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_0028(Package Module): The Email Archive folder should be present in folder level after changing the package from Basic to Enterprise.");
     		
    		String uName = PropertyReader.getProperty("userpkg4");
    		String pWord = PropertyReader.getProperty("passtri");
    		projectsLoginPage = new ProjectsLoginPage(driver).get();
    		projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
    		projectssettings=projectDashboardPage.navigatingtoSettingsPage();
    		projectDashboardPage=projectssettings.changingbasictoenter();
    		folderPage=projectDashboardPage.enteringanyProject("Sample Project - Technology Center Renovation");
    		String xpathexp="//i[@class='icon icon-3x icon-project-email pw-icon-dk-orange']";
    		
    		Log.assertThat(folderPage.validateenterEmail_Auto(xpathexp),"Email Archive folder is displayed","Email Archive folder is not displayed", driver);
   
    	}
         catch(Exception e)
         {
         	CommonMethod.analyzeLog(driver);   
         	e.getCause();
         	Log.exception(e, driver);
         	
         }
     	 finally
     	 {
     		Log.endTestCase();
     		driver.quit();
     		
     	 }
    	
     }
    
    
    /** TC_0029(Package Module): The Autosave autohyperlink should be present in folder level after changing the package from  Basic to Enterprise.
     * Scripted By : TRINANJWAN SARKAR
     * Modified By : RANADEEP GHOSH
     * @throws Exception
     */
    @Test(priority = 28, enabled = true, description = "TC_0029(Package Module): The Autosave autohyperlink should be present in folder level after changing the package from  Basic to Enterprise.")
    public void verifychangebasictoenter_Autosaveautohyperlink() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_0029(Package Module): The Autosave autohyperlink should be present in folder level after changing the package from  Basic to Enterprise.");
     		
    		String uName = PropertyReader.getProperty("userpkg4");
    		String pWord = PropertyReader.getProperty("passtri");
    		projectsLoginPage = new ProjectsLoginPage(driver).get();
    		projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
    		projectssettings=projectDashboardPage.navigatingtoSettingsPage();
    		projectDashboardPage=projectssettings.changingbasictoenter();
    		folderPage=projectDashboardPage.enteringanyProject("Sample Project - Technology Center Renovation");
	
    		Log.assertThat(folderPage.validateAutosaveautohyperlinkispresent(), "The Autosave autohyperlink is available in folder level", "The Autosave autohyperlink is not available in folder level", driver);
	
    		
    	}
        catch(Exception e)
        {
         	CommonMethod.analyzeLog(driver);
         	e.getCause();
         	Log.exception(e, driver);
         	
        }
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     		
     	}
    	
     }  
    
    
    /** TC_0030(Package Module): Verify credit card added successfully for a freshly registered account.
     * Scripted By : TRINANJWAN SARKAR
     * Modified By : RANADEEP GHOSH
     * @throws Exception
     */
     @Test(priority = 29, enabled = true, description = "TC_0030(Package Module): Verify credit card added successfully for a freshly registered account.")
     public void creditCardAddtion() throws Exception
     {
     	try
     	{
     		Log.testCaseInfo("TC_0030(Package Module): Verify credit card added successfully for a freshly registered account.");
     		
     		projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectRegistrationPage=projectsLoginPage.enterRegistrationScreen();
			String emailId = projectRegistrationPage.RandomEmailId();
			projectDashboardPage=projectRegistrationPage.RegistrationWithValidCredentials(emailId);
			projectDashboardPage.registrationValidation();
			projectssettings=projectDashboardPage.navigatingtoSettingsPage();
			
			Log.assertThat(projectssettings.addtionofcreditcardinfo(), "Credit card is added successfully","Credit card is not added successfully");
			

     	}
         catch(Exception e)
         {
         	AnalyzeLog.analyzeLog(driver);
         	e.getCause();
         	Log.exception(e, driver);
         	
         }
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     		
     	}
     	
     }                      

}