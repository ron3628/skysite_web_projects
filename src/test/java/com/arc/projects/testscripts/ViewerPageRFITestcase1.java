package com.arc.projects.testscripts;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectAndFolder_Level_Search;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.Search_Testcases;
import com.arc.projects.pages.ViewerScreenPage;
import com.arc.projects.pages.Viewer_ScreenRFI;
import com.arc.projects.pages.Viewerlevel_RFI_Script;
import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;

public class ViewerPageRFITestcase1 {
	public static WebDriver driver;
	ProjectsLoginPage projectsLoginPage;
	ProjectDashboardPage projectDashboardPage;
	Viewerlevel_RFI_Script viewerlevel_RFI_Script;
	FolderPage folderPage;
	Search_Testcases search_Testcases;
	ProjectAndFolder_Level_Search projectAndFolder_Level_Search;
	ViewerScreenPage viewerScreenPage;

	@Parameters("browser")
	@BeforeMethod
	public WebDriver beforeTest(String browser) {

		if (browser.equalsIgnoreCase("firefox")) {
			File dest = new File("./drivers/win/geckodriver.exe");
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		} else if (browser.equalsIgnoreCase("chrome")) {
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory", "C:" + File.separator + "Users" + File.separator
					+ System.getProperty("user.name") + File.separator + "Downloads");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			options.setExperimentalOption("prefs", prefs);
			driver = new ChromeDriver(options);
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		} else if (browser.equalsIgnoreCase("safari")) {
			System.setProperty("webdriver.safari.noinstall", "");
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		return driver;
	}

	/**
	 * TC_001 : Verify RFI creation with To, CC user, Attachment - Upload File,
	 * Project File and validating Answer and Comment received from 'To' user by
	 * Owner.
	 */

	@Test(priority = 0, enabled = true , description = "Verify RFI creation with To, CC user, Attachment -  Upload File, Project File and validating Answer and Comment received from 'To' user by Owner.")
	public void RFIcreationAndValidate_AnsAndCommentByOwner() throws Throwable {

		try {
			Log.testCaseInfo(
					"TC_001 (RFI ToolsBaar validation):  Verify RFI creation with To, CC user, Attachment -  Upload File, Project File and validating Answer and Comment received from 'To' user by Owner.");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String uName1 = PropertyReader.getProperty("Username2");
			String pWord1 = PropertyReader.getProperty("Password2");

			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);

			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);

			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);

			// Owner Level
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFICreation_ownerLevel();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully","Logout not working Successfully", driver);

			// Employee level login
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName1, pWord1);
			// Select Project
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			folderPage.Select_Folder(Foldername);
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFI_EMPLOYEEATTACHAndMessage();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

			// Owner level Validation
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			folderPage.Select_Folder(Foldername);
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFIOwnerLevelValidation();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}

	/**
	 * TC_002 : Verify RFI creation with To, CC user, Attachment - Upload File,
	 * Project File and validating Close functionlity for 'To' user.
	 */

	@Test(priority = 1, enabled = true, description = "Verify RFI creation with To, CC user, Attachment -  Upload File, Project File and validating Close functionlity for 'To' user.")
	public void RFIValidation_withcloseStatus() throws Throwable {

		try {
			Log.testCaseInfo(
					"TC_002 (RFI ToolsBaar validation):  Verify RFI creation with To, CC user, Attachment -  Upload File, Project File and validating Close functionlity for 'To' user.");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");

			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			// Owner Level
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFIOwnerLevel_Close_Statusvalidation();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_003 : Verify RFI creation with To, CC user, Attachment - Upload File,
	 * Project File and validating Re-open functionlity for 'To' user..
	 */

	@Test(priority = 2, enabled = true, description = "Verify RFI creation with To, CC user, Attachment -  Upload File, Project File and validating Re-open functionlity for 'To' user.")
	public void REOPENRFIByOwner() throws Throwable {

		try {
			Log.testCaseInfo(
					"TC_003 (RFI ToolsBaar validation):  Verify RFI creation with To, CC user, Attachment -  Upload File, Project File and validating Re-open functionlity for 'To' user.");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String uName1 = PropertyReader.getProperty("Username2");
			String pWord1 = PropertyReader.getProperty("Password2");

			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			// Owner Level
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFIOwnerLevel_Reopen_Statusvalidation();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

			// Employee level- for responded
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName1, pWord1);
			projectDashboardPage.ValidateSelectProject(Project_Name);
			folderPage.Select_Folder(Foldername);
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFI_EMPLOYEEResponded();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_004 : Verify RFI creation with To, CC user, Attachment - Upload File,
	 * Project File and validating Reject functionlity for 'To' user..
	 */

	@Test(priority = 3, enabled = true, description = "Verify RFI creation with To, CC user, Attachment -  Upload File, Project File and validating Reject functionlity for 'To' user.")
	public void RejectRFIByOwner() throws Throwable {

		try {
			Log.testCaseInfo(
					"TC_004 (RFI ToolsBaar validation):  Verify RFI creation with To, CC user, Attachment -  Upload File, Project File and validating Reject functionlity for 'To' user.");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");

			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			// Owner Level
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFIOwnerLevel_Reject_Statusvalidation();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_05 : Verify RFI creation with To, CC user, Attachment - Upload File, Project File and validating VOID functionlity for 'To' user..
	 */

	@Test(priority = 4, enabled = true , description = "TC_05:--Verify RFI creation with To, CC user, Attachment -  Upload File, Project File and validating void functionlity for 'To' user.")
	public void VoidRFIByOwner() throws Throwable {

		try {
			Log.testCaseInfo("TC_05 (RFI ToolsBaar validation):  Verify RFI creation with To, CC user, Attachment -  Upload File, Project File and validating void functionlity for 'To' user.");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");

			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);

			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);

			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);

			// Owner Level
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFICreation_ownerLevel_void();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_006 : Verify RFI REASSIGNING with To, Attachment - Upload File,
	 * Project File and validating Answer and Comment received from 'To' user by
	 * Owner...
	 */

	@Test(priority = 5, enabled = true, description = "Verify RFI REASSIGNING with To, Attachment -  Upload File, Project File and validating Answer and Comment received from 'To' user by Owner..")
	public void ReassignRFIWithAttachment() throws Throwable {

		try {
			Log.testCaseInfo("TC_006(RFI ToolsBaar validation):  Verify RFI REASSIGNING with To, Attachment -  Upload File, Project File and validating Answer and Comment received from 'To' user by Owner.");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String uName1 = PropertyReader.getProperty("Username2");
			String pWord1 = PropertyReader.getProperty("Password2");
			String parentHandle = driver.getWindowHandle();
			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			// Owner Level
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFICreation_OwnerLevelAndReassign();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}

	/**
	 * TC_007 :Verify RFI is visible in RFI listing which is created through
	 * viewer level.
	 */

	@Test(priority = 6, enabled = true, description = "Verify RFI is visible in RFI listing which is created through viewer level.")
	public void CreateRFIIn_viewer_validateinDocument() throws Throwable {

		try {
			Log.testCaseInfo("TC_007 Verify RFI is visible in RFI listing which is created through viewer level.");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");

			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			// Owner Level
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFICreation_ownerLevel();
			viewerlevel_RFI_Script.ProjectManagement();
			viewerlevel_RFI_Script.validationInRFIList();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_008 : Verify RFI creartion with custom Attribute (5)
	 */

	@Test(priority = 7, enabled = true, description = "Verify RFI creartion with custom Attribute (5)")
	public void VerifyRFIwithCustomAttributeFive() throws Throwable {

		try {
			Log.testCaseInfo("TC_011 (RFI ToolsBaar validation): Verify RFI creartion with custom Attribute (5)");

			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String CustomAttribute5 = PropertyReader.getProperty("customAttribute5");
			String CustomAttribute1 = PropertyReader.getProperty("customAttribute1");
			String CustomAttribute2 = PropertyReader.getProperty("customAttribute2");
			String CustomAttribute3 = PropertyReader.getProperty("customAttribute3");
			String CustomAttribute4 = PropertyReader.getProperty("customAttribute4");
			String RandomNo = Generate_Random_Number.generateRandomValue();
			String Project_Name = "Proj_" + Generate_Random_Number.generateRandomValue();
			// ==============Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// =========Create Project
			// Randomly===================================
			folderPage = projectDashboardPage.createProjectwith_Five_CustomAtribute(Project_Name, CustomAttribute1,
					CustomAttribute2, CustomAttribute3, CustomAttribute4, CustomAttribute5, RandomNo);
			// ================File Upload without Index=====================
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.RFI_CreationWithMultipleAttribute();
			Log.assertThat(projectAndFolder_Level_Search.RFI_AttributeValidation(),
					"RFI Attribute Validation Completed Sucessfully",
					"RFI Attribute Validation  not Completed Sucessfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_009 : Verify sorting in RFI listing - Owner.
	 */

	@Test(priority = 8, enabled = true, description = "Verify sorting in RFI listing - Owner")
	public void RFIlistAscendingdecending_Owner() throws Throwable {

		try {
			Log.testCaseInfo("TC_009 (RFI ToolsBaar validation): Verify sorting in RFI listing - Owner");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ==============Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// =========Create Project
			// Randomly===================================
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// ================File Upload without Index=====================
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			Log.assertThat(projectAndFolder_Level_Search.RFI_AscendingorderValidation(),
					"Ascending order Validation working Successfully",
					"Ascending order Validation not working Successfully", driver);
			Log.assertThat(projectAndFolder_Level_Search.RFI_DecendingorderValidation(),
					"Decending order Validation working Successfully",
					"Decending order Validation not working Successfully", driver);
			projectAndFolder_Level_Search.Logout();

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}

	/**
	 * TC_010 : Verify sorting in RFI listing - Employee.
	 */

	@Test(priority = 9, enabled = true, description = "Verify sorting in RFI listing - Employee")
	public void RFIlistAscendingdecending_Employee() throws Throwable {

		try {
			Log.testCaseInfo("TC_010 (RFI ToolsBaar validation): Verify sorting in RFI listing - Employee");
			String uName1 = PropertyReader.getProperty("Username2");
			String pWord1 = PropertyReader.getProperty("Password2");
			// ========= Employee Level validation=============================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName1, pWord1);
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// ================File Upload without Index=====================
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			Log.assertThat(projectAndFolder_Level_Search.RFI_AscendingorderValidation(),
					"Ascending order Validation working Successfully",
					"Ascending order Validation not working Successfully", driver);

			Log.assertThat(projectAndFolder_Level_Search.RFI_DecendingorderValidation(),
					"Decending order Validation working Successfully",
					"Decending order Validation not working Successfully", driver);
			projectAndFolder_Level_Search.Logout();

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_011 : Verify sorting in RFI listing - Guest User.
	 */

	@Test(priority = 10, enabled = true, description = "Verify sorting in RFI listing - Guest User")
	public void RFIlistAscendingdecending_GuestUser() throws Throwable {

		try {
			Log.testCaseInfo("TC_011 (RFI ToolsBaar validation): Verify sorting in RFI listing - Guest User");
			String uName2 = PropertyReader.getProperty("GuestUserName");
			String pWord2 = PropertyReader.getProperty("GuestUserPassword");
			// =======Guest User Level Validation=======================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			String Project_Name = "Specific_Testdata_Static";
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName2, pWord2);
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// ================File Upload without Index=====================
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			Log.assertThat(projectAndFolder_Level_Search.RFI_AscendingorderValidation(),
					"Ascending order Validation working Successfully",
					"Ascending order Validation not working Successfully", driver);
			Log.assertThat(projectAndFolder_Level_Search.RFI_DecendingorderValidation(),
					"Decending order Validation working Successfully",
					"Decending order Validation not working Successfully", driver);
			projectAndFolder_Level_Search.Logout();

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_012 :Verify RFI creation with CC user, Attachment - Upload File,
	 * Project File and validating Comment received from 'CC' user by Owner. .
	 */

	@Test(priority = 11, enabled = true, description = "Verify RFI creation with  CC user, Attachment -  Upload File, Project File and validating Answer and Comment received from 'To' user by Owner.")
	public void RFI_CC_User_Validate_AndCommentByCCUser() throws Throwable {

		try {
			Log.testCaseInfo(
					"TC_012 (RFI ToolsBaar validation):  Verify RFI creation with  CC user, Attachment -  Upload File, Project File and validating Answer and Comment received from 'To' user by Owner.");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String uName1 = PropertyReader.getProperty("Email_CCOption");
			String pWord1 = PropertyReader.getProperty("Password2");

			// Login with valid UserID/PassWord

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);

			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			// Owner Level
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFICreation_ownerLevel();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

			// Employee level
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName1, pWord1);

			// Select Project
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder

			folderPage.Select_Folder(Foldername);
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFI_CC_Employee();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_013 : Navigate to Document level RFI through RFI listing and create
	 * new RFI
	 */

	@Test(priority = 12, enabled = true, description = "Navigate to Document level RFI through RFI listing and create new RFI")
	public void NavigatefromRFIListingtocreateRFI() throws Throwable {
		try {
			Log.testCaseInfo(
					"TC_013 (RFI Related Testcases):Navigate to Document level RFI through RFI listing and create new RFI");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ==============Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);

			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFICreation_ownerLevel();
			// viewerlevel_RFI_Script.ProjectManagement();
			// viewerlevel_RFI_Script.FromRFIlist_RFICreation();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_014 :Verify RFI assigne level - 2 [Owner assigning Employee and then
	 * Employee assiging another Employee]
	 */

	@Test(priority = 13, enabled = true, description = "Verify RFI assigne level - 2 [Owner assigning Employee and then Employee assiging another Employee]")
	public void OwnerAssignTwoEmployee() throws Throwable {

		try {
			Log.testCaseInfo(
					"TC_014:Verify RFI assigne level - 2 [Owner assigning Employee and then Employee assiging another Employee]");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");

			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			// Owner Level
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFICreation_ownerLevel();
			// viewerlevel_RFI_Script.Image1();
			// viewerlevel_RFI_Script.RFI_Reassign_seconduser();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}
	
	
	/**TC_015 :Navigate to Document level RFI through RFI listing and create new RFI and create a photo annotation.*/
	
	@Test(priority = 14, enabled = true, description = "Navigate to Document level RFI through RFI listing and create new RFI and create a photo annotation.")
	public void NavigatefromRFIListingtocreatePhotoAnnotation() throws Throwable {
		try {
			Log.testCaseInfo(
					"TC_015 (RFI Related Testcases):Navigate to Document level RFI through RFI listing and create new RFI and create a photo annotation.");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ==============Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String PhotoPath = PropertyReader.getProperty("Upload_PhotoFile");
			String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
			int FileCount = Integer.parseInt(CountOfFilesInFolder);
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			viewerScreenPage = new ViewerScreenPage(driver).get();
			// ============RFI Creation ON first File===========================			
			viewerScreenPage.Image1();		
			String subject = PropertyReader.getProperty("RFI_Subject");
			viewerScreenPage.RFITestDataCreationLevel(subject);			
			viewerScreenPage.Image1();	
			String parentHandle = driver.getWindowHandle();
			viewerScreenPage.PhotoAnnotation(parentHandle);
			viewerScreenPage.UploadPhoto(PhotoPath, FileCount);
			viewerScreenPage.untitledphotovalidation();
			driver.close();
			driver.switchTo().window(parentHandle);
			Log.assertThat(viewerScreenPage.Logout(), "Logout working Successfully", "Logout not working Successfully",
					driver);

		}
		catch (Exception e)
		{
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} 
		finally
		{
			Log.endTestCase();
			driver.quit();
		}

	}
	    
	
	//======================= Project Level RFI test cases================================
	
	
	 /**
	TC_016 Verify User is able to create RFI from project level (With attachments)- completed
	 */

	@Test(priority = 15, enabled = true, description = " Verify User is able to create RFI from project level (With attachments)")
	public void ProjectLevel_RFI_Creation() throws Throwable {

		try {
			Log.testCaseInfo("TC_016: Verify User is able to create RFI from project level (With attachments)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			//Project level RFI Creation			
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.RFI_Creation_projectlevel();
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();			
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}
	
	
	
	
	
     /**TC_017:Verify that assignee should be able to see the created RFI, Download Attchments, reply and change the status- completed*/
	
	@Test(priority = 16, enabled = true , description = "Verify that assignee should be able to see the created RFI, Download Attchments, reply and change the status")
	public void Assignee_shouldbe_abletoSee_createdRFI() throws Throwable {

		try {
			Log.testCaseInfo("TC_017:Verify that assignee should be able to see the created RFI, Download Attchments, reply and change the status");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String uName1 = PropertyReader.getProperty("Email_AssignTo_emp");
			String pWord1 = PropertyReader.getProperty("Password1");

			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			//Project level RFI Creation			
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.RFI_Creation_projectlevel();			
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();			
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);
			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName1, pWord1);
			// Select Project
	        String Project_Name1 = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name1);
			// select folder
			String Foldername1 = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername1);
			//Project level RFI Creation			
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();	
			// download RFI
			String usernamedir = System.getProperty("user.name");
			String Sys_Download_Path = "C:\\" + "Users\\" + usernamedir + "\\Downloads";
			Log.assertThat(folderPage.Assignee_levelDownloadFiledownload(Sys_Download_Path), "File downloded  Verified Successfully", "File downloded  Not Verified Successfully",
					driver);		
						
			Log.assertThat(projectAndFolder_Level_Search.RFI_replyAndChangeStatus(), "Reply  changed Status Verified sucessfully","Reply  changed Status not Verified sucessfully", driver);
			

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}
	
	
	
	
	
	
	
	/**TC_018:Verify User is able to save the RFI as draft and asign it to someone later.- completed*/
    
	@Test(priority = 17, enabled = true , description = "Verify User is able to save the RFI as draft and asign it to someone later.")
	public void Save_the_RFI_Asignto_Later() throws Throwable {

		try {
			Log.testCaseInfo("TC_018:Verify User is able to save the RFI as draft and asign it to someone later.");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String uName1 = PropertyReader.getProperty("Email_CCOption");
			String pWord1 = PropertyReader.getProperty("Password1");


			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			//Project level RFI Creation			
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.RFI_Creation_projectlevel_Draft();			
			projectAndFolder_Level_Search.RFI__projectlevel_send();
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();			
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);
			

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}
	
	
	
	
	/**
	TC_019 Verify the attached photos of RFI should be available in gallery. 
	 */

	@Test(priority = 18, enabled = true , description = "Verify the attached photos of RFI should be available in gallery: completed") 
	public void ProjectLevel_RFI_Creation_withphoto() throws Throwable {

		try {
			Log.testCaseInfo("TC_019: Verify the attached photos of RFI should be available in gallery");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");

			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			//Project level RFI Creation			
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.RFI_Creation_projectlevel();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.RFI_photoUpload();			
			String Project_Name1 = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name1);			
			folderPage = new FolderPage(driver).get();
			folderPage.Select_Gallery_Folder();
			search_Testcases = new Search_Testcases(driver).get();		
			Log.assertThat(search_Testcases.Searchscenario_validation(), "Matching photo Found Successfully","Matching photo  not found Successfully", driver);
			
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}
	
	
	
	
	/**
	TC_020 Verify User is able to download the RFI report. -completed
	 */

	@Test(priority = 19, enabled = true , description = "Verify User is able to download the RFI report")
	public void ProjectLevel_RFI_ReportDownload() throws Throwable {

		try {
			Log.testCaseInfo("TC_020:Verify User is able to download the RFI report");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");

			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			//Project level RFI Creation			
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.RFI_Creation_projectlevel();
			projectAndFolder_Level_Search.ProjectManagement();			
			String usernamedir = System.getProperty("user.name");
			String Sys_Download_Path = "C:\\" + "Users\\" + usernamedir + "\\Downloads";
			Log.assertThat(folderPage.DownloadFiledownload(Sys_Download_Path), "File downloded  Verified Successfully", "File downloded  Not Verified Successfully",
					driver);			
			
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();			
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}
	
	
	/**
	TC_021 Verify User is able to create RFI from project level (Without attachments). - completed
	 */

	@Test(priority = 20, enabled = true , description = "Verify User is able to create RFI from project level (Without attachments)")
	public void CreateRFI_without_Attachment() throws Throwable {

		try {
			Log.testCaseInfo("TC_021:Verify User is able to create RFI from project level (Without attachments)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");

			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			//Project level RFI Creation			
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.RFI_Creation_projectlevel_WITHOUT_attachment();			
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();			
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}
	
	
	
	
	/**
	TC_022 Verify the employee who is added in CC should not be able to edit or change the RFI details. 
	 */

	@Test(priority = 21, enabled = true  , description = "Verify the employee who is added in CC should not be able to edit or change the RFI details.")
	public void Cc_user_shouldnot_edit() throws Throwable {

		try {
			Log.testCaseInfo("TC_022:Verify the employee who is added in CC should not be able to edit or change the RFI details.");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String uName1 = PropertyReader.getProperty("Email_CCOption");
			String pWord1 = PropertyReader.getProperty("Password1");


			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			
			//Project level RFI Creation			
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.RFI_Creation_projectlevel_WITHOUT_attachment();			
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();			
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully","Logout not working Successfully", driver);
			
			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName1, pWord1);
			
			// Select Project
	        String Project_Name1 = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name1);
			
			// select folder
			String Foldername1 = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername1);
			
			//Project level RFI Creation			
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();	
			Log.assertThat(projectAndFolder_Level_Search.RFI_EditValidation_notPresent(), "No Edit Option foung In RFI","edit Option found in RFI ", driver);
			

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}
	
	
	 /**TC_023 Verify the employee who is added in To should not be able to edit or change the RFI details.*/

	@Test(priority = 22, enabled = true  , description = "Verify the employee who is added in CC should not be able to edit or change the RFI details.")
	public void To_user_shouldnot_edit() throws Throwable {

		try {
			Log.testCaseInfo("TC_023:Verify the employee who is added in CC should not be able to edit or change the RFI details.");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String uName1 = PropertyReader.getProperty("Email_AssignTo_emp");
			String pWord1 = PropertyReader.getProperty("Password1");
			
			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			
			//Project level RFI Creation			
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.RFI_Creation_projectlevel_WITHOUT_attachment();			
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();			
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);
			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName1, pWord1);
			// Select Project
	        String Project_Name1 = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name1);
			// select folder
			String Foldername1 = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername1);
			//Project level RFI Creation			
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();			
			Log.assertThat(projectAndFolder_Level_Search.RFI_EditValidation(), "Editable option Found","Editable option not Found", driver);
			

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}
	
	//============================ RFI self Assign cases ======================================
	
	 /**TC_024 Verify user can assign RFI self.*/

		@Test(priority = 23, enabled = true  , description = "Verify user can assign RFI self")
		public void RFI_Self_Assign() throws Throwable {

			try {
				Log.testCaseInfo("TC_024:Verify user can assign RFI self");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				
				// Login with valid UserID/PassWord
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				
				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				
				//Project level RFI Creation			
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();
				String ToEmail = PropertyReader.getProperty("Self_user");
				Log.assertThat(projectAndFolder_Level_Search.RFI_CreationwithEmail(ToEmail),"Self RFI created Sucessfully","Self RFI Not created Sucessfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		/**TC_025 Verify user can assign RFI self from draft status.*/

		@Test(priority = 24, enabled = true  , description = "Verify user can assign RFI self from draft status")
		public void RFI_Self_draft() throws Throwable {

			try {
				Log.testCaseInfo("TC_025 :Verify user can assign RFI self from draft status");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				
				// Login with valid UserID/PassWord
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				
				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				
				//Project level RFI Creation			
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();				
				Log.assertThat(projectAndFolder_Level_Search.RFI_Creation_projectlevel_Draft(),"Self RFI created Sucessfully","Self RFI Not created Sucessfully", driver);


			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		 
		/**TC_026 Verify the self assign RFI in all tab*/

		@Test(priority = 25, enabled = true  , description = "Verify the self assign RFI in all tab")
		public void RFI_Self_Assign_with_AllTab() throws Throwable {

			try {
				Log.testCaseInfo("TC_026:Verify the self assign RFI in all tab");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				
				// Login with valid UserID/PassWord
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				
				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				
				//Project level RFI Creation			
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();
				String ToEmail = PropertyReader.getProperty("Self_user");
				Log.assertThat(projectAndFolder_Level_Search.RFI_CreationwithEmail(ToEmail),"Self RFI created Sucessfully","Self RFI Not created Sucessfully", driver);
                 // Verify Self Assign RFI in all tab				
				Log.assertThat(projectAndFolder_Level_Search.RFI_AllTabValidation()," RFI All Tab Verification verified Sucessfully"," RFI All Tab Verification verified Sucessfully", driver);
				
				

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}

		
		
		
		
		
		
		/**TC_027 Verify user can assign RFI self with Cc user.*/

		@Test(priority = 26, enabled = true  , description = "Verify user can assign RFI self with Cc user")
		public void RFI_Self_Assign_with_CCUser() throws Throwable {

			try {
				Log.testCaseInfo("TC_027:Verify user can assign RFI self with Cc user");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				
				// Login with valid UserID/PassWord
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				
				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				
				//Project level RFI Creation			
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();
				String ToEmail = PropertyReader.getProperty("Self_user");
				String CCMail =PropertyReader.getProperty("SelectUserCCOption_Email");
				Log.assertThat(projectAndFolder_Level_Search.RFI_CreationwithEmail(ToEmail, CCMail),"Self RFI created Sucessfully","Self RFI Not created Sucessfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}

		
		/**TC_028 Verify the email template of self assign RFI.*/

		@Test(priority = 27, enabled = true  , description = "Verify the email template of self assign RFI")
		public void RFISelf_Assign_EmailTemplate() throws Throwable {

			try {
				Log.testCaseInfo("TC_028 :Verify the email template of self assign RFI");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				
				// Login with valid UserID/PassWord
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				
				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				
				

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		/**TC_028 :Verify the email template of self assign RFI*/

		@Test(priority = 28, enabled = true  , description = "Verify the email template of self assign RFI")
		public void RFISelfAssign_EmailTemplate() throws Throwable {

			try {
				Log.testCaseInfo("TC_028 :Verify the email template of self assign RFI");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				
				// Login with valid UserID/PassWord
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				
				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				
				

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		/**TC_029 :Verify the email template of self assign with Cc RFI*/

		@Test(priority = 28, enabled = true  , description = "Verify the email template of self assign with Cc RFI")
		public void RFISelfAssignEmail_TemplatewithCC() throws Throwable {

			try {
				Log.testCaseInfo("TC_029 :Verify the email template of self assign with Cc RFI");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				
				// Login with valid UserID/PassWord
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				
				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				
				

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		/**TC_030 :Verify the available status with option of self assign open RFI*/

		@Test(priority = 29, enabled = true  , description = "Verify the available status with option of self assign open RFI")
		public void Available_Status_option_OpenStatus() throws Throwable {

			try {
				Log.testCaseInfo("TC_030 :Verify the available status with option of self assign open RFI");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				
				// Login with valid UserID/PassWord
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				
				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				
				//Project level RFI Creation			
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();
				String ToEmail = PropertyReader.getProperty("Self_user");
				String CCMail =PropertyReader.getProperty("SelectUserCCOption_Email");
				projectAndFolder_Level_Search.RFI_CreationwithEmail(ToEmail, CCMail);
				// Owner Level
				viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
				//viewerlevel_RFI_Script.RFI_OpenStatus();
				Log.assertThat(viewerlevel_RFI_Script.RFI_OpenStatus(),"Open Status Verified Sucessfully","Open Status not Verified Sucessfully", driver);

				
		

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		/**TC_031 :Verify the available status with option of self assign responded RFI*/

		@Test(priority = 30, enabled = true  , description = "Verify the available status with option of self assign responded RFI")
		public void Available_Status_option_RespondedStatus() throws Throwable {

			try {
				Log.testCaseInfo("TC_031 :Verify the available status with option of self assign responded RFI");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				
				
				// Login with valid UserID/PassWord
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				
				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				
				//Project level RFI Creation			
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();
				String ToEmail = PropertyReader.getProperty("Self_user");
				String CCMail =PropertyReader.getProperty("SelectUserCCOption_Email");
				projectAndFolder_Level_Search.RFI_CreationwithEmail(ToEmail, CCMail);
				// Owner Level
				viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();				
				Log.assertThat(viewerlevel_RFI_Script.RFI_OpenStatus(),"Open Status Verified Sucessfully","Open Status not Verified Sucessfully", driver);
				Log.assertThat(viewerlevel_RFI_Script.RFI_SelfResponded(),"RESPONDED Status Verified Sucessfully","RESPONDED Status not Verified Sucessfully", driver);
		

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		
		/**TC_032 :Verify the available status with option of self assign rejected RFI*/

		@Test(priority = 31, enabled = true  , description = "Verify the available status with option of self assign rejected RFI")
		public void Available_Status_option_RejectStatus() throws Throwable {

			try {
				Log.testCaseInfo("TC_032 :Verify the available status with option of self assign rejected RFI");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				
				
				// Login with valid UserID/PassWord
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				
				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				
				//Project level RFI Creation			
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();
				String ToEmail = PropertyReader.getProperty("Self_user");
				String CCMail =PropertyReader.getProperty("SelectUserCCOption_Email");
				projectAndFolder_Level_Search.RFI_CreationwithEmail(ToEmail, CCMail);
				// Owner Level
				viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();				
				Log.assertThat(viewerlevel_RFI_Script.RFI_OpenStatus(),"Open Status Verified Sucessfully","Open Status not Verified Sucessfully", driver);
				Log.assertThat(viewerlevel_RFI_Script.RFI_SelfResponded(),"RESPONDED Status Verified Sucessfully","RESPONDED Status not Verified Sucessfully", driver);
				Log.assertThat(viewerlevel_RFI_Script.RFI_SelfResponded(),"RESPONDED Status Verified Sucessfully","RESPONDED Status not Verified Sucessfully", driver);
				


			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}

		
}