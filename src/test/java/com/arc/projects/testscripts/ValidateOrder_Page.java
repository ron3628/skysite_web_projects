package com.arc.projects.testscripts;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.projects.utils.*;
import com.arcautoframe.utils.Log;



public class ValidateOrder_Page extends LoadableComponent<ValidateOrder_Page> {
	
	WebDriver driver;
	private boolean isPageLoaded;
	
	/**
	 * Identifying web elements using FindBy annotatation 
	 */
	
	@FindBy(xpath="//*[@id='toolbar_navitem3']")
	WebElement OrderMonitor;
	
	@FindBy(xpath="//*[@id='mnuFilter']")
	WebElement SearchButton;	
	
	@FindBy(xpath="//*[@id='ddlField_1']")
	WebElement Field;
	
	@FindBy(xpath="(//option [@value='OD.OrderID^int'])[1]")
	WebElement Field_firstvalue;
	
	@FindBy(xpath=".//*[@id='ddlCondition_1']")
	WebElement Condition;	
	
	@FindBy(xpath="(//option [@value='Equals'])[1]")
	WebElement Condition_firstvalue;	
	
	@FindBy(xpath="//*[@id='txtValue_1']")
	WebElement value1;
	
	@FindBy(xpath="//*[@id='btnGo']")
	WebElement Go_button;	
	
	@FindBy(xpath="(//div[@id='divDownload'] )[1]")
	WebElement Download_link;
	
	
	
	
	
	

	
	
	
	
	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, OrderMonitor, 20);
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}
	
	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * @param driver
	 * @return 
	 */
	public ValidateOrder_Page(WebDriver driver) 
	{
		   this.driver = driver;
		   PageFactory.initElements(this.driver, this);
	}
	
	
	public ValidateOrder_Page loginWithValidCredential(int orderID) throws AWTException
	{
		SkySiteUtils.waitTill(2000);		
		SkySiteUtils.waitForElement(driver, OrderMonitor, 40);		
		OrderMonitor.click();		
		Log.message("Order Monitor Link Clicked Sucessfully" );
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, SearchButton, 40);	
		SearchButton.click();
		Log.message("Search Button Clicked Sucessfully" );
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Field, 40);
		Field.click();
		Log.message("Search Field Clicked Sucessfully" );
		SkySiteUtils.waitTill(5000);
		Field_firstvalue.click();
		Log.message("Search Field first Value Clicked Sucessfully" );
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, value1, 40);		
		value1.sendKeys(String.valueOf(orderID));
		Log.message("Order ID Entered In First Value field" );
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Go_button, 40);
		Go_button.click();
		Log.message("Go Button Clicked Sucessfully" );
		SkySiteUtils.waitTill(5000);
		return new ValidateOrder_Page(driver).get();
	}
	
	
	
	public void  ValidateFileCount(String FileCount) throws InterruptedException, AWTException, IOException
	{
		
		SkySiteUtils.waitTill(5000);
		int Avl_Projects_Count=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@class, 'project-icon')]"));
		for (WebElement Element : allElements)
		{ 
			Avl_Projects_Count =Avl_Projects_Count+1; 	
		}
		Log.message("Available private projects count is: "+Avl_Projects_Count);
		SkySiteUtils.waitTill(5000);
		
		
	
	
	}
}
