package com.arc.projects.testscripts;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.ITestResult;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.PDF_Reports_Page;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.utils.AnalyzeLog;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)
public class PDF_Report_Suit {
	
	String webSite;
    ITestResult result;
    
    static WebDriver driver;
    ProjectsLoginPage projectsLoginPage;
    ProjectDashboardPage projectDashboardPage;
    FolderPage folderPage;
    PDF_Reports_Page pdf_Reports_Page;
    
    @Parameters("browser")
    @BeforeMethod(groups = "naresh_test")//Newly Added due to group execution Naresh
 public WebDriver beforeTest(String browser) {
        
        if(browser.equalsIgnoreCase("firefox")) 
        {
               File dest = new File("./drivers/win/geckodriver.exe");
               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
               driver = new FirefoxDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
        
        else if (browser.equalsIgnoreCase("chrome")) 
        { 
        	File dest = new File("./drivers/win/chromedriver.exe");
            System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--start-maximized");
            driver = new ChromeDriver( options );
            driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        } 
        
        else if (browser.equalsIgnoreCase("safari"))
        {
        	System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
            driver = new SafariDriver();
            driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
   return driver;
 }
 
/** TC_001 (PDF Report Module): Verify Create Template (No Approval required) and validate.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 */
@Test(priority = 0, enabled = true, description = "TC_001 (PDF Report Module): Verify Create Template (No Approval required) and validate.")
public void verify_Create_Template_AndValidate() throws Exception
{
	try
    {
     	Log.testCaseInfo("TC_001 (PDF Report Module): Verify Create Template (No Approval required) and validate.");
     	//Getting Static data from properties file
   		String uName = PropertyReader.getProperty("NewData_Username");
   		String pWord = PropertyReader.getProperty("NewData_Password");
   		String Project_Name = PropertyReader.getProperty("Report_ProjectName");
   		String Randome_TemplateName = "Template_"+Generate_Random_Number.generateRandomValue();
   		String Input_ApprovalType = PropertyReader.getProperty("Input_ApprovalType1");
   		String Approver_MailId = PropertyReader.getProperty("Employee_Email");
   		String Notify_MailID = PropertyReader.getProperty("Employee2_Email");
   		File Path_FMProperties = new File(PropertyReader.getProperty("PDF_Template1_FilePath"));
   		String PDF_Template_FoldPath = Path_FMProperties.getAbsolutePath().toString();
   		
   		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     	folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
  		
     	pdf_Reports_Page = new PDF_Reports_Page(driver).get();
     	folderPage=pdf_Reports_Page.SelectPDFReportsTab();//Select Reports Tab
     
     	Log.assertThat(pdf_Reports_Page.Add_Template_WithPDFAttachment_AndValidate(PDF_Template_FoldPath, Randome_TemplateName, Input_ApprovalType, Approver_MailId, Notify_MailID), 
     			"Add a new Template is working.", "Add a new Template is NOT working.", driver);
     		     
    }
	catch(Exception e)
	{
		AnalyzeLog.analyzeLog(driver);
		e.getCause();
		Log.exception(e, driver);
	}
	finally
	{
     	Log.endTestCase();
     	driver.quit();
	}
}

/** TC_002 (PDF Report Module): Verify Edit Template and validate.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 */
@Test(priority = 1, enabled = true, description = "TC_002 (PDF Report Module): Verify Edit Template and validate.")
public void verify_Edit_Template_AndValidate() throws Exception
{
	try
    {
     	Log.testCaseInfo("TC_002 (PDF Report Module): Verify Edit Template and validate.");
     	//Getting Static data from properties file
   		String uName = PropertyReader.getProperty("NewData_Username");
   		String pWord = PropertyReader.getProperty("NewData_Password");
   		String Project_Name = PropertyReader.getProperty("Report_ProjectName");
   		String Randome_TemplateName = "Template_"+Generate_Random_Number.generateRandomValue();
   		String Edit_TempName = "Edit_"+Generate_Random_Number.generateRandomValue();
   		String Input_ApprovalType = PropertyReader.getProperty("Input_ApprovalType1");
   		String Approver_MailId = PropertyReader.getProperty("Employee_Email");
   		String Notify_MailID = PropertyReader.getProperty("Employee2_Email");
   		File Path_FMProperties = new File(PropertyReader.getProperty("PDF_Template1_FilePath"));
   		String PDF_Template_FoldPath = Path_FMProperties.getAbsolutePath().toString();
   		File Path_ReplaceFile = new File(PropertyReader.getProperty("Replace_FilePath"));
   		String PDF_Replace_FoldPath = Path_ReplaceFile.getAbsolutePath().toString();
   		
   		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     	folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
  		
     	pdf_Reports_Page = new PDF_Reports_Page(driver).get();
     	folderPage=pdf_Reports_Page.SelectPDFReportsTab();//Select Reports Tab
     
     	Log.assertThat(pdf_Reports_Page.Add_Template_WithPDFAttachment_AndValidate(PDF_Template_FoldPath, Randome_TemplateName, Input_ApprovalType, Approver_MailId, Notify_MailID), 
     			"Add a new Template is working.", "Add a new Template is NOT working.", driver);
     	
     	Log.assertThat(pdf_Reports_Page.Edit_Template_AndValidate(Randome_TemplateName, Input_ApprovalType, PDF_Replace_FoldPath, Edit_TempName), 
     			"Edit Template and validate is working.", "Edit Template and validate is NOT working.", driver);
     		     
     }
     catch(Exception e)
     {
    	 AnalyzeLog.analyzeLog(driver);
         e.getCause();
         Log.exception(e, driver);
     }
     finally
     {
     	Log.endTestCase();
     	driver.quit();
     }
}

/** TC_003 (PDF Report Module): Verify Download Template and validate.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 */
@Test(priority = 2, enabled = true, description = "TC_003 (PDF Report Module): Verify Download Template and validate.")
public void verify_Download_Template_AndValidate() throws Exception
{
	try
    {
     	Log.testCaseInfo("TC_003 (PDF Report Module): Verify Download Template and validate.");
     	//Getting Static data from properties file
   		String uName = PropertyReader.getProperty("NewData_Username");
   		String pWord = PropertyReader.getProperty("NewData_Password");
   		String Project_Name = PropertyReader.getProperty("Report_ProjectName");
   		String Randome_TemplateName = "Template_"+Generate_Random_Number.generateRandomValue();
   		String Input_ApprovalType = PropertyReader.getProperty("Input_ApprovalType1");
   		String Approver_MailId = PropertyReader.getProperty("Employee_Email");
   		String Notify_MailID = PropertyReader.getProperty("Employee2_Email");
   		File Path_FMProperties = new File(PropertyReader.getProperty("PDF_Template1_FilePath"));
   		String PDF_Template_FoldPath = Path_FMProperties.getAbsolutePath().toString();
   		
   		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     	folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
  		
     	pdf_Reports_Page = new PDF_Reports_Page(driver).get();
     	folderPage=pdf_Reports_Page.SelectPDFReportsTab();//Select Reports Tab
     
     	Log.assertThat(pdf_Reports_Page.Add_Template_WithPDFAttachment_AndValidate(PDF_Template_FoldPath, Randome_TemplateName, Input_ApprovalType, Approver_MailId, Notify_MailID), 
     			"Add a new Template is working.", "Add a new Template is NOT working.", driver);
     	
     	String usernamedir=System.getProperty("user.name");
 		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
     	Log.assertThat(pdf_Reports_Page.Download_Template_AndValidate(Randome_TemplateName, Input_ApprovalType, Sys_Download_Path), 
     			"Download Template is working.", "Download Template is NOT working.", driver);
     		     
     }
     catch(Exception e)
     {
    	 AnalyzeLog.analyzeLog(driver);
         e.getCause();
         Log.exception(e, driver);
     }
     finally
     {
     	Log.endTestCase();
     	driver.quit();
     }
}

/** TC_004 (PDF Report Module): Verify Delete Template and validate.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 */
@Test(priority = 3, enabled = true, description = "TC_004 (PDF Report Module): Verify Delete Template and validate.")
public void verify_Delete_Template_AndValidate() throws Exception
{
	try
    {
     	Log.testCaseInfo("TC_004 (PDF Report Module): Verify Delete Template and validate.");
     	//Getting Static data from properties file
   		String uName = PropertyReader.getProperty("NewData_Username");
   		String pWord = PropertyReader.getProperty("NewData_Password");
   		String Project_Name = PropertyReader.getProperty("Report_ProjectName");
   		String Randome_TemplateName = "Template_"+Generate_Random_Number.generateRandomValue();
   		String Input_ApprovalType = PropertyReader.getProperty("Input_ApprovalType1");
   		String Approver_MailId = PropertyReader.getProperty("Employee_Email");
   		String Notify_MailID = PropertyReader.getProperty("Employee2_Email");
   		File Path_FMProperties = new File(PropertyReader.getProperty("PDF_Template1_FilePath"));
   		String PDF_Template_FoldPath = Path_FMProperties.getAbsolutePath().toString();
   		
   		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     	folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
  		
     	pdf_Reports_Page = new PDF_Reports_Page(driver).get();
     	folderPage=pdf_Reports_Page.SelectPDFReportsTab();//Select Reports Tab
     
     	Log.assertThat(pdf_Reports_Page.Add_Template_WithPDFAttachment_AndValidate(PDF_Template_FoldPath, Randome_TemplateName, Input_ApprovalType, Approver_MailId, Notify_MailID), 
     			"Add a new Template is working.", "Add a new Template is NOT working.", driver);
     	
     	Log.assertThat(pdf_Reports_Page.Delete_Template_AndValidate(Randome_TemplateName, Input_ApprovalType), 
     			"Delete Template is working.", "Delete Template is NOT working.", driver);
     		     
    }
	catch(Exception e)
	{
        AnalyzeLog.analyzeLog(driver);
         e.getCause();
         Log.exception(e, driver);
	}
	finally
	{
		Log.endTestCase();
		driver.quit();
	}
}

/** TC_005 (PDF Report Module): Verify Create a report using No Approval required and validate.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 */
@Test(priority = 4, enabled = true, description = "TC_005 (PDF Report Module): Verify Create a report using No Approval required and validate.")
public void verify_CreateReport_NoApprovalRequired_AndValidate() throws Exception
{
	try
    {
     	Log.testCaseInfo("TC_005 (PDF Report Module): Verify Create a report using No Approval required and validate.");
     	//Getting Static data from properties file
   		String uName = PropertyReader.getProperty("NewData_Username");
   		String pWord = PropertyReader.getProperty("NewData_Password");
   		String Project_Name = "Report"+Generate_Random_Number.generateRandomValue();
   		String Randome_TemplateName = "Template_"+Generate_Random_Number.generateRandomValue();
   		String Input_ApprovalType = PropertyReader.getProperty("Input_ApprovalType1");
   		String Approver_MailId = PropertyReader.getProperty("Employee_Email");
   		String Notify_MailID = PropertyReader.getProperty("Employee2_Email");
   		File Path_FMProperties = new File(PropertyReader.getProperty("PDF_Template1_FilePath"));
   		String PDF_Template_FoldPath = Path_FMProperties.getAbsolutePath().toString();
   		
   		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     	folderPage=projectDashboardPage.createProject(Project_Name);//Create A Project
  		
     	pdf_Reports_Page = new PDF_Reports_Page(driver).get();
     	folderPage=pdf_Reports_Page.SelectPDFReportsTab();//Select Reports Tab
     
     	Log.assertThat(pdf_Reports_Page.Add_Template_WithPDFAttachment_AndValidate(PDF_Template_FoldPath, Randome_TemplateName, Input_ApprovalType, Approver_MailId, Notify_MailID), 
     			"Add a new Template is working.", "Add a new Template is NOT working.", driver);
     	
     	Log.assertThat(pdf_Reports_Page.Create_Report_NoApprovalRequired(Randome_TemplateName, Input_ApprovalType), 
     			"Create report is working.", "Create report is NOT working.", driver);
     	
    }
	catch(Exception e)
	{
        AnalyzeLog.analyzeLog(driver);
         e.getCause();
         Log.exception(e, driver);
	}
	finally
	{
		Log.endTestCase();
		driver.quit();
	}
}

/** TC_006 (PDF Report Module): Verify Draft a report and validate.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 */
@Test(priority = 5, enabled = true, description = "TC_006 (PDF Report Module): Verify Draft a report and validate.")
public void verify_DraftReport_AndValidate() throws Exception
{
	try
    {
     	Log.testCaseInfo("TC_006 (PDF Report Module): Verify Draft a report and validate.");
     	//Getting Static data from properties file
   		String uName = PropertyReader.getProperty("NewData_Username");
   		String pWord = PropertyReader.getProperty("NewData_Password");
   		String Project_Name = "Draft_Report"+Generate_Random_Number.generateRandomValue();
   		String Randome_TemplateName = "Template_"+Generate_Random_Number.generateRandomValue();
   		String Input_ApprovalType = PropertyReader.getProperty("Input_ApprovalType1");
   		String Approver_MailId = PropertyReader.getProperty("Employee_Email");
   		String Notify_MailID = PropertyReader.getProperty("Employee2_Email");
   		File Path_FMProperties = new File(PropertyReader.getProperty("PDF_Template1_FilePath"));
   		String PDF_Template_FoldPath = Path_FMProperties.getAbsolutePath().toString();
   		
   		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     	folderPage=projectDashboardPage.createProject(Project_Name);//Create A Project
  		
     	pdf_Reports_Page = new PDF_Reports_Page(driver).get();
     	folderPage=pdf_Reports_Page.SelectPDFReportsTab();//Select Reports Tab
     
     	Log.assertThat(pdf_Reports_Page.Add_Template_WithPDFAttachment_AndValidate(PDF_Template_FoldPath, Randome_TemplateName, Input_ApprovalType, Approver_MailId, Notify_MailID), 
     			"Add a new Template is working.", "Add a new Template is NOT working.", driver);
     	
     	Log.assertThat(pdf_Reports_Page.Draft_Report_Validate(Randome_TemplateName, Input_ApprovalType), 
     			"Draft a report is working.", "Draft a report is NOT working.", driver);     
    }
	catch(Exception e)
	{
        AnalyzeLog.analyzeLog(driver);
         e.getCause();
         Log.exception(e, driver);
	}
	finally
	{
		Log.endTestCase();
		driver.quit();
	}
}

/** TC_007 (PDF Report Module): Verify Create a report using Anyone can Approve and validate.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 */
@Test(priority = 6, enabled = true, description = "TC_007 (PDF Report Module): Verify Create a report using Anyone can Approve and validate.")
public void verify_CreateReport_AnyoneCanApprove() throws Exception
{
	try
    {
     	Log.testCaseInfo("TC_007 (PDF Report Module): Verify Create a report using Anyone can Approve and validate.");
     	//Getting Static data from properties file
   		String uName = PropertyReader.getProperty("NewData_Username");
   		String pWord = PropertyReader.getProperty("NewData_Password");
   		String Project_Name = "Report"+Generate_Random_Number.generateRandomValue();
   		//String Project_Name = PropertyReader.getProperty("Report_ProjectName");
   		String Randome_TemplateName = "Template_"+Generate_Random_Number.generateRandomValue();
   		String Input_ApprovalType = PropertyReader.getProperty("Input_ApprovalType2");
   		String Approver_MailId = PropertyReader.getProperty("Employee_Email");
   		String Notify_MailID = PropertyReader.getProperty("Employee2_Email");
   		File Path_FMProperties = new File(PropertyReader.getProperty("PDF_Template1_FilePath"));
   		String PDF_Template_FoldPath = Path_FMProperties.getAbsolutePath().toString();
   		
   		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     	folderPage=projectDashboardPage.createProject(Project_Name);//Create A Project
     	//folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
     	pdf_Reports_Page = new PDF_Reports_Page(driver).get();
     	folderPage=pdf_Reports_Page.SelectPDFReportsTab();//Select Reports Tab
     
     	Log.assertThat(pdf_Reports_Page.Add_Template_WithPDFAttachment_AndValidate(PDF_Template_FoldPath, Randome_TemplateName, Input_ApprovalType, Approver_MailId, Notify_MailID), 
     			"Add a new Template is working.", "Add a new Template is NOT working.", driver);
     	
     	Log.assertThat(pdf_Reports_Page.Create_Report_WithApprovalRequired(Randome_TemplateName, Input_ApprovalType), 
     			"Create report is working.", "Create report is NOT working.", driver);
    }
	catch(Exception e)
	{
        AnalyzeLog.analyzeLog(driver);
         e.getCause();
         Log.exception(e, driver);
	}
	finally
	{
		Log.endTestCase();
		driver.quit();
	}
}

/** TC_008 (PDF Report Module): Verify approve the report from approver side and validate.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 */
@Test(priority = 7, enabled = true, description = "TC_008 (PDF Report Module): Verify approve the report from approver side and validate.",groups = "naresh_test")
public void verify_Approve_report_validate() throws Exception
{
	try
    {
     	Log.testCaseInfo("TC_008 (PDF Report Module): Verify approve the report from approver side and validate.");
     	//Getting Static data from properties file
   		String uName = PropertyReader.getProperty("NewData_Username");
   		String pWord = PropertyReader.getProperty("NewData_Password");
   		String Project_Name = "Report"+Generate_Random_Number.generateRandomValue();
   		//String Project_Name = PropertyReader.getProperty("Report_ProjectName");
   		String Randome_TemplateName = "Template_"+Generate_Random_Number.generateRandomValue();
   		String Input_ApprovalType = PropertyReader.getProperty("Input_ApprovalType2");
   		String Approver_MailId = PropertyReader.getProperty("Employee_Email");
   		String Notify_MailID = PropertyReader.getProperty("Employee2_Email");
   		String CreatorName = PropertyReader.getProperty("TemplateCreater_Name");
   		String Approver_Name = PropertyReader.getProperty("Employee1_Name");
   		File Path_FMProperties = new File(PropertyReader.getProperty("PDF_Template1_FilePath"));
   		String PDF_Template_FoldPath = Path_FMProperties.getAbsolutePath().toString();
   		
   		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     	folderPage=projectDashboardPage.createProject(Project_Name);//Create A Project
     	//folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
     	pdf_Reports_Page = new PDF_Reports_Page(driver).get();
     	folderPage=pdf_Reports_Page.SelectPDFReportsTab();//Select Reports Tab
     
     	Log.assertThat(pdf_Reports_Page.Add_Template_WithPDFAttachment_AndValidate(PDF_Template_FoldPath, Randome_TemplateName, Input_ApprovalType, Approver_MailId, Notify_MailID), 
     			"Add a new Template is working.", "Add a new Template is NOT working.", driver);
     	
     	Log.assertThat(pdf_Reports_Page.Create_Report_WithApprovalRequired(Randome_TemplateName, Input_ApprovalType), 
     			"Create report is working.", "Create report is NOT working.", driver);
     	
     	projectDashboardPage.Logout_Projects();//Logout from creator
 		projectsLoginPage.loginWithValidCredential(Approver_MailId,pWord);//Login as Approver
 		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
 		pdf_Reports_Page.SelectPDFReportsTab();//Select Reports Tab
 		
 		String Act_ReportNO = "1";
 		String Exp_ReportNO = "2";
 		Log.assertThat(pdf_Reports_Page.Create_Submit_FMApproverSide_WithCommentAndAttachments(Randome_TemplateName, Input_ApprovalType, Act_ReportNO, CreatorName, Approver_Name, Exp_ReportNO), 
     			"Create and submit report with attachments is working.", "Create and submit report with attachments is NOT working.", driver);
 		
 		Log.assertThat(pdf_Reports_Page.Approve_Report_Validate(Randome_TemplateName, Input_ApprovalType, Approver_Name), 
     			"Create report is working.", "Create report is NOT working.", driver);

    }
	catch(Exception e)
	{
        AnalyzeLog.analyzeLog(driver);
         e.getCause();
         Log.exception(e, driver);
	}
	finally
	{
		Log.endTestCase();
		driver.quit();
	}
}

/** TC_009 (PDF Report Module): Verify Reject the report from approver side and validate.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 */
@Test(priority = 8, enabled = true, description = "TC_009 (PDF Report Module): Verify Reject the report from approver side and validate.")
public void verify_Reject_report_validate() throws Exception
{
	try
    {
     	Log.testCaseInfo("TC_009 (PDF Report Module): Verify Reject the report from approver side and validate.");
     	//Getting Static data from properties file
   		String uName = PropertyReader.getProperty("NewData_Username");
   		String pWord = PropertyReader.getProperty("NewData_Password");
   		String Project_Name = "Report"+Generate_Random_Number.generateRandomValue();
   		String Randome_TemplateName = "Template_"+Generate_Random_Number.generateRandomValue();
   		String Input_ApprovalType = PropertyReader.getProperty("Input_ApprovalType2");
   		String Approver_MailId = PropertyReader.getProperty("Employee_Email");
   		String Notify_MailID = PropertyReader.getProperty("Employee2_Email");
   		String CreatorName = PropertyReader.getProperty("TemplateCreater_Name");
   		String Approver_Name = PropertyReader.getProperty("Employee1_Name");
   		File Path_FMProperties = new File(PropertyReader.getProperty("PDF_Template1_FilePath"));
   		String PDF_Template_FoldPath = Path_FMProperties.getAbsolutePath().toString();
   		
   		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     	folderPage=projectDashboardPage.createProject(Project_Name);//Create A Project
     	pdf_Reports_Page = new PDF_Reports_Page(driver).get();
     	folderPage=pdf_Reports_Page.SelectPDFReportsTab();//Select Reports Tab
     
     	Log.assertThat(pdf_Reports_Page.Add_Template_WithPDFAttachment_AndValidate(PDF_Template_FoldPath, Randome_TemplateName, Input_ApprovalType, Approver_MailId, Notify_MailID), 
     			"Add a new Template is working.", "Add a new Template is NOT working.", driver);
     	
     	Log.assertThat(pdf_Reports_Page.Create_Report_WithApprovalRequired(Randome_TemplateName, Input_ApprovalType), 
     			"Create report is working.", "Create report is NOT working.", driver);
     	
     	projectDashboardPage.Logout_Projects();//Logout from creator
 		projectsLoginPage.loginWithValidCredential(Approver_MailId,pWord);//Login as Approver
 		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
 		pdf_Reports_Page.SelectPDFReportsTab();//Select Reports Tab
 		
 		String Act_ReportNO = "1";
 		String Exp_ReportNO = "2";
 		Log.assertThat(pdf_Reports_Page.Create_Submit_FMApproverSide_WithCommentAndAttachments(Randome_TemplateName, Input_ApprovalType, Act_ReportNO, CreatorName, Approver_Name, Exp_ReportNO), 
     			"Create and submit report with attachments is working.", "Create and submit report with attachments is NOT working.", driver);
 		
 		Log.assertThat(pdf_Reports_Page.Reject_Report_Validate(Randome_TemplateName, Input_ApprovalType, Approver_Name), 
     			"Reject a report is working.", "Reject a report is NOT working.", driver);
    }
	catch(Exception e)
	{
        AnalyzeLog.analyzeLog(driver);
         e.getCause();
         Log.exception(e, driver);
	}
	finally
	{
		Log.endTestCase();
		driver.quit();
	}
}

/** TC_010 (PDF Report Module): Verify Export Template and validate.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 */
@Test(priority = 9, enabled = true, description = "TC_010 (PDF Report Module): Verify Export Template and validate.")
public void verify_Export_Template_AndValidate() throws Exception
{
	try
    {
     	Log.testCaseInfo("TC_010 (PDF Report Module): Verify Export Template and validate.");
     	//Getting Static data from properties file
   		String uName = PropertyReader.getProperty("NewData_Username");
   		String pWord = PropertyReader.getProperty("NewData_Password");
   		String Project_Name = PropertyReader.getProperty("Report_ProjectName");
   		String Randome_TemplateName = "Samp_Template_"+Generate_Random_Number.generateRandomValue();
   		String Input_ApprovalType = PropertyReader.getProperty("Input_ApprovalType1");
   		String Report_CreatorName = PropertyReader.getProperty("TempCreater_ExportValidation");
   		String Approver_MailId = PropertyReader.getProperty("Employee_Email");
   		String Notify_MailID = PropertyReader.getProperty("Employee2_Email");
   		File Path_FMProperties = new File(PropertyReader.getProperty("PDF_Template1_FilePath"));
   		String PDF_Template_FoldPath = Path_FMProperties.getAbsolutePath().toString();
   		
   		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     	folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
  		
     	pdf_Reports_Page = new PDF_Reports_Page(driver).get();
     	folderPage=pdf_Reports_Page.SelectPDFReportsTab();//Select Reports Tab
     
     	Log.assertThat(pdf_Reports_Page.Add_Template_WithPDFAttachment_AndValidate(PDF_Template_FoldPath, Randome_TemplateName, Input_ApprovalType, Approver_MailId, Notify_MailID), 
     			"Add a new Template is working.", "Add a new Template is NOT working.", driver);
     
     	String usernamedir=System.getProperty("user.name");
     	String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
   		Log.assertThat(pdf_Reports_Page.Export_Templates_AndValidate(Randome_TemplateName, Input_ApprovalType, Sys_Download_Path, Report_CreatorName), 
   				"Report Export is working Successfully", "Report Export is NOT working", driver);
    }
	catch(Exception e)
	{
        AnalyzeLog.analyzeLog(driver);
         e.getCause();
         Log.exception(e, driver);
	}
	finally
	{
		Log.endTestCase();
		driver.quit();
	}
}

/** TC_011 (PDF Report Module): Verify approved report should not be editable.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 * Unit testing is Pending.
 */
@Test(priority = 10, enabled = true, description = "TC_011 (PDF Report Module): Verify approved report should not be editable.")
public void verify_Aproved_Report_ShouldNotEditable() throws Exception
{
	try
    {
     	Log.testCaseInfo("TC_011 (PDF Report Module): Verify approved report should not be editable.");
     	//Getting Static data from properties file
   		String uName = PropertyReader.getProperty("NewData_Username");
   		String pWord = PropertyReader.getProperty("NewData_Password");
   		String Project_Name = PropertyReader.getProperty("Aproved_ReportPrj");
   		String Template_Name = PropertyReader.getProperty("Aprvd_TempltName");
   		String Input_ApprovalType = PropertyReader.getProperty("Input_ApprovalType2");
   		String Report_CreatorName = PropertyReader.getProperty("TempCreater_ExportValidation");
   		
   		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     	folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
  		
     	pdf_Reports_Page = new PDF_Reports_Page(driver).get();
     	folderPage=pdf_Reports_Page.SelectPDFReportsTab();//Select Reports Tab
     
     	Log.assertThat(pdf_Reports_Page.Verify_AprvedReport_AbleToEdit(Template_Name, Input_ApprovalType, Report_CreatorName), 
     			"Approved Report can't be edit.", "Approved Report can be edit.", driver);
    }
	catch(Exception e)
	{
        AnalyzeLog.analyzeLog(driver);
         e.getCause();
         Log.exception(e, driver);
	}
	finally
	{
		Log.endTestCase();
		driver.quit();
	}
}

/** TC_012 (PDF Report Module): Verify Rejected report should able to resubmit.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 * Unit testing is Pending.
 */
@Test(priority = 11, enabled = true, description = "TC_012 (PDF Report Module): Verify Rejected report should able to resubmit.")
public void verify_Resubmit_Rejected_Report() throws Exception
{
	try
    {
     	Log.testCaseInfo("TC_012 (PDF Report Module): Verify Rejected report should able to resubmit.");
     	//Getting Static data from properties file
   		String uName = PropertyReader.getProperty("NewData_Username");
   		String pWord = PropertyReader.getProperty("NewData_Password");
   		String Project_Name = "Report"+Generate_Random_Number.generateRandomValue();
   		String Randome_TemplateName = "Template_"+Generate_Random_Number.generateRandomValue();
   		String Input_ApprovalType = PropertyReader.getProperty("Input_ApprovalType2");
   		String Approver_MailId = PropertyReader.getProperty("Employee_Email");
   		String Notify_MailID = PropertyReader.getProperty("Employee2_Email");
   		String CreatorName = PropertyReader.getProperty("TemplateCreater_Name");
   		String Approver_Name = PropertyReader.getProperty("Employee1_Name");
   		File Path_FMProperties = new File(PropertyReader.getProperty("PDF_Template1_FilePath"));
   		String PDF_Template_FoldPath = Path_FMProperties.getAbsolutePath().toString();
   		
   		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     	folderPage=projectDashboardPage.createProject(Project_Name);//Create A Project
     	pdf_Reports_Page = new PDF_Reports_Page(driver).get();
     	folderPage=pdf_Reports_Page.SelectPDFReportsTab();//Select Reports Tab
     
     	Log.assertThat(pdf_Reports_Page.Add_Template_WithPDFAttachment_AndValidate(PDF_Template_FoldPath, Randome_TemplateName, Input_ApprovalType, Approver_MailId, Notify_MailID), 
     			"Add a new Template is working.", "Add a new Template is NOT working.", driver);
     	
     	Log.assertThat(pdf_Reports_Page.Create_Report_WithApprovalRequired(Randome_TemplateName, Input_ApprovalType), 
     			"Create report is working.", "Create report is NOT working.", driver);
     	
     	projectDashboardPage.Logout_Projects();//Logout from creator
 		projectsLoginPage.loginWithValidCredential(Approver_MailId,pWord);//Login as Approver
 		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
 		pdf_Reports_Page.SelectPDFReportsTab();//Select Reports Tab
 		
 		String Act_ReportNO = "1";
 		String Exp_ReportNO = "2";
 		Log.assertThat(pdf_Reports_Page.Create_Report_WithApprovalRequired(Randome_TemplateName, Input_ApprovalType), 
     			"Submit report from approver is working.", "Submit report from approver is NOT working.", driver);
 		
 		Log.assertThat(pdf_Reports_Page.Reject_Report_Validate(Randome_TemplateName, Input_ApprovalType, Approver_Name), 
     			"Reject a report is working.", "Reject a report is NOT working.", driver);
 		
 		projectDashboardPage.Logout_Projects();//Logout from creator
 		projectsLoginPage.loginWithValidCredential(uName,pWord);//Login as Approver
 		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
 		pdf_Reports_Page.SelectPDFReportsTab();//Select Reports Tab
 		
 		//Going to resubmit the rejected report.
 		Log.assertThat(pdf_Reports_Page.Create_Report_WithApprovalRequired(Randome_TemplateName, Input_ApprovalType), 
     			"ReSubmit report is working.", "ReSubmit report is NOT working.", driver);
    }
	catch(Exception e)
	{
        AnalyzeLog.analyzeLog(driver);
         e.getCause();
         Log.exception(e, driver);
	}
	finally
	{
		Log.endTestCase();
		driver.quit();
	}
}

/** TC_013 (PDF Report Module): Verify Create report using All Must Approve and reject from any one then check the status should be Rejected.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 * Unit testing is Pending.
 */
@Test(priority = 12, enabled = true, description = "TC_013 (PDF Report Module): Verify Create report using All Must Approve and reject from any one then check the status should be Rejected.")
public void verify_CreateReport_AllMustAprov_RejectFromAnyOne_CheckTheStatus() throws Exception
{
	try
    {
     	Log.testCaseInfo("TC_013 (PDF Report Module): Verify Create report using All Must Approve and reject from any one then check the status should be Rejected.");
     	//Getting Static data from properties file
   		String uName = PropertyReader.getProperty("NewData_Username");
   		String pWord = PropertyReader.getProperty("NewData_Password");
   		String Project_Name = "Report"+Generate_Random_Number.generateRandomValue();
   		String Randome_TemplateName = "Template_"+Generate_Random_Number.generateRandomValue();
   		String Input_ApprovalType = PropertyReader.getProperty("Input_ApprovalType3");
   		String Approver_MailId = PropertyReader.getProperty("Employee_Email");
   		String Notify_MailID = PropertyReader.getProperty("Employee2_Email");
   		String CreatorName = PropertyReader.getProperty("TemplateCreater_Name");
   		String Approver_Name = PropertyReader.getProperty("Employee1_Name");
   		File Path_FMProperties = new File(PropertyReader.getProperty("PDF_Template1_FilePath"));
   		String PDF_Template_FoldPath = Path_FMProperties.getAbsolutePath().toString();
   		
   		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     	folderPage=projectDashboardPage.createProject(Project_Name);//Create A Project
     	pdf_Reports_Page = new PDF_Reports_Page(driver).get();
     	folderPage=pdf_Reports_Page.SelectPDFReportsTab();//Select Reports Tab
     
     	Log.assertThat(pdf_Reports_Page.Add_Template_WithPDFAttachment_AndValidate(PDF_Template_FoldPath, Randome_TemplateName, Input_ApprovalType, Approver_MailId, Notify_MailID), 
     			"Add a new Template is working.", "Add a new Template is NOT working.", driver);
     	
     	Log.assertThat(pdf_Reports_Page.Create_Report_WithApprovalRequired(Randome_TemplateName, Input_ApprovalType), 
     			"Create report is working.", "Create report is NOT working.", driver);
     	
     	projectDashboardPage.Logout_Projects();//Logout from creator
 		projectsLoginPage.loginWithValidCredential(Approver_MailId,pWord);//Login as Approver
 		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
 		pdf_Reports_Page.SelectPDFReportsTab();//Select Reports Tab
 		
 		String Act_ReportNO = "1";
 		String Exp_ReportNO = "2";
 		Log.assertThat(pdf_Reports_Page.Create_Report_WithApprovalRequired(Randome_TemplateName, Input_ApprovalType), 
     			"Submit report from approver is working.", "Submit report from approver is NOT working.", driver);
 		
 		Log.assertThat(pdf_Reports_Page.Reject_Report_Validate(Randome_TemplateName, Input_ApprovalType, Approver_Name), 
     			"Reject a report is working.", "Reject a report is NOT working.", driver);
    }
	catch(Exception e)
	{
        AnalyzeLog.analyzeLog(driver);
         e.getCause();
         Log.exception(e, driver);
	}
	finally
	{
		Log.endTestCase();
		driver.quit();
	}
}

}
