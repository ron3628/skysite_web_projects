package com.arc.projects.testscripts;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectAndFolder_Level_Search;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.ViewerScreenPage;
import com.arc.projects.pages.Viewer_ScreenRFI;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;

public class ViewerPage_RFI_Testcases {
	public static WebDriver driver;
	ProjectsLoginPage projectsLoginPage;
	ProjectDashboardPage projectDashboardPage;
	ViewerScreenPage viewerScreenPage;
	FolderPage folderPage;
	ProjectAndFolder_Level_Search projectAndFolder_Level_Search;

	@Parameters("browser")
	@BeforeMethod
	public WebDriver beforeTest(String browser) {

		if (browser.equalsIgnoreCase("firefox")) {
			File dest = new File("./drivers/win/geckodriver.exe");
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		else if (browser.equalsIgnoreCase("chrome")) {
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory", "C:" + File.separator + "Users" + File.separator
					+ System.getProperty("user.name") + File.separator + "Downloads");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			options.setExperimentalOption("prefs", prefs);
			driver = new ChromeDriver(options);
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		else if (browser.equalsIgnoreCase("safari")) {
			System.setProperty("webdriver.safari.noinstall", ""); 
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		return driver;
	}	
	
	
	
	
	
	//===============================Arrange Code===============================================
	//==========================================================================================
	
    /*TC_062 Execute 58-63 taping on the document thumbnail of the RFI details pop over window from the RFI listings.*/
	
	@Test(priority = 0, enabled = true, description = "Execute 58-63 taping on the document thumbnail of the RFI details pop over window from the RFI listings.")
	public void DocumentthumbnailRFIListing() throws Throwable {
		try {

			Log.testCaseInfo("TC_062 (RFI Testcases):Execute 58-63 taping on the document thumbnail of the RFI details pop over window from the RFI listings.");
			String uName1 = PropertyReader.getProperty("Username1");
			String pWord1 = PropertyReader.getProperty("Password1");
			String Project_Name = "Specific_Testdata_Static";
			// ==============Login with valid UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			// =======Guest User Level Validation=======================
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName1, pWord1);			
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// ================File Upload without Index=====================
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.RFICREATION_New();
			String Email1 = PropertyReader.getProperty("user1");
			String Email2 = PropertyReader.getProperty("user2");
			String Email3 = PropertyReader.getProperty("user3");
			Log.assertThat(projectAndFolder_Level_Search.RFIForwadedUser_Validation(Email1,Email2,Email3),"RFI forwaded user Validation working Successfully","RFI Forwaded Validation not working Successfully", driver);

			
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.close();
		}

	}
	
	
	// For RFI number further related cases need to validate the below ones as
		// well

		/*
		 * (1) TC_047Create one fresh project. 2) In the RFI settings after creating
		 * project change the: a) RFI starting number to AB-0001 b) Due in 2days. C)
		 * Create 5 custom attributes. Validation: Steps 1: i) Login in the web and
		 * take the change set and observe the project available in the listings.
		 * ii) Sync the project in web and move inside the RFI section. iii) Verfiy
		 * the create RFI button should be available in the top right corner and in
		 * the blank section of the window when no RFI is available. Steps 2a) i)
		 * Try to create RFI, in the create RFI pop over window check the order of
		 * custom attributes it should be similar as provided in web. Steps 2b and
		 * 2c: i) Create the RFI and check the RFI number it should be AB-0001 ii)
		 * The due days should be due in 2 days. iii) Check the custom attributes it
		 * should be similar as provided in setttings section. iv) Verify
		 * availability of comment field. v) Check the RFI number in the listings as
		 * well including the status. TestCase1: Starting  RFI with number(009)
		 * 
		 */

		@Test(priority = 1, enabled = true, description = "Starting  RFI with number(009)")
		public void StartingRFIwithNumber_009() throws Throwable {
			try {
				Log.testCaseInfo("TC_047 (RFI Testcases):Starting  RFI with number(009)");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String CustomAttribute5 = PropertyReader.getProperty("customAttribute5");
				String CustomAttribute1 = PropertyReader.getProperty("customAttribute1");
				String CustomAttribute2 = PropertyReader.getProperty("customAttribute2");
				String CustomAttribute3 = PropertyReader.getProperty("customAttribute3");
				String CustomAttribute4 = PropertyReader.getProperty("customAttribute4");
				String RFINUMBER_009 = PropertyReader.getProperty("RFINUMBER_009");
				String Project_Name = "Projet_" + Generate_Random_Number.MyDate();
				// String Project_Name = "Specific_Testdata_Static";
				// ==============Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);																							
																							
				// =========Create Project Randomly===================================
				folderPage = projectDashboardPage.createProjectwith_Five_CustomAtribute(Project_Name, CustomAttribute1,
						CustomAttribute2, CustomAttribute3, CustomAttribute4, CustomAttribute5, RFINUMBER_009);

				// ================File Upload without Index=====================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();
				projectAndFolder_Level_Search.RFI_CreationWithMultipleAttribute();			
				Log.assertThat(projectAndFolder_Level_Search.RFI_AttributeValidation(), "RFI Attribute Validation Completed Sucessfully", "RFI Attribute Validation  not Completed Sucessfully",driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}
		
		
		
		/*
		 * (2)Testcase_TC_048 Create one fresh project. 2) In the RFI settings after
		 * creating project change the: a) RFI starting number to AB-0001 b) Due in
		 * 2days. C) Create 5 custom attributes. Validation: Steps 1: i) Login in
		 * the web and take the change set and observe the project available in the
		 * listings. ii) Sync the project in web and move inside the RFI section.
		 * iii) Verfiy the create RFI button should be available in the top right
		 * corner and in the blank section of the window when no RFI is available.
		 * 
		 * Steps 2a) i) Try to create RFI, in the create RFI pop over window check
		 * the order of custom attributes it should be similar as provided in web.
		 * 
		 * Steps 2b and 2c: i) Create the RFI and check the RFI number it should be
		 * AB-0001 ii) The due days should be due in 2 days. iii) Check the custom
		 * attributes it should be similar as provided in setttings section. iv)
		 * Verify availability of comment field. v) Check the RFI number in the
		 * listings as well including the status. TestCase2: Starting  RFI with
		 * number(999)
		 */

		@Test(priority = 2, enabled = true, description = "Starting RFI with number(999)")
		public void StartingRFIwithnumber_999() throws Throwable {
			try {
				Log.testCaseInfo("TC_048(RFI Testcases):Starting  RFI with number(999)");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String CustomAttribute5 = PropertyReader.getProperty("customAttribute5");
				String CustomAttribute1 = PropertyReader.getProperty("customAttribute1");
				String CustomAttribute2 = PropertyReader.getProperty("customAttribute2");
				String CustomAttribute3 = PropertyReader.getProperty("customAttribute3");
				String CustomAttribute4 = PropertyReader.getProperty("customAttribute4");
				String RFINUMBER_999 = PropertyReader.getProperty("RFINUMBER_999");
				String Project_Name = "Projet_" + Generate_Random_Number.generateRandomValue();
				// ==============Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// =========Create Project Randomly===================================
				folderPage = projectDashboardPage.createProjectwith_Five_CustomAtribute(Project_Name, CustomAttribute1,
						CustomAttribute2, CustomAttribute3, CustomAttribute4, CustomAttribute5, RFINUMBER_999);
				// ================File Upload without Index=====================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();
				projectAndFolder_Level_Search.RFI_CreationWithMultipleAttribute();			
				Log.assertThat(projectAndFolder_Level_Search.RFI_AttributeValidation(), "RFI Attribute Validation Completed Sucessfully", "RFI Attribute Validation  not Completed Sucessfully",driver);


			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}

		/*
		 * (3) TC_049 Create one fresh project. 2) In the RFI settings after
		 * creating project change the: a) RFI starting number to AB-0001 b) Due in
		 * 2days. C) Create 5 custom attributes. Validation: Steps 1: i) Login in
		 * the web and take the change set and observe the project available in the
		 * listings. ii) Sync the project in web and move inside the RFI section.
		 * iii) Verfiy the create RFI button should be available in the top right
		 * corner and in the blank section of the window when no RFI is available.
		 * 
		 * Steps 2a) i) Try to create RFI, in the create RFI pop over window check
		 * the order of custom attributes it should be similar as provided in web.
		 * 
		 * Steps 2b and 2c: i) Create the RFI and check the RFI number it should be
		 * AB-0001 ii) The due days should be due in 2 days. iii) Check the custom
		 * attributes it should be similar as provided in setttings section. iv)
		 * Verify availability of comment field. v) Check the RFI number in the
		 * listings as well including the status. TestCase2: Starting  RFI with
		 * number(9999)
		 * 
		 */

		@Test(priority = 3, enabled = true , description = "Starting RFI with number(9999)")
		public void StartingRFIwithnumber_9999() throws Throwable {
			try {
				Log.testCaseInfo("TC_049 (RFI Testcases):Starting  RFI with number(9999)");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String CustomAttribute5 = PropertyReader.getProperty("customAttribute5");
				String CustomAttribute1 = PropertyReader.getProperty("customAttribute1");
				String CustomAttribute2 = PropertyReader.getProperty("customAttribute2");
				String CustomAttribute3 = PropertyReader.getProperty("customAttribute3");
				String CustomAttribute4 = PropertyReader.getProperty("customAttribute4");
				String RFINUMBER_9999 = PropertyReader.getProperty("RFINUMBER_9999");
				String Project_Name = "Projet_" + Generate_Random_Number.generateRandomValue();
				// ==============Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// =========Create Project Randomly===================================
				folderPage = projectDashboardPage.createProjectwith_Five_CustomAtribute(Project_Name, CustomAttribute1,
						CustomAttribute2, CustomAttribute3, CustomAttribute4, CustomAttribute5, RFINUMBER_9999);
				// ================File Upload without Index=====================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();
				projectAndFolder_Level_Search.RFI_CreationWithMultipleAttribute();
				//projectAndFolder_Level_Search.ProjectManagement();
				Log.assertThat(projectAndFolder_Level_Search.RFI_AttributeValidation(),"RFI Attribute Validation working Successfully","RFI Attribute Validation not working Successfully", driver);



			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}
		
		
		/*
		 * (4)TC_050 Create one fresh project. 2) In the RFI settings after creating
		 * project change the: a) RFI starting number to AB-0001 b) Due in 2days. C)
		 * Create 5 custom attributes. Validation: Steps 1: i) Login in the web and
		 * take the change set and observe the project available in the listings.
		 * ii) Sync the project in web and move inside the RFI section. iii) Verfiy
		 * the create RFI button should be available in the top right corner and in
		 * the blank section of the window when no RFI is available.
		 * 
		 * Steps 2a) i) Try to create RFI, in the create RFI pop over window check
		 * the order of custom attributes it should be similar as provided in web.
		 * 
		 * Steps 2b and 2c: i) Create the RFI and check the RFI number it should be
		 * AB-0001 ii) The due days should be due in 2 days. iii) Check the custom
		 * attributes it should be similar as provided in setttings section. iv)
		 * Verify availability of comment field. v) Check the RFI number in the
		 * listings as well including the status. TestCase2: Starting  RFI with
		 * number(9999999)
		 * 
		 */

		@Test(priority = 4, enabled = true, description = "Starting RFI with number(9999999)")
		public void StartingRFIwithnumber_9999999() throws Throwable {
			try {
				Log.testCaseInfo("TC_050 (RFI Testcases):Starting  RFI with number(9999999)");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String CustomAttribute5 = PropertyReader.getProperty("customAttribute5");
				String CustomAttribute1 = PropertyReader.getProperty("customAttribute1");
				String CustomAttribute2 = PropertyReader.getProperty("customAttribute2");
				String CustomAttribute3 = PropertyReader.getProperty("customAttribute3");
				String CustomAttribute4 = PropertyReader.getProperty("customAttribute4");
				String RFINUMBER_9999999 = PropertyReader.getProperty("RFINUMBER_9999999");
				String Project_Name = "Projet_" + Generate_Random_Number.generateRandomValue();
				// ==============Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// =========Create Project Randomly===================================
				folderPage = projectDashboardPage.createProjectwith_Five_CustomAtribute(Project_Name, CustomAttribute1,
						CustomAttribute2, CustomAttribute3, CustomAttribute4, CustomAttribute5, RFINUMBER_9999999);
				// ================File Upload without Index=====================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();
				projectAndFolder_Level_Search.RFI_CreationWithMultipleAttribute();
				//Log.assertThat(projectAndFolder_Level_Search.RFI_CreationWithMultipleAttribute(),"RFI creation with multiple Validation working Successfully","RFI creation with multiple Validation not working Successfully", driver);
				//projectAndFolder_Level_Search.ProjectManagement();
				Log.assertThat(projectAndFolder_Level_Search.RFI_AttributeValidation(),"RFI Attribute Validation working Successfully","RFI Attribute Validation not working Successfully", driver);


			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}

		/*
		 * 5)TC_051 Create one fresh project. 2) In the RFI settings after creating
		 * project change the: a) RFI starting number to AB-0001 b) Due in 2days. C)
		 * Create 5 custom attributes. Validation: Steps 1: i) Login in the web and
		 * take the change set and observe the project available in the listings.
		 * ii) Sync the project in web and move inside the RFI section. iii) Verfiy
		 * the create RFI button should be available in the top right corner and in
		 * the blank section of the window when no RFI is available.
		 * 
		 * Steps 2a) i) Try to create RFI, in the create RFI pop over window check
		 * the order of custom attributes it should be similar as provided in web.
		 * 
		 * Steps 2b and 2c: i) Create the RFI and check the RFI number it should be
		 * AB-0001 ii) The due days should be due in 2 days. iii) Check the custom
		 * attributes it should be similar as provided in setttings section. iv)
		 * Verify availability of comment field. v) Check the RFI number in the
		 * listings as well including the status. TestCase2: Starting  RFI with
		 * Special Character
		 * 
		 */

		@Test(priority = 5, enabled = true, description = "Starting RFI No with Special Characterts")
		public void RFInumberwithSpecialCharacters() throws Throwable {
			try {
				Log.testCaseInfo("TC_051 (RFI Testcases):Starting RFI No with Special Characterts");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String CustomAttribute5 = PropertyReader.getProperty("customAttribute5");
				String CustomAttribute1 = PropertyReader.getProperty("customAttribute1");
				String CustomAttribute2 = PropertyReader.getProperty("customAttribute2");
				String CustomAttribute3 = PropertyReader.getProperty("customAttribute3");
				String CustomAttribute4 = PropertyReader.getProperty("customAttribute4");
				String RFINUMBER_Splcharacter = PropertyReader.getProperty("RFINUMBER_withSpecialCharacter");
				String Project_Name = "Projet_" + Generate_Random_Number.MyDate();
				// ==============Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// =========Create Project Randomly===================================
				folderPage = projectDashboardPage.createProjectwith_Five_CustomAtribute(Project_Name, CustomAttribute1,
						CustomAttribute2, CustomAttribute3, CustomAttribute4, CustomAttribute5, RFINUMBER_Splcharacter);
				// ================File Upload without Index=====================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();
				projectAndFolder_Level_Search.RFI_CreationWithMultipleAttribute();
				projectAndFolder_Level_Search.ProjectManagement();		
				Log.assertThat(projectAndFolder_Level_Search.RFI_AttributeValidation(),"RFI Attribute Validation working Successfully","RFI Attribute Validation not working Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}
		
		
		/*
		 * 1) TC_052Create one fresh project. 2) In the RFI settings after creating
		 * project change the: a) RFI starting number to AB-0001 b) Due in 2days. C)
		 * Create 5 custom attributes. Validation: Steps 1: i) Login in the web and
		 * take the change set and observe the project available in the listings.
		 * ii) Sync the project in web and move inside the RFI section. iii) Verfiy
		 * the create RFI button should be available in the top right corner and in
		 * the blank section of the window when no RFI is available.
		 * 
		 * Steps 2a) i) Try to create RFI, in the create RFI pop over window check
		 * the order of custom attributes it should be similar as provided in web.
		 * 
		 * Steps 2b and 2c: i) Create the RFI and check the RFI number it should be
		 * AB-0001 ii) The due days should be due in 2 days. iii) Check the custom
		 * attributes it should be similar as provided in setttings section. iv)
		 * Verify availability of comment field. v) Check the RFI number in the
		 * listings as well including the status. TestCase2: Check Validation if
		 * Given special characters given before alphabets (-AB00001)
		 * 
		 */

		@Test(priority = 6, enabled = true, description = "Check Validation if Given special characters given before alphabets ")
		public void CheckValidationifGiven_special_characters_before_alphabets() throws Throwable {
			try {
				Log.testCaseInfo(
						"TC_052 (RFI )Testcases:Check Validation if Given special characters given before alphabets ");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String CustomAttribute5 = PropertyReader.getProperty("customAttribute5");
				String CustomAttribute1 = PropertyReader.getProperty("customAttribute1");
				String CustomAttribute2 = PropertyReader.getProperty("customAttribute2");
				String CustomAttribute3 = PropertyReader.getProperty("customAttribute3");
				String CustomAttribute4 = PropertyReader.getProperty("customAttribute4");
				String RFINUMBER_alphabet = PropertyReader.getProperty("RFINUMBER_SplChacter_Alphabet");
				String Project_Name = "Projet_" + Generate_Random_Number.generateRandomValue();
				// ==============Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// =========Create Project Randomly===================================
				folderPage = projectDashboardPage.createProjectwith_Five_CustomAtribute(Project_Name, CustomAttribute1,
						CustomAttribute2, CustomAttribute3, CustomAttribute4, CustomAttribute5, RFINUMBER_alphabet);
				// ================File Upload without Index=====================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();	
				Log.assertThat(projectAndFolder_Level_Search.RFIProjectlevelMessageValidation(),"RFI Message validation Display Successfully","RFI Message  not Displaying Successfully", driver);
				viewerScreenPage = new ViewerScreenPage(driver).get();
				viewerScreenPage.Logout();

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}

		/*
		 * 1) TC_053Create one fresh project. 2) In the RFI settings after creating
		 * project change the: a) RFI starting number to AB-0001 b) Due in 2days. C)
		 * Create 5 custom attributes. Validation: Steps 1: i) Login in the web and
		 * take the change set and observe the project available in the listings.
		 * ii) Sync the project in web and move inside the RFI section. iii) Verfiy
		 * the create RFI button should be available in the top right corner and in
		 * the blank section of the window when no RFI is available.
		 * 
		 * Steps 2a) i) Try to create RFI, in the create RFI pop over window check
		 * the order of custom attributes it should be similar as provided in web.
		 * 
		 * Steps 2b and 2c: i) Create the RFI and check the RFI number it should be
		 * AB-0001 ii) The due days should be due in 2 days. iii) Check the custom
		 * attributes it should be similar as provided in setttings section. iv)
		 * Verify availability of comment field. v) Check the RFI number in the
		 * listings as well including the status. TestCase2: Special character
		 * testing in the RFI number field. (#00001)
		 * 
		 */

		@Test(priority = 7, enabled = true, description = "Special character testing in the RFI number field.")
		public void Verify_SpecialCharacter_TestingIn_RFINumber_Field() throws Throwable {
			try {
				Log.testCaseInfo("TC_053 (RFI )Testcases:Special character testing in the RFI number field.");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String CustomAttribute5 = PropertyReader.getProperty("customAttribute5");
				String CustomAttribute1 = PropertyReader.getProperty("customAttribute1");
				String CustomAttribute2 = PropertyReader.getProperty("customAttribute2");
				String CustomAttribute3 = PropertyReader.getProperty("customAttribute3");
				String CustomAttribute4 = PropertyReader.getProperty("customAttribute4");
				String SpecialCharacter = PropertyReader.getProperty("SplCharacter_RFINUMBER");
				String Project_Name = "Projet_" + Generate_Random_Number.generateRandomValue();
				// ==============Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// =========Create Project Randomly===================================
				folderPage = projectDashboardPage.createProjectwith_Five_CustomAtribute(Project_Name, CustomAttribute1,
						CustomAttribute2, CustomAttribute3, CustomAttribute4, CustomAttribute5, SpecialCharacter);
				// ================File Upload without Index=====================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				Log.assertThat(projectAndFolder_Level_Search.RFIProjectlevelMessageValidation(),"RFI Message validation Display Successfully","RFI Message  not Displaying Successfully", driver);

				viewerScreenPage = new ViewerScreenPage(driver).get();
				viewerScreenPage.Logout();

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}
		
		
		/*
		 * 1) TC_054Create one fresh project. 2) In the RFI settings after creating
		 * project change the: a) RFI starting number to AB-0001 b) Due in 2days. C)
		 * Create 5 custom attributes. Validation: Steps 1: i) Login in the web and
		 * take the change set and observe the project available in the listings.
		 * ii) Sync the project in web and move inside the RFI section. iii) Verfiy
		 * the create RFI button should be available in the top right corner and in
		 * the blank section of the window when no RFI is available.
		 * 
		 * Steps 2a) i) Try to create RFI, in the create RFI pop over window check
		 * the order of custom attributes it should be similar as provided in web.
		 * 
		 * Steps 2b and 2c: i) Create the RFI and check the RFI number it should be
		 * AB-0001 ii) The due days should be due in 2 days. iii) Check the custom
		 * attributes it should be similar as provided in setttings section. iv)
		 * Verify availability of comment field. v) Check the RFI number in the
		 * listings as well including the status. TestCase2: Check the validation
		 * without giving starting RFI number
		 * 
		 */

		@Test(priority = 8, enabled = true, description = "Check the validation without giving starting RFI number")
		public void Check_ValidationWithout_GivingRFI() throws Throwable {
			try {
				Log.testCaseInfo("TC_054 (RFI )Testcases:Check the validation without giving starting RFI number");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String CustomAttribute5 = PropertyReader.getProperty("customAttribute5");
				String CustomAttribute1 = PropertyReader.getProperty("customAttribute1");
				String CustomAttribute2 = PropertyReader.getProperty("customAttribute2");
				String CustomAttribute3 = PropertyReader.getProperty("customAttribute3");
				String CustomAttribute4 = PropertyReader.getProperty("customAttribute4");
				String WithoutRFI = PropertyReader.getProperty("withoutRFI_Number");
				String Project_Name = "Projet_" + Generate_Random_Number.generateRandomValue();
				// ==============Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// =========Create Project Randomly===================================
				folderPage = projectDashboardPage.createProjectwith_Five_CustomAtribute(Project_Name, CustomAttribute1,
						CustomAttribute2, CustomAttribute3, CustomAttribute4, CustomAttribute5, WithoutRFI);
				// ================File Upload without Index=====================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				Log.assertThat(projectAndFolder_Level_Search.RFIProjectlevelMessageValidation_blank(),"RFI Message validation Display Successfully","RFI Message  not Displaying Successfully", driver);

				viewerScreenPage = new ViewerScreenPage(driver).get();
				viewerScreenPage.Logout();

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}
		
		
		
		// For RFI number further related cases need to validate the below ones as
		// well

		/*
		 * (1) TC_065 Create one fresh project. 2) In the RFI settings after
		 * creating project change the: a) RFI starting number to AB-0001 b) Due in
		 * 2days. C) Create 5 custom attributes. Validation: Steps 1: i) Login in
		 * the web and take the change set and observe the project available in the
		 * listings. ii) Sync the project in web and move inside the RFI section.
		 * iii) Verfiy the create RFI button should be available in the top right
		 * corner and in the blank section of the window when no RFI is available.
		 * Steps 2a) i) Try to create RFI, in the create RFI pop over window check
		 * the order of custom attributes it should be similar as provided in web.
		 * Steps 2b and 2c: i) Create the RFI and check the RFI number it should be
		 * AB-0001 ii) The due days should be due in 2 days. iii) Check the custom
		 * attributes it should be similar as provided in setttings section. iv)
		 * Verify availability of comment field. v) Check the RFI number in the
		 * listings as well including the status. TestCase1:Add RFI Custom
		 * attributes with Special Characters
		 * 
		 */

		@Test(priority = 9, enabled = true, description = "Add RFI Custom attributes with Special Characters")
		public void CustomAttribute_SPL_Character() throws Throwable {
			try {
				Log.testCaseInfo("TC_065 (RFI Testcases):Add RFI Custom attributes with Special Characters");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String CustomAttribute1 = PropertyReader.getProperty("customAttribute_SplChacter");
				String Project_Name = "Projet_" + Generate_Random_Number.generateRandomValue();
				// ==============Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// =========Create Project Randomly===================================
				folderPage = projectDashboardPage.createProjectwithCustomAtribute(Project_Name, CustomAttribute1);
				// ================File Upload without Index=====================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();
				projectAndFolder_Level_Search.RFI_CreationWithSingleAttribute();
				Log.assertThat(projectAndFolder_Level_Search.RFI_Attribute_Single_Validation(CustomAttribute1),"RFI  Single Validation completed Successfully","RFI  Single Validation noy completed Successfully", driver);
				

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}

		// For RFI number further related cases need to validate the below ones as
		// well

		/*
		 * (1) TC_066 Create one fresh project. 2) In the RFI settings after
		 * creating project change the: a) RFI starting number to AB-0001 b) Due in
		 * 2days. C) Create 5 custom attributes. Validation: Steps 1: i) Login in
		 * the web and take the change set and observe the project available in the
		 * listings. ii) Sync the project in web and move inside the RFI section.
		 * iii) Verfiy the create RFI button should be available in the top right
		 * corner and in the blank section of the window when no RFI is available.
		 * Steps 2a) i) Try to create RFI, in the create RFI pop over window check
		 * the order of custom attributes it should be similar as provided in web.
		 * Steps 2b and 2c: i) Create the RFI and check the RFI number it should be
		 * AB-0001 ii) The due days should be due in 2 days. iii) Check the custom
		 * attributes it should be similar as provided in setttings section. iv)
		 * Verify availability of comment field. v) Check the RFI number in the
		 * listings as well including the status. TestCase1:Add RFI Custom
		 * attributes with Alphabets
		 * 
		 */

		@Test(priority = 10, enabled =true, description = "Add RFI Custom attributes with Alphabets")
		public void CustomAttribute_with_Alphabet() throws Throwable {
			try {
				Log.testCaseInfo("TC_066 (RFI Testcases):Add RFI Custom attributes with Alphabets");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String CustomAttribute1 = PropertyReader.getProperty("customAttribute_Alphabets");
				String Project_Name = "Projet_" + Generate_Random_Number.MyDate();
				// ==============Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// =========Create Project Randomly===================================
				folderPage = projectDashboardPage.createProjectwithCustomAtribute(Project_Name, CustomAttribute1);
				// ================File Upload without Index=====================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();			
				projectAndFolder_Level_Search.RFI_CreationWithSingleAttribute();
				//projectAndFolder_Level_Search.RFI_CreationWithSingleAttribute();
				Log.assertThat(projectAndFolder_Level_Search.RFI_Attribute_Single_Validation(CustomAttribute1),"RFI  Single Validation completed Successfully","RFI  Single Validation noy completed Successfully", driver);
				

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}
		
		// For RFI number further related cases need to validate the below ones as
		// well

		/*
		 * (1) TC_067 Create one fresh project. 2) In the RFI settings after
		 * creating project change the: a) RFI starting number to AB-0001 b) Due in
		 * 2days. C) Create 5 custom attributes. Validation: Steps 1: i) Login in
		 * the web and take the change set and observe the project available in the
		 * listings. ii) Sync the project in web and move inside the RFI section.
		 * iii) Verfiy the create RFI button should be available in the top right
		 * corner and in the blank section of the window when no RFI is available.
		 * Steps 2a) i) Try to create RFI, in the create RFI pop over window check
		 * the order of custom attributes it should be similar as provided in web.
		 * Steps 2b and 2c: i) Create the RFI and check the RFI number it should be
		 * AB-0001 ii) The due days should be due in 2 days. iii) Check the custom
		 * attributes it should be similar as provided in setttings section. iv)
		 * Verify availability of comment field. v) Check the RFI number in the
		 * listings as well including the status. TestCase1:Add RFI Custom
		 * attributes with Alphabets
		 * 
		 */

		@Test(priority = 11, enabled = true, description = "Add RFI Custom attributes with Numbers")
		public void CustomAttribute_with_Number() throws Throwable {
			try {
				Log.testCaseInfo("TC_067 (RFI Testcases):Add RFI Custom attributes with Numbers");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String CustomAttribute1 = PropertyReader.getProperty("customAttribute_Number");
				String Project_Name = "Projet_" + Generate_Random_Number.MyDate();
				// ==============Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// =========Create Project Randomly===================================
				folderPage = projectDashboardPage.createProjectwithCustomAtribute(Project_Name, CustomAttribute1);
				// ================File Upload without Index=====================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();
				projectAndFolder_Level_Search.RFI_CreationWithSingleAttribute();
				Log.assertThat(projectAndFolder_Level_Search.RFI_Attribute_Single_Validation(CustomAttribute1),"RFI  Single Validation completed Successfully","RFI  Single Validation noy completed Successfully", driver);
				
			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}

		// For RFI number further related cases need to validate the below ones as
		// well

		/*
		 * (1) TC_068 Create one fresh project. 2) In the RFI settings after
		 * creating project change the: a) RFI starting number to AB-0001 b) Due in
		 * 2days. C) Create 5 custom attributes. Validation: Steps 1: i) Login in
		 * the web and take the change set and observe the project available in the
		 * listings. ii) Sync the project in web and move inside the RFI section.
		 * iii) Verfiy the create RFI button should be available in the top right
		 * corner and in the blank section of the window when no RFI is available.
		 * Steps 2a) i) Try to create RFI, in the create RFI pop over window check
		 * the order of custom attributes it should be similar as provided in web.
		 * Steps 2b and 2c: i) Create the RFI and check the RFI number it should be
		 * AB-0001 ii) The due days should be due in 2 days. iii) Check the custom
		 * attributes it should be similar as provided in setttings section. iv)
		 * Verify availability of comment field. v) Check the RFI number in the
		 * listings as well including the status. TestCase1:Add RFI Custom
		 * attributes with Alphabets
		 * 
		 */

		@Test(priority = 12, enabled = true, description = "Add RFI Custom Attributes with The combinaton of Special Characters,numbers and alphabets")
		public void CustomAttribute_with_Spl_Alphabet_number() throws Throwable {
			try {
				Log.testCaseInfo(
						"TC_068 (RFI Testcases):Add RFI Custom Attributes with The combinaton of Special Characters,numbers and alphabets");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String CustomAttribute1 = PropertyReader.getProperty("customAttribute_Spl_Alpha_Number");
				String Project_Name = "Projet_" + Generate_Random_Number.MyDate();
				// ==============Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// =========Create Project Randomly===================================
				folderPage = projectDashboardPage.createProjectwithCustomAtribute(Project_Name, CustomAttribute1);
				// ================File Upload without Index=====================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();
				projectAndFolder_Level_Search.RFI_CreationWithSingleAttribute();			
				Log.assertThat(projectAndFolder_Level_Search.RFI_Attribute_Single_Validation(CustomAttribute1),"RFI  Single Validation completed Successfully","RFI  Single Validation noy completed Successfully", driver);
				

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}

		// For RFI number further related cases need to validate the below ones as
		// well

		/*
		 * (1) TC_070 Create one fresh project. 2) In the RFI settings after
		 * creating project change the: a) RFI starting number to AB-0001 b) Due in
		 * 2days. C) Create 5 custom attributes. Validation: Steps 1: i) Login in
		 * the web and take the change set and observe the project available in the
		 * listings. ii) Sync the project in web and move inside the RFI section.
		 * iii) Verfiy the create RFI button should be available in the top right
		 * corner and in the blank section of the window when no RFI is available.
		 * Steps 2a) i) Try to create RFI, in the create RFI pop over window check
		 * the order of custom attributes it should be similar as provided in web.
		 * Steps 2b and 2c: i) Create the RFI and check the RFI number it should be
		 * AB-0001 ii) The due days should be due in 2 days. iii) Check the custom
		 * attributes it should be similar as provided in setttings section. iv)
		 * Verify availability of comment field. v) Check the RFI number in the
		 * listings as well including the status. TestCase1: Custom attributes upto
		 * maximum five
		 * 
		 */

		@Test(priority = 13, enabled = true, description = "Custom attributes upto maximum five")
		public void Customattributesuptomaximum() throws Throwable {
			try {
				Log.testCaseInfo("TC_070 (RFI Testcases):Custom attributes upto maximum FIVE");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String CustomAttribute5 = PropertyReader.getProperty("customAttribute5");
				String CustomAttribute1 = PropertyReader.getProperty("customAttribute1");
				String CustomAttribute2 = PropertyReader.getProperty("customAttribute2");
				String CustomAttribute3 = PropertyReader.getProperty("customAttribute3");
				String CustomAttribute4 = PropertyReader.getProperty("customAttribute4");
				String RFINUMBER_009 = PropertyReader.getProperty("RFINUMBER_009");
				String Project_Name = "Projet_" + Generate_Random_Number.MyDate();
				// String Project_Name = "Specific_Testdata_Static";
				// ==============Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// =========Create Project Randomly===================================
				folderPage = projectDashboardPage.createProjectwith_Five_CustomAtribute(Project_Name, CustomAttribute1,
						CustomAttribute2, CustomAttribute3, CustomAttribute4, CustomAttribute5, RFINUMBER_009);
				// ================File Upload without Index=====================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();
				projectAndFolder_Level_Search.RFI_CreationWithMultipleAttribute();
				projectAndFolder_Level_Search.ProjectManagement();
				Log.assertThat(projectAndFolder_Level_Search.RFI_AttributeValidation(),"RFI  Attribute Validated Successfully","RFI  Attribute validated not  Successfully", driver);
				
			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}

		// For RFI number further related cases need to validate the below ones as
		// well

		/*
		 * (1) TC_069 Create one fresh project. 2) In the RFI settings after
		 * creating project change the: a) RFI starting number to AB-0001 b) Due in
		 * 2days. C) Create 5 custom attributes. Validation: Steps 1: i) Login in
		 * the web and take the change set and observe the project available in the
		 * listings. ii) Sync the project in web and move inside the RFI section.
		 * iii) Verfiy the create RFI button should be available in the top right
		 * corner and in the blank section of the window when no RFI is available.
		 * Steps 2a) i) Try to create RFI, in the create RFI pop over window check
		 * the order of custom attributes it should be similar as provided in web.
		 * Steps 2b and 2c: i) Create the RFI and check the RFI number it should be
		 * AB-0001 ii) The due days should be due in 2 days. iii) Check the custom
		 * attributes it should be similar as provided in setttings section. iv)
		 * Verify availability of comment field. v) Check the RFI number in the
		 * listings as well including the status. TestCase: Check the Duplication of
		 * Custom Atrributes
		 * 
		 */

		@Test(priority = 14, enabled = true, description = "Check the Duplication of Custom Atrributes")
		public void DuplicateCustomAtribute() throws Throwable {
			try {
				Log.testCaseInfo("TC_069 (RFI Testcases):Check the Duplication of Custom Atrributes");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String CustomAttribute1 = PropertyReader.getProperty("customAttribute1");
				String CustomAttribute2 = PropertyReader.getProperty("DuplicaecustomAttribute");
				String RFINUMBER = PropertyReader.getProperty("RFINUMBER_009");
				String Project_Name = "Projet_" + Generate_Random_Number.MyDate();
				// String Project_Name = "Specific_Testdata_Static";
				// ==============Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// =========Create Project Randomly===================================
				folderPage = projectDashboardPage.createProjectwith_two_CustomAtribute(Project_Name, CustomAttribute1,CustomAttribute2, RFINUMBER);

				// ================File Upload without Index=====================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();			
				Log.assertThat(projectAndFolder_Level_Search.RFI_CreationWithMultipleAttribute(),"RFI Multiple Attribute created Successfully","RFI Multiple Attribute  not created  Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}

		/*
		 * TC_073 Verify the sorting with: 1) Owner. 2) Employee. 3) Guest user.
		 * Steps: 1) Move inside the RFI section. 2) Change the sorting to ascending
		 * to descending and vice versa.
		 */
		@Test(priority = 15, enabled = true , description = "Change the sorting to ascending to descending and vice versa")
		public void SortingwithAscendingAndDecending() throws Throwable {
			try {
				Log.testCaseInfo("TC_073 (RFI Testcases):Change the sorting to ascending to descending and vice versa");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String uName1 = PropertyReader.getProperty("Username2");
				String pWord1 = PropertyReader.getProperty("Password2");
				String uName2 = PropertyReader.getProperty("GuestUserName");
				String pWord2 = PropertyReader.getProperty("GuestUserPassword");				
				// ==============Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// =========Create Project Randomly===================================
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				// ================File Upload without Index=====================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();
				projectAndFolder_Level_Search.RFI_AscendingorderValidation();
				projectAndFolder_Level_Search.RFI_DecendingorderValidation();
				//Log.assertThat(projectAndFolder_Level_Search.RFI_AscendingorderValidation(),"Ascending order Validation working Successfully","Ascending order Validation not working Successfully", driver);
				//Log.assertThat(projectAndFolder_Level_Search.RFI_DecendingorderValidation(),"Decending order Validation working Successfully","Decending order Validation not working Successfully", driver);
			   	projectAndFolder_Level_Search.Logout();
				// ========= Employee Level validation=============================
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName1, pWord1);// Calling Login MethodS
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				// ================File Upload without Index=====================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();
				projectAndFolder_Level_Search.RFI_AscendingorderValidation();
				projectAndFolder_Level_Search.RFI_DecendingorderValidation();
				//Log.assertThat(projectAndFolder_Level_Search.RFI_AscendingorderValidation(),"Ascending order Validation working Successfully","Ascending order Validation not working Successfully", driver);
				//Log.assertThat(projectAndFolder_Level_Search.RFI_DecendingorderValidation(),"Decending order Validation working Successfully","Decending order Validation not working Successfully", driver);
			   	projectAndFolder_Level_Search.Logout();
				// =======Guest User Level Validation=======================
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName2, pWord2);															
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				// ================File Upload without Index=====================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();
				projectAndFolder_Level_Search.RFI_AscendingorderValidation();
				projectAndFolder_Level_Search.RFI_DecendingorderValidation();
				//Log.assertThat(projectAndFolder_Level_Search.RFI_AscendingorderValidation(),"Ascending order Validation working Successfully","Ascending order Validation not working Successfully", driver);
				//Log.assertThat(projectAndFolder_Level_Search.RFI_DecendingorderValidation(),"Decending order Validation working Successfully","Decending order Validation not working Successfully", driver);
			    projectAndFolder_Level_Search.Logout();

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}

		/*
		 * (1) TC_0071Create one fresh project. 2) In the RFI settings after
		 * creating project change the: a) RFI starting number to AB-0001 b) Due in
		 * 2days. C) Create 5 custom attributes. Validation: Steps 1: i) Login in
		 * the web and take the change set and observe the project available in the
		 * listings. ii) Sync the project in web and move inside the RFI section.
		 * iii) Verfiy the create RFI button should be available in the top right
		 * corner and in the blank section of the window when no RFI is available.
		 * Steps 2a) i) Try to create RFI, in the create RFI pop over window check
		 * the order of custom attributes it should be similar as provided in web.
		 * Steps 2b and 2c: i) Create the RFI and check the RFI number it should be
		 * AB-0001 ii) The due days should be due in 2 days. iii) Check the custom
		 * attributes it should be similar as provided in setttings section. iv)
		 * Verify availability of comment field. v) Check the RFI number in the
		 * listings as well including the status. TestCase1: Check in order to
		 * delete the already used custom attributes.
		 * 
		 */

		@Test(priority = 16, enabled = true, description = "Check in order to delete the already used custom attributes.")
		public void CheckAndOrderAlreadyUsedCustomAttribute() throws Throwable {
			try {

				Log.testCaseInfo("TC_071 (RFI Testcases):Login as the guest user and sync that particular project and move into the ALL RFI list");
				String uName1 = PropertyReader.getProperty("Username1");
				String pWord1 = PropertyReader.getProperty("Password1");
				String CustomAttribute5 = PropertyReader.getProperty("customAttribute5");
				String CustomAttribute1 = PropertyReader.getProperty("customAttribute1");
				String CustomAttribute2 = PropertyReader.getProperty("customAttribute2");
				String CustomAttribute3 = PropertyReader.getProperty("customAttribute3");
				String CustomAttribute4 = PropertyReader.getProperty("customAttribute4");
				String RFINUMBER = PropertyReader.getProperty("RFINUMBER_009");
				String Project_Name = "Project_" + Generate_Random_Number.generateRandomValue();
				// ==============Login with valid
				// UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				// =======Guest User Level Validation=======================
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName1, pWord1);
				folderPage = projectDashboardPage.CreateProjectwithMaxCustomAtributeAndValidation(Project_Name,
						CustomAttribute1, CustomAttribute2, CustomAttribute3, CustomAttribute4, CustomAttribute5);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}
		
		//======================================uncomplete testcases=================================

		
		
		/*
		 * TC_075 Re-assign the RFI from secondary assignee side. 1) The creator X
		 * creates a RFI and assign to user Y. 2) Now user Y re-assigns the RFI to
		 * user Z. 3) Again user Z re-assigns the RFI to user Z'
		 * 
		 */

		@Test(priority = 17, enabled = true, description = "The creator X creates a RFI and assign to user Y,Now user Y re-assigns the RFI to user Z,Again user Z re-assigns the RFI to user Z")
		public void ReassignTheRFIcase1() throws Throwable {
			try {

				Log.testCaseInfo("TC_075 (RFI Testcases):Login as the guest user and sync that particular project and move into the ALL RFI list");
				String uName1 = PropertyReader.getProperty("Username1");
				String pWord1 = PropertyReader.getProperty("Password1");
				String Project_Name = "Specific_Testdata_Static";
				// ==============Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				// =======Guest User Level Validation=======================
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName1, pWord1);// Calling
																									// Logi
																									// MethodS
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				// ================File Upload without Index=====================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();
				projectAndFolder_Level_Search.RFICREATION_New();
				viewerScreenPage = new ViewerScreenPage(driver).get();
				// viewerScreenPage.ReassignRFI_new();

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}

		/*
		 * TC_076 Re-assigning a RFI to a forwarded user 1) The creator X creates a
		 * RFI and assign to user Y. 2) The user X forwards the RFI to user Z1, Z2,
		 * Z3 with comments. 3) Now user Y re-assign the to user Z1.
		 */

		@Test(priority = 18, enabled =true, description = "The creator X creates a RFI and assign to user Y,Now user Y re-assigns the RFI to user Z,Again user Z re-assigns the RFI to user Z")
		public void ReassignTheRFIToforwadeduser() throws Throwable {
			try {

				Log.testCaseInfo("TC_076 (RFI Testcases):Re-assigning a RFI to a forwarded user");
				String uName1 = PropertyReader.getProperty("Username1");
				String pWord1 = PropertyReader.getProperty("Password1");
				String Project_Name = "Specific_Testdata_Static";
				// ==============Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				// =======Guest User Level Validation=======================
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName1, pWord1);
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				// ================File Upload without Index=====================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();
				//projectAndFolder_Level_Search.RFICREATION_New();
				String Email1 = PropertyReader.getProperty("user1");
				String Email2 = PropertyReader.getProperty("user2");
				String Email3 = PropertyReader.getProperty("user3");
				Log.assertThat(projectAndFolder_Level_Search.RFIForwadedUser_Validation(Email1,Email2,Email3),"RFI reassign user Validation working Successfully","RFI Reaasign Validation not working Successfully", driver);

				
			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}
		
		
		/**
		 * TC_026(Viewer Page):RFI creation with Internal attachments, external
		 * attachments adding attributes employee in ‘To’ section CC is shared user
		 * Recipient opening the same posting answer and attaching a document then
		 * sending to owner answered RFI closing by the creator
		 * 
		 */

		@Test(priority = 19, enabled = true , description = "Recipient opening the same posting answer and attaching a document then sending to owner answered RFI closing by the creator.")
		public void Verify_Recipientopening_posting_attaching_Documentclosingbythecreator() throws Throwable {

			try {
				Log.testCaseInfo("TC_026 (RFI ToolsBaar validation):  Recipient opening the same posting answer and attaching a document then sending to owner answered RFI closing");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String uName1 = PropertyReader.getProperty("Username2");
				String pWord1 = PropertyReader.getProperty("Password2");
				String parentHandle = driver.getWindowHandle();
				//=====================Login with valid UserID/PassWord============
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);			
				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				//select folder			
				String Foldername = "RFI_Folder";
				folderPage.Select_Folder(Foldername);
				// =======MarkUP Validation========================
				viewerScreenPage = new ViewerScreenPage(driver).get();			
				viewerScreenPage.Image1();				
				viewerScreenPage.RFI_TestDataCreation_ownerLevel();					
				//viewerScreenPage.RFI_OwnerLevel_validation();			
				Log.assertThat(viewerScreenPage.Logout(), "Logout working Successfully", "Logout not working Successfully",driver);

				// ====Employee level===================================
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName1, pWord1);
				projectDashboardPage.ValidateSelectProject(Project_Name);
				folderPage.Select_Folder(Foldername);
				viewerScreenPage.Image1();				
				viewerScreenPage.RFI_EMPLOYEEVALIDATION();									
				Log.assertThat(viewerScreenPage.Logout(), "Logout working Successfully", "Logout not working Successfully",driver);

				// ====== Owner level file Close=========================

				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				projectDashboardPage.ValidateSelectProject(Project_Name);
				folderPage.Select_Folder(Foldername);	
				viewerScreenPage.Image1();
				viewerScreenPage.RFI_Closed();				
				Log.assertThat(viewerScreenPage.Logout(), "Logout working Successfully", "Logout not working Successfully",driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}
		
		
		
		/**
		 * TC_028(Viewer Page):RFI creation with Internal attachments, external
		 * attachments adding attributes employee in ‘To’ section CC is shared user
		 * Recipient is uploading the new revision of the document in
		 */
		@Test(priority = 20, enabled = true, description = " Recipient is uploading the new revision of the document in ")
		public void Verify_RFI_Recipientis_uploading_new_revision() throws Throwable {

			try {
				Log.testCaseInfo("TC_028 (RFI ToolsBaar validation):  Recipient is uploading the new revision of the document in ");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String uName1 = PropertyReader.getProperty("Username2");
				String pWord1 = PropertyReader.getProperty("Password2");
				// ==============================Login with valid UserID/PassWord=====================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				
				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				//select folder			
				String Foldername = "RFI_Folder";
				folderPage.Select_Folder(Foldername);
				viewerScreenPage = new ViewerScreenPage(driver).get();
				// =======Owner Level Punch Validation========================
				
				viewerScreenPage.Image1();
				viewerScreenPage.RFI_TestDataCreation_ownerLevel();
				//viewerScreenPage.RFI_OwnerLevel_validation();			
				//viewerScreenPage.swichToMainWindow();				

				// ============== Revision Image Validation===========================

				
				viewerScreenPage.Image2();
				viewerScreenPage.RFI_TestDataCreation_ownerLevel1();
				//viewerScreenPage.RFI_OwnerLevel_validation();
				//viewerScreenPage.swichToMainWindow();			
				viewerScreenPage.Logout();
				// ====Employee level- first Image Validation===================================
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName1, pWord1);
				projectDashboardPage.ValidateSelectProject(Project_Name);
				folderPage.Select_Folder(Foldername);			
				viewerScreenPage.Image1();
				viewerScreenPage.RFI_EMPLOYEEVALIDATION1();
				//viewerScreenPage.swichToMainWindow();
				
				// ====Employee level- Second Image Validation===================================
				
				viewerScreenPage.Image2();
				viewerScreenPage.RFI_EMPLOYEEVALIDATION2();
				//viewerScreenPage.swichToMainWindow();		
				viewerScreenPage.Logout();
				
			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}

		
		/**
		 * TC_030(Viewer Page):RFI creation with Internal attachments, external
		 * attachments adding attributes employee in ‘To’ section CC is shared user
		 * Recipient opening reassigning the RFI to the other recipient.
		 **/

		@Test(priority =21, enabled = true, description = "Recipient opening reassigning the RFI to the other recipient.")
		public void Verify_Recipientopening_reassigningTo_Otherrecipient() throws Throwable {

			try {
				Log.testCaseInfo("TC_030 -(RFI ToolsBaar validation): Recipient opening reassigning the RFI to the other recipient");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String uName1 = PropertyReader.getProperty("Username2");
				String pWord1 = PropertyReader.getProperty("Password2");
				//===========Login with valid UserID/PassWord======
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				//select folder			
				String Foldername = "RFI_Folder";
				folderPage.Select_Folder(Foldername);			
				// ============RFI Creation ON first File===========================
				viewerScreenPage = new ViewerScreenPage(driver).get();			
				viewerScreenPage.Image1();			
				viewerScreenPage.RFI_TestDataCreation_ownerLevel();			
				//viewerScreenPage.RFI_OwnerLevel_validation();
				//viewerScreenPage.swichToMainWindow();
				Log.assertThat(viewerScreenPage.Logout(), "Logout working Successfully", "Logout not working Successfully",driver);
				
				// ====Employee level===================================
				viewerScreenPage = new ViewerScreenPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName1, pWord1);
				projectDashboardPage.ValidateSelectProject(Project_Name);
				folderPage.Select_Folder(Foldername);
				viewerScreenPage.Image1();			
				viewerScreenPage.RFI_EMPLOYEEVALIDATION();
				Log.assertThat(viewerScreenPage.Logout(), "Logout working Successfully", "Logout not working Successfully",driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}


		/**
		 * TC_060(Viewer Page- document level):Navigating to the document by
		 * clicking on the document thumbnail of the RFI and try to delete mark up
		 * for multi-page documents..
		 */

		@Test(priority = 22, enabled = true, description = "Navigating to the document by clicking on the document thumbnail of the RFI and try to delete mark up for multi-page documents.")
		public void Verify_NavigatetoDocumentTryToDeleteMarkUPFor_MultiplePage() throws Throwable {

			try {
				Log.testCaseInfo("TC_060 (RFI ToolsBaar validation): Navigating to the document by clicking on the document thumbnail of the RFI and try to delete mark up for multi-page documents.");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");			
				// ==============================Login with valid UserID/PassWord=====================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				//select folder			
				String Foldername = "RFI_Folder";
				folderPage.Select_Folder(Foldername);			
				// ============RFI Creation ON first File===========================
				viewerScreenPage = new ViewerScreenPage(driver).get();			
				viewerScreenPage.Image1();
				viewerScreenPage.RFI_TestDataCreation_ownerLevel();
                //viewerScreenPage.RFI_OwnerLevel_validation();
				//viewerScreenPage.swichToMainWindow();		
				Log.assertThat(viewerScreenPage.Logout(), "Logout working Successfully", "Logout not working Successfully",
						driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}
		
		/**
		 * TC_055(Viewer Page):User X create RFI and assign to user Y with both
		 * Potential cost impact and potential schedule impact Uncheck
		 **/

		@Test(priority = 23, enabled = true , description = "Both Potential cost impact and potential schedule impact checked")
		public void Verify_BothPotentialcostimpactAndpotentialscheduleimpact_Uncheck() throws Throwable {

			try {
				Log.testCaseInfo("TC_055 -(RFI ToolsBaar validation): Both Potential cost impact and potential schedule impact Unchecked");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String uName1 = PropertyReader.getProperty("Username2");
				String pWord1 = PropertyReader.getProperty("Password2");
				// ==============================Login with valid UserID/PassWord=====================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				
				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				//select folder			
				String Foldername = "RFI_Folder";
				folderPage.Select_Folder(Foldername);					
				// =======Owner Level Punch Validation========================
				viewerScreenPage = new ViewerScreenPage(driver).get();			
				viewerScreenPage.Image1();
				viewerScreenPage.RFI_TestDataCreation_ownerLevel();
				//viewerScreenPage.RFI_OwnerLevel_validation();
				//viewerScreenPage.swichToMainWindow();			
				viewerScreenPage = new ViewerScreenPage(driver).get();
				viewerScreenPage.ProjectManagement();
				String usernamedir = System.getProperty("user.name");
				String Sys_Download_Path = "C:\\" + "Users\\" + usernamedir + "\\Downloads";
				folderPage.DownloadFiledownload(Sys_Download_Path);
				Log.assertThat(viewerScreenPage.Logout(), "Logout working Successfully", "Logout not working Successfully",
						driver);

				// ====Employee level===================================

				viewerScreenPage = new ViewerScreenPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName1, pWord1);
				projectDashboardPage.ValidateSelectProject(Project_Name);
				viewerScreenPage.ProjectManagement();
				viewerScreenPage.RFITAB();
				Log.message("Download Path Display " + Sys_Download_Path);
				folderPage.DownloadFiledownload(Sys_Download_Path);
				Log.assertThat(viewerScreenPage.Logout(), "Logout working Successfully", "Logout not working Successfully",driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}
		
		
		
		/**
		 * TC_056(Viewer Page):User X create RFI and assign to user Y with both
		 * Potential cost impact and potential schedule impact check
		 **/

		@Test(priority = 24, enabled =true, description = "Both Potential cost impact and potential schedule impact checked")
		public void Verify_BothPotentialcostimpactAndpotentialscheduleimpact() throws Throwable {

			try {
				Log.testCaseInfo("TC_056 -(RFI ToolsBaar validation): Both Potential cost impact and potential schedule impact checked");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String uName1 = PropertyReader.getProperty("Username2");
				String pWord1 = PropertyReader.getProperty("Password2");
				// ==============================Login with valid UserID/PassWord=====================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				
				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				//select folder			
				String Foldername = "RFI_Folder";
				folderPage.Select_Folder(Foldername);
				
				viewerScreenPage = new ViewerScreenPage(driver).get();
				viewerScreenPage.Image1();
				viewerScreenPage.RFITestDataCreation_potentialChecked();
				//viewerScreenPage.RFI_OwnerLevel_validation();
				//viewerScreenPage.swichToMainWindow();			
				
				viewerScreenPage.ProjectManagement();
				String usernamedir = System.getProperty("user.name");
				String Sys_Download_Path = "C:\\" + "Users\\" + usernamedir + "\\Downloads";
				folderPage.DownloadFiledownload(Sys_Download_Path);
				Log.assertThat(viewerScreenPage.Logout(), "Logout working Successfully", "Logout not working Successfully",driver);
				// ====Employee level===================================
				viewerScreenPage = new ViewerScreenPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName1, pWord1);
				projectDashboardPage.ValidateSelectProject(Project_Name);
				viewerScreenPage.ProjectManagement();
				viewerScreenPage.RFITAB();
				Log.message("Download Path Display " + Sys_Download_Path);
				folderPage.DownloadFiledownload(Sys_Download_Path);
				Log.assertThat(viewerScreenPage.Logout(), "Logout working Successfully", "Logout not working Successfully",driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}
		
		
		/**
		 * TC_057(Viewer Page- document level):Navigating to the document by
		 * clicking on the document thumbnail of the RFI and try to save mark up for
		 * single page documents..
		 */

		@Test(priority = 25, enabled = true, description = "Navigating to the document by clicking on the document thumbnail of the RFI and try to save mark up for single page documents.")
		public void Verify_NavigatetoDocumenttrytoSaveMarkup_SinglePage() throws Throwable {

			try {
				Log.testCaseInfo("TC_057 (RFI -Viewer level): Navigating to the document by clicking on the document thumbnail of the RFI and try to save mark up for single page documents.");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String uName1 = PropertyReader.getProperty("Username2");
				String pWord1 = PropertyReader.getProperty("Password2");
				// ==============================Login with valid UserID/PassWord=====================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// Create project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				//select folder			
				String Foldername = "RFI_Folder";
				folderPage.Select_Folder(Foldername);	
				viewerScreenPage = new ViewerScreenPage(driver).get();
				// =======RFI Creation and Validation==============================
				
				viewerScreenPage.Image1();
				viewerScreenPage.RFITestDataCreation_potentialChecked();			
				//viewerScreenPage.RFI_OwnerLevel_validation();
				//viewerScreenPage.swichToMainWindow();
				
				// =========Project level ========================================
				viewerScreenPage.ProjectManagement();
				viewerScreenPage.UPDATED_RFIROW();				
				viewerScreenPage.Close_POPUP();
				Log.assertThat(viewerScreenPage.TextValidation(), "Text Validation working Successfully","Text validation Not  working sucessfully", driver);
				viewerScreenPage.swichToMainWindow();
				Log.assertThat(viewerScreenPage.Logout(), "Logout working Successfully", "Logout not working Successfully",driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}
		
		
		/**
		 * TC_058(Viewer Page- document level):Navigating to the document by
		 * clicking on the document thumbnail of the RFI and try to delete mark up
		 * for single page documents..
		 */

		@Test(priority = 26, enabled = true, description = "Navigating to the document by clicking on the document thumbnail of the RFI and try to delete mark up for single page documents.")
		public void Verify_NavigatetToDocumnetAndTryTodeleteMarkUp() throws Throwable {

			try {
				Log.testCaseInfo(
						"TC_058 (RFI ToolsBaar validation): Navigating to the document by clicking on the document thumbnail of the RFI and try to delete mark up for single page documents.");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String uName1 = PropertyReader.getProperty("Username2");
				String pWord1 = PropertyReader.getProperty("Password2");
				// ==============================Login with valid UserID/PassWord=====================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				//select folder			
				String Foldername = "RFI_Folder";
				folderPage.Select_Folder(Foldername);	
				viewerScreenPage = new ViewerScreenPage(driver).get();
				// =======RFI Creation and Validation========================

				viewerScreenPage.Image1();
				viewerScreenPage.RFITestDataCreation_potentialChecked();
				//viewerScreenPage.RFI_OwnerLevel_validation();
						
				//viewerScreenPage.swichToMainWindow();
				
				// =========Project level ==========================
				viewerScreenPage.ProjectManagement();
				viewerScreenPage.UPDATED_RFIROW();
				
				viewerScreenPage.Close_POPUP();
				Log.assertThat(viewerScreenPage.TextValidation(), "Text Validation working Successfully",
						"text validation Not  working sucessfully", driver);
				
				viewerScreenPage.swichToMainWindow();
				//viewerScreenPage.Delete_MarkUP();
				
				Log.assertThat(viewerScreenPage.Logout(), "Logout working Successfully", "Logout not working Successfully",
						driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}


		
		/**
		 * TC_059(Viewer Page- document level):Navigating to the document by
		 * clicking on the document thumbnail of the RFI and try to save mark up for
		 * multi-page documents..
		 */

		@Test(priority = 27, enabled = true, description = "Navigating to the document by clicking on the document thumbnail of the RFI and try to save mark up for multi-page documents.")
		public void Verify_NavigatetoDocumentTryToSaveMarkUPforMultiplePage() throws Throwable {

			try {
				Log.testCaseInfo("TC_059 (RFI -viewer Level): Navigating to the document by clicking on the document thumbnail of the RFI and try to save mark up for multi-page documents.");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String uName1 = PropertyReader.getProperty("Username2");
				String pWord1 = PropertyReader.getProperty("Password2");
				// ==============================Login with valid UserID/PassWord=====================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);

				// =========Create Project Randomly===================================
				String Project_Name = "ARC_Project_" + Generate_Random_Number.MyDate();
				folderPage = projectDashboardPage.CreateProject(Project_Name);
				
				String Foldername = "Folder_" + Generate_Random_Number.generateRandomValue();
				folderPage.New_Folder_Create(Foldername);
				folderPage.Select_Folder(Foldername);

				// ================File Upload without Index=====================
				String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_MultipleFile");
				String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
				int FileCount = Integer.parseInt(CountOfFilesInFolder);
				folderPage.Upload_WithoutIndex(FolderPath, FileCount);

				// =======RFI Creation and Validation========================
				viewerScreenPage = new ViewerScreenPage(driver).get();
				viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);
				
				viewerScreenPage.Image1();
				viewerScreenPage.RFITestDataCreation_potentialChecked();
				//viewerScreenPage.RFI_OwnerLevel_validation();
				//driver.close();
				
				// =========Project level ==========================
				viewerScreenPage = new ViewerScreenPage(driver).get();			
				viewerScreenPage.ProjectManagement();
				viewerScreenPage.UPDATED_RFIROW();
				viewerScreenPage.MarkUPValidation();
				viewerScreenPage.swichToMainWindow();			
				Log.assertThat(viewerScreenPage.Logout(), "Logout working Successfully", "Logout not working Successfully",
						driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}
		
		
		/**
		 * TC_042(Viewer Page):Upload one file having two revision from web.Create
		 * RFI in the 1st revision file .RFI should be carry forward to second
		 * revision.
		 * 
		 **/

		@Test(priority = 28, enabled = true, description = "RFI should be carry forward to second revision.")
		public void Verify_CreateRFIinthe1strevisionfile_RFI_carryforwardtosecondrevision() throws Throwable {
			try {
				Log.testCaseInfo("TC_042 -(RFI ToolsBaar validation): RFI should be carry forward to second revision.");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String uName1 = PropertyReader.getProperty("Username2");
				String pWord1 = PropertyReader.getProperty("Password2");
				// ==============================Login with valid UserID/PassWord=====================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				
				//=========  Static Data======================
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				//select folder			
				String Foldername = "RFI_Folder";
				folderPage.Select_Folder(Foldername);	
				viewerScreenPage = new ViewerScreenPage(driver).get();
				// =======Owner Level Punch Validation========================
				
				viewerScreenPage.Image1();
				viewerScreenPage.RFI_TestDataCreation_ownerLevel();
				//viewerScreenPage.RFI_OwnerLevel_validation();
				//driver.close();
			
				// ============== Revision Image Validation====================
				
				viewerScreenPage.Image2();
				viewerScreenPage.RFI_RevisionFile();
				//viewerScreenPage.RFI_OwnerLevel_validation();
				// viewerScreenPage.MainStatus();
				//driver.close();			
				viewerScreenPage.Logout();

				// ===Employee level Login And validation

				// projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName1,pWord1);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}
		
		
		
		/**
		 * TC_044(Viewer Page):Upload one file having two revision from web.Create
		 * RFI in the 2nd revision file .RFI should be not carry forward to 1st
		 * revision..
		 * 
		 **/

		@Test(priority = 29, enabled = true, description = "RFI should be carry forward to second revision.")
		public void Verify__RFI_carryforwardtoFirstrevision() throws Throwable {

			try {
				Log.testCaseInfo("TC_044 -(RFI ToolsBaar validation): RFI should be not carry forward to 1st revision.");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String uName1 = PropertyReader.getProperty("Username2");
				String pWord1 = PropertyReader.getProperty("Password2");
				// ==============================Login with valid UserID/PassWord=====================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				
				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				//select folder			
				String Foldername = "RFI_Folder";
				folderPage.Select_Folder(Foldername);
				// =======RFI Revision2 File Validation========================
				viewerScreenPage = new ViewerScreenPage(driver).get();
				
				viewerScreenPage.Image2();
				viewerScreenPage.RFI_TestDataCreation_ownerLevel1();
				
				// =======RFI Revision1 File Validation========================
				viewerScreenPage = new ViewerScreenPage(driver).get();
				
				viewerScreenPage.Image1();
				viewerScreenPage.SecondRFI_Validation();
				//viewerScreenPage.swichToMainWindow();			
				viewerScreenPage.Logout();

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}


		// =========== RFI related Testcases=======================================
		/*
		 * Testcase:045. Delete 1st revision file from web where RFI is exist.Take
		 * the update change set in the device for deleted file.Verify the existance
		 * of RFI in the 2nd revision file. 1) RFI should be carry forward to second
		 * revision. 2) Verify the position of RFI. 3) Verify with all kind of RFI
		 * status(Open, close, re-open, responded, reject, void, closed)
		 */

		@Test(priority = 30, enabled = true, description = "Delete 1st revision file from web where RFI is exist.Take the update change set in the device for deleted file.Verify the existance of RFI in the 2nd revision file.")
		public void DeleteFirstRFIfromWebCheking() throws Throwable {

			try {
				Log.testCaseInfo("TC_045 (RFI Delete Related Testcases):Delete 1st revision file from web where RFI is exist.Take the update change set in the device for deleted file.Verify the existance of RFI in the 2nd revision file.");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");

				// ==============Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
	            
				
				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				//select folder			
				String Foldername = "RFI_Folder";
				folderPage.Select_Folder(Foldername);			
				// ============RFI Creation ON first File===========================
				viewerScreenPage = new ViewerScreenPage(driver).get();			
				viewerScreenPage.Image1();
				viewerScreenPage.RFI_TestDataCreation_ownerLevel();
				
				/*
				 * String parentHandle1 = driver.getWindowHandle();
				 * viewerScreenPage.Image1();
				 * viewerScreenPage.DeleteFirstRevisionfile(parentHandle1);
				 * driver.switchTo().window(parentHandle1); //===========RFI
				 * Creation Of Second Revision File================== String
				 * parentHandle2 = driver.getWindowHandle();
				 * viewerScreenPage.Image1();
				 * viewerScreenPage.RFI_RevisionFile(parentHandle2);
				 * viewerScreenPage.RFI_OwnerLevel_validation();
				 * viewerScreenPage.StatusValidation(); driver.close();
				 * driver.switchTo().window(parentHandle2);
				 */
				viewerScreenPage.Logout();

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}
		
		
		/*
		 * Test case :TC_061.Navigate from RFI listings and try to create RFI
		 * annotation. Create one in document level in online mode with To, Cc,
		 * Subject, disciple, sheet number, custom attributes, specification,
		 * potential cost impact(checked), potential schedule impact(checked),
		 * Question and attachment(external and internal) 1) Navigate to the
		 * document and validate the document. 2) Validate the postion and content
		 * of RFI. 3) Close the pop over and tap on view all and validate the
		 * position of mark ups. 4) Create RFI, Punch and photo annotation on the
		 * document an validate those in web.
		 */

		@Test(priority = 31, enabled = true, description = "Navigate from RFI listings and try to create RFI")
		public void NavigatefromRFIListingtocreateRFI() throws Throwable {
			try {
				Log.testCaseInfo("TC_061 (RFI Related Testcases):Navigate from RFI listings and try to create RFI");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				// ==============Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				
		
	           // Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				//select folder			
				String Foldername = "RFI_Folder";
				folderPage.Select_Folder(Foldername);
				
				viewerScreenPage = new ViewerScreenPage(driver).get();
				
				viewerScreenPage.Image1();
				viewerScreenPage.RFITestDataCreation_potentialChecked();			
				viewerScreenPage.RFI_OwnerLevel_validation();
				viewerScreenPage.swichToMainWindow();			
				viewerScreenPage.Image1();
				viewerScreenPage.RFI_RevisionFile();
				viewerScreenPage.RFI_OwnerLevel_validation();
				viewerScreenPage.swichToMainWindow();			
				viewerScreenPage.Logout();

			}

			catch (Exception e)

			{
				e.getCause();
				Log.exception(e, driver);
			} finally

			{
				Log.endTestCase();
				driver.close();
			}

		}
		
		
		
		/*
		 * Test case :TC_046 .Now Delete 2nd revision file from web where RFI is
		 * exist.Take the update change set in the device for deleted file.File
		 * level RFI should converted to project level RFI. 1) File level RFI should
		 * converted to project level RFI. 2) Validate the user should be able to
		 * change the status of the RFI. 3) The user should be able to comment in
		 * the RFI.
		 */

		@Test(priority = 32, enabled = true, description = "File level RFI should converted to project level RFI.")
		public void DeleteSecondRFI() throws Throwable {
			try {
				Log.testCaseInfo("TC_046 (RFI Delete Related Testcases):File level RFI should converted to project level RFI.");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				// ==============Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// =========Create Project Randomly===================================
				 // Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				//select folder			
				String Foldername = "RFI_Folder";
				folderPage.Select_Folder(Foldername);
				viewerScreenPage.Logout();
				// ============RFI Creation ON first File===========================
				/*
				 * String parentHandle = driver.getWindowHandle();
				 * viewerScreenPage.Image1();
				 * Log.assertThat(viewerScreenPage.RFI_TestDataCreation_ownerLevel(
				 * parentHandle), "Test Data Creation working Successfully"
				 * ,"Test Data Creation not working Successfully", driver);
				 * Log.assertThat(viewerScreenPage.RFI_OwnerLevel_validation(),
				 * "Owner Level validation working Successfully"
				 * ,"Owner Level validation not working Successfully", driver);
				 * driver.close(); viewerScreenPage.Image1();
				 * //viewerScreenPage.DeleteRFI();
				 * driver.switchTo().window(parentHandle);
				 * 
				 * //===========RFI Creation Of Second Revision
				 * File==================
				 * 
				 * String parentHandle1 = driver.getWindowHandle();
				 * viewerScreenPage.Image2();
				 * viewerScreenPage.RFI_RevisionFile(parentHandle1);
				 * viewerScreenPage.RFI_OwnerLevel_validation();
				 * viewerScreenPage.MainStatus(); SkySiteUtils.waitTill(5000);
				 * driver.close(); SkySiteUtils.waitTill(5000);
				 * driver.switchTo().window(parentHandle1);
				 * viewerScreenPage.Logout();
				 */
				// Log.assertThat( viewerScreenPage.Logout(), "Logout working
				// Successfully","Logout not working Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}

		
		/*
		 * TC_074 Operation in Web: 1) Share any project to any guest user. 2)
		 * Accept the project from the guest user account. Operation in device: 1)
		 * Login as the guest user and sync that particular project and move into
		 * the ALL RFI list.
		 * 
		 * 
		 */

		@Test(priority = 34, enabled = true, description = "Login as the guest user and sync that particular project and move into the ALL RFI list")
		public void GuestUserSyncWithParticularproject() throws Throwable {
			try {
				Log.testCaseInfo("TC_074 (RFI Testcases):Login as the guest user and sync that particular project and move into the ALL RFI list");
				String uName2 = PropertyReader.getProperty("GuestUserName");
				String pWord2 = PropertyReader.getProperty("GuestUserPassword");
				String Project_Name = "Specific_Testdata_Static";
				// ==============Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				// =======Guest User Level Validation=======================
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName2, pWord2);
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				// ================File Upload without Index=====================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();			
				Log.assertThat(projectAndFolder_Level_Search.RFI_AscendingorderValidation(),"Ascending order Validation working Successfully","Ascending order Validation not working Successfully", driver);

				projectAndFolder_Level_Search.Logout();

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
			} finally {
				Log.endTestCase();
				driver.close();
			}

		}

		
		
		


	
	
	

	
	
	
	

	

	
	
	
	
	
	

	
	
	

}
