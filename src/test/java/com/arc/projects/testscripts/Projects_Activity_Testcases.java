package com.arc.projects.testscripts;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.PublishLogPage;
import com.arc.projects.pages.PunchPage;
import com.arc.projects.pages.RFIPage;
import com.arc.projects.pages.SubmitalPage;
import com.arc.projects.pages.UploadPhotosPage;
import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;
import com.arc.projects.pages.ProjectGalleryPage;
import com.arc.projects.pages.PhotoAttributesPage;
import com.arc.projects.pages.ProjectActivitiesPage;

public class Projects_Activity_Testcases {
	
public static WebDriver driver;
	
	ProjectsLoginPage projectsLoginPage;
	ProjectDashboardPage projectDashboardPage;  
	FolderPage folderPage;
	ProjectGalleryPage projectGalleryPage;
	UploadPhotosPage uploadPhotosPage;
	PhotoAttributesPage photoAttributesPage;
	ProjectActivitiesPage projectActivitiesPage;
	PublishLogPage publishLogPage;
	PunchPage punchPage;
	RFIPage rfiPage;
	SubmitalPage submittalPage;
	
	
	
	@Parameters("browser")
	@BeforeMethod
    public WebDriver beforeTest(String browser) {
        
    	if(browser.equalsIgnoreCase("firefox")) 
        {
               File dest = new File("./drivers/win/geckodriver.exe");
               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
               driver = new FirefoxDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
        
        else if (browser.equalsIgnoreCase("chrome")) 
        { 
        File dest = new File("./drivers/win/chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());  
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        options.setExperimentalOption("prefs", prefs);
        driver = new ChromeDriver( options );
        driver.get(PropertyReader.getProperty("SkysiteProdURL"));
 
        } 
        
        else if (browser.equalsIgnoreCase("safari"))
        {
               System.setProperty("webdriver.safari.noinstall", "false"); //To stop uninstall each time
               driver = new SafariDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
   
    	return driver;
 }
	
	
	
	
	/** TC_001 ("Verify User is able to see the View detail button in Project activity for any Photo upload in Gallery")*/
	
	@Test(priority = 0, enabled = true, description = "Verify User is able to see the View detail button in Project activity for any Photo upload in Gallery")
	public void Verify_ViewDetailsButtonForPhotoUpload() throws Throwable
    {
		
		try
    	{
			
			Log.testCaseInfo("Verify User is able to see the View detail button in Project activity for any Photo upload in Gallery");
			
			String username = PropertyReader.getProperty("USERNAME1");
		    String password = PropertyReader.getProperty("PASSWORD1");
		    
		  //==============================Login with valid UserID/PassWord=====================//
			projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			
			folderPage=projectDashboardPage.enteringanyProject("Test_Project_01");
			projectGalleryPage=folderPage.enterGallery();
			uploadPhotosPage=projectGalleryPage.uploadPhotosClick();
            
            File fis=new  File(PropertyReader.getProperty("FolderPathFile"));            
            String filepath=fis.getAbsolutePath().toString();            
            File fis1=new  File(PropertyReader.getProperty("tempfilepath"));            
            String tempfilepath=fis1.getAbsolutePath().toString();            
            photoAttributesPage=uploadPhotosPage.UploadPhoto(filepath,tempfilepath);  
            
            projectGalleryPage=photoAttributesPage.saveAttributes();
            projectActivitiesPage=projectGalleryPage.documentActivity();
            Log.assertThat(projectActivitiesPage.viewDetailsPhoto(), "View details button is displayed for the Photo Uploaded", "View details button is not displayed for the Photo Uploaded", driver);
           
            folderPage=projectActivitiesPage.enterProjectFromBreadcrumb();
            projectGalleryPage=folderPage.enterGallery();
            projectGalleryPage.deletePhoto();
            
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
		
	
	
	
	/** TC_002 ("Verify User is able to see the View detail button in Project activity for any File upload in a Folder")*/
	
	@Test(priority = 1, enabled = true, description = "Verify User is able to see the View detail button in Project activity for any File upload in a Folder")
	public void Verify_ViewDetailsButtonForFileUpload() throws Throwable
	{
	
		try
		{
			
			Log.testCaseInfo("Verify User is able to see the View detail button in Project activity for any File upload in a Folder");
		
			String username = PropertyReader.getProperty("USERNAME1");
		    String password = PropertyReader.getProperty("PASSWORD1");
		    
		    //==============================Login with valid UserID/PassWord=====================//
			projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			
			folderPage=projectDashboardPage.enteringanyProject("Test_Project_01");
			folderPage.enterFolder();
			         
            File fis=new  File(PropertyReader.getProperty("FolderPathFile2"));            
            String filepath=fis.getAbsolutePath().toString();
            File fis1=new  File(PropertyReader.getProperty("tempfilepath"));            
            String tempfilepath=fis1.getAbsolutePath().toString();            
            publishLogPage=folderPage.fileUpload(filepath,tempfilepath); 
            folderPage=publishLogPage.publishLogProcessCompleted();
            
            projectActivitiesPage=folderPage.documentActivity();
            Log.assertThat(projectActivitiesPage.viewDetailsFile(), "View details button is displayed for the File Uploaded", "View details button is not displayed for the File Uploaded", driver);
           
            folderPage=projectActivitiesPage.enterProjectFromBreadcrumb();
            folderPage.enterFolder();
            folderPage.deleteFile();
			
		}	
		catch(Exception e)
	    {
			CommonMethod.analyzeLog(driver);
			e.getCause();
    		Log.exception(e, driver);
        }	
			
		finally
		{
    		Log.endTestCase();
    		driver.close();
    	}	
		
	}	

	
	
	/** TC_003 ("Verify when user opens any Project the information should be listed in the activity page")*/
	
	@Test(priority = 2, enabled = true, description = "Verify when user opens any Project the information should be listed in the activity page")
	public void Verify_ViewProject() throws Throwable
	{
	
		try
		{
			
			Log.testCaseInfo("Verify when user opens any Project the information should be listed in the activity page");
		
			String username = PropertyReader.getProperty("USERNAME1");
		    String password = PropertyReader.getProperty("PASSWORD1");
		    
		    //==============================Login with valid UserID/PassWord=====================//
			projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password);
		
			folderPage=projectDashboardPage.enteringanyProject("Test_Project_01");
			projectActivitiesPage=folderPage.documentActivity();
					
			Log.assertThat(projectActivitiesPage.viewProject(), "Viewed Project is displayed", "Viewed Project is not displayed", driver);
	           
		}
		catch(Exception e)
	    {
			CommonMethod.analyzeLog(driver);
			e.getCause();
    		Log.exception(e, driver);
        }	
			
		finally
		{
    		Log.endTestCase();
    		driver.close();
    	}	
	}	

	
	/** TC_004 ("Verify when user opens any File the information should be listed in the activity page")*/
	
	@Test(priority = 3, enabled = true, description = "Verify when user opens any File the information should be listed in the activity page")
	public void Verify_OpenFileActivity() throws Throwable
	{
		
		try
		{
			
			Log.testCaseInfo("Verify when user opens any File the information should be listed in the activity page");
		
			String username = PropertyReader.getProperty("USERNAME1");
		    String password = PropertyReader.getProperty("PASSWORD1");

		  //==============================Login with valid UserID/PassWord=====================//
			projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			
			folderPage=projectDashboardPage.enteringanyProject("Test_Project_01");
			folderPage.enterFolder();
			
			File fis=new  File(PropertyReader.getProperty("FolderPathFile2"));            
            String filepath=fis.getAbsolutePath().toString();
            File fis1=new  File(PropertyReader.getProperty("tempfilepath"));            
            String tempfilepath=fis1.getAbsolutePath().toString();            
            publishLogPage=folderPage.fileUpload(filepath,tempfilepath); 
            folderPage=publishLogPage.publishLogProcessCompleted();
            folderPage.openFile();
            
            projectActivitiesPage=folderPage.documentActivity();
            Log.assertThat(projectActivitiesPage.viewFileActivity(), "File viewed is tracked in Project Activity", "File viewed is not tracked in Project Activity", driver);

            folderPage=projectActivitiesPage.enterProjectFromBreadcrumb();
            folderPage.enterFolder();
            folderPage.deleteFile();
            
		}
		catch(Exception e)
	    {
			CommonMethod.analyzeLog(driver);
			e.getCause();
    		Log.exception(e, driver);
        }	
			
		finally
		{
    		Log.endTestCase();
    		driver.close();
    	}	
		
	}
	
	
	
	
	/** TC_005 ("Verify when user deletes a file the information should be listed in the activity page")*/
	
	@Test(priority = 4, enabled = true, description = "Verify when user deletes a file the information should be listed in the activity page")
	public void Verify_DeleteFileActivity() throws Throwable
	{
	
		try
		{
			
			Log.testCaseInfo("Verify when user deletes a file the information should be listed in the activity page");
		
			String username = PropertyReader.getProperty("USERNAME1");
		    String password = PropertyReader.getProperty("PASSWORD1");
		    
		    //==============================Login with valid UserID/PassWord=====================//
			projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			
			folderPage=projectDashboardPage.enteringanyProject("Test_Project_01");
			folderPage.enterFolder();
			
			File fis=new  File(PropertyReader.getProperty("FolderPathFile2"));            
            String filepath=fis.getAbsolutePath().toString();
            File fis1=new  File(PropertyReader.getProperty("tempfilepath"));            
            String tempfilepath=fis1.getAbsolutePath().toString();            
            publishLogPage=folderPage.fileUpload(filepath,tempfilepath); 
            folderPage=publishLogPage.publishLogProcessCompleted();
            folderPage.deleteFile();
            projectActivitiesPage=folderPage.documentActivity();              
            Log.assertThat(projectActivitiesPage.deleteFileActivity(), "Deleted file is tracked in Project Activity", "Deleted file is not tracked in Project Activity", driver);
        
		}
		catch(Exception e)
	    {
			
			CommonMethod.analyzeLog(driver);
			e.getCause();
    		Log.exception(e, driver);
        }	
			
		finally
		{
    		Log.endTestCase();
    		driver.close();
    	}	
		
	}
	
	
	/** TC_006 ("Verify when user create any Punch the information should be listed in the activity page")*/
	
	@Test(priority = 5, enabled = true, description = "Verify when user create any Punch the information should be listed in the activity page")
	public void Verify_CreatePunchActivity() throws Throwable
	{
	
		try
		{
			
			Log.testCaseInfo("Verify when user create any Punch the information should be listed in the activity page");
		
			String username = PropertyReader.getProperty("USERNAME1");
		    String password = PropertyReader.getProperty("PASSWORD1");
		    
		    //==============================Login with valid UserID/PassWord=====================//
			projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			
			folderPage=projectDashboardPage.enteringanyProject("Test_Project_01");
			
			punchPage=folderPage.enterPunchPage();
			projectActivitiesPage=punchPage.createPunch();			
			
			Log.assertThat(projectActivitiesPage.createPunchActivity(), "Punch created is tracked in Project Activity", "Punch created is not tracked in Project Activity", driver);
	        	
		}
		catch(Exception e)
	    {
			CommonMethod.analyzeLog(driver);
			e.getCause();
    		Log.exception(e, driver);
        }	
			
		finally
		{
    		Log.endTestCase();
    		driver.close();
    	}
	
	}
	

	
   /** TC_007 ("Verify when user create any RFI the information should be listed in the activity page")*/
	
	@Test(priority = 6, enabled = true, description = "Verify when user create any RFI the information should be listed in the activity page")
	public void Verify_CreateRFIActivity() throws Throwable
	{
	
		try
		{
			
			Log.testCaseInfo("Verify when user create any RFI the information should be listed in the activity page");
			
			String username = PropertyReader.getProperty("USERNAME1");
		    String password = PropertyReader.getProperty("PASSWORD1");
		    
		    //==============================Login with valid UserID/PassWord=====================//
			projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enteringanyProject("Test_Project_01");
			
			rfiPage=folderPage.enterRFIPage();			
			projectActivitiesPage=rfiPage.createRFI();
			Log.assertThat(projectActivitiesPage.createRFIActivity(), "RFI created is tracked in Project Activity", "RFI created is not tracked in Project Activity", driver);
			
		}
		
		catch(Exception e)
	    {
			CommonMethod.analyzeLog(driver);
			e.getCause();
    		Log.exception(e, driver);
        }	
			
		finally
		{
    		Log.endTestCase();
    		driver.close();
    	}
	
	}
	
	
	/** TC_008 ("Verify when user create any Submittal the information should be listed in the activity page")*/
	
	@Test(priority = 7, enabled = true, description = "Verify when user create any Submittal the information should be listed in the activity page")
	public void Verify_CreateSubmittalActivity() throws Throwable
	{
	
		try
		{
			
			Log.testCaseInfo("Verify when user create any Submittal the information should be listed in the activity page");
			
			String username = PropertyReader.getProperty("USERNAME1");
		    String password = PropertyReader.getProperty("PASSWORD1");
		    
		    //==============================Login with valid UserID/PassWord=====================//
			projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enteringanyProject("Test_Project_01");
			
			submittalPage=folderPage.enterSubmittalPage();
			String randonSubmittalNumber=submittalPage.RandomSubmittalNumber();
			projectActivitiesPage=submittalPage.createSubmittal(randonSubmittalNumber);
			
			Log.assertThat(projectActivitiesPage.createSubmittalActivity(randonSubmittalNumber), "Submittal created is tracked in Project Activity", "Submittal created is not tracked in Project Activity", driver);
		
		}
		
		catch(Exception e)
	    {
			CommonMethod.analyzeLog(driver);
			e.getCause();
    		Log.exception(e, driver);
        }	
			
		finally
		{
    		Log.endTestCase();
    		driver.close();
    	}
		
	}
	
}
	
