package com.arc.projects.testscripts;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectAndFolder_Level_Search;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.ViewerScreenPage;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)
public class Folder_level_Search_TestCases {
	 

		public static WebDriver driver;	    
		ProjectsLoginPage projectsLoginPage;
	    ProjectDashboardPage projectDashboardPage;   
	    ViewerScreenPage viewerScreenPage; 
	    FolderPage folderPage;
	    ProjectAndFolder_Level_Search projectAndFolder_Level_Search;
	    
	    @Parameters("browser")
	    @BeforeMethod
 public WebDriver beforeTest(String browser) {
	        
	        if(browser.equalsIgnoreCase("firefox")) 
	        {
	               File dest = new File("./drivers/win/geckodriver.exe");
	               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
	               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
	               driver = new FirefoxDriver();
	               driver.get(PropertyReader.getProperty("SkysiteProdURL"));}
	        
	        else if (browser.equalsIgnoreCase("chrome")) 
	        { 
	        File dest = new File("./drivers/win/chromedriver.exe");
	        System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());  
	        Map<String, Object> prefs = new HashMap<String, Object>();
	        prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
	        ChromeOptions options = new ChromeOptions();
	         options.addArguments("--start-maximized");
	        options.setExperimentalOption("prefs", prefs);
	        driver = new ChromeDriver( options );
	        driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	 
	        } 
	        
	        else if (browser.equalsIgnoreCase("safari"))
	        {
	               System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
	               driver = new SafariDriver();
	               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	        }
	         return driver;
	 }
	    
	    
 /*TC_038(Local Search an RFI with it's RFI number.) 
  * 1) Verify all the content of the RFI including status opening the RFI in the search result window.
  * 2) Try to download associated attachments and verify.
  * 3) Change the status of the RFI from the search result window and verify in cloud.
		   
  */
		    
		    
                 @Test(priority = 0, enabled = true , description = "Local Search an RFI with it's RFI number.")
                 public void VerifyRFI_LocalSearch_With_RFINumber() throws Throwable
                {
	       	      try
	        	{
	        		Log.testCaseInfo("TC_038(RFI Folder Level Search):Local Search an RFI with it's RFI number.");
	        		String uName = PropertyReader.getProperty("Username1");
	       		    String pWord = PropertyReader.getProperty("Password1");
	       		   
	       		    //==============Login with valid UserID/PassWord===========================
	         		projectsLoginPage = new ProjectsLoginPage(driver).get();  
	         		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);   		
	         		
	         		//=========Create Project Randomly========================================
	         		String Project_Name = "Prj_"+Generate_Random_Number.MyDate();
	         		String RFINumber = Generate_Random_Number.generateRandomValue1();
	         		Log.message("Subject verified Sucessfully :- "+RFINumber);
	         		folderPage=projectDashboardPage.createProjectwithSpecificRFINumber(Project_Name, RFINumber);
	         		         		
	         		 //=========Create Folder Randomly========================================
	         		String Foldername = "Folder_"+Generate_Random_Number.generateRandomValue();
	         		folderPage.New_Folder_Create(Foldername);      		
	         		folderPage.Select_Folder(Foldername);
	         		//================File Upload without Index===============================
	         		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
	         	    String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
	         		int FileCount = Integer.parseInt(CountOfFilesInFolder);
	         		folderPage.Upload_WithoutIndex(FolderPath, FileCount);	         		
	         		//=======Owner Level  Validation========================  			         		
	         		viewerScreenPage = new ViewerScreenPage(driver).get(); 
	         		viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);	         		
	         		viewerScreenPage.Image1();	         		
	         		viewerScreenPage.RFI_TestDataCreation_ownerLevel2();	     			
	    			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();	    			
	    			projectAndFolder_Level_Search.ProjectManagement();	         		
	         		projectAndFolder_Level_Search.RFINumberWithLocalSearch(RFINumber);
	         		projectAndFolder_Level_Search.ProjectManagement();	
	         		String usernamedir=System.getProperty("user.name");
	          		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";	         	            		
	         		folderPage.DownloadFiledownload(Sys_Download_Path);	
	         		//Log.assertThat( folderPage.DownloadFiledownload(Sys_Download_Path), "file Is downloading Downloading","File Is not Downloading Successfully", driver);
	    			viewerScreenPage = new ViewerScreenPage(driver).get(); 
	         		//viewerScreenPage.ClosePOPUP();	         	
	    			Log.assertThat( viewerScreenPage.Logout(), "Logout working Successfully","Logout not working Successfully", driver);
	    			
	         	    	       			
	        		
	        	}
	            catch(Exception e)
	            {
	            	e.getCause();
	            	Log.exception(e, driver);
	            }
	        	finally
	        	{
	        		Log.endTestCase();
	        		driver.close();
	        	}
	     	
	     }    
		    
		   		    
		       /*TC_039(Local search an RFI by it's subject.) 
			   1) Verify all the content of the RFI including status opening the RFI in the search result window.
		       2) Try to download associated attachments and verify.
		       3) Change the status of the RFI from the search result window and verify in cloud.
			   */
			    
			    
			    @Test(priority = 4, enabled = true , description = "Local search an RFI by it's subject.")
		        public void Verify_RFI_LocalSearch_With_Subject() throws Throwable
		        {
		       	 
		        	try
		        	{
		        		Log.testCaseInfo("TC_039(RFI Folder Level Validation):Local search an RFI by it's subject.");
		        		String uName = PropertyReader.getProperty("Username1");
		       		    String pWord = PropertyReader.getProperty("Password1");
		       		    //==============Login with valid UserID/PassWord===========================
		         		projectsLoginPage = new ProjectsLoginPage(driver).get();  
		         		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);    		
		         		
		         		//=========Create Project Randomly===================================
		         		String Project_Name = "Prj_"+Generate_Random_Number.generateRandomValue();
		         		String subject = "Cloud subject"+Generate_Random_Number.generateRandomValue1();
		         		Log.message("Subject verified Sucessfully :- "+subject);
		         		folderPage=projectDashboardPage.createProjectwithSubject(Project_Name, subject);		         		         		
		         		 //=========Create Folder Randomly===================================
		         		String Foldername = "Folder_"+Generate_Random_Number.generateRandomValue();
		         		folderPage.New_Folder_Create(Foldername);      		
		         		folderPage.Select_Folder(Foldername);
		         		//================File Upload without Index=====================
		         		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		         		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		         		int FileCount = Integer.parseInt(CountOfFilesInFolder);
	                    folderPage.Upload_WithoutIndex(FolderPath, FileCount);		         		
		         		//=======Owner Level  Validation========================  			         		
		         		viewerScreenPage = new ViewerScreenPage(driver).get(); 
		         		viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);
		         		
		         		viewerScreenPage.Image1();
		         		viewerScreenPage.RFITestDataCreationLevel(subject);
		     					    		
		         		//viewerScreenPage.swichToMainWindow();
		    			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();		    			
		    			projectAndFolder_Level_Search.ProjectManagement();		         		
		         		projectAndFolder_Level_Search.SearchRFISubject_Module(subject);		         		
		         		String usernamedir=System.getProperty("user.name");
		          		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";       	            		
		         		folderPage.DownloadFiledownload(Sys_Download_Path);
		          		//Log.assertThat( folderPage.DownloadFiledownload(Sys_Download_Path), "file Is downloading Downloading","File Is not Downloading Successfully", driver);
		         		viewerScreenPage = new ViewerScreenPage(driver).get(); 
		         		//viewerScreenPage.ClosePOPUP();
		    			Log.assertThat( viewerScreenPage.Logout(), "Logout working Successfully","Logout not working Successfully", driver);
		    			
		         		
		        	       			
		        		
		        	}
		            catch(Exception e)
		            {
		            	e.getCause();
		            	Log.exception(e, driver);
		            }
		        	finally
		        	{
		        		Log.endTestCase();
		        		driver.close();
		        	}
		     	
		     }       
			    
	               /*TC_040(Local search an RFI by it's custom attribute..) 
				   1) Verify all the content of the RFI including status opening the RFI in the search result window.
	               2) Try to download associated attachments and verify.
	               3) Change the status of the RFI from the search result window and verify in cloud.
	               4) Verify the order of custom attribute from search result window.
				    
				   */
				    
				    
				    @Test(priority =0, enabled =true , description = "Local search an RFI by custom attribute.")
			        public void Verify_RFI_LocalSearch_With_customattribute() throws Throwable
			        {
			       	 
			        	try
			        	{
			        		Log.testCaseInfo("TC_040(RFI Folder Level Search):Local search an RFI by it's Custom Attribute..");
			        		String uName = PropertyReader.getProperty("Username1");
			       		    String pWord = PropertyReader.getProperty("Password1");
			       		    //==============Login with valid UserID/PassWord===========================
			         		projectsLoginPage = new ProjectsLoginPage(driver).get();  
			         		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);			         		
			         		//=========Create Project Randomly===================================
			         		String Project_Name = "Project_"+Generate_Random_Number.generateRandomValue();
			         		String CustumAttribute = "NewScenario"+Generate_Random_Number.generateRandomValue1();
			         		Log.message("Custom Attribute Display As: "+CustumAttribute);
			         		folderPage=projectDashboardPage.createProjectwithCustomAtribute(Project_Name, CustumAttribute);         		
			         		//=========Create Folder Randomly===================================
			         		String Foldername = "Folder_"+Generate_Random_Number.generateRandomValue();
			         		folderPage.New_Folder_Create(Foldername);      		
			         		folderPage.Select_Folder(Foldername);
			         		//================File Upload without Index=========================
			         		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
			         		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
			         		int FileCount = Integer.parseInt(CountOfFilesInFolder);			         			
			         		folderPage.Upload_WithoutIndex(FolderPath, FileCount);			         		
			         		//=======Owner Level  Validation====================================  			         		
			         		viewerScreenPage = new ViewerScreenPage(driver).get(); 
			         		viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);			         		
			         		viewerScreenPage.Image1();
			         		viewerScreenPage.RFI_TestDataCreationLevel( CustumAttribute);
			         		//viewerScreenPage.swichToMainWindow();  
			    			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();			    			
			    			projectAndFolder_Level_Search.ProjectManagement();			    			
                            projectAndFolder_Level_Search.LocalSearchwithAnything(CustumAttribute);                            
                            String usernamedir=System.getProperty("user.name");
        	          		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads"; 			         		
			         		folderPage.DownloadFiledownload(Sys_Download_Path);
			         		viewerScreenPage = new ViewerScreenPage(driver).get(); 
			         		//viewerScreenPage.ClosePOPUP();
			    			Log.assertThat( viewerScreenPage.Logout(), "Logout working Successfully","Logout not working Successfully", driver);
			    				
			         		        	       			
			        		
			        	}
			            catch(Exception e)
			            {
			            	e.getCause();
			            	Log.exception(e, driver);
			            }
			        	finally
			        	{
			        		Log.endTestCase();
			        		driver.close();
			        	}
			     	
			     }      
				    
				    
				 
		   
	    
	    
	    
	    /*1)TC_041 Verify all the content of the RFI including status opening the RFI in the search result window.
        2) Try to download associated attachments and verify.
        3) Change the status of the RFI from the search result window and verify in cloud.
        4) Verify this with all kind of status.
        
	    */	    
	    
	    @Test(priority = 2, enabled = true , description = "Local Search the RFI with the status.")
      public void Verify_RFI_LocalSearch_AllStatus() throws Throwable
      {
     	 
      	try
      	
      	{
      		Log.testCaseInfo("TC_041(RFI Folder Level Validation):Local Search RFI by it's Status");
      		String uName = PropertyReader.getProperty("Username1");
     		String pWord = PropertyReader.getProperty("Password1");     		
     		String RFINumber = Generate_Random_Number.generateRandomValue1();
     		String Subject = "Subject123"+Generate_Random_Number.generateRandomValue1();
     		//==============Login with valid UserID/PassWord===========================
       		projectsLoginPage = new ProjectsLoginPage(driver).get();  
       		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);        		
       		
       	   //=========Create Project Randomly===================================
     		String Project_Name = "Prj_"+Generate_Random_Number.generateRandomValue();     		
     		folderPage=projectDashboardPage.CreateProject(Project_Name);        		
       		
       		//=========Create Folder Randomly===================================
     		String Foldername = "Folder_"+Generate_Random_Number.generateRandomValue();
     		folderPage.New_Folder_Create(Foldername);      		
     		folderPage.Select_Folder(Foldername);
     		//================File Upload without Index=====================
     		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
     		//Log.message(FolderPath);
     		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
     		int FileCount = Integer.parseInt(CountOfFilesInFolder);
     		folderPage.Upload_WithoutIndex(FolderPath, FileCount);
     		
     		//=======Owner Level  Validation========================  			         		
     		viewerScreenPage = new ViewerScreenPage(driver).get(); 
     		viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);     		
     		viewerScreenPage.Image1();
     		viewerScreenPage.RFI_TestDataCreation_ownerLevel_Status(); 			
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();			
			projectAndFolder_Level_Search.ProjectManagement();
			String Open = PropertyReader.getProperty("Status");
	       	projectAndFolder_Level_Search.AdvanceStatus(Open); 			
			String usernamedir=System.getProperty("user.name");
      		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";       	            		
     		folderPage.DownloadFiledownload(Sys_Download_Path);	
     		//viewerScreenPage.ClosePOPUP();
			projectAndFolder_Level_Search.MainStatus();
			
      	}
          catch(Exception e)
          {
          	e.getCause();
          	Log.exception(e, driver);
          }
      	finally
      	{
      		Log.endTestCase();
      		driver.close();
      	}
   	
   }       
	    
	  
	
				    
				    
				    
				    
				    
      
}
