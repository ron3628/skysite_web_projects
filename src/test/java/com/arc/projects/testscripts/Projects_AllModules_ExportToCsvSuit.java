package com.arc.projects.testscripts;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.ITestResult;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.projects.pages.ContactsPage;
import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.utils.AnalyzeLog;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arcautoframe.utils.*;


@Listeners(EmailReport.class)

public class Projects_AllModules_ExportToCsvSuit {
	
	
	String webSite;
    ITestResult result;    
    static WebDriver driver;
    ProjectsLoginPage projectsLoginPage;
    ProjectDashboardPage projectDashboardPage;
    FolderPage folderPage;
    ContactsPage contactsPage;
    ProjectDashboardPage projects_ExportToCSVScenarios;
    
    @Parameters("browser")
    @BeforeMethod(groups = "naresh_test")//Newly Added due to group execution Naresh
 public WebDriver beforeTest(String browser) {
        
        if(browser.equalsIgnoreCase("firefox")) 
        {
               File dest = new File("./drivers/win/geckodriver.exe");
               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
               driver = new FirefoxDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
        
        else if (browser.equalsIgnoreCase("chrome")) 
        { 
        File dest = new File("./drivers/win/chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());  
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
        ChromeOptions options = new ChromeOptions();
         options.addArguments("--start-maximized");
        options.setExperimentalOption("prefs", prefs);
        driver = new ChromeDriver( options );
        driver.get(PropertyReader.getProperty("SkysiteProdURL"));
 
        } 
        
        else if (browser.equalsIgnoreCase("safari"))
        {
               System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
               driver = new SafariDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
   return driver;
 }
     
     /** TC_001 (Verify Export to CSV for Normal Project, by Edit and by Search a project): Verify Export  to CSV for Normal Project, by Edit and by Search a project.
      * Scripted By : NARESH BABU kavuru
      * @throws Exception
      */
      @Test(priority = 0, enabled = true, description = "TC_001 (Verify Export to CSV for Normal Project, by Edit and by Search a project): Verify Export  to CSV for Normal Project, by Edit and by Search a project.")
      public void verify_ProjectExport_BySearch() throws Exception
      {
      	try
      	{
      		Log.testCaseInfo("TC_001 (Verify Export to CSV for Normal Project, by Edit and by Search a project): Verify Export  to CSV for Normal Project, by Edit and by Search a project.");
      	    //Getting Static data from properties file
    		String uName = PropertyReader.getProperty("Username");
    		String pWord = PropertyReader.getProperty("Password");
    		
      		projectsLoginPage = new ProjectsLoginPage(driver).get();  
      		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
      		
      		String usernamedir=System.getProperty("user.name");
      		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
      		String csvFileToRead = "C:\\" + "Users\\" + usernamedir + "\\Downloads\\Documents.csv";
      		Log.assertThat(projectDashboardPage.Validate_ModifiedProject_Export(Sys_Download_Path,csvFileToRead), "Modified Project Export is working Successfully", "Modified Project Export is Not working", driver);
    
    		//Log.assertThat(projectDashboardPage.Validate_ProjectExport(Sys_Download_Path,csvFileToRead), "Project level Export is working Successfully", "Project level Export is Not working", driver);
    		
      		String Search_PrjExp_csvPath = "C:\\" + "Users\\" + usernamedir + "\\Downloads\\Project.csv";
      		Log.assertThat(projectDashboardPage.Validate_SearchAProject_Export(Sys_Download_Path,Search_PrjExp_csvPath), "Search a Project and Export is working Successfully", "Search a Project and Export is Not working", driver);
      	}
          catch(Exception e)
          {
        	  AnalyzeLog.analyzeLog(driver);
        	  e.getCause();
        	  Log.exception(e, driver);
          }
      	finally
      	{
      		Log.endTestCase();
      		driver.quit();
      	}
      }
    
      
      /** TC_002 (Verify Search Parent Folder and Export to CSV): Verify Search Parent Folder and Export to CSV.
       * Scripted By: NARESH BABU 
       * @throws Exception
       */
       @Test(priority = 1, enabled = true, description = "TC_002 (Verify Search Parent Folder and Export to CSV): Verify Search Parent Folder and Export to CSV.")
       public void verify_Folder_Export() throws Exception
       {
       	try
       	{
       		Log.testCaseInfo("TC_002 (Verify Search Parent Folder and Export to CSV): Verify Search Parent Folder and Export to CSV.");
       	//Getting Static data from properties file
     		String uName = PropertyReader.getProperty("Username");
     		String pWord = PropertyReader.getProperty("Password");
     		
       		projectsLoginPage = new ProjectsLoginPage(driver).get();  
       		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
       		
       		String Prj_Name = PropertyReader.getProperty("Prj_Folder_Exp");
       		folderPage = projectDashboardPage.Validate_SelectAProject(Prj_Name);//Calling Select Project
       		
       		String usernamedir=System.getProperty("user.name");
       		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";       		
    		String csvFileToRead = "C:\\" + "Users\\" + usernamedir + "\\Downloads\\Documents.csv";
       		String Foldername = PropertyReader.getProperty("Search_Folder_Exp");
     		Log.assertThat(folderPage.SearchA_Folder_AndExport(Sys_Download_Path,Foldername,csvFileToRead), "Search a Folder and Export is working Successfully", "Search a Folder and Export is NOT working", driver);
       	}
       	catch(Exception e)
       	{
       		AnalyzeLog.analyzeLog(driver);
       		e.getCause();
       		Log.exception(e, driver);
       	}
       	finally
       	{
       		Log.endTestCase();
       		driver.quit();
       	}
       }
     
       /** TC_003 (Verify Export a Folder that contains unsupported files): Verify Export to csv for a folder contains unsupported files.
        * Scripted By: NARESH BABU KAVURU
        * @throws Exception
        */
        @Test(priority = 2, enabled = true, description = "TC_003 (Verify Export a Folder that contains unsupported files): Verify Export to csv for a folder contains unsupported files.")
        public void verify_FoldWithUnSupportFiles_Export() throws Exception
        {
        	try
        	{
        		Log.testCaseInfo("TC_003 (Verify Export a Folder that contains unsupported files): Verify Export to csv for a folder contains unsupported files.");
        	        //Getting Static data from properties file
        		String uName = PropertyReader.getProperty("Username");
        		String pWord = PropertyReader.getProperty("Password");
      		
        		projectsLoginPage = new ProjectsLoginPage(driver).get();  
        		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
        		
        		String Prj_Name = PropertyReader.getProperty("Prj_UnSuportFiles");
        		folderPage = projectDashboardPage.Validate_SelectAProject(Prj_Name);//Calling Select Project
        		
        		String usernamedir=System.getProperty("user.name");
                String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
        		String csvFileToRead = "C:\\" + "Users\\" + usernamedir + "\\Downloads\\Documents.csv";
        		Log.assertThat(folderPage.Export_Folder_WithUnSupportFiles(Sys_Download_Path,csvFileToRead), "Export A Folder contains unsupported files is working Successfully", "Export A Folder contains unsupported files is NOT working", driver);
        	}
            catch(Exception e)
            {
            	AnalyzeLog.analyzeLog(driver);
            	e.getCause();
            	Log.exception(e, driver);
            }
        	finally
        	{
        		Log.endTestCase();
        		driver.quit();
        	}
        }
        
        /** TC_004 (Verify Search Sub Folder and Export to CSV): Verify Export to csv by searching a Sub folder.
         * Scripted By: NARESH BABU 
         * @throws Exception
         */
         @Test(priority = 3, enabled = true, description = "TC_004 (Verify Search Sub Folder and Export to CSV): Verify Export to csv by searching a Sub folder.")
         public void verify_SubFold_Export() throws Exception
         {
         	try
         	{
         		Log.testCaseInfo("TC_004 (Verify Search Sub Folder and Export to CSV): Verify Export to csv by searching a Sub folder.");
         	//Getting Static data from properties file
         		String uName = PropertyReader.getProperty("Username");
         		String pWord = PropertyReader.getProperty("Password");
       		
         		projectsLoginPage = new ProjectsLoginPage(driver).get();  
         		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
         		
         		String Prj_Name = PropertyReader.getProperty("Prj_Folder_Exp");
         		folderPage = projectDashboardPage.Validate_SelectAProject(Prj_Name);//Calling Select Project

         		String usernamedir=System.getProperty("user.name");
         		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
         		
        		String csvFileToRead = "C:\\" + "Users\\" + usernamedir + "\\Downloads\\Documents.csv";
         		String Foldername = PropertyReader.getProperty("Search_SubFolder");
         		Log.assertThat(folderPage.SearchA_SubFolder_AndExport(Sys_Download_Path,Foldername,csvFileToRead), "Search a sub folder and export is working Successfully", "Search a sub folder and export is NOT working", driver);
         	}
             catch(Exception e)
             {
            	 AnalyzeLog.analyzeLog(driver);
            	 e.getCause();
            	 Log.exception(e, driver);
             }
         	finally
         	{
         		Log.endTestCase();
         		driver.quit();
         	}
         }
         
         /** TC_005 (Verify Gallery Folder Export to CSV): Verify Gallery Folder Export .
          * Scripted By: NARESH BABU 
          * @throws Exception
          */
          @Test(priority = 4, enabled = true, description = "TC_005 (Verify Gallery Folder Export to CSV): Verify Gallery Folder Export .")
          public void verify_GalleryFolder_Export() throws Exception
          {
          	try
          	{
          		Log.testCaseInfo("TC_005 (Verify Gallery Folder Export to CSV): Verify Gallery Folder Export .");
          	//Getting Static data from properties file
          		String uName = PropertyReader.getProperty("Username");
          		String pWord = PropertyReader.getProperty("Password");
        		
          		projectsLoginPage = new ProjectsLoginPage(driver).get();  
          		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
          		
          		String Prj_Name = PropertyReader.getProperty("Prj_Folder_Exp");
          		folderPage = projectDashboardPage.Validate_SelectAProject(Prj_Name);//Calling Select Project
          		
          		String usernamedir=System.getProperty("user.name");
          		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
          		String csvFileToRead = "C:\\" + "Users\\" + usernamedir + "\\Downloads\\Gallery.csv";
          		Log.assertThat(folderPage.Gallery_Folder_Export(Sys_Download_Path,csvFileToRead), "Gallery folder export is working Successfully", "Gallery folder export is NOT working", driver);
          	}
              catch(Exception e)
              {
            	  AnalyzeLog.analyzeLog(driver);
            	  e.getCause();
            	  Log.exception(e, driver);
              }
          	finally
          	{
          		Log.endTestCase();
          		driver.quit();
          	}
          }
          
          
          /** TC_006 (Verify Create A New Contact and Export to CSV): Verify Create Contact and Export to CSV file.
           * Scripted By: NARESH BABU KAVURU
           * @throws Exception
           */
           @Test(priority = 5, enabled = true, description = "TC_006 (Verify Create A New Contact and Export to CSV): Verify Create Contact and Export to CSV file.")
           public void Create_NewContact_Export() throws Exception
           {
           	try
           	{
           		Log.testCaseInfo("TC_006 (Verify Create A New Contact and Export to CSV): Verify Create Contact and Export to CSV file.");
           	//Getting Static data from properties file naresh1
           		String uName = PropertyReader.getProperty("Username");
           		String pWord = PropertyReader.getProperty("Password");
         		
           		projectsLoginPage = new ProjectsLoginPage(driver).get();  
           		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
           		
           		contactsPage = projectDashboardPage.Validate_SelectContactsBut();//Click on Contacts
           		String Random_Contct_Name = "Contact_"+Generate_Random_Number.generateRandomValue();
           		
           		Log.assertThat(contactsPage.Add_New_Contact(Random_Contct_Name), 
           				"New Contact Created successfully!!!", "Contact failed to add!!!", driver);
           		String usernamedir=System.getProperty("user.name");
           		
          		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
          		String csvFileToRead = "C:\\" + "Users\\" + usernamedir + "\\Downloads\\Contact.csv";
           		Log.assertThat(contactsPage.Add_New_Contact_AndExport(Sys_Download_Path,Random_Contct_Name,csvFileToRead), 
           				"Contacts Export to CSV is working Successfully", "Contacts Export to CSV is NOT working", driver);
           	}
            catch(Exception e)
            {
            	AnalyzeLog.analyzeLog(driver);
               	e.getCause();
               	Log.exception(e, driver);
            }
           	finally
           	{
           		Log.endTestCase();
           		driver.quit();
           	}
           }
            
            /** TC_007 (Verify Contact Group Export to CSV): Verify Contact Group Export to CSV File.
             * Scripted By: NARESH BABU KAVURU
             * @throws Exception
             */
             @Test(priority = 6, enabled = true, description = "TC_007 (Verify Contact Group Export to CSV): Verify Contact Group Export to CSV File.")
             public void Validate_Contact_Group_Export() throws Exception
             {
             	try
             	{
             		Log.testCaseInfo("TC_007 (Verify Contact Group Export to CSV): Verify Contact Group Export to CSV File.");
             	//Getting Static data from properties file naresh1
             		String uName = PropertyReader.getProperty("Username");
             		String pWord = PropertyReader.getProperty("Password");
           		
             		projectsLoginPage = new ProjectsLoginPage(driver).get();  
             		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
             		
             		contactsPage = projectDashboardPage.Validate_SelectContactsBut();//Click on Contacts
             		String usernamedir=System.getProperty("user.name");
               		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
               		String csvFileToRead = "C:\\" + "Users\\" + usernamedir + "\\Downloads\\ContactGroup.csv";
             		Log.assertThat(contactsPage.Contacts_GroupLevel_Export(Sys_Download_Path,csvFileToRead), 
             				"Contact Group Export to CSV is working Successfully", "Contact Group Export to CSV is NOT working", driver);
             	}
             	catch(Exception e)
             	{
             		AnalyzeLog.analyzeLog(driver);
                 	e.getCause();
                 	Log.exception(e, driver);
             	}
             	finally
             	{
             		Log.endTestCase();
             		driver.quit();
             	}
             }
             
/** TC_001_ImportModule (Import Contact and validate): Verify Import Contact and validate.
 * Scripted By: NARESH BABU KAVURU
 * @throws Exception
 */
@Test(priority = 7, enabled = true, description = "TC_001_ImportModule (Import Contact and validate): Verify Import Contact and validate.",groups = "naresh_test")
public void Validate_ImportContact() throws Exception
{
	try
	{
		Log.testCaseInfo("TC_001_ImportModule (Import Contact and validate): Verify Import Contact and validate.");
      	//Getting Static data from properties file naresh1
      		String uName = PropertyReader.getProperty("Username");
      		String pWord = PropertyReader.getProperty("Password");
    		
      		projectsLoginPage = new ProjectsLoginPage(driver).get();  
      		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
      		
      		contactsPage = projectDashboardPage.Validate_SelectContactsBut();//Click on Contacts
      		contactsPage.Write_newContactDetails_IntoImportFile();//Write new values into xlsx file
      		
      		File ContImp = new File(PropertyReader.getProperty("ImpContacts_FolderPath"));
      		String ImpContact_FoldPath = ContImp.getAbsolutePath().toString();
     		Log.assertThat(contactsPage.Import_Contact_FromXLSX_Validate(ImpContact_FoldPath), 
     				"Import Contact using .XLSX working Successfully", "Import Contact using .XLSX is NOT working", driver);

      	}
      	catch(Exception e)
      	{
      		AnalyzeLog.analyzeLog(driver);
          	e.getCause();
          	Log.exception(e, driver);
      	}
      	finally
      	{
      		Log.endTestCase();
      		driver.quit();
      	}
}
            
        
}
