package com.arc.projects.testscripts;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.Gallery_Photo_Page;
import com.arc.projects.pages.ProjectAndFolder_Level_Search;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.Search_Testcases;
import com.arc.projects.pages.SubmitalPage;
import com.arc.projects.pages.ViewerScreenPage;
import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)
public class Search_TestCases {

	public static WebDriver driver;
	ProjectsLoginPage projectsLoginPage;
	ProjectDashboardPage projectDashboardPage;
	ViewerScreenPage viewerScreenPage;	
	FolderPage folderPage;
	ProjectAndFolder_Level_Search projectAndFolder_Level_Search;
	Search_Testcases search_Testcases;
	Gallery_Photo_Page gallery_Photo_Page;
	SubmitalPage submitalPage;
	@Parameters("browser")
	@BeforeMethod
	public WebDriver beforeTest(String browser) {

		if (browser.equalsIgnoreCase("firefox")) {
			File dest = new File("./drivers/win/geckodriver.exe");
			// System.setProperty("webdriver.gecko.driver",
			// dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}

		else if (browser.equalsIgnoreCase("chrome")) {
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory", "C:" + File.separator + "Users" + File.separator
					+ System.getProperty("user.name") + File.separator + "Downloads");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			options.setExperimentalOption("prefs", prefs);
			driver = new ChromeDriver(options);
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}

		else if (browser.equalsIgnoreCase("safari")) {
			System.setProperty("webdriver.safari.noinstall", "true");
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		return driver;
	}

	 
	
	 /*TC_001:-----Verify user is able to do project search (Module ) Scripted By Ranjan */

	@Test(priority = 0, enabled = true, description = "Verify user is able to do project name search (Module Search)")
	public void ProjectName_ModuleSearch() throws Throwable {

		try {
			Log.testCaseInfo("TC_001:-Verify user is able to do project Name search (Module Search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ==============Login with valid UserID/PassWord=============
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// =========Create Proj Randomly===================================
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Searchscenario(Project_Name);
			Log.assertThat(search_Testcases.Searchprojectvalidation(Project_Name), "Search  verification  Successfully","Search Not verification Successfully", driver);
			
		    }
		    catch (Exception e)
		    {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		    } 
		    finally 
		    {
			Log.endTestCase();
			driver.quit();
		    }

	        }
	
	

	 /* TC_002:-----Verify user is able to do project search (Filter Search By ProjectName) : scripted by Ranjan*/

	@Test(priority = 1, enabled = true, description = "Verify user is able to do filter Search -Project Name")
	public void FilterSearch_ProjectName() throws Throwable {

		try {
			Log.testCaseInfo("TC_002 :-Verify user is able to do filter Search -Project Name");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Project_Name = "Project_" + Generate_Random_Number.generateRandomValue();
			String Project_Number = Generate_Random_Number.generateRandomValue();
			String Description = PropertyReader.getProperty("ProjectDescription");
			String Country = PropertyReader.getProperty("country");
			String City = PropertyReader.getProperty("city");
			String State = PropertyReader.getProperty("state");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.CreateProject(Project_Name, Project_Number, Description, Country, City, State);
			String Flag = "Search_ProjectName";
			Log.assertThat(search_Testcases.AdvanceSearch_Project(Project_Name, Flag),"Search  verification  Successfully", "Search Not verification Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	
	 /* TC_003:-----Verify user is able to Search by Folder name (Module search)): scripted by Ranjan */

	@Test(priority = 2, enabled = true , description = "Verify user is able to do folder Name search (Filter Search By folder Name)")
	public void FolderName_ModuleSearch() throws Throwable {

		try {
			Log.testCaseInfo("TC_003:-Verify user is able to do folder Name  search (Filter Search By folder Name)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Foldername = "Folder_" + Generate_Random_Number.generateRandomValue();
			//==============Login with valid UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// =========Create ProjectRandomly===================================
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			folderPage = new FolderPage(driver).get();
			folderPage.New_Folder_Create(Foldername);
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Searchscenario(Foldername);
			Log.assertThat(search_Testcases.SearchFoldervalidation(Foldername),"Search  folder verification  Successfully", "Search Folder Not verification Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	
	
 /*TC_004:---Verify user is able to do File search (Module ):scripted byRanjan */

	@Test(priority = 3, enabled = true , description = "Verify user is able to do  File search (Module )")
	public void FileName_ModuleSearch() throws Throwable {

		try {
			Log.testCaseInfo("TC_004:-Verify user is able to do file search (Filter Search By file Name)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Foldername = "Folder_" + Generate_Random_Number.generateRandomValue();
			String Filename = PropertyReader.getProperty("FileName");
			// ==============Login with valid UserID/PassWord======
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// =========Create Project Randomly======================
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			folderPage = new FolderPage(driver).get();
			folderPage.New_Folder_Create(Foldername);
			folderPage.Select_Folder(Foldername);
			// ============File Upload without Index================
			String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
			String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
			int FileCount = Integer.parseInt(CountOfFilesInFolder);
			folderPage.Upload_WithoutIndex(FolderPath, FileCount);
			viewerScreenPage = new ViewerScreenPage(driver).get();
			viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Searchscenario(Filename);
			Log.assertThat(search_Testcases.SearchFilevalidation(Filename), "File Name verified  Successfully","File Name verified Not verification Successfully", driver);

		    } 
		     catch (Exception e)
		    {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);
		    } finally
		    {
			Log.endTestCase();
			driver.quit();
		    }
	        }

	/* TC_005:---Verify user is able to do Album (Module search)-scripted by Ranjan*/

	@Test(priority = 4, enabled = true  , description = "Verify user is able to do Album (Module")
	public void Album_ModuleSearch() throws Throwable {

		try {
			Log.testCaseInfo("TC_005:-Verify user is able to do Album (Module search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Foldername = "Folder_" + Generate_Random_Number.generateRandomValue();
			String Albumname = Generate_Random_Number.AlbumName();
			String Filename = PropertyReader.getProperty("FileName");
			// ==============Login with valid UserID/PassWord========
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// =========Create Projec Randomly========================
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			folderPage = new FolderPage(driver).get();
			folderPage.Select_Gallery_Folder();
			gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Verify_AddAlbum(Albumname);
			search_Testcases.Searchscenario(Albumname);
			Log.assertThat(search_Testcases.Searchfolderphotovalidation_1(Albumname), "Album Name verified  Successfully","Album Name verified Not verification Successfully", driver);

		    } 
		    catch (Exception e) 
		    {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);
			
		    } 
		    finally 
		    {
			Log.endTestCase();
			driver.quit();
		    }

	        }

	
	 /* TC_006:---Verify user is able to do Photo Album Search (Module)-scripted by Ranjan*/

	@Test(priority = 5, enabled = true   , description = "Verify user is able to do Photo Album (Module search")
	public void photoSearch_Album() throws Throwable {

		try {
			Log.testCaseInfo("TC_006:-Verify user is able to do Photo album (Module search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String photo = PropertyReader.getProperty("photoName");
			String photolocation = PropertyReader.getProperty("Upload_PhotoFile");
			String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
			int FileCount = Integer.parseInt(CountOfFilesInFolder);
			String Foldername = "Folder_" + Generate_Random_Number.generateRandomValue();
			String Filename = PropertyReader.getProperty("FileName");
			String Albumname = Generate_Random_Number.AlbumName();
			// ==============Login with valid UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			// ================File Upload without Index Randomly===================================
			folderPage = new FolderPage(driver).get();
			folderPage.Select_Gallery_Folder();
			gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Verify_AddAlbum(Albumname);
			search_Testcases.Searchscenario(Albumname);
			search_Testcases.SelectGalleryFolder(Albumname);
			search_Testcases.UploadPhotos_InGallery(photolocation, FileCount);
			search_Testcases.Searchscenario(photo);
			Log.assertThat(search_Testcases.Searchfolderphotovalidation(),"Album Name verified Successfully","Album Name Not verified  Successfully", driver);

			

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_007:- Verify user is able to do Photos search (Global)-scripted by Ranjan*/

	@Test(priority = 6, enabled = true , description = "Verify user is able to do  Photos search (Global)")
	public void PhotoSearch_global() throws Throwable {

		try {
			Log.testCaseInfo("TC_007:-Verify user is able to do Photo Search (Global search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");			
			// ==============Login with valid UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			// ================File Upload without Index Randomly===================================
			gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
			folderPage = new FolderPage(driver).get();
			folderPage.Select_Gallery_Folder();			
			search_Testcases = new Search_Testcases(driver).get();
			// Select Album
			String Album_Name = "Static_Photo";
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Select_Album(Album_Name);
			Log.assertThat(search_Testcases.global_PhotoSearchscenario(Album_Name), "Album verified Successfully","Album Not verified Successfully", driver);

			
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_008:- Verify user is able to do Punch,(Module)*/

	@Test(priority = 7, enabled = true , description = "Verify user is able to do Punch,(Module search)")
	public void PunchSearch_Module() throws Throwable {

		try {
			Log.testCaseInfo("TC_008 :-Verify user is able to do Punch,(Module Search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			// =========File Upload without Index
			// Randomly===================================
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement_Punch();
			viewerScreenPage = new ViewerScreenPage(driver).get();
			viewerScreenPage.PunchCreation();
			search_Testcases = new Search_Testcases(driver).get();
			//search_Testcases.Searchscenario_punch();
			Log.assertThat(search_Testcases.Searchscenario_punch(), "Punch Search verified Successfully","Punch Search Not verified Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/*TC_009:- Verify user is able to do Punch,(Filter Search)*/

	@Test(priority = 8, enabled = true, description = "Verify user is able to do Punch,(Filter Search)")
	public void PunchSearch_Filter() throws Throwable {

		try {
			Log.testCaseInfo("TC_009 :-Verify user is able to do Punch,(Filter Search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			// =========File Upload without Index
			// Randomly===================================
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement_Punch();
			viewerScreenPage = new ViewerScreenPage(driver).get();
			viewerScreenPage.PunchCreation();
			search_Testcases = new Search_Testcases(driver).get();
			Log.assertThat(search_Testcases.AdvanceSearch_Description(),"Punch Advance Search Description verified  Successfully","Punch Advance Search Description Not verified Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_010:- Verify user is able to do RFI,(MODULE Search)*/

	@Test(priority = 9, enabled = true, description = "Verify user is able to do RFI(Module Search)")
	public void RFISearch_Module() throws Throwable {

		try {
			Log.testCaseInfo("TC_010 :-Verify user is able to do RFI(Module Search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.RFICREATION_New();
			search_Testcases = new Search_Testcases(driver).get();
			Log.assertThat(search_Testcases.Searchscenario_RFI(), "RFI Description verified  Successfully","RFI Description Not verified Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/*TC_011:- Verify user is able to Search RFI with RFI Number (Filter Search): Scripted By ranjan*/

	@Test(priority = 10, enabled = true, description = "Verify user is able to Search RFI with RFI Number (Filter Search)")
	public void RFISearch_Filter() throws Throwable {

		try {
			Log.testCaseInfo("TC_011 :- Verify user is able to Search RFI with RFI Number(Filter Search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.RFICREATION_New();
			search_Testcases = new Search_Testcases(driver).get();
			Log.assertThat(search_Testcases.RFI_AdvanceSearchWithRFINumber(),"RFI Advance Search Number verified  Successfully","RFI Advance Search Number Not verified Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_012:- Verify user is able to Search RFI with RFI Due Date (Filter Search)* Scripted bY RANJAN*/

	@Test(priority = 11, enabled = true, description = "Verify user is able to Search RFI with RFI Due Date (Filter Search)")
	public void RFIFilterSearch_DueDate() throws Throwable {

		try {
			Log.testCaseInfo("TC_012 :-Verify user is able to Search RFI with RFI Due Date (Filter Search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.RFICREATION_New();
			search_Testcases = new Search_Testcases(driver).get();
			Log.assertThat(search_Testcases.RFI_AdvanceSearchWithRFIDueDate(), "RFI Due Date verified  Successfully",
					"RFI Due Date Not verified Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/*TC_013:- Verify user is able to Search Submittal Number (Module Search): Scripted BY RANJAN */

	@Test(priority = 12, enabled = true , description = "Verify user is able to Search Submittal (Module Search)")
	public void SubmitalNumber_ModuleSearch() throws Throwable {

		try {
			Log.testCaseInfo("TC_013 :-Verify user is able to Search Submittal (Module Search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Submittal_Number = "SubId_" + Generate_Random_Number.generateRandomValue();
			String Submittal_Name = "SubName_" + Generate_Random_Number.generateRandomValue();
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement_Submittal();
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Create_Submital_AndValidate(Submittal_Number, Submittal_Name);			
			String Flag = "Submittal_Number_search";
			Log.assertThat(search_Testcases.Searchscenario_submittal_ModuleSearch(Flag),"Submittal Number verified  Successfully", "Submittal Number Not verified Successfully", driver);

			
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}
	
	/*TC_014:- Verify user is able to Search Submittal Name (Module Search): Scripted BY RANJAN */

	@Test(priority = 13, enabled = true , description = "Verify user is able to Search Submittal Name (Module Search)")
	public void SubmitalName_ModuleSearch() throws Throwable {

		try {
			Log.testCaseInfo("TC_014 :-Verify user is able to Search Submittal Name (Module Search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Submittal_Number = "SubId_" + Generate_Random_Number.generateRandomValue();
			String Submittal_Name = "SubName_" + Generate_Random_Number.generateRandomValue();
			// ===========Login with valid UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement_Submittal();
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Create_Submital_AndValidate(Submittal_Number, Submittal_Name);			
			String Flag = "Search_Submittal_Name";
			Log.assertThat(search_Testcases.Searchscenario_submittal_ModuleSearch(Flag),"Submittal Name verified  Successfully", "Submittal Name Not verified Successfully", driver);

			
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}
	
	/*TC_015:- Verify user is able to Search Submittal Status (Module Search): Scripted BY RANJAN */

	@Test(priority = 14, enabled = true, description = "Verify user is able to Search Submittal Status (Module Search)")
	public void SubmitalStatus_ModuleSearch() throws Throwable {

		try {
			Log.testCaseInfo("TC_015 :-Verify user is able to Search Submittal Status (Module Search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Submittal_Number = "SubId_" + Generate_Random_Number.generateRandomValue();
			String Submittal_Name = "SubName_" + Generate_Random_Number.generateRandomValue();
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement_Submittal();
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Create_Submital_AndValidate(Submittal_Number, Submittal_Name);			
			String Flag = "Search_Status";
			Log.assertThat(search_Testcases.Searchscenario_submittal_ModuleSearch(Flag),"Submittal Status verified  Successfully", "Submittal Status Not verified Successfully", driver);

			
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}



	/* TC_016:- Verify user is able to filter Search Submittal number (Filter Search)-Scripted by Ranjan*/

	@Test(priority = 15, enabled = true, description = "Verify user is able to filter Search Submittal number (Filter Search)")
	public void SubmitalfilterSearch_Number() throws Throwable {

		try {
			Log.testCaseInfo("TC_016 :-Verify user is able to filter Search Submittal number (Filter Search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Submittal_Number = Generate_Random_Number.generateRandomValue();
			String Submittal_Name = "SubName_" + Generate_Random_Number.generateRandomValue();
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement_Submittal();
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Create_Submital_AndValidate(Submittal_Number, Submittal_Name);
			projectAndFolder_Level_Search.ProjectManagement_Submittal();
			search_Testcases = new Search_Testcases(driver).get();
			String Flag = "Search_SubmittalNumber";
			Log.assertThat(search_Testcases.AdvanceSearch_Submittal(Submittal_Number, Flag),"Submittal Number verified  Successfully", "Submittal Number Not verified Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_017:- Verify user is able to filter Search Submittal Name (Filter Search) Scripted by Ranjan*/

	@Test(priority = 16, enabled = true, description = "Verify user is able to filter Search Submittal Name (Filter Search)")
	public void SubmitalfilterSearch_Name() throws Throwable {

		try {
			Log.testCaseInfo("TC_017 :-Verify user is able to filter Search Submittal Name (Filter Search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Submittal_Number = "SubId_" + Generate_Random_Number.generateRandomValue();
			String Submittal_Name = "SubName_" + Generate_Random_Number.generateRandomValue();

			// ===========Login with valid UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();

			projectAndFolder_Level_Search.ProjectManagement_Submittal();
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Create_Submital_AndValidate(Submittal_Number, Submittal_Name);
			projectAndFolder_Level_Search.ProjectManagement_Submittal();
			search_Testcases = new Search_Testcases(driver).get();
			String Flag = "Search_SubmittalName";
			Log.assertThat(search_Testcases.AdvanceSearch_Submittal(Submittal_Name, Flag),"Submittal Name verified  Successfully", "Submittal Name Not verified Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_018:- Verify user is able to filter Search Submittal Type (Filter Search) Scripted by Ranjan*/

	@Test(priority = 17, enabled = true, description = "Verify user is able to filter Search Submittal Type (Filter Search)")
	public void SubmitalfilterSearch_Type_olddata() throws Throwable {

		try {
			Log.testCaseInfo("TC_018 :-Verify user is able to filter Search Submittal Type (Filter Search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Submittal_Number = "SubId_" + Generate_Random_Number.generateRandomValue();
			String Submittal_Name = "SubName_" + Generate_Random_Number.generateRandomValue();
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement_Submittal();
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Create_Submital_AndValidate(Submittal_Number, Submittal_Name);
			String CommonValue = PropertyReader.getProperty("SubmittalType");
			String Flag = "Search_SubmittalType";
			Log.assertThat(search_Testcases.AdvanceSearch_Submittal(CommonValue, Flag),"Submittal type verified  Successfully", "Submittal type  Not verified Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_019:- Verify user is able to filter Search Submittal Status (Filter Search) */

	@Test(priority = 18, enabled = true, description = "Verify user is able to filter Search Submittal Status (Filter Search)")
	public void SubmitalfilterSearch_Status() throws Throwable {

		try {
			Log.testCaseInfo("TC_019 :-Verify user is able to filter Search Submittal Status (Filter Search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Submittal_Number = "SubId_" + Generate_Random_Number.generateRandomValue();
			String Submittal_Name = "SubName_" + Generate_Random_Number.generateRandomValue();

			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();

			projectAndFolder_Level_Search.ProjectManagement_Submittal();
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Create_Submital_AndValidate(Submittal_Number, Submittal_Name);
			String CommonValue = PropertyReader.getProperty("Status");
			String Flag = "Search_Status";
			Log.assertThat(search_Testcases.AdvanceSearch_Submittal(CommonValue, Flag),"Submittal Status verified  Successfully", "Submittal Status  Not verified Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_020:- Verify user is able to filter Search Submittal Due Date (Filter Search) Scripted by Ranjan */

	@Test(priority = 19, enabled = true, description = "Verify user is able to filter Search Submittal Due Date (Filter Search)")
	public void SubmitalfilterSearch_DueDate() throws Throwable {

		try {
			Log.testCaseInfo("TC_020 :-Verify user is able to filter Search Submittal Due Date (Filter Search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Submittal_Number = "SubId_" + Generate_Random_Number.generateRandomValue();
			String Submittal_Name = "SubName_" + Generate_Random_Number.generateRandomValue();

			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement_Submittal();

			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Create_Submital_AndValidate(Submittal_Number, Submittal_Name);
			String CommonValue = PropertyReader.getProperty("Status");
			String Flag = "Search_DueDate";
			search_Testcases.AdvanceSearch_Submittal(CommonValue, Flag);
			viewerScreenPage = new ViewerScreenPage(driver).get();
			Log.assertThat(viewerScreenPage.Logout(), "Logout working Successfully", "Logout not working Successfully",
					driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	// ===================Contacts Related Testcases=========================

	/* TC_021.Verify user is able to do filter Search- First Name- Scripted by Ranjan*/

	@Test(priority = 20, enabled = true, description = "Verify user is able to do filter Serarch  contact First Name (Filter Search)")
	public void ContactSearch_FirstName() throws Throwable {

		try {
			Log.testCaseInfo("TC_021:-Verify user is able to do filter Serarch First Name (Filter Search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ===========Login with valid UserID/PassWord==========
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			String Contact_FirstName = PropertyReader.getProperty("Contact_FirstName");
			String Flag = "Search_FirstName";
			// search_Testcases.AdvanceSearch_Contact(Contact_FirstName, Flag);
			Log.assertThat(search_Testcases.AdvanceSearch_Contact(Contact_FirstName, Flag),"Contact First Name  verified  Successfully", "Contact First Name  Not verified Successfully",
					driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_022.Verify user is able to do filter Search- Last Name-Scripted by Ranjan */

	@Test(priority = 21, enabled =true, description = "Verify user is able to do filter Search  Contact Last Name (Filter Search)")
	public void ContactSearch_LastName() throws Throwable {

		try {
			Log.testCaseInfo("TC_022 :-Verify user is able to do filter Search Last Name ");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");

			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			String Contact_LastName = PropertyReader.getProperty("Contact_LastName");
			String Flag = "Search_LastName";
			Log.assertThat(search_Testcases.AdvanceSearch_Contact(Contact_LastName, Flag),"Contact Last Name  verified  Successfully", "Contact Last Name  Not verified Successfully",
					driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_023.Verify user is able to do filter Search- Email :Scripted by Ranjan*/

	@Test(priority = 22, enabled = true, description = "Verify user is able to do filter Search- Email  (Filter Search)")
	public void ContactSearch_Email() throws Throwable {

		try {
			Log.testCaseInfo("TC_023 :-Verify user is able to do Contact filter Search Email");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");

			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			String Contact_Email = PropertyReader.getProperty("Contact_Email");
			String Flag = "Search_EMAIL";
			Log.assertThat(search_Testcases.AdvanceSearch_Contact(Contact_Email, Flag),"Contact Email verified  Successfully", "Contact Email  Not verified Successfully", driver);

		} catch (Exception e)
		{
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_24.Verify user is able to do filter Search- Phone-Scripted by Ranjan*/

	@Test(priority = 23, enabled = true , description = "Verify user is able to do Contact  filter Search PHONE ")
	public void ContactSearch_Phone() throws Throwable {

		try {
			Log.testCaseInfo("TC_024 :-Verify user is able to do filter Search Phone");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");

			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			String Contact_phone = PropertyReader.getProperty("Contact_Phone");
			String Flag = "Search_PHONE";
			// search_Testcases.AdvanceSearch_Contact(Contact_phone, Flag);
			Log.assertThat(search_Testcases.AdvanceSearch_Contact(Contact_phone, Flag),"Contact phone verified  Successfully", "Contact phone Not verified Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_025.Verify user is able to do filter Search- City - Scripted by Ranjan */

	@Test(priority = 24, enabled = true, description = "Verify user is able to do filter Search CITY ")
	public void ContactSearch_City() throws Throwable {

		try {
			Log.testCaseInfo("TC_025 :-Verify user is able to do Contact filter Search City");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");

			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			String Contact_CITY = PropertyReader.getProperty("Contact_City");
			String Flag = "Search_CITY";
			Log.assertThat(search_Testcases.AdvanceSearch_Contact(Contact_CITY, Flag),"Contact City verified  Successfully", "Contact City Not verified Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_026.Verify user is able to do filter Search- Address-Scripted by Ranjan*/

	@Test(priority = 25, enabled = true, description = "Verify user is able to do filter Search Address ")
	public void ContactSearch_Address() throws Throwable {

		try {
			Log.testCaseInfo("TC_026 :-Verify user is able to do Contact filter Search Address");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			String Contact_Address = PropertyReader.getProperty("Contact_Address");
			String Flag = "Search_Address";
			Log.assertThat(search_Testcases.AdvanceSearch_Contact(Contact_Address, Flag),"Contact Address verified  Successfully", "Contact Address  Not verified Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_027.Verify user is able to punch filter Search- Description-Scripted by Ranjan */

	@Test(priority = 26, enabled = true, description = "Verify user is able to do filter Search Punch - Description")
	public void FilterSearch_Punch_Description() throws Throwable {

		try {
			Log.testCaseInfo("TC_027 :-Verify user is able to do  filter Search Punch - Description");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement_Punch();
			viewerScreenPage = new ViewerScreenPage(driver).get();
			viewerScreenPage.PunchCreation();
			search_Testcases = new Search_Testcases(driver).get();
			String Description_sub = PropertyReader.getProperty("Punch_Subject");
			String Flag = "Search_Description";
			Log.assertThat(search_Testcases.AdvanceSearch_Punch(Description_sub, Flag),"Punch Description verified  Successfully", "Punch Description  Not verified Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_028.Verify user is able to punch filter Search- creator Scripted by Ranjan */

	@Test(priority = 27, enabled = true, description = "Verify user is able to do filter Search Punch - Creator")
	public void FilterSearch_Punch_Creator() throws Throwable {

		try {
			Log.testCaseInfo("TC_028 :-Verify user is able to do filter Search Punch - Creator");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");

			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement_Punch();
			viewerScreenPage = new ViewerScreenPage(driver).get();
			viewerScreenPage.PunchCreation();
			search_Testcases = new Search_Testcases(driver).get();
			String creator = PropertyReader.getProperty("Creator");
			String Flag = "Search_Creator";
			Log.assertThat(search_Testcases.AdvanceSearch_Punch(creator, Flag), "Punch Creator verified  Successfully","Punch Creator  Not verified Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_029.Verify user is able to punch filter Search- Assignee Scripted by Ranjan*/

	@Test(priority = 28, enabled = true , description = "Verify user is able to do filter Search Punch - Assignee")
	public void FilterSearch_Punch_Assignee() throws Throwable {

		try {
			Log.testCaseInfo("TC_029 :-Verify user is able to do filter Search Punch - Assignee");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// ======================================================================
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement_Punch();

			viewerScreenPage = new ViewerScreenPage(driver).get();
			viewerScreenPage.PunchCreation();
			search_Testcases = new Search_Testcases(driver).get();
			String Assigne = PropertyReader.getProperty("SelectUserAssignTo_Email");
			String Flag = "Search_Assignee";
			Log.assertThat(search_Testcases.AdvanceSearch_Punch(Assigne, Flag), "Punch Assignee verified  Successfully","Punch Assignee  Not verified Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}

	/* TC_030.Verify user is able to punch filter Search- Status :Scripted by Ranjan*/

	@Test(priority = 29, enabled = true, description = "Verify user is able to do filter Search Punch - Status")
	public void FilterSearch_Punch_Status() throws Throwable {

		try {
			Log.testCaseInfo("TC_030 :-Verify user is able to do filter Search Punch - Status");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);

			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement_Punch();
			viewerScreenPage = new ViewerScreenPage(driver).get();
			viewerScreenPage.PunchCreation();
			search_Testcases = new Search_Testcases(driver).get();
			String status = PropertyReader.getProperty("Status");
			String Flag = "Search_Status";
			Log.assertThat(search_Testcases.AdvanceSearch_Punch(status, Flag), "Punch status verified  Successfully","Punch status  Not verified Successfully", driver);

			// search_Testcases.AdvanceSearch_Punch(status, Flag);
			// viewerScreenPage = new ViewerScreenPage(driver).get();
			// Log.assertThat(viewerScreenPage.Logout(), "Logout working
			// Successfully", "Logout not working Successfully",
			// driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	// ======================Project Related search========================================

	/* TC_031.Verify user is able do Filter Search- Project Number Scripted by Ranjan */

	@Test(priority = 30, enabled = true, description = "Verify user is able to do filter Search -Project Number")
	public void FilterSearch_ProjectNumber() throws Throwable {

		try {
			Log.testCaseInfo("TC_031 :-Verify user is able to do filter Search -Project Number");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Project_Name = "Project_" + Generate_Random_Number.generateRandomValue();
			String Project_Number = Generate_Random_Number.generateRandomValue();
			String Description = PropertyReader.getProperty("ProjectDescription");
			String Country = PropertyReader.getProperty("country");
			String City = PropertyReader.getProperty("city");
			String State = PropertyReader.getProperty("state");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.CreateProject(Project_Name, Project_Number, Description, Country, City, State);
			String Flag = "Search_ProjectNumber";
			// search_Testcases.AdvanceSearch_Project(Project_Number, Flag);
			Log.assertThat(search_Testcases.AdvanceSearch_Project(Project_Number, Flag),"Project Number verified  Successfully", "project Number  Not verified Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_032.Verify user is able do Filter Search- Project Description :Scripted by Ranjan*/

	@Test(priority = 31, enabled = true, description = "Verify user is able to do filter Search -Description")
	public void FilterSearch_Description() throws Throwable {

		try {
			Log.testCaseInfo("TC_032 :-Verify user is able to do filter Search -Description");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Project_Name = "Project_" + Generate_Random_Number.MyDate();
			String Project_Number = Generate_Random_Number.MyDate();
			String Description = Generate_Random_Number.generateRandomValue() + "desc";
			String Country = PropertyReader.getProperty("country");
			String City = PropertyReader.getProperty("city");
			String State = PropertyReader.getProperty("state");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.CreateProject(Project_Name, Project_Number, Description, Country, City, State);
			String Flag = "Search_ProjectDescription";
			Log.assertThat(search_Testcases.AdvanceSearch_Project(Description, Flag),"Project Description verified  Successfully", "project Description  Not verified Successfully",
					driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_033.Verify user is able do Filter Search- project City :Scripted by Ranjan*/

	@Test(priority = 32, enabled = true, description = "Verify user is able to do filter Search -CITY")
	public void FilterSearch_CITY() throws Throwable {

		try {
			Log.testCaseInfo("TC_033 :-Verify user is able to do filter Search - Project CITY");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Project_Name = "Project_" + Generate_Random_Number.generateRandomValue();
			String Project_Number = Generate_Random_Number.generateRandomValue();
			String Description = Generate_Random_Number.generateRandomValue() + "desc";
			String Country = PropertyReader.getProperty("country");
			String City = PropertyReader.getProperty("city");
			String State = PropertyReader.getProperty("state");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.CreateProject(Project_Name, Project_Number, Description, Country, City, State);
			String Flag = "Search_City";
			Log.assertThat(search_Testcases.AdvanceSearch_Project(City, Flag), "Project City verified  Successfully","project City  Not verified Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_034.Verify user is able do Filter Search-State :Scripted by Ranjan*/

	@Test(priority = 33, enabled = true, description = "Verify user is able to do filter Search -State")
	public void FilterSearch_State() throws Throwable {

		try {
			Log.testCaseInfo("TC_034 :-Verify user is able to do filter Search -Project State");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Project_Name = "Project_" + Generate_Random_Number.generateRandomValue();
			String Project_Number = Generate_Random_Number.generateRandomValue();
			String Description = Generate_Random_Number.generateRandomValue() + "desc";
			String Country = PropertyReader.getProperty("country");
			String City = PropertyReader.getProperty("city");
			String state = PropertyReader.getProperty("State");

			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.CreateProject(Project_Name, Project_Number, Description, Country, City, state);
			String Flag = "Search_State";
			// search_Testcases.AdvanceSearch_Project(State, Flag);
			Log.assertThat(search_Testcases.AdvanceSearch_Project(state, Flag), "Project State verified  Successfully","project State  Not verified Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_035.Verify user is able do Filter Search Project-Country :Scripted by Ranjan*/

	@Test(priority = 34, enabled = true, description = "Verify user is able to do filter Search project -Country")
	public void FilterSearch_Country() throws Throwable {

		try {
			Log.testCaseInfo("TC_035 :-Verify user is able to do filter Search Project-Country");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Project_Name = "Project_" + Generate_Random_Number.generateRandomValue();
			String Project_Number = Generate_Random_Number.generateRandomValue();
			String Description = Generate_Random_Number.generateRandomValue() + "desc";
			String Country = PropertyReader.getProperty("country");
			String City = PropertyReader.getProperty("city");
			String State = PropertyReader.getProperty("state");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.CreateProject(Project_Name, Project_Number, Description, Country, City, State);

			String Flag = "Search_Country";
			Log.assertThat(search_Testcases.AdvanceSearch_Project(Country, Flag),"Project State verified  Successfully", "project State  Not verified Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_036.Verify user is able do Filter Search SendFile-Inbox-Exact order :Scripted by Ranjan*/

	@Test(priority = 35, enabled = true, description = "Verify user is able to do filter Search SendFile-Exact order")
	public void SendFile_Exactorder() throws Throwable {

		try {
			Log.testCaseInfo("TC_036 :-Verify user is able to do filter Search SendFile-Exact order");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			// search_Testcases.Sendfile_selection();
			String Flag = "Search_exactorder";
			// search_Testcases.AdvanceSearch_Sendfile_Inbox(Flag);
			Log.assertThat(search_Testcases.AdvanceSearch_Sendfile_Inbox(Flag),"Send File inbox Exact order verified Successfully", "Send File inbox Not verified Successfully",
					driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_037.Verify user is able do Filter Search SendFile-inbox-Date :Scripted by Ranjan*/

	@Test(priority = 36, enabled = true, description = "Verify user is able to do filter Search SendFile inbox -Date")
	public void SendFile_Date() throws Throwable {

		try {
			Log.testCaseInfo("TC_037 :-Verify user is able to do filter Search SendFile inbox -Date");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Sendfile_selection();
			String Flag = "Search_date";
			Log.assertThat(search_Testcases.AdvanceSearch_Sendfile_Inbox(Flag),"Send File inbox Date verified Successfully", "Send File inbox Date Not verified Successfully",
					driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_038.Verify user is able do Filter Search SendFile Inbox-subject :Scripted by Ranjan*/

	@Test(priority = 37, enabled = true , description = "Verify user is able to do filter Search SendFile-Subject")
	public void SendFile_Subject() throws Throwable {

		try {
			Log.testCaseInfo("TC_038 :-Verify user is able to do filter Search SendFile-subject");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");

			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			// search_Testcases.Sendfile_selection();
			String Flag = "Search_Subject";			
			Log.assertThat(search_Testcases.AdvanceSearch_Sendfile_Inbox(Flag),"Subject   verified   Successfully", "Subject Not verification Successfully", driver);


		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_039.Verify user is able do Filter Search SendFile-Sender :Scripted by Ranjan*/

	@Test(priority = 38, enabled = true, description = "Verify user is able to do filter Search SendFile-Sender Email with Old data")
	public void SendFile_Email() throws Throwable {

		try {
			Log.testCaseInfo("TC_039 :-Verify user is able to do filter Search SendFile-sender Email ");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");

			// ===========Login with valid UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases = new Search_Testcases(driver).get();
			// search_Testcases.Sendfile_selection();
			String Flag = "Search_senderEmail";			
			Log.assertThat(search_Testcases.AdvanceSearch_Sendfile_Inbox(Flag),"Email   verified   Successfully", "Email  Not verification Successfully", driver);


		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/** TC_040.Verify user is able do Filter Search SendFile-Tracking-Exact order:Scripted by Ranjan*/

	@Test(priority = 39, enabled = true, description = "Verify user is able to do filter Search SendFile-tracking-Exact order")
	public void SendFile_Tracking_Exactorder() throws Throwable {

		try {
			Log.testCaseInfo("TC_040 :-Verify user is able to do filter Search SendFile-Tracking-Exact order");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			// search_Testcases.Sendfile_selection();
			String Flag = "Search_exactorder";			
			Log.assertThat(search_Testcases.AdvanceSearch_Sendfile_Tracking(Flag),"Exact Order   verified   Successfully", "Exact Order  Not verification Successfully", driver);



		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_041.Verify user is able do Filter Search SendFile-tracking-Date :Scripted by Ranjan*/

	@Test(priority = 40, enabled = true  , description = "Verify user is able to do filter Search SendFile tracking -Date")
	public void SendFile_Tracking_Date() throws Throwable {

		try {
			Log.testCaseInfo("TC_041 :-Verify user is able to do filter Search SendFile Tracking-Date");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Sendfile_selection();
			String Flag = "Search_date";			
			Log.assertThat(search_Testcases.AdvanceSearch_Sendfile_Tracking(Flag),"Date   verified   Successfully", "Date  Not verification Successfully", driver);


		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_042.Verify user is able do Filter Search SendFile Tracking-subject :Scripted by Ranjan*/

	@Test(priority = 41, enabled = true, description = "Verify user is able to do filter Search SendFile-Tracking-Subject")
	public void SendFile_Tracking_Subject() throws Throwable {

		try {
			Log.testCaseInfo("TC_042 :-Verify user is able to do filter Search SendFile-subject");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");

			// ===========Login with valid UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			// search_Testcases.Sendfile_selection();
			String Flag = "Search_Subject";		
			Log.assertThat(search_Testcases.AdvanceSearch_Sendfile_Tracking(Flag),"Subject  verified   Successfully", "subject  Not verification Successfully", driver);


		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_043.Verify user is able do Filter Search SendFile-receiverEmail:Scripted by Ranjan */

	@Test(priority = 42, enabled = true , description = "Verify user is able to do filter Search SendFile-receiver Email")
	public void SendFile_receiver_Email() throws Throwable {

		try {
			Log.testCaseInfo("TC_043 :-Verify user is able to do filter Search SendFile-sender Email");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");

			// ===========Login with valid UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Sendfile_selection();
			String Flag="Search_senderEmail";
			Log.assertThat(search_Testcases.AdvanceSearch_Sendfile_Tracking( Flag),"Sender Email  verified  Successfully", "Sender Email Not verified Successfully", driver);


		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	// ============ Shared Project Filter  Serach==========================================
	//====================================================================================

	/* TC_044:-----Verify user is able to do Shared project (Filter Search By ProjectName ) scripted by Ranjan*/

	@Test(priority = 43, enabled = true, description = "Verify user is able to do Shared Project filter Search -Project Name")
	public void FilterSearch_SharedProject_ProjectName() throws Throwable {

		try {
			Log.testCaseInfo("TC_044 :-Verify user is able to do SharedProject filter Search -Project Name");
			String uName = PropertyReader.getProperty("GuestUserName");
			String pWord = PropertyReader.getProperty("GuestUserPassword");
			String Project_Name = "Project_" + Generate_Random_Number.generateRandomValue();
			String Project_Number = Generate_Random_Number.generateRandomValue();
			String Description = PropertyReader.getProperty("ProjectDescription");
			String Country = PropertyReader.getProperty("country");
			String City = PropertyReader.getProperty("city");
			String State = PropertyReader.getProperty("state");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.CreateProject(Project_Name, Project_Number, Description, Country, City, State);
			String Flag = "Search_ProjectName";
			Log.assertThat(search_Testcases.AdvanceSearch_Project(Project_Name, Flag),"Search  verification  Successfully", "Search Not verification Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/*TC_045:-----Verify user is able to do Shared project (Filter Search By ProjectNumber ) scripted by Ranjan*/

	@Test(priority = 44, enabled = true , description = "Verify user is able to do Shared Project filter Search -Project Number")
	public void FilterSearch_SharedProject_ProjectNumber() throws Throwable {

		try {
			Log.testCaseInfo("TC_045 :-Verify user is able to do SharedProject filter Search -Project Number");
			String uName = PropertyReader.getProperty("GuestUserName");
			String pWord = PropertyReader.getProperty("GuestUserPassword");
			String Project_Name = "Project_" + Generate_Random_Number.generateRandomValue();
			String Project_Number = Generate_Random_Number.generateRandomValue();
			String Description = PropertyReader.getProperty("ProjectDescription");
			String Country = PropertyReader.getProperty("country");
			String City = PropertyReader.getProperty("city");
			String State = PropertyReader.getProperty("state");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.CreateProject(Project_Name, Project_Number, Description, Country, City, State);
			String Flag = "Search_ProjectNumber";
			Log.assertThat(search_Testcases.AdvanceSearch_Project(Project_Number, Flag),"Search  verification  Successfully", "Search Not verification Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/*TC_046:-----Verify user is able to do Shared project (Filter Search By Description ) scripted by Ranjan*/

	@Test(priority = 45, enabled = true, description = "Verify user is able to do Shared Project filter Search -Description")
	public void FilterSearch_SharedProject_Description() throws Throwable {

		try {
			Log.testCaseInfo("TC_046 :-Verify user is able to do SharedProject filter Search -Description");
			String uName = PropertyReader.getProperty("GuestUserName");
			String pWord = PropertyReader.getProperty("GuestUserPassword");
			String Project_Name = "Project_" + Generate_Random_Number.generateRandomValue();
			String Project_Number = Generate_Random_Number.generateRandomValue();
			String Description = PropertyReader.getProperty("ProjectDescription");
			String Country = PropertyReader.getProperty("country");
			String City = PropertyReader.getProperty("city");
			String State = PropertyReader.getProperty("state");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.CreateProject(Project_Name, Project_Number, Description, Country, City, State);
			String Flag = "Search_ProjectDescription";
			Log.assertThat(search_Testcases.AdvanceSearch_Project(Description, Flag),"Search  verification  Successfully", "Search Not verification Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_047:-----Verify user is able to do Shared project (Filter Search By Country ) scripted by Ranjan*/

	@Test(priority = 46, enabled = true, description = "Verify user is able to do Shared Project filter Search -By Country")
	public void FilterSearch_SharedProject_Country() throws Throwable {

		try {
			Log.testCaseInfo("TC_047 :-Verify user is able to do SharedProject filter Search -By Country");
			String uName = PropertyReader.getProperty("GuestUserName");
			String pWord = PropertyReader.getProperty("GuestUserPassword");
			String Project_Name = "Project_" + Generate_Random_Number.generateRandomValue();
			String Project_Number = Generate_Random_Number.generateRandomValue();
			String Description = PropertyReader.getProperty("ProjectDescription");
			String Country = PropertyReader.getProperty("country");
			String City = PropertyReader.getProperty("city");
			String State = PropertyReader.getProperty("state");
			// ===========Login with valid UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.CreateProject(Project_Name, Project_Number, Description, Country, City, State);
			String Flag = "Search_Country";
			Log.assertThat(search_Testcases.AdvanceSearch_Project(Country, Flag), "Search  verification  Successfully","Search Not verification Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/*TC_048:-----Verify user is able to do Shared project (Filter Search By  CITY ) scripted by Ranjan*/

	@Test(priority = 47, enabled = true, description = "Verify user is able to do Shared Project filter Search -By CITY")
	public void FilterSearch_SharedProject_CITY() throws Throwable {

		try {
			Log.testCaseInfo("TC_048 :-Verify user is able to do SharedProject filter Search -By CITY");
			String uName = PropertyReader.getProperty("GuestUserName");
			String pWord = PropertyReader.getProperty("GuestUserPassword");
			String Project_Name = "Project_" + Generate_Random_Number.generateRandomValue();
			String Project_Number = Generate_Random_Number.generateRandomValue();
			String Description = PropertyReader.getProperty("ProjectDescription");
			String Country = PropertyReader.getProperty("country");
			String City = PropertyReader.getProperty("city");
			String State = PropertyReader.getProperty("state");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.CreateProject(Project_Name, Project_Number, Description, Country, City, State);
			String Flag = "Search_City";
			Log.assertThat(search_Testcases.AdvanceSearch_Project(City, Flag), "Search  verification  Successfully","Search Not verification Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/*TC_049:-----Verify user is able to do Shared project (Filter Search By State ) scripted by Ranjan*/

	@Test(priority = 48, enabled = true, description = "Verify user is able to do Shared Project filter Search -By State")
	public void FilterSearch_SharedProject_State() throws Throwable {

		try {
			Log.testCaseInfo("TC_049 :-Verify user is able to do SharedProject filter Search -By State");
			String uName = PropertyReader.getProperty("GuestUserName");
			String pWord = PropertyReader.getProperty("GuestUserPassword");
			String Project_Name = "Project_" + Generate_Random_Number.generateRandomValue();
			String Project_Number = Generate_Random_Number.generateRandomValue();
			String Description = PropertyReader.getProperty("ProjectDescription");
			String Country = PropertyReader.getProperty("country");
			String City = PropertyReader.getProperty("city");
			String State = PropertyReader.getProperty("state");
			// ===========Login with valid UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.CreateProject(Project_Name, Project_Number, Description, Country, City, State);
			String Flag = "Search_State";
			Log.assertThat(search_Testcases.AdvanceSearch_Project(State, Flag), "Search  verification  Successfully","Search Not verification Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/*TC_050:-----Verify user is able to do Shared project (Filter Search By RFI Number) scripted by Ranjan*/

	@Test(priority = 49, enabled = true  , description = "Verify user is able to do Shared project  (Filter Search By RFI Number) ")
	public void FilterSearch_SharedProject_RFINumber() throws Throwable {

		try {
			Log.testCaseInfo("TC_050 :-Verify user is able to do Shared project  (Filter Search By RFI Number) ");
			String uName = PropertyReader.getProperty("GuestUserName");
			String pWord = PropertyReader.getProperty("GuestUserPassword");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);

			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.ProjectManagement();
			String Flag = "Search_RFINumber";			
			Log.assertThat(search_Testcases.AdvanceSearch_SharedProject_RFINumber(Flag), "RFI Number verified Successfully", "RFI Number not Verified Successfully",
					driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_051:-----Verify user is able to do Shared project (Module Search By RFI Number) scripted by Ranjan*/

	@Test(priority = 50, enabled = true , description = "Verify user is able to do Shared project  (Module Search By RFI Number) ")
	public void ModuleSearch_SharedProject_RFINumber() throws Throwable {

		try {
			Log.testCaseInfo("TC_051 :-Verify user is able to do Shared project  (Module Search By RFI Number) ");
			String uName = PropertyReader.getProperty("GuestUserName");
			String pWord = PropertyReader.getProperty("GuestUserPassword");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.ProjectManagement();
			String Flag = "Search_RFINumber";		
			Log.assertThat(search_Testcases.ModuleSearch_SharedProject_RFINumber(Flag), "RFI Number verified Successfully", "RFI Number not Verified Successfully",
					driver);
			

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	// =================================Photo Attribute Test-cases=================================================
	
	//============================================================================================================
	
	/* TC_052:-----Verify user is able to do Photo Attribute filter search By Level (Filter search by level name) scripted by Ranjan*/

	@Test(priority = 51, enabled = true , description = "Verify user is able to do Photo Attribute filter search By Level ")
	public void PhotoAttribute_level() throws Throwable {

		try {
			Log.testCaseInfo("TC_052 :-Verify user is able to do Photo Attribute filter search By Level ");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Level = PropertyReader.getProperty("Level");
			// ===========Login with valid UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// gallery  selection
			folderPage.Select_Gallery_Folder();
			// Select Album
			String Album_Name = "Static_Photo";
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Verify_SelectAn_Album(Album_Name);
			// search_Testcases.ProjectManagement();
			String Flag = "Search_level";
			Log.assertThat(search_Testcases.FilterSearch_Photoscenario(Level, Flag), "Level verified Successfully", "Level not Verified Successfully",
					driver);
			

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/*TC_053:-----Verify user is able to do Photo Attribute filter search By ROOM (Filter search by ROOM name) scripted by Ranjan*/

	@Test(priority = 52, enabled = true, description = "Verify user is able to do Photo Attribute filter search By ROOM ")
	public void PhotoAttribute_Room() throws Throwable {

		try {
			Log.testCaseInfo("TC_053 :-Verify user is able to do Photo Attribute filter search By ROOM ");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Room = PropertyReader.getProperty("Room");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// Gallery Selection
			folderPage.Select_Gallery_Folder();
			// Select Album
			String Album_Name = "Static_Photo";
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Verify_SelectAn_Album(Album_Name);
			// search_Testcases.ProjectManagement();
			String Flag = "Search_Room";			
			Log.assertThat(search_Testcases.FilterSearch_Photoscenario(Room, Flag), "Room verified Successfully", "Room not Verified Successfully",
					driver);
			

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_054:-----Verify user is able to do Photo Attribute filter search By Area (Filter search by Area) scripted by Ranjan*/

	@Test(priority = 53, enabled = true , description = "Verify user is able to do Photo Attribute filter search By Area ")
	public void PhotoAttribute_Area() throws Throwable {

		try {
			Log.testCaseInfo("TC_054 :-Verify user is able to do Photo Attribute filter search By Area ");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Area = PropertyReader.getProperty("Area");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			//
			folderPage.Select_Gallery_Folder();
			// Select Album
			String Album_Name = "Static_Photo";
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Verify_SelectAn_Album(Album_Name);
			// search_Testcases.ProjectManagement();
			String Flag = "Search_Area";			
			Log.assertThat(search_Testcases.FilterSearch_Photoscenario(Area, Flag), "Area verified Successfully", "Area not Verified Successfully",
					driver);
			

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/*
	 * TC_055:-----Verify user is able to do Photo Attribute filter search By Description (Filter search by Description) scripted by Ranjan*/

	@Test(priority = 54, enabled = true, description = "Verify user is able to do Photo Attribute filter search By Description ")
	public void PhotoAttribute_Description() throws Throwable {

		try {
			Log.testCaseInfo("TC_055 :-Verify user is able to do Photo Attribute filter search By Description ");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Description = PropertyReader.getProperty("Description");
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			//
			folderPage.Select_Gallery_Folder();
			// Select Album
			String Album_Name = "Static_Photo";
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Verify_SelectAn_Album(Album_Name);
			// search_Testcases.ProjectManagement();
			String Flag = "Search_Description";			
			Log.assertThat(search_Testcases.FilterSearch_Photoscenario(Description, Flag), "Description verified Successfully", "Description not Verified Successfully",
					driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}

	
	//===============Project Level Search============================================================
	/*
	 * TC_056(Local Search an RFI with it's RFI number.) 1) Verify all the
	 * content of the RFI including status opening the RFI in the search result
	 * window. 2) Try to download associated attachments and verify. 3) Change
	 * the status of the RFI from the search result window and verify in cloud.
	 * 
	 */

	@Test(priority = 55, enabled = true , description = "Local Search an RFI with it's RFI number.")
	public void VerifyRFI_LocalSearch_With_RFINumber() throws Throwable {

		try {
			Log.testCaseInfo("TC_056(RFI Project Level Validation):Local Search an RFI with it's RFI number.");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ==============Login with validUserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// =========Create Project Randomly===================================
			String Project_Name = "Prj_" + Generate_Random_Number.generateRandomValue();
			String RFINumber = Generate_Random_Number.generateRandomValue1();
			Log.message("Subject verified Sucessfully :- " + RFINumber);
			folderPage = projectDashboardPage.createProjectwithSpecificRFINumber(Project_Name, RFINumber);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.RFICREATION_New();
			projectAndFolder_Level_Search.RFINumberWithLocalSearch(RFINumber);
			String usernamedir = System.getProperty("user.name");
			String Sys_Download_Path = "C:\\" + "Users\\" + usernamedir + "\\Downloads";
			Log.assertThat(folderPage.DownloadFiledownload(Sys_Download_Path), "RFI Number Verified Successfully", "RFI Number Not Verified Successfully",
					driver);


		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/*
	 * TC_057(Local search an RFI by it's subject.) 1) Verify all the content of
	 * the RFI including status opening the RFI in the search result window. 2)
	 * Try to download associated attachments and verify. 3) Change the status
	 * of the RFI from the search result window and verify in cloud.
	 *
	 *
	 *
	 */

	@Test(priority = 56, enabled = true , description = "Local search an RFI by it's subject.")
	public void Verify_RFI_LocalSearch_With_Subject() throws Throwable {

		try {
			Log.testCaseInfo("TC_057(RFI Project Level Validation):Local search an RFI by it's subject.");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ==============Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = "Prj_" + Generate_Random_Number.generateRandomValue();
			String Subject = "Subject123" + Generate_Random_Number.generateRandomValue1();
			Log.message("Subject verified Sucessfully " + Subject);
			folderPage = projectDashboardPage.CreateProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.RFICreation_withSubject(Subject);
			projectAndFolder_Level_Search.LocalSearchwithAnything(Subject);			
			String usernamedir = System.getProperty("user.name");
			String Sys_Download_Path = "C:\\" + "Users\\" + usernamedir + "\\Downloads";
			Log.assertThat(folderPage.DownloadFiledownload(Sys_Download_Path), "subject Verified Successfully", "Subject Not Verified Successfully",
					driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/*
	 * TC_058(Local search an RFI by it's custom attribute..) 1) Verify all the
	 * content of the RFI including status opening the RFI in the search result
	 * window. 2) Try to download associated attachments and verify. 3) Change
	 * the status of the RFI from the search result window and verify in cloud.
	 * 4) Verify the order of custom attribute from search result window.
	 */

	@Test(priority = 57, enabled = true , description = "Local search an RFI by custom attribute.")
	public void Verify_RFI_LocalSearch_With_customattribute() throws Throwable

	{

		try

		{

			Log.testCaseInfo("TC_058(RFI Project Level Validation):Local search an RFI by it's custom attribute..");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ==============Login with valid UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// =========Create Project Randomly===================================
			String Project_Name = "Prj_" + Generate_Random_Number.generateRandomValue();
			String CustumAttribute = "NewScenario" + Generate_Random_Number.generateRandomValue1();
			Log.message("Custom Attribute Display As: " + CustumAttribute);
			folderPage = projectDashboardPage.createProjectwithCustomAtribute(Project_Name, CustumAttribute);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.RFI_CreationwithcustomAttribute(CustumAttribute);
			projectAndFolder_Level_Search.ProjectManagement();
		    projectAndFolder_Level_Search.LocalSearchwithAnything(CustumAttribute);			
			String usernamedir = System.getProperty("user.name");
			String Sys_Download_Path = "C:\\" + "Users\\" + usernamedir + "\\Downloads";
			Log.assertThat(folderPage.DownloadFiledownload(Sys_Download_Path), "Custom Attribute Verified Successfully", "Custom Attribute Not Verified Successfully",
					driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}

	/*
	 * 1) TC_059 .Verify all the content of the RFI including status opening the
	 * RFI in the search result window. 2) Try to download associated
	 * attachments and verify. 3) Change the status of the RFI from the search
	 * result window and verify in cloud. 4) Verify this with all kind of
	 * status.
	 * 
	 */

	@Test(priority = 58, enabled = true, description = "Local Search the RFI with the status.")
	public void Verify_RFI_LocalSearch_AllStatus() throws Throwable {

		try {
			Log.testCaseInfo("TC_059(RFI Project Level Validation):Local Search RFI by it's Status");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String uName1 = PropertyReader.getProperty("Username2");
			String pWord1 = PropertyReader.getProperty("Password2");
			// String SystemDownloadPath =
			// PropertyReader.getProperty("SystemDownloadPath");
			String RFINumber = Generate_Random_Number.generateRandomValue1();
			String Subject = "Subject123" + Generate_Random_Number.generateRandomValue1();
			// ==============Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);

			// =========Create Project
			// Randomly===================================
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			String Project_Name = "Prj_" + Generate_Random_Number.generateRandomValue();
			folderPage = projectDashboardPage.CreateProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.RFICREATION_New();
			projectAndFolder_Level_Search.ProjectManagement();
			String Open = PropertyReader.getProperty("Status");
			projectAndFolder_Level_Search.AdvanceStatus(Open);
			String usernamedir = System.getProperty("user.name");
			String Sys_Download_Path = "C:\\" + "Users\\" + usernamedir + "\\Downloads";
			folderPage.DownloadFiledownload(Sys_Download_Path);			
			Log.assertThat(projectAndFolder_Level_Search.MainStatus(), "Open,Close,reopen Status Verified Successfully", "Open,Close,reopen Not Verified Successfully",
					driver);

			// open/close/reopen

			// projectAndFolder_Level_Search = new
			// ProjectAndFolder_Level_Search(driver).get();
			// projectAndFolder_Level_Search.Logout();
			// Log.assertThat( projectAndFolder_Level_Search.Logout(), "Logout
			// working Successfully","Logout not working Successfully", driver);

			// ==============Employee Level================================
			/*
			 * String Project_Name1 ="Prj_22842"; projectsLoginPage = new
			 * ProjectsLoginPage(driver).get();
			 * projectDashboardPage=projectsLoginPage.loginWithValidCredential(
			 * uName1,pWord1);
			 * projectDashboardPage.Validate_SelectAProject(Project_Name1) ;
			 * projectAndFolder_Level_Search = new
			 * ProjectAndFolder_Level_Search(driver).get();
			 * projectAndFolder_Level_Search.ProjectManagement();
			 * projectAndFolder_Level_Search.EmployeeStatus();//Responded
			 */
			// projectAndFolder_Level_Search.RFICreation_withSubject(Subject);

			// projectAndFolder_Level_Search.LocalSearchwithAnything(Subject);

			// folderPage.ValidateDownload_File_download(SystemDownloadPath);

			// String Status = PropertyReader.getProperty("Status");
			// projectAndFolder_Level_Search.FilterSearchwithStatus(Status);

			// projectAndFolder_Level_Search.DownloadPDF();

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/*
	 * TC_060(Global Search an RFI with it's RFI number) 1) Verify all the
	 * content of the RFI including status opening the RFI in the search result
	 * window. 2) Try to download associated attachments and verify. 3) Change
	 * the status of the RFI from the search result window and verify in cloud.
	 */

	@Test(priority = 59, enabled = true, description = "Global Search an RFI with it's RFI number")
	public void Verify_RFI_GlobalSearch_with_RFINumber() throws Throwable {

		try {
			Log.testCaseInfo("TC_060(RFI Project Level Validation):Global Search an RFI with it's RFI number");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ==============Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// projectDashboardPage.projectselection();
			// =========Create Project Randomly===================================
			String Project_Name = "Prj_" + Generate_Random_Number.generateRandomValue();
			String rFINumber = Generate_Random_Number.generateRandomValue1();
			Log.message("Subject verified Sucessfully " + rFINumber);
			folderPage = projectDashboardPage.createProjectwithSpecificRFINumber(Project_Name, rFINumber);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.RFICREATION_New();
			//projectAndFolder_Level_Search.SearchRFI_RFINumber(rFINumber);			
			Log.assertThat(projectAndFolder_Level_Search.SearchRFI_RFINumber(rFINumber), "RFI Number Verified Successfully", "RFI Number Not Verified Successfully",
					driver);


			// projectAndFolder_Level_Search.Logout() ;

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/*
	 * TC_061(Global Search an RFI with it's subject ) 1) Verify all the content
	 * of the RFI including status opening the RFI in the search result window.
	 * 2) Try to download associated attachments and verify. 3) Change the
	 * status of the RFI from the search result window and verify in cloud.
	 *
	 */

	@Test(priority = 60, enabled = true , description = "Global Search an RFI By Its Subject")
	public void Verify_RFI_GlobalSearch_By_SubJect() throws Throwable {

		try {
			Log.testCaseInfo("TC_61(RFI Project Level Validation):Global Search an RFI By Its Subject");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ==============Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);

			// =========Create Project
			// Randomly===================================
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			String Sub_Name = "subject_" + Generate_Random_Number.generateRandomValue();
			projectAndFolder_Level_Search.RFICreation_withSubject(Sub_Name);			
			Log.assertThat(projectAndFolder_Level_Search.SearchRFISubject_Global(Sub_Name), "RFI Subject Verified Successfully", "RFI Subject Not Verified Successfully",
					driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/*
	 * TC_062(Global Search RFI by it's custom attribute) 1) Verify all the
	 * content of the RFI including status opening the RFI in the search result
	 * window. 2) Try to download associated attachments and verify. 3) Change
	 * the status of the RFI from the search result window and verify in cloud.
	 * 
	 */

	@Test(priority = 61, enabled = true , description = "Global Search RFI by it's custom attribute.")
	public void Verify_RFI_GlobalSearch_customattribute() throws Throwable {

		try {
			Log.testCaseInfo("TC_062(RFI Project Level Validation):Global Search RFI by it's custom attribute");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ==============Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = "Prj_" + Generate_Random_Number.generateRandomValue();
			String CustumAttribute = "NewScenario" + Generate_Random_Number.generateRandomValue1();
			projectDashboardPage.createProjectwithCustomAtribute(Project_Name, CustumAttribute);

			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.RFI_CreationwithcustomAttribute(CustumAttribute);
			Log.assertThat(projectAndFolder_Level_Search.SearchRFI_Global(CustumAttribute), "Custom Attribute Verified Successfully", "Custom Attribute Not Verified Successfully",
					driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	// =========================Folder level Search ================================================
	//==============================================================================================

	/*
	 * TC_063(Local Search an RFI with it's RFI number.) 1) Verify all the
	 * content of the RFI including status opening the RFI in the search result
	 * window. 2) Try to download associated attachments and verify. 3) Change
	 * the status of the RFI from the search result window and verify in cloud.
	 * 
	 */

	@Test(priority = 62, enabled = true , description = "Local Search an RFI with it's RFI number.-Folder Level")
	public void VerifyRFILocalSearch_With_RFINumber() throws Throwable {
		try {
			Log.testCaseInfo(
					"TC_063:-(RFI Folder Level Search):Local Search an RFI with it's RFI number.- Folder Level");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");

			// ==============Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);

			// =========Create Project
			// Randomly========================================
			String Project_Name = "Prj_" + Generate_Random_Number.MyDate();
			String RFINumber = Generate_Random_Number.generateRandomValue1();
			Log.message("Subject verified Sucessfully :- " + RFINumber);
			folderPage = projectDashboardPage.createProjectwithSpecificRFINumber(Project_Name, RFINumber);

			// =========Create Folder
			// Randomly========================================
			String Foldername = "Folder_" + Generate_Random_Number.generateRandomValue();
			folderPage.New_Folder_Create(Foldername);
			folderPage.Select_Folder(Foldername);
			// ================File Upload without
			// Index===============================
			String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
			String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
			int FileCount = Integer.parseInt(CountOfFilesInFolder);
			folderPage.Upload_WithoutIndex(FolderPath, FileCount);
			// =======Owner Level Validation========================
			viewerScreenPage = new ViewerScreenPage(driver).get();
			viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);
			viewerScreenPage.Image1();
			viewerScreenPage.RFI_TestDataCreation_ownerLevel2();
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.RFINumberWithLocalSearch(RFINumber);
			projectAndFolder_Level_Search.ProjectManagement();
			String usernamedir = System.getProperty("user.name");
			String Sys_Download_Path = "C:\\" + "Users\\" + usernamedir + "\\Downloads";
			folderPage.DownloadFiledownload(Sys_Download_Path);
			// Log.assertThat(
			// folderPage.DownloadFiledownload(Sys_Download_Path), "file Is
			// downloading Downloading","File Is not Downloading Successfully",
			// driver);
			viewerScreenPage = new ViewerScreenPage(driver).get();
			// viewerScreenPage.ClosePOPUP();
			Log.assertThat(viewerScreenPage.Logout(), "Logout working Successfully", "Logout not working Successfully",
					driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/*
	 * TC_064(Local search an RFI by it's subject.) 1) Verify all the content of
	 * the RFI including status opening the RFI in the search result window. 2)
	 * Try to download associated attachments and verify. 3) Change the status
	 * of the RFI from the search result window and verify in cloud.
	 */

	@Test(priority = 63, enabled = true  , description = "Tc_064:---Local search an RFI by it's subject.- Folder Level")
	public void VerifyRFI_LocalSearch_With_Subject() throws Throwable {

		try {
			Log.testCaseInfo("TC_064(RFI Folder Level Validation):Local search an RFI by it's subject.- Folder Level");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ==============Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);

			// =========Create Project
			// Randomly===================================
			String Project_Name = "Prj_" + Generate_Random_Number.generateRandomValue();
			String subject = "Cloud subject" + Generate_Random_Number.generateRandomValue1();
			Log.message("Subject verified Sucessfully :- " + subject);
			folderPage = projectDashboardPage.createProjectwithSubject(Project_Name, subject);
			// =========Create Folder
			// Randomly===================================
			String Foldername = "Folder_" + Generate_Random_Number.generateRandomValue();
			folderPage.New_Folder_Create(Foldername);
			folderPage.Select_Folder(Foldername);
			// ================File Upload without Index=====================
			String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
			String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
			int FileCount = Integer.parseInt(CountOfFilesInFolder);
			folderPage.Upload_WithoutIndex(FolderPath, FileCount);
			// =======Owner Level Validation========================
			viewerScreenPage = new ViewerScreenPage(driver).get();
			viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);
			viewerScreenPage.Image1();
			viewerScreenPage.RFITestDataCreationLevel(subject);
			// viewerScreenPage.swichToMainWindow();
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.SearchRFISubject_Module(subject);
			String usernamedir = System.getProperty("user.name");
			String Sys_Download_Path = "C:\\" + "Users\\" + usernamedir + "\\Downloads";
			folderPage.DownloadFiledownload(Sys_Download_Path);
			// Log.assertThat(
			// folderPage.DownloadFiledownload(Sys_Download_Path), "file Is
			// downloading Downloading","File Is not Downloading Successfully",
			// driver);
			viewerScreenPage = new ViewerScreenPage(driver).get();
			// viewerScreenPage.ClosePOPUP();
			Log.assertThat(viewerScreenPage.Logout(), "Logout working Successfully", "Logout not working Successfully",
					driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}

	/*
	 * TC_065(Local search an RFI by it's custom attribute..) 1) Verify all the
	 * content of the RFI including status opening the RFI in the search result
	 * window. 2) Try to download associated attachments and verify. 3) Change
	 * the status of the RFI from the search result window and verify in cloud.
	 * 4) Verify the order of custom attribute from search result window.
	 * 
	 */

	@Test(priority = 64, enabled = true , description = "TC_065:--Local search an RFI by custom attribute.")
	public void VerifyRFI_LocalSearch_With_customattribute() throws Throwable {

		try {
			Log.testCaseInfo("TC_065(RFI Folder Level Search):Local search an RFI by it's Custom Attribute..");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ==============Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// =========Create Project
			// Randomly===================================
			String Project_Name = "Project_" + Generate_Random_Number.generateRandomValue();
			String CustumAttribute = "NewScenario" + Generate_Random_Number.generateRandomValue1();
			Log.message("Custom Attribute Display As: " + CustumAttribute);
			folderPage = projectDashboardPage.createProjectwithCustomAtribute(Project_Name, CustumAttribute);
			// =========Create Folder
			// Randomly===================================
			String Foldername = "Folder_" + Generate_Random_Number.generateRandomValue();
			folderPage.New_Folder_Create(Foldername);
			folderPage.Select_Folder(Foldername);
			// ================File Upload without
			// Index=========================
			String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
			String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
			int FileCount = Integer.parseInt(CountOfFilesInFolder);
			folderPage.Upload_WithoutIndex(FolderPath, FileCount);
			// =======Owner Level Validation====================================
			viewerScreenPage = new ViewerScreenPage(driver).get();
			viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);
			viewerScreenPage.Image1();
			viewerScreenPage.RFI_TestDataCreationLevel(CustumAttribute);
			// viewerScreenPage.swichToMainWindow();
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.LocalSearchwithAnything(CustumAttribute);
			String usernamedir = System.getProperty("user.name");
			String Sys_Download_Path = "C:\\" + "Users\\" + usernamedir + "\\Downloads";
			folderPage.DownloadFiledownload(Sys_Download_Path);
			viewerScreenPage = new ViewerScreenPage(driver).get();
			// viewerScreenPage.ClosePOPUP();
			Log.assertThat(viewerScreenPage.Logout(), "Logout working Successfully", "Logout not working Successfully",
					driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/*
	 * 1)TC_066 Verify all the content of the RFI including status opening the
	 * RFI in the search result window. 2) Try to download associated
	 * attachments and verify. 3) Change the status of the RFI from the search
	 * result window and verify in cloud. 4) Verify this with all kind of
	 * status.
	 * 
	 */

	@Test(priority = 65, enabled = true , description = "Local Search the RFI with the status.")
	public void VerifyRFI_LocalSearch_AllStatus() throws Throwable {

		try

		{
			Log.testCaseInfo("TC_066(RFI Folder Level Validation):Local Search RFI by it's Status");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String RFINumber = Generate_Random_Number.generateRandomValue1();
			String Subject = "Subject123" + Generate_Random_Number.generateRandomValue1();
			// ==============Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);

			// =========Create Project
			// Randomly===================================
			String Project_Name = "Prj_" + Generate_Random_Number.generateRandomValue();
			folderPage = projectDashboardPage.CreateProject(Project_Name);

			// =========Create Folder
			// Randomly===================================
			String Foldername = "Folder_" + Generate_Random_Number.generateRandomValue();
			folderPage.New_Folder_Create(Foldername);
			folderPage.Select_Folder(Foldername);
			// ================File Upload without Index=====================
			String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
			// Log.message(FolderPath);
			String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
			int FileCount = Integer.parseInt(CountOfFilesInFolder);
			folderPage.Upload_WithoutIndex(FolderPath, FileCount);

			// =======Owner Level Validation========================
			viewerScreenPage = new ViewerScreenPage(driver).get();
			viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);
			viewerScreenPage.Image1();
			viewerScreenPage.RFI_TestDataCreation_ownerLevel_Status();
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			String Open = PropertyReader.getProperty("Status");
			projectAndFolder_Level_Search.AdvanceStatus(Open);
			String usernamedir = System.getProperty("user.name");
			String Sys_Download_Path = "C:\\" + "Users\\" + usernamedir + "\\Downloads";
			folderPage.DownloadFiledownload(Sys_Download_Path);
			// viewerScreenPage.ClosePOPUP();
			//projectAndFolder_Level_Search.MainStatus();
			Log.assertThat(projectAndFolder_Level_Search.MainStatus(),"All Status verified  Successfully", "All Status  Not verified Successfully", driver);
			

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	// ========================================================== Extra test cases=======================
	
	/*
	 * TC_067(Global Search an RFI with it's subject ) 1) Verify all the content
	 * of the RFI including status opening the RFI in the search result window.
	 * 2) Try to download associated attachments and verify. 3) Change the
	 * status of the RFI from the search result window and verify in cloud( with Old Data).
	 *
	 */

	@Test(priority =66, enabled = true , description = "Global Search an RFI By Its Subject with Old Data")
	public void Verify_RFI_GlobalSearch_By_SubJect_with_OldData() throws Throwable {

		try {
			Log.testCaseInfo("TC_67(RFI Project Level Validation):Global Search an RFI By Its Subject with Old Data");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ==============Login with valid UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// =========Create Project Randomly===================================
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();				
			Log.assertThat(projectAndFolder_Level_Search.SearchRFISubject_Global_Olddata(), "RFI Subject verification Completed Successfully","RFI Subject  verification Not Completed Successfully", driver);


		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/* TC_068:-----Verify user is able to do Photo Attribute filter search By Uploded BY (Filter search by Uploded by) scripted by Ranjan*/

	@Test(priority = 67, enabled = true , description = "Verify user is able to do Photo Attribute filter Search Uploded By")
	public void PhotoAttribute_Uploded_By() throws Throwable {

		try {
			Log.testCaseInfo("TC_068 :-Verify user is able to do Photo Attribute filter search Uploded BY ");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			//String Description = PropertyReader.getProperty("Description1");
			// ===========Login with valid UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			//
			folderPage.Select_Gallery_Folder();
			// Select Album
			String Album_Name = "Static_Photo";
			search_Testcases = new Search_Testcases(driver).get();
			Log.assertThat(search_Testcases.Verify_SelectAn_Album(Album_Name),"Album Name verified  Successfully", "Album Name  Not verified Successfully", driver);
			
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	
	/*TC_069:- Verify user is able to do shared Project filter Search Submittal number (FilterSearch)*/
	 

	@Test(priority = 68, enabled = true , description = "Verify user is able to do shared Project filter Search Submittal number (FilterSearch)")
	public void Shared_Project_FilterSearch_SubmitalNumber() throws Throwable {

		try {
			Log.testCaseInfo("TC_069 :-Verify user is able to do shared Project filter Search Submittal number (FilterSearch)");
			String uName = PropertyReader.getProperty("GuestUserName");
			String pWord = PropertyReader.getProperty("GuestUserPassword");
			String Submittal_Number = Generate_Random_Number.generateRandomValue();
			String Submittal_Name = "SubName_" + Generate_Random_Number.generateRandomValue();
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement_Submittal();
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Create_Submital_AndValidate(Submittal_Number, Submittal_Name);
			//projectAndFolder_Level_Search.ProjectManagement_Submittal();
			search_Testcases = new Search_Testcases(driver).get();
			String Flag = "Search_SubmittalNumber";
			Log.assertThat(search_Testcases.AdvanceSearch_Submittal(Submittal_Number, Flag),"Submittal Number verified  Successfully", "Submittal Number Not verified Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	
	 /* TC_070:- Verify user is able to do shared Project filter Search Submittal Name (Filter Search)*/
	 

	@Test(priority = 69, enabled = true, description = "Verify user is able to do shared Project filter Search Submittal Name (Filter Search)")
	public void ShareProject_SubmitalfilterSearch_Name() throws Throwable {

		try {
			Log.testCaseInfo("TC_070 :-Verify user is able to do shared Project filter Search Submittal Name (Filter Search)");
			String uName = PropertyReader.getProperty("GuestUserName");
			String pWord = PropertyReader.getProperty("GuestUserPassword");
			String Submittal_Number = "SubId_" + Generate_Random_Number.generateRandomValue();
			String Submittal_Name = "SubName_" + Generate_Random_Number.generateRandomValue();

			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();

			projectAndFolder_Level_Search.ProjectManagement_Submittal();
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Create_Submital_AndValidate(Submittal_Number, Submittal_Name);
			projectAndFolder_Level_Search.ProjectManagement_Submittal();
			search_Testcases = new Search_Testcases(driver).get();
			String Flag = "Search_SubmittalName";
			Log.assertThat(search_Testcases.AdvanceSearch_Submittal(Submittal_Name, Flag),"Submittal Name verified  Successfully", "Submittal Name Not verified Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	
	  /** TC_071:- Verify user is able to do shared Project filter Search Submittal Type (Filter Search)*/
	 

	@Test(priority = 70, enabled = true, description = "Verify user is able to do shared Project filter Search Submittal Type (Filter Search)")
	public void SharedProject_SubmitalfilterSearch_Type() throws Throwable {

		try {
			Log.testCaseInfo("TC_071 :-Verify user is able to do shared Project filter Search Submittal Type (Filter Search)");
			String uName = PropertyReader.getProperty("GuestUserName");
			String pWord = PropertyReader.getProperty("GuestUserPassword");
			String Submittal_Number = "SubId_" + Generate_Random_Number.generateRandomValue();
			String Submittal_Name = "SubName_" + Generate_Random_Number.generateRandomValue();
			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement_Submittal();
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Create_Submital_AndValidate(Submittal_Number, Submittal_Name);
			String CommonValue = PropertyReader.getProperty("SubmittalType");
			String Flag = "Search_SubmittalType";
			Log.assertThat(search_Testcases.AdvanceSearch_Submittal(CommonValue, Flag),"Submittal type verified  Successfully", "Submittal type  Not verified Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	
	   /** TC_072:- Verify user is able to do shared Project filter Search Submittal Status (Filter Search)*/
	 

	@Test(priority = 71, enabled = true, description = "Verify user is able to do shared Project filter Search Submittal Status (Filter Search)")
	public void SharedProject_SubmitalfilterSearch_Status() throws Throwable {

		try {
			Log.testCaseInfo("TC_072 :-Verify user is able to do shared Project filter Search Submittal Status (Filter Search)");
			String uName = PropertyReader.getProperty("GuestUserName");
			String pWord = PropertyReader.getProperty("GuestUserPassword");
			String Submittal_Number = "SubId_" + Generate_Random_Number.generateRandomValue();
			String Submittal_Name = "SubName_" + Generate_Random_Number.generateRandomValue();
			// ===========Login with valid UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement_Submittal();
			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Create_Submital_AndValidate(Submittal_Number, Submittal_Name);
			String CommonValue = PropertyReader.getProperty("Status");
			String Flag = "Search_Status";
			Log.assertThat(search_Testcases.AdvanceSearch_Submittal(CommonValue, Flag),"Submittal Status verified  Successfully", "Submittal Status  Not verified Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}
	
	
	

	
	   /* * TC_073:- Verify user is able to filter Search Submittal Due Date (Filter Search)
	 

	@Test(priority = 72, enabled = true, description = "Verify user is able to filter Search Submittal Due Date (Filter Search)")
	public void SubmitalfilterSearch_DueDate() throws Throwable {

		try {
			Log.testCaseInfo("TC_073 :-Verify user is able to filter Search Submittal Due Date (Filter Search)");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String Submittal_Number = "SubId_" + Generate_Random_Number.generateRandomValue();
			String Submittal_Name = "SubName_" + Generate_Random_Number.generateRandomValue();

			// ===========Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			String Project_Name = PropertyReader.getProperty("StaticProject");
			projectDashboardPage.ValidateSelectProject(Project_Name);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement_Submittal();

			search_Testcases = new Search_Testcases(driver).get();
			search_Testcases.Create_Submital_AndValidate(Submittal_Number, Submittal_Name);
			String CommonValue = PropertyReader.getProperty("Status");
			String Flag = "Search_DueDate";
			search_Testcases.AdvanceSearch_Submittal(CommonValue, Flag);
			viewerScreenPage = new ViewerScreenPage(driver).get();
			Log.assertThat(viewerScreenPage.Logout(), "Logout working Successfully", "Logout not working Successfully",
					driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}*/
	
	
	
	  /** TC_074:- Verify user is able to do Upload with Index ,filter Search Sheet Number (Filter Search)*/
	 

		@Test(priority = 73, enabled = true , description = "Verify user is able to do Upload with Index ,filter Search Sheet Number (Filter Search)")
		public void UploadWithindex_SheetNumber() throws Throwable {

			try {
				Log.testCaseInfo("TC_074 :-Verify user is able to do Upload with Index ,filter Search Sheet Number (Filter Search)");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");				
				
				// Login with valid UserID/PassWord
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);

				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);

				// select folder
				String Foldername = "RFI_UploadWithIndex";
				folderPage.Select_Folder(Foldername);				
				search_Testcases = new Search_Testcases(driver).get();				
				String CommonValue = PropertyReader.getProperty("SheetNumber");
				String Flag = "Search_SheetNumber";				
				Log.assertThat(search_Testcases.AdvanceSearch_Uploadwithindex(CommonValue, Flag),"Sheet Number verified  Successfully", "Sheet number  Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		
		/** TC_075:- Verify user is able to do Upload with Index ,filter Search SheetName/Description (Filter Search)*/		 

		@Test(priority = 74, enabled = true, description = " Verify user is able to do Upload with Index ,filter Search SheetName/Description (Filter Search)")
		public void UploadWithindex_SheetName_Description() throws Throwable {

			try {
				Log.testCaseInfo("TC_075 :- Verify user is able to do Upload with Index ,filter Search SheetName/Description (Filter Search)");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");				
				
				// Login with valid UserID/PassWord
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);

				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);

				// select folder
				String Foldername = "RFI_UploadWithIndex";
				folderPage.Select_Folder(Foldername);				
				search_Testcases = new Search_Testcases(driver).get();				
				String CommonValue = PropertyReader.getProperty("SheetName");
				String Flag = "Search_SheetName";				
				Log.assertThat(search_Testcases.AdvanceSearch_Uploadwithindex(CommonValue, Flag),"Sheet Number verified Successfully", "Sheet number  Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		/** TC_076:- Verify user is able to do Upload with Index ,filter Search Revision Name (Filter Search)*/		 

		@Test(priority = 75, enabled = true , description = "Verify user is able to do Upload with Index ,filter Search Revision Name (Filter Search)")
		public void UploadWithindex_revision () throws Throwable {

			try {
				Log.testCaseInfo("TC_076 :- Verify user is able to do Upload with Index ,filter Search Revision Name (Filter Search)");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");				
				
				// Login with valid UserID/PassWord
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);

				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);

				// select folder
				String Foldername = "RFI_UploadWithIndex";
				folderPage.Select_Folder(Foldername);				
				search_Testcases = new Search_Testcases(driver).get();				
				String CommonValue = PropertyReader.getProperty("RevisionName");
				String Flag = "Search_Revision";	
				//search_Testcases.AdvanceSearch_Uploadwithindex(CommonValue, Flag);
				Log.assertThat(search_Testcases.AdvanceSearch_Uploadwithindex(CommonValue, Flag),"Revision Name verified  Successfully", "Revision Name  Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		
		/** TC_077:- Verify user is able to do Upload with Index ,filter Search Discipline (Filter Search)*/
		 

		@Test(priority = 76, enabled = true , description = "Verify user is able to do Upload with Index ,filter Search Discipline (Filter Search)")
		public void UploadWithindex_Discipline () throws Throwable {

			try {
				Log.testCaseInfo("TC_077 :- Verify user is able to do Upload with Index ,filter Search Discipline (Filter Search)");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");				
				
				// Login with valid UserID/PassWord
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);

				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);

				// select folder
				String Foldername = "RFI_UploadWithIndex";
				folderPage.Select_Folder(Foldername);				
				search_Testcases = new Search_Testcases(driver).get();				
				String CommonValue = PropertyReader.getProperty("Discipline");
				String Flag = "Search_Discipline";	
				//search_Testcases.AdvanceSearch_Uploadwithindex(CommonValue, Flag);
				Log.assertThat(search_Testcases.AdvanceSearch_Uploadwithindex(CommonValue, Flag),"Discipline verified  Successfully", "Discipline  Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}	
		
		/** TC_078:- Verify user is able to do Upload with Index ,Revision Date (Filter Search)*/		 

		@Test(priority = 77, enabled = true , description = "Verify user is able to do Upload with Index ,Revision Date (Filter Search)")
		public void UploadWithindex_RevisionDate () throws Throwable {

			try {
				Log.testCaseInfo("TC_078 :- Verify user is able to do Upload with Index ,Revision Date (Filter Search)");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");				
				
				// Login with valid UserID/PassWord
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);

				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);

				// select folder
				String Foldername = "RFI_UploadWithIndex";
				folderPage.Select_Folder(Foldername);				
				search_Testcases = new Search_Testcases(driver).get();				
				String CommonValue = PropertyReader.getProperty("Discipline");
				String Flag = "Search_RevisionDate";	
				//search_Testcases.AdvanceSearch_Uploadwithindex(CommonValue, Flag);
				Log.assertThat(search_Testcases.AdvanceSearch_Uploadwithindex(CommonValue, Flag),"revision Date verified  Successfully", "revision Date  Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		
		
		/* TC_079:- Verify user is able to do shared project Punch,(Module Search)*/

		@Test(priority = 78, enabled = true , description = "Verify user is able to do Punch,(Module search)")
		public void Sharedprojec_PunchSearch_Module() throws Throwable {

			try {
				Log.testCaseInfo("TC_079 :-Verify user is able to do Punch,(Module Search)");
				String uName = PropertyReader.getProperty("GuestUserName");
				String pWord = PropertyReader.getProperty("GuestUserPassword");
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				// =========File Upload without Index Randomly===================================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement_Punch();
				viewerScreenPage = new ViewerScreenPage(driver).get();
				viewerScreenPage.PunchCreation();
				search_Testcases = new Search_Testcases(driver).get();				
				Log.assertThat(search_Testcases.Searchscenario_punch(), "Punch Search verified Successfully","Punch Search Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}

		
		
		/*TC_080:- Verify user is able to do shared project Punch,(Filter Search- Description)*/

		@Test(priority = 79, enabled = true, description = "Verify user is able to do shared project Punch,(Filter Search- Description)")
		public void SharedProject_FilterSearch_Description() throws Throwable {

			try {
				Log.testCaseInfo("TC_080 :-Verify user is able to do shared project Punch,(Filter Search- Description)");
				String uName = PropertyReader.getProperty("GuestUserName");
				String pWord = PropertyReader.getProperty("GuestUserPassword");
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				// =========File Upload without Index
				// Randomly===================================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement_Punch();
				viewerScreenPage = new ViewerScreenPage(driver).get();
				viewerScreenPage.PunchCreation();
				search_Testcases = new Search_Testcases(driver).get();
				String Description = PropertyReader.getProperty("Punch_Subject");
				String Flag = "Search_Description";
				Log.assertThat(search_Testcases.AdvanceSearch_Punch(Description, Flag), "Punch Description verified  Successfully","Punch Assignee  Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		/*TC_081:- Verify user is able to do shared project Punch,(Filter Search- Creator)*/

		@Test(priority = 80, enabled = true , description = "Verify user is able to do shared project Punch,(Filter Search- Creator)")
		public void SharedProject_FilterSearch_Creator() throws Throwable {

			try {
				Log.testCaseInfo("TC_081:-Verify user is able to do shared project Punch,(Filter Search- Creator)");
				String uName = PropertyReader.getProperty("GuestUserName");
				String pWord = PropertyReader.getProperty("GuestUserPassword");
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				// =========File Upload without Index Randomly===================================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement_Punch();
				viewerScreenPage = new ViewerScreenPage(driver).get();
				viewerScreenPage.PunchCreation();
				search_Testcases = new Search_Testcases(driver).get();
				String Creator = PropertyReader.getProperty("Creator");
				String Flag = "Search_Creator";
				Log.assertThat(search_Testcases.AdvanceSearch_Punch(Creator, Flag), "Punch Creator verified  Successfully","Punch Creator  Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}
		}
		
		/*TC_082:- Verify user is able to do shared project Punch,(Filter Search- Assignee)*/

		@Test(priority = 81, enabled = true, description = "Verify user is able to do shared project Punch,(Filter Search- Assignee)")
		public void SharedProject_FilterSearch_Assignee() throws Throwable {

			try {
				Log.testCaseInfo("TC_082:-Verify user is able to do shared project Punch,(Filter Search- Assignee)");
				String uName = PropertyReader.getProperty("GuestUserName");
				String pWord = PropertyReader.getProperty("GuestUserPassword");
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				// =========File Upload without Index
				// Randomly===================================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement_Punch();
				viewerScreenPage = new ViewerScreenPage(driver).get();
				viewerScreenPage.PunchCreation();
				search_Testcases = new Search_Testcases(driver).get();
				String Assigne = PropertyReader.getProperty("SelectUserAssignTo_Email");
				String Flag = "Search_Assignee";
				Log.assertThat(search_Testcases.AdvanceSearch_Punch(Assigne, Flag), "Punch Assignee verified  Successfully","Punch Assignee  Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}

		/*TC_083:- Verify user is able to do shared project Punch,(Filter Search- Status)*/

		@Test(priority = 82, enabled = true , description = "Verify user is able to do shared project Punch,(Filter Search- Status)")
		public void SharedProject_FilterSearch_Status() throws Throwable {

			try {
				Log.testCaseInfo("TC_083:-Verify user is able to do shared project Punch,(Filter Search- Status)");
				String uName = PropertyReader.getProperty("GuestUserName");
				String pWord = PropertyReader.getProperty("GuestUserPassword");
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				// =========File Upload without Index Randomly===================================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement_Punch();
				viewerScreenPage = new ViewerScreenPage(driver).get();
				viewerScreenPage.PunchCreation();
				search_Testcases = new Search_Testcases(driver).get();
				String status1 = PropertyReader.getProperty("Status");
				String Flag = "Search_Status";
				Log.assertThat(search_Testcases.AdvanceSearch_Punch(status1, Flag), "Punch Status verified  Successfully","Punch Status  Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		
		// Content Search Related Test cases		
		
		/** TC_084:- Verify user is able to do Upload with Index - File  words Content search*/		 

		@Test(priority = 83, enabled = true  , description = "Verify user is able to do Upload with Index - File  words Content search")
		public void ContentSearch_FileContentSearch () throws Throwable {

			try {
				Log.testCaseInfo("TC_084 :- Verify user is able to do Upload with Index - File  words Content search");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");				
				
				// Login with valid UserID/PassWord
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);

	
	            // Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);

				// select folder
				String Foldername = "RFI_UploadWithIndex";
				folderPage.Select_Folder(Foldername);
				
				// Search related Cases 
				search_Testcases = new Search_Testcases(driver).get();
				String text = PropertyReader.getProperty("Context_Text");
				Log.assertThat(search_Testcases.SearchContent(text), "Text verified  Successfully","Text Not verified Successfully", driver);
				
				
			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		/** TC_085:- Verify user is able to do Upload with Index - Multiple-File  words Content search*/		 

		@Test(priority = 84, enabled = true  , description = "TC_084:- Verify user is able to do Upload with Index - Multiple-File  words Content search")
		public void ContentSearch_MultipleFileContentSearch () throws Throwable {

			try {
				Log.testCaseInfo("TC_085 :-TC_085:- Verify user is able to do Upload with Index - Multiple-File  words Content search");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");				
				
				// Login with valid UserID/PassWord
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);

				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);

				// select folder
				String Foldername = "RFI_MultiplePage";
				folderPage.Select_Folder(Foldername);
				
				// Search related Cases 
				search_Testcases = new Search_Testcases(driver).get();
				String text = PropertyReader.getProperty("Content_Text_Multiple");
				//search_Testcases.SearchContent(text);
				Log.assertThat(search_Testcases.multipleSearchContent(text), "file verified  Successfully","file Not verified Successfully", driver);
				
				
			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		
	//================= Testcases with old data	=========================================
		
		 /*
		 * TC_086:-----Verify user is able to do project search (Module) By: Scripted Ranjan
		 */

		@Test(priority = 85, enabled = true, description = "Verify user is able to do project search (Module Search) with Old Data")
		public void ProjectName_ModuleSearch_oldData() throws Throwable {

			try {
				Log.testCaseInfo("TC_086:-Verify user is able to do project search (Module Search)");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				// ==============Login with valid UserID/PassWord=============
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// =========Create Proj Randomly===================================
				//String Project_Name = PropertyReader.getProperty("StaticProject");
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				search_Testcases = new Search_Testcases(driver).get();
				String Flag = "Project_Module";
				String Project = search_Testcases.gettext_function(Flag);	
				search_Testcases.Searchscenario(Project);
				Log.assertThat(search_Testcases.Searchprojectvalidation(Project), "Project Name  verification   completed Successfully","Project Name verification Not Completed Successfully", driver);
				
				
			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		/* TC_087:-----Verify user is able to do project search (Filter Search By ProjectName with Old Data) : scripted by Ranjan*/

		@Test(priority = 86, enabled = true, description = "Verify user is able to do filter Search -Project Name with Old Data")
		public void FilterSearch_ProjectName_olddata() throws Throwable {

			try {
				Log.testCaseInfo("TC_087 :-Verify user is able to do filter Search -Project Name With Old Data");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				search_Testcases = new Search_Testcases(driver).get();
				String Flag = "Project_Module";
				String Project = search_Testcases.gettext_function(Flag);	
				String Flag1 = "Search_ProjectName";
				Log.assertThat(search_Testcases.AdvanceSearch_Project(Project, Flag1),"Project Name  verified  Successfully", "Project Name Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);
			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}

		/* TC_088:-----Verify user is able to Search by Folder name (Module search) with Old data): scripted by Ranjan */

		@Test(priority = 87, enabled = true, description = "Verify user is able to Search by Folder name (Module search)with Old Data)")
		public void FolderName_ModuleSearch_olddata() throws Throwable {

			try {
				Log.testCaseInfo("TC_088:-Verify user is able to do project search (Filter Search By folder Name with Old Data)");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String Foldername = "Folder_" + Generate_Random_Number.generateRandomValue();
				// ==============Login with valid
				// UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// =========Create
				// ProjectRandomly===================================
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				search_Testcases = new Search_Testcases(driver).get();
				String Flag = "folderName_old";
				String FolderNm = search_Testcases.gettext_function(Flag);				
				search_Testcases.Searchscenario(FolderNm);
				Log.assertThat(search_Testcases.SearchFoldervalidation(FolderNm)," folder Name verified  Successfully", "Folder Name Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);
			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}	
		
		/*TC_089:---Verify user is able to do File search with old data(Module ):scripted byRanjan */

		@Test(priority =88, enabled = true, description = "Verify user is able to do  File search with Old Data (Module )")
		public void FileName_ModuleSearch_With_OldData() throws Throwable {

			try {
				Log.testCaseInfo("TC_089:-Verify user is able to do File search with old Data (Module)");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String Foldername = "Folder_" + Generate_Random_Number.generateRandomValue();
				String Filename = PropertyReader.getProperty("FileName");
				// ==============Login with valid UserID/PassWord======
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// =========Create Project Randomly======================
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				
				folderPage = new FolderPage(driver).get();
				String folder_Name = "RFI_UploadWithIndex";
				folderPage.Select_Folder(folder_Name);;
				search_Testcases = new Search_Testcases(driver).get();				
				String Flag = "FileName_old";
				String FileNm = search_Testcases.gettext_function(Flag);				
				search_Testcases.Searchscenario(FileNm);
				Log.assertThat(search_Testcases.SearchFilevalidation(FileNm), "File Name verified  Successfully","File Name verified Not verification Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);
			} finally {
				Log.endTestCase();
				driver.quit();
			}
		}
		
		/* TC_090:---Verify user is able to do Album (Module search) with old Data-scripted by Ranjan*/

		@Test(priority = 89, enabled = true, description = "Verify user is able to do Album (Module) with Old Data")
		public void Album_ModuleSearch_OldData() throws Throwable {

			try {
				Log.testCaseInfo("TC_090:-Verify user is able to do Album (Module search) with old Data");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");			
				// ==============Login with valid UserID/PassWord========
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// =========Create Projec Randomly========================
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				folderPage = new FolderPage(driver).get();
				folderPage.Select_Gallery_Folder();
				gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
				search_Testcases = new Search_Testcases(driver).get();
				String Albumname= "Static_Photo";
				search_Testcases.Searchscenario(Albumname);
				Log.assertThat(search_Testcases.Searchfolderphotovalidation_1(Albumname), "Album Name verified  Successfully","Album Name verified Not verification Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);
				
			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		 /* TC_091:---Verify user is able to do Photo Search with old data (Module)-scripted by Ranjan*/

		@Test(priority = 90, enabled = true, description = "Verify user is able to do Photo with old data (Module search")
		public void photofilterSearch_olddata() throws Throwable {

			try {
				Log.testCaseInfo("TC_091:-Verify user is able to do Photo search with old data(Module search)");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String photo = PropertyReader.getProperty("photoName1");				
				
				// ==============Login with valid
				// UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				// ================File Upload without Index
				// Randomly===================================
				folderPage = new FolderPage(driver).get();
				folderPage.Select_Gallery_Folder();
				gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
				search_Testcases = new Search_Testcases(driver).get();
				folderPage = new FolderPage(driver).get();
				folderPage.Select_Gallery_Folder();
				gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
				//search_Testcases = new Search_Testcases(driver).get();
				String Albumname= "Static_Photo";
				gallery_Photo_Page.Verify_SelectAn_Album(Albumname);
				search_Testcases.Searchscenario(photo);
				Log.assertThat(search_Testcases.Searchfolderphotovalidation(), "photo file Name verified  Successfully","Photo File Name verified Not verification Successfully", driver);
				 

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);
			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		
		/* TC_092:- Verify user is able to do Photos search (Filter with building )-scripted by Ranjan*/

		@Test(priority = 91 , enabled = true   , description = "Verify user is able to do  Photos search with old data (Filter with building)")
		public void PhotoSearch_building() throws Throwable {

			try {
				Log.testCaseInfo("TC_092:-Verify user is able to do Photo Search with old data(Filter search with building )");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				String photolocation = PropertyReader.getProperty("Upload_PhotoFile");
				String Build = PropertyReader.getProperty("Building");
				String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
				int FileCount = Integer.parseInt(CountOfFilesInFolder);
				
				// ==============Login with valid
				// UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				// ================File Upload without Index
				// Randomly===================================
				folderPage = new FolderPage(driver).get();
				folderPage.Select_Gallery_Folder();
				gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
				String Albumname= "Static_Photo";
				gallery_Photo_Page.Verify_SelectAn_Album(Albumname);
				search_Testcases = new Search_Testcases(driver).get();
				search_Testcases.AdvanceSearch_building(Build);
				Log.assertThat(search_Testcases.SearchFilevalidation_building(Build), "Building verified Successfully","Building Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		
		/* TC_093:- Verify user is able to do Punch,(Module)*/

		@Test(priority = 92, enabled = true , description = "Verify user is able to do Punch with old data,(Module search)")
		public void PunchSearch_Module_olddata() throws Throwable {

			try {
				Log.testCaseInfo("TC_093 :-Verify user is able to do Punch with old data ,(Module Search)");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				// ===========Login with valid
				// UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				// =========File Upload without Index
				// Randomly===================================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement_Punch();
				viewerScreenPage = new ViewerScreenPage(driver).get();				
				search_Testcases = new Search_Testcases(driver).get();				
				Log.assertThat(search_Testcases.Searchscenario_punch(), "Punch Search verified Successfully","Punch Search Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}

		/*TC_094:- Verify user is able to do Punch with old Data,(Filter Search)*/

		@Test(priority = 93, enabled = true, description = "Verify user is able to do Punch with old data,(Filter Search)")
		public void PunchSearch_Filter_Olddata() throws Throwable {

			try {
				Log.testCaseInfo("TC_094 :-Verify user is able to do Punch with old data,(Filter Search)");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				// ===========Login with valid
				// UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				// =========File Upload without Index
				// Randomly===================================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement_Punch();
				viewerScreenPage = new ViewerScreenPage(driver).get();				
				search_Testcases = new Search_Testcases(driver).get();
				Log.assertThat(search_Testcases.AdvanceSearch_Description(),"Punch Advance Search Description verified  Successfully","Punch Advance Search Description Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		/* TC_095:- Verify user is able to do RFI with old Data,(MODULE Search)*/

		@Test(priority = 94, enabled = true , description = "Verify user is able to do RFI(Module Search) with Old data")
		public void RFISearch_Module_with_OldData() throws Throwable {

			try {
				Log.testCaseInfo("TC_095 :-Verify user is able to do RFI(Module Search) with old data");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();				
				search_Testcases = new Search_Testcases(driver).get();
				Log.assertThat(search_Testcases.Searchscenario_RFI(), "RFI Description verified  Successfully","RFI Description Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		/*TC_096:- Verify user is able to Search RFI with RFI Number (Filter Search): Scripted By ranjan*/

		@Test(priority = 95, enabled = true, description = "Verify user is able to Search RFI with RFI Number with old data(Filter Search)")
		public void RFISearch_Filter_olddata() throws Throwable {

			try {
				Log.testCaseInfo("TC_096 :- Verify user is able to Search RFI with RFI Numberwith old data(Filter Search)");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				// ===========Login with valid
				// UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();
				//projectAndFolder_Level_Search.RFICREATION_New();
				search_Testcases = new Search_Testcases(driver).get();
				Log.assertThat(search_Testcases.RFI_AdvanceSearchWithRFINumber(),"RFI Advance Search Number verified  Successfully","RFI Advance Search Number Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		
		/* TC_097:- Verify user is able to Search RFI with RFI Due Date with Old Data (Filter Search)* Scripted bY RANJAN*/

		@Test(priority = 96, enabled = true, description = "Verify user is able to Search RFI with RFI Due Date with old Data (Filter Search)")
		public void RFIFilterSearch_DueDate_Olddata() throws Throwable {

			try {
				Log.testCaseInfo("TC_097 :-Verify user is able to Search RFI with RFI Due Date (Filter Search)");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();
				//projectAndFolder_Level_Search.RFICREATION_New();
				search_Testcases = new Search_Testcases(driver).get();
				Log.assertThat(search_Testcases.RFI_AdvanceSearchWithRFIDueDate(), "RFI Due Date verified  Successfully","RFI Due Date Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}

		/*TC_098:- Verify user is able to Search Submittal Number (Module Search): Scripted BY RANJAN */

		@Test(priority = 97, enabled = true , description = "Verify user is able to Search Submittal with old data (Module Search)")
		public void SubmitalNumber_ModuleSearch_witholddata() throws Throwable {

			try {
				Log.testCaseInfo("TC_098 :-Verify user is able to Search Submittal (Module Search)");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement_Submittal();
				search_Testcases = new Search_Testcases(driver).get();
				//search_Testcases.Create_Submital_AndValidate(Submittal_Number, Submittal_Name);			
				String Flag = "Submittal_Number_search";
				Log.assertThat(search_Testcases.Searchscenario_submittal_ModuleSearch(Flag),"Submittal Number verified  Successfully", "Submittal Number Not verified Successfully", driver);

				
			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		/*TC_099:- Verify user is able to Search Submittal Name (Module Search): Scripted BY RANJAN */

		@Test(priority = 98, enabled = true , description = "Verify user is able to Search Submittal Name with old data(Module Search)")
		public void SubmitalName_ModuleSearch_oldData() throws Throwable {

			try {
				Log.testCaseInfo("TC_099 :-Verify user is able to Search Submittal Name with old data (Module Search)");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");				
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement_Submittal();
				search_Testcases = new Search_Testcases(driver).get();							
				String Flag = "Search_Submittal_Name";
				Log.assertThat(search_Testcases.Searchscenario_submittal_ModuleSearch(Flag),"Submittal Name verified  Successfully", "Submittal Name Not verified Successfully", driver);

				
			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		/*TC_100:- Verify user is able to Search Submittal Status (Module Search): Scripted BY RANJAN */

		@Test(priority = 99, enabled = true, description = "Verify user is able to Search Submittal Status with old data (Module Search)")
		public void SubmitalStatus_ModuleSearch_olddata() throws Throwable {

			try {
				Log.testCaseInfo("TC_100 :-Verify user is able to Search Submittal Status with old data (Module Search)");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");				
				// ===========Login with validUserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement_Submittal();
				search_Testcases = new Search_Testcases(driver).get();						
				String Flag = "Search_Status";
				Log.assertThat(search_Testcases.Searchscenario_submittal_ModuleSearch(Flag),"Submittal Status verified  Successfully", "Submittal Status Not verified Successfully", driver);

				
			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}

		/* TC_0101:- Verify user is able to filter Search Submittal number (Filter Search)-Scripted by Ranjan*/

		@Test(priority = 100, enabled = true, description = "Verify user is able to filter Search Submittal number (Filter Search)with Old data")
		public void SubmitalfilterSearch_Number_withOldData() throws Throwable {

			try {
				Log.testCaseInfo("TC_101 :-Verify user is able to do filter Search Submittal number (Filter Search) with old data");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				// ===========Login with UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement_Submittal();
				search_Testcases = new Search_Testcases(driver).get();
				//search_Testcases.Create_Submital_AndValidate(Submittal_Number, Submittal_Name);
				projectAndFolder_Level_Search.ProjectManagement_Submittal();
				search_Testcases = new Search_Testcases(driver).get();
				String Flag = "Search_SubmittalNumber";
				Log.assertThat(search_Testcases.AdvanceSearch_Submittal_old(Flag),"Submittal Number verified  Successfully", "Submittal Number Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		/* TC_102:- Verify user is able to filter Search Submittal Name (Filter Search)with old data Scripted by Ranjan*/

		@Test(priority = 101, enabled = true, description = "Verify user is able to filter Search Submittal Name (Filter Search) with Old Data")
		public void SubmitalfilterSearch_Name_olddata() throws Throwable {

			try {
				Log.testCaseInfo("TC_102 :-Verify user is able to filter Search Submittal Name (Filter Search) with old data");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement_Submittal();
				search_Testcases = new Search_Testcases(driver).get();				
				projectAndFolder_Level_Search.ProjectManagement_Submittal();
				search_Testcases = new Search_Testcases(driver).get();
				String Flag = "Search_SubmittalName";
				Log.assertThat(search_Testcases.AdvanceSearch_Submittal_old( Flag),"Submittal Name verified  Successfully", "Submittal Name Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}

		/* TC_103:- Verify user is able to filter Search Submittal Type (Filter Search) with old data Scripted by Ranjan*/

		@Test(priority = 102, enabled = true, description = "Verify user is able to filter Search Submittal Type (Filter Search) with old data")
		public void SubmitalfilterSearch_Type() throws Throwable {

			try {
				Log.testCaseInfo("TC_103:-Verify user is able to filter Search Submittal Type (Filter Search) with old data");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				
				// ===========Login with valid
				// UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement_Submittal();
				search_Testcases = new Search_Testcases(driver).get();				
				String Flag = "Search_SubmittalType";
				Log.assertThat(search_Testcases.AdvanceSearch_Submittal_old(Flag),"Submittal type verified  Successfully", "Submittal type  Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}

		/* TC_104:- Verify user is able to filter Search Submittal Status (Filter Search) with old data */

		@Test(priority = 103, enabled = true, description = "Verify user is able to filter Search Submittal Status (Filter Search) with old data")
		public void SubmitalfilterSearch_Status_old() throws Throwable {

			try {
				Log.testCaseInfo("TC_104 :-Verify user is able to filter Search Submittal Status (Filter Search)");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement_Submittal();
				search_Testcases = new Search_Testcases(driver).get();				
				String CommonValue = PropertyReader.getProperty("Status");
				String Flag = "Search_Status";
				Log.assertThat(search_Testcases.AdvanceSearch_Submittal(CommonValue, Flag),"Submittal Status verified  Successfully", "Submittal Status  Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		/* TC_105:- Verify user is able to filter Search Submittal Due Date (Filter Search)_old data Scripted by Ranjan */

		@Test(priority = 104, enabled = true, description = "Verify user is able to filter Search Submittal Due Date (Filter Search) with old data")
		public void SubmitalfilterSearch_DueDate_olddata() throws Throwable {

			try {
				Log.testCaseInfo("TC_105 :-Verify user is able to filter Search Submittal Due Date (Filter Search) with old data");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");				
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement_Submittal();
				search_Testcases = new Search_Testcases(driver).get();				
				String CommonValue = PropertyReader.getProperty("Status");
				String Flag = "Search_DueDate";
				search_Testcases.AdvanceSearch_Submittal(CommonValue, Flag);
				viewerScreenPage = new ViewerScreenPage(driver).get();
				Log.assertThat(viewerScreenPage.Logout(), "Logout working Successfully", "Logout not working Successfully",driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		/* TC_106.Verify user is able to punch filter Search- Description-Scripted by Ranjan */

		@Test(priority = 105, enabled = true, description = "Verify user is able to do filter Search Punch - Description with old data")
		public void FilterSearch_Punch_Description_olddata() throws Throwable {

			try {
				Log.testCaseInfo("TC_106 :-Verify user is able to do  filter Search Punch - Description with old data");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement_Punch();
				viewerScreenPage = new ViewerScreenPage(driver).get();				
				search_Testcases = new Search_Testcases(driver).get();
				String Description_sub = PropertyReader.getProperty("Punch_Subject");
				String Flag = "Search_Description";
				Log.assertThat(search_Testcases.AdvanceSearch_Punch(Description_sub, Flag),"Punch Description verified  Successfully", "Punch Description  Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
	
		
		/* TC_107.Verify user is able to do punch filter Search with OLD DATA- creator Scripted by Ranjan */

		@Test(priority = 106, enabled = true, description = "Verify user is able to do filter Search Punch with Old Data - Creator")
		public void FilterSearch_Punch_Creator_olddata() throws Throwable {

			try {
				Log.testCaseInfo("TC_107 :-Verify user is able to do filter Search Punch with Old Data- Creator");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");

				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement_Punch();
				viewerScreenPage = new ViewerScreenPage(driver).get();				
				search_Testcases = new Search_Testcases(driver).get();
				String creator = PropertyReader.getProperty("Creator");
				String Flag = "Search_Creator";
				Log.assertThat(search_Testcases.AdvanceSearch_Punch(creator, Flag), "Punch Creator verified  Successfully","Punch Creator  Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		
		
		/* TC_108.Verify user is able to punch filter Search- Assignee with Old Data Scripted by Ranjan*/

		@Test(priority = 107, enabled = true , description = "Verify user is able to do filter Search Punch - Assignee With Old Data")
		public void FilterSearch_Punch_Assignee_olddata() throws Throwable {

			try {
				Log.testCaseInfo("TC_108 :-Verify user is able to do filter Search Punch - Assignee with Old Data");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// ======================================================================
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement_Punch();
				viewerScreenPage = new ViewerScreenPage(driver).get();
				search_Testcases = new Search_Testcases(driver).get();
				String Assigne = PropertyReader.getProperty("SelectUserAssignTo_Email");
				String Flag = "Search_Assignee";
				Log.assertThat(search_Testcases.AdvanceSearch_Punch(Assigne, Flag), "Punch Assignee verified  Successfully","Punch Assignee  Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}
		}

		/* TC_109.Verify user is able to punch filter Search- Status with Old Data :Scripted by Ranjan*/

		@Test(priority = 108, enabled = true, description = "Verify user is able to do filter Search Punch - Status with Old Data")
		public void FilterSearch_Punch_Status_OldData() throws Throwable {

			try {
				Log.testCaseInfo("TC_109 :-Verify user is able to do filter Search Punch - Status with Old Data");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				// ===========Login with valid
				// UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = PropertyReader.getProperty("StaticProject");
				projectDashboardPage.ValidateSelectProject(Project_Name);
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement_Punch();
				viewerScreenPage = new ViewerScreenPage(driver).get();				
				search_Testcases = new Search_Testcases(driver).get();
				String status = PropertyReader.getProperty("Status");
				String Flag = "Search_Status";
				Log.assertThat(search_Testcases.AdvanceSearch_Punch(status, Flag), "Punch status verified  Successfully","Punch status  Not verified Successfully", driver);

				

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}

		/* TC_110.Verify user is able do Filter Search- Project Number with Old Data Scripted by Ranjan */

		@Test(priority = 109, enabled = true, description = "Verify user is able to do filter Search -Project Number With Old Data")
		public void FilterSearch_ProjectNumber_oldData() throws Throwable {

			try {
				Log.testCaseInfo("TC_110 :-Verify user is able to do filter Search -Project Number with Old Data");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");				
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				search_Testcases = new Search_Testcases(driver).get();				
				String Project_Name = PropertyReader.getProperty("StaticProject");				
				search_Testcases.Searchscenario(Project_Name);
				String Flag = "Search_ProjectNumber";			
				Log.assertThat(search_Testcases.AdvanceSearch_Project_oldData(Flag),"Project Number verified  Successfully", "project Number  Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		/* TC_111.Verify user is able do Filter Search- Project Name with Old Data :Scripted by Ranjan*/

		@Test(priority = 110, enabled = true , description = "Verify user is able to do filter Search -Project Name with Old Data")
		public void FilterSearch_projectname() throws Throwable {

			try {
				Log.testCaseInfo("TC_111:-Verify user is able to do filter Search -Project Name With Old Data");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");				
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				search_Testcases = new Search_Testcases(driver).get();
				String Project_Name = PropertyReader.getProperty("StaticProject");				
				search_Testcases.Searchscenario(Project_Name);				
				String Flag = "Search_ProjectName";
				Log.assertThat(search_Testcases.AdvanceSearch_Project_oldData(Flag),"Project Description verified  Successfully", "project Description  Not verified Successfully",driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}

		/* TC_112.Verify user is able do Filter Search- Project Description with Old Data :Scripted by Ranjan*/

		@Test(priority = 111, enabled = true , description = "Verify user is able to do filter Search -Description with Old Data")
		public void FilterSearch_Description_oldData() throws Throwable {

			try {
				Log.testCaseInfo("TC_112 :-Verify user is able to do filter Search -Description with Old Data");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				search_Testcases = new Search_Testcases(driver).get();
				String Project_Name = PropertyReader.getProperty("StaticProject");				
				search_Testcases.Searchscenario(Project_Name);				
				String Flag = "Search_ProjectDescription";
				Log.assertThat(search_Testcases.AdvanceSearch_Project_oldData(Flag),"Project Description verified  Successfully", "project Description  Not verified Successfully",
						driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		/* TC_113.Verify user is able do Filter Search- Project CITY  with  Old Data :  Scripted by Ranjan*/
		
		@Test(priority = 112, enabled = true , description = "Verify user is able to do filter Search -CITY with old data")
		public void FilterSearch_CITY_OldData() throws Throwable {

			try {
				Log.testCaseInfo("TC_113 :-Verify user is able to do filter Search - Project CITY with Old Data");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				// ===========Login with validUserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				search_Testcases = new Search_Testcases(driver).get();
				String Project_Name = PropertyReader.getProperty("StaticProject");				
				search_Testcases.Searchscenario(Project_Name);				
				String Flag = "Search_City";
				Log.assertThat(search_Testcases.AdvanceSearch_Project_oldData(Flag),"Project City verified  Successfully","project City  Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		
		/* TC_114.Verify user is able do Filter Search SendFile-Inbox-Exact order with Old Data :Scripted by Ranjan*/

		@Test(priority = 113, enabled = true, description = "Verify user is able to do filter Search SendFile-Exact order with Old Data")
		public void SendFile_Exactorder_OldData() throws Throwable {

			try {
				Log.testCaseInfo("TC_114 :-Verify user is able to do filter Search SendFile-Exact order with Old Data");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				search_Testcases = new Search_Testcases(driver).get();				
				String Flag = "Search_exactorder";				
				Log.assertThat(search_Testcases.AdvanceSearch_Sendfile_Inbox(Flag),"Send File inbox Exact order verified Successfully", "Send File inbox Not verified Successfully",driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		
		/* TC_115.Verify user is able do Filter Search SendFile-inbox-Date With Old Data :Scripted by Ranjan*/

		@Test(priority = 114, enabled = true , description = "Verify user is able to do filter Search SendFile inbox -Date With Old Data")
		public void SendFile_Date_with_OldData() throws Throwable {

			try {
				Log.testCaseInfo("TC_115 :-Verify user is able to do filter Search SendFile inbox -Date With Old Data");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				// ===========Login with valid
				// UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				search_Testcases = new Search_Testcases(driver).get();				
				String Flag = "Search_date";
				Log.assertThat(search_Testcases.AdvanceSearch_Sendfile_Date(Flag),"Send File inbox Date verified Successfully", "Send File inbox Date Not verified Successfully",driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		
		/* TC_116.Verify user is able do Filter Search SendFile Inbox-subject With Old Data:Scripted by Ranjan*/

		@Test(priority = 115, enabled = true , description = "Verify user is able to do filter Search SendFile-Subject With Old Data")
		public void SendFile_Subject_OldData() throws Throwable {

			try {
				Log.testCaseInfo("TC_116 :-Verify user is able to do filter Search SendFile-subject with Old Data");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				search_Testcases = new Search_Testcases(driver).get();
				// search_Testcases.Sendfile_selection();
				String Flag = "Search_Subject";
				Log.assertThat(search_Testcases.AdvanceSearch_Sendfile_Inbox(Flag),"Subject  verified  Successfully", "Subject Not verified Successfully", driver);


			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}

		/* TC_117.Verify user is able do Filter Search SendFile-SenderEmail with Old Data :Scripted by Ranjan*/

		@Test(priority = 116, enabled = true , description = "Verify user is able to do filter Search SendFile-Sender Email with Old Data")
		public void SendFile_Email_Old_Data() throws Throwable {

			try {
				Log.testCaseInfo("TC_117 :-Verify user is able to do filter Search SendFile-sender Email with Old Data");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");

				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				search_Testcases = new Search_Testcases(driver).get();
				search_Testcases = new Search_Testcases(driver).get();				
				String Flag = "Search_senderEmail";				
				Log.assertThat(search_Testcases.AdvanceSearch_Sendfile_Inbox(Flag),"Sender Email  verified  Successfully", "Sender Email Not verified Successfully", driver);



			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		
		/** TC_118.Verify user is able do Filter Search SendFile-Tracking-Exact order with Old Data :Scripted by Ranjan*/

		@Test(priority = 117, enabled = true , description = "Verify user is able to do filter Search SendFile-tracking-Exact order with Old data")
		public void SendFile_Tracking_Exactorder_OldData() throws Throwable {

			try {
				Log.testCaseInfo("TC_118 :-Verify user is able to do filter Search SendFile-Tracking-Exact order With Old Data");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				// ===========Login with valid
				// UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				search_Testcases = new Search_Testcases(driver).get();			
				String Flag = "Search_exactorder";				
				Log.assertThat(search_Testcases.AdvanceSearch_Sendfile_Tracking(Flag),"Exactorder  verified  Successfully", "Exactorder Not verified Successfully", driver);


			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}

		/* TC_119.Verify user is able do Filter Search SendFile-tracking-Date with Old Data:Scripted by Ranjan- failded

		@Test(priority = 118, enabled = true, description = "Verify user is able to do filter Search SendFile tracking -Date")
		public void SendFile_Tracking_Date_OldData() throws Throwable {

			try {
				Log.testCaseInfo("TC_119 :-Verify user is able to do filter Search SendFile Tracking-Date");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				// ===========Login with valid
				// UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				search_Testcases = new Search_Testcases(driver).get();				
				String Flag = "Search_date";
				search_Testcases.gettext_date(Flag);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}*/

		/* TC_120.Verify user is able do Filter Search SendFile Tracking-subject :Scripted by Ranjan*/

		@Test(priority = 119, enabled = true, description = "Verify user is able to do filter Search SendFile-Tracking-Subject")
		public void SendFile_Tracking_Subject_OldData() throws Throwable {

			try {
				Log.testCaseInfo("TC_120 :-Verify user is able to do filter Search SendFile-subject");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				// ===========Login with valid  UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				search_Testcases = new Search_Testcases(driver).get();				
				String Flag = "Search_Subject";
				Log.assertThat(search_Testcases.AdvanceSearch_Sendfile_Tracking(Flag),"Subject  verified  Successfully", "Subject Not verified Successfully", driver);


			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}

		/* TC_121.Verify user is able do Filter Search SendFile-receiverEmail with Old Data:Scripted by Ranjan */

		@Test(priority = 120, enabled = true , description = "Verify user is able to do filter Search SendFile-receiver Email with Old Data")
		public void SendFile_receiver_Email_OldData() throws Throwable {

			try {
				Log.testCaseInfo("TC_121 :-Verify user is able to do filter Search SendFile-sender Email with Old Data");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");

				// ===========Login with valid
				// UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				search_Testcases = new Search_Testcases(driver).get();				
				String Flag="Search_senderEmail";				
				Log.assertThat(search_Testcases.AdvanceSearch_Sendfile_Tracking(Flag),"Email  verified  Successfully", "Email Not verified Successfully", driver);


			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}

		/* TC_122:-----Verify user is able to do Shared project (Filter Search By ProjectName )  with old data scripted by Ranjan*/

		@Test(priority = 121, enabled = true, description = "Verify user is able to do Shared Project filter Search -Project Name with old data")
		public void FilterSearch_SharedProject_ProjectName_olddata() throws Throwable {

			try {
				Log.testCaseInfo("TC_122 :-Verify user is able to do SharedProject filter Search -Project Name with Old data");
				String uName = PropertyReader.getProperty("GuestUserName");
				String pWord = PropertyReader.getProperty("GuestUserPassword");
				
				// ===========Login with valid	UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				search_Testcases = new Search_Testcases(driver).get();				
				String Flag = "Search_ProjectName";
				Log.assertThat(search_Testcases.AdvanceSearch_Project_oldData(Flag),"Project Name  verified  Successfully", "Project Name Not verified Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		
		/*TC_123:-----Verify user is able to do Shared project (Filter Search By ProjectNumber ) with Old Data scripted by Ranjan*/

		@Test(priority = 122, enabled = true, description = "Verify user is able to do Shared Project filter Search -Project Number with Old data")
		public void FilterSearch_SharedProject_ProjectNumber_oldData() throws Throwable {

			try {
				Log.testCaseInfo("TC_123 :-Verify user is able to do SharedProject filter Search -Project Number-Old data");
				String uName = PropertyReader.getProperty("GuestUserName");
				String pWord = PropertyReader.getProperty("GuestUserPassword");
				
				// ===========Login with valid
				// UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				search_Testcases = new Search_Testcases(driver).get();				
				Log.assertThat(search_Testcases.projectno_sharedproject(),"Project Number  verification Completed  Successfully", "Project Number verification Not Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		
		/*TC_124:-----Verify user is able to do Shared project (Filter Search By Description ) with Old Data scripted by Ranjan*/

		@Test(priority = 123, enabled = true, description = "Verify user is able to do Shared Project filter Search -Description with Old data")
		public void FilterSearch_SharedProject_Description_OldData() throws Throwable {

			try {
				Log.testCaseInfo("TC_124 :-Verify user is able to do SharedProject filter Search -Description with Old Data");
				String uName = PropertyReader.getProperty("GuestUserName");
				String pWord = PropertyReader.getProperty("GuestUserPassword");				
				String Description = PropertyReader.getProperty("ProjectDescription");			
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				search_Testcases = new Search_Testcases(driver).get();				
				String Flag = "Search_ProjectDescription";
				Log.assertThat(search_Testcases.AdvanceSearch_Project(Description, Flag),"Description  verification Completed  Successfully", "Description verification Not Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}

		/* TC_125:-----Verify user is able to do Shared project (Filter Search By Country ) with Old data scripted by Ranjan*/

		@Test(priority = 124, enabled = true , description = "Verify user is able to do Shared Project filter Search -By Country with Old Data")
		public void FilterSearch_SharedProject_Country_withOldData() throws Throwable {

			try {
				Log.testCaseInfo("TC_125 :-Verify user is able to do SharedProject filter Search -By Country with Old Data");
				String uName = PropertyReader.getProperty("GuestUserName");
				String pWord = PropertyReader.getProperty("GuestUserPassword");				
				String Country = PropertyReader.getProperty("country");
				
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				search_Testcases = new Search_Testcases(driver).get();				
				String Flag = "Search_Country";
				Log.assertThat(search_Testcases.AdvanceSearch_Project(Country, Flag), "Country verification Completed Successfully",
						"Country verification Not Completed Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		
		/*TC_126:-----Verify user is able to do Shared project (Filter Search By  CITY ) with Old Datascripted by Ranjan*/

		@Test(priority = 125, enabled = true, description = "Verify user is able to do Shared Project filter Search -By CITY with Old Data")
		public void FilterSearch_SharedProject_CITY_with_OldData() throws Throwable {

			try {
				Log.testCaseInfo("TC_126 :-Verify user is able to do SharedProject filter Search -By CITY with Old Data");
				String uName = PropertyReader.getProperty("GuestUserName");
				String pWord = PropertyReader.getProperty("GuestUserPassword");				
				String City = PropertyReader.getProperty("city");				
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				search_Testcases = new Search_Testcases(driver).get();				
				String Flag = "Search_City";
				Log.assertThat(search_Testcases.AdvanceSearch_Project(City, Flag), "Shared Project -City   verification  Completed Successfully","Shared Project - City verification Not Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		/*TC_127:-----Verify user is able to do Shared project (Filter Search By State ) With Old Data scripted by Ranjan*/

		@Test(priority = 126, enabled = true , description = "TC_127:-Verify user is able to do Shared Project filter Search -By State with Old Data")
		public void FilterSearch_SharedProject_State_OldData() throws Throwable {

			try {
				Log.testCaseInfo("TC_127 :-Verify user is able to do SharedProject filter Search -By State with Old Data");
				String uName = PropertyReader.getProperty("GuestUserName");
				String pWord = PropertyReader.getProperty("GuestUserPassword");				
				String State = PropertyReader.getProperty("state");
				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				search_Testcases = new Search_Testcases(driver).get();				
				String Flag = "Search_State";
				Log.assertThat(search_Testcases.AdvanceSearch_Project(State, Flag), "Shared Project -State verification Completed Successfully","Shared Project -State verification Not Completed Successfully", driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		/*TC_128:-----Verify user is able to do Shared project (Filter Search By RFI Number) with old data scripted by Ranjan*/

		@Test(priority = 127, enabled = true  , description = "TC_128:-Verify user is able to do Shared project  (Filter Search By RFI Number) ")
		public void FilterSearch_SharedProject_RFINumber_Olddata() throws Throwable {

			try {
				Log.testCaseInfo("TC_128 :-Verify user is able to do Shared project  (Filter Search By RFI Number) with old data ");
				String uName = PropertyReader.getProperty("GuestUserName");
				String pWord = PropertyReader.getProperty("GuestUserPassword");
				// ===========Login with valid
				// UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				search_Testcases = new Search_Testcases(driver).get();
				search_Testcases.ProjectManagement();
				String Flag = "Search_RFINumber";
				//search_Testcases.AdvanceSearch_SharedProject_RFINumber(Flag);
				Log.assertThat(search_Testcases.AdvanceSearch_SharedProject_RFINumber(Flag), "Shared Project -RFI Number verification Completed Successfully","Shared Project -RFI Number verification Not Completed Successfully", driver);


			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		
		/*
		 * TC_129(Global Search an RFI with it's RFI number) 1) Verify all the
		 * content of the RFI including status opening the RFI in the search result
		 * window. 2) Try to download associated attachments and verify. 3) Change
		 * the status of the RFI from the search result window and verify in cloud.
		 */

		@Test(priority = 128, enabled = true , description = "TC_129:-Global Search an RFI with it's RFI number with Old data")
		public void Verify_RFI_GlobalSearch_with_RFINumber_With_Old_data() throws Throwable {

			try {
				Log.testCaseInfo("TC_129(RFI Project Level Validation):Global Search an RFI with it's RFI number with Old data");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				// ==============Login with valid
				// UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);

				// projectDashboardPage.projectselection();

				// =========Create Project Randomly===================================
				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);			
				projectAndFolder_Level_Search.ProjectManagement();				
				Log.assertThat(projectAndFolder_Level_Search.SearchRFI_RFINumber_olddata(), "Search  verification  Successfully","Search Not verification Successfully", driver);

			

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		
		
		/*
		 * TC_130:-----Verify user is able to do Photo Attribute filter search By Date Taken (Filter search by Date Taken) scripted by Ranjan*/

		@Test(priority = 129, enabled = true , description = "Verify user is able to do Photo Attribute filter search By Date Taken")
		public void PhotoAttribute_DateTaken() throws Throwable {

			try {
				Log.testCaseInfo("TC_130 :-Verify user is able to do Photo Attribute filter search By Date Taken ");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");

				// ===========Login with valid UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				// Select Project
				String Project_Name = "Specific_Testdata_Static";
				folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
				//
				folderPage.Select_Gallery_Folder();
				// Select Album
				String Album_Name = "Static_Photo";
				search_Testcases = new Search_Testcases(driver).get();
				search_Testcases.Verify_SelectAn_Album(Album_Name);
				//search_Testcases.ProjectManagement();
				String Flag = "Search_Date";			
				Log.assertThat(search_Testcases.FilterSearch_Date(Flag),"Date verified  Successfully", "Date Not verified Successfully", driver);
				

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
		
		
		
		/*
		 * TC_131(Global Search RFI by it's custom attribute) 1) Verify all the
		 * content of the RFI including status opening the RFI in the search result
		 * window. 2) Try to download associated attachments and verify. 3) Change
		 * the status of the RFI from the search result window and verify in cloud with Old Data .
		 * 
		 */

		@Test(priority = 130, enabled = true , description = "Global Search RFI by it's custom attribute.with Old Data")
		public void Verify_RFI_GlobalSearch_customattribute_oldData() throws Throwable {

			try {
				Log.testCaseInfo("TC_131(RFI Project Level Validation):Global Search RFI by it's custom attribute with old data");
				String uName = PropertyReader.getProperty("Username1");
				String pWord = PropertyReader.getProperty("Password1");
				// ==============Login with valid
				// UserID/PassWord===========================
				projectsLoginPage = new ProjectsLoginPage(driver).get();
				projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
				String Project_Name = "Prj_" + Generate_Random_Number.generateRandomValue();
				String CustumAttribute = "NewScenario" + Generate_Random_Number.generateRandomValue1();
				projectDashboardPage.createProjectwithCustomAtribute(Project_Name, CustumAttribute);

				projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
				projectAndFolder_Level_Search.ProjectManagement();
				projectAndFolder_Level_Search.RFI_CreationwithcustomAttribute(CustumAttribute);
				Log.assertThat(projectAndFolder_Level_Search.SearchRFI_Global(CustumAttribute), "Custom Attribute Verified Successfully", "Custom Attribute Not Verified Successfully",
						driver);

			} catch (Exception e) {
				e.getCause();
				Log.exception(e, driver);
				CommonMethod.analyzeLog(driver);

			} finally {
				Log.endTestCase();
				driver.quit();
			}

		}
		
}
