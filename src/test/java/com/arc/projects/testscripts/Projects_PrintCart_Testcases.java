package com.arc.projects.testscripts;

import org.testng.annotations.Test;

import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectGalleryPage;
import com.arc.projects.pages.ProjectLatestDocumentsPage;
import com.arc.projects.pages.ProjectPrintCartPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.ProjectsRegistrationPage;
import com.arc.projects.pages.PublishLogPage;
import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.PropertyReader;
import com.arcautoframe.utils.Log;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;


public class Projects_PrintCart_Testcases 
{
	
	public static WebDriver driver;
	
	ProjectsLoginPage projectsLoginPage;
	ProjectDashboardPage projectDashboardPage;
	FolderPage folderPage;
	ProjectPrintCartPage projectPrintCartPage;
	ProjectLatestDocumentsPage projectLatestDocumentsPage;
	
	
	@Parameters("browser")
	@BeforeMethod
    public WebDriver beforeTest(String browser) 
	{
		
		if(browser.equalsIgnoreCase("firefox")) 
        {
               File dest = new File("./drivers/win/geckodriver.exe");
               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
               driver = new FirefoxDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
		
		else if (browser.equalsIgnoreCase("chrome")) 
        { 
        File dest = new File("./drivers/win/chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());  
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        options.setExperimentalOption("prefs", prefs);
        driver = new ChromeDriver( options );
        driver.get(PropertyReader.getProperty("SkysiteProdURL"));
 
        }
		
		else if (browser.equalsIgnoreCase("safari"))
        {
               System.setProperty("webdriver.safari.noinstall", "false"); //To stop uninstall each time
               driver = new SafariDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
   
    	return driver;
		
	}

	
	
	/** TC_001 (" Verify addition of a Folder to print cart ")*/
	
	@Test(priority = 0, enabled = true, description = " Verify addition of a Folder to print cart ")
	public void Verify_FolderAddToPrintCart() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify addition of a Folder to print cart ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			folderPage=projectDashboardPage.enteringanyProject("A_Test");
			String filesCount=folderPage.addFolderToPrintCart();			
			projectPrintCartPage=folderPage.enterPrintCart();
			
			Log.assertThat(projectPrintCartPage.folderAdditionInPrintCart(filesCount), " Folder Selected is successfully added in the Print Cart ", " Folder Selected addition failed in the Print Cart!!! ", driver);		
				
			Log.assertThat(projectPrintCartPage.removeAllCartItems(), " All items in the cart are deleted successfully ", " All items deletion from cart failed!!! ", driver);
			
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }

	
	
	/** TC_002 (" Verify addition of Files to print cart ")*/
	
	@Test(priority = 1, enabled = true, description = " Verify addition of Files to print cart ")
	public void Verify_FilesAddToPrintCart() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify addition of Files to print cart ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
	    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			folderPage=projectDashboardPage.enteringanyProject("A_Test");
			folderPage.enterFolder1();
			int count= folderPage.addAllFilesInFolderToPrintCart();
			ArrayList<String> arr= folderPage.filesAddedToPrintCart();
			projectPrintCartPage=folderPage.enterPrintCart();
			
			Log.assertThat(projectPrintCartPage.filesAdditionInPrintCart(count,arr), " Files selected are successfully added in the Print Cart ", " Files selected addition failed  Print Cart!!! ", driver);		
				
			Log.assertThat(projectPrintCartPage.removeAllCartItems(), " All items in the cart are deleted successfully ", " All items deletion from cart failed!!! ", driver);
			
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }


	
	/** TC_003 (" Verify addition of an empty Folder to print cart ")*/
	
	@Test(priority = 2, enabled = true, description = " Verify addition of an empty Folder to print cart ")
	public void Verify_AddEmptyFolderToPrintCart() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify remove all item removes all item from print cart ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
	    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			folderPage=projectDashboardPage.enteringanyProject("A_Test");		
			
			Log.assertThat(folderPage.addEmptyFolderToPrintCart(), " Adding empty folder to print cart validation successful ", " Adding empty folder to print cart validation failed!!! ", driver);
			
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	
	/** TC_004 (" Verify delete option for individual files added in the print cart ")*/
	
	@Test(priority = 3, enabled = true, description = " Verify delete option for individual files added in the print cart ")
	public void Verify_DeleteFilesIndividuallyFromPrintCart() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify delete option for individual files added in the print cart ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
	    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			folderPage=projectDashboardPage.enteringanyProject("A_Test");
			folderPage.enterFolder1();
			int count= folderPage.addAllFilesInFolderToPrintCart();
			ArrayList<String> arr= folderPage.filesAddedToPrintCart();
			projectPrintCartPage=folderPage.enterPrintCart();
			
			Log.assertThat(projectPrintCartPage.filesAdditionInPrintCart(count,arr), " Files selected are successfully added in the Print Cart ", " Files selected addition failed  Print Cart!!! ", driver);		
				
			Log.assertThat(projectPrintCartPage.removeItemsIndividuallyFromCart(), " All files in the cart are deleted successfully individually ", " All files deletion individually from cart failed!!! ", driver);
			
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	
	/** TC_005 (" Verify remove all item removes all item from print cart ")*/
	
	@Test(priority = 4, enabled = true, description = " Verify remove all item removes all item from print cart ")
	public void Verify_RemoveAllItemsFromPrintCart() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify remove all item removes all item from print cart ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
	    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			folderPage=projectDashboardPage.enteringanyProject("A_Test");
			folderPage.enterFolder1();
			int count= folderPage.addAllFilesInFolderToPrintCart();
			ArrayList<String> arr= folderPage.filesAddedToPrintCart();
			projectPrintCartPage=folderPage.enterPrintCart();
			projectPrintCartPage.filesAdditionInPrintCart(count,arr);		
			
			Log.assertThat(projectPrintCartPage.removeAllCartItems(), " All items in the cart are deleted successfully ", " All items deletion from cart failed!!! ", driver);
			
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	
	/** TC_006 (" Verify delete option for individual folders added in the print cart ")*/
	
	@Test(priority = 5, enabled = true, description = " Verify delete option for individual folders added in the print cart ")
	public void Verify_DeleteFoldersIndividuallyFromPrintCart() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify delete option for individual folders added in the print cart ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
	    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			folderPage=projectDashboardPage.enteringanyProject("A_Test");
			ArrayList<String> arr= folderPage.addFoldersToPrintCart();
			int count= arr.size();
			Log.message("Folder count added in the cart is:- " +count);
			projectPrintCartPage=folderPage.enterPrintCart();
			projectPrintCartPage.filesAdditionInPrintCart(count,arr);	
			
			Log.assertThat(projectPrintCartPage.removeAllCartItems(), " All items in the cart are deleted successfully ", " All items deletion from cart failed!!! ", driver);
			
			
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_007 (" Verify 'Back to file' option navigate to Project Dashboard ")*/
	
	@Test(priority = 6, enabled = true, description = " Verify 'Back to file' option navigate to Project Dashboard ")
	public void Verify_BackToFileFromPrintCart() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify 'Back to file' option navigate to Project Dashboard ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
	    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			folderPage=projectDashboardPage.enteringanyProject("A_Test");
			String filesCount=folderPage.addFolderToPrintCart();			
			projectPrintCartPage=folderPage.enterPrintCart();	
			Log.assertThat(projectPrintCartPage.folderAdditionInPrintCart(filesCount), " Folder Selected is successfully added in the Print Cart ", " Folder Selected addition failed in the Print Cart!!! ", driver);			
			Log.assertThat(projectPrintCartPage.removeAllCartItems(), " All items in the cart are deleted successfully ", " All items deletion from cart failed!!! ", driver);
			projectPrintCartPage.backToFile();
			
			Log.assertThat(projectDashboardPage.verifyProjectDashboardLanding(), " 'Back to file' navigation from print Cart to Project Dashboard is successfully ", " 'Back to file' navigation from print Cart to Project Dashboard failed!!! ", driver);
			
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_008 (" Verify file count is also visible for folder added in print cart ")*/
	
	@Test(priority = 7, enabled = true, description = " Verify file count is also visible for folder added in print cart ")
	public void Verify_FilesCountForFolderAddedToPrintCart() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify file count is also visible for folder added in print cart ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			folderPage=projectDashboardPage.enteringanyProject("A_Test");
			String filesCount=folderPage.addFolderToPrintCart();			
			projectPrintCartPage=folderPage.enterPrintCart();
			
			Log.assertThat(projectPrintCartPage.folderAdditionInPrintCart(filesCount), " Files count inside the folder are displayed succesfully ", " Files count inside the folder are not displayed!!! ", driver);		
				
			Log.assertThat(projectPrintCartPage.removeAllCartItems(), " All items in the cart are deleted successfully ", " All items deletion from cart failed!!! ", driver);
			
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_009 (" Verify 'no of copies' option in print cart for an item added ")*/
	
	@Test(priority = 8, enabled = true, description = " Verify 'no of copies' option in print cart for an item added ")
	public void Verify_NoOfCopiesForIndividualItemsAddedToPrintCart() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify 'no of copies' option in print cart for an item added ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
	    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			folderPage=projectDashboardPage.enteringanyProject("A_Test");
			folderPage.enterFolder1();
			int count=0;
			int count1=0;
			for(count1=0; count1<2; count1++)
			{
				count= folderPage.addAllFilesInFolderToPrintCart();
			}
			
			ArrayList<String> arr= folderPage.filesAddedToPrintCart();
			projectPrintCartPage=folderPage.enterPrintCart();
			
			Log.assertThat(projectPrintCartPage.verifyNoOfCopiesForIndividualItemsInCart(count,arr,count1), " No of copies are displayed successfully of the items added in the cart ", " No of copies of the items added in the cart display failed!!! ", driver);		
				
			Log.assertThat(projectPrintCartPage.removeAllCartItems(), " All items in the cart are deleted successfully ", " All items deletion from cart failed!!! ", driver);
			
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_010 (" Verify add new delivery address in print cart ")*/
	
	@Test(priority = 9, enabled = true, description = " Verify add new delivery address in print cart ")
	public void Verify_AddNewDeliveryAddressInPrintCart() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify add new delivery address in print cart ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			folderPage=projectDashboardPage.enteringanyProject("A_Test");
			String filesCount=folderPage.addFolderToPrintCart();			
			projectPrintCartPage=folderPage.enterPrintCart();			
			Log.assertThat(projectPrintCartPage.folderAdditionInPrintCart(filesCount), " Folder Selected is successfully added in the Print Cart ", " Folder Selected addition failed in the Print Cart!!! ", driver);		
			
			Log.assertThat(projectPrintCartPage.addNewDeliveryAddress(), " Print cart new delivery address is added successfully " , " Print cart new delivery address addition failed!!! ", driver);
					
			Log.assertThat(projectPrintCartPage.removeAllCartItems(), " All items in the cart are deleted successfully ", " All items deletion from cart failed!!! ", driver);
			
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	
	/** TC_011 (" Verify upload is working in 'Upload Transmittals or Document distribution addresses' section ")*/
	
	@Test(priority = 10, enabled = true, description = " Verify upload is working in 'Upload Transmittals or Document distribution addresses' section ")
	public void Verify_UploadTransmittalDocumentDistributionAddress() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify upload is working in 'Upload Transmittals or Document distribution addresses' section ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			folderPage=projectDashboardPage.enteringanyProject("A_Test");
			String filesCount=folderPage.addFolderToPrintCart();			
			projectPrintCartPage=folderPage.enterPrintCart();			
			Log.assertThat(projectPrintCartPage.folderAdditionInPrintCart(filesCount), " Folder Selected is successfully added in the Print Cart ", " Folder Selected addition failed in the Print Cart!!! ", driver);		
			
			Log.assertThat(projectPrintCartPage.uploadTransmittalDocumentDistributionAddress(), " Transmittal or Document distribution addresses document uploaded successfully " , " Transmittal or Document distribution addresses document upload failed!!! ", driver);
					
			Log.assertThat(projectPrintCartPage.removeAllCartItems(), " All items in the cart are deleted successfully ", " All items deletion from cart failed!!! ", driver);
			
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
		

	
	/** TC_012 (" Verify order placed successfully and visible in my order section ")*/
	
	@Test(priority = 10, enabled = true, description = " Verify upload is working in 'Upload Transmittals or Document distribution addresses' section ")
	public void Verify_PlacePrintOrder() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify order placed successfully and visible in my order section ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			folderPage=projectDashboardPage.enteringanyProject("A_Test");
			String filesCount=folderPage.addFolderToPrintCart();			
			projectPrintCartPage=folderPage.enterPrintCart();			
			Log.assertThat(projectPrintCartPage.folderAdditionInPrintCart(filesCount), " Folder Selected is successfully added in the Print Cart ", " Folder Selected addition failed in the Print Cart!!! ", driver);		
					
			Log.assertThat(projectPrintCartPage.placePrintOrder(), " Order is placed and successsfully added in the My Order " , " Order is place and addition My Order failed!!! ", driver);
			
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	
	/** TC_013 (" Verify items added in the cart are still added after Logout and Log In ")*/
	
	@Test(priority = 12, enabled = true, description = " Verify items added in the cart are still added after Logout and Log In ")
	public void Verify_CartItemAfterLogoutLogin() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify items added in the cart are still added after Logout and Log In ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			folderPage=projectDashboardPage.enteringanyProject("A_Test");
			String filesCount=folderPage.addFolderToPrintCart();			
			projectPrintCartPage=folderPage.enterPrintCart();			
			Log.assertThat(projectPrintCartPage.folderAdditionInPrintCart(filesCount), " Folder Selected is successfully added in the Print Cart ", " Folder Selected addition failed in the Print Cart!!! ", driver);		
			Log.assertThat(projectPrintCartPage.logout(), " Logout successfull ", " Logout failed!!! ", driver);		
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			folderPage=projectDashboardPage.enteringanyProject("A_Test");
			projectPrintCartPage=folderPage.enterPrintCart();
			
			Log.assertThat(projectPrintCartPage.folderAdditionInPrintCart(filesCount), " Items added in the Print Cart are verified successfully after Logout and Login", " Verification for Items added in the cart after Logout and Log In failed!!! ", driver);		
					
			Log.assertThat(projectPrintCartPage.removeAllCartItems(), " All items in the cart are deleted successfully ", " All items deletion from cart failed!!! ", driver);
			
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	
	/** TC_014 (" Verify add items from Latest Documents ")*/
	
	@Test(priority = 13, enabled = true, description = " Verify add items from latest Documents ")
	public void Verify_CartItemAadditionFromLatestDocuments() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify add items from latest Documents ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(username,password); 
			folderPage = projectDashboardPage.enteringanyProject("A_Test");
			projectLatestDocumentsPage = folderPage.enterLatestDocuments();
			projectLatestDocumentsPage.latestDocumentEntryValidation();
			int count = projectLatestDocumentsPage.addAllFilesInFolderToPrintCart();
			ArrayList<String> arr = projectLatestDocumentsPage.addFilesToCart();
			projectPrintCartPage = projectLatestDocumentsPage.enterPrintCart();
								
			Log.assertThat(projectPrintCartPage.filesAdditionInPrintCart(count,arr), " Files selected are successfully added in the Print Cart ", " Files selected addition failed  Print Cart!!! ", driver);		
			
			Log.assertThat(projectPrintCartPage.removeAllCartItems(), " All items in the cart are deleted successfully ", " All items deletion from cart failed!!! ", driver);
			
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
}
