package com.arc.projects.testscripts;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.ITestResult;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.Gallery_Photo_Page;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.utils.AnalyzeLog;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)

public class Gallery_Album_Suit {
	
	String webSite;
    ITestResult result;
    
    static WebDriver driver;
    ProjectsLoginPage projectsLoginPage;
    ProjectDashboardPage projectDashboardPage;
    FolderPage folderPage;
    Gallery_Photo_Page gallery_Photo_Page;
    
    @Parameters("browser")
    @BeforeMethod(groups = "naresh_test")
 public WebDriver beforeTest(String browser) {
        
        if(browser.equalsIgnoreCase("firefox")) 
        {
               File dest = new File("./drivers/win/geckodriver.exe");
               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
               driver = new FirefoxDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
        
        else if (browser.equalsIgnoreCase("chrome")) 
        { 
        	File dest = new File("./drivers/win/chromedriver.exe");
            System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());  
            Map<String, Object> prefs = new HashMap<String, Object>();
            prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
            
            prefs.put("safebrowsing.enabled", "false");//this condition should be f@lse every time
 
            ChromeOptions options = new ChromeOptions();
             options.addArguments("--start-maximized");
            options.setExperimentalOption("prefs", prefs);
            driver = new ChromeDriver( options );
            driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        
        } 
        
        else if (browser.equalsIgnoreCase("safari"))
        {
               System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
               driver = new SafariDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
   return driver;
    }
    
   /** TC_001(upload photos to the gallery (With CopyDown)): Verify upload photos to the gallery (With CopyDown).
    * Scripted By : NARESH  BABU KAVURU
    * @throws Exception
    */
    @Test(priority = 0, enabled = true, description = "TC_001(upload photos to the gallery (With CopyDown)): Verify upload photos to the gallery (With CopyDown).")
    public void verify_UploadPotos_InGallery_WithCopyDown() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_001(upload photos to the gallery (With CopyDown)): Verify upload photos to the gallery (With CopyDown).");
    		//Getting Static data from properties file
    		String uName = PropertyReader.getProperty("NewData_Username");
    		String pWord = PropertyReader.getProperty("NewData_Password");
  		
    		projectsLoginPage = new ProjectsLoginPage(driver).get();  
    		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
    		    		
    		String Project_Name = "Prj_Galery"+Generate_Random_Number.generateRandomValue();
      		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
    		
      		folderPage.Select_Gallery_Folder();//Select Gallery
      			
      		File Path_FMProperties = new File(PropertyReader.getProperty("MultPhoto_TestData_Path"));
     		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
     		String CountOfPhotosInFolder = PropertyReader.getProperty("PhotoCount");
     		int FileCount = Integer.parseInt(CountOfPhotosInFolder);//String to Integer
     		Log.assertThat(folderPage.UploadPhotos_InGallery(FolderPath, FileCount), "Photo upload in Gallery is working Successfully",
     				"Photo upload in Gallery is Not working", driver);		    
    	}
        catch(Exception e)
        {
        	AnalyzeLog.analyzeLog(driver);
        	e.getCause();
        	Log.exception(e, driver);
        }
    	finally
    	{
    		Log.endTestCase();
    		driver.quit();
    	}
    }
    
    /** TC_002(Verify Gallery Download.): Verify Gallery Download.
     * Scripted By : NARESH  BABU KAVURU
     * @throws Exception
     */
     @Test(priority = 1, enabled = true, description = "TC_002(Verify Gallery Download.): Verify Gallery Download.")
     public void verify_Gallery_Download() throws Exception
     {
     	try
     	{
     		Log.testCaseInfo("TC_002(Verify Gallery Download.): Verify Gallery Download.");
     		//Getting Static data from properties file
     		String uName = PropertyReader.getProperty("Username");
     		String pWord = PropertyReader.getProperty("Password");
   		
     		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     			
     		String Project_Name = PropertyReader.getProperty("prj_FolderOperations");
       		folderPage = projectDashboardPage.Validate_SelectAProject(Project_Name);//Calling Select Project
     		
       		gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
       		folderPage=gallery_Photo_Page.Verify_GalleryFolder_IsAvailable();//Verify Gallery Folder
       			
       		String usernamedir=System.getProperty("user.name");
     		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
      		Log.assertThat(gallery_Photo_Page.Verify_Gallery_Download(Sys_Download_Path, Project_Name), 
      				"Gallery Download is working Successfully","Gallery Download is Not working", driver);		    
     	}
        catch(Exception e)
        {
        	AnalyzeLog.analyzeLog(driver);
        	e.getCause();
         	Log.exception(e, driver);
        }
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     	}
     }
     
     /** TC_003(Verify Gallery SendLink and Download.): Verify Gallery SendLink and Download.
      * Scripted By : NARESH  BABU KAVURU
      * @throws Exception
      */
      @Test(priority = 2, enabled = true, description = "TC_003(Verify Gallery SendLink and Download.): Verify Gallery SendLink and Download.")
      public void verify_Gallery_SendLink_AndDownload() throws Exception
      {
      	try
      	{
      		Log.testCaseInfo("TC_003(Verify Gallery SendLink and Download.): Verify Gallery SendLink and Download.");
      		//Getting Static data from properties file
      		String uName = PropertyReader.getProperty("Username");
      		String pWord = PropertyReader.getProperty("Password");
    		
      		projectsLoginPage = new ProjectsLoginPage(driver).get();  
      		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
      			
      		String Project_Name = PropertyReader.getProperty("prj_FolderOperations");
        	folderPage = projectDashboardPage.Validate_SelectAProject(Project_Name);//Calling Select Project
      		
        	gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
        	folderPage=gallery_Photo_Page.Verify_GalleryFolder_IsAvailable();//Verify Gallery Folder
        			
       		Log.assertThat(gallery_Photo_Page.Verify_Gallery_SendLink(), 
       				"Gallery SendLink is working Successfully","Gallery SendLink is Not working", driver);		    
      	}
        catch(Exception e)
        {
        	AnalyzeLog.analyzeLog(driver);
        	e.getCause();
          	Log.exception(e, driver);
        }
      	finally
      	{
      		Log.endTestCase();
      		driver.quit();
      	}
      }
      
      /** TC_004(Verify Add Album and Upload Photos.): Verify Add Album and Upload Photos.
       * Scripted By : NARESH  BABU KAVURU
       * @throws Exception
       */
       @Test(priority = 3, enabled = true, description = "TC_004(Verify Add Album and Upload Photos.): Verify Add Album and Upload Photos.")
       public void verify_AddAlbum_AndUpload_Photos() throws Exception
       {
       	try
       	{
       		Log.testCaseInfo("TC_004(Verify Add Album and Upload Photos.): Verify Add Album and Upload Photos.");
       		//Getting Static data from properties file
       		String uName = PropertyReader.getProperty("Username");
       		String pWord = PropertyReader.getProperty("Password");
     		
       		projectsLoginPage = new ProjectsLoginPage(driver).get();  
       		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
       			
       		String Project_Name = PropertyReader.getProperty("Prj_UnSuportFiles");
         	folderPage = projectDashboardPage.Validate_SelectAProject(Project_Name);//Calling Select Project
       		
         	gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
         	folderPage=gallery_Photo_Page.Verify_GalleryFolder_IsAvailable();//Verify Gallery Folder
         	
         	folderPage.Select_Gallery_Folder();//Select Gallery
         	
         	String Album_Name = "Album_"+Generate_Random_Number.generateRandomValue();
         	Log.assertThat(gallery_Photo_Page.Verify_Add_Album(Album_Name), 
    				"New Almub adding is working Successfully","New Almub adding is Not working", driver);
         	
         	Log.assertThat(gallery_Photo_Page.Verify_SelectAn_Album(Album_Name), 
    				"Almub selected Successfully","Almub selection is Failed", driver);
         			
         	File Path_FMProperties = new File(PropertyReader.getProperty("MultPhoto_TestData_Path"));
     		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
     		String CountOfPhotosInFolder = PropertyReader.getProperty("PhotoCount");
     		int FileCount = Integer.parseInt(CountOfPhotosInFolder);//String to Integer
     		Log.assertThat(folderPage.UploadPhotos_InGallery(FolderPath, FileCount), "Photo upload in Album is working Successfully",
     				"Photo upload in Album is Not working", driver);		    
       	}
        catch(Exception e)
        {
        	AnalyzeLog.analyzeLog(driver);
         	e.getCause();
           	Log.exception(e, driver);
        }
       	finally
       	{
       		Log.endTestCase();
       		driver.quit();
       	}
       }
       
       /** TC_005(Verify Download Album.): Verify Download Album.
        * Scripted By : NARESH  BABU KAVURU
        * @throws Exception
        */
        @Test(priority = 4, enabled = true, description = "TC_005(Verify Download Album.): Verify Download Album.")
        public void verify_Album_Download() throws Exception
        {
        	try
        	{
        		Log.testCaseInfo("TC_005(Verify Download Album.): Verify Download Album.");
        		//Getting Static data from properties file
        		String uName = PropertyReader.getProperty("Username");
        		String pWord = PropertyReader.getProperty("Password");
      		
        		projectsLoginPage = new ProjectsLoginPage(driver).get();  
        		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
        			
        		String Project_Name = PropertyReader.getProperty("Prj_UnSuportFiles");
        		folderPage = projectDashboardPage.Validate_SelectAProject(Project_Name);//Calling Select Project
        		
        		gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
        		folderPage=gallery_Photo_Page.Verify_GalleryFolder_IsAvailable();//Verify Gallery Folder
          	
        		folderPage.Select_Gallery_Folder();//Select Gallery
        		
        		String Album_Name = PropertyReader.getProperty("Album_Static");
        		String usernamedir=System.getProperty("user.name");
         		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
        		Log.assertThat(gallery_Photo_Page.Verify_Album_Download(Sys_Download_Path, Album_Name), 
        				"Downloading an Almub is working Successfully","Downloading an Almub is Not working", driver);		    
        	}
        	catch(Exception e)
        	{
        		AnalyzeLog.analyzeLog(driver);
        		e.getCause();
            	Log.exception(e, driver);
        	}
        	finally
        	{
        		Log.endTestCase();
        		driver.quit();
        	}
        }
        
        /**TC_006(Verify Send Link of an Album.): Verify Send Link of an Album.
         * Scripted By : NARESH  BABU KAVURU
         * @throws Exception
         */
         @Test(priority = 5, enabled = true, description = "TC_006(Verify Send Link of an Album.): Verify Send Link of an Album.")
         public void verify_Album_SendLink() throws Exception
         {
         	try
         	{
         		Log.testCaseInfo("TC_006(Verify Send Link of an Album.): Verify Send Link of an Album.");
         		//Getting Static data from properties file
         		String uName = PropertyReader.getProperty("Username");
         		String pWord = PropertyReader.getProperty("Password");
       		
         		projectsLoginPage = new ProjectsLoginPage(driver).get();  
         		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
         			
         		String Project_Name = PropertyReader.getProperty("Prj_UnSuportFiles");
         		folderPage = projectDashboardPage.Validate_SelectAProject(Project_Name);//Calling Select Project
         		
         		gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
         		folderPage=gallery_Photo_Page.Verify_GalleryFolder_IsAvailable();//Verify Gallery Folder
           	
         		folderPage.Select_Gallery_Folder();//Select Gallery
         		
         		String Album_Name = PropertyReader.getProperty("Album_Static");
         		Log.assertThat(gallery_Photo_Page.Verify_Album_SendLink(Album_Name), 
         				"Almub Send Link is working Successfully","Almub Send Link is Not working", driver);		    
         	}
         	catch(Exception e)
         	{
         		AnalyzeLog.analyzeLog(driver);
         		e.getCause();
             	Log.exception(e, driver);
         	}
         	finally
         	{
         		Log.endTestCase();
         		driver.quit();
         	}
         }
         
/**TC_007(Verify Album Edit and Delete.): Verify Album Edit and Delete.
 * Scripted By : NARESH  BABU KAVURU
 * @throws Exception
 */
@Test(priority = 6, enabled = true, description = "TC_007(Verify Album Edit and Delete.): Verify Album Edit and Delete.")
	public void verify_Album_Edit_Delete() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_007(Verify Album Edit and Delete.): Verify Album Edit and Delete.");
      		//Getting Static data from properties file
      		String uName = PropertyReader.getProperty("Username");
      		String pWord = PropertyReader.getProperty("Password");
    		
      		projectsLoginPage = new ProjectsLoginPage(driver).get();  
      		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
      			
      		String Project_Name = PropertyReader.getProperty("Prj_UnSuportFiles");
      		folderPage = projectDashboardPage.Validate_SelectAProject(Project_Name);//Calling Select Project
      		
      		gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
      		folderPage=gallery_Photo_Page.Verify_GalleryFolder_IsAvailable();//Verify Gallery Folder
        	
      		folderPage.Select_Gallery_Folder();//Select Gallery
      		String Album_Name = "Album_"+Generate_Random_Number.generateRandomValue();
         	Log.assertThat(gallery_Photo_Page.Verify_Add_Album(Album_Name), 
    				"New Almub adding is working Successfully","New Almub adding is Not working", driver);
         	String Edit_Album_Name = "Edit_Album_"+Generate_Random_Number.generateRandomValue();
         	
      		Log.assertThat(gallery_Photo_Page.Verify_Album_Edit(Album_Name, Edit_Album_Name), 
      				"Edit an Almub is working Successfully","Edit an Almub is Not working", driver);
      		
      		Log.assertThat(gallery_Photo_Page.Verify_Album_Delete(Edit_Album_Name), 
      				"Delete an Almub is working Successfully","Delete an Almub is Not working", driver);
      		
      	}
      	catch(Exception e)
      	{
      		AnalyzeLog.analyzeLog(driver);
      		e.getCause();
          	Log.exception(e, driver);
      	}
      	finally
      	{
      		Log.endTestCase();
      		driver.quit();
      	}
      }

/**TC_008(Verify Photo download from Gallery.): Verify Photo download from Gallery.
 * Scripted By : NARESH  BABU KAVURU
 * @throws Exception
 */
@Test(priority = 7, enabled = true, description = "TC_008(Verify Photo download from Gallery.): Verify Photo download from Gallery.")
	public void verify_Photo_Download_Gallery() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_008(Verify Photo download from Gallery.): Verify Photo download from Gallery.");
      		//Getting Static data from properties file
      		String uName = PropertyReader.getProperty("Username");
      		String pWord = PropertyReader.getProperty("Password");
      		String Photo_Name = PropertyReader.getProperty("Photo_Name");
    		
      		projectsLoginPage = new ProjectsLoginPage(driver).get();  
      		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
      			
      		String Project_Name = PropertyReader.getProperty("prj_FolderOperations");
      		folderPage = projectDashboardPage.Validate_SelectAProject(Project_Name);//Calling Select Project
      		
      		gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
      		folderPage=gallery_Photo_Page.Verify_GalleryFolder_IsAvailable();//Verify Gallery Folder
        	
      		folderPage.Select_Gallery_Folder();//Select Gallery
      		
      		String usernamedir=System.getProperty("user.name");
     		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
      		Log.assertThat(gallery_Photo_Page.Verify_Photo_DownloadOfGallery(Sys_Download_Path, Photo_Name), 
      				"Photo Download From Gallery is working Successfully","Photo Download From Gallery is Not working", driver);
      		
      	}
      	catch(Exception e)
      	{
      		AnalyzeLog.analyzeLog(driver);
      		e.getCause();
          	Log.exception(e, driver);
      	}
      	finally
      	{
      		Log.endTestCase();
      		driver.quit();
      	}
      }
	
/**TC_009(Verify Photo Send Link and download.): Verify Photo Send Link and download.
 * Scripted By : NARESH  BABU KAVURU
 * @throws Exception
 */
@Test(priority = 8, enabled = true, description = "TC_009(Verify Photo Send Link and download.): Verify Photo Send Link and download.")
	public void verify_Photo_SendLink_Download() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_009(Verify Photo Send Link and download.): Verify Photo Send Link and download.");
      		//Getting Static data from properties file
      		String uName = PropertyReader.getProperty("Username");
      		String pWord = PropertyReader.getProperty("Password");
      		String Photo_Name = PropertyReader.getProperty("Photo_Name");
    		
      		projectsLoginPage = new ProjectsLoginPage(driver).get();  
      		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
      			
      		String Project_Name = PropertyReader.getProperty("prj_FolderOperations");
      		folderPage = projectDashboardPage.Validate_SelectAProject(Project_Name);//Calling Select Project
      		
      		gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
      		folderPage=gallery_Photo_Page.Verify_GalleryFolder_IsAvailable();//Verify Gallery Folder
        	
      		folderPage.Select_Gallery_Folder();//Select Gallery
      		
      		String usernamedir=System.getProperty("user.name");
     		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
     		
     		Log.assertThat(gallery_Photo_Page.Verify_Photo_SendLink_And_Download(Sys_Download_Path, Photo_Name), 
      				"Photo Send Link From Gallery is working Successfully","Photo Send Link From Gallery is Not working", driver);
     		
      		Log.assertThat(folderPage.Download_SingleFile_SendLinkForAFIle(Sys_Download_Path, Photo_Name),
      				"Photo Download From Gallery is working Successfully","Photo Download From Gallery is Not working", driver);
      		
      	}
      	catch(Exception e)
      	{
      		AnalyzeLog.analyzeLog(driver);
      		e.getCause();
          	Log.exception(e, driver);
      	}
      	finally
      	{
      		Log.endTestCase();
      		driver.quit();
      	}
      }

/**TC_010(Verify Photo Edit and Delete.): Verify Photo Edit and Delete.
 * Scripted By : NARESH  BABU KAVURU
 * @throws Exception
 */
@Test(priority = 9, enabled = true, description = "TC_010(Verify Photo Edit and Delete.): Verify Photo Edit and Delete.")
	public void verify_Photo_Edit_Delete() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_010(Verify Photo Edit and Delete.): Verify Photo Edit and Delete.");
    		//Getting Static data from properties file
    		String uName = PropertyReader.getProperty("NewData_Username");
    		String pWord = PropertyReader.getProperty("NewData_Password");
  		
    		projectsLoginPage = new ProjectsLoginPage(driver).get();  
    		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
    		    		
    		String Project_Name = "Prj_Galery"+Generate_Random_Number.generateRandomValue();
      		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
    		
      		gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
      		folderPage=gallery_Photo_Page.Verify_GalleryFolder_IsAvailable();//Verify Gallery Folder
      		
      		folderPage.Select_Gallery_Folder();//Select Gallery
      			
      		File Path_FMProperties = new File(PropertyReader.getProperty("SinglePhoto_TestData_Path"));
     		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
     		int FileCount = 1;
     		Log.assertThat(folderPage.UploadPhotos_InGallery(FolderPath, FileCount), "Single Photo upload in Gallery is working Successfully",
     				"Single Photo upload in Gallery is Not working", driver);
     		
     		String Photo_Name = PropertyReader.getProperty("Photo_Name");
     		String Edit_PhotoName = "Edit_"+Generate_Random_Number.generateRandomValue();
     		Log.assertThat(gallery_Photo_Page.Verify_Photo_EditAttributes(Photo_Name, Edit_PhotoName), "Photo Edit Attributes working Successfully",
     				"Photo Edit Attributes is Not working", driver);
     		
     		Log.assertThat(gallery_Photo_Page.Verify_Photo_Delete(Edit_PhotoName), "Photo Delete working Successfully",
     				"Photo Delete is Not working", driver);
      		
      	}
      	catch(Exception e)
      	{
      		AnalyzeLog.analyzeLog(driver);
      		e.getCause();
          	Log.exception(e, driver);
      	}
      	finally
      	{
      		Log.endTestCase();
      		driver.quit();
      	}
      }

/**TC_011(Verify Multiple Photo selection and Delete.): Verify Multiple Photo selection and Delete.
 * Scripted By : NARESH  BABU KAVURU
 * @throws Exception
 */
@Test(priority = 10, enabled = true, description = "TC_011(Verify Multiple Photo selection and Delete.): Verify Multiple Photo selection and Delete.")
	public void verify_MultiplePhotoSelection_Delete() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_011(Verify Multiple Photo selection and Delete.): Verify Multiple Photo selection and Delete.");
      		//Getting Static data from properties file
      		String uName = PropertyReader.getProperty("Username");
      		String pWord = PropertyReader.getProperty("Password");
    		
      		projectsLoginPage = new ProjectsLoginPage(driver).get();  
      		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
      			
      		String Project_Name = PropertyReader.getProperty("Prj_UnSuportFiles");
      		folderPage = projectDashboardPage.Validate_SelectAProject(Project_Name);//Calling Select Project
      		
      		gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
      		folderPage=gallery_Photo_Page.Verify_GalleryFolder_IsAvailable();//Verify Gallery Folder
        	
      		folderPage.Select_Gallery_Folder();//Select Gallery
      		String Album_Name = "Album_"+Generate_Random_Number.generateRandomValue();
         	Log.assertThat(gallery_Photo_Page.Verify_Add_Album(Album_Name), 
    				"New Almub adding is working Successfully","New Almub adding is Not working", driver);
 
         	Log.assertThat(gallery_Photo_Page.Verify_SelectAn_Album(Album_Name), 
    				"Almub selected Successfully","Almub selection is Failed", driver);
         			
         	File Path_FMProperties = new File(PropertyReader.getProperty("MultPhoto_TestData_Path"));
     		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
     		String CountOfPhotosInFolder = PropertyReader.getProperty("PhotoCount");
     		int FileCount = Integer.parseInt(CountOfPhotosInFolder);//String to Integer
     		Log.assertThat(folderPage.UploadPhotos_InGallery(FolderPath, FileCount), "Photo upload in Album is working Successfully",
     				"Photo upload in Album is Not working", driver);
      		
      		Log.assertThat(gallery_Photo_Page.Verify_Delete_BySelectMultPhotos(), 
      				"Delete by select multiple photos is working Successfully","Delete by select multiple photos is Not working", driver);
      		
      	}
      	catch(Exception e)
      	{
      		AnalyzeLog.analyzeLog(driver);
      		e.getCause();
          	Log.exception(e, driver);
      	}
      	finally
      	{
      		Log.endTestCase();
      		driver.quit();
      	}
      }

/**TC_014(Verify Album Edit Attributes.): Verify Album Edit Attributes.
 * Scripted By : NARESH  BABU KAVURU
 * @throws Exception
 */
@Test(priority = 11, enabled = true, description = "TC_014(Verify Album Edit Attributes.): Verify Album Edit Attributes.")
	public void verify_Album_Edit_Attributes() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_014(Verify Album Edit Attributes.): Verify Album Edit Attributes.");
      		//Getting Static data from properties file
      		String uName = PropertyReader.getProperty("Username");
      		String pWord = PropertyReader.getProperty("Password");
    		
      		projectsLoginPage = new ProjectsLoginPage(driver).get();  
      		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
      			
      		String Project_Name = PropertyReader.getProperty("Prj_UnSuportFiles");
      		folderPage = projectDashboardPage.Validate_SelectAProject(Project_Name);//Calling Select Project
      		
      		gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
      		folderPage=gallery_Photo_Page.Verify_GalleryFolder_IsAvailable();//Verify Gallery Folder
        	
      		folderPage.Select_Gallery_Folder();//Select Gallery
      		String Album_Name = PropertyReader.getProperty("Album_EditAttribute");
      		String CountOfPhotosInFolder = PropertyReader.getProperty("PhotoCount");
     		int PhotoCount = Integer.parseInt(CountOfPhotosInFolder);//String to Integer
         	
      		Log.assertThat(gallery_Photo_Page.Verify_Album_Edit_Attributes(Album_Name, PhotoCount), 
      				"Edit Attributes Almub Level is working Successfully","Edit Attributes Almub Level is Not working", driver);
      		
      	}
      	catch(Exception e)
      	{
      		AnalyzeLog.analyzeLog(driver);
      		e.getCause();
          	Log.exception(e, driver);
      	}
      	finally
      	{
      		Log.endTestCase();
      		driver.quit();
      	}
      }

/**TC_015(Verify Photo annotation with attachments.): Verify Photo annotation with attachments.
 * Scripted By : NARESH  BABU KAVURU
 * @throws Exception
 */
@Test(priority = 12, enabled = true, description = "TC_015(Verify Photo annotation with attachments.): Verify Photo annotation with attachments.")
	public void verify_Photo_Annotation() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_015(Verify Photo annotation with attachments.): Verify Photo annotation with attachments.");
      		//Getting Static data from properties file
    		String uName = PropertyReader.getProperty("NewData_Username");
    		String pWord = PropertyReader.getProperty("NewData_Password");
    		
      		projectsLoginPage = new ProjectsLoginPage(driver).get();  
      		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
      			
      		String Project_Name = PropertyReader.getProperty("Shared_Project1");
      		folderPage = projectDashboardPage.Validate_SelectAProject(Project_Name);//Calling Select Project
      		
      		gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
      		folderPage=gallery_Photo_Page.Verify_GalleryFolder_IsAvailable();//Verify Gallery Folder
        	
      		String Foldername = PropertyReader.getProperty("Share_Folder");
     		folderPage.Select_Folder(Foldername);//Select a Folder
         	
     		String File_Name = PropertyReader.getProperty("Exp_Shr_FileName");
      		Log.assertThat(folderPage.Verify_Select_Expected_File(File_Name), 
      				"File Selection is working Successfully","File Selection is Not working", driver);
      		
      		File Path_FMProperties = new File(PropertyReader.getProperty("MultPhoto_TestData_Path"));
     		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
     		String Annotation_Name = "PhotoAnnot_"+Generate_Random_Number.generateRandomValue();
      	
     		Log.assertThat(gallery_Photo_Page.Verify_Photo_Annotation_With_Attachments(FolderPath, Annotation_Name), 
      				"Photo Annotation is working Successfully","Photo Annotation is Not working", driver);
      	}
      	catch(Exception e)
      	{
      		AnalyzeLog.analyzeLog(driver);
      		e.getCause();
          	Log.exception(e, driver);
      	}
      	finally
      	{
      		Log.endTestCase();
      		driver.quit();
      	}
      }

/**TC_016(Verify Edit Attributes of a Photo Annotation.): Verify Edit Attributes of a Photo Annotation.
 * Scripted By : NARESH  BABU KAVURU
 * @throws Exception
 */
@Test(priority = 13, enabled = true, description = "TC_016(Verify Edit Attributes of a Photo Annotation.): Verify Edit Attributes of a Photo Annotation.")
	public void verify_EditAttributes_Photo_Annotation() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_016(Verify Edit Attributes of a Photo Annotation.): Verify Edit Attributes of a Photo Annotation.");
      		//Getting Static data from properties file
    		String uName = PropertyReader.getProperty("NewData_Username");
    		String pWord = PropertyReader.getProperty("NewData_Password");
    		
      		projectsLoginPage = new ProjectsLoginPage(driver).get();  
      		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
      			
      		String Project_Name = PropertyReader.getProperty("Shared_Project1");
      		folderPage = projectDashboardPage.Validate_SelectAProject(Project_Name);//Calling Select Project
      		
      		gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
      		folderPage=gallery_Photo_Page.Verify_GalleryFolder_IsAvailable();//Verify Gallery Folder
        	
      		String Foldername = PropertyReader.getProperty("Share_Folder");
     		folderPage.Select_Folder(Foldername);//Select a Folder
         	
     		String File_Name = PropertyReader.getProperty("Edit_PhotoAnnot_File");
      		Log.assertThat(folderPage.Verify_Select_Expected_File(File_Name), 
      				"File Selection is working Successfully","File Selection is Not working", driver);
      		
     		String Edit_Photo_Annotation_Name = "EditAttribts_"+Generate_Random_Number.generateRandomValue();
     		Log.assertThat(gallery_Photo_Page.Verify_Photo_Annotation_EditAttributes(Edit_Photo_Annotation_Name), 
      				"Edit Attribute of Photo Annotation is working Successfully","Edit Attribute of Photo Annotation is Not working", driver);
      	}
      	catch(Exception e)
      	{
      		AnalyzeLog.analyzeLog(driver);
      		e.getCause();
          	Log.exception(e, driver);
      	}
      	finally
      	{
      		Log.endTestCase();
      		driver.quit();
      	}
      }

/**TC_017(Verify Add Photo to Photo annotation.): Verify Add Photo to Photo annotation.
 * Scripted By : NARESH BABU KAVURU
 * @throws Exception
 */
@Test(priority = 14, enabled = true, description = "TC_017(Verify Add Photo to Photo annotation.): Verify Add Photo to Photo annotation.",groups = "naresh_test")
	public void verify_Add_PhotoTo_Photo_Annotation() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_017(Verify Add Photo(Internal & External) to Photo annotation.): Verify Add Photo to Photo annotation.");
      		//Getting Static data from properties file
    		String uName = PropertyReader.getProperty("NewData_Username");
    		String pWord = PropertyReader.getProperty("NewData_Password");
    		
      		projectsLoginPage = new ProjectsLoginPage(driver).get();  
      		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
      			
      		String Project_Name = PropertyReader.getProperty("Shared_Project2");
      		folderPage = projectDashboardPage.Validate_SelectAProject(Project_Name);//Calling Select Project
      		
      		gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
      		folderPage=gallery_Photo_Page.Verify_GalleryFolder_IsAvailable();//Verify Gallery Folder
        	
      		String Foldername = PropertyReader.getProperty("Share_Folder");
     		folderPage.Select_Folder(Foldername);//Select a Folder
         	
     		String File_Name = PropertyReader.getProperty("Edit_PhotoAnnot_File");
      		Log.assertThat(folderPage.Verify_Select_Expected_File(File_Name), 
      				"File Selection is working Successfully","File Selection is Not working", driver);
      		
      		File Path_FMProperties = new File(PropertyReader.getProperty("SinglePhoto_TestData_Path"));
     		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
     		Log.assertThat(gallery_Photo_Page.Verify_AddPhoto_ToPhotoAnnotation(FolderPath), 
      				"Add Photo to an Annotation is working Successfully","Add Photo to an Annotation is Not working", driver);
      	}
      	catch(Exception e)
      	{
      		AnalyzeLog.analyzeLog(driver);
      		e.getCause();
          	Log.exception(e, driver);
      	}
      	finally
      	{
      		Log.endTestCase();
      		driver.quit();
      	}
      }

/**TC_018(Verify Remove Single and All Photos from Photo annotation): Verify Remove Photo from Photo annotation.
 * Scripted By : NARESH  BABU KAVURU
 * @throws Exception
 */
@Test(priority = 15, enabled = true, description = "TC_018(Verify Remove Single and All Photos from Photo annotation): Verify Remove Photo from Photo annotation.")
	public void verify_Remove_PhotoFrom_Photo_Annotation() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_018(Verify Remove Single and All Photos from Photo annotation): Verify Remove Photo from Photo annotation.");
      		//Getting Static data from properties file
    		String uName = PropertyReader.getProperty("NewData_Username");
    		String pWord = PropertyReader.getProperty("NewData_Password");
    		
      		projectsLoginPage = new ProjectsLoginPage(driver).get();  
      		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
      			
      		String Project_Name = PropertyReader.getProperty("Shared_Project1");
      		folderPage = projectDashboardPage.Validate_SelectAProject(Project_Name);//Calling Select Project
      		
      		gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
      		folderPage=gallery_Photo_Page.Verify_GalleryFolder_IsAvailable();//Verify Gallery Folder
        	
      		String Foldername = PropertyReader.getProperty("Share_Folder");
     		folderPage.Select_Folder(Foldername);//Select a Folder
         	
     		String File_Name = PropertyReader.getProperty("Exp_Shr_FileName");
      		Log.assertThat(folderPage.Verify_Select_Expected_File(File_Name), 
      				"File Selection is working Successfully","File Selection is Not working", driver);
      		
     		Log.assertThat(gallery_Photo_Page.Verify_Remove_APhoto_FromPhotoAnnotation(), 
      				"Remove Photo From an Annotation is working Successfully","Remove Photo From an Annotation is Not working", driver);
     		
     		Log.assertThat(folderPage.Verify_Select_Expected_File(File_Name), 
      				"File Selection is working Successfully","File Selection is Not working", driver);
     		
     		Log.assertThat(gallery_Photo_Page.Verify_DeleteAll_Photos_FromPhotoAnnotation(), 
      				"Delete All Photos From an Annotation is working Successfully","Delete All Photos From an Annotation is Not working", driver);
      	}
      	catch(Exception e)
      	{
      		AnalyzeLog.analyzeLog(driver);
      		e.getCause();
          	Log.exception(e, driver);
      	}
      	finally
      	{
      		Log.endTestCase();
      		driver.quit();
      	}
     }

/**TC_019(Verify Delete photo annotation from the list.): Verify Delete photo annotation from the list.
 * Scripted By : NARESH BABU KAVURU
 * @throws Exception
 */
@Test(priority = 16, enabled = true, description = "TC_019(Verify Delete photo annotation from the list.): Verify Delete photo annotation from the list.")
	public void verify_Delete_PhotoAnnotation_FromList() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_019(Verify Delete photo annotation from the list.): Verify Delete photo annotation from the list.");
      		//Getting Static data from properties file
    		String uName = PropertyReader.getProperty("NewData_Username");
    		String pWord = PropertyReader.getProperty("NewData_Password");
    		
      		projectsLoginPage = new ProjectsLoginPage(driver).get();  
      		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
      			
      		String Project_Name = PropertyReader.getProperty("Shared_Project1");
      		folderPage = projectDashboardPage.Validate_SelectAProject(Project_Name);//Calling Select Project
      		
      		gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
      		folderPage=gallery_Photo_Page.Verify_GalleryFolder_IsAvailable();//Verify Gallery Folder
        	
      		String Foldername = PropertyReader.getProperty("Share_Folder");
     		folderPage.Select_Folder(Foldername);//Select a Folder
         	
     		String File_Name = PropertyReader.getProperty("Exp_Shr_FileName");
      		Log.assertThat(folderPage.Verify_Select_Expected_File(File_Name), 
      				"File Selection is working Successfully","File Selection is Not working", driver);
      		
      		File Path_FMProperties = new File(PropertyReader.getProperty("MultPhoto_TestData_Path"));
     		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
     		String Annotation_Name = "PhotoAnnot_"+Generate_Random_Number.generateRandomValue();
      	
     		Log.assertThat(gallery_Photo_Page.Verify_Photo_Annotation_With_Attachments(FolderPath, Annotation_Name), 
      				"Photo Annotation is working Successfully","Photo Annotation is Not working", driver);
     		
     		Log.assertThat(folderPage.Verify_Select_Expected_File(File_Name), 
      				"File Selection is working Successfully","File Selection is Not working", driver);
     		
     		Log.assertThat(gallery_Photo_Page.Verify_Delete_APhotoAnnotation(Annotation_Name), 
      				"Delete A Photo Annotation is working Successfully","Delete A Photo Annotation is Not working", driver);
      	}
      	catch(Exception e)
      	{
      		AnalyzeLog.analyzeLog(driver);
      		e.getCause();
          	Log.exception(e, driver);
      	}
      	finally
      	{
      		Log.endTestCase();
      		driver.quit();
      	}
     }

/**TC_020(Verify create Photo annotation by adding photos and validate From Gallery.): Verify photos from Gallery after create Photo annotation.
 * Scripted By : NARESH BABU KAVURU
 * @throws Exception
 */
@Test(priority = 17, enabled = true, description = "TC_020(Verify create Photo annotation by adding photos and validate From Gallery.): Verify photos from Gallery after create Photo annotation.")
	public void verify_PhotosFrom_Gallery_AfterCreatePhotoAnnotation() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_020(Verify create Photo annotation by adding photos and validate From Gallery.): Verify photos from Gallery after create Photo annotation.");
      		//Getting Static data from properties file
    		String uName = PropertyReader.getProperty("NewData_Username");
    		String pWord = PropertyReader.getProperty("NewData_Password");
    		
      		projectsLoginPage = new ProjectsLoginPage(driver).get();  
      		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
      			
      		String Project_Name = "Photo_Annotin"+Generate_Random_Number.generateRandomValue();
      		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
      		
      		String Foldername = "Annotation_"+Generate_Random_Number.generateRandomValue();
     		folderPage.New_Folder_Create(Foldername);//Create a Folder
      		
      		gallery_Photo_Page = new Gallery_Photo_Page(driver).get();
      		folderPage=gallery_Photo_Page.Verify_GalleryFolder_IsAvailable();//Verify Gallery Folder
        	
     		folderPage.Select_Folder(Foldername);//Select a Folder
         	
     		File Path_FMProperties = new File(PropertyReader.getProperty("SingleFIle_TestData_Path"));
     		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
     		String CountOfFilesInFolder = PropertyReader.getProperty("FileCou_OneFile");
     		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
     		folderPage.UploadFiles_WithoutIndex(FolderPath,FileCount);//Calling Upload using do not index
     		folderPage.Select_Folder(Foldername);//Select a folder - newly created
     		
     		String File_Name = PropertyReader.getProperty("Single_File_Name");
      		Log.assertThat(folderPage.Verify_Select_Expected_File(File_Name), 
      				"File Selection is working Successfully","File Selection is Not working", driver);
      		
      		File Path_OfPhotos = new File(PropertyReader.getProperty("SinglePhoto_TestData_Path"));
     		String Photo_FolderPath = Path_OfPhotos.getAbsolutePath().toString();
     		String Annotation_Name = "PhotoAnnot_"+Generate_Random_Number.generateRandomValue();
      	
     		Log.assertThat(gallery_Photo_Page.Verify_Photo_Annotation_With_Attachments(Photo_FolderPath, Annotation_Name), 
      				"Photo Annotation is working Successfully","Photo Annotation is Not working", driver);
     		
     		driver.findElement(By.xpath("//a[@class='aProjHeaderLink']")).click();//Click on Project Header Link
     		folderPage.Select_Gallery_Folder();//Select Gallery
     		
     		Log.assertThat(folderPage.ValidateUploadPhotos(Photo_FolderPath, FileCount), 
      				"Photos are available in Gallery Folder","Photos are NOT available in Gallery Folder", driver);
      	}
      	catch(Exception e)
      	{
      		AnalyzeLog.analyzeLog(driver);
      		e.getCause();
          	Log.exception(e, driver);
      	}
      	finally
      	{
      		Log.endTestCase();
      		driver.quit();
      	}
     }
	
	

}
