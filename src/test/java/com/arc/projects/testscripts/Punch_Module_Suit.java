package com.arc.projects.testscripts;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.ITestResult;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.PunchPage;
import com.arc.projects.utils.AnalyzeLog;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.getCurrentDate;
import com.arcautoframe.utils.Log;

public class Punch_Module_Suit {
	
	String webSite;
    ITestResult result;
    
    static WebDriver driver;
    ProjectsLoginPage projectsLoginPage;
    ProjectDashboardPage projectDashboardPage;
    FolderPage folderPage;
    PunchPage punchPage;
 
    @Parameters("browser")
    @BeforeMethod(groups = "naresh_test")//Newly Added due to group execution Naresh
 public WebDriver beforeTest(String browser) {
        
        if(browser.equalsIgnoreCase("firefox")) 
        {
               File dest = new File("./drivers/win/geckodriver.exe");
               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
               driver = new FirefoxDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
        
        else if (browser.equalsIgnoreCase("chrome")) 
        { 
        	File dest = new File("./drivers/win/chromedriver.exe");
            System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--start-maximized");
            driver = new ChromeDriver( options );
            driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        } 
        
        else if (browser.equalsIgnoreCase("safari"))
        {
        	System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
            driver = new SafariDriver();
            driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
   return driver;
 }
    
    /** TC_001 (Project level punch): Create a punch without any attachments and validate.
     * Scripted By : NARESH  BABU kavuru
     * @throws Exception
     */
     @Test(priority = 0, enabled = true, description = "TC_001 (Project level punch): Create a punch without any attachments and validate.")
     public void verify_CreatePunch_WithoutAttachments() throws Exception
     {
     	try
     	{
     		Log.testCaseInfo("TC_001 (Project level punch): Create a punch without any attachments and validate.");
     	//Getting Static data from properties file
     		String UserName = PropertyReader.getProperty("UserName_Punch");
     		String Password = PropertyReader.getProperty("Password_Punch");
     		String Project_Name = PropertyReader.getProperty("Punch_StaticDataProject");
     		String Employee_Name = PropertyReader.getProperty("Punch_Emp1_Name");
     		String Employee_Mailid = PropertyReader.getProperty("Punch_Emp1_Mail");
     		String CC_UserName = PropertyReader.getProperty("Punch_Emp2_Name");
     		String Punch_Description = PropertyReader.getProperty("Punch_Descr_WithoutAtchmnts");
   		
     		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     		projectDashboardPage=projectsLoginPage.loginWithValidCredential(UserName,Password);//Calling Login MethodS
     		
     		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
  		
     		punchPage = new PunchPage(driver).get();
     		folderPage=punchPage.SelectPunchList();//Select Punch List
     		
     		String Stamp_Number = Generate_Random_Number.randomStampNumber();
     		Log.assertThat(punchPage.Create_Punch_NoAttachments_AndValidate(Stamp_Number, Employee_Name, Employee_Mailid, CC_UserName, Punch_Description), 
     				"Create Punch without attachments working Successfully", "Create Punch without attachments is Not working", driver);     
     	}
        catch(Exception e)
        {
        	AnalyzeLog.analyzeLog(driver);
        	e.getCause();
         	Log.exception(e, driver);
        }
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     	}
     }
     
     /** TC_002 (Project level punch): Create a punch with attachments and validate.
      * Scripted By : NARESH  BABU kavuru
      * @throws Exception
      */
      @Test(priority = 1, enabled = true, description = "TC_002 (Project level punch): Create a punch with attachments and validate.")
      public void verify_CreatePunch_WithAttachments() throws Exception
      {
      	try
      	{
      		Log.testCaseInfo("TC_002 (Project level punch): Create a punch with attachments and validate.");
      	//Getting Static data from properties file
      		String UserName = PropertyReader.getProperty("UserName_Punch");
      		String Password = PropertyReader.getProperty("Password_Punch");
      		String Project_Name = PropertyReader.getProperty("Punch_StaticDataProject");
      		String Employee_Name = PropertyReader.getProperty("Punch_Emp1_Name");
      		String Employee_Mailid = PropertyReader.getProperty("Punch_Emp1_Mail");
    		String Punch_Description = PropertyReader.getProperty("Punch_Descr_Atchmts");
    		String File_Name = PropertyReader.getProperty("Punch_Search_File");
    		
      		projectsLoginPage = new ProjectsLoginPage(driver).get();  
      		projectDashboardPage=projectsLoginPage.loginWithValidCredential(UserName,Password);//Calling Login MethodS
      		
      		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
   		
      		punchPage = new PunchPage(driver).get();
      		folderPage=punchPage.SelectPunchList();//Select Punch List
      		
      		File Path_FMProperties = new File(PropertyReader.getProperty("DonotInd_TestData_Path"));
     		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
      		String Stamp_Number = Generate_Random_Number.randomStampNumber();
      		Log.assertThat(punchPage.Create_Punch_WithAttachments_AndValidate(Stamp_Number,Employee_Name,Employee_Mailid,FolderPath,Punch_Description,File_Name), 
      				"Create Punch with attachments working Successfully", "Create Punch with attachments is Not working", driver);     
      	}
         catch(Exception e)
         {
        	 AnalyzeLog.analyzeLog(driver);
         	e.getCause();
          	Log.exception(e, driver);
         }
      	finally
      	{
      		Log.endTestCase();
      		driver.quit();
      	}
      }
      
      /** TC_003 (Project level punch): Create a Stamp, Edit and Delete and validate.
       * Scripted By : NARESH  BABU kavuru
       * @throws Exception
       */
       @Test(priority = 2, enabled = true, description = "TC_003 (Project level punch): Create a Stamp, Edit and Delete and validate.")
       public void verify_CreateStamp_Edit_Delete() throws Exception
       {
       	try
       	{
       		Log.testCaseInfo("TC_003 (Project level punch): Create a Stamp, Edit and Delete and validate.");
       	//Getting Static data from properties file
       		String UserName = PropertyReader.getProperty("UserName_Punch");
       		String Password = PropertyReader.getProperty("Password_Punch");
       		String Project_Name = PropertyReader.getProperty("StampCreate_Project");
       		String Stamp_Creater = PropertyReader.getProperty("PunchCreater_Name");
     		
       		projectsLoginPage = new ProjectsLoginPage(driver).get();  
       		projectDashboardPage=projectsLoginPage.loginWithValidCredential(UserName,Password);//Calling Login MethodS
       		
       		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
    		
       		punchPage = new PunchPage(driver).get();
       		folderPage=punchPage.SelectPunchList();//Select Punch List
       		
       		String Stamp_Number = Generate_Random_Number.randomStampNumber();
       		Log.assertThat(punchPage.Create_Edit_DeleteStamp_FromManageStamp(Stamp_Number, Stamp_Creater), 
       				"Create stamp, Edit and Delete is working Successfully", "Create stamp, Edit and Delete is Not working", driver);     
       	}
          catch(Exception e)
          {
        	  AnalyzeLog.analyzeLog(driver);
          	e.getCause();
           	Log.exception(e, driver);
          }
       	finally
       	{
       		Log.endTestCase();
       		driver.quit();
       	}
       }
       
/** TC_004 (Project level punch): Verify punch list from receiver, download attachments, commit and change the status to Completed.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 */
@Test(priority = 3, enabled = true, description = "TC_004 (Project level punch): Verify punch list from receiver, download attachments, commit and change the status to Completed.")
public void verify_PunchList_DownloadAttachments_Commit_ChangeStatus_ToCompleted() throws Exception
{
	try
	{
		Log.testCaseInfo("TC_004 (Project level punch): Verify punch list from receiver, download attachments, commit and change the status to Completed.");
    	//Getting Static data from properties file
		String UserName = PropertyReader.getProperty("UserName_Punch");
		String Password = PropertyReader.getProperty("Password_Punch");
		String Project_Name = PropertyReader.getProperty("Punch_StaticDataProject");
		String Punch_Creater = PropertyReader.getProperty("PunchCreater_Name");
		String Employee_Name = PropertyReader.getProperty("Punch_Emp1_Name");
  		String Employee_Mailid = PropertyReader.getProperty("Punch_Emp1_Mail");
  		String Punch_Description = PropertyReader.getProperty("Punch_Descr_Atchmts");
  		String Attachment_Count = PropertyReader.getProperty("Punch_WithFile_AtchmentCount");
  		String usernamedir=System.getProperty("user.name");
 		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
 		String Employee_Comment = PropertyReader.getProperty("Receiver_Comment");
 		String File_Name = PropertyReader.getProperty("Punch_Search_File");
		
  		projectsLoginPage = new ProjectsLoginPage(driver).get();  
  		projectDashboardPage=projectsLoginPage.loginWithValidCredential(UserName,Password);//Calling Login MethodS
  		
  		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
		
  		punchPage = new PunchPage(driver).get();
  		folderPage=punchPage.SelectPunchList();//Select Punch List
  		
  		File Path_FMProperties = new File(PropertyReader.getProperty("DonotInd_TestData_Path"));
 		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
  		String Stamp_Number = Generate_Random_Number.randomStampNumber();
  		Log.assertThat(punchPage.Create_Punch_WithAttachments_AndValidate(Stamp_Number, Employee_Name, Employee_Mailid, FolderPath,Punch_Description,File_Name), 
  				"Create Punch with attachments working Successfully", "Create Punch with attachments is Not working", driver);
  		
  		projectDashboardPage.Logout_Projects();//Logout from Owner
 		projectsLoginPage.loginWithValidCredential(Employee_Mailid,Password);//Login as Employee
 		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
 		punchPage.SelectPunchList();//Select Punch List
 		
		Log.assertThat(punchPage.Verify_ExpectedPunch_FromReceiver(Stamp_Number, Punch_Creater, Punch_Description, Attachment_Count), 
				"Punch is successfully assigned to employee.", "Punch is failed to assigne to employee.", driver);
		
		Log.assertThat(punchPage.Verify_SelectFistPunch_UnderAssignToMeTab_FromReceivr(), 
				"First Punch is successfully Selected.", "First Punch is Failed to Select.", driver);
		
 		Log.assertThat(punchPage.Verify_download_attachments_from_receiver(Sys_Download_Path), 
				"User successfully downloaded punch attachment.", "User failed to download punch attachment.", driver);
 		
 		Log.assertThat(punchPage.Verify_PunchComment_AndValidate(Employee_Comment), 
				"User is able to comment on punch successfully.", "User is NOT able to comment on punch", driver);
 		
 		Log.assertThat(punchPage.Verify_StatusChange_ToCompleted_AndValidate(), 
				"Status changed to Completed successfully.", "Status changed to Completed is failed.", driver);
 		
	}
   catch(Exception e)
   {
	   AnalyzeLog.analyzeLog(driver);
   		e.getCause();
    	Log.exception(e, driver);
   }
	finally
	{
		Log.endTestCase();
		driver.quit();
	}
}
        
/** TC_005 (Project level punch): Verify Punch Save as Draft and assign the drafted punch.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 */
@Test(priority = 4, enabled = true, description = "TC_005 (Project level punch): Verify Punch Save as Draft and assign the drafted punch.")
public void verify_PunchSaveAsDraft_Assign() throws Exception
{
	try
 	{
 		Log.testCaseInfo("TC_005 (Project level punch): Verify Punch making as Save as Draft and assign the drafted punch.");
 	//Getting Static data from properties file
 		String UserName = PropertyReader.getProperty("UserName_Punch");
 		String Password = PropertyReader.getProperty("Password_Punch");
 		String Project_Name = PropertyReader.getProperty("Punch_StaticDataProject");
 		String Employee_Name = PropertyReader.getProperty("Punch_Emp1_Name");
   		String Employee_Mailid = PropertyReader.getProperty("Punch_Emp1_Mail");
   		String Punch_Description = PropertyReader.getProperty("PunchDraft_Description");
 		
   		projectsLoginPage = new ProjectsLoginPage(driver).get();  
   		projectDashboardPage=projectsLoginPage.loginWithValidCredential(UserName,Password);//Calling Login MethodS
   		
   		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
		
   		punchPage = new PunchPage(driver).get();
   		folderPage=punchPage.SelectPunchList();//Select Punch List
   		
   		String Stamp_Number = Generate_Random_Number.randomStampNumber();
   		Log.assertThat(punchPage.Create_Punch_SaveAsDraft(Stamp_Number, Employee_Name, Employee_Mailid, Punch_Description), 
   				"Drafting a Punch is working Successfully", "Drafting a Punch is Not working", driver);
   		
  		Log.assertThat(punchPage.Verify_Assign_DraftedPunch(Stamp_Number, Employee_Name), 
 				"Assigning Drafted punch is working successfully.", "Assigning Drafted punch is failed.", driver);
  		
 	}
    catch(Exception e)
    {
    	AnalyzeLog.analyzeLog(driver);
    	e.getCause();
     	Log.exception(e, driver);
    }
 	finally
 	{
 		Log.endTestCase();
 		driver.quit();
 	}
}

/** TC_006 (Project level punch): Create a punch by attaching Photo and validate from Gallery.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 */
 @Test(priority = 5, enabled = true, description = "TC_006 (Project level punch): Create a punch by attaching Photo and validate from Gallery")
 public void verify_CreatePunch_AttachPhotos_ValidateGallery() throws Exception
 {
 	try
 	{
 		Log.testCaseInfo("TC_006 (Project level punch): Create a punch by attaching Photo and validate from Gallery");
 	//Getting Static data from properties file
 		String UserName = PropertyReader.getProperty("NewData_Username");
 		String Password = PropertyReader.getProperty("NewData_Password");
 		String Project_Name = "Prj_"+Generate_Random_Number.generateRandomValue();
 		String Employee_Name = PropertyReader.getProperty("Employee1_Name");
 		String Employee_Mailid = PropertyReader.getProperty("Employee_Email");
		String Punch_Description = PropertyReader.getProperty("Punch_Descr_Atchmts");
		
 		projectsLoginPage = new ProjectsLoginPage(driver).get();  
 		projectDashboardPage=projectsLoginPage.loginWithValidCredential(UserName,Password);//Calling Login MethodS
 		
 		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
		
 		punchPage = new PunchPage(driver).get();
 		folderPage=punchPage.SelectPunchList();//Select Punch List
 		
 		File Path_FMProperties = new File(PropertyReader.getProperty("SinglePhoto_TestData_Path"));
		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
 		String Stamp_Number = Generate_Random_Number.randomStampNumber();
 		Log.assertThat(punchPage.Create_Punch_WithAttachments_FromOutside(Stamp_Number,Employee_Name,Employee_Mailid,FolderPath,Punch_Description), 
 				"Create Punch with attachment from computer working Successfully", "Create Punch with attachments is Not working", driver);  
 		
 		driver.findElement(By.xpath("//a[@class='aProjHeaderLink']")).click();//Click on Project Header Link
 		folderPage.Select_Gallery_Folder();//Select Gallery
 		
 		String CountOfFilesInFolder = PropertyReader.getProperty("FileCou_OneFile");
 		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
 		Log.assertThat(folderPage.ValidateUploadPhotos(FolderPath, FileCount), 
  				"Photos are available in Gallery Folder","Photos are NOT available in Gallery Folder", driver);
 	}
    catch(Exception e)
    {
    	AnalyzeLog.analyzeLog(driver);
    	e.getCause();
     	Log.exception(e, driver);
    }
 	finally
 	{
 		Log.endTestCase();
 		driver.quit();
 	}
 }
 
 /** TC_007 (Project level punch): Verify download punch report.
  * Scripted By : NARESH  BABU kavuru
  * @throws Exception
  */
  @Test(priority = 6, enabled = true, description = "TC_007 (Project level punch): Verify download punch report.")
  public void verify_Download_PunchReport() throws Exception
  {
  	try
  	{
  		Log.testCaseInfo("TC_007 (Project level punch): Verify download punch report.");
  	//Getting Static data from properties file
  		String UserName = PropertyReader.getProperty("UserName_Punch");
 		String Password = PropertyReader.getProperty("Password_Punch");
 		String Project_Name = PropertyReader.getProperty("Punch_StaticDataProject");
 		String usernamedir=System.getProperty("user.name");
 		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
  		
 		
  		projectsLoginPage = new ProjectsLoginPage(driver).get();  
  		projectDashboardPage=projectsLoginPage.loginWithValidCredential(UserName,Password);//Calling Login MethodS
  		
  		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
 		
  		punchPage = new PunchPage(driver).get();
  		folderPage=punchPage.SelectPunchList();//Select Punch List
  		
  		Log.assertThat(punchPage.Verify_download_Punch_Report(Sys_Download_Path), 
   				"Generate punch report working successfullt.","Generate punch report not working.", driver);
  	}
    catch(Exception e)
    {
    	AnalyzeLog.analyzeLog(driver);
     	e.getCause();
      	Log.exception(e, driver);
    }
  	finally
  	{
  		Log.endTestCase();
  		driver.quit();
  	}
  }
  
  /** TC_008 (Project level punch): Verify download attachments from comment window and change the status to Close.
   * Scripted By : NARESH  BABU kavuru
   * @throws Exception
   */
   @Test(priority = 7, enabled = true, description = "TC_008 (Project level punch): Verify download attachments from comment window and change the status to Close.")
   public void verify_DownloadCommentAttachments_ChangeStatus_ToClose() throws Exception
   {
   	try
   	{
   		Log.testCaseInfo("TC_008 (Project level punch): Verify download attachments from comment window and change the status to Close.");
   	//Getting Static data from properties file
   		String UserName = PropertyReader.getProperty("UserName_Punch");
   		String Password = PropertyReader.getProperty("Password_Punch");
   		String Project_Name = PropertyReader.getProperty("Punch_StaticDataProject");
   		String Punch_Creater = PropertyReader.getProperty("PunchCreater_Name");
   		String Employee_Name = PropertyReader.getProperty("Punch_Emp1_Name");
   		String Employee_Mailid = PropertyReader.getProperty("Punch_Emp1_Mail");
   		String CC_UserName = PropertyReader.getProperty("Punch_Emp2_Name");
 		String Punch_Description = PropertyReader.getProperty("Punch_Descr_WithoutAtchmnts");
 		String Attachment_Count = PropertyReader.getProperty("Punch_NOFile_AtchmentCount");
 		String usernamedir=System.getProperty("user.name");
		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
		String Employee_Comment = PropertyReader.getProperty("Receiver_Comment");
		String Stamp_Number = Generate_Random_Number.randomStampNumber();
		
 		projectsLoginPage = new ProjectsLoginPage(driver).get();  
 		projectDashboardPage=projectsLoginPage.loginWithValidCredential(UserName,Password);//Calling Login MethodS
 		
 		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
		
 		punchPage = new PunchPage(driver).get();
 		folderPage=punchPage.SelectPunchList();//Select Punch List
 			
 		Log.assertThat(punchPage.Create_Punch_NoAttachments_AndValidate(Stamp_Number, Employee_Name, Employee_Mailid, CC_UserName, Punch_Description), 
 				"Create Punch without attachments working Successfully", "Create Punch without attachments is Not working", driver); 
 		
 		projectDashboardPage.Logout_Projects();//Logout from Owner
		projectsLoginPage.loginWithValidCredential(Employee_Mailid,Password);//Login as Employee
		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
		punchPage.SelectPunchList();//Select Punch List
		
		Log.assertThat(punchPage.Verify_ExpectedPunch_FromReceiver(Stamp_Number, Punch_Creater, Punch_Description, Attachment_Count), 
				"Punch is successfully assigned to employee.", "Punch is failed to assigne to employee.", driver);
		
		File Path_FMProperties = new File(PropertyReader.getProperty("SingleFIle_TestData_Path"));
		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
		Log.assertThat(punchPage.Verify_PunchComment_WithAttachments_AndValidate(Employee_Comment, FolderPath), 
   				"User comment with attachment on punch successfully.", "User comment with attachment is failed", driver);
		
		Log.assertThat(punchPage.Verify_StatusChange_ToCompleted_AndValidate(), 
   				"Status changed to Completed successfully.", "Status changed to Completed is failed.", driver);
		
		projectDashboardPage.Logout_Projects();//Logout from Employee
		projectsLoginPage.loginWithValidCredential(UserName,Password);//Calling Login Method
		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
		punchPage.SelectPunchList();//Select Punch List
		
		Log.assertThat(punchPage.Verify_SelectFistPunch_FromCreater(), 
				"First Punch is successfully Selected.", "First Punch is Failed to Select.", driver);
			
		Log.assertThat(punchPage.Verify_download_attachments_from_PunchHistory(Sys_Download_Path), 
				"User successfully downloaded punch attachment.", "User failed to download punch attachment.", driver);
    		
		Log.assertThat(punchPage.Verify_StatusChange_ToClosed_AndValidate(Stamp_Number), 
				"Status changed to Closed successfully.", "Status changed to Closed is failed.", driver);
    		
   	}
      catch(Exception e)
      {
    	  AnalyzeLog.analyzeLog(driver);
      	e.getCause();
       	Log.exception(e, driver);
      }
   	finally
   	{
   		Log.endTestCase();
   		driver.quit();
   	}
   }
   
   /** TC_009 (Project level punch): Verify employee other than assignee, not in TO and CC should not get punch details.
    * Scripted By : NARESH  BABU KAVURU
    * @throws Exception
    */
    @Test(priority = 8, enabled = true, description = "TC_009 (Project level punch): Verify employee other then assignee, not in TO and CC should not get punch details.")
    public void verify_Employee_OtherThan_Assignee_NotInTOAndCC_ShouldNotGetPunchList() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_009 (Project level punch): Verify employee other then assignee, not in TO and CC should not get punch details.");
    	//Getting Static data from properties file
    		String UserName = PropertyReader.getProperty("UserName_Punch");
    		String Password = PropertyReader.getProperty("Password_Punch");
    		String Project_Name = PropertyReader.getProperty("Punch_StaticEmp");
    		String Employee_Name = PropertyReader.getProperty("Punch_Emp1_Name");
    		String Employee_Mailid = PropertyReader.getProperty("Punch_Emp1_Mail");
    		String CC_UserName = PropertyReader.getProperty("Punch_Emp2_Name");
    		String Employee3_MailId = PropertyReader.getProperty("Punch_Emp3_Mail");
    		String Punch_Description = PropertyReader.getProperty("Punch_Descr_WithoutAtchmnts");
    		String Stamp_Number = Generate_Random_Number.randomStampNumber(); 		
 		
    		projectsLoginPage = new ProjectsLoginPage(driver).get();  
    		projectDashboardPage=projectsLoginPage.loginWithValidCredential(UserName,Password);//Calling Login MethodS
  		
    		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
 		
    		punchPage = new PunchPage(driver).get();
    		folderPage=punchPage.SelectPunchList();//Select Punch List
  			
    		Log.assertThat(punchPage.Create_Punch_NoAttachments_AndValidate(Stamp_Number, Employee_Name, Employee_Mailid, CC_UserName, Punch_Description), 
  				"Create Punch without attachments working Successfully", "Create Punch without attachments is Not working", driver); 
  		
    		projectDashboardPage.Logout_Projects();//Logout from Owner
    		projectsLoginPage.loginWithValidCredential(Employee3_MailId,Password);//Login as Employee
    		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
    		punchPage.SelectPunchList();//Select Punch List
 		
    		Log.assertThat(punchPage.Verify_PunchFrom_Employee_OtherThanAssignee_ToAndCC(), 
 				"Punch list not found for only employee.", "Punch found for employee with out having relation with punch.", driver);
     		
    	}
       catch(Exception e)
       {
    	   AnalyzeLog.analyzeLog(driver);
       		e.getCause();
        	Log.exception(e, driver);
       }
    	finally
    	{
    		Log.endTestCase();
    		driver.quit();
    	}
    }
    
    /** TC_010 (Project level punch): Verify the CC user should not be change punch status other than comment. 
     * Scripted By : NARESH  BABU KAVURU
     * @throws Exception
     */
     @Test(priority = 9, enabled = true, description = "TC_010 (Project level punch): Verify the CC user should not be change punch status other than comment.")
     public void verify_CCUser_ShouldNotChange_PunchDetails() throws Exception
     {
     	try
     	{
     		Log.testCaseInfo("TC_010 (Project level punch): Verify the CC user should not be change punch status other than comment.");
     	//Getting Static data from properties file
     		String UserName = PropertyReader.getProperty("UserName_Punch");
     		String Password = PropertyReader.getProperty("Password_Punch");
     		String Project_Name = PropertyReader.getProperty("Punch_StaticEmp");
     		String Employee_Name = PropertyReader.getProperty("Punch_Emp1_Name");
     		String Employee_Mailid = PropertyReader.getProperty("Punch_Emp1_Mail");
     		String CC_UserName = PropertyReader.getProperty("Punch_Emp2_Name");
     		String CC_User_MailId = PropertyReader.getProperty("Punch_Emp2_Mail");
     		String Punch_Description = PropertyReader.getProperty("Punch_Descr_WithoutAtchmnts");
     		String Stamp_Number = Generate_Random_Number.randomStampNumber();
     		String Employee_Comment = PropertyReader.getProperty("CCUser_Comment");
  		
     		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     		projectDashboardPage=projectsLoginPage.loginWithValidCredential(UserName,Password);//Calling Login MethodS
   		
     		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
  		
     		punchPage = new PunchPage(driver).get();
     		folderPage=punchPage.SelectPunchList();//Select Punch List
   			
     		Log.assertThat(punchPage.Create_Punch_NoAttachments_AndValidate(Stamp_Number, Employee_Name, Employee_Mailid, CC_UserName, Punch_Description), 
   				"Create Punch without attachments working Successfully", "Create Punch without attachments is Not working", driver); 
   		
     		projectDashboardPage.Logout_Projects();//Logout from Owner
     		projectsLoginPage.loginWithValidCredential(CC_User_MailId,Password);//Login as CC user
     		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
     		punchPage.SelectPunchList();//Select Punch List
     		
     		File Path_FMProperties = new File(PropertyReader.getProperty("SingleFIle_TestData_Path"));
    		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
    		Log.assertThat(punchPage.Verify_PunchComment_WithAttachments_AndValidate(Employee_Comment, FolderPath), 
       				"User comment with attachment on punch successfully.", "User comment with attachment is failed", driver);
  		
     		Log.assertThat(punchPage.Verify_CCUser_NotGettingStatusButtons(), 
  				"CC user not getting any status related buttons.", "CC user is getting status related buttons.", driver);
     	}
        catch(Exception e)
        {
        	AnalyzeLog.analyzeLog(driver);
        	e.getCause();
         	Log.exception(e, driver);
        }
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     	}
     }
     
/** TC_011 (Viewer level punch): Create a punch without any attachments and validate.
 * Scripted By : NARESH  BABU kavuru
 * Staging
 * @throws Exception
 */
@Test(priority = 10, enabled = true, description = "TC_011 (Viewer level punch): Create a punch without any attachments and validate.")
public void verify_CreatePunch_WithoutAttachments_Viewer() throws Exception
{
	try
	{
  		Log.testCaseInfo("TC_011 (Viewer level punch): Create a punch without any attachments and validate.");
  	//Getting Static data from properties file
  		String UserName = PropertyReader.getProperty("UserName_Punch");
  		String Password = PropertyReader.getProperty("Password_Punch");
  		String Project_Name = PropertyReader.getProperty("Prj_ViewerPunch");
  		String Foldername = PropertyReader.getProperty("Folder_ViewerPunch");
  		String File_Name = PropertyReader.getProperty("File1_Punch");
  		String Employee_Name = PropertyReader.getProperty("Punch_Emp1_Name");
  		String Employee_Mailid = PropertyReader.getProperty("Punch_Emp1_Mail");
  		String CC_UserName = PropertyReader.getProperty("Punch_Emp2_Name");
  		String Punch_Description = PropertyReader.getProperty("Punch_Descr_WithoutAtchmnts");
		
  		projectsLoginPage = new ProjectsLoginPage(driver).get();  
  		projectDashboardPage=projectsLoginPage.loginWithValidCredential(UserName,Password);//Calling Login MethodS
  		
  		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
  		folderPage.Select_Folder(Foldername);//Select Folder
  		folderPage.Open_Expected_File_InViewer(File_Name);//Select File
  		
  		punchPage = new PunchPage(driver).get();
 		folderPage=punchPage.SelectPunchAnnotation();//Select Punch Annotation
  
  		String Stamp_Number = Generate_Random_Number.randomStampNumber();
  		Log.assertThat(punchPage.Create_Punch_NoAttachments_AndValidate_InViewer(Stamp_Number, Employee_Name, Employee_Mailid, CC_UserName, Punch_Description), 
  				"Create Punch Annotation without attachments working Successfully", "Create Punch Annotation without attachments is Not working", driver);     
  	}
	catch(Exception e)
	{
		AnalyzeLog.analyzeLog(driver);
     	e.getCause();
      	Log.exception(e, driver);
	}
  	finally
  	{
  		Log.endTestCase();
  		driver.quit();
  	}
}

/** TC_012 (Viewer level punch): Create a punch with attachments and validate.
 * Scripted By : NARESH  BABU kavuru
 * Staging
 * @throws Exception
 */
@Test(priority = 11, enabled = true, description = "TC_012 (Viewer level punch): Create a punch with attachments and validate.")
public void verify_CreatePunch_WithAttachments_Viewer() throws Exception
{
	try
	{
  		Log.testCaseInfo("TC_012 (Viewer level punch): Create a punch without any attachments and validate.");
  	//Getting Static data from properties file
  		String UserName = PropertyReader.getProperty("UserName_Punch");
  		String Password = PropertyReader.getProperty("Password_Punch");
  		String Project_Name = PropertyReader.getProperty("Prj_ViewerPunch");
  		String Foldername = PropertyReader.getProperty("Folder_ViewerPunch");
  		String File_Name = PropertyReader.getProperty("File1_Punch");
  		String Employee_Name = PropertyReader.getProperty("Punch_Emp1_Name");
  		String Employee_Mailid = PropertyReader.getProperty("Punch_Emp1_Mail");
  		String CC_UserName = PropertyReader.getProperty("Punch_Emp2_Name");
  		String Punch_Description = PropertyReader.getProperty("Punch_Descr_Atchmts");
  		String File_Name_InnerAttach = PropertyReader.getProperty("File2_Punch");
  		
  		projectsLoginPage = new ProjectsLoginPage(driver).get();  
  		projectDashboardPage=projectsLoginPage.loginWithValidCredential(UserName,Password);//Calling Login MethodS
  		
  		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
  		folderPage.Select_Folder(Foldername);//Select Folder
  		folderPage.Open_Expected_File_InViewer(File_Name);//Select File
  		
  		punchPage = new PunchPage(driver).get();
 		folderPage=punchPage.SelectPunchAnnotation();//Select Punch Annotation
  
 		File Path_FMProperties = new File(PropertyReader.getProperty("DonotInd_TestData_Path"));
 		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
  		String Stamp_Number = Generate_Random_Number.randomStampNumber();
  		Log.assertThat(punchPage.Create_Punch_WithAttachments_AndValidate_InViewer(Stamp_Number, Employee_Name, Employee_Mailid, CC_UserName, Punch_Description, FolderPath,File_Name_InnerAttach), 
  				"Create Punch Annotation with attachments working Successfully", "Create Punch Annotation with attachments is Not working", driver);     
  	}
	catch(Exception e)
	{
		AnalyzeLog.analyzeLog(driver);
     	e.getCause();
      	Log.exception(e, driver);
	}
  	finally
  	{
  		Log.endTestCase();
  		driver.quit();
  	}
}

/** TC_013 (Viewer level punch): Create a Stamp, Edit and Delete from Manage Stamp and validate.
 * Scripted By : NARESH  BABU kavuru
 * Staging
 * @throws Exception
 */
@Test(priority = 12, enabled = true, description = "TC_013 (Viewer level punch): Create a Stamp, Edit and Delete from Manage Stamp and validate.")
public void verify_CreateStamp_EditDelete_ManageStamp_Viewer() throws Exception
{
	try
	{
  		Log.testCaseInfo("TC_013 (Viewer level punch): Create a Stamp, Edit and Delete from Manage Stamp and validate.");
  	//Getting Static data from properties file
  		String UserName = PropertyReader.getProperty("UserName_Punch");
  		String Password = PropertyReader.getProperty("Password_Punch");
  		String Project_Name = PropertyReader.getProperty("StampCreate_Project");
  		String Foldername = PropertyReader.getProperty("StampCreate_Folder");
  		String File_Name = PropertyReader.getProperty("File1_Punch");
  		String Stamp_Creater = PropertyReader.getProperty("PunchCreater_Name");
  		
  		projectsLoginPage = new ProjectsLoginPage(driver).get();  
  		projectDashboardPage=projectsLoginPage.loginWithValidCredential(UserName,Password);//Calling Login MethodS
  		
  		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
  		folderPage.Select_Folder(Foldername);//Select Folder
  		folderPage.Open_Expected_File_InViewer(File_Name);//Select File
  		
  		punchPage = new PunchPage(driver).get();
 		folderPage=punchPage.SelectPunchAnnotation();//Select Punch Annotation
  
  		String Stamp_Number = Generate_Random_Number.randomStampNumber();
  		Log.assertThat(punchPage.Create_Edit_DeleteStamp_FromManageStamp_Viewer(Stamp_Number, Stamp_Creater), 
  				"Create Punch Annotation without attachments working Successfully", "Create Punch Annotation without attachments is Not working", driver);     
  	}
	catch(Exception e)
	{
		AnalyzeLog.analyzeLog(driver);
     	e.getCause();
      	Log.exception(e, driver);
	}
  	finally
  	{
  		Log.endTestCase();
  		driver.quit();
  	}
}

/** TC_014 (Viewer level punch): Verify punch list from receiver, download attachments, commit and change the status to Completed.
 * Scripted By : NARESH  BABU kavuru
 * Staging
 * @throws Exception
 */
@Test(priority = 13, enabled = true, description = "TC_014 (Viewer level punch): Verify punch list from receiver, download attachments, commit and change the status to Completed.")
public void verify_PunchList_DownloadAttachments_Commit_ChangeStatus_ToCompleted_Viewer() throws Exception
{
	try
	{
  		Log.testCaseInfo("TC_014 (Viewer level punch): Verify punch list from receiver, download attachments, commit and change the status to Completed.");
  	//Getting Static data from properties file
  		String UserName = PropertyReader.getProperty("UserName_Punch");
  		String Password = PropertyReader.getProperty("Password_Punch");
  		String Project_Name = PropertyReader.getProperty("Prj_ViewerPunch");
  		String Foldername = PropertyReader.getProperty("Folder_ViewerPunch");
  		String File_Name = PropertyReader.getProperty("File1_Punch");
  		String Employee_Name = PropertyReader.getProperty("Punch_Emp1_Name");
  		String Employee_Mailid = PropertyReader.getProperty("Punch_Emp1_Mail");
  		String CC_UserName = PropertyReader.getProperty("Punch_Emp2_Name");
  		String Punch_Description = PropertyReader.getProperty("Punch_Descr_Atchmts");
  		String File_Name_InnerAttach = PropertyReader.getProperty("File2_Punch");
  		String usernamedir=System.getProperty("user.name");
 		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
 		String Employee_Comment = PropertyReader.getProperty("Receiver_Comment");
  		
  		projectsLoginPage = new ProjectsLoginPage(driver).get();  
  		projectDashboardPage=projectsLoginPage.loginWithValidCredential(UserName,Password);//Calling Login MethodS
  		
  		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
  		folderPage.Select_Folder(Foldername);//Select Folder
  		folderPage.Open_Expected_File_InViewer(File_Name);//Select File
  		
  		punchPage = new PunchPage(driver).get();
 		folderPage=punchPage.SelectPunchAnnotation();//Select Punch Annotation
  
 		File Path_FMProperties = new File(PropertyReader.getProperty("DonotInd_TestData_Path"));
 		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
  		String Stamp_Number = Generate_Random_Number.randomStampNumber();
  		Log.assertThat(punchPage.Create_Punch_WithAttachments_AndValidate_InViewer(Stamp_Number, Employee_Name, Employee_Mailid, CC_UserName, Punch_Description, FolderPath,File_Name_InnerAttach), 
  				"Create Punch Annotation without attachments working Successfully", "Create Punch Annotation without attachments is Not working", driver);  
  		projectDashboardPage.Logout_Projects();//Logout from Owner
 		projectsLoginPage.loginWithValidCredential(Employee_Mailid,Password);//Login as Employee
 		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
 		folderPage.Select_Folder(Foldername);//Select Folder
 		folderPage.Open_Expected_File_InViewer(File_Name);//Select File
 		punchPage.Select_ExpectedPunch_InViewer(Stamp_Number, Punch_Description);//Open expected punch
 		Log.assertThat(punchPage.Verify_download_attachments_from_receiver_Viewer(Sys_Download_Path), 
				"User successfully downloaded punch attachment.", "User failed to download punch attachment.", driver);
 		Log.assertThat(punchPage.Verify_PunchComment_AndValidate(Employee_Comment), 
				"User is able to comment on punch successfully.", "User is NOT able to comment on punch", driver);
 		Log.assertThat(punchPage.Verify_StatusChange_ToCompleted_Viewer(), 
				"Status changed to Completed successfully.", "Status changed to Completed is failed.", driver);
 		
  	}
	catch(Exception e)
	{
		AnalyzeLog.analyzeLog(driver);
     	e.getCause();
      	Log.exception(e, driver);
	}
  	finally
  	{
  		Log.endTestCase();
  		driver.quit();
  	}
}

/** TC_015 (Viewer level punch): Verify Punch Save as Draft and assign the drafted punch.
 * Scripted By : NARESH  BABU kavuru
 * Staging
 * @throws Exception
 */
@Test(priority = 14, enabled = true, description = "TC_015 (Viewer level punch): Verify Punch Save as Draft and assign the drafted punch.")
public void verify_PunchSaveAsDraft_Assign_Viewer() throws Exception
{
	try
 	{
 		Log.testCaseInfo("TC_015 (Viewer level punch): Verify Punch Save as Draft and assign the drafted punch.");
 	//Getting Static data from properties file
 		String UserName = PropertyReader.getProperty("UserName_Punch");
  		String Password = PropertyReader.getProperty("Password_Punch");
  		String Project_Name = PropertyReader.getProperty("Prj_ViewerPunch");
  		String Foldername = PropertyReader.getProperty("Folder_ViewerPunch");
  		String File_Name = PropertyReader.getProperty("File1_Punch");
  		String Employee_Name = PropertyReader.getProperty("Punch_Emp1_Name");
  		String Employee_Mailid = PropertyReader.getProperty("Punch_Emp1_Mail");
   		String Punch_Description = PropertyReader.getProperty("PunchDraft_Description");
 		
   		projectsLoginPage = new ProjectsLoginPage(driver).get();  
   		projectDashboardPage=projectsLoginPage.loginWithValidCredential(UserName,Password);//Calling Login MethodS
   		
   		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
   		folderPage.Select_Folder(Foldername);//Select Folder
  		folderPage.Open_Expected_File_InViewer(File_Name);//Select File
  		
  		punchPage = new PunchPage(driver).get();
 		folderPage=punchPage.SelectPunchAnnotation();//Select Punch Annotation
   		
   		String Stamp_Number = Generate_Random_Number.randomStampNumber();
   		Log.assertThat(punchPage.Create_Punch_SaveAsDraft_Viewer(Stamp_Number, Employee_Name, Employee_Mailid, Punch_Description), 
   				"Drafting a Punch is working Successfully", "Drafting a Punch is Not working", driver);
   		
  		Log.assertThat(punchPage.Verify_Assign_DraftedPunch_InViewer(Stamp_Number), 
 				"Assigning Drafted punch is working successfully.", "Assigning Drafted punch is failed.", driver);
  		
 	}
    catch(Exception e)
    {
    	AnalyzeLog.analyzeLog(driver);
    	e.getCause();
     	Log.exception(e, driver);
    }
 	finally
 	{
 		Log.endTestCase();
 		driver.quit();
 	}
}

/** TC_016 (Viewer level punch): Create a punch by attaching Photo and validate from Gallery.
 * Scripted By : NARESH  BABU kavuru
 * Staging
 * @throws Exception
 */
 @Test(priority = 15, enabled = true, description = "TC_016 (Viewer level punch): Create a punch by attaching Photo and validate from Gallery.")
 public void verify_CreatePunch_AttachPhotos_ValidateGallery_Viewer() throws Exception
 {
 	try
 	{
 		Log.testCaseInfo("TC_016 (Viewer level punch): Create a punch by attaching Photo and validate from Gallery.");
 	//Getting Static data from properties file
 		String UserName = PropertyReader.getProperty("NewData_Username");
 		String Password = PropertyReader.getProperty("NewData_Password");
 		String Employee_Name = PropertyReader.getProperty("Employee1_Name");
 		String Employee_Mailid = PropertyReader.getProperty("Employee_Email");
		String Punch_Description = PropertyReader.getProperty("Punch_Descr_Atchmts");
		String File_Name = PropertyReader.getProperty("File1_Punch");
		
 		projectsLoginPage = new ProjectsLoginPage(driver).get();  
 		projectDashboardPage=projectsLoginPage.loginWithValidCredential(UserName,Password);//Calling Login MethodS
 		
 		String Project_Name = "Downld_PunchReport"+getCurrentDate.getISTTime();
 		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
 		String Foldername = "Fold_"+Generate_Random_Number.generateRandomValue();
 		folderPage.New_Folder_Create(Foldername);//Create a new Folder
 		
 		folderPage.Select_Folder(Foldername);//Select a folder - newly created
 			
 		File Path_FMProperties = new File(PropertyReader.getProperty("DonotInd_TestData_Path"));
 		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
 		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
 		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
 		folderPage.UploadFiles_WithoutIndex(FolderPath,FileCount);//Calling Upload using do not index
 		folderPage.Select_Folder(Foldername);//Select a folder - newly created
 		folderPage.Open_Expected_File_InViewer(File_Name);//Select File
  		
  		punchPage = new PunchPage(driver).get();
 		folderPage=punchPage.SelectPunchAnnotation();//Select Punch Annotation
 		
 		File Photo_Path_FMProperties = new File(PropertyReader.getProperty("SinglePhoto_TestData_Path"));
		String FolderPath_Photo = Photo_Path_FMProperties.getAbsolutePath().toString();
 		String Stamp_Number = Generate_Random_Number.randomStampNumber();
 		Log.assertThat(punchPage.Create_Punch_OutSideAttachments_AndValidate_InViewer(Stamp_Number, Employee_Name, Employee_Mailid, Punch_Description, FolderPath_Photo), 
 				"Create Punch with attachment from computer working Successfully", "Create Punch with attachments is Not working", driver);  
 		
 		driver.findElement(By.xpath("//a[@class='aProjHeaderLink']")).click();//Click on Project Header Link
 		folderPage.Select_Gallery_Folder();//Select Gallery
 		
 		String CountOfPhotosInFolder = PropertyReader.getProperty("FileCou_OneFile");
 		int PhotoCount = Integer.parseInt(CountOfPhotosInFolder);//String to Integer
 		Log.assertThat(folderPage.ValidateUploadPhotos(FolderPath_Photo, PhotoCount), 
  				"Photos are available in Gallery Folder","Photos are NOT available in Gallery Folder", driver);
 	}
    catch(Exception e)
    {
    	AnalyzeLog.analyzeLog(driver);
    	e.getCause();
     	Log.exception(e, driver);
    }
 	finally
 	{
 		Log.endTestCase();
 		driver.quit();
 	}
 }
 
 /** TC_017 (Viewer level punch): Verify download punch report.
  * Scripted By : NARESH  BABU kavuru
  * Staging
  * @throws Exception
  */
  @Test(priority = 16, enabled = true, description = "TC_017 (Viewer level punch): Verify download punch report.")
  public void verify_Download_PunchReport_Viewer() throws Exception
  {
  	try
  	{
  		Log.testCaseInfo("TC_017 (Viewer level punch): Verify download punch report.");
  	//Getting Static data from properties file
  		String UserName = PropertyReader.getProperty("NewData_Username");
  		String Password = PropertyReader.getProperty("NewData_Password");
 		
  		projectsLoginPage = new ProjectsLoginPage(driver).get();  
  		projectDashboardPage=projectsLoginPage.loginWithValidCredential(UserName,Password);//Calling Login MethodS
  		//Using Recently created project as static data
  		String Project_Name = "Downld_PunchReport"+getCurrentDate.getISTTime();
  		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
  		
  		punchPage = new PunchPage(driver).get();
  		folderPage=punchPage.SelectPunchList();//Select Punch List
  		String usernamedir=System.getProperty("user.name");
 		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
  		Log.assertThat(punchPage.Verify_download_Punch_Report(Sys_Download_Path), 
   				"Generate punch report working successfullt.","Generate punch report not working.", driver);
  	}
    catch(Exception e)
    {
    	AnalyzeLog.analyzeLog(driver);
     	e.getCause();
      	Log.exception(e, driver);
    }
  	finally
  	{
  		Log.endTestCase();
  		driver.quit();
  	}
  }
  
/** TC_018 (Viewer level punch): Verify download attachments from comment window and change the status to Close.
 * Scripted By : NARESH  BABU kavuru
 * Staging
 * @throws Exception
 */
@Test(priority = 17, enabled = true, description = "TC_018 (Viewer level punch): Verify download attachments from comment window and change the status to Close.",groups = "naresh_test")
public void verify_DownloadCommentAttachments_ChangeStatus_ToClose_Viewer() throws Exception
{
   	try
   	{
   		Log.testCaseInfo("TC_018 (Viewer level punch): Verify download attachments from comment window and change the status to Close.");
   	//Getting Static data from properties file
   		String UserName = PropertyReader.getProperty("UserName_Punch");
  		String Password = PropertyReader.getProperty("Password_Punch");
  		String Project_Name = PropertyReader.getProperty("Prj_ViewerPunch");
  		String Foldername = PropertyReader.getProperty("Folder_ViewerPunch");
  		String File_Name = PropertyReader.getProperty("File1_Punch");
   		String Employee_Name = PropertyReader.getProperty("Punch_Emp1_Name");
   		String Employee_Mailid = PropertyReader.getProperty("Punch_Emp1_Mail");
   		String CC_UserName = PropertyReader.getProperty("Punch_Emp2_Name");
 		String Punch_Description = PropertyReader.getProperty("Punch_Descr_WithoutAtchmnts");
 		String usernamedir=System.getProperty("user.name");
		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
		String Employee_Comment = PropertyReader.getProperty("Receiver_Comment");
  		
  		projectsLoginPage = new ProjectsLoginPage(driver).get();  
  		projectDashboardPage=projectsLoginPage.loginWithValidCredential(UserName,Password);//Calling Login MethodS
  		
  		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
  		folderPage.Select_Folder(Foldername);//Select Folder
  		folderPage.Open_Expected_File_InViewer(File_Name);//Select File
  		
  		punchPage = new PunchPage(driver).get();
 		folderPage=punchPage.SelectPunchAnnotation();//Select Punch Annotation
  
  		String Stamp_Number = Generate_Random_Number.randomStampNumber();
  		Log.assertThat(punchPage.Create_Punch_NoAttachments_AndValidate_InViewer(Stamp_Number, Employee_Name, Employee_Mailid, CC_UserName, Punch_Description), 
  				"Create Punch Annotation without attachments working Successfully", "Create Punch Annotation without attachments is Not working", driver);  
 		
 		projectDashboardPage.Logout_Projects();//Logout from Owner
		projectsLoginPage.loginWithValidCredential(Employee_Mailid,Password);//Login as Employee
		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
		//folderPage.Select_Folder(Foldername);//Select Folder
 		//folderPage.Open_Expected_File_InViewer(File_Name);//Select File
 		//punchPage.Select_ExpectedPunch_InViewer(Stamp_Number, Punch_Description);//Open expected punch
		punchPage.SelectPunchList();//Select Punch List
 		File Path_FMProperties = new File(PropertyReader.getProperty("SingleFIle_TestData_Path"));
		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
		Log.assertThat(punchPage.Verify_PunchComment_WithAttachments_AndValidate(Employee_Comment, FolderPath), 
   				"User comment with attachment on punch successfully.", "User comment with attachment is failed", driver);
 		//Log.assertThat(punchPage.Verify_StatusChange_ToCompleted_Viewer(), 
				//"Status changed to Completed successfully.", "Status changed to Completed is failed.", driver);
		
		//projectDashboardPage.Logout_Projects();//Logout from Employee
		//projectsLoginPage.loginWithValidCredential(UserName,Password);//Calling Login Method
		//.Validate_SelectAProject(Project_Name);//Select A Project
		//folderPage.Select_Folder(Foldername);//Select Folder
 		//folderPage.Open_Expected_File_InViewer(File_Name);//Select File
 		//punchPage.Select_ExpectedPunch_InViewer(Stamp_Number, Punch_Description);//Open expected punch
		//punchPage.SelectPunchList();//Select Punch List
		//Log.assertThat(punchPage.Verify_download_attachments_from_PunchHistory(Sys_Download_Path), 
				//"User successfully downloaded punch attachment.", "User failed to download punch attachment.", driver);
    		
		//Log.assertThat(punchPage.Verify_StatusChange_ToClosed_AndValidate_Viewer(Stamp_Number), 
				//"Status changed to Closed successfully.", "Status changed to Closed is failed.", driver);	
   	}
    catch(Exception e)
    {
    	AnalyzeLog.analyzeLog(driver);
      	e.getCause();
       	Log.exception(e, driver);
    }
   	finally
   	{
   		Log.endTestCase();
   		driver.quit();
   	}
}

/** TC_019 (Viewer level punch): Verify employee other than assignee, not in TO and CC should not get punch details.
 * Scripted By : NARESH  BABU KAVURU
 * Staging
 * @throws Exception
 */
 @Test(priority = 18, enabled = true, description = "TC_019 (Viewer level punch): Verify employee other than assignee, not in TO and CC should not get punch details.")
 public void verify_Employee_OtherThan_Assignee_NotInTOAndCC_ShouldNotGetPunchList_Viewer() throws Exception
 {
 	try
 	{
 		Log.testCaseInfo("TC_019 (Viewer level punch): Verify employee other than assignee, not in TO and CC should not get punch details.");
 	//Getting Static data from properties file
 		String UserName = PropertyReader.getProperty("UserName_Punch");
 		String Password = PropertyReader.getProperty("Password_Punch");
 		String Project_Name = PropertyReader.getProperty("Prj_ViewerPunch");
  		String Foldername = PropertyReader.getProperty("Folder_ViewerPunch");
  		String File_Name = PropertyReader.getProperty("File1_Punch");
 		String Employee_Name = PropertyReader.getProperty("Punch_Emp1_Name");
 		String Employee_Mailid = PropertyReader.getProperty("Punch_Emp1_Mail");
 		String CC_UserName = PropertyReader.getProperty("Punch_Emp2_Name");
 		String Employee3_MailId = PropertyReader.getProperty("Punch_Emp3_Mail");
 		String Punch_Description = PropertyReader.getProperty("Punch_Descr_WithoutAtchmnts");	
		
 		projectsLoginPage = new ProjectsLoginPage(driver).get();  
 		projectDashboardPage=projectsLoginPage.loginWithValidCredential(UserName,Password);//Calling Login MethodS
		
 		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
 		folderPage.Select_Folder(Foldername);//Select Folder
  		folderPage.Open_Expected_File_InViewer(File_Name);//Select File
  		
  		punchPage = new PunchPage(driver).get();
 		folderPage=punchPage.SelectPunchAnnotation();//Select Punch Annotation
  
  		String Stamp_Number = Generate_Random_Number.randomStampNumber();
  		Log.assertThat(punchPage.Create_Punch_NoAttachments_AndValidate_InViewer(Stamp_Number, Employee_Name, Employee_Mailid, CC_UserName, Punch_Description), 
  				"Create Punch Annotation without attachments working Successfully", "Create Punch Annotation without attachments is Not working", driver);  
 		
 		projectDashboardPage.Logout_Projects();//Logout from Owner
 		projectsLoginPage.loginWithValidCredential(Employee3_MailId,Password);//Login as Employee
 		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
 		punchPage.SelectPunchList();//Select Punch List
		
 		Log.assertThat(punchPage.Verify_PunchFrom_Employee_OtherThanAssignee_ToAndCC(), 
				"Punch list not found for only employee.", "Punch found for employee with out having relation with punch.", driver);
  		
 	}
    catch(Exception e)
    {
    	AnalyzeLog.analyzeLog(driver);
 	   	e.getCause();
 	   	Log.exception(e, driver);
    }
 	finally
 	{
 		Log.endTestCase();
 		driver.quit();
 	}
 }
 
 /** TC_020 (Viewer level punch): Verify the CC user should not be change punch status other than comment. 
  * Scripted By : NARESH  BABU KAVURU
  * Staging
  * @throws Exception
  */
  @Test(priority = 19, enabled = true, description = "TC_020 (Viewer level punch): Verify the CC user should not be change punch status other than comment.")
  public void verify_CCUser_ShouldNotChange_PunchDetails_Viewer() throws Exception
  {
  	try
  	{
  		Log.testCaseInfo("TC_020 (Viewer level punch): Verify the CC user should not be change punch status other than comment.");
  	//Getting Static data from properties file
  		String UserName = PropertyReader.getProperty("UserName_Punch");
 		String Password = PropertyReader.getProperty("Password_Punch");
 		String Project_Name = PropertyReader.getProperty("Prj_ViewerPunch");
  		String Foldername = PropertyReader.getProperty("Folder_ViewerPunch");
  		String File_Name = PropertyReader.getProperty("File1_Punch");
 		String Employee_Name = PropertyReader.getProperty("Punch_Emp1_Name");
 		String Employee_Mailid = PropertyReader.getProperty("Punch_Emp1_Mail");
 		String CC_UserName = PropertyReader.getProperty("Punch_Emp2_Name");
  		String CC_User_MailId = PropertyReader.getProperty("Punch_Emp2_Mail");
  		String Punch_Description = PropertyReader.getProperty("Punch_Descr_WithoutAtchmnts");
  		String Stamp_Number = Generate_Random_Number.randomStampNumber();
  		String Employee_Comment = PropertyReader.getProperty("Receiver_Comment");
		
  		projectsLoginPage = new ProjectsLoginPage(driver).get();  
  		projectDashboardPage=projectsLoginPage.loginWithValidCredential(UserName,Password);//Calling Login MethodS
		
  		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
  		folderPage.Select_Folder(Foldername);//Select Folder
  		folderPage.Open_Expected_File_InViewer(File_Name);//Select File
  		
  		punchPage = new PunchPage(driver).get();
 		folderPage=punchPage.SelectPunchAnnotation();//Select Punch Annotation
 
  		Log.assertThat(punchPage.Create_Punch_NoAttachments_AndValidate_InViewer(Stamp_Number, Employee_Name, Employee_Mailid, CC_UserName, Punch_Description), 
  				"Create Punch Annotation without attachments working Successfully", "Create Punch Annotation without attachments is Not working", driver);  
 		
 		projectDashboardPage.Logout_Projects();//Logout from Owner
  		projectsLoginPage.loginWithValidCredential(CC_User_MailId,Password);//Login as CC user
  		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
  		punchPage.SelectPunchList();//Select Punch List
  		
  		File Path_FMProperties = new File(PropertyReader.getProperty("SingleFIle_TestData_Path"));
 		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
 		Log.assertThat(punchPage.Verify_PunchComment_WithAttachments_AndValidate(Employee_Comment, FolderPath), 
    				"User comment with attachment on punch successfully.", "User comment with attachment is failed", driver);
		
  		Log.assertThat(punchPage.Verify_CCUser_NotGettingStatusButtons(), 
				"CC user not getting any status related buttons.", "CC user is getting status related buttons.", driver);
  	}
    catch(Exception e)
    {
    	AnalyzeLog.analyzeLog(driver);
     	e.getCause();
      	Log.exception(e, driver);
    }
  	finally
  	{
  		Log.endTestCase();
  		driver.quit();
  	}
  }

}
