package com.arc.projects.testscripts;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.ITestResult;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.SubmitalPage;
import com.arc.projects.utils.AnalyzeLog;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arcautoframe.utils.Log;

public class Project_Dashboard_Suit {
	
	String webSite;
    ITestResult result;
    
    static WebDriver driver;
    ProjectsLoginPage projectsLoginPage;
    ProjectDashboardPage projectDashboardPage;
    FolderPage folderPage;
    SubmitalPage submitalPage;
    
    @Parameters("browser")
    @BeforeMethod (groups = "naresh_test")//Newly Added due to group execution Naresh
 public WebDriver beforeTest(String browser) {
        if(browser.equalsIgnoreCase("firefox")) 
        {
               File dest = new File("./drivers/win/geckodriver.exe");
               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
               driver = new FirefoxDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
        
        else if (browser.equalsIgnoreCase("chrome")) 
        { 
        	File dest = new File("./drivers/win/chromedriver.exe");
            System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());  
            Map<String, Object> prefs = new HashMap<String, Object>();
            prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
            
            prefs.put("safebrowsing.enabled", "false");//this condition should be f@lse every time
 
            ChromeOptions options = new ChromeOptions();
             options.addArguments("--start-maximized");
            options.setExperimentalOption("prefs", prefs);
            driver = new ChromeDriver( options );
            driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        } 
        
        else if (browser.equalsIgnoreCase("safari"))
        {
        	System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
            driver = new SafariDriver();
            driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
   return driver;
 }
 
/** TC_001 (Create a project, Edit and Delete the project): Verify Create a project, Edit and Delete the project.
 * Scripted By : NARESH BABU KAVURU
 * @throws Exception
 */
     @Test(priority = 0, enabled = true, description = "TC_001 (Create a project, Edit and Delete the project): Verify Create a project, Edit and Delete the project.")
     public void verify_CreateProject_Edit_Delete() throws Exception
     {
     	try
     	{
     		Log.testCaseInfo("TC_001 (Create a project, Edit and Delete the project): Verify Create a project, Edit and Delete the project.");
     	//Getting Static data from properties file
     		String uName = PropertyReader.getProperty("NewData_Username");
     		String pWord = PropertyReader.getProperty("NewData_Password");
   		
     		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     		String Project_Name = "Project_"+Generate_Random_Number.generateRandomValue();
     		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
     		
     		String Edit_projectName = "EditProject_"+Generate_Random_Number.generateRandomValue();
     		Log.assertThat(projectDashboardPage.Edit_Project(Project_Name, Edit_projectName), "Project Edit Successfully","Project Edit Failed", driver);
                     		
     		Log.assertThat(projectDashboardPage.Delete_Project(Edit_projectName), "Project Deleted successfully","Project Delete Failed", driver);                 
     	}
        catch(Exception e)
        {
        	AnalyzeLog.analyzeLog(driver);
         	e.getCause();
         	Log.exception(e, driver);
        }
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     	}
     }
 
/** TC_002 (Validate Protected project Creation and access from Employee): Verify Protected project Creation.
 * Scripted By : NARESH BABU KAVURU
 * @throws Exception
 */
     @Test(priority = 1, enabled = true, description = "TC_002 (Validate Protected project Creation and access from Employee): Verify Protected project Creation.")
     public void verify_Protected_Project_Creation() throws Exception
     {
    	 try
    	 {
    		 Log.testCaseInfo("TC_002 (Validate Protected project Creation and access from Employee): Verify Protected project Creation.");
    	     //Getting Static data from properties file
    	     String uName = PropertyReader.getProperty("NewData_Username");
    	     String pWord = PropertyReader.getProperty("NewData_Password");
    	     String Emp_UserName = PropertyReader.getProperty("Employee_Email");
    	           		
    	     projectsLoginPage = new ProjectsLoginPage(driver).get();  
    	     projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
    	      
    	     String Project_Name = "Batch_"+Generate_Random_Number.generateRandomValue();
    	     //String Invalid_Password = "123abc";
    	     folderPage=projectDashboardPage.ProjectCreation_WithPassword(Project_Name, pWord);//Create Protected project
    	     projectDashboardPage.Logout_Projects();//Logout
    	     projectsLoginPage.loginWithValidCredential(Emp_UserName,pWord);//Login with Employee
    	      
    	     //Log.assertThat(projectDashboardPage.OpenPasswordProtectedProject_WithInvalidPassword(Project_Name, Invalid_Password), "Unable to open project with Invalid Password", "Able to open project with Invalid Password", driver);
    	      
    	     Log.assertThat(projectDashboardPage.selectPasswordProtectedProject(Project_Name, pWord), 
    	     		"Able to open project with Valid Password", "UnAble to open project with Valid Password", driver); 
    	 }
    	 catch(Exception e)
    	 {
    		 AnalyzeLog.analyzeLog(driver);
    		 e.getCause();
    		 Log.exception(e, driver);
    	 }
    	 finally
    	 {
    		 Log.endTestCase();
    		 driver.quit();
    	 }
     }
     
/** TC_003 (Validate making project as Favorite and UnFavorite): Verify making project as Favorite and UnFavorite.
* Scripted By : NARESH BABU KAVURU
* @throws Exception
*/
     @Test(priority = 2, enabled = true, description = "TC_003 (Validate making project as Favorite and UnFavorite): Verify making project as Favorite and UnFavorite.")
     public void verify_FavoriteAndUnFavoriteProject() throws Exception
     {
    	 try
    	 {
    		 Log.testCaseInfo("TC_003 (Validate making project as Favorite and UnFavorite): Verify making project as Favorite and UnFavorite.");
    		 //Getting Static data from properties file
    		 String uName = PropertyReader.getProperty("Username");
    		 String pWord = PropertyReader.getProperty("Password");
    		
    		 projectsLoginPage = new ProjectsLoginPage(driver).get();  
    		 projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
                      		
    		 Log.assertThat(projectDashboardPage.AddFavorite_RemoveFavorite(), "Add Favorite & UnFavorite successfully","Add Favorite & UnFavorite Failed", driver);                 
    	 }
    	 catch(Exception e)
    	 {
    		 AnalyzeLog.analyzeLog(driver);
    		 e.getCause();
    		 Log.exception(e, driver);
    	 }
    	 finally
    	 {
    		 Log.endTestCase();
    		 driver.quit();
    	 }
     }

/** TC_004 (Validate All the Footer Links(Download Sync, FAQ, Support, Feedback & Logout)): Verify All the Footer Links.
* Scripted By : NARESH BABU KAVURU
* @throws Exception
*/
     @Test(priority = 3, enabled = true, description = "TC_004 (Validate All the Footer Links(Download Sync, FAQ, Support, Feedback & Logout)): Verify All the Footer Links.")
     public void verify_AllFooterLinks() throws Exception
     {
    	 try
    	 {
    		 Log.testCaseInfo("TC_004 (Validate All the Footer Links(Download Sync, FAQ, Support, Feedback & Logout)): Verify All the Footer Links.");
    		 //Getting Static data from properties file
    		 String uName = PropertyReader.getProperty("NewData_Username");
    		 String pWord = PropertyReader.getProperty("NewData_Password");
        		
    		 projectsLoginPage = new ProjectsLoginPage(driver).get();  
    		 projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
   
    		 /*String usernamedir=System.getProperty("user.name");
    		 String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
    		 Log.assertThat(projectDashboardPage.validateFooterLink_DownloadDesktopSync(Sys_Download_Path), 
    				 "Download Desktop Sync Link is working successfully", "Download Desktop Sync Link is NOT working", driver);*/
    		 //Log.assertThat(projectDashboardPage.validateFooterLink_FAQ(), 
    				 //"FAQ Link is working successfully", "FAQ Link is NOT working", driver);
    		 //Log.assertThat(projectDashboardPage.validateFooterLink_Support(), 
    				 //"Support Link is working successfully", "Support Link is NOT working", driver);
    		 //Log.assertThat(projectDashboardPage.validateFooterLink_Feedback(), 
    		 	//"Feedback Link is working successfully", "Feedback Link is NOT working", driver);
    		 Log.assertThat(projectDashboardPage.validateFooterLink_Logout(), 
    				 "Logout Link is working successfully", "Logout Link is NOT working", driver);
  
    	 }
    	 catch(Exception e)
    	 {
    		 AnalyzeLog.analyzeLog(driver);
    		 e.getCause();
    		 Log.exception(e, driver);
    	 }
    	 finally
    	 {
    		 Log.endTestCase();
    		 driver.quit();
    	 }
     }
 
/** TC_005 (Do the Send Files and validate from Tracking.): Verify Do the Send Files and validate from Tracking.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 */
    @Test(priority = 4, enabled = true, description = "TC_005 (Do the Send Files and validate from Tracking.): Verify Do the Send Files and validate from Tracking.")
    public void verify_SendFiles_validateTrack() throws Exception
    {
     	try
     	{
     		Log.testCaseInfo("TC_005 (Do the Send Files and validate from Tracking.): Verify Do the Send Files and validate from Tracking.");
     	//Getting Static data from properties file
     		String uName = PropertyReader.getProperty("NewData_Username");
     		String pWord = PropertyReader.getProperty("NewData_Password");
     		String Rec_MailID = PropertyReader.getProperty("User_Folder_Exp");
     		File Path_FMProperties = new File(PropertyReader.getProperty("DonotInd_TestData_Path"));
     		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
     		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
     		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
   		
     		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login Method
     		
     		Log.assertThat(projectDashboardPage.validate_SendFilesModule(FolderPath, Rec_MailID, FileCount), 
     				"Send Files module is working.", "Send Files module is NOT working.", driver);     		     
     	}
         catch(Exception e)
         {
        	 AnalyzeLog.analyzeLog(driver);
        	 e.getCause();
        	 Log.exception(e, driver);
         }
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     	}
     }
    
/** TC_006 (Do the Send Files and validate from Inbox.): Verify Do the Send Files and validate from Inbox.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 */
    @Test(priority = 5, enabled = true, description = "TC_006 (Do the Send Files and validate from Inbox.): Verify Do the Send Files and validate from Inbox.")
    public void verify_SendFiles_validateInbox() throws Exception
    {
    	try
    	{
         	Log.testCaseInfo("TC_006 (Do the Send Files and validate from Inbox.): Verify Do the Send Files and validate from Inbox.");
         	//Getting Static data from properties file
         	String uName = PropertyReader.getProperty("NewData_Username");
         	String pWord = PropertyReader.getProperty("NewData_Password");
         	String Rec_MailID = PropertyReader.getProperty("User_Folder_Exp");
         	File Path_FMProperties = new File(PropertyReader.getProperty("DonotInd_TestData_Path"));
         	String FolderPath = Path_FMProperties.getAbsolutePath().toString();
         	String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
         	int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
       		
         	projectsLoginPage = new ProjectsLoginPage(driver).get();  
         	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login Method
         		
         	Log.assertThat(projectDashboardPage.validate_SendFilesModule(FolderPath, Rec_MailID, FileCount), 
         			"Send Files module is working.", "Send Files module is NOT working.", driver);
         	
         	projectDashboardPage.Logout_Projects();//Logout from sender
         	projectsLoginPage.loginWithValidCredential(Rec_MailID,pWord);//Login as receiver
         	
         	Log.assertThat(projectDashboardPage.validate_SendFiles_Details_Inbox(), 
         			"Send Files Order details available in Inbox.", "Send Files Order details NOT available in Inbox.", driver);
         	
    	}
    	catch(Exception e)
    	{
    		AnalyzeLog.analyzeLog(driver);
    		e.getCause();
    		Log.exception(e, driver);
    	}
    	finally
    	{
    		Log.endTestCase();
    		driver.quit();
    	}
    }
    
/** TC_007 (validate ZIP & File Download from Inbox and Tracking of Download.): Verify ZIP & File Download from Inbox and Tracking of Download.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 */
    @Test(priority = 6, enabled = true, description = "TC_007 (validate ZIP & File Download from Inbox and Tracking of Download.): Verify ZIP & File Download from Inbox and Tracking of Download.",groups = "naresh_test")
    public void verify_SendFiles_RecInbox_Download() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_007 (validate ZIP & File Download from Inbox and Tracking of Download.): Verify ZIP & File Download from Inbox and Tracking of Download.");
         	//Getting Static data from properties file
         	String uName = PropertyReader.getProperty("NewData_Username");
         	String pWord = PropertyReader.getProperty("NewData_Password");
         	String Rec_MailID = PropertyReader.getProperty("User_Folder_Exp");
         	String usernamedir=System.getProperty("user.name");
     		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
       		
         	projectsLoginPage = new ProjectsLoginPage(driver).get();  
         	projectDashboardPage=projectsLoginPage.loginWithValidCredential(Rec_MailID,pWord);//Calling Login Method
         		
         	Log.assertThat(projectDashboardPage.validate_ZipDownload_SendFilesInbox(Sys_Download_Path), 
         			"Zip Download working from Inbox.", "Zip Download NOT working from Inbox.", driver);
         	Log.assertThat(projectDashboardPage.validate_FileDownload_SendFilesDetailsWindow(Sys_Download_Path), 
         			"File Download working from Inbox Details Window.", "File Download NOT working from Inbox Details Window.", driver);
         	
         	projectDashboardPage.Logout_Projects();//Logout from sender
         	projectsLoginPage.loginWithValidCredential(uName,pWord);//Login as Sender
         	
         	Log.assertThat(projectDashboardPage.validate_DownloadTrackingDetail(), 
         			"Download Tracking Details are properly updated.", "Download Tracking Details are NOT properly updated.", driver);	
        }
        catch(Exception e)
        {
        	AnalyzeLog.analyzeLog(driver);
        	e.getCause();
        	Log.exception(e, driver);
        }
        finally
        {
        	Log.endTestCase();
        	driver.quit();
        }
    }
    
/** TC_008 (validate Order Prints from Inbox.): Verify Order Prints from Inbox.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 */
    @Test(priority = 7, enabled = false, description = "TC_008 (validate Order Prints from Inbox.): Verify Order Prints from Inbox.")
    public void verify_Orderprints_RecInbox() throws Exception
    {
    	try
        {
    		Log.testCaseInfo("TC_008 (validate Order Prints from Inbox.): Verify Order Prints from Inbox.");
         	//Getting Static data from properties file
    		String Rec_MailID = PropertyReader.getProperty("User_Folder_Exp");
         	String pWord = PropertyReader.getProperty("NewData_Password");        	
         	String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
         	int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
       		
         	projectsLoginPage = new ProjectsLoginPage(driver).get();  
         	projectDashboardPage=projectsLoginPage.loginWithValidCredential(Rec_MailID,pWord);//Calling Login Method
         	
         	Log.assertThat(projectDashboardPage.validate_OrderPrints_SendFilesInbox(FileCount), 
         			"Doing Order Prints from the send files inbox is Working.", "Doing Order Prints from the send files inbox is NOT Working.", driver);
        }
    	catch(Exception e)
        {
    		AnalyzeLog.analyzeLog(driver);
        	e.getCause();
        	Log.exception(e, driver);
        }
        finally
        {
        	Log.endTestCase();
        	driver.quit();
        }
    }
    
/** TC_001_FolderLevelOperations (validate Create a folder Edit and Delete.): Verify Create a folder Edit and Delete.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 */
    @Test(priority = 8, enabled = true, description = "TC_001_FolderLevelOperations (validate Create a folder Edit and Delete.): Verify Create a folder Edit and Delete.")
    public void verify_FolderCreate_Edit_Delete() throws Exception
    {
        try
        {
        	Log.testCaseInfo("TC_001_FolderLevelOperations (validate Create a folder Edit and Delete.): Verify Create a folder Edit and Delete.");
         	//Getting Static data from properties file
    		String uName = PropertyReader.getProperty("Username_Export");
         	String pWord = PropertyReader.getProperty("Password_Export");        	
       		
         	projectsLoginPage = new ProjectsLoginPage(driver).get();  
         	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login Method
         	
         	String Project_Name = PropertyReader.getProperty("prj_FolderOperations");
    		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
    		
         	String Foldername = "Fold_"+Generate_Random_Number.generateRandomValue();	
     		folderPage.New_Folder_Create(Foldername);//Create a new Folder
     		
         	String Edit_FoldName = "Edit_Fold_"+Generate_Random_Number.generateRandomValue();
         	Log.assertThat(folderPage.Expected_Folder_Edit(Foldername, Edit_FoldName), 
         			"Folder Edit done Successfully.", "Folder Edit is Failed.", driver);
         	
         	Log.assertThat(folderPage.Expected_Folder_Delete(Edit_FoldName), 
         			"Folder Delete done Successfully.", "Folder Delete is Failed.", driver);
        }
    	catch(Exception e)
        {
    		AnalyzeLog.analyzeLog(driver);
        	e.getCause();
        	Log.exception(e, driver);
        }
        finally
        {
        	Log.endTestCase();
        	driver.quit();
        }
    }
    
/** TC_002_FolderLevelOperations (validate Download a Folder.): Verify Download a Folder.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 */
    @Test(priority = 9, enabled = true, description = "TC_002_FolderLevelOperations (validate Download a Folder.): Verify Download a Folder.")
    public void verify_Folder_Download() throws Exception
    {
    	try
    	{
    		Log.testCaseInfo("TC_002_FolderLevelOperations (validate Download a Folder.): Verify Download a Folder.");
         	//Getting Static data from properties file
    		String uName = PropertyReader.getProperty("Username_Export");
         	String pWord = PropertyReader.getProperty("Password_Export");        	
       		
         	projectsLoginPage = new ProjectsLoginPage(driver).get();  
         	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login Method
         	
         	String Project_Name = PropertyReader.getProperty("prj_FolderOperations");
    		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
    		
         	String Foldername = PropertyReader.getProperty("Fold_LevOperations");	
         	String usernamedir=System.getProperty("user.name");
     		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
    		Log.assertThat(folderPage.Download_Folder(Sys_Download_Path,Foldername), 
    				"Download a Folder is working Successfully", "Download a Folder is Not working", driver);
        }
    	catch(Exception e)
        {
    		AnalyzeLog.analyzeLog(driver);
        	e.getCause();
        	Log.exception(e, driver);
        }
        finally
        {
        	Log.endTestCase();
        	driver.quit();
        }
    }
    
/** TC_003_FolderLevelOperations (validate OrderPrints of a Folder.): Verify OrderPrints of a Folder.
 * Scripted By : NARESH  BABU KAVURU
 * @throws Exception
 */
    @Test(priority = 10, enabled = false, description = "TC_003_FolderLevelOperations (validate OrderPrints of a Folder.): Verify OrderPrints of a Folder.")
    public void verify_Folder_OrderPrints() throws Exception
    {
    	try
        {	
    		Log.testCaseInfo("TC_003_FolderLevelOperations (validate OrderPrints of a Folder.): Verify OrderPrints of a Folder.");
         	//Getting Static data from properties file
    		String uName = PropertyReader.getProperty("Username_Export");
         	String pWord = PropertyReader.getProperty("Password_Export");
         	String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
         	int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
       		
         	projectsLoginPage = new ProjectsLoginPage(driver).get();  
         	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login Method
         	
         	String Project_Name = PropertyReader.getProperty("prj_FolderOperations");
    		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
    		
         	String Foldername = PropertyReader.getProperty("Fold_LevOperations");	
    		Log.assertThat(folderPage.validate_OrderPrints_FolderLevel(Foldername, FileCount), 
    				"Order Prints of a Folder is working Successfully", "Order Prints of a Folder is Not working", driver);
        }
    	catch(Exception e)
        {
    		AnalyzeLog.analyzeLog(driver);
        	e.getCause();
        	Log.exception(e, driver);
        }
        finally
        {
        	Log.endTestCase();
        	driver.quit();
        }
    }
    
/** TC_005_FolderLevelOperations (Verify User is not able to Send, OrderPrints & Download any blank folder.): Verify User is not able to Send, OrderPrints & Download any blank folder.
 * Scripted By : NARESH BABU KAVURU
 * @throws Exception
 */
    @Test(priority = 11, enabled = true, description = "TC_005_FolderLevelOperations (Verify User is not able to Send, OrderPrints & Download any blank folder.): Verify User is not able to Send, OrderPrints & Download any blank folder.")
    public void verify_EmptyFolder_Download_Send_OrderPrints() throws Exception
    {
    	try
    	{	
    		Log.testCaseInfo("TC_005_FolderLevelOperations (Verify User is not able to Send, OrderPrints & Download any blank folder.): Verify User is not able to Send, OrderPrints & Download any blank folder.");
         	//Getting Static data from properties file
    		String uName = PropertyReader.getProperty("Username_Export");
         	String pWord = PropertyReader.getProperty("Password_Export");
       		
         	projectsLoginPage = new ProjectsLoginPage(driver).get();  
         	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login Method
         	
         	String Project_Name = PropertyReader.getProperty("prj_FolderOperations");
    		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
    		
         	String Foldername = PropertyReader.getProperty("Fold_Empty");	
    		Log.assertThat(folderPage.validate_Download_EmptyFolder(Foldername), 
    				"Application is not allowing to Download empty folder.", "Application is allowing to Download empty folder.", driver);
    		Log.assertThat(folderPage.validate_SendLink_EmptyFolder(Foldername), 
    				"Application is not allowing to Send empty folder.", "Application is allowing to Send empty folder.", driver);
    		//Log.assertThat(folderPage.validate_OrderPrints_EmptyFolder(Foldername), 
    				//"Application is not allowing to OrderPrint empty folder.", "Application is allowing to OrderPrint empty folder.", driver);
        }
    	catch(Exception e)
        {
    		AnalyzeLog.analyzeLog(driver);
        	e.getCause();
        	Log.exception(e, driver);
        }
        finally
        {
        	Log.endTestCase();
        	driver.quit();
        }
    }

/**TC_006_FolderLevelOperations (Verify Exclude LD folder Functionality.): Verify Exclude LD folder Functionality.
 * Scripted By : NARESH BABU KAVURU
 * @throws Exception
 */
    @Test(priority = 12, enabled = true, description = "TC_006_FolderLevelOperations (Verify Exclude LD folder Functionality.): Verify Exclude LD folder Functionality.")
    public void verify_Exclude_LD_Folder_Functionality() throws Exception
    {
    	try
    	{	
    		Log.testCaseInfo("TC_006_FolderLevelOperations (Verify Exclude LD folder Functionality.): Verify Exclude LD folder Functionality.");
         	//Getting Static data from properties file
    		String uName = PropertyReader.getProperty("NewData_Username");
         	String pWord = PropertyReader.getProperty("NewData_Password");
       		
         	projectsLoginPage = new ProjectsLoginPage(driver).get();  
         	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login Method
         	
         	String Project_Name = "Excl_LD_Prj_"+Generate_Random_Number.generateRandomValue();
     		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
    		
         	String Foldername = "Excl_LD_Fold"+Generate_Random_Number.generateRandomValue();
         	folderPage.New_Folder_Create_ExcludeLD(Foldername);//Create a new Folder Exclude LD
         	
         	folderPage.Select_Folder(Foldername);//Select a folder - newly created
         	
         	File Path_FMProperties = new File(PropertyReader.getProperty("DonotInd_TestData_Path"));
       		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
       		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
       		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
       		folderPage.UploadFiles_WithoutIndex(FolderPath,FileCount);//Calling Upload using do not index
       		
       		Log.assertThat(folderPage.Validate_LD_FolderIsEmpty(), 
       				"Exclude LD Folder is working successfully","Exclude LD Folder is Not working", driver);
        }
    	catch(Exception e)
        {
    		AnalyzeLog.analyzeLog(driver);
        	e.getCause();
        	Log.exception(e, driver);
        }
        finally
        {
        	Log.endTestCase();
        	driver.quit();
        }
    }
    
/**TC_001_LatestDocuments (Verify Download of Latest Documents folder.): Verify Download of Latest Documents folder.
 * Scripted By : NARESH BABU KAVURU
 * @throws Exception
 */
    @Test(priority = 13, enabled = true, description = "TC_001_LatestDocuments (Verify Download of Latest Documents folder.): Verify Download of Latest Documents folder.")
    public void verify_LD_Folder_Download() throws Exception
    {
    	try
    	{	
    		Log.testCaseInfo("TC_001_LatestDocuments (Verify Download of Latest Documents folder.): Verify Download of Latest Documents folder.");
         	//Getting Static data from properties file
    		String uName = PropertyReader.getProperty("Username_Export");
         	String pWord = PropertyReader.getProperty("Password_Export");
       
         	projectsLoginPage = new ProjectsLoginPage(driver).get();  
         	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login Method
         	
         	String Project_Name = PropertyReader.getProperty("prj_FolderOperations");
    		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
     
    		String usernamedir=System.getProperty("user.name");
   		 	String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
       		Log.assertThat(folderPage.Download_LatestDocuments_Folder(Sys_Download_Path,Project_Name), 
       				"LD Folder Download is working successfully","LD Folder Download is Not working", driver);
        }
    	catch(Exception e)
        {
    		AnalyzeLog.analyzeLog(driver);
        	e.getCause();
        	Log.exception(e, driver);
        }
        finally
        {
        	Log.endTestCase();
        	driver.quit();
        }
    }

/**TC_002_LatestDocuments (Verify SendLink of latest Documents folder.): Verify SendLink of latest Documents folder.
 * Scripted By : NARESH BABU KAVURU
 * @throws Exception
 */
    @Test(priority = 14, enabled = true, description = "TC_002_LatestDocuments (Verify SendLink of latest Documents folder.): Verify SendLink of latest Documents folder.")
    public void verify_LD_Folder_SendLink() throws Exception
    {
        try
        {	
        	Log.testCaseInfo("TC_002_LatestDocuments (Verify SendLink of latest Documents folder.): Verify SendLink of latest Documents folder.");
         	//Getting Static data from properties file
    		String uName = PropertyReader.getProperty("Username_Export");
         	String pWord = PropertyReader.getProperty("Password_Export");
       
         	projectsLoginPage = new ProjectsLoginPage(driver).get();  
         	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login Method
         	
         	String Project_Name = PropertyReader.getProperty("prj_FolderOperations");
    		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
     
    		String usernamedir=System.getProperty("user.name");
   		 	String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
   		 	String Expected_FolderName = "LD-"+Project_Name+".zip";
       		Log.assertThat(folderPage.Validate_LD_Folder_SendLink(Sys_Download_Path,Expected_FolderName), 
       				"LD Folder SendLink is working successfully","LD Folder SendLink is Not working", driver);
        }
    	catch(Exception e)
        {
    		AnalyzeLog.analyzeLog(driver);
        	e.getCause();
        	Log.exception(e, driver);
        }
        finally
        {
        	Log.endTestCase();
        	driver.quit();
        }
    }
    
/**TC_003_LatestDocuments (Verify OrderPrints of latest Documents folder.): Verify OrderPrints of latest Documents folder.
 * Scripted By : NARESH BABU KAVURU
 * @throws Exception
 */
    @Test(priority = 15, enabled = false, description = "TC_003_LatestDocuments (Verify OrderPrints of latest Documents folder.): Verify OrderPrints of latest Documents folder.")
    public void verify_LD_Folder_OrderPrints() throws Exception
    {
    	try
    	{	
    		Log.testCaseInfo("TC_003_LatestDocuments (Verify OrderPrints of latest Documents folder.): Verify OrderPrints of latest Documents folder.");
         	//Getting Static data from properties file
    		String uName = PropertyReader.getProperty("Username_Export");
         	String pWord = PropertyReader.getProperty("Password_Export");
       
         	projectsLoginPage = new ProjectsLoginPage(driver).get();  
         	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login Method
         	
         	String Project_Name = PropertyReader.getProperty("prj_FolderOperations");
    		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
     
    		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
       		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
       		Log.assertThat(folderPage.validate_OrderPrints_LD_Folder(FileCount), 
       				"LD Folder OrderPrints is working successfully","LD Folder OrderPrints is Not working", driver);
        }
    	catch(Exception e)
        {
    		AnalyzeLog.analyzeLog(driver);
        	e.getCause();
        	Log.exception(e, driver);
        }
        finally
        {
        	Log.endTestCase();
        	driver.quit();
        }
    }
	

}
