package com.arc.projects.testscripts;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.PublishingPage;
import com.arc.projects.utils.AnalyzeLog;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.getCurrentDate;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)
public class PublishingScenarioPage {

	public static WebDriver driver;
    
    ProjectsLoginPage projectsLoginPage;
    ProjectDashboardPage projectDashboardPage;
    PublishingPage publishingPage;
   	FolderPage folderPage;
   	Generate_Random_Number generate_Random_Number;
   
    @Parameters("browser")
    @BeforeMethod (groups = "naresh_test")//Newly Added due to group execution Naresh
    public WebDriver beforeTest(String browser) {
        
        if(browser.equalsIgnoreCase("firefox")) 
        {
               File dest = new File("./drivers/win/geckodriver.exe");
               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
               driver = new FirefoxDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
        
        else if (browser.equalsIgnoreCase("chrome")) 
        { 
        File dest = new File("./drivers/win/chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());  
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
        ChromeOptions options = new ChromeOptions();
         options.addArguments("--start-maximized");
        options.setExperimentalOption("prefs", prefs);
        driver = new ChromeDriver( options );
        driver.get(PropertyReader.getProperty("SkysiteProdURL"));
 
        } 
        
        else if (browser.equalsIgnoreCase("safari"))
        {
               System.setProperty("webdriver.safari.noinstall", "false"); //To stop uninstall each time
               driver = new SafariDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
   return driver;
 }

    
    /** TC_001 (Publishing Cases): Verify Create a project and upload files using Without Index.
     * Scripted By : NARESH BABU KAVURU
     * @throws Exception
     */
         
    @Test(priority = 0, enabled = true, description = "TC_001 (Publishing Cases): Verify Create a project and upload files using Without Index.",groups = "naresh_test")
    public void VERIFY_UPLOAD_WITHOUTINDEX() throws Exception
    {
    	try
    	{
     		Log.testCaseInfo("TC_001 (Publishing Cases): Verify Create a project and upload files using Without Index.");
     	//Getting Static data from properties file
     		String uName = PropertyReader.getProperty("NewData_Username");
     		String pWord = PropertyReader.getProperty("NewData_Password");
    		
     		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     		//Defining static project based on the day throughout the suit
        	String Project_Name = "Ba_"+getCurrentDate.getISTTime();
     		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
     		
     		String Foldername = "WITHOUT_INDEX";	
     		folderPage.New_Folder_Create(Foldername);//Create a new Folder
     		
     		folderPage.Select_Folder(Foldername);//Select Folder
     		
     		File Path_FMProperties = new File(PropertyReader.getProperty("DonotInd_TestData_Path"));
       		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
       		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
       		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
     		Log.assertThat(folderPage.UploadFiles_WithoutIndex(FolderPath,FileCount), 
     				"Upload using Without Index working successfully","Upload using Without Index is Failed", driver);                
     	}
        catch(Exception e)
        {
        	AnalyzeLog.analyzeLog(driver);
         	e.getCause();
         	Log.exception(e, driver);
        }
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     	}
     }

    /** TC_002 (Publishing Cases): Verify upload files using With Index (Index By File Name).
     * Scripted By : NARESH BABU KAVURU
     * @throws Exception
     */
         
    @Test(priority = 1, enabled = true, description = "TC_002 (Publishing Cases): Verify upload files using With Index (Index By File Name).")
    public void VERIFY_UPLOAD_INDEXBYFILENAME() throws Exception
    {
    	try
    	{
     		Log.testCaseInfo("TC_002 (Publishing Cases): Verify upload files using With Index (Index By File Name).");
     	//Getting Static data from properties file
     		String uName = PropertyReader.getProperty("NewData_Username");
     		String pWord = PropertyReader.getProperty("NewData_Password");
    		
     		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     		//Defining static project based on the day throughout the suit
        	String Project_Name = "Ba_"+getCurrentDate.getISTTime();
     		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
     		
     		String Foldername = "IND_BY_FILENAME";	
     		folderPage.New_Folder_Create(Foldername);//Create a new Folder
     		
     		folderPage.Select_Folder(Foldername);//Select Folder
     		
     		File Path_FMProperties = new File(PropertyReader.getProperty("SingleFile_MultiPage_Path"));
       		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
       		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_Single");
       		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
       		File Path_SheetCount = new File(PropertyReader.getProperty("SheetCount_XlPath"));
       		String sheetCount_FilePath = Path_SheetCount.getAbsolutePath().toString();
     		Log.assertThat(folderPage.UploadFiles_IndexByFileName(FolderPath, FileCount, sheetCount_FilePath), 
     				"Upload using Index by file name is working successfully","Upload using Index by file name is Failed", driver); 
     	}
        catch(Exception e)
        {
        	AnalyzeLog.analyzeLog(driver);
         	e.getCause();
         	Log.exception(e, driver);
        }
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     	}
     }

    /** TC_003 (Publishing Cases): Verify upload files using With Index (Index By Sheet Number).
     * Scripted By : NARESH BABU KAVURU
     * @throws Exception
     */
         
    @Test(priority = 2, enabled = true, description = "TC_003 (Publishing Cases): Verify upload files using With Index (Index By Sheet Number).")
    public void VERIFY_UPLOAD_INDEXBYSHEETNUMBER() throws Exception
    {
    	try
    	{
     		Log.testCaseInfo("TC_003 (Publishing Cases): Verify upload files using With Index (Index By Sheet Number).");
     	//Getting Static data from properties file
     		String uName = PropertyReader.getProperty("NewData_Username");
     		String pWord = PropertyReader.getProperty("NewData_Password");
    		
     		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     		//Defining static project based on the day throughout the suit
        	String Project_Name = "Ba_"+getCurrentDate.getISTTime();
     		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
     		
     		String Foldername = "IND_BY_SHEETNUMBER";	
     		folderPage.New_Folder_Create(Foldername);//Create a new Folder
     		
     		folderPage.Select_Folder(Foldername);//Select Folder
     		
     		File Path_FMProperties = new File(PropertyReader.getProperty("SingleFile_MultiPage_Path"));
       		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
       		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_Single");
       		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
       		File Path_SheetCount = new File(PropertyReader.getProperty("SheetNumber_XLpath"));
       		String sheetCount_FilePath = Path_SheetCount.getAbsolutePath().toString();
     		Log.assertThat(folderPage.UploadFiles_IndexBySheetNumber(FolderPath, FileCount, sheetCount_FilePath), 
     				"Upload using Index by sheet number is working successfully","Upload using Index by sheet number is Failed", driver);                 
     	}
        catch(Exception e)
        {
        	AnalyzeLog.analyzeLog(driver);
         	e.getCause();
         	Log.exception(e, driver);
        }
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     	}
     }
    
    /** TC_004 (Publishing Cases): Verify Delete a session from publishing Log page.
     * Scripted By : NARESH BABU KAVURU
     * @throws Exception
     */
         
    @Test(priority = 3, enabled = true, description = "TC_004 (Publishing Cases): Verify Delete a session from publishing Log page.")
    public void VERIFY_DELETE_SESSION_FMPUBLISHINGLOGPAGE() throws Exception
    {
    	try
    	{
     		Log.testCaseInfo("TC_004 (Publishing Cases): Verify Delete a session from publishing Log page.");
     	//Getting Static data from properties file
     		String uName = PropertyReader.getProperty("NewData_Username");
     		String pWord = PropertyReader.getProperty("NewData_Password");
    		
     		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     		//Defining static project based on the day throughout the suit
        	String Project_Name = "Ba_"+getCurrentDate.getISTTime();
     		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
     		
     		String Foldername = "DELTALL_FM_PUBLISHPAGE";	
     		folderPage.New_Folder_Create(Foldername);//Create a new Folder
     		
     		folderPage.Select_Folder(Foldername);//Select Folder
     		
     		File Path_FMProperties = new File(PropertyReader.getProperty("SingleFile_MultiPage_Path"));
       		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
       		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_Single");
       		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
     		Log.assertThat(folderPage.Verify_DeleteIcon_From_ConversionPage(FolderPath, FileCount), 
     				"Delete a session from publishing Log page is working successfully","Delete a session from publishing Log page is not working", driver);                 
     	}
        catch(Exception e)
        {
        	AnalyzeLog.analyzeLog(driver);
         	e.getCause();
         	Log.exception(e, driver);
        }
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     	}
     }
    
    /** TC_005 (Publishing Cases): Verify Delete All button from publishing page.
     * Scripted By : NARESH BABU KAVURU
     * @throws Exception
     */
         
    @Test(priority = 4, enabled = true, description = "TC_005 (Publishing Cases): Verify Delete All button from publishing page.")
    public void VERIFY_DELETEALL_BUTTON_FMPUBLISHINGPAGE() throws Exception
    {
    	try
    	{
     		Log.testCaseInfo("TC_005 (Publishing Cases): Verify Delete All button from publishing page.");
     	//Getting Static data from properties file
     		String uName = PropertyReader.getProperty("NewData_Username");
     		String pWord = PropertyReader.getProperty("NewData_Password");
    		
     		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     		//Defining static project based on the day throughout the suit
        	String Project_Name = "Ba_"+getCurrentDate.getISTTime();
     		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
     		
     		String Foldername = "DELTALL_BUTTON_PUBLISHPAGE";	
     		folderPage.New_Folder_Create(Foldername);//Create a new Folder
     		
     		folderPage.Select_Folder(Foldername);//Select Folder
     		
     		File Path_FMProperties = new File(PropertyReader.getProperty("SingleFile_MultiPage_Path"));
       		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
       		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_Single");
       		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
     		Log.assertThat(folderPage.Verify_DeleteAllButton_From_PublishPage(FolderPath, FileCount), 
     				"Delete All button from publish page is working successfully","Delete All button from publish page is not working", driver);                 
     	}
        catch(Exception e)
        {
        	AnalyzeLog.analyzeLog(driver);
         	e.getCause();
         	Log.exception(e, driver);
        }
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     	}
     }
    
    /** TC_006 (Publishing Cases): Verify Publish by using Publish Later by Adding set name.
     * Scripted By : NARESH BABU KAVURU
     * @throws Exception
     */
         
    @Test(priority = 5, enabled = true, description = "TC_006 (Publishing Cases): Verify Publish by using Publish Later by Adding set name.",groups = "naresh_test")
    public void VERIFY_PUBLISH_LATER_BYADDING_SETNAME() throws Exception
    {
    	try
    	{
     		Log.testCaseInfo("TC_006 (Publishing Cases): Verify Publish by using Publish Later by Adding set name.");
     	//Getting Static data from properties file
     		String uName = PropertyReader.getProperty("NewData_Username");
     		String pWord = PropertyReader.getProperty("NewData_Password");
    		
     		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     		//Defining static project based on the day throughout the suit
        	String Project_Name = "Ba_"+getCurrentDate.getISTTime();
     		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
     		
     		String Foldername = "PUBLISH_SETNAME";	
     		folderPage.New_Folder_Create(Foldername);//Create a new Folder
     		
     		folderPage.Select_Folder(Foldername);//Select Folder
     		
     		File Path_FMProperties = new File(PropertyReader.getProperty("Ten_PdfFilea_Path"));
       		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
       		String CountOfFilesInFolder = PropertyReader.getProperty("Count_Ten_PdfFiles");
       		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
     		Log.assertThat(folderPage.Verify_PublishLater_ByAdding_SetName(FolderPath, FileCount), 
     				"Publish later is working successfully","Publish later is not working", driver);                 
     	}
        catch(Exception e)
        {
        	AnalyzeLog.analyzeLog(driver);
         	e.getCause();
         	Log.exception(e, driver);
        }
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     	}
     }
    
    /** TC_007 (Publishing Cases): Verify Skip the publishing session.
     * Scripted By : NARESH BABU KAVURU
     * @throws Exception
     */
         
    @Test(priority = 6, enabled = true, description = "TC_007 (Publishing Cases): Verify Skip the publishing session.")
    public void VERIFY_SKIP_PUBLISHING_SESSION() throws Exception
    {
    	try
    	{
     		Log.testCaseInfo("TC_007 (Publishing Cases): Verify Skip the publishing session.");
     	//Getting Static data from properties file
     		String uName = PropertyReader.getProperty("NewData_Username");
     		String pWord = PropertyReader.getProperty("NewData_Password");
    		
     		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     		//Defining static project based on the day throughout the suit
        	String Project_Name = "Ba_"+getCurrentDate.getISTTime();
     		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
     		
     		String Foldername = "SKIP_PUBLISH";	
     		folderPage.New_Folder_Create(Foldername);//Create a new Folder
     		
     		folderPage.Select_Folder(Foldername);//Select Folder
     		
     		File Path_FMProperties = new File(PropertyReader.getProperty("SingleFile_MultiPage_Path"));
       		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
       		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_Single");
       		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
       		Log.assertThat(folderPage.Verify_Skip_From_PublishPage(FolderPath, FileCount), 
     				"Skip button from publish page is working successfully","Skip button from publish page is not working", driver);                
     	}
        catch(Exception e)
        {
        	AnalyzeLog.analyzeLog(driver);
         	e.getCause();
         	Log.exception(e, driver);
        }
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     	}
     }

   
}






/////////////////////////////////RANJAN////////////////////////////////////

/*
 *//** TC_1 (Publishing Module Test-Cases):Verification Of Sheet Number After Uploading the file . 
  *  Scripted By Ranjan P
  *  
 * @throws Exception
 *//*
  public static String uName = PropertyReader.getProperty("Username");
   public static String pWord = PropertyReader.getProperty("Password");	   
   public static String FolderName = PropertyReader.getProperty("FolderName");	   
   public static String FullName = PropertyReader.getProperty("fullName");
   public static String state = PropertyReader.getProperty("state");	  
   public static String phoneNumber = PropertyReader.getProperty("phoneNumber");
   public static String password = PropertyReader.getProperty("password");
 @Test(priority = 0, enabled = false, description = "Verification Of Sheet Number After Uploading the file ")
 public void Verify_SheetNumber_Updation() throws Exception
 {
	 
 	try
 	{
 		
 		String Prj_Name = generate_Random_Number.ProjectName();
 		Log.testCaseInfo("TC_1(Publishing Page): Verification Of Sheet Number After Uploading the file ");
 	    //========Login Entry Into The Application=================================     		
 		projectsLoginPage = new ProjectsLoginPage(driver).get();  
  		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);   		
    	folderPage=projectDashboardPage.createProject(Prj_Name);
		folderPage.New_Folder_Create(FolderName);
		folderPage.Select_Folder(FolderName);
		String Req_FileCount = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(Req_FileCount);
	  	String FolderPath = PropertyReader.getProperty("Publishing_Upload_TestData_Path");	  
		//publishingPage = folderPage.UploadFiles_WithIndexing(FolderPath, FileCount);
		
		//folderPage = new FolderPage(driver).get();
  		publishingPage = projectDashboardPage.SimpleExecution();
		publishingPage.MultipleRowValidationwithExcelSheet(); ;
  		
  		
  		
 		
 		
 	}
     catch(Exception e)
     {
     	e.getCause();
     	Log.exception(e, driver);
     }
 	finally
 	{
 		Log.endTestCase();
 		//driver.quit();
 	}
 }*/

/*  *//** TC_2 (Publishing Module Test-Cases):Verification Of Multiple Sheet Number After Uploading the file . 
  *  Scripted By Ranjan P
  *  
 * @throws Exception
 *//*
 @Test(priority = 0, enabled = false, description = "Verification Multiple Sheet Number After Uploading the file ")
 public void Verify_MultipleSheet_Validation() throws Exception
 {
	 
 	try
 	{
 		
 		String Prj_Name = generate_Random_Number.ProjectName();
 		Log.testCaseInfo("TC_1(Publishing Page): Verification Of Sheet Number After Uploading the file ");
 	    //========Login Entry Into The Application=================================     		
 		projectsLoginPage = new ProjectsLoginPage(driver).get();  
  		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);   		
    	folderPage=projectDashboardPage.createProject(Prj_Name);
		folderPage.New_Folder_Create(FolderName);
		folderPage.Select_Folder(FolderName);
		String Req_FileCount = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(Req_FileCount);
	  	String FolderPath = PropertyReader.getProperty("Publishing_Upload_TestData_Path");	  
		publishingPage = folderPage.UploadFiles_WithIndexing(FolderPath, FileCount);
		
		folderPage = new FolderPage(driver).get();
  		//projectDashboardPage.SimpleExecution();
		
		//publishingPage.ValidationwithExcelSheet() ;
 		
 	}
     catch(Exception e)
     {
     	e.getCause();
     	Log.exception(e, driver);
     }
 	finally
 	{
 		Log.endTestCase();
 		//driver.quit();
 	}
 }*/
 
