package com.arc.projects.testscripts;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectAndFolder_Level_Search;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.ViewerScreenPage;
import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.PropertyReader;
import com.arcautoframe.utils.Log;

public class ViewerSanityTests {

	public static WebDriver driver;

	ProjectsLoginPage projectsLoginPage;
	ProjectDashboardPage projectDashboardPage;
	ViewerScreenPage viewerScreenPage;
	FolderPage folderPage;
	ProjectAndFolder_Level_Search projectAndFolder_Level_Search;

	@Parameters("browser")
	@BeforeMethod
	public WebDriver beforeTest(String browser) {

		if (browser.equalsIgnoreCase("firefox")) {
			File dest = new File("./drivers/win/geckodriver.exe");
			// System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}

		else if (browser.equalsIgnoreCase("chrome")) {
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory", "C:" + File.separator + "Users" + File.separator
					+ System.getProperty("user.name") + File.separator + "Downloads");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			options.setExperimentalOption("prefs", prefs);
			driver = new ChromeDriver(options);
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));

		}

		else if (browser.equalsIgnoreCase("safari")) {
			System.setProperty("webdriver.safari.noinstall", "true"); // To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		return driver;
	}

	/**
	 * TC_XXX (Viewer Console Log): Extract the Console Log for all Zoom Levels in Viewer
	 * 
	 * @throws Exception
	 * Created by Arka Halder
	 */

	@Test(priority = 1, enabled = true, description = "TC_XXX (Viewer Console Log): Extract the Console Log for all Zoom Levels in Viewer")
	public void extractConsoleLogForViewerZoomIn() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_XXX (Viewer Console Log): Extract the Console Log for all Zoom Levels in Viewer");
			String uName = PropertyReader.getProperty("Viewertestuser");
			String pWord = PropertyReader.getProperty("Viewertestuserpassword");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			folderPage = projectDashboardPage.enterviewerlevelProject();
			boolean allFilesDisplayed = folderPage.clickOnViewerSanityFolder();
			while (!allFilesDisplayed) {
				folderPage = new FolderPage(driver).get();
				allFilesDisplayed = folderPage.displayAllViewerSanityItems();
			}
			folderPage = new FolderPage(driver).get();
			int filesCount = folderPage.getViewerFileCount();
			for (int i = 0; i < filesCount; i++) {
			//for (int i = 0; i < 2; i++) {
				viewerScreenPage = folderPage.openingViewerFileForConsole(i);
				CommonMethod.analyzeLog(driver);
				for (int j = 0; j < 3; j++) {
					viewerScreenPage.zoomInViewer();
					CommonMethod.analyzeLog(driver);
				}
				folderPage = viewerScreenPage.closeViewerAndReturnToFolderPage();
			}
		} catch (Exception e) {
			
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}
	
}
